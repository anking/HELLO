package com.yanhua.media.fragment;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.module.effect.bgm.TCMusicInfo;
import com.tencent.qcloud.ugckit.utils.BackgroundTasks;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.media.R;
import com.yanhua.media.R2;
import com.yanhua.media.adapter.MusicInfoAdapter;
import com.yanhua.media.adapter.MusicTypeAdapter;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.model.MusicTypeModel;
import com.yanhua.media.presenter.VideoBgMusicPresenter;
import com.yanhua.media.presenter.contract.VideoBgMusicContract;
import com.yanhua.media.utils.YXMusicManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class RecomendMusicFragment extends BaseMvpFragment<VideoBgMusicPresenter> implements VideoBgMusicContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_recommend_music)
    RecyclerView rvRecommendMusic;

    private RecyclerView rvMusicType;
    private RelativeLayout llMoreMusicType;
    private RelativeLayout llMoreMusic;
    private MusicInfoAdapter mAdapter;
    private MusicTypeAdapter mMusicTypeAdapter;
    private int current = 1;
    private int size = 20;
    private int total;
    private HashMap<String, Object> params;
    private HashMap<String, Object> paramsMap;
    private List<MusicInfoModel> musicInfoList;
    private List<MusicTypeModel> musicTypeList;
    private MusicInfoModel currentMusic;
    private int currentPos = 0;
    private MediaPlayer mediaPlayer;
    private YXMusicManager.LoadMusicListener mLoadMusicListener;

    public RecomendMusicFragment() {
        // Required empty public constructor
    }

    @Override
    protected void creatPresent() {
        basePresenter = new VideoBgMusicPresenter();
    }


    @Override
    public int bindLayout() {
        return R.layout.fragment_recomend_music;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        musicInfoList = new ArrayList<>();
        mediaPlayer = new MediaPlayer();
        rvRecommendMusic.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new MusicInfoAdapter(getContext());
        View headerView = LayoutInflater.from(getContext()).inflate(R.layout.layout_header_recommend_music, null);
        rvMusicType = headerView.findViewById(R.id.rv_music_type);
        llMoreMusicType = headerView.findViewById(R.id.ll_more_music_type);
        llMoreMusic = headerView.findViewById(R.id.ll_more_music);
        llMoreMusicType.setOnClickListener(v -> ARouter.getInstance().build(ARouterPath.MUSIC_TYPE_ACTIVITY).navigation(getActivity(), UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE));
        llMoreMusic.setOnClickListener(v -> {
            ARouter.getInstance()
                    .build(ARouterPath.MUSIC_LIST_TYPE_ACTIVITY)
                    .withBoolean("isTop", true)
                    .navigation(getActivity(), UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
        });
        rvMusicType.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        mMusicTypeAdapter = new MusicTypeAdapter(getContext());
        rvMusicType.setAdapter(mMusicTypeAdapter);

        mMusicTypeAdapter.setOnItemClickListener((itemView, pos) -> {
            MusicTypeModel musicType = musicTypeList.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.MUSIC_LIST_TYPE_ACTIVITY)
                    .withSerializable("musicType", musicType)
                    .navigation(getActivity(), UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
        });
        mAdapter.addHeaderView(headerView);
        rvRecommendMusic.setAdapter(mAdapter);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvRecommendMusic.setRecycledViewPool(pool);
        //关闭rvRecommendMusic动画
        rvRecommendMusic.getItemAnimator().setAddDuration(0);
        rvRecommendMusic.getItemAnimator().setChangeDuration(0);
        rvRecommendMusic.getItemAnimator().setMoveDuration(0);
        rvRecommendMusic.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvRecommendMusic.getItemAnimator()).setSupportsChangeAnimations(false);

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            currentPos = 0;
            current = 1;

            paramsMap = new HashMap<>();
            paramsMap.put("current", 1);
            paramsMap.put("size", 10);
            getMusicTypeList();
            getMusicList();

            refreshLayout.finishRefresh(1000);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (musicInfoList.size() < total) {
                current++;
                getMusicList();
                refreshLayout.finishLoadMore(1000);
            } else {
                refreshLayout.finishLoadMore(1000, true, true);
            }
        });
        mAdapter.setOnItemClickOperateListener(new MusicInfoAdapter.OnItemClickOperateListener() {
            @Override
            public void followMusic(int pos) {
                //收藏
                if (pos <= 0) return;
//                currentPos = pos;
                currentMusic = musicInfoList.get(pos - 1);
                basePresenter.followMusicOrNot(currentMusic.getId(), pos);
            }

            @Override
            public void playMusic(View view, int pos) {
                //播放
                if (pos <= 0) return;
                if (currentPos != 0 && currentPos != pos) {
                    musicInfoList.get(currentPos - 1).setPlaying(false);
                    mAdapter.notifyItemChanged(currentPos);
                }

                currentPos = pos;
                currentMusic = musicInfoList.get(pos - 1);

                if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
                    currentMusic.setPlaying(!currentMusic.isPlaying());

                    // 播放音乐
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(currentMusic.getMusicUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();

                        // 设置播放的时候一直让屏幕变亮
                        mediaPlayer.setScreenOnWhilePlaying(true);
                        mediaPlayer.setOnCompletionListener(mp -> {
                            currentMusic.setPlaying(false);

                            mAdapter.notifyItemChanged(currentPos);//刷新一下状态
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
                    downloadMusic();
                }

                mAdapter.notifyItemChanged(currentPos);//刷新一下状态
            }

            @Override
            public void pauseMusic(View view, int pos) {
                //暂停
                if (pos <= 0) return;
                currentPos = pos;
                currentMusic = musicInfoList.get(pos - 1);
                currentMusic.setPlaying(false);
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    currentMusic.setPlaying(false);
                }
                if (view.getAnimation() != null) {
                    view.clearAnimation();
                }

                mAdapter.notifyDataSetChanged();
            }


            @Override
            public void onClickUseBtn(TextView tv, int pos) {
                //使用
                if (pos <= 0) return;
                currentPos = pos;
                currentMusic = musicInfoList.get(pos - 1);

                if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
                    // 已下载
                    backToEditActivity(pos, currentMusic.getLocalPath());
                }
            }
        });

        mLoadMusicListener = new YXMusicManager.LoadMusicListener() {
            @Override
            public void onBgmDownloadSuccess(MusicInfoModel model, final String filePath) {

                BackgroundTasks.getInstance().postDelayed(() -> {
                    if (model == null) return;

                    model.setStatus(MusicInfoModel.STATE_DOWNLOADED);
                    model.setLocalPath(filePath);

                    // 播放音乐
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(model.getMusicUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        // 设置播放的时候一直让屏幕变亮
                        mediaPlayer.setScreenOnWhilePlaying(true);
                        mediaPlayer.setOnCompletionListener(mp -> {
                            model.setPlaying(false);
                            mAdapter.notifyDataSetChanged();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    model.setPlaying(true);

                    mAdapter.notifyDataSetChanged();
                }, 200);
            }

            @Override
            public void onDownloadFail(MusicInfoModel model, final String errorMsg) {
                BackgroundTasks.getInstance().runOnUiThread(() -> {
                    if (model == null) return;
                    model.setStatus(MusicInfoModel.STATE_UNDOWNLOAD);
                    mAdapter.notifyDataSetChanged();
                    ToastUtils.showShort( "下载失败");
                });
            }

            @Override
            public void onDownloadProgress(MusicInfoModel model, final int progress) {
//                BackgroundTasks.getInstance().runOnUiThread(() -> {
                if (model == null) return;
                model.setStatus(MusicInfoModel.STATE_DOWNLOADING);
//                    mAdapter.notifyDataSetChanged();
//                });
            }
        };

        //拿取上次缓存数据（本地存放第一页数据）
//        if (null != mMMKV) {
//            Gson gson = new Gson();
//            String musicListJson = Session.get().getMusicListJson();
//
//            Type musicListType = new TypeToken<List<MusicInfoModel>>() {
//            }.getType();
//            if (!TextUtils.isEmpty(musicListJson)) {
//                musicInfoList = gson.fromJson(musicListJson, musicListType);
//                mAdapter.setItems(musicInfoList);
//            }
//
//            String musicTypeJson = Session.get().getMusicTypesJson();
//            Type musicTypeType = new TypeToken<List<MusicTypeModel>>() {
//            }.getType();
//            if (!TextUtils.isEmpty(musicTypeJson)) {
//                musicTypeList = gson.fromJson(musicTypeJson, musicTypeType);
//                mMusicTypeAdapter.setItems(musicTypeList);
//            }
//        }
    }


    private void downloadMusic() {
        YXMusicManager.getInstance().setOnLoadMusicListener(mLoadMusicListener);

        if (TextUtils.isEmpty(currentMusic.getLocalPath())) {
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
            return;
        }
        File file = new File(currentMusic.getLocalPath());
        if (!file.isFile()) {
            currentMusic.setLocalPath("");
            currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
        }
    }

    private void backToEditActivity(int position, String path) {
        Intent intent = new Intent();
        intent.putExtra(UGCKitConstants.MUSIC_POSITION, position);
        intent.putExtra(UGCKitConstants.MUSIC_PATH, path);
        intent.putExtra(UGCKitConstants.MUSIC_NAME, currentMusic.getName());
        intent.putExtra(UGCKitConstants.MUSIC_ITEM, currentMusic);
        getActivity().setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, intent);
        getActivity().finish();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        params = new HashMap<>();

        refreshLayout.autoRefresh();
    }

    private void getMusicTypeList() {
        basePresenter.getMusicTypeList(paramsMap);
    }

    public void getMusicList() {
        params.clear();
        params.put("current", current);
        params.put("size", size);
        params.put("isTop", 0);
        basePresenter.getMusicList(params);
    }

    public void getMusicList(int cur) {
        current = cur;
        params.clear();
        params.put("current", current);
        params.put("size", size);
        params.put("isTop", 0);
        basePresenter.getMusicList(params);
    }

    @Override
    public void handleSuccessMusicTypeList(ListResult<MusicTypeModel> result) {
        if (result != null && result.getRecords() != null) {
            musicTypeList = result.getRecords();

            Gson gson = new Gson();
            String content = gson.toJson(musicTypeList);

            DiscoCacheUtils.getInstance().setMusicTypesJson(content);

            if (!musicTypeList.isEmpty()) {
                mMusicTypeAdapter.setItems(musicTypeList);
            } else {
                llMoreMusicType.setVisibility(View.GONE);
                rvMusicType.setVisibility(View.GONE);
            }
        } else {
            llMoreMusicType.setVisibility(View.GONE);
            rvMusicType.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleSuccessMusicList(ListResult<MusicInfoModel> result) {
        if (current == 1) {
            musicInfoList.clear();
        }
        if (result != null && result.getRecords() != null) {
            List<MusicInfoModel> records = result.getRecords();

            if (current == 1) {
                Gson gson = new Gson();
                String content = gson.toJson(records);
                DiscoCacheUtils.getInstance().setMusicListJson(content);
            }

            total = result.getTotal();
            if (records != null && !records.isEmpty()) {
                YXMusicManager.getInstance().getLocalPath(records);
                musicInfoList.addAll(records);
            }
        }
        if (!musicInfoList.isEmpty()) {
            mAdapter.setItems(musicInfoList);
        }
    }

    @Override
    public void handleSuccessFollowMusicOrNot(int pos) {
        if (currentMusic.isFollow() == 1) {
            ToastUtils.showShort( "取消收藏");
        } else {
            ToastUtils.showShort( "收藏成功");
        }
//        EventBus.getDefault().post(new MessageEvent(CommonConstant.REFRESH_MUSIC_COLLECTION));

        MusicInfoModel item = musicInfoList.get(pos - 1);
        item.setIsFollow(item.isFollow() == 1 ? 0 : 1);

        mAdapter.notifyItemChanged(pos);
    }

    @Override
    public void handleFailFollowMusicOrNot(int pos) {
        if (currentMusic.isFollow() == 1) {
            ToastUtils.showShort("取消收藏失败");
        } else {
            ToastUtils.showShort("收藏失败");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        pauseMediaPlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void pauseMediaPlayer() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

            currentMusic.setPlaying(false);

            mAdapter.notifyItemChanged(currentPos);
        }
    }

}