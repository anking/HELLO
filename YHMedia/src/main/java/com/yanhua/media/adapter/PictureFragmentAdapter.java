package com.yanhua.media.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.DateUtils;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.luck.picture.lib.tools.ScreenUtils;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.CropImageView;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;
import com.yanhua.media.R;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PictureFragmentAdapter extends PagerAdapter {

    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 90;
    private Context mContext;
    private OnChangeCropListener onChangeCropListener;

    private final List<LocalMedia> data = new ArrayList<>();
    private final int mScreenWidth, mScreenHeight;
    /**
     * Maximum number of cached images
     */
    private static final int MAX_CACHE_SIZE = 20;
    /**
     * To cache the view
     */
    private final SparseArray<View> mCacheView = new SparseArray<>();

    public void clear() {
        mCacheView.clear();
    }

    public void removeCacheView(int position) {
        if (position < mCacheView.size()) {
            mCacheView.removeAt(position);
        }
    }

    public PictureFragmentAdapter(Context context, OnChangeCropListener listener) {
        super();
        this.mContext = context;

        this.mScreenWidth = ScreenUtils.getScreenWidth(context);
        this.mScreenHeight = ScreenUtils.getScreenHeight(context);

        this.onChangeCropListener = listener;
    }

    /**
     * bind data
     *
     * @param data
     */
    public void bindData(List<LocalMedia> data) {
        if (data != null) {
            this.data.clear();
            this.data.addAll(data);
        }
    }

    /**
     * get data
     *
     * @return
     */
    public List<LocalMedia> getData() {
        return data;
    }

    public int getSize() {
        return data.size();
    }

    public void remove(int currentItem) {
        if (getSize() > currentItem) {
            data.remove(currentItem);
        }
    }

    public LocalMedia getItem(int position) {
        return getSize() > 0 && position < getSize() ? data.get(position) : null;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((View) object);
        if (mCacheView.size() > MAX_CACHE_SIZE) {
            mCacheView.remove(position);
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NotNull View view, @NotNull Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup container, int position) {
        View contentView = mCacheView.get(position);
        if (contentView == null) {
            contentView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.yx_picture_image_preview, container, false);
            mCacheView.put(position, contentView);
        }
//        ImageView photoView = contentView.findViewById(R.id.preview_image);

        UCropView photoView = contentView.findViewById(R.id.preview_image);
        LinearLayout llCrop = contentView.findViewById(R.id.ll_crop);

        llCrop.setOnClickListener(v -> {
            if (null != this.onChangeCropListener) {
                this.onChangeCropListener.onChangeCrop(position);
            }
        });

        LocalMedia media = getItem(position);

        //获取图片信息
        String originalPath;
        String mimeType = media.getMimeType();
        float twidth;
        float theight;

        if (media.isCut() && !media.isCompressed()) {
            originalPath = media.getCutPath();

            twidth = media.getCropImageWidth();
            theight = media.getCropImageHeight();
        } else if (media.isCompressed() || (media.isCut() && media.isCompressed())) {
            originalPath = media.getCompressPath();
            twidth = media.getWidth();
            theight = media.getHeight();
        } else {
            originalPath = media.getPath();
            twidth = media.getWidth();
            theight = media.getHeight();
        }

        GestureCropImageView mGestureCropImageView = photoView.getCropImageView();
        OverlayView mOverlayView = photoView.getOverlayView();

        mGestureCropImageView.setTransformImageListener(new TransformImageView.TransformImageListener() {
            @Override
            public void onRotate(float currentAngle) {
            }

            @Override
            public void onScale(float currentScale) {
            }

            @Override
            public void onLoadComplete() {
                photoView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());

                photoView.postDelayed(() -> mGestureCropImageView.cropAndSaveImage(mCompressFormat, mCompressQuality, new BitmapCropCallback() {
                    @Override
                    public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
//                                Log.e("onLoadComplete", media.getFileName() + "==onBitmapCropped=");
                        setResultUri(media, resultUri, mGestureCropImageView.getTargetAspectRatio(), offsetX, offsetY, imageWidth, imageHeight);
                    }

                    @Override
                    public void onCropFailure(@NonNull Throwable t) {
//                                Log.e("onLoadComplete", media.getFileName() + "==onCropFailure=");
                    }
                }), 200);
            }

            @Override
            public void onLoadFailure(@NonNull Exception e) {
            }
        });

        if (twidth > 0 && theight > 0) {
            float originAspectRatioX = CropImageView.SOURCE_IMAGE_ASPECT_RATIO, originAspectRatioY = CropImageView.SOURCE_IMAGE_ASPECT_RATIO;

            float wh = 0;
            wh = twidth / theight;//4:3 1:1 3:4
            if (wh < 0.9f) {
                originAspectRatioX = 3;
                originAspectRatioY = 4;
            } else if (wh >= 0.9 && wh <= 1.1f) {
                originAspectRatioX = 1;
                originAspectRatioY = 1;
            } else if (wh > 1.1) {
                originAspectRatioX = 4;
                originAspectRatioY = 3;
            }

            // 只需让图片的宽是屏幕的宽，高乘以比例
            float displayHeight = mScreenWidth * originAspectRatioY / originAspectRatioX;
            //最终让图片按照宽是屏幕 高是等比例缩放的大小
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) photoView.getLayoutParams();
            layoutParams.width = mScreenWidth;
            layoutParams.height = (int) displayHeight;

            photoView.setLayoutParams(layoutParams);

            setImageInfo(originalPath, mimeType, mGestureCropImageView, mOverlayView);

            mGestureCropImageView.setTargetAspectRatio(originAspectRatioX / originAspectRatioY);
            mGestureCropImageView.setImageToWrapCropBounds();

        } else {
            Glide.with(mContext).asBitmap().load(originalPath).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, com.bumptech.glide.request.transition.Transition<? super Bitmap> transition) {
                    float width = bitmap.getWidth();
                    float height = bitmap.getHeight();
                    float originAspectRatioX = CropImageView.SOURCE_IMAGE_ASPECT_RATIO, originAspectRatioY = CropImageView.SOURCE_IMAGE_ASPECT_RATIO;
                    float wh = 0;
                    wh = width / height;//4:3 1:1 3:4
                    if (wh < 0.9f) {
                        originAspectRatioX = 3;
                        originAspectRatioY = 4;
                    } else if (wh >= 0.9 && wh <= 1.1f) {
                        originAspectRatioX = 1;
                        originAspectRatioY = 1;
                    } else if (wh > 1.1) {
                        originAspectRatioX = 4;
                        originAspectRatioY = 3;
                    }

                    // 只需让图片的宽是屏幕的宽，高乘以比例
                    float displayHeight = mScreenWidth * originAspectRatioY / originAspectRatioX;
                    //最终让图片按照宽是屏幕 高是等比例缩放的大小
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) photoView.getLayoutParams();
                    layoutParams.width = mScreenWidth;
                    layoutParams.height = (int) displayHeight;

                    setImageInfo(originalPath, mimeType, mGestureCropImageView, mOverlayView);

                    mGestureCropImageView.setTargetAspectRatio(originAspectRatioX / originAspectRatioY);
                    mGestureCropImageView.setImageToWrapCropBounds();
                }
            });
        }

//        PictureSelectionConfig.imageEngine.loadImage
//                (contentView.getContext(), path, photoView);
        (container).addView(contentView, 0);
        return contentView;
    }

    private void setImageInfo(String originalPath, String mimeType, GestureCropImageView mGestureCropImageView, OverlayView mOverlayView) {
        // Crop image view options
        mGestureCropImageView.setMinimumHeight(200);
        mGestureCropImageView.setMinimumWidth(200);
        mGestureCropImageView.setMaxScaleMultiplier(CropImageView.DEFAULT_MAX_SCALE_MULTIPLIER);
        mGestureCropImageView.setImageToWrapCropBoundsAnimDuration(CropImageView.DEFAULT_IMAGE_TO_CROP_BOUNDS_ANIM_DURATION);
        // Overlay view options
        mOverlayView.setDimmedBorderColor(mContext.getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setDimmedStrokeWidth(1);

        mOverlayView.setFreestyleCropMode(OverlayView.FREESTYLE_CROP_MODE_ENABLE);

        mOverlayView.setDragSmoothToCenter(true);//false--->true
        mOverlayView.setDragFrame(false);
        mOverlayView.setDimmedColor(mContext.getResources().getColor(R.color.black));

        mOverlayView.setShowCropFrame(true);
        mOverlayView.setCropFrameColor(mContext.getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setCropFrameStrokeWidth(mContext.getResources().getDimensionPixelSize(R.dimen.ucrop_default_crop_frame_stoke_width));

        mOverlayView.setShowCropGrid(false);

        boolean isHttp = PictureMimeType.isHasHttp(originalPath);
        String suffix = mimeType.replace("image/", ".");
        File file = new File(PictureFileUtils.getDiskCacheDir(mContext), DateUtils.getCreateFileName("IMG_CROP_") + suffix);
        Uri inputUri = isHttp || PictureMimeType.isContent(originalPath) ? Uri.parse(originalPath) : Uri.fromFile(new File(originalPath));

        Uri outputUri = Uri.fromFile(file);

        mGestureCropImageView.setForbit(false);
        mGestureCropImageView.setRotateEnabled(false);
        mGestureCropImageView.setScaleEnabled(false);
        if (inputUri != null && outputUri != null) {
            try {
                mGestureCropImageView.setImageUri(inputUri, outputUri);
            } catch (Exception e) {
            }
        }
    }

    /**
     * 预览加载进行加载
     *
     * @param curLocalMedia
     * @param uri
     * @param resultAspectRatio
     * @param offsetX
     * @param offsetY
     * @param imageWidth
     * @param imageHeight
     */
    protected void setResultUri(LocalMedia curLocalMedia, Uri uri, float resultAspectRatio, int offsetX, int offsetY, int imageWidth, int imageHeight) {
        if (uri != null) {
            String cutPath = uri.getPath();
            LocalMedia selectLocalMedia = null;
            boolean isExits = false;
            for (int i = 0; i < data.size(); i++) {
                LocalMedia item = data.get(i);
                if (TextUtils.equals(curLocalMedia.getPath(), item.getPath()) || curLocalMedia.getId() == item.getId()) {
                    isExits = true;
                    selectLocalMedia = item;
                    break;
                }
            }
            // 更新当前适配选中的LocalMedia裁剪参数
            curLocalMedia.setCut(!TextUtils.isEmpty(cutPath));
            curLocalMedia.setCutPath(cutPath);
            curLocalMedia.setCropOffsetX(offsetX);
            curLocalMedia.setCropOffsetY(offsetY);
            curLocalMedia.setCropResultAspectRatio(resultAspectRatio);
            curLocalMedia.setCropImageWidth(imageWidth);
            curLocalMedia.setCropImageHeight(imageHeight);
            curLocalMedia.setEditorImage(curLocalMedia.isCut());
            if (SdkVersionUtils.checkedAndroid_Q() && PictureMimeType.isContent(curLocalMedia.getPath())) {
                curLocalMedia.setAndroidQToPath(cutPath);
            }
            if (isExits) {
                // 更新当前选中列表的LocalMedia裁剪参数
                selectLocalMedia.setCut(!TextUtils.isEmpty(cutPath));
                selectLocalMedia.setCutPath(cutPath);

                selectLocalMedia.setCropOffsetX(offsetX);
                selectLocalMedia.setCropOffsetY(offsetY);
                selectLocalMedia.setCropResultAspectRatio(resultAspectRatio);
                selectLocalMedia.setCropImageWidth(imageWidth);
                selectLocalMedia.setCropImageHeight(imageHeight);
                selectLocalMedia.setEditorImage(curLocalMedia.isCut());
                if (SdkVersionUtils.checkedAndroid_Q() && PictureMimeType.isContent(curLocalMedia.getPath())) {
                    selectLocalMedia.setAndroidQToPath(cutPath);
                }
            }
        }
    }

    public interface OnChangeCropListener {
        void onChangeCrop(int pos);
    }
}
