package com.yanhua.media.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.gsyvideoplayer.utils.CommonUtil;
import com.yanhua.media.R2;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.media.R;
import com.yanhua.media.model.MusicInfoModel;

import butterknife.BindView;

public class MusicItemAdapter extends BaseRecyclerAdapter<MusicInfoModel, MusicItemAdapter.ViewHolder> {
    private Context mContext;

    private OnItemClickOperateListener listener;

    private boolean isFav;

    public interface OnItemClickOperateListener {

        void selectMusic(View view, int pos);
    }

    public MusicItemAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    public void setTabType(boolean isFav) {
        this.isFav = isFav;
    }

    public void setOnItemClickOperateListener(OnItemClickOperateListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bottom_music, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MusicInfoModel item) {
        if (item.getId().equals("0") && !this.isFav) {
            holder.ivCover.setImageResource(R.drawable.ic_white_search);
            holder.tvMusicTypeName.setText(item.getName());
            holder.flCover.setSelected(false);
        } else {
            holder.flCover.setSelected(item.isSelect());
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, item.getImageUrl(), R.drawable.icon_default_music, 6);
            holder.tvMusicTypeName.setText(item.getName());

            //llMusicInfo--->ivCover
            holder.ivCover.setOnClickListener(v -> {
                if (listener != null) {
                    listener.selectMusic(holder.ivCover, getPosition(holder));
                }
            });

            int time = item.getTime();
            if (time > 0) {
                holder.tvTime.setVisibility(View.VISIBLE);
                holder.tvTime.setText(CommonUtil.stringForTime(time * 1000));//后台数据是秒的
            } else {
                holder.tvTime.setVisibility(View.GONE);
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_cover)
        ImageButton ivCover;
        @BindView(R2.id.tv_music_type_name)
        TextView tvMusicTypeName;

        @BindView(R2.id.tv_time)
        TextView tvTime;

        @BindView(R2.id.llMusicInfo)
        LinearLayout llMusicInfo;

        @BindView(R2.id.fl_cover)
        FrameLayout flCover;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
