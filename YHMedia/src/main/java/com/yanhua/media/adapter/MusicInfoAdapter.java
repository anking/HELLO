package com.yanhua.media.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.tencent.qcloud.ugckit.module.effect.bgm.TCMusicInfo;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.media.R;
import com.yanhua.media.R2;

import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.utils.ColorBgManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import butterknife.BindView;

public class MusicInfoAdapter extends BaseRecyclerAdapter<MusicInfoModel, MusicInfoAdapter.ViewHolder> {

    private String[] colorArr = {"#3395FF", "#FDA94A", "#FE496A", "#6EE6B2"};
    private Integer[] resids = {
            R.drawable.shape_bg_music_tag_1,
            R.drawable.shape_bg_music_tag_2,
            R.drawable.shape_bg_music_tag_3,
            R.drawable.shape_bg_music_tag_4};
    private List<String> colorList;
    private List<Integer> resList;
    private Context mContext;

    public MusicInfoAdapter(Context context) {
        super(context);
        this.mContext = context;
        colorList = new ArrayList<>(Arrays.asList(colorArr));
        resList = new ArrayList<>(Arrays.asList(resids));
    }

    private OnItemClickOperateListener listener;
    private int selectPos;

    public interface OnItemClickOperateListener {
        default void followMusic(int pos) {
        }

        default void playMusic(View view, int pos) {
        }

        default void pauseMusic(View view, int pos) {
        }

        default void onClickUseBtn(TextView tv, int pos) {
        }
    }

    public void setOnItemClickOperateListener(OnItemClickOperateListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_music_info, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MusicInfoModel item) {
        if (TextUtils.isEmpty(item.getImageUrl())) {
            holder.civMusic.setImageResource(R.drawable.icon_default_music);
        } else {
            ImageLoaderUtil.loadImg(holder.civMusic, item.getImageUrl());
        }

        holder.tvMusicName.setText(item.getName());
        holder.tvMusicAuthor.setText(item.getAuthor());
        String tagNames = item.getTagNames();
        if (!TextUtils.isEmpty(tagNames)) {//标签设置
            holder.tflMusicTag.setVisibility(View.VISIBLE);
            String[] tags = tagNames.split(",");
            List<String> tagList = Arrays.asList(tags);
            holder.tflMusicTag.setAdapter(new TagAdapter<String>(tagList) {
                @Override
                public View getView(FlowLayout parent, int position, String s) {
                    String bgColor = ColorBgManager.getInstance().getColorByTag(s);
                    TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_bg_music_tag, parent, false);
                    if (TextUtils.isEmpty(bgColor)) {
                        if (colorList.isEmpty()) {
                            colorList.addAll(Arrays.asList(colorArr));
                        }
                        if (resList.isEmpty()) {
                            resList.addAll(Arrays.asList(resids));
                        }
                        int index = new Random().nextInt(colorList.size());
                        tv.setTextColor(Color.parseColor(colorList.get(index)));
                        tv.setBackgroundResource(resList.get(index));
                        ColorBgManager.getInstance().putColor(s, colorList.get(index));
                        // 从临时数组中去掉这个已经选择过的颜色
                        colorList.remove(colorList.get(index));
                        resList.remove(resList.get(index));
                    } else {
                        tv.setTextColor(Color.parseColor(bgColor));
                        int index = 0;
                        for (int i = 0; i < colorArr.length; i++) {
                            if (bgColor.equals(colorArr[i])) {
                                index = i;
                                break;
                            }
                        }
                        tv.setTextColor(Color.parseColor(colorArr[index]));
                        tv.setBackgroundResource(resids[index]);
                    }
                    tv.setText(s);
                    return tv;
                }
            });
        } else {
            holder.tflMusicTag.setVisibility(View.GONE);
        }

        if (item.status == TCMusicInfo.STATE_UNDOWNLOAD) {
            holder.tvOperate.setText("使用");//下载---使用

            holder.rcProgress.setVisibility(View.GONE);
            holder.ivPlay.setVisibility(View.VISIBLE);
        } else if (item.status == TCMusicInfo.STATE_DOWNLOADED) {
            holder.tvOperate.setText("使用");

            holder.rcProgress.clearAnimation();

            holder.rcProgress.setVisibility(View.GONE);
            holder.ivPlay.setVisibility(View.VISIBLE);
        } else if (item.status == TCMusicInfo.STATE_DOWNLOADING) {
            holder.tvOperate.setText("使用");//下载---使用

            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.progress_rote);
            holder.rcProgress.startAnimation(animation);
            holder.rcProgress.setVisibility(View.VISIBLE);
            holder.ivPlay.setVisibility(View.GONE);
        }

        if (item.isPlaying()) {
            holder.ivPlay.setImageResource(R.drawable.icon_music_pause);

            // 动画
            Animation animation = AnimationUtils.loadAnimation(holder.civMusic.getContext(), R.anim.music_rotate);
            animation.setRepeatCount(Animation.INFINITE);
//            animation.setRepeatMode(Animation.RESTART);
            animation.setInterpolator(new LinearInterpolator());
            holder.civMusic.startAnimation(animation);

            holder.tvOperate.setVisibility(item.status == TCMusicInfo.STATE_DOWNLOADED ? View.VISIBLE : View.GONE);
        } else {
            holder.ivPlay.setImageResource(R.drawable.icon_music_play);
            if (holder.civMusic.getAnimation() != null) {
                holder.civMusic.clearAnimation();
            }

            holder.tvOperate.setVisibility(View.GONE);
        }

        holder.ivCollect.setSelected(item.isFollow() == 1);
        //收藏、取消收藏
        holder.ivCollect.setOnClickListener(v -> {
            if (listener != null && FastClickUtil.isFastClick(FastClickUtil.MIN_DURATION_1SECOND_TIME)) {
                listener.followMusic(getPosition(holder));
            }
        });

        //播放操作
        holder.rlRootView.setOnClickListener(v -> {
            int headerCount = getHeaderCount();
            int selectPosTemp = getPosition(holder) - headerCount;

            if (selectPos == selectPosTemp) {
                playAction(holder, item, selectPosTemp);
            } else {
                if (FastClickUtil.isFastClick(100)) {
                    playAction(holder, item, selectPosTemp);
                }
            }
        });

        holder.tvOperate.setOnClickListener(v -> {
            if (listener != null && FastClickUtil.isFastClick(500)) {
                listener.onClickUseBtn(holder.tvOperate, getPosition(holder));
            }
        });
    }

    /**
     * @param holder
     * @param item
     */
    private void playAction(@NonNull ViewHolder holder, @NonNull MusicInfoModel item, int temp) {
        //此处需要控制，避免多个下载
        if (temp < 0) {
            return;
        }

        MusicInfoModel tempItem = mDataList.get(temp);
        if (tempItem.status == TCMusicInfo.STATE_DOWNLOADING) {
            //以前的那个正在下载
            ToastUtils.showShort("操作过于频繁，请稍等");
            return;
        }

        if (listener != null) {
            if (item.isPlaying()) {
                holder.ivPlay.setImageResource(R.drawable.icon_music_play);
                listener.pauseMusic(holder.civMusic, getPosition(holder));

                holder.tvOperate.setVisibility(item.status == TCMusicInfo.STATE_DOWNLOADED ? View.VISIBLE : View.GONE);

                if (holder.civMusic.getAnimation() != null) {
                    holder.civMusic.clearAnimation();
                }
            } else {
                holder.ivPlay.setImageResource(R.drawable.icon_music_pause);
                listener.playMusic(holder.civMusic, getPosition(holder));

                holder.tvOperate.setVisibility(View.GONE);

                Animation animation = AnimationUtils.loadAnimation(holder.civMusic.getContext(), R.anim.music_rotate);
                animation.setRepeatCount(Animation.INFINITE);
//                animation.setRepeatMode(Animation.RESTART);
                animation.setInterpolator(new LinearInterpolator());
                holder.civMusic.startAnimation(animation);
            }
        }
        //
        selectPos = temp;
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.civ_music)
        CircleImageView civMusic;
        @BindView(R2.id.tv_music_name)
        TextView tvMusicName;
        @BindView(R2.id.tv_music_author)
        TextView tvMusicAuthor;
        @BindView(R2.id.iv_collect)
        ImageView ivCollect;
        @BindView(R2.id.iv_play)
        ImageView ivPlay;
        @BindView(R2.id.tfl_music_tag)
        TagFlowLayout tflMusicTag;
        @BindView(R2.id.ll_music_info)
        LinearLayout llMusicInfo;
        @BindView(R2.id.ll_operate)
        LinearLayout llOperate;
        @BindView(R2.id.tv_operate)
        TextView tvOperate;

        @BindView(R2.id.rl_root_view)
        RelativeLayout rlRootView;

        @BindView(R2.id.rc_progress)
        ProgressBar rcProgress;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
