package com.yanhua.media.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.media.R2;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.media.R;
import com.yanhua.media.model.MusicTypeModel;

import butterknife.BindView;

public class MusicTypeAdapter extends BaseRecyclerAdapter<MusicTypeModel, MusicTypeAdapter.ViewHolder> {

    private Context mContext;
    private boolean isGrid = false;

    public MusicTypeAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    public MusicTypeAdapter(Context context, boolean isGrid) {
        super(context);
        this.mContext = context;
        this.isGrid = isGrid;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(isGrid ? R.layout.item_music_type_grid : R.layout.item_music_type, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MusicTypeModel item) {
        if (isGrid) {
            holder.ivCover.setLayoutParams(new LinearLayout.LayoutParams(
                    (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30)) / 3,
                    (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30)) / 3
                    ));
        }
        ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, item.getUrl(), R.drawable.icon_default_placeholder, 6);
        holder.tvMusicTypeName.setText(item.getName());
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.iv_cover)
        ImageView ivCover;
        @BindView(R2.id.tv_music_type_name)
        TextView tvMusicTypeName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
