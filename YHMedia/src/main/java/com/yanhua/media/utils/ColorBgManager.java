package com.yanhua.media.utils;

import java.util.Hashtable;

public class ColorBgManager {

    private static ColorBgManager instance;
    private Hashtable<String, String> tagBgTable = new Hashtable<>();

    private ColorBgManager() {
    }

    public static synchronized ColorBgManager getInstance() {
        if (instance == null) {
            instance = new ColorBgManager();
        }
        return instance;
    }

    public String getColorByTag(String tag) {
        if (tagBgTable.containsKey(tag)) {
            return tagBgTable.get(tag);
        }
        return null;
    }

    public void putColor(String tag, String color) {
        tagBgTable.put(tag, color);
    }

}
