package com.yanhua.media.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tencent.qcloud.ugckit.UGCKitImpl;
import com.tencent.qcloud.ugckit.module.effect.bgm.TCMusicInfo;
import com.yanhua.media.model.MusicInfoModel;

import java.util.List;

public class YXMusicManager {

    private SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(UGCKitImpl.getAppContext());
    private YXMusicManager.LoadMusicListener mLoadMusicListener;

    private static class YXMusicMgrHolder {
        @NonNull
        private static YXMusicManager instance = new YXMusicManager();
    }

    @NonNull
    public static YXMusicManager getInstance() {
        return YXMusicMgrHolder.instance;
    }

    /**
     * 获取本地已保存过的路径
     */
    public void getLocalPath(@Nullable List<MusicInfoModel> musicInfos) {
        if (musicInfos == null || musicInfos.size() == 0) {
            return;
        }

        for (MusicInfoModel musicInfoModel : musicInfos) {
            musicInfoModel.setLocalPath(mPrefs.getString(musicInfoModel.getName(), ""));
            if (!TextUtils.isEmpty(musicInfoModel.getLocalPath())) {
                musicInfoModel.status = TCMusicInfo.STATE_DOWNLOADED;
            }
        }
    }

    public void downloadMusicInfo(MusicInfoModel musicInfoModel) {
        YXMusicDownloadProgress yxMusicDownloadProgress = new YXMusicDownloadProgress(musicInfoModel.getName(), musicInfoModel.getMusicUrl());
        yxMusicDownloadProgress.start(new YXMusicDownloadProgress.Downloadlistener() {
            @Override
            public void onDownloadFail(String errorMsg) {
                LoadMusicListener loadMusicListener = null;
                synchronized (this) {
                    loadMusicListener = mLoadMusicListener;
                }
                if (loadMusicListener != null) {
                    loadMusicListener.onDownloadFail(musicInfoModel, errorMsg);
                }
//
//                if (mLoadMusicListener != null) {
//                    mLoadMusicListener.onDownloadFail(musicInfoModel, errorMsg);
//                }
            }

            @Override
            public void onDownloadProgress(int progress) {
                LoadMusicListener loadMusicListener = null;
                synchronized (this) {
                    loadMusicListener = mLoadMusicListener;
                }
                if (loadMusicListener != null) {
                    loadMusicListener.onDownloadProgress(musicInfoModel, progress);
                }

//                if (mLoadMusicListener != null) {
//                    mLoadMusicListener.onDownloadProgress(musicInfoModel, progress);
//                }
            }

            @Override
            public void onDownloadSuccess(String filePath) {
                LoadMusicListener loadMusicListener = null;
                synchronized (this) {
                    loadMusicListener = mLoadMusicListener;
                }
                if (loadMusicListener != null) {
                    loadMusicListener.onBgmDownloadSuccess(musicInfoModel, filePath);
                }
                // 本地保存，防止重复下载
                synchronized (this) {
                    mPrefs.edit().putString(musicInfoModel.getName(), filePath).apply();
                }

//                if (mLoadMusicListener != null) {
//                    mLoadMusicListener.onBgmDownloadSuccess(musicInfoModel, filePath);
//                    // 本地保存，防止重复下载
//                    synchronized (this) {
//                        mPrefs.edit().putString(musicInfoModel.getName(), filePath).apply();
//                    }
//                }
            }
        });
    }

    public void setOnLoadMusicListener(LoadMusicListener loadMusicListener) {
        synchronized (this) {
            mLoadMusicListener = loadMusicListener;
        }
    }

    public interface LoadMusicListener {

        void onBgmDownloadSuccess(MusicInfoModel musicInfoModel, String filePath);

        void onDownloadFail(MusicInfoModel musicInfoModel, String errorMsg);

        void onDownloadProgress(MusicInfoModel musicInfoModel, int progress);
    }

}
