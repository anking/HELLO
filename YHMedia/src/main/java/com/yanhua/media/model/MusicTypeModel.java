package com.yanhua.media.model;

import java.io.Serializable;

public class MusicTypeModel implements Serializable {

    private String id;            // 主键ID
    private String name;          // 名称
    private String url;           // 封面
    private int isEnable;         // 0: 启用, 1: 禁用
    private String remark;        // 备注

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(int isEnable) {
        this.isEnable = isEnable;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
