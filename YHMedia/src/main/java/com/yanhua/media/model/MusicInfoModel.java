package com.yanhua.media.model;

import java.io.Serializable;

public class MusicInfoModel implements Serializable {

    public static final int STATE_UNDOWNLOAD = 1;  // 未下载
    public static final int STATE_DOWNLOADING = 2; // 下载中
    public static final int STATE_DOWNLOADED = 3;  // 已下载

    private String id;
    private String area;                     // 地区
    private String author;                   // 作者
    private String imageUrl;                 // 封面
    private int isFollow;                    // 0: 未关注, 1: 已关注
    private String musicUrl;                 // 音乐文件路径
    private String localPath;                // 本地音乐文件路径   处理app业务逻辑使用
    private String name;                     // 名称
    private String tagIds;                   // 标签 ID (, 分隔)
    private String tagNames;                 // 标签 名称 (, 分隔)
    private String typeIds;                  // 类型 ID (, 分隔)
    private String typeNames;                // 类型 名称 (, 分隔)
    private boolean isPlaying = false;       // 处理业务逻辑使用 是否正在播放
    public int status = STATE_UNDOWNLOAD;    // 下载状态
    private boolean isSelect;
    private int time;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }


    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int isFollow() {
        return isFollow;
    }

    public void setFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(int isFollow) {
        this.isFollow = isFollow;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagIds() {
        return tagIds;
    }

    public void setTagIds(String tagIds) {
        this.tagIds = tagIds;
    }

    public String getTagNames() {
        return tagNames;
    }

    public void setTagNames(String tagNames) {
        this.tagNames = tagNames;
    }

    public String getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(String typeIds) {
        this.typeIds = typeIds;
    }

    public String getTypeNames() {
        return typeNames;
    }

    public void setTypeNames(String typeNames) {
        this.typeNames = typeNames;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
