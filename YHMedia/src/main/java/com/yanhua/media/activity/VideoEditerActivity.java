package com.yanhua.media.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitVideoEdit;
import com.tencent.qcloud.ugckit.UGCKitVideoRecord;
import com.tencent.qcloud.ugckit.basic.UGCKitResult;
import com.tencent.qcloud.ugckit.component.dialog.ProgressDialogUtil;
import com.tencent.qcloud.ugckit.module.editer.IVideoEditKit;
import com.tencent.qcloud.ugckit.module.editer.UGCKitEditConfig;
import com.tencent.qcloud.ugckit.module.effect.VideoEditerSDK;
import com.tencent.qcloud.ugckit.module.effect.utils.DraftEditer;
import com.tencent.qcloud.ugckit.module.record.MusicInfo;
import com.tencent.qcloud.ugckit.module.record.RecordMusicManager;
import com.tencent.qcloud.ugckit.module.record.RecordMusicPannel;
import com.tencent.qcloud.ugckit.module.record.VideoRecordSDK;
import com.tencent.qcloud.ugckit.module.record.interfaces.IRecordMusicPannel;
import com.tencent.qcloud.ugckit.utils.ToastUtil;
import com.tencent.ugc.TXUGCRecord;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.view.MusicBottomPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.yanhua.common.config.CommonConstant.FINISH_PAGE;

@Route(path = ARouterPath.VIDEO_EDIT)
public class VideoEditerActivity extends FragmentActivity implements View.OnClickListener {

    private static final String TAG = "VideoEditerActivity";
    /**
     * 视频路径
     */
    @Autowired(name = "videoPath")
    String mVideoPath;
    private UGCKitVideoEdit mUGCKitVideoEdit;
    // 背景音乐
    private TextView mTvBgm;
    // 动态滤镜
//    private TextView mTvMotion;
    // 时间特效
//    private TextView mTvSpeed;
    // 静态滤镜
    private TextView mTvFilter;
    // 贴纸
    private TextView mTvPaster;
    // 字幕
    private TextView mTvSubtitle;
    // 定位
    private TextView mTvLocation;
    // 打卡
//    private TextView mTvSign;

    private MusicBottomPopup musicBottomPopup;

    private ProgressDialogUtil mProgressDialogUtil;

    private Context mContext;
    //当前选中的音乐
    private MusicInfo musicInfo;

    private RelativeLayout rlPickMusic;//选取music
    private TextView tvMusic;
    private ImageButton ibtDeleteMusic;
    private Button ibtPublish;

    private RecordMusicPannel mRecordMusicPannel;

    private boolean firstOpen;


    private IVideoEditKit.OnEditListener mOnVideoEditListener = new IVideoEditKit.OnEditListener() {
        @Override
        public void onEditCompleted(UGCKitResult ugcKitResult) {
            if (ugcKitResult.errorCode == 0) {
                startPreviewActivity(ugcKitResult);
            } else {
                ToastUtil.toastShortMessage("edit video failed. error code:" + ugcKitResult.errorCode + ",desc msg:" + ugcKitResult.descMsg);
            }
        }

        @Override
        public void onEditCanceled() {
            finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowParam();

        mContext = this;

        ARouter.getInstance().inject(this);

        setContentView(R.layout.activity_video_editer);
        ActivityPageManager.getInstance().addActivity(this);
        firstOpen = true;

        musicInfo = DraftEditer.getInstance().loadMusicInfo();

        initData();

        mUGCKitVideoEdit = findViewById(R.id.video_edit);

        ibtPublish = findViewById(R.id.ibtPublish);

        mRecordMusicPannel = findViewById(R.id.record_music_pannel);
        // 设置"音乐面板"监听器
        mRecordMusicPannel.setOnMusicChangeListener(new IRecordMusicPannel.MusicChangeListener() {

            /************************************   音效Pannel回调接口 End    ********************************************/

            /************************************   音乐Pannel回调接口 Begin  ********************************************/
            @Override
            public void onMusicVolumChanged(float volume) {
                TXUGCRecord record = VideoRecordSDK.getInstance().getRecorder();
                if (record != null) {
                    record.setBGMVolume(volume);
                }
            }

            /**
             * 背景音乐裁剪
             *
             * @param startTime
             * @param endTime
             */
            @Override
            public void onMusicTimeChanged(long startTime, long endTime) {
                MusicInfo musicInfo = RecordMusicManager.getInstance().getMusicInfo();
                musicInfo.startTime = startTime;
                musicInfo.endTime = endTime;

                RecordMusicManager.getInstance().startPreviewMusic();
            }

            /**
             * 点击"音乐Pannel"的确定</p>
             * 1、关闭音乐Pannel</p>
             * 2、停止音乐试听
             */
            @Override
            public void onMusicSelect() {
                mRecordMusicPannel.setVisibility(GONE);
            }

            /**
             * 点击"音乐Pannel"的切换音乐
             */
            @Override
            public void onMusicReplace() {
            }

            @Override
            public void onMusicPanelClose() {
                mRecordMusicPannel.setVisibility(GONE);
            }

            /**
             * 点击"音乐Pannel"删除背景音乐
             */
            @Override
            public void onMusicDelete() {
                showDeleteMusicDialog();
            }
        });

        mTvBgm = (TextView) findViewById(R.id.tv_bgm);
//        mTvMotion = (TextView) findViewById(R.id.tv_motion);
//        mTvSpeed = (TextView) findViewById(R.id.tv_speed);
        mTvFilter = (TextView) findViewById(R.id.tv_filter);
        mTvPaster = (TextView) findViewById(R.id.tv_paster);
        mTvSubtitle = (TextView) findViewById(R.id.tv_subtitle);
        mTvLocation = findViewById(R.id.tv_location);
//        mTvSign = findViewById(R.id.tv_sign);

        mTvBgm.setOnClickListener(this);
//        mTvMotion.setOnClickListener(this);
//        mTvSpeed.setOnClickListener(this);
        mTvFilter.setOnClickListener(this);
        mTvPaster.setOnClickListener(this);
        mTvSubtitle.setOnClickListener(this);
        mTvLocation.setOnClickListener(this);
//        mTvSign.setOnClickListener(this);

        //选取音乐
        rlPickMusic = findViewById(R.id.rl_pick_music);
        rlPickMusic.setVisibility(GONE);
        ibtDeleteMusic = findViewById(R.id.ibt_delete);
        tvMusic = findViewById(R.id.tv_music);
        ibtDeleteMusic.setOnClickListener(this);

        rlPickMusic.setOnClickListener(this);

        ibtPublish.setOnClickListener(this);

        if (null != musicInfo && null != musicInfo.path && !"".equals(musicInfo.path)) {
            ibtDeleteMusic.setVisibility(VISIBLE);
            tvMusic.setText(musicInfo.name);
        }

        mProgressDialogUtil = new ProgressDialogUtil(mContext);

        mProgressDialogUtil.showProgressDialog();
        mUGCKitVideoEdit.post(() -> {
            //耗时操做，须要在子线程中完成操做后通知主线程实现UI更新
            UGCKitEditConfig config = new UGCKitEditConfig();
            config.isPublish = true;
            config.mTailWaterMarkConfig = null;
            mUGCKitVideoEdit.setConfig(config);
            if (!TextUtils.isEmpty(mVideoPath)) {
                mUGCKitVideoEdit.setVideoPath(mVideoPath);
            }
            // 初始化播放器
            mUGCKitVideoEdit.initPlayer();
        });

        EventBus.getDefault().register(this);
    }

    private void showDeleteMusicDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        AlertDialog alertDialog = builder.setTitle(getResources().getString(com.tencent.qcloud.ugckit.R.string.ugckit_tips)).setCancelable(false).setMessage(com.tencent.qcloud.ugckit.R.string.ugckit_delete_bgm_or_not)
                .setPositiveButton(com.tencent.qcloud.ugckit.R.string.ugckit_confirm_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        dialog.dismiss();

                        RecordMusicManager.getInstance().deleteMusic();
                        // 录制添加BGM后是录制不了人声的，而音效是针对人声有效的
                        mRecordMusicPannel.setVisibility(View.GONE);
                    }
                })
                .setNegativeButton(getResources().getString(com.tencent.qcloud.ugckit.R.string.ugckit_btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alertDialog.show();
    }

    private void initWindowParam() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initData() {
        if (TextUtils.isEmpty(mVideoPath)) {
            mVideoPath = getIntent().getStringExtra(UGCKitConstants.VIDEO_PATH);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mUGCKitVideoEdit.initPlayer();

        if (requestCode != UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE) {
            return;
        }
        if (data == null) {
            return;
        }

        //当前pick音乐
        musicInfo = new MusicInfo();

        musicInfo.path = data.getStringExtra(UGCKitConstants.MUSIC_PATH);
        musicInfo.name = data.getStringExtra(UGCKitConstants.MUSIC_NAME);
        musicInfo.position = data.getIntExtra(UGCKitConstants.MUSIC_POSITION, -1);

        if (null != musicInfo && null != musicInfo.path && !"".equals(musicInfo.path)) {
            ibtDeleteMusic.setVisibility(VISIBLE);
            tvMusic.setText(musicInfo.name);
            UGCKitVideoRecord.setRecordMusic(musicInfo);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (firstOpen) {
            // 显示圆形进度条
            mUGCKitVideoEdit.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mUGCKitVideoEdit.setOnVideoEditListener(mOnVideoEditListener);
                    mUGCKitVideoEdit.start();
                    mProgressDialogUtil.dismissProgressDialog();
                }
            }, 3000);//暂定3秒
        } else {
            mUGCKitVideoEdit.setOnVideoEditListener(mOnVideoEditListener);
            mUGCKitVideoEdit.start();
        }

        firstOpen = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUGCKitVideoEdit.stop();
        mUGCKitVideoEdit.setOnVideoEditListener(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                mUGCKitVideoEdit.stop();
                mUGCKitVideoEdit.setOnVideoEditListener(null);
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                mUGCKitVideoEdit.stop();
                mUGCKitVideoEdit.setOnVideoEditListener(null);
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUGCKitVideoEdit.release();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }

    private void startPreviewActivity(UGCKitResult ugcKitResult) {
        if (TextUtils.isEmpty(ugcKitResult.outputPath)) {
            return;
        }
        int duration = (int) VideoEditerSDK.getInstance().getVideoDuration();

        if (ugcKitResult.isPublish) {
            ARouter.getInstance().build(ARouterPath.PUBLISH_CONTENT_ACTIVITY)
                    .withInt("type", YXConfig.CONTENT_TYPE_VIDEO)
                    .withString("videoPath", ugcKitResult.outputPath)
                    .withString("coverPath", ugcKitResult.coverPath)
                    .withInt("videoTime", duration)
                    .withFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .navigation(VideoEditerActivity.this);
        }
        finish();

        EventBus.getDefault().post(new MessageEvent(FINISH_PAGE));
    }

    @Override
    public void onBackPressed() {
        mUGCKitVideoEdit.backPressed();
    }


    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {//开了定位服务
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // 没有权限
            } else {
                // 有权限了
            }
            startEffectActivity(UGCKitConstants.TYPE_EDITER_LOCATION);
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });

            commonDialog.setOnCancelListener(() -> {
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }

    @Override
    public void onClick(@NonNull View v) {
        int id = v.getId();
        if (id == R.id.tv_bgm) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_BGM);
        } /*else if (id == R.id.tv_motion) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_MOTION);
        } else if (id == R.id.tv_speed) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_SPEED);
        } */ else if (id == R.id.tv_filter) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_FILTER);
        } else if (id == R.id.tv_paster) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_PASTER);
        } else if (id == R.id.tv_subtitle) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_SUBTITLE);
        } else if (id == R.id.ibtPublish) {
            mUGCKitVideoEdit.showPublishDialog();
        } else if (id == R.id.tv_location) {
            checkLoacationStatus();//先检测是否开启定位
        } /*else if (id == R.id.tv_sign) {
            startEffectActivity(UGCKitConstants.TYPE_EDITER_SIGN);
        }*/ else if (v.getId() == R.id.ibt_delete) {
            tvMusic.setText("");
            musicInfo = null;
            ibtDeleteMusic.setVisibility(GONE);

            showDeleteMusicDialog();

            MediaPlayer mediaPlayer = musicBottomPopup.getMediaPlayer();
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
        } else if (v.getId() == R.id.rl_pick_music) {
            if (null == musicInfo) {
                ARouter.getInstance().build(ARouterPath.VIDEO_BGMUSIC_ACTIVITY).navigation(VideoEditerActivity.this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
            } else {
                if (TextUtils.isEmpty(musicInfo.path)) {
                    ARouter.getInstance().build(ARouterPath.VIDEO_BGMUSIC_ACTIVITY).navigation(VideoEditerActivity.this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
                    return;
                }

                //弹出底部弹出框，并且根据当前音乐类型请求
                if (null == musicBottomPopup) {
                    musicBottomPopup = new MusicBottomPopup(mContext);
                    musicBottomPopup.setOnMusicSelectListener(new MusicBottomPopup.OnMusicSelectListener() {
                        @Override
                        public void findMore() {
                            ARouter.getInstance().build(ARouterPath.VIDEO_BGMUSIC_ACTIVITY).navigation(VideoEditerActivity.this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
                        }

                        @Override
                        public void selectResult(MusicInfoModel item, int pos) {
                            musicInfo = new MusicInfo();

                            musicInfo.path = item.getLocalPath();
                            musicInfo.name = item.getName();
                            musicInfo.position = pos;

                            if (null != musicInfo && null != musicInfo.path && !"".equals(musicInfo.path)) {
                                ibtDeleteMusic.setVisibility(VISIBLE);
                                tvMusic.setText(musicInfo.name);
                                UGCKitVideoRecord.setRecordMusic(musicInfo);
                            }
                        }

                        @Override
                        public void editMusic(MusicInfoModel item, int pos) {
                            musicInfo = new MusicInfo();

                            musicInfo.path = item.getLocalPath();
                            musicInfo.name = item.getName();
                            musicInfo.position = pos;

                            if (null != musicInfo && null != musicInfo.path && !"".equals(musicInfo.path)) {
                                ibtDeleteMusic.setVisibility(VISIBLE);
                                tvMusic.setText(musicInfo.name);

                                musicBottomPopup.dismiss();
                                mRecordMusicPannel.setVisibility(VISIBLE);
                            }
                        }

                        @Override
                        public void onDismiss() {
                            mUGCKitVideoEdit.initPlayer();
                            onResume();
                        }
                    });
                }
                new XPopup.Builder(mContext).asCustom(musicBottomPopup).show();
            }
        }
    }

    /**
     * 跳转到视频特效编辑界面
     *
     * @param effectType {@link UGCKitConstants#TYPE_EDITER_BGM} 添加背景音乐</p>
     *                   {@link UGCKitConstants#TYPE_EDITER_MOTION} 添加动态滤镜</p>
     *                   {@link UGCKitConstants#TYPE_EDITER_SPEED} 添加时间特效</p>
     *                   {@link UGCKitConstants#TYPE_EDITER_FILTER} 添加静态滤镜</p>
     *                   {@link UGCKitConstants#TYPE_EDITER_PASTER} 添加贴纸</p>
     *                   {@link UGCKitConstants#TYPE_EDITER_SUBTITLE} 添加字幕</p>
     */
    private void startEffectActivity(int effectType) {
        Intent intent = new Intent(this, VideoEffectActivity.class);
        intent.putExtra(UGCKitConstants.KEY_FRAGMENT, effectType);
        startActivityForResult(intent, UGCKitConstants.ACTIVITY_OTHER_REQUEST_CODE);
    }

}