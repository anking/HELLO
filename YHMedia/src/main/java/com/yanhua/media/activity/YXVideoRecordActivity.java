package com.yanhua.media.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.model.LocalMediaPageLoader;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitVideoRecord;
import com.tencent.qcloud.ugckit.basic.UGCKitResult;
import com.tencent.qcloud.ugckit.component.swipemenu.MenuAdapter;
import com.tencent.qcloud.ugckit.component.swipemenu.SwipeMenuRecyclerView;
import com.tencent.qcloud.ugckit.component.swipemenu.touch.OnItemMoveListener;
import com.tencent.qcloud.ugckit.module.picker.data.ItemView;
import com.tencent.qcloud.ugckit.module.picker.data.TCVideoFileInfo;
import com.tencent.qcloud.ugckit.module.record.MusicInfo;
import com.tencent.qcloud.ugckit.module.record.RecordMusicManager;
import com.tencent.qcloud.ugckit.module.record.UGCKitRecordConfig;
import com.tencent.qcloud.ugckit.module.record.VideoRecordSDK;
import com.tencent.qcloud.ugckit.module.record.interfaces.IVideoRecordKit;
import com.tencent.ugc.TXUGCPartsManager;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.TabEntity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.core.widget.AutoMarqueeTextView;
import com.yanhua.media.R;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.view.MusicBottomPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.tencent.qcloud.ugckit.module.picker.data.TCVideoFileInfo.FILE_TYPE_PICTURE;
import static com.tencent.qcloud.ugckit.module.picker.data.TCVideoFileInfo.FILE_TYPE_VIDEO;
import static com.tencent.qcloud.ugckit.module.record.RecordModeView.RECORD_MODE_CLICK;
import static com.tencent.qcloud.ugckit.module.record.RecordModeView.RECORD_MODE_TAKE_PHOTO;
import static com.yanhua.core.util.FastClickUtil.MIN_DURATION_2SECOND_TIME;

public class YXVideoRecordActivity extends YXVideoBaseRecordActivity implements ActivityCompat.OnRequestPermissionsResultCallback, ItemView.OnDeleteListener, OnItemMoveListener {

    private TextView tvSwitchPhoto;
    private TextView tvSwitchVideo;
    private boolean isShowPhoto;

    private UGCKitVideoRecord mUGCKitVideoRecord;
    private Context mContext;
    private RelativeLayout rlRecord;
    private RelativeLayout rlPhoto;
    private CommonTabLayout ctlTab;
    private LinearLayout ll_video_tab;
    private ImageView ivLeft;
    private RelativeLayout rlPickMusic;//选取music
    private AutoMarqueeTextView tvMusic;
    private ImageButton ibtDeleteMusic;
    private FrameLayout bottom_toolbar;

    private MusicBottomPopup musicBottomPopup;

    private LinearLayout ll_ok_combine, ll_ok;
    private boolean isContinueAdd;

    private int curPos;
    //当前选中的音乐
    private MusicInfo musicInfo;
    private MusicInfoModel selectMusic;

    private MenuAdapter mMenuAdapter;
    private ArrayList<TCVideoFileInfo> mTCVideoFileInfoList = new ArrayList<>();
    private SwipeMenuRecyclerView mSwipeMenuRecyclerView;
    private boolean currentSwitchPhone;
    private boolean currentSwitchVedio;
    private int choseNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActivityPageManager.getInstance().addActivity(this);
        mContext = this;

        isContinueAdd = getIntent().getBooleanExtra("isContinueAdd", false);
        alreadyChooseSize = getIntent().getIntExtra("alreadyChooseSize", 0);

        initView();

        initData();

        EventBus.getDefault().register(this);
    }

    private void initView() {
        //选取音乐
        rlPickMusic = findViewById(R.id.rl_pick_music);
        ibtDeleteMusic = findViewById(R.id.ibt_delete);
        tvMusic = findViewById(R.id.tv_music);
        ibtDeleteMusic.setOnClickListener(this);

        mSwipeMenuRecyclerView = findViewById(R.id.rlv_selector);

        ll_ok_combine = findViewById(R.id.ll_ok_combine);
        ll_ok = findViewById(R.id.ll_ok);

        ll_ok_combine.setOnClickListener(this);
        ll_ok.setOnClickListener(this);

        tvSwitchPhoto = findViewById(R.id.tv_photo);
        tvSwitchVideo = findViewById(R.id.tv_video);

        tvSwitchPhoto.setOnClickListener(view -> swithAction(true, true));

        swithAction(true, false);

        if (isContinueAdd) {
            tvSwitchVideo.setEnabled(false);
            tvSwitchVideo.setClickable(false);
        } else {
            tvSwitchVideo.setOnClickListener(view -> swithAction(false, true));
        }

        rlRecord = findViewById(R.id.rl_record);
        rlPhoto = findViewById(R.id.rl_photo);

        mUGCKitVideoRecord = findViewById(R.id.video_record_layout);

        UGCKitRecordConfig ugcKitRecordConfig = UGCKitRecordConfig.getInstance();
        mUGCKitVideoRecord.setConfig(ugcKitRecordConfig);
        mUGCKitVideoRecord.getTitleBar().setOnBackClickListener(v -> onBackPressed());
        mUGCKitVideoRecord.setOnRecordListener(new IVideoRecordKit.OnRecordListener() {
            @Override
            public void onRecordCanceled() {
                finish();
            }

            @Override
            public void onRecordCompleted(UGCKitResult ugcKitResult) {
                if (ugcKitResult.errorCode == 0) {
                    //完成录像
                    startEditActivity();
                } else {
                    ToastUtils.showShort("record video failed. error code:" + ugcKitResult.errorCode + ", desc msg:" + ugcKitResult.descMsg);
                }
            }
        });

        rlPickMusic.setOnClickListener(this);

        //底部切换
        ll_video_tab = findViewById(R.id.ll_video_tab);
        ctlTab = findViewById(R.id.ctl_video_tab);

        if (isContinueAdd) {
            ll_video_tab.setVisibility(GONE);
            ctlTab.setVisibility(GONE);
        }

        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("相册", 0, 0));
        tabEntityList.add(new TabEntity("拍照片", 0, 0));
        tabEntityList.add(new TabEntity("拍视频", 0, 0));
        ctlTab.setTabData(tabEntityList);
        ctlTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                curPos = position;
                switch (position) {
                    case 0://相册
                        rlPhoto.setVisibility(VISIBLE);
                        rlRecord.setVisibility(GONE);
                        rlPickMusic.setVisibility(GONE);

                        if (null != musicBottomPopup && musicBottomPopup.isShow()) {
                            musicBottomPopup.dismiss();
                        }

                        //切换到相册和拍照如果当前正在播放音乐停止
                        mUGCKitVideoRecord.switchOther();
                        break;
                    case 1://拍照片
                    case 2://拍视频
                        mUGCKitVideoRecord.getBeautyPanel().setVisibility(View.GONE);
                        mUGCKitVideoRecord.getRecordMusicPannel().setVisibility(View.GONE);
                        mUGCKitVideoRecord.getSoundEffectPannel().setVisibility(View.GONE);

                        mUGCKitVideoRecord.getRecordBottomLayout().setVisibility(View.VISIBLE);
                        mUGCKitVideoRecord.getRecordRightLayout().setVisibility(View.VISIBLE);

                        rlPhoto.setVisibility(GONE);
                        rlRecord.setVisibility(VISIBLE);
                        if (position == 2) {//拍视频
                            mUGCKitVideoRecord.getRecordBottomLayout().getRecordButton().setCurrentRecordMode(RECORD_MODE_CLICK);

                            mUGCKitVideoRecord.getRecordProgressView().setVisibility(VISIBLE);
                            mUGCKitVideoRecord.getRecordBottomLayout().swithToVideoRecord();
                            mUGCKitVideoRecord.getRecordRightLayout().swithToVideoRecord();

                            mUGCKitVideoRecord.getRecordRightLayout().swithAspect(false);
                            rlPickMusic.setVisibility(VISIBLE);
                        } else {//拍照片
                            mUGCKitVideoRecord.getRecordBottomLayout().getRecordButton().setCurrentRecordMode(RECORD_MODE_TAKE_PHOTO);

                            mUGCKitVideoRecord.getRecordProgressView().setVisibility(GONE);
                            mUGCKitVideoRecord.getRecordBottomLayout().swithToTakePhoto();
                            mUGCKitVideoRecord.getRecordRightLayout().swithToTakePhoto();

                            mUGCKitVideoRecord.getRecordRightLayout().swithAspect(true);
                            rlPickMusic.setVisibility(GONE);

                            if (null != musicBottomPopup && musicBottomPopup.isShow()) {
                                musicBottomPopup.dismiss();
                            }

                            //切换到相册和拍照如果当前正在播放音乐停止
                            mUGCKitVideoRecord.switchOther();
                        }
                        break;
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        ctlTab.setCurrentTab(0);
        rlPhoto.setVisibility(VISIBLE);
        rlRecord.setVisibility(GONE);

        mUGCKitVideoRecord.setOnRecordActivityListener(new UGCKitVideoRecord.OnRecordActivityListener() {
            @Override
            public void onRecordStart() {
                rlPickMusic.setVisibility(GONE);
                ll_video_tab.setVisibility(View.GONE);
                mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(VISIBLE);
            }

            @Override
            public void onRecordPause() {
//                rlPickMusic.setVisibility(GONE);
                ll_video_tab.setVisibility(View.GONE);
                mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(VISIBLE);
            }

            @Override
            public void onReRecord() {
                rlPickMusic.setVisibility(VISIBLE);
                ll_video_tab.setVisibility(isContinueAdd || choseNum != 0 ? GONE : VISIBLE);

                mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(GONE);
            }
        });
    }

    private void initData() {
        mSwipeMenuRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mMenuAdapter = new MenuAdapter(mContext, mTCVideoFileInfoList);
        mMenuAdapter.setOnItemDeleteListener(this);

        mSwipeMenuRecyclerView.setAdapter(mMenuAdapter);
        mSwipeMenuRecyclerView.setLongPressDragEnabled(true);
        mSwipeMenuRecyclerView.setItemViewSwipeEnabled(false);
        mSwipeMenuRecyclerView.setOnItemMoveListener(this);

        mSwipeMenuRecyclerView.setOnItemClickListener(new SwipeMenuRecyclerView.OnItemClickListener() {
            @Override
            public void onItemClickListener(View view, int position) {
                YXVideoRecordActivity.this.onItemClick(position);
            }
        });
    }

    private void startEditActivity() {
        Intent intent = new Intent(this, VideoEditerActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUGCKitVideoRecord.screenOrientationChange();
    }

    /**
     * 切换
     */
    @Override
    public void swithAction(boolean showPhoto, boolean isSwitch) {
        currentSwitchPhone = tvSwitchPhoto.isSelected();
        currentSwitchVedio = tvSwitchVideo.isSelected();

        isShowPhoto = showPhoto;
        tvSwitchPhoto.setSelected(isShowPhoto);
        tvSwitchVideo.setSelected(!isShowPhoto);

        if (isShowPhoto) {
            LocalMediaPageLoader.getInstance(getContext()).setOnlySelectPicture(true);
            LocalMediaPageLoader.getInstance(getContext()).setOnlySelectVideo(false);
        } else {
            LocalMediaPageLoader.getInstance(getContext()).setOnlySelectPicture(false);
            LocalMediaPageLoader.getInstance(getContext()).setOnlySelectVideo(true);
        }

        if (isSwitch) {
            //此处还可优化，点击跟上次一样逻辑
            if (isShowPhoto && currentSwitchPhone || !isShowPhoto && currentSwitchVedio) {

            } else {
                mAdapter.clear();

                readLocalMedia();
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.ll_ok_combine) {
            if (FastClickUtil.isFastClick(MIN_DURATION_2SECOND_TIME)) {
                //根据选择的合成视频
                nextAction(false);
            }
        } else if (v.getId() == R.id.ll_ok) {
            nextAction(true);
        } else if (v.getId() == R.id.ibt_delete) {
            showDeleteMusicDialog();

            if (null != musicBottomPopup) {
                MediaPlayer mediaPlayer = musicBottomPopup.getMediaPlayer();
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                }
            }
        } else if (v.getId() == R.id.rl_pick_music) {
            mUGCKitVideoRecord.getBeautyPanel().setVisibility(View.GONE);
            mUGCKitVideoRecord.getRecordMusicPannel().setVisibility(View.GONE);
            mUGCKitVideoRecord.getSoundEffectPannel().setVisibility(View.GONE);
//
//            mUGCKitVideoRecord.getRecordBottomLayout().setVisibility(View.VISIBLE);
//            mUGCKitVideoRecord.getRecordRightLayout().setVisibility(View.VISIBLE);

            if (null == musicInfo) {
                ARouter.getInstance().build(ARouterPath.VIDEO_BGMUSIC_ACTIVITY).navigation(YXVideoRecordActivity.this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
            } else {
                //弹出底部弹出框，并且根据当前音乐类型请求
                if (null == musicBottomPopup) {
                    musicBottomPopup = new MusicBottomPopup(mContext, selectMusic);
                    musicBottomPopup.setOnMusicSelectListener(new MusicBottomPopup.OnMusicSelectListener() {
                        @Override
                        public void findMore() {
                            ARouter.getInstance().build(ARouterPath.VIDEO_BGMUSIC_ACTIVITY).navigation(YXVideoRecordActivity.this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
                        }

                        @Override
                        public void selectResult(MusicInfoModel item, int pos) {
                            selectMusic = item;

                            musicInfo = new MusicInfo();

                            musicInfo.path = item.getLocalPath();
                            musicInfo.name = item.getName();
                            musicInfo.position = pos;

                            rlPickMusic.setVisibility(VISIBLE);
                            ibtDeleteMusic.setVisibility(VISIBLE);
                            tvMusic.setText(musicInfo.name);

                            musicInfo.bgmVolume = 50;
                            mUGCKitVideoRecord.setRecordMusicInfo(musicInfo);
                        }

                        @Override
                        public void editMusic(MusicInfoModel item, int pos) {
                            musicInfo = new MusicInfo();

                            musicInfo.path = item.getLocalPath();
                            musicInfo.name = item.getName();
                            musicInfo.position = pos;

                            rlPickMusic.setVisibility(VISIBLE);
                            musicBottomPopup.dismiss();
                            musicInfo.bgmVolume = 50;
                            mUGCKitVideoRecord.setRecordMusicInfo(musicInfo);
                        }

                        @Override
                        public void onDismiss() {
                        }
                    });
                } else {
                    musicBottomPopup.setCurrentMusct(selectMusic);
                }
                new XPopup.Builder(mContext).asCustom(musicBottomPopup).show();
            }
        }
    }

    private void showDeleteMusicDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        AlertDialog alertDialog = builder.setTitle(getResources().getString(com.tencent.qcloud.ugckit.R.string.ugckit_tips)).setCancelable(false).setMessage(com.tencent.qcloud.ugckit.R.string.ugckit_delete_bgm_or_not)
                .setPositiveButton(com.tencent.qcloud.ugckit.R.string.ugckit_confirm_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        dialog.dismiss();

                        tvMusic.setText("选择音乐");
                        musicInfo = null;
                        selectMusic = null;

                        ibtDeleteMusic.setVisibility(GONE);
                        rlPickMusic.setVisibility(VISIBLE);

                        RecordMusicManager.getInstance().deleteMusic();
                        // 录制添加BGM后是录制不了人声的，而音效是针对人声有效的
                        mUGCKitVideoRecord.getRecordMusicPannel().setVisibility(View.GONE);

                        //下面代码处理删除选中音乐后按钮被隐藏的逻辑
                        mUGCKitVideoRecord.getRecordBottomLayout().setVisibility(View.VISIBLE);
                        mUGCKitVideoRecord.getRecordRightLayout().setVisibility(View.VISIBLE);
                        if (curPos == 2) {//拍视频
                            mUGCKitVideoRecord.getRecordProgressView().setVisibility(VISIBLE);
                            mUGCKitVideoRecord.getRecordBottomLayout().swithToVideoRecord();
                            mUGCKitVideoRecord.getRecordRightLayout().swithToVideoRecord();

                            mUGCKitVideoRecord.getRecordRightLayout().swithAspect(false);
                            rlPickMusic.setVisibility(VISIBLE);
                        } else if (curPos == 1) {//拍照片
                            mUGCKitVideoRecord.getRecordProgressView().setVisibility(GONE);
                            mUGCKitVideoRecord.getRecordBottomLayout().swithToTakePhoto();
                            mUGCKitVideoRecord.getRecordRightLayout().swithToTakePhoto();

                            mUGCKitVideoRecord.getRecordRightLayout().swithAspect(true);
                            rlPickMusic.setVisibility(GONE);

                            if (null != musicBottomPopup && musicBottomPopup.isShow()) {
                                musicBottomPopup.dismiss();
                            }

                            //切换到相册和拍照如果当前正在播放音乐停止
                            mUGCKitVideoRecord.switchOther();
                        }
                    }
                })
                .setNegativeButton(getResources().getString(com.tencent.qcloud.ugckit.R.string.ugckit_btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alertDialog.show();
    }

    /**
     * 发布还是怎么
     *
     * @param isPublish
     */
    private void nextAction(boolean isPublish) {
        ArrayList<TCVideoFileInfo> result = mMenuAdapter.getAll();

        ArrayList<String> pathList = new ArrayList<>();
        for (int i = 0; i < result.size(); i++) {
            TCVideoFileInfo info = result.get(i);
            pathList.add(info.getFilePath());
        }

        if (result != null && result.size() > 0) {
            String path = pathList.get(0);
            int videoTime = 0;
            // 取第一个元素判断是图片还是视频
            boolean isVideo = FileUtils.isVideo(path);

            if (isVideo) {
                // 视频
                // 异步获取视频第一帧生成封面图
                //----add by HRJ ----2021-12-15-------
                int size = result.size();
                if (size == 0) {
                    return;
                } else if (size == 1) {
                    //假如只有一个视频直接裁剪
                    TCVideoFileInfo fileInfo = (TCVideoFileInfo) result.get(0);
                    startVideoCutActivity(fileInfo, path);
                } else {
                    ////假如多个视频先拼接，在裁剪
                    startVideoJoinActivity(result);
                }
            } else {
                // 图片
                if (isPublish) {
                    if (null != pathList) {
                        startPicturePreviewActivity(0, null, pathList);
                    }
                } else {
                    //需要加工
                    ArrayList<TCVideoFileInfo> fileInfoList = mMenuAdapter.getAll();

                    if (null != fileInfoList) {
                        if (fileInfoList.size() < 3) {
                            ToastUtils.showShort("请至少选择3张图片");
                        } else {
                            startPictureJoinActivity(fileInfoList);
                        }
                    }
                }
            }
        }
    }

    /**
     * 跳转到图片合成视频界面，将多张图片添加转场动画后生成一个图频
     */
    private void startPictureJoinActivity(ArrayList<TCVideoFileInfo> fileInfoList) {
        ArrayList<String> picturePathList = new ArrayList<String>();
        for (TCVideoFileInfo info : fileInfoList) {
            if (Build.VERSION.SDK_INT >= 29) {
                picturePathList.add(info.getFileUri().toString());
            } else {
                picturePathList.add(info.getFilePath());
            }
        }
        Intent intent = new Intent(this, PictureJoinActivity.class);
        intent.putExtra(UGCKitConstants.INTENT_KEY_MULTI_PIC_LIST, picturePathList);
        startActivity(intent);
    }

    private void startPicturePreviewActivity(int type, ArrayList describeList, ArrayList pathList) {
        List<LocalMedia> result = mAdapter.getSelectedData();

        Intent intent = new Intent(this, YXPicturePreviewActivity.class);

        Bundle data = new Bundle();
        data.putInt("type", 0);
        data.putStringArrayList("describeList", null);
        data.putStringArrayList("pathList", pathList);
        data.putBoolean("isContinueAdd", isContinueAdd);
        data.putInt("curPos", 0);
        data.putParcelableArrayList("listLocalMedia", (ArrayList<? extends Parcelable>) result);

        intent.putExtras(data);
        startActivity(intent);
    }

    public ArrayList<TCVideoFileInfo> getSelectItems() {
        List<LocalMedia> result = mAdapter.getSelectedData();

        int count = result.size();

        ArrayList<TCVideoFileInfo> picturePathList = new ArrayList<TCVideoFileInfo>();

        for (int i = 0; i < count; i++) {
            TCVideoFileInfo fileInfo = new TCVideoFileInfo();
            LocalMedia item = result.get(i);

            fileInfo.setDuration(item.getDuration());
            fileInfo.setFileId(item.getId());
            fileInfo.setFileName(item.getFileName());
            fileInfo.setFilePath(item.getRealPath());
            fileInfo.setFileType(FILE_TYPE_PICTURE);
            fileInfo.setFileUri(Uri.parse(item.getUri()));

            File file = new File(fileInfo.getFilePath());
            if (!file.exists()) {
                ToastUtils.showShort(getResources().getString(com.tencent.qcloud.ugckit.R.string.ugckit_picture_choose_activity_the_selected_file_does_not_exist));
                return null;
            }
            picturePathList.add(fileInfo);
        }

        return picturePathList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.FINISH_PAGE:
                finish();
                break;
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(mContext)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {
                    });
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(mContext, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(mContext)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    PageJumpUtil.firstIsLoginThenJump(() -> {
                    });
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(mContext)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(mContext, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(mContext)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onChangeData(List<LocalMedia> selectData) {
        mMenuAdapter.removeAll();

        for (LocalMedia image : selectData) {
            TCVideoFileInfo fileInfo = new TCVideoFileInfo();

            fileInfo.setDuration(image.getDuration());
            fileInfo.setFileId(image.getId());
            fileInfo.setFileName(image.getFileName());
            fileInfo.setFilePath(image.getRealPath());

            String fileName = image.getFileName();
            String[] split = fileName.split("[.]");
            String str = "";
            if (split.length >= 2) {
                str = split[split.length - 1];
            }
            if (!str.equals("mp4") && !str.equals("MP4") && !str.equals("avi") && !str.equals("AVI")) {
                fileInfo.setFileType(FILE_TYPE_PICTURE);
            } else {
                fileInfo.setFileType(FILE_TYPE_VIDEO);
            }

            fileInfo.setFileUri(Uri.parse(image.getUri()));

            mMenuAdapter.addItem(fileInfo);
        }
    }

    @Override
    protected void changeImageNumber(List<LocalMedia> selectData, boolean isCheck, LocalMedia image) {
        super.changeImageNumber(selectData, isCheck, image);

        if (null != selectData && selectData.size() > 0) {
            choseNum = selectData.size();

            if (isCheck) {
                List<LocalMedia> result = selectData;
                int count = result.size();

                TCVideoFileInfo fileInfo = new TCVideoFileInfo();
                LocalMedia item = result.get(count - 1);

                fileInfo.setDuration(item.getDuration());
                fileInfo.setFileId(item.getId());
                fileInfo.setFileName(item.getFileName());
                fileInfo.setFilePath(item.getRealPath());

                String fileName = item.getFileName();
                String[] split = fileName.split("[.]");
                String str = "";
                if (split.length >= 2) {
                    str = split[split.length - 1];
                }
                if (!str.equals("mp4") && !str.equals("MP4") && !str.equals("avi") && !str.equals("AVI")) {
                    fileInfo.setFileType(FILE_TYPE_PICTURE);
                } else {
                    fileInfo.setFileType(FILE_TYPE_VIDEO);
                }

                fileInfo.setFileUri(Uri.parse(item.getUri()));

                if (null != mMenuAdapter) {

                    boolean isExist = false;
                    List<TCVideoFileInfo> list = mMenuAdapter.getAll();
                    for (int i = 0; i < list.size(); i++) {
                        TCVideoFileInfo info = list.get(i);
                        if (info.getFileId() == item.getId()) {
                            isExist = true;
                        }
                    }

                    if (!isExist) {
                        mMenuAdapter.addItem(fileInfo);
                    }
                }
            } else {
                TCVideoFileInfo fileInfo = new TCVideoFileInfo();

                fileInfo.setDuration(image.getDuration());
                fileInfo.setFileId(image.getId());
                fileInfo.setFileName(image.getFileName());
                fileInfo.setFilePath(image.getRealPath());

                String fileName = image.getFileName();
                String[] split = fileName.split("[.]");
                String str = "";
                if (split.length >= 2) {
                    str = split[split.length - 1];
                }
                if (!str.equals("mp4") && !str.equals("MP4") && !str.equals("avi") && !str.equals("AVI")) {
                    fileInfo.setFileType(FILE_TYPE_PICTURE);
                } else {
                    fileInfo.setFileType(FILE_TYPE_VIDEO);
                }

                fileInfo.setFileUri(Uri.parse(image.getUri()));

                if (null != mMenuAdapter) {
                    List<TCVideoFileInfo> list = mMenuAdapter.getAll();
                    for (int i = 0; i < list.size(); i++) {
                        TCVideoFileInfo info = list.get(i);
                        if (info.getFileId() == image.getId()) {
                            mMenuAdapter.removeIndex(i);
                            return;
                        }
                    }
                }
            }

            if (null != mMenuAdapter) {
                List<TCVideoFileInfo> list = mMenuAdapter.getAll();
                //---------------------屏蔽选中后直接预览编辑
                //----modified by HRJ date---2021-12-15
                if (null != list && list.size() > 0 && list.get(0).getFileType() == FILE_TYPE_VIDEO) {//不为空&&且为视频类型
                    ll_ok_combine.setVisibility(GONE);
                } else {
                    ll_ok_combine.setVisibility(VISIBLE);
                }
            }
            mBottomLayout.setVisibility(VISIBLE);

            if (null != ctlTab) {
                ll_video_tab.setVisibility(GONE);//有货的话隐藏底部切换栏
            }
        } else {
            if (null != ctlTab) {
                ll_video_tab.setVisibility(VISIBLE);
            }
            hideBottomLayout();
        }
    }

    private void hideBottomLayout() {
        mBottomLayout.setVisibility(GONE);

        if (null != mMenuAdapter) {
            mMenuAdapter.removeAll();
        }
    }

    @Override
    public void onItemClick(int position) {
        if (position < 0) {  // 异常情况处理，getAdapterPosition可能返回为 -1
            return;
        }
//        List<LocalMedia> result = mAdapter.getSelectedData();
//        result.get(position);
        onPreview(position);
    }

    @Override
    public void onDelete(int position) {
        if (position < 0) {  // 异常情况处理，getAdapterPosition可能返回为 -1
            return;
        }
        mMenuAdapter.removeIndex(position);
        mAdapter.getSelectedData().remove(position);

        if (mAdapter.getSelectedData().size() > 0) {
            mBottomLayout.setVisibility(VISIBLE);
        } else {
            hideBottomLayout();
        }

        mAdapter.notifyDataSetChanged();
        //删除选中
//        onChange(mAdapter.getSelectedData(), true, null);

        mAdapter.bindSelectData(mAdapter.getSelectedData());
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        ArrayList<TCVideoFileInfo> oriLsit = getSelectItems();
        Collections.swap(oriLsit, fromPosition, toPosition);
        Collections.swap(mTCVideoFileInfoList, fromPosition, toPosition);

        //移动后需要调换位置
        List<LocalMedia> localMediaList = mAdapter.getSelectedData();
        Collections.swap(localMediaList, fromPosition, toPosition);

        mMenuAdapter.notifyItemMoved(fromPosition, toPosition);

        return true;
    }

    @Override
    public void onItemDismiss(int position) {

    }

    @Override
    public void onBackPressed() {
        if (null != mUGCKitVideoRecord) {
            if (curPos == 2) {
//                TXUGCPartsManager manager = VideoRecordSDK.getInstance().getPartManager();
//                if (null != manager) {
//                    int duration = manager.getDuration();
//                    if (duration > 0) {
//                        mUGCKitVideoRecord.showGiveupRecordDialog();
//
//                        return;
//                    }
//                }

                mUGCKitVideoRecord.showGiveupRecordDialog();
                return;
            }
        }

        super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE) {
            return;
        }
        if (data == null) {
            return;
        }

        //当前pick音乐
        musicInfo = new MusicInfo();
        selectMusic = (MusicInfoModel) data.getSerializableExtra(UGCKitConstants.MUSIC_ITEM);

        musicInfo.path = data.getStringExtra(UGCKitConstants.MUSIC_PATH);
        musicInfo.name = data.getStringExtra(UGCKitConstants.MUSIC_NAME);
        musicInfo.position = data.getIntExtra(UGCKitConstants.MUSIC_POSITION, -1);

        ibtDeleteMusic.setVisibility(VISIBLE);
        tvMusic.setText(musicInfo.name);
        rlPickMusic.setVisibility(VISIBLE);
        musicInfo.bgmVolume = 50;
        mUGCKitVideoRecord.setRecordMusicInfo(musicInfo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null != mUGCKitVideoRecord) {
            mUGCKitVideoRecord.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (curPos == 0) {
            ll_video_tab.setVisibility(isContinueAdd || choseNum != 0 ? GONE : VISIBLE);

            mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(GONE);
        } else {
            mUGCKitVideoRecord.postDelayed(new Runnable() {
                @Override
                public void run() {
                    TXUGCPartsManager manager = VideoRecordSDK.getInstance().getPartManager();
                    if (null != manager) {
                        int size = manager.getDuration();
                        ll_video_tab.setVisibility(size > 0 ? GONE : (isContinueAdd || choseNum != 0 ? GONE : VISIBLE));
                        mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(size > 0 ? VISIBLE : GONE);
                    } else {
                        ll_video_tab.setVisibility(isContinueAdd || choseNum != 0 ? GONE : VISIBLE);
                        mUGCKitVideoRecord.getRecordBottomLayout().getViewBottomHeight().setVisibility(GONE);
                    }

                    //设置摄像头
                    mUGCKitVideoRecord.getRecordRightLayout().setCamera();
                }
            }, 300);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        RecordMusicManager.getInstance().resumeMusic();

        mUGCKitVideoRecord.getBeautyPanel().setVisibility(View.GONE);
        mUGCKitVideoRecord.getRecordMusicPannel().setVisibility(View.GONE);
        mUGCKitVideoRecord.getSoundEffectPannel().setVisibility(View.GONE);

        mUGCKitVideoRecord.getRecordBottomLayout().setVisibility(View.VISIBLE);
        mUGCKitVideoRecord.getRecordRightLayout().setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mUGCKitVideoRecord.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
        mUGCKitVideoRecord.release();
    }

    public void startVideoCutActivity(TCVideoFileInfo fileInfo, String path) {
        long durationTime = fileInfo.getDuration() / 1000;
        if (durationTime > YXConfig.VIDEO_CUT_MAX_TIME) {
            Intent intent = new Intent(this, VideoCutActivity.class);
            intent.putExtra(UGCKitConstants.VIDEO_PATH, fileInfo.getFilePath());
            startActivity(intent);
        } else {
            ARouter.getInstance().build(ARouterPath.VIDEO_EDIT)
                    .withString("videoPath", path)
                    .navigation();
        }
    }

    public void startVideoJoinActivity(ArrayList<TCVideoFileInfo> videoPathList) {
        VideoJoinerActivity.mTCVideoFileInfoList = null;

        Intent intent = new Intent(this, VideoJoinerActivity.class);
//        intent.putExtra(UGCKitConstants.INTENT_KEY_MULTI_CHOOSE, videoPathList);//这种方式传过去的数据有问题

        VideoJoinerActivity.mTCVideoFileInfoList = videoPathList;
        startActivity(intent);
    }
}
