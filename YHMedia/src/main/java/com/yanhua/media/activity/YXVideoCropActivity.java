package com.yanhua.media.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.VideoView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.luck.picture.lib.immersive.ImmersiveManage;
import com.luck.picture.lib.tools.ScreenUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.component.dialog.ProgressDialogUtil;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.view.CropImageView;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;


@SuppressWarnings("ConstantConditions")
public class YXVideoCropActivity extends AppCompatActivity {
    /**
     * 是否使用沉浸式，子类复写该方法来确定是否采用沉浸式
     *
     * @return 是否沉浸式，默认true
     */
    @Override
    public boolean isImmersive() {
        return true;
    }

    /**
     * 具体沉浸的样式，可以根据需要自行修改状态栏和导航栏的颜色
     */
    public void immersive() {
        ImmersiveManage.immersiveAboveAPI23(this
                , mStatusBarColor
                , mToolbarColor
                , isOpenWhiteStatusBar);
    }


    public static final int DEFAULT_COMPRESS_QUALITY = 90;
    public static final Bitmap.CompressFormat DEFAULT_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;

    public static final int NONE = 0;
    public static final int SCALE = 1;

    private static final String TAG = "YXVideoCropActivity";
    private static final long CONTROLS_ANIMATION_DURATION = 50;

    protected int mScreenWidth;
    // Enables dynamic coloring
    private int mToolbarColor;
    private int mStatusBarColor;
    private int mToolbarWidgetColor;
    @ColorInt
    private int mRootViewBackgroundColor;
    private int mLogoColor;

    @DrawableRes
    private int mToolbarCancelDrawable;

    @DrawableRes
    private int mToolbarCropDrawable;
    private boolean mShowLoader = true;
    protected RelativeLayout uCropPhotoBox;
    private UCropView mUCropView;
    private GestureCropImageView mGestureCropImageView;
    private OverlayView mOverlayView;
    private Transition mControlsTransition;

    private Bitmap.CompressFormat mCompressFormat = DEFAULT_COMPRESS_FORMAT;
    private int mCompressQuality = DEFAULT_COMPRESS_QUALITY;
    /**
     * 是否可拖动裁剪框
     */
    private boolean isDragFrame;
    /**
     * 是否改变状态栏字体颜色
     */
    private boolean isOpenWhiteStatusBar;

    private String mVideoPath;
    private VideoView videoView;
    private SeekBar skb;
    //    private ImageView ivHead;
    private int currentPos = 1000;
    MediaMetadataRetriever mmr;
    private int totalTime;//总时长
    private LinearLayout linearLayout;
    private FrameLayout layoutTop;
    int lastX;
    private ImageView ivThumb;

    private ProgressDialogUtil mProgressDialogUtil;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setNewRequestedOrientation(intent);
        getCustomOptionsData(intent);
        if (isImmersive()) {
            immersive();
        }
        setContentView(R.layout.activity_yxvideo_crop);
        ActivityPageManager.getInstance().addActivity(this);
        mScreenWidth = ScreenUtils.getScreenWidth(this);
        setupViews(intent);

        setViews(intent);
        //
        setImageData(intent);
        EventBus.getDefault().register(this);
    }

    //-------------------------------start---------------------------------------------------
    private void setViews(@NonNull Intent intent) {
        mVideoPath = intent.getStringExtra(UCrop.EXTRA_INPUT_VIDEO_PATH);
        currentPos = intent.getIntExtra("CURRENT_POS", 1000);//1000000
        lastX = intent.getIntExtra("CURRENT_POS_X", 0);

        linearLayout = findViewById(R.id.ll);
        skb = findViewById(R.id.skb);
        videoView = findViewById(R.id.vv);
        ivThumb = findViewById(R.id.ivThumb);
        layoutTop = findViewById(R.id.layout_top);

        initVideoPath(intent);//初始化MediaPlayer
    }

    private void initVideoPath(@NonNull Intent intent) {
        mmr = new MediaMetadataRetriever();
        mmr.setDataSource(mVideoPath);

        mProgressDialogUtil = new ProgressDialogUtil(this);
        mProgressDialogUtil.showProgressDialog();

        videoView.setOnPreparedListener(mp -> {
            totalTime = videoView.getDuration();//毫秒

            //避免滑动次数过多，将最大值设置100
//            skb.setMax(100);
//            int cp = currentPos * 100 / totalTime;
//            skb.setProgress(cp);
//
            skb.setMax(totalTime);
            skb.setProgress(currentPos);


            getVideoThumbnail(mVideoPath);

            videoView.postDelayed(() -> {
                mProgressDialogUtil.dismissProgressDialog();

                //加载完成后直接跳到上次的位置
                videoView.seekTo(currentPos);
                Bitmap bitmap = mmr.getFrameAtTime(currentPos * 1000);
                mGestureCropImageView.setImageBitmap(bitmap);

                ivThumb.setImageBitmap(bitmap);

                setFrameLayout(lastX);
            }, 1000);
        });

//        skb.setClickable(false);
        skb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int cpos, boolean fromUser) {
                if (fromUser) {//只处理用户手动操作的结果
                    currentPos = cpos;
//                    currentPos = cpos * totalTime / 100;
                    videoView.seekTo(currentPos);

//                    if (currentPos % 3 == 0) {
                    //获取第一帧图像的bitmap对象 单位是微秒
                    Bitmap bitmap = mmr.getFrameAtTime(currentPos * 1000);
                    mGestureCropImageView.setImageBitmap(bitmap);

                    ivThumb.setImageBitmap(bitmap);
//                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Bitmap bitmap = mmr.getFrameAtTime(currentPos * 1000);
                mGestureCropImageView.setImageBitmap(bitmap);

                ivThumb.setImageBitmap(bitmap);
            }
        });

        skb.setClickable(false);
        skb.setOnTouchListener((v, event) -> {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                    lastX = (int) event.getX();
//                    return interceptAction(x, y);
                case MotionEvent.ACTION_MOVE:
                    int dx = (int) event.getX();
                    setFrameLayout(dx);
                    lastX = dx;
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        });
        //设置路径
        videoView.setVideoPath(mVideoPath);
    }

    private void setFrameLayout(int dx) {
        int top = layoutTop.getTop();
        int left = dx;

        int screenWidth = linearLayout.getRight();

        if (left >= screenWidth - layoutTop.getWidth()) {
            left = screenWidth - layoutTop.getWidth();
        }

        if (left <= 0) {
            left = 0;
        }

        FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(layoutTop.getWidth(), layoutTop.getHeight());
        param.leftMargin = left;
        param.topMargin = top;
        layoutTop.setLayoutParams(param);

        layoutTop.postInvalidate();
    }

    public void getVideoThumbnail(String uri) {
        if (linearLayout.getChildCount() > 0) {
            return;
        }

        Bitmap bitmap = null;
        int timeS = totalTime / 11;
        for (int i = 1; i < 11; i++) {
            int pos = i * timeS * 1000;

            bitmap = mmr.getFrameAtTime(pos);
            addImgView(bitmap);
        }
    }

    public void addImgView(Bitmap bitmap) {
        //
        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((ScreenUtils.getScreenWidth(this) - ScreenUtils.dip2px(this, 24)) / 10, ViewGroup.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(lp);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setImageBitmap(bitmap);
        linearLayout.addView(imageView);
    }

    /**
     * setNewRequestedOrientation
     */
    protected void setNewRequestedOrientation(Intent intent) {
        int requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
        if (getRequestedOrientation() != requestedOrientation) {
            setRequestedOrientation(requestedOrientation);
        }
    }

    /**
     * 获取自定义配制参数
     *
     * @param intent
     */
    private void getCustomOptionsData(@NonNull Intent intent) {
        isOpenWhiteStatusBar = false;
        mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);
        if (mToolbarColor == 0) {
            mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);
        }
        if (mStatusBarColor == 0) {
            mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != videoView) {
            //让VideoView获取焦点
            videoView.requestFocus();

//            videoView.seekTo(currentPos);
//
////                    if (currentPos % 3 == 0) {
//            //获取第一帧图像的bitmap对象 单位是微秒
//            Bitmap bitmap = mmr.getFrameAtTime(currentPos * 1000);
//            mGestureCropImageView.setImageBitmap(bitmap);
//
//            ivThumb.setImageBitmap(bitmap);
        }
    }

//--------------------------------------------------------end--------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.ucrop_menu_activity, menu);

        // Change crop & loader menu icons color to match the rest of the UI colors

        MenuItem menuItemLoader = menu.findItem(R.id.menu_loader);
        Drawable menuItemLoaderIcon = menuItemLoader.getIcon();
        if (menuItemLoaderIcon != null) {
            try {
                menuItemLoaderIcon.mutate();
                ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(mToolbarWidgetColor, BlendModeCompat.SRC_ATOP);
                menuItemLoaderIcon.setColorFilter(colorFilter);
                menuItemLoader.setIcon(menuItemLoaderIcon);
            } catch (IllegalStateException e) {
                Log.i(TAG, String.format("%s - %s", e.getMessage(), "必須指定輸入以及輸出的 Uri"));
            }
            ((Animatable) menuItemLoader.getIcon()).start();
        }

        MenuItem menuItemCrop = menu.findItem(R.id.menu_crop);
        Drawable menuItemCropIcon = ContextCompat.getDrawable(this, mToolbarCropDrawable);
        if (menuItemCropIcon != null) {
            menuItemCropIcon.mutate();
            ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, R.color.white), BlendModeCompat.SRC_ATOP);
            menuItemCropIcon.setColorFilter(colorFilter);
            menuItemCrop.setIcon(menuItemCropIcon);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_crop).setVisible(!mShowLoader);
        menu.findItem(R.id.menu_loader).setVisible(mShowLoader);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_crop) {
            cropAndSaveImage();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGestureCropImageView != null) {
            mGestureCropImageView.cancelAllAnimations();
        }
    }

    /**
     * This method extracts all data from the incoming intent and setups views properly.
     */
    protected void setImageData(@NonNull Intent intent) {
        Uri inputUri = intent.getParcelableExtra(UCrop.EXTRA_INPUT_URI);
        Uri outputUri = intent.getParcelableExtra(UCrop.EXTRA_OUTPUT_URI);

        processOptions(intent);

        if (inputUri != null && outputUri != null) {
            try {
                mGestureCropImageView.setRotateEnabled(false);
                mGestureCropImageView.setScaleEnabled(true);
                mGestureCropImageView.setImageUri(inputUri, outputUri);
            } catch (Exception e) {
                setResultError(e);
                onBackPressed();
            }
        } else {
            setResultError(new NullPointerException("在你的 App 內复写颜色资源 (ucrop_color_toolbar_widget) 使 5.0 以前裝置正常运作"));
            onBackPressed();
        }
    }

    /**
     * and setups Activity, {@link OverlayView} and {@link CropImageView} properly.
     */
    @SuppressWarnings("deprecation")
    private void processOptions(@NonNull Intent intent) {
        mCompressQuality = YXVideoCropActivity.DEFAULT_COMPRESS_QUALITY;

        // custom options
        isDragFrame = false;

        // Crop image view options
//        mGestureCropImageView.setMinimumHeight(200);
//        mGestureCropImageView.setMinimumWidth(200);
        mGestureCropImageView.setMaxBitmapSize(intent.getIntExtra(UCrop.Options.EXTRA_MAX_BITMAP_SIZE, CropImageView.DEFAULT_MAX_BITMAP_SIZE));
        mGestureCropImageView.setMaxScaleMultiplier(CropImageView.DEFAULT_MAX_SCALE_MULTIPLIER);
        mGestureCropImageView.setImageToWrapCropBoundsAnimDuration(CropImageView.DEFAULT_IMAGE_TO_CROP_BOUNDS_ANIM_DURATION);
        mGestureCropImageView.setRotateEnabled(true);
        // Overlay view options
        int freeStyleCropMode = intent.getIntExtra(UCrop.Options.EXTRA_FREE_STYLE_CROP_MODE, -1);
        if (freeStyleCropMode == -1 || freeStyleCropMode > OverlayView.FREESTYLE_CROP_MODE_ENABLE_WITH_PASS_THROUGH) {
            mOverlayView.setFreestyleCropEnabled(intent.getBooleanExtra(UCrop.Options.EXTRA_FREE_STYLE_CROP, OverlayView.DEFAULT_FREESTYLE_CROP_MODE != OverlayView.FREESTYLE_CROP_MODE_DISABLE));
        } else {
            mOverlayView.setFreestyleCropMode(freeStyleCropMode);
        }

        mOverlayView.setFreestyleCropMode(OverlayView.FREESTYLE_CROP_MODE_ENABLE);


        mOverlayView.setDragSmoothToCenter(true);//false--->true
        mOverlayView.setDragFrame(isDragFrame);
        mOverlayView.setDimmedColor(getResources().getColor(R.color.picture_crop_frame));
        mOverlayView.setCircleDimmedLayer(intent.getBooleanExtra(UCrop.Options.EXTRA_CIRCLE_DIMMED_LAYER, OverlayView.DEFAULT_CIRCLE_DIMMED_LAYER));

        mOverlayView.setShowCropFrame(true);
        mOverlayView.setCropFrameColor(getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setCropFrameStrokeWidth(getResources().getDimensionPixelSize(R.dimen.ucrop_default_crop_frame_stoke_width));

        mOverlayView.setDimmedBorderColor(getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setDimmedStrokeWidth(1);


        mOverlayView.setShowCropGrid(false);
        mOverlayView.setCropGridRowCount(OverlayView.DEFAULT_CROP_GRID_ROW_COUNT);
        mOverlayView.setCropGridColumnCount(OverlayView.DEFAULT_CROP_GRID_COLUMN_COUNT);
        mOverlayView.setCropGridColor(getResources().getColor(R.color.ucrop_color_default_crop_grid));
        mOverlayView.setCropGridStrokeWidth(getResources().getDimensionPixelSize(R.dimen.ucrop_default_crop_grid_stoke_width));

        // Aspect ratio options
        float aspectRatioX = intent.getFloatExtra(UCrop.EXTRA_ASPECT_RATIO_X, 0);
        float aspectRatioY = intent.getFloatExtra(UCrop.EXTRA_ASPECT_RATIO_Y, 0);

        if (aspectRatioX > 0 && aspectRatioY > 0) {
            mGestureCropImageView.setTargetAspectRatio(aspectRatioX / aspectRatioY);
        } else {
            mGestureCropImageView.setTargetAspectRatio(0.75f);
        }

        // Result bitmap max size options
        int maxSizeX = intent.getIntExtra(UCrop.EXTRA_MAX_SIZE_X, 0);
        int maxSizeY = intent.getIntExtra(UCrop.EXTRA_MAX_SIZE_Y, 0);

        if (maxSizeX > 0 && maxSizeY > 0) {
            mGestureCropImageView.setMaxResultImageSizeX(maxSizeX);
            mGestureCropImageView.setMaxResultImageSizeY(maxSizeY);
        }
    }

    protected void setupViews(@NonNull Intent intent) {
        mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);

        mToolbarWidgetColor = ContextCompat.getColor(this, R.color.ucrop_color_toolbar_widget);
        mToolbarCancelDrawable = R.drawable.close_white;
        mToolbarCropDrawable = R.drawable.ic_white_ok;
        mRootViewBackgroundColor = ContextCompat.getColor(this, R.color.ucrop_color_crop_background);
        mLogoColor = intent.getIntExtra(UCrop.Options.EXTRA_UCROP_LOGO_COLOR, ContextCompat.getColor(this, R.color.ucrop_color_default_logo));

        setupAppBar();

        mControlsTransition = new AutoTransition();
        mControlsTransition.setDuration(CONTROLS_ANIMATION_DURATION);

        initiateRootViews();

        setupAspectRatioWidget();//裁剪比例
    }

    /**
     * Configures and styles both status bar and toolbar.
     */
    private void setupAppBar() {
        setStatusBarColor(mStatusBarColor);

        final Toolbar toolbar = findViewById(R.id.toolbar);

        // Set all of the Toolbar coloring
        toolbar.setBackgroundColor(mToolbarColor);
        toolbar.setTitleTextColor(mToolbarWidgetColor);

        // Color buttons inside the Toolbar
        Drawable stateButtonDrawable = AppCompatResources.getDrawable(this, mToolbarCancelDrawable).mutate();
        ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, R.color.white), BlendModeCompat.SRC_ATOP);
        stateButtonDrawable.setColorFilter(colorFilter);
        toolbar.setNavigationIcon(stateButtonDrawable);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private void initiateRootViews() {
        uCropPhotoBox = findViewById(R.id.ucrop_photobox);
        mUCropView = findViewById(R.id.ucrop);
        mGestureCropImageView = mUCropView.getCropImageView();
        mOverlayView = mUCropView.getOverlayView();

        mGestureCropImageView.setTransformImageListener(mImageListener);

        findViewById(R.id.ucrop_frame).setBackgroundColor(mRootViewBackgroundColor);

        ((ImageView) findViewById(R.id.image_view_logo)).setColorFilter(mLogoColor, PorterDuff.Mode.SRC_ATOP);
    }

    private TransformImageView.TransformImageListener mImageListener = new TransformImageView.TransformImageListener() {
        @Override
        public void onRotate(float currentAngle) {
            //旋转
        }

        @Override
        public void onScale(float currentScale) {
        }

        @Override
        public void onLoadComplete() {
            mUCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
            mShowLoader = false;
            supportInvalidateOptionsMenu();
        }

        @Override
        public void onLoadFailure(@NonNull Exception e) {
            setResultError(e);
            onBackPressed();
        }
    };

    /**
     * Sets status-bar color for L devices.
     *
     * @param color - status-bar color
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Window window = getWindow();
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        }
    }

    private void setupAspectRatioWidget() {
        Intent intent = getIntent();
        String path = intent.getStringExtra(UCrop.EXTRA_INPUT_VIDEO_COVER);

        Glide.with(this).asBitmap().load(path).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, com.bumptech.glide.request.transition.Transition<? super Bitmap> transition) {
                float width = bitmap.getWidth();
                float height = bitmap.getHeight();
                float originAspectRatioX = CropImageView.SOURCE_IMAGE_ASPECT_RATIO, originAspectRatioY = CropImageView.SOURCE_IMAGE_ASPECT_RATIO;
                float wh = 0;
                wh = width / height;//4:3 1:1 3:4
                if (wh < 0.9f) {
                    originAspectRatioX = 3;
                    originAspectRatioY = 4;
                } else if (wh >= 0.9 && wh <= 1.1f) {
                    originAspectRatioX = 1;
                    originAspectRatioY = 1;
                } else if (wh > 1.1) {
                    originAspectRatioX = 4;
                    originAspectRatioY = 3;
                }

                setCropAspectRatio(originAspectRatioX, originAspectRatioY);
            }
        });
    }

    private void setCropAspectRatio(float originAspectRatioX, float originAspectRatioY) {
        //默认原始比例-----根据宽度高度、比例--------
        mGestureCropImageView.setTargetAspectRatio(originAspectRatioX / originAspectRatioY);
        mGestureCropImageView.setImageToWrapCropBounds();
    }

    protected void cropAndSaveImage() {
        mShowLoader = true;
        supportInvalidateOptionsMenu();

        mGestureCropImageView.cropAndSaveImage(mCompressFormat, mCompressQuality, new BitmapCropCallback() {
            @Override
            public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {

                Glide.with(YXVideoCropActivity.this).asBitmap().load(resultUri).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull @NotNull Bitmap bitmap, @Nullable com.bumptech.glide.request.transition.Transition<? super Bitmap> transition) {
                        String newPath = System.currentTimeMillis() + ".jpg";

                        new AsyncTask<Void, Integer, String>() {//Params, Progress, Result
                            @Override
                            protected String doInBackground(Void... voids) {
                                String filePath = FileUtils.saveImageBitmap(newPath, bitmap, YXVideoCropActivity.this);

                                return filePath;
                            }

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                            }

                            @Override
                            protected void onPostExecute(String filePath) {
                                Uri uri = Uri.parse(filePath);

                                setResultUri(uri, mGestureCropImageView.getTargetAspectRatio(), offsetX, offsetY, imageWidth, imageHeight);
                                onBackPressed();
                            }
                        }.execute();
                    }
                });
            }

            @Override
            public void onCropFailure(@NonNull Throwable t) {
                setResultError(t);
                onBackPressed();
            }
        });
    }

    protected void setResultUri(Uri uri, float resultAspectRatio, int offsetX, int offsetY, int imageWidth, int imageHeight) {
        setResult(RESULT_OK, new Intent()
                .putExtra(UCrop.EXTRA_OUTPUT_URI, uri)
                .putExtra(UCrop.EXTRA_OUTPUT_CROP_ASPECT_RATIO, resultAspectRatio)
                .putExtra(UCrop.EXTRA_OUTPUT_IMAGE_WIDTH, imageWidth)
                .putExtra(UCrop.EXTRA_OUTPUT_IMAGE_HEIGHT, imageHeight)
                .putExtra(UCrop.EXTRA_OUTPUT_OFFSET_X, offsetX)
                .putExtra(UCrop.EXTRA_OUTPUT_OFFSET_Y, offsetY)
                .putExtra(UCrop.EXTRA_EDITOR_IMAGE, true)
                .putExtra("CURRENT_POS", currentPos)//当前的选取位置
                .putExtra("CURRENT_POS_X", lastX)

        );
    }

    protected void setResultError(Throwable throwable) {
        setResult(UCrop.RESULT_ERROR, new Intent().putExtra(UCrop.EXTRA_ERROR, throwable));
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * exit activity
     */
    protected void closeActivity() {
        finish();
        exitAnimation();
    }

    protected void exitAnimation() {
        overridePendingTransition(R.anim.ucrop_anim_fade_in, R.anim.ucrop_close);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                    //后台运行不弹
                    if (ActivityPageManager.isBackground(this)) {
                        UserManager.getInstance().setLogin(false);
                        UserManager.getInstance().setLoginToken("");

                        PageJumpUtil.firstIsLoginThenJump(() -> {});
                        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                        new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                        ToastUtils.showShort( "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                        return;
                    }
                    // 判断是不是栈顶的activity，如果不是就不弹框
                    if (this != ActivityPageManager.getInstance().currentActivity()) {
                        return;
                    }
                    // 被踢下线
                    AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                    new XPopup.Builder(this)
                            .isViewMode(true)
                            .dismissOnTouchOutside(false)
                            .dismissOnBackPressed(false)
                            .autoDismiss(false)
                            .setPopupCallback(new SimpleCallback())
                            .asCustom(alertPopup)
                            .show();
                    alertPopup.setOnConfirmListener(() -> {
                        alertPopup.dismiss();
                        UserManager.getInstance().setLogin(false);
                        UserManager.getInstance().setLoginToken("");

                        PageJumpUtil.firstIsLoginThenJump(() -> {});
                        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                        new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                    });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
        if (videoView != null) {
            videoView.suspend();
        }
    }
}
