package com.yanhua.media.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitVideoEffect;
import com.tencent.qcloud.ugckit.module.effect.IVideoEffectKit;
import com.tencent.qcloud.ugckit.module.effect.utils.EffectEditer;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class VideoEffectActivity extends FragmentActivity {

    private static final String TAG = "VideoEffectActivity";
    private int mFragmentType;
    private UGCKitVideoEffect mUGCKitVideoEffect;
    private IVideoEffectKit.OnVideoEffectListener mOnVideoEffectListener = new IVideoEffectKit.OnVideoEffectListener() {
        @Override
        public void onEffectApply() {
            int position = EffectEditer.getInstance().getBgmPos();
            String path = EffectEditer.getInstance().getBgmPath();
            String name = EffectEditer.getInstance().getBgmName();

            Intent intent = new Intent();
            intent.putExtra(UGCKitConstants.MUSIC_POSITION, position);
            intent.putExtra(UGCKitConstants.MUSIC_PATH, path);
            intent.putExtra(UGCKitConstants.MUSIC_NAME, name);
            setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, intent);
            finish();
        }

        @Override
        public void onEffectCancel() {
            finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowParam();
        // 必须在代码中设置主题(setTheme)或者在AndroidManifest中设置主题(android:theme)
//        setTheme(R.style.EditerActivityTheme);
        setContentView(R.layout.activity_video_effect);
        ActivityPageManager.getInstance().addActivity(this);
        mFragmentType = getIntent().getIntExtra(UGCKitConstants.KEY_FRAGMENT, 0);

        mUGCKitVideoEffect = findViewById(R.id.video_effect_layout);
        mUGCKitVideoEffect.setEffectType(mFragmentType);

        EventBus.getDefault().register(this);
    }

    private void initWindowParam() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUGCKitVideoEffect.setOnVideoEffectListener(mOnVideoEffectListener);
        mUGCKitVideoEffect.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUGCKitVideoEffect.stop();
        mUGCKitVideoEffect.setOnVideoEffectListener(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                mUGCKitVideoEffect.stop();
                mUGCKitVideoEffect.setOnVideoEffectListener(null);
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();

                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                mUGCKitVideoEffect.stop();
                mUGCKitVideoEffect.setOnVideoEffectListener(null);
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUGCKitVideoEffect.release();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //tag看UGCKitVideoEffect
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("bgm_setting_fragment");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        mUGCKitVideoEffect.backPressed();
    }

}