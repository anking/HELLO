package com.yanhua.media.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.yanhua.media.R2;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.media.R;
import com.yanhua.media.fragment.FavoriteMusicFragment;
import com.yanhua.media.fragment.RecomendMusicFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.VIDEO_BGMUSIC_ACTIVITY)
public class VideoBgMusicActivity extends BaseMvpActivity {

    @BindView(R2.id.ctl_top)
    CommonTabLayout ctlTop;

    private RecomendMusicFragment recomendMusicFragment;
    private FavoriteMusicFragment favoriteMusicFragment;

    @Override
    protected void creatPresent() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_video_bg_music;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("推荐", 0, 0));
        tabEntityList.add(new TabEntity("收藏", 0, 0));
        ArrayList<Fragment> fragments = new ArrayList<>();
        recomendMusicFragment = new RecomendMusicFragment();
        favoriteMusicFragment = new FavoriteMusicFragment();
        fragments.add(recomendMusicFragment);
        fragments.add(favoriteMusicFragment);
        ctlTop.setTabData(tabEntityList, this, R.id.fl_container, fragments);
        ctlTop.setCurrentTab(0);

        ctlTop.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 1) {
                    if (null != favoriteMusicFragment) {
                        //刚参考了新浪微博，点击切换的时候才去刷新
                        favoriteMusicFragment.getFollowMusicList(1);//获取第一页数据

                        if (null != recomendMusicFragment) {
                            recomendMusicFragment.pauseMediaPlayer();
                        }
                    }
                } else {
                    if (null != recomendMusicFragment) {
                        recomendMusicFragment.getMusicList(1);
                        if (null != favoriteMusicFragment) {
                            favoriteMusicFragment.pauseMediaPlayer();
                        }
                    }
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @OnClick({R2.id.ll_search, R2.id.et_search})
    public void goSearch() {
        ARouter.getInstance().build(ARouterPath.MUSIC_SEARCH_ACTIVITY).navigation(this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
    }

    @OnClick(R2.id.nav_left)
    public void goBack() {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE) {
            return;
        }
        if (data == null) {
            return;
        }
        setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, data);
        finish();
    }
}