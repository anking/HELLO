package com.yanhua.media.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.module.effect.bgm.TCMusicInfo;
import com.tencent.qcloud.ugckit.utils.BackgroundTasks;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.media.R;
import com.yanhua.media.R2;
import com.yanhua.media.adapter.MusicInfoAdapter;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.presenter.VideoBgMusicPresenter;
import com.yanhua.media.presenter.contract.VideoBgMusicContract;
import com.yanhua.media.utils.YXMusicManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.MUSIC_SEARCH_RESULT_ACTIVITY)
public class MusicSearchResultActivity extends BaseMvpActivity<VideoBgMusicPresenter> implements VideoBgMusicContract.IView {

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_music)
    RecyclerView rvMusic;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;

    @Autowired
    String keyWord;

    private int current = 1;
    private int size = 20;
    private int total;
    private MusicInfoAdapter mAdapter;
    private List<MusicInfoModel> musicInfoList;
    private HashMap<String, Object> params;
    private MusicInfoModel currentMusic;
    private int currentPos = 0;
    private MediaPlayer mediaPlayer;
    private YXMusicManager.LoadMusicListener mLoadMusicListener;

    @Override
    protected void creatPresent() {
        basePresenter = new VideoBgMusicPresenter();
    }

    @Override
    public int bindLayout() {
       return R.layout.activity_music_search_result;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        musicInfoList = new ArrayList<>();
        mediaPlayer = new MediaPlayer();
        params = new HashMap<>();
        etSearch.setText(keyWord);
        etSearch.setHint("搜索音乐、歌手、标签");
        etSearch.setHintTextColor(mContext.getResources().getColor(R.color.color_cccccc));
        rvMusic.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MusicInfoAdapter(this);
        rvMusic.setAdapter(mAdapter);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvMusic.setRecycledViewPool(pool);
        //关闭recyclerView动画
        rvMusic.getItemAnimator().setAddDuration(0);
        rvMusic.getItemAnimator().setChangeDuration(0);
        rvMusic.getItemAnimator().setMoveDuration(0);
        rvMusic.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvMusic.getItemAnimator()).setSupportsChangeAnimations(false);

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            currentPos = 0;
            current = 1;
            getMusicList();
            refreshLayout.finishRefresh(1000);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (musicInfoList.size() < total) {
                current++;
                getMusicList();
                refreshLayout.finishLoadMore(100);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                goSearch();
                return true;
            }
            return false;
        });

        mAdapter.setOnItemClickOperateListener(new MusicInfoAdapter.OnItemClickOperateListener() {
            @Override
            public void followMusic(int pos) {
//                currentPos = pos;
                currentMusic = musicInfoList.get(pos);
                basePresenter.followMusicOrNot(currentMusic.getId(),pos);
            }

            @Override
            public void playMusic(View view, int pos) {
                if (currentPos != pos) {
                    musicInfoList.get(currentPos).setPlaying(false);
                    mAdapter.notifyItemChanged(currentPos);
                }

                currentPos = pos;
                currentMusic = musicInfoList.get(pos);

                if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
                    currentMusic.setPlaying(!currentMusic.isPlaying());

                    // 播放音乐
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(currentMusic.getMusicUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();

                        // 设置播放的时候一直让屏幕变亮
                        mediaPlayer.setScreenOnWhilePlaying(true);
                        mediaPlayer.setOnCompletionListener(mp -> {
                            currentMusic.setPlaying(false);
                            mAdapter.notifyItemChanged(currentPos);//刷新一下状态
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
                    downloadMusic();
                }

                mAdapter.notifyItemChanged(currentPos);//刷新一下状态
            }

            @Override
            public void pauseMusic(View view, int pos) {
                currentPos = pos;
                currentMusic = musicInfoList.get(pos);
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    currentMusic.setPlaying(false);
                }
                if (view.getAnimation() != null) {
                    view.clearAnimation();
                }
            }

            @Override
            public void onClickUseBtn(TextView tv, int pos) {
                currentPos = pos;
                currentMusic = musicInfoList.get(pos);
//                if (currentMusic.status == TCMusicInfo.STATE_UNDOWNLOAD) {
//                    currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
//                    tv.setText("下载中");
//                    downloadMusic();
//                } else if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
//                    // 已下载
//                    backToEditActivity(pos, currentMusic.getLocalPath());
//                }
                if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
                    // 已下载
                    backToEditActivity(pos, currentMusic.getLocalPath());
                }
            }
        });

        mLoadMusicListener = new YXMusicManager.LoadMusicListener() {
            @Override
            public void onBgmDownloadSuccess(MusicInfoModel model, final String filePath) {
                BackgroundTasks.getInstance().postDelayed(() -> {
                    if (model == null) return;

                    model.setStatus(MusicInfoModel.STATE_DOWNLOADED);
                    model.setLocalPath(filePath);

                    // 播放音乐
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(model.getMusicUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        // 设置播放的时候一直让屏幕变亮
                        mediaPlayer.setScreenOnWhilePlaying(true);
                        mediaPlayer.setOnCompletionListener(mp -> {
                            model.setPlaying(false);
                            mAdapter.notifyDataSetChanged();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    model.setPlaying(true);


                    mAdapter.notifyDataSetChanged();
                },200);
            }

            @Override
            public void onDownloadFail(MusicInfoModel model, final String errorMsg) {
                BackgroundTasks.getInstance().runOnUiThread(() -> {
                    if (model == null) return;
                    model.setStatus(MusicInfoModel.STATE_UNDOWNLOAD);
                    mAdapter.notifyDataSetChanged();

                    ToastUtils.showShort("下载失败");
                });
            }

            @Override
            public void onDownloadProgress(MusicInfoModel model, final int progress) {
//                BackgroundTasks.getInstance().runOnUiThread(() -> {
                    if (model == null) return;
                    model.setStatus(MusicInfoModel.STATE_DOWNLOADING);
//                    mAdapter.notifyDataSetChanged();
//                });
            }
        };
    }

    private void downloadMusic() {
        YXMusicManager.getInstance().setOnLoadMusicListener(mLoadMusicListener);
        if (TextUtils.isEmpty(currentMusic.getLocalPath())) {
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
            return;
        }
        File file = new File(currentMusic.getLocalPath());
        if (!file.isFile()) {
            currentMusic.setLocalPath("");
            currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
        }
    }

    private void backToEditActivity(int position, String path) {
        Intent intent = new Intent();
        intent.putExtra(UGCKitConstants.MUSIC_POSITION, position);
        intent.putExtra(UGCKitConstants.MUSIC_PATH, path);
        intent.putExtra(UGCKitConstants.MUSIC_NAME, currentMusic.getName());
        intent.putExtra(UGCKitConstants.MUSIC_ITEM, currentMusic);
        setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, intent);
        finish();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        getMusicList();
    }

    private void getMusicList() {
        params.clear();
        params.put("current", current);
        params.put("size", size);
        params.put("keywordLike", keyWord);
        basePresenter.getMusicList(params);
    }

    @OnClick(R2.id.nav_left)
    public void goBack() {
        onBackPressed();
    }

    @OnClick(R2.id.tv_right)
    public void goSearch() {
        hideSoftKeyboard();
        keyWord = etSearch.getText().toString().trim();
        if (TextUtils.isEmpty(keyWord)) {

            ToastUtils.showShort("请输入要搜索的音乐、歌手、标签");
            return;
        }
        getMusicList();
    }

    @Override
    public void handleSuccessMusicList(ListResult<MusicInfoModel> result) {
        if (current == 1) {
            musicInfoList.clear();
        }
        if (result != null && result.getRecords() != null) {
            List<MusicInfoModel> records = result.getRecords();
            total = result.getTotal();
            if (records != null && !records.isEmpty()) {
                YXMusicManager.getInstance().getLocalPath(records);
                musicInfoList.addAll(records);
            }
        }
        if (musicInfoList.isEmpty()) {
            refreshLayout.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
//            emptyLayout.setErrorType(EmptyLayout.NODATA);
//            emptyLayout.setErrorMessage("无搜索结果");

            emptyLayout.setErrorMessage("没有找到相关搜索关键词","换个词搜搜看吧");
//            emptyLayout.setErrorImag(R.mipmap.empty_search);
        } else {
            refreshLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            mAdapter.setItems(musicInfoList);
        }
    }

    @Override
    public void handleSuccessFollowMusicOrNot(int pos) {
        if (currentMusic.isFollow() == 1) {

            ToastUtils.showShort("取消收藏");
            currentMusic.setFollow(0);
        } else {

            ToastUtils.showShort("收藏成功");
            currentMusic.setFollow(1);
        }
        mAdapter.notifyItemChanged(pos);
//        EventBus.getDefault().post(new MessageEvent(CommonConstant.REFRESH_MUSIC_COLLECTION));
    }

    @Override
    public void handleFailFollowMusicOrNot(int pos) {
        if (currentMusic.isFollow() == 1) {

            ToastUtils.showShort("取消收藏失败");
        } else {

            ToastUtils.showShort("收藏失败");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        pauseMediaPlayer();
    }

    public void pauseMediaPlayer(){
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

            currentMusic.setPlaying(false);

            mAdapter.notifyItemChanged(currentPos);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

}