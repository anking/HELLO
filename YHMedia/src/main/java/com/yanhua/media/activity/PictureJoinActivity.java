package com.yanhua.media.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitPictureJoin;
import com.tencent.qcloud.ugckit.basic.UGCKitResult;
import com.tencent.qcloud.ugckit.module.picturetransition.IPictureJoinKit;
import com.tencent.qcloud.ugckit.utils.ToastUtil;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class PictureJoinActivity extends FragmentActivity {

    private UGCKitPictureJoin mPictureTransition;
    private ArrayList<String> mPicPathList;
    private IPictureJoinKit.OnPictureJoinListener mOnPictureJoinListener = new IPictureJoinKit.OnPictureJoinListener() {

        @Override
        public void onPictureJoinCompleted(UGCKitResult ugcKitResult) {
            /**
             * 跳转到视频裁剪页面
             */
            if (ugcKitResult.errorCode == 0) {
                Intent intent = new Intent(PictureJoinActivity.this, VideoEditerActivity.class);
                intent.putExtra(UGCKitConstants.VIDEO_PATH, ugcKitResult.outputPath);
                startActivity(intent);
                finish();
            } else {
                ToastUtil.toastShortMessage("join picture failed. error code:" + ugcKitResult.errorCode + ",desc msg:" + ugcKitResult.descMsg);
            }
        }

        @Override
        public void onPictureJoinCanceled() {
            /**
             * 生成视频操作取消
             */
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowParam();
        // 必须在代码中设置主题(setTheme)或者在AndroidManifest中设置主题(android:theme)
//        setTheme(R.style.PictureTransitionActivityStyle);
        setContentView(R.layout.activity_picture_join);
        ActivityPageManager.getInstance().addActivity(this);
        mPictureTransition = (UGCKitPictureJoin) findViewById(R.id.picture_transition);
        /**
         *  获取从图片路径集合，并设置给TUIKit {@link UGCKitPictureJoin#setInputPictureList(ArrayList)}
         */
        mPicPathList = getIntent().getStringArrayListExtra(UGCKitConstants.INTENT_KEY_MULTI_PIC_LIST);
        mPictureTransition.setInputPictureList(mPicPathList);
        mPictureTransition.getTitleBar().setOnBackClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPictureTransition.stopPlay();
                finish();
            }
        });

        EventBus.getDefault().register(this);
    }

    private void initWindowParam() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        mPictureTransition.stopPlay();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPictureTransition.setOnPictureJoinListener(mOnPictureJoinListener);
//        mPictureTransition.resumePlay();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPictureTransition.post(new Runnable() {
            @Override
            public void run() {
                //耗时操做，须要在子线程中完成操做后通知主线程实现UI更新
                mPictureTransition.pausePlay();
                mPictureTransition.setOnPictureJoinListener(null);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                mPictureTransition.post(new Runnable() {
                    @Override
                    public void run() {
                        //耗时操做，须要在子线程中完成操做后通知主线程实现UI更新
                        mPictureTransition.pausePlay();
                        mPictureTransition.setOnPictureJoinListener(null);
                    }
                });
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});

                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                mPictureTransition.post(new Runnable() {
                    @Override
                    public void run() {
                        //耗时操做，须要在子线程中完成操做后通知主线程实现UI更新
                        mPictureTransition.pausePlay();
                        mPictureTransition.setOnPictureJoinListener(null);
                    }
                });
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }
}