package com.yanhua.media.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;

import com.yanhua.media.R;
import com.yanhua.media.R2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.MUSIC_SEARCH_ACTIVITY)
public class MusicSearchActivity extends BaseMvpActivity {

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.ll_search)
    LinearLayout llSearch;
    @BindView(R2.id.tv_right)
    TextView tvRight;
    @BindView(R2.id.ll_toolbar)
    LinearLayout llToolbar;
    @BindView(R2.id.ib_delete)
    ImageButton ibDelete;
    @BindView(R2.id.tfl_recent)
    TagFlowLayout tflRecent;
    @BindView(R2.id.ll_recent)
    LinearLayout llRecent;
    private String keyWord;
    private List<String> recentList;

    @Override
    protected void creatPresent() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_music_search;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        etSearch.setHint("搜索音乐、歌手、标签");
        etSearch.setHintTextColor(mContext.getResources().getColor(R.color.color_cccccc));

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                toSearch();
                return true;
            }
            return false;
        });

        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
        getWindow().getDecorView().postDelayed(() -> {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                etSearch.requestFocus();
                inputManager.showSoftInput(etSearch, 0);
            }
        }, 100);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        recentList = new ArrayList<>();
        tflRecent.setAdapter(new TagAdapter<String>(recentList) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(mContext).inflate(R.layout.item_music_search_tag, parent, false);
                TextView tvTag = frameLayout.findViewById(R.id.tv_tag);
                ImageView ivDelete = frameLayout.findViewById(R.id.iv_delete);
                tvTag.setText(s);
                ivDelete.setOnClickListener(v -> {
                    String searchMusicHistory = DiscoCacheUtils.getInstance().getSearchMusicHistory();
                    if (searchMusicHistory.contains(s)) {
                        int from = searchMusicHistory.lastIndexOf(s);
                        String newStr = searchMusicHistory.substring(0, from) + searchMusicHistory.substring(from + s.length() + 1, searchMusicHistory.length());

                        DiscoCacheUtils.getInstance().setSearchMusicHistory(newStr);
                        recentList.remove(s);
                        tflRecent.getAdapter().refreshData(recentList);
                    }
                });
                return frameLayout;
            }
        });
        tflRecent.setOnTagClickListener((view, position, parent) -> {
            keyWord = recentList.get(position);
            toSearch();
            return true;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        recentList.clear();
        String searchMusicHistory = DiscoCacheUtils.getInstance().getSearchMusicHistory();
        String[] arr = searchMusicHistory.split(",");
        for (String str : arr) {
            if (!TextUtils.isEmpty(str)) {
                recentList.add(str);
            }
        }
        if (recentList.isEmpty()) {
            llRecent.setVisibility(View.GONE);
        } else {
            llRecent.setVisibility(View.VISIBLE);
        }
        tflRecent.getAdapter().refreshData(recentList);
    }

    @OnClick({
            R2.id.nav_left,
            R2.id.tv_right,
            R2.id.ib_delete
    })
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.nav_left) {
            onBackPressed();
        } else if (id == R.id.tv_right) {
            toSearch();
        } else if (id == R.id.ib_delete) {
            CommonDialog commonDialog = new CommonDialog(mContext, "是否清除搜索历史记录？");
            new XPopup.Builder(mContext).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                DiscoCacheUtils.getInstance().setSearchMusicHistory("");
                recentList.clear();

                if (recentList.isEmpty()) {
                    llRecent.setVisibility(View.GONE);
                } else {
                    llRecent.setVisibility(View.VISIBLE);
                }

                tflRecent.getAdapter().refreshData(recentList);
            });
            commonDialog.setOnCancelListener(commonDialog::dismiss);
        }
    }

    private void toSearch() {
        if (TextUtils.isEmpty(keyWord)) {
            ToastUtils.showShort("请输入要搜索的内容");
            return;
        }
        String searchMusicHistory = DiscoCacheUtils.getInstance().getSearchMusicHistory();
        String[] arr = searchMusicHistory.split(",");
        if (!Arrays.asList(arr).contains(keyWord)) {
            if (arr.length >= 10) {
                searchMusicHistory = searchMusicHistory.substring(0, searchMusicHistory.length() - 1);
                searchMusicHistory = searchMusicHistory.substring(0, searchMusicHistory.lastIndexOf(","));
            }
            searchMusicHistory = keyWord + "," + searchMusicHistory;
            DiscoCacheUtils.getInstance().setSearchMusicHistory(searchMusicHistory);
        }
        ARouter.getInstance()
                .build(ARouterPath.MUSIC_SEARCH_RESULT_ACTIVITY)
                .withString("keyWord", keyWord)
                .navigation(this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE) {
            return;
        }
        if (data == null) {
            return;
        }
        setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, data);
        finish();
    }

}