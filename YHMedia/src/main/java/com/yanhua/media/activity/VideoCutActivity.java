package com.yanhua.media.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitVideoCut;
import com.tencent.qcloud.ugckit.basic.UGCKitResult;
import com.tencent.qcloud.ugckit.module.cut.IVideoCutKit;
import com.tencent.qcloud.ugckit.utils.ToastUtil;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class VideoCutActivity extends FragmentActivity {
    private String TAG = "TCVideoCutActivity";
    private UGCKitVideoCut mUGCKitVideoCut;
    private String mInVideoPath;
    private IVideoCutKit.OnCutListener mOnCutListener = new IVideoCutKit.OnCutListener() {
        /**
         * 视频裁剪进度条执行完成后调用
         */
        @Override
        public void onCutterCompleted(UGCKitResult ugcKitResult) {
            Log.i(TAG, "onCutterCompleted");
            if (ugcKitResult.errorCode == 0) {
                startEditActivity();
            } else {
                ToastUtil.toastShortMessage("cut video failed. error code:" + ugcKitResult.errorCode + ",desc msg:" + ugcKitResult.descMsg);
            }
        }

        /**
         * 点击视频裁剪进度叉号，取消裁剪时被调用
         */
        @Override
        public void onCutterCanceled() {
            Log.i(TAG, "onCutterCanceled");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindowParam();
        // 必须在代码中设置主题(setTheme)或者在AndroidManifest中设置主题(android:theme)
        setTheme(R.style.EditerActivityTheme);
        setContentView(R.layout.activity_video_cut);
        ActivityPageManager.getInstance().addActivity(this);
        mUGCKitVideoCut = (UGCKitVideoCut) findViewById(R.id.video_cutter_layout);
        mInVideoPath = getIntent().getStringExtra(UGCKitConstants.VIDEO_PATH);
        mUGCKitVideoCut.setVideoPath(mInVideoPath);
        mUGCKitVideoCut.getTitleBar().setOnBackClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        EventBus.getDefault().register(this);
    }

    private void initWindowParam() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUGCKitVideoCut.setOnCutListener(mOnCutListener);
        mUGCKitVideoCut.startPlay();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUGCKitVideoCut.stopPlay();
        mUGCKitVideoCut.setOnCutListener(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                mUGCKitVideoCut.stopPlay();
                mUGCKitVideoCut.setOnCutListener(null);

                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");

                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();

                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                mUGCKitVideoCut.stopPlay();
                mUGCKitVideoCut.setOnCutListener(null);

                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");

                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUGCKitVideoCut.release();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void startEditActivity() {
        Intent intent = new Intent(this, VideoEditerActivity.class);
        startActivity(intent);
        finish();
    }

}