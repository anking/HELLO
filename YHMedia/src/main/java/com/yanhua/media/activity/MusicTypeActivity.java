package com.yanhua.media.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.core.view.GridSpaceItemDecoration;
import com.yanhua.media.R;
import com.yanhua.media.R2;
import com.yanhua.media.adapter.MusicTypeAdapter;
import com.yanhua.media.model.MusicTypeModel;
import com.yanhua.media.presenter.VideoBgMusicPresenter;
import com.yanhua.media.presenter.contract.VideoBgMusicContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.MUSIC_TYPE_ACTIVITY)
public class MusicTypeActivity extends BaseMvpActivity<VideoBgMusicPresenter> implements VideoBgMusicContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_music_type)
    RecyclerView rvMusicType;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;

    private int current = 1;
    private int size = 20;
    private int total;
    private HashMap<String, Object> params;
    private MusicTypeAdapter mAdapter;
    private List<MusicTypeModel> musicTypeList;

    @OnClick(R2.id.iv_left)
    public void goBack() {
        onBackPressed();
    }

    @Override
    protected void creatPresent() {
        basePresenter = new VideoBgMusicPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_music_type;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        tvHeader.setText("歌单分类");
        musicTypeList = new ArrayList<>();
        rvMusicType.setLayoutManager(new GridLayoutManager(mContext, 3));
        rvMusicType.addItemDecoration(new GridSpaceItemDecoration(3, DisplayUtils.dip2px(mContext, 10), 0));
        mAdapter = new MusicTypeAdapter(mContext, true);
        rvMusicType.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            MusicTypeModel musicType = musicTypeList.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.MUSIC_LIST_TYPE_ACTIVITY)
                    .withSerializable("musicType", musicType)
                    .navigation(this, UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE);
        });
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getMusicTypeList();
            refreshLayout.finishRefresh(1000);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (musicTypeList.size() < total) {
                current++;
                getMusicTypeList();
                refreshLayout.finishLoadMore(1000);
            } else {
                refreshLayout.finishLoadMore(1000, true, true);
            }
        });
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        params = new HashMap<>();
        getMusicTypeList();
    }

    private void getMusicTypeList() {
        params.clear();
        params.put("current", current);
        params.put("size", size);
        basePresenter.getMusicTypeList(params);
    }

    @Override
    public void handleSuccessMusicTypeList(ListResult<MusicTypeModel> result) {
        if (current == 1) {
            musicTypeList.clear();
        }
        if (result != null) {
            total = result.getTotal();
            List<MusicTypeModel> records = result.getRecords();
            if (records != null && !records.isEmpty()) {
                musicTypeList.addAll(records);
            }
        }
        if (musicTypeList.isEmpty()) {
            emptyLayout.setVisibility(View.VISIBLE);
            refreshLayout.setVisibility(View.GONE);
            emptyLayout.setErrorMessage("暂无分类");
            emptyLayout.setErrorType(EmptyLayout.NODATA);
        } else {
            emptyLayout.setVisibility(View.GONE);
            refreshLayout.setVisibility(View.VISIBLE);
            mAdapter.setItems(musicTypeList);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE) {
            return;
        }
        if (data == null) {
            return;
        }
        setResult(UGCKitConstants.ACTIVITY_MUSIC_REQUEST_CODE, data);
        finish();
    }

}