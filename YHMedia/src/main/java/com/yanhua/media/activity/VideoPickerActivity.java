package com.yanhua.media.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.tencent.qcloud.ugckit.UGCKitConstants;
import com.tencent.qcloud.ugckit.UGCKitVideoPicker;
import com.tencent.qcloud.ugckit.module.picker.data.TCVideoFileInfo;
import com.tencent.qcloud.ugckit.module.picker.view.IPickerLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

@Route(path = ARouterPath.VIDEO_PICKER)
public class VideoPickerActivity  extends Activity {

    private UGCKitVideoPicker mUGCKitVideoPicker;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        initWindowParam();
        // 必须在代码中设置主题(setTheme)或者在AndroidManifest中设置主题(android:theme)
//        setTheme(R.style.PickerActivityTheme);
        setContentView(R.layout.activity_video_picker);
        ActivityPageManager.getInstance().addActivity(this);
        mUGCKitVideoPicker = findViewById(R.id.video_choose_layout);
        mUGCKitVideoPicker.getTitleBar().setOnBackClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mUGCKitVideoPicker.setOnPickerListener(new IPickerLayout.OnPickerListener() {
            @Override
            public void onPickedList(ArrayList list) {
                int size = list.size();
                if (size == 0) {
                    return;
                } else if (size == 1) {
                    TCVideoFileInfo fileInfo = (TCVideoFileInfo) list.get(0);
                    startVideoCutActivity(fileInfo);
                } else {
                    startVideoJoinActivity(list);
                }
            }
        });
        EventBus.getDefault().register(this);
    }

    private void initWindowParam() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mUGCKitVideoPicker.pauseRequestBitmap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUGCKitVideoPicker.resumeRequestBitmap();
    }

    public void startVideoCutActivity(TCVideoFileInfo fileInfo) {
        Intent intent = new Intent(this, VideoCutActivity.class);
        intent.putExtra(UGCKitConstants.VIDEO_PATH, fileInfo.getFilePath());
        startActivity(intent);
    }

    public void startVideoJoinActivity(ArrayList<TCVideoFileInfo> videoPathList) {
        Intent intent = new Intent(this, VideoJoinerActivity.class);
        intent.putExtra(UGCKitConstants.INTENT_KEY_MULTI_CHOOSE, videoPathList);
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                    mUGCKitVideoPicker.pauseRequestBitmap();
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                    //后台运行不弹
                    if (ActivityPageManager.isBackground(this)) {
                        UserManager.getInstance().setLogin(false);
                        UserManager.getInstance().setLoginToken("");

                        PageJumpUtil.firstIsLoginThenJump(() -> {});
                        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                        new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                        ToastUtils.showShort( "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                        return;
                    }
                    // 判断是不是栈顶的activity，如果不是就不弹框
                    if (this != ActivityPageManager.getInstance().currentActivity()) {
                        return;
                    }
                    // 被踢下线
                    AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                    new XPopup.Builder(this)
                            .isViewMode(true)
                            .dismissOnTouchOutside(false)
                            .dismissOnBackPressed(false)
                            .autoDismiss(false)
                            .setPopupCallback(new SimpleCallback())
                            .asCustom(alertPopup)
                            .show();
                    alertPopup.setOnConfirmListener(() -> {
                        alertPopup.dismiss();
                        UserManager.getInstance().setLogin(false);
                        UserManager.getInstance().setLoginToken("");

                        PageJumpUtil.firstIsLoginThenJump(() -> {});
                        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                        new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                    });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                mUGCKitVideoPicker.pauseRequestBitmap();
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }

}