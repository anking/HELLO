package com.yanhua.media.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.DateUtils;
import com.luck.picture.lib.tools.DoubleUtils;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.luck.picture.lib.tools.ToastUtils;
import com.luck.picture.lib.widget.PreviewViewPager;
import com.yalantis.ucrop.UCrop;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.StatusBarUtil;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.media.R;
import com.yanhua.media.adapter.PictureFragmentAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.yalantis.ucrop.UCrop.EXTRA_CROP_IMAGE;
import static com.yalantis.ucrop.UCrop.EXTRA_INPUT_URI;
import static com.yalantis.ucrop.UCrop.EXTRA_OUTPUT_URI;
import static com.yalantis.ucrop.UCrop.REQUEST_CROP;

/**
 * 发现页面中预览图片
 */
@Route(path = ARouterPath.YXPICTURE_PREVIEW_ACTIVITY)
public class YXPicturePreviewActivity extends BaseMvpActivity implements PictureFragmentAdapter.OnChangeCropListener, View.OnClickListener {
    private int type;
    private int position;
    private ArrayList pathList;

    protected PreviewViewPager viewPager;
    private int totalNumber;
    protected TextView mTvPictureRight, tvTitle;
    private AliIconFontTextView pictureLeftBack;

    private List<LocalMedia> dataLsit;
    protected PictureFragmentAdapter adapter;

    private boolean isContinueAdd;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initEvent();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_yx_picture_preview;
    }

    @Override
    protected void creatPresent() {
        return;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        StatusBarUtil.setImmersiveStatusBar(this, false);//设置为白色字体

        viewPager = findViewById(R.id.preview_pager);
        tvTitle = findViewById(R.id.picture_title);
        pictureLeftBack = findViewById(R.id.pictureLeftBack);
        mTvPictureRight = findViewById(R.id.picture_right);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        Bundle data = getIntent().getExtras();
        if (null != data) {
            type = data.getInt("type", 0);
            position = data.getInt("curPos", 0);
            pathList = data.getStringArrayList("pathList");

            isContinueAdd = data.getBoolean("isContinueAdd", false);

            totalNumber = pathList.size();
        }

        setTitle();

        dataLsit = getIntent().getParcelableArrayListExtra("listLocalMedia");
        initViewPageAdapterData(dataLsit);
    }

    protected void initEvent() {
        mTvPictureRight.setOnClickListener(this);
        pictureLeftBack.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int i) {
                position = i;
                setTitle();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        viewPager.setCurrentItem(position);
    }

    /**
     * 设置标题
     */
    private void setTitle() {
        tvTitle.setText((position + 1) + "/" + totalNumber);
    }

    /**
     * 初始化ViewPage数据
     *
     * @param list
     */
    private void initViewPageAdapterData(List<LocalMedia> list) {
        adapter = new PictureFragmentAdapter(mContext, this);
        adapter.bindData(list);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);

        viewPager.setOffscreenPageLimit(9);
        setTitle();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.pictureLeftBack) {
            onBackPressed();
        } else if (id == R.id.picture_right) {
            ArrayList<String> pathList = new ArrayList<>();
            for (int i = 0; i < dataLsit.size(); i++) {
                LocalMedia item = dataLsit.get(i);

                if (item.isCut()) {
                    pathList.add(item.getCutPath());
                } else {
                    pathList.add(item.getRealPath());
                }
            }

            //发布图片
            ARouter.getInstance().build(ARouterPath.PUBLISH_CONTENT_ACTIVITY)
                    .withInt("type", YXConfig.CONTENT_TYPE_PICTURE)
                    .withSerializable("describeList", null)
                    .withSerializable("pathList", pathList)
                    .withBoolean("isContinueAdd", isContinueAdd)
                    .withParcelableArrayList("dataList", (ArrayList<? extends Parcelable>) dataLsit)
                    .withFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .navigation(mActivity);

            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Uri resultUri = UCrop.getOutput(data);
                        if (resultUri != null && adapter != null) {
                            String cutPath = resultUri.getPath();
                            LocalMedia curLocalMedia = adapter.getItem(viewPager.getCurrentItem());
                            LocalMedia selectLocalMedia = null;
                            boolean isExits = false;
                            for (int i = 0; i < dataLsit.size(); i++) {
                                LocalMedia item = dataLsit.get(i);
                                if (TextUtils.equals(curLocalMedia.getPath(), item.getPath()) || curLocalMedia.getId() == item.getId()) {
                                    isExits = true;
                                    selectLocalMedia = item;
                                    break;
                                }
                            }
                            // 更新当前适配选中的LocalMedia裁剪参数
                            curLocalMedia.setCut(!TextUtils.isEmpty(cutPath));
                            curLocalMedia.setCutPath(cutPath);
                            curLocalMedia.setCropOffsetX(data.getIntExtra(UCrop.EXTRA_OUTPUT_OFFSET_X, 0));
                            curLocalMedia.setCropOffsetY(data.getIntExtra(UCrop.EXTRA_OUTPUT_OFFSET_Y, 0));
                            curLocalMedia.setCropResultAspectRatio(data.getFloatExtra(UCrop.EXTRA_OUTPUT_CROP_ASPECT_RATIO, 0));
                            curLocalMedia.setCropImageWidth(data.getIntExtra(UCrop.EXTRA_OUTPUT_IMAGE_WIDTH, 0));
                            curLocalMedia.setCropImageHeight(data.getIntExtra(UCrop.EXTRA_OUTPUT_IMAGE_HEIGHT, 0));
                            curLocalMedia.setEditorImage(curLocalMedia.isCut());
                            if (SdkVersionUtils.checkedAndroid_Q() && PictureMimeType.isContent(curLocalMedia.getPath())) {
                                curLocalMedia.setAndroidQToPath(cutPath);
                            }
                            if (isExits) {
                                // 更新当前选中列表的LocalMedia裁剪参数
                                selectLocalMedia.setCut(!TextUtils.isEmpty(cutPath));
                                selectLocalMedia.setCutPath(cutPath);

                                selectLocalMedia.setCropOffsetX(data.getIntExtra(UCrop.EXTRA_OUTPUT_OFFSET_X, 0));
                                selectLocalMedia.setCropOffsetY(data.getIntExtra(UCrop.EXTRA_OUTPUT_OFFSET_Y, 0));
                                selectLocalMedia.setCropResultAspectRatio(data.getFloatExtra(UCrop.EXTRA_OUTPUT_CROP_ASPECT_RATIO, 0));
                                selectLocalMedia.setCropImageWidth(data.getIntExtra(UCrop.EXTRA_OUTPUT_IMAGE_WIDTH, 0));
                                selectLocalMedia.setCropImageHeight(data.getIntExtra(UCrop.EXTRA_OUTPUT_IMAGE_HEIGHT, 0));
                                selectLocalMedia.setEditorImage(curLocalMedia.isCut());
                                if (SdkVersionUtils.checkedAndroid_Q() && PictureMimeType.isContent(curLocalMedia.getPath())) {
                                    selectLocalMedia.setAndroidQToPath(cutPath);
                                }
                            }
//                            adapter.notifyDataSetChanged();
//                            initViewPageAdapterData(dataLsit);
                            adapter.clear();

                            adapter.bindData(dataLsit);
                            viewPager.setAdapter(adapter);
                            viewPager.setOffscreenPageLimit(9);
                            viewPager.setCurrentItem(position);
                            setTitle();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onChangeCrop(int pos) {
        //裁剪页面
        LocalMedia localMedia = dataLsit.get(pos);//获取当前点击裁剪的item
        if (null != localMedia) {
            //页面跳转
            onEditorImage(localMedia);
        }
    }

    public void onEditorImage(LocalMedia image) {
        if (DoubleUtils.isFastDoubleClick()) {
            return;
        }

        Activity activity = this;
//        String originalPath = image.isEditorImage() && !TextUtils.isEmpty(image.getCutPath()) ? image.getCutPath() : image.getPath();
        String originalPath = image.getPath();
        String mimeType = image.getMimeType();

        if (TextUtils.isEmpty(originalPath)) {
            ToastUtils.s(activity.getApplicationContext(), activity.getString(R.string.picture_not_crop_data));
            return;
        }

        boolean isHttp = PictureMimeType.isHasHttp(originalPath);
        String suffix = mimeType.replace("image/", ".");
        File file = new File(PictureFileUtils.getDiskCacheDir(activity.getApplicationContext()), DateUtils.getCreateFileName("IMG_CROP_") + suffix);
        Uri uri = isHttp || PictureMimeType.isContent(originalPath) ? Uri.parse(originalPath) : Uri.fromFile(new File(originalPath));
//        UCrop.Options options = UCropManager.basicOptions(activity);
//        options.setHideBottomControls(false);
//        options.setEditorImage(true);

        Intent mCropIntent;
        Bundle mCropOptionsBundle;

        mCropIntent = new Intent();
        mCropOptionsBundle = new Bundle();
        mCropOptionsBundle.putParcelable(EXTRA_INPUT_URI, uri);
        mCropOptionsBundle.putParcelable(EXTRA_OUTPUT_URI, Uri.fromFile(file));
        mCropOptionsBundle.putParcelable(EXTRA_CROP_IMAGE, image);

//        mCropOptionsBundle.putAll(options.getOptionBundle());

        mCropIntent.setClass(activity, YXUCropActivity.class);
        mCropIntent.putExtras(mCropOptionsBundle);

        activity.startActivityForResult(mCropIntent, REQUEST_CROP);
        activity.overridePendingTransition(PictureSelectionConfig.windowAnimationStyle.activityCropEnterAnimation, R.anim.ucrop_anim_fade_in);
    }
}
