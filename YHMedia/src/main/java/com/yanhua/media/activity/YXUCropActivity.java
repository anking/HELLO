package com.yanhua.media.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.immersive.ImmersiveManage;
import com.luck.picture.lib.tools.ScreenUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.yalantis.ucrop.PictureMultiCuttingActivity;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.callback.BitmapCropCallback;
import com.yalantis.ucrop.model.AspectRatio;
import com.yalantis.ucrop.view.CropImageView;
import com.yalantis.ucrop.view.GestureCropImageView;
import com.yalantis.ucrop.view.OverlayView;
import com.yalantis.ucrop.view.TransformImageView;
import com.yalantis.ucrop.view.UCropView;
import com.yalantis.ucrop.view.widget.AspectRatioTextView;
import com.yalantis.ucrop.view.widget.HorizontalProgressWheelView;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.ActivityPageManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.media.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("ConstantConditions")
public class YXUCropActivity extends AppCompatActivity {
    /**
     * 是否使用沉浸式，子类复写该方法来确定是否采用沉浸式
     *
     * @return 是否沉浸式，默认true
     */
    @Override
    public boolean isImmersive() {
        return true;
    }


    /**
     * 具体沉浸的样式，可以根据需要自行修改状态栏和导航栏的颜色
     */
    public void immersive() {
        ImmersiveManage.immersiveAboveAPI23(this
                , mStatusBarColor
                , mToolbarColor
                , isOpenWhiteStatusBar);
    }


    public static final int DEFAULT_COMPRESS_QUALITY = 90;
    public static final Bitmap.CompressFormat DEFAULT_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;

    public static final int NONE = 0;
    public static final int SCALE = 1;

    private static final String TAG = "YXUCropActivity";
    private static final long CONTROLS_ANIMATION_DURATION = 50;
    private static final int ROTATE_WIDGET_SENSITIVITY_COEFFICIENT = 42;

    protected int mScreenWidth;
    // Enables dynamic coloring
    private int mToolbarColor;
    private int mStatusBarColor;
    private int mToolbarWidgetColor;
    @ColorInt
    private int mRootViewBackgroundColor;
    @DrawableRes
    private int mToolbarCancelDrawable;
    @DrawableRes
    private int mToolbarCropDrawable;

    private boolean mShowLoader = true;

    protected RelativeLayout uCropPhotoBox;
    private UCropView mUCropView;
    private GestureCropImageView mGestureCropImageView;
    private OverlayView mOverlayView;
    private List<ViewGroup> mCropAspectRatioViews = new ArrayList<>();
    private TextView mTextViewRotateAngle;
    private Transition mControlsTransition;

    private Bitmap.CompressFormat mCompressFormat = DEFAULT_COMPRESS_FORMAT;
    private int mCompressQuality = DEFAULT_COMPRESS_QUALITY;
    /**
     * 是否可拖动裁剪框
     */
    private boolean isDragFrame;
    /**
     * 是否改变状态栏字体颜色
     */
    private boolean isOpenWhiteStatusBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setNewRequestedOrientation(intent);
        getCustomOptionsData(intent);
        if (isImmersive()) {
            immersive();
        }
        setContentView(R.layout.activity_yxucrop_photobox);
        ActivityPageManager.getInstance().addActivity(this);
        mScreenWidth = ScreenUtils.getScreenWidth(this);
        setupViews(intent);
        setImageData(intent);
        EventBus.getDefault().register(this);
    }

    /**
     * setNewRequestedOrientation
     */
    protected void setNewRequestedOrientation(Intent intent) {
        int requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
        if (getRequestedOrientation() != requestedOrientation) {
            setRequestedOrientation(requestedOrientation);
        }
    }

    /**
     * 获取自定义配制参数
     *
     * @param intent
     */
    private void getCustomOptionsData(@NonNull Intent intent) {
        isOpenWhiteStatusBar = false;
        mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);
        if (mToolbarColor == 0) {
            mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);
        }
        if (mStatusBarColor == 0) {
            mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.ucrop_menu_activity, menu);

        // Change crop & loader menu icons color to match the rest of the UI colors

        MenuItem menuItemLoader = menu.findItem(R.id.menu_loader);
        Drawable menuItemLoaderIcon = menuItemLoader.getIcon();
        if (menuItemLoaderIcon != null) {
            try {
                menuItemLoaderIcon.mutate();
                ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(mToolbarWidgetColor, BlendModeCompat.SRC_ATOP);
                menuItemLoaderIcon.setColorFilter(colorFilter);
                menuItemLoader.setIcon(menuItemLoaderIcon);
            } catch (IllegalStateException e) {
                Log.i(TAG, String.format("%s - %s", e.getMessage(), "必須指定輸入以及輸出的 Uri"));
            }
            ((Animatable) menuItemLoader.getIcon()).start();
        }

        MenuItem menuItemCrop = menu.findItem(R.id.menu_crop);
        Drawable menuItemCropIcon = ContextCompat.getDrawable(this, mToolbarCropDrawable);
        if (menuItemCropIcon != null) {
            menuItemCropIcon.mutate();
            ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, R.color.white), BlendModeCompat.SRC_ATOP);
            menuItemCropIcon.setColorFilter(colorFilter);
            menuItemCrop.setIcon(menuItemCropIcon);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_crop).setVisible(!mShowLoader);
        menu.findItem(R.id.menu_loader).setVisible(mShowLoader);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_crop) {
            cropAndSaveImage();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGestureCropImageView != null) {
            mGestureCropImageView.cancelAllAnimations();
        }
    }

    /**
     * This method extracts all data from the incoming intent and setups views properly.
     */
    protected void setImageData(@NonNull Intent intent) {
        processOptions(intent);

        Uri inputUri = intent.getParcelableExtra(UCrop.EXTRA_INPUT_URI);
        Uri outputUri = intent.getParcelableExtra(UCrop.EXTRA_OUTPUT_URI);

        if (inputUri != null && outputUri != null) {
            try {
                mGestureCropImageView.setRotateEnabled(false);
                mGestureCropImageView.setScaleEnabled(true);
                mGestureCropImageView.setImageUri(inputUri, outputUri);
            } catch (Exception e) {
                setResultError(e);
                onBackPressed();
            }
        } else {
            setResultError(new NullPointerException("在你的 App 內复写颜色资源 (ucrop_color_toolbar_widget) 使 5.0 以前裝置正常运作"));
            onBackPressed();
        }
    }

    /**
     * and setups Activity, {@link OverlayView} and {@link CropImageView} properly.
     */
    @SuppressWarnings("deprecation")
    private void processOptions(@NonNull Intent intent) {
        mCompressQuality = YXUCropActivity.DEFAULT_COMPRESS_QUALITY;

        // custom options
        isDragFrame = false;

        // Crop image view options
        mGestureCropImageView.setMinimumHeight(200);
        mGestureCropImageView.setMinimumWidth(200);
        mGestureCropImageView.setMaxScaleMultiplier(CropImageView.DEFAULT_MAX_SCALE_MULTIPLIER);
        mGestureCropImageView.setImageToWrapCropBoundsAnimDuration(CropImageView.DEFAULT_IMAGE_TO_CROP_BOUNDS_ANIM_DURATION);
        mGestureCropImageView.setRotateEnabled(false);
        // Overlay view options
        mOverlayView.setDimmedBorderColor(getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setDimmedStrokeWidth(1);

        mOverlayView.setFreestyleCropMode(OverlayView.FREESTYLE_CROP_MODE_ENABLE);

        mOverlayView.setDragSmoothToCenter(true);//false--->true
        mOverlayView.setDragFrame(isDragFrame);
        mOverlayView.setDimmedColor(getResources().getColor(R.color.picture_crop_frame));

        mOverlayView.setShowCropFrame(true);
        mOverlayView.setCropFrameColor(getResources().getColor(R.color.ucrop_color_default_crop_frame));
        mOverlayView.setCropFrameStrokeWidth(getResources().getDimensionPixelSize(R.dimen.ucrop_default_crop_frame_stoke_width));


//        mOverlayView.setShowCropGrid(false);
        mOverlayView.setShowCropGrid(OverlayView.DEFAULT_SHOW_CROP_GRID);
        mOverlayView.setCropGridRowCount(OverlayView.DEFAULT_CROP_GRID_ROW_COUNT);
        mOverlayView.setCropGridColumnCount(OverlayView.DEFAULT_CROP_GRID_COLUMN_COUNT);
        mOverlayView.setCropGridColor(getResources().getColor(R.color.ucrop_color_default_crop_grid));
        mOverlayView.setCropGridStrokeWidth(getResources().getDimensionPixelSize(R.dimen.ucrop_default_crop_grid_stoke_width));

        // Result bitmap max size options
        int maxSizeX = intent.getIntExtra(UCrop.EXTRA_MAX_SIZE_X, 0);
        int maxSizeY = intent.getIntExtra(UCrop.EXTRA_MAX_SIZE_Y, 0);

        if (maxSizeX > 0 && maxSizeY > 0) {
            mGestureCropImageView.setMaxResultImageSizeX(maxSizeX);
            mGestureCropImageView.setMaxResultImageSizeY(maxSizeY);
        }
    }

    protected void setupViews(@NonNull Intent intent) {
        mStatusBarColor = ContextCompat.getColor(this, R.color.txt_black);
        mToolbarColor = ContextCompat.getColor(this, R.color.txt_black);

        mToolbarWidgetColor = ContextCompat.getColor(this, R.color.ucrop_color_toolbar_widget);
        mToolbarCancelDrawable = R.drawable.close_white;
        mToolbarCropDrawable = R.drawable.ic_white_ok;
        mRootViewBackgroundColor = ContextCompat.getColor(this, R.color.ucrop_color_crop_background);

        setupAppBar();

        //展示底部控制View
        ViewGroup viewGroup = findViewById(R.id.ucrop_photobox);
        ViewGroup wrapper = viewGroup.findViewById(R.id.controls_wrapper);
        wrapper.setBackgroundColor(mRootViewBackgroundColor);
        LayoutInflater.from(this).inflate(R.layout.yx_ucrop_controls, wrapper, true);

        mControlsTransition = new AutoTransition();
        mControlsTransition.setDuration(CONTROLS_ANIMATION_DURATION);

        setupRotateWidget();//旋转

        initiateRootViews();

        setupAspectRatioWidget();//裁剪比例
    }

    /**
     * Configures and styles both status bar and toolbar.
     */
    private void setupAppBar() {
        setStatusBarColor(mStatusBarColor);

        final Toolbar toolbar = findViewById(R.id.toolbar);

        // Set all of the Toolbar coloring
        toolbar.setBackgroundColor(mToolbarColor);
        toolbar.setTitleTextColor(mToolbarWidgetColor);

        // Color buttons inside the Toolbar
        Drawable stateButtonDrawable = AppCompatResources.getDrawable(this, mToolbarCancelDrawable).mutate();
        ColorFilter colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, R.color.white), BlendModeCompat.SRC_ATOP);
        stateButtonDrawable.setColorFilter(colorFilter);
        toolbar.setNavigationIcon(stateButtonDrawable);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private void initiateRootViews() {
        uCropPhotoBox = findViewById(R.id.ucrop_photobox);
        mUCropView = findViewById(R.id.ucrop);
        mGestureCropImageView = mUCropView.getCropImageView();
        mOverlayView = mUCropView.getOverlayView();

        mGestureCropImageView.setTransformImageListener(mImageListener);

        findViewById(R.id.ucrop_frame).setBackgroundColor(mRootViewBackgroundColor);
    }

    private TransformImageView.TransformImageListener mImageListener = new TransformImageView.TransformImageListener() {
        @Override
        public void onRotate(float currentAngle) {
            setAngleText(currentAngle);
        }

        @Override
        public void onScale(float currentScale) {
        }

        @Override
        public void onLoadComplete() {
            mUCropView.animate().alpha(1).setDuration(300).setInterpolator(new AccelerateInterpolator());
            mShowLoader = false;
            supportInvalidateOptionsMenu();

//            processOptions(getIntent());
        }

        @Override
        public void onLoadFailure(@NonNull Exception e) {
            setResultError(e);
            onBackPressed();
        }
    };

    /**
     * Sets status-bar color for L devices.
     *
     * @param color - status-bar color
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Window window = getWindow();
            if (window != null) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        }
    }

    private void setupAspectRatioWidget() {
        mCropAspectRatioViews.clear();

        Intent intent = getIntent();
        LocalMedia media = intent.getParcelableExtra(UCrop.EXTRA_CROP_IMAGE);

        float twidth;
        float theight;

//        if (media.isCut() && !media.isCompressed()) {
//            twidth = media.getCropImageWidth();
//            theight = media.getCropImageHeight();
//        } else if (media.isCompressed() || (media.isCut() && media.isCompressed())) {
//            twidth = media.getWidth();
//            theight = media.getHeight();
//        } else {
//            twidth = media.getWidth();
//            theight = media.getHeight();
//        }
        twidth = media.getWidth();
        theight = media.getHeight();

        if (twidth > 0 && theight > 0) {
            float originAspectRatioX = CropImageView.SOURCE_IMAGE_ASPECT_RATIO, originAspectRatioY = CropImageView.SOURCE_IMAGE_ASPECT_RATIO;

            float wh = 0;
            wh = twidth / theight;//4:3 1:1 3:4
            if (wh < 0.9f) {
                originAspectRatioX = 3;
                originAspectRatioY = 4;
            } else if (wh >= 0.9 && wh <= 1.1f) {
                originAspectRatioX = 1;
                originAspectRatioY = 1;
            } else if (wh > 1.1) {
                originAspectRatioX = 4;
                originAspectRatioY = 3;
            }

            setCropAspectRatio(originAspectRatioX, originAspectRatioY);
            Log.e(TAG, "wh=" + wh + ",originAspectRatioX=" + originAspectRatioX + ",originAspectRatioY=" + originAspectRatioY + ",twidth=" + twidth + ",theight=" + theight);
        } else {
            final String path;
//            if (media.isCut() && !media.isCompressed()) {
//                // 裁剪过
//                path = media.getCutPath();
//            } else if (media.isCompressed() || (media.isCut() && media.isCompressed())) {
//                // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
//                path = media.getCompressPath();
//            } else if (!TextUtils.isEmpty(media.getAndroidQToPath())) {
//                // AndroidQ特有path
//                path = media.getAndroidQToPath();
//            } else {
//                // 原图
//                path = media.getPath();
//            }
            path = media.getPath();
            Glide.with(this).asBitmap().load(path).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, com.bumptech.glide.request.transition.Transition<? super Bitmap> transition) {
                    float width = bitmap.getWidth();
                    float height = bitmap.getHeight();
                    float originAspectRatioX = CropImageView.SOURCE_IMAGE_ASPECT_RATIO, originAspectRatioY = CropImageView.SOURCE_IMAGE_ASPECT_RATIO;
                    float wh = 0;
                    wh = width / height;//4:3 1:1 3:4
                    if (wh < 0.9f) {
                        originAspectRatioX = 3;
                        originAspectRatioY = 4;
                    } else if (wh >= 0.9 && wh <= 1.1f) {
                        originAspectRatioX = 1;
                        originAspectRatioY = 1;
                    } else if (wh > 1.1) {
                        originAspectRatioX = 4;
                        originAspectRatioY = 3;
                    }

                    setCropAspectRatio(originAspectRatioX, originAspectRatioY);
//                    Log.e(TAG, "wh=" + wh + ",originAspectRatioX=" + originAspectRatioX + ",originAspectRatioY=" + originAspectRatioY + ",twidth=" + twidth + ",theight=" + theight);
                }
            });
        }

    }

    private void setCropAspectRatio(float originAspectRatioX, float originAspectRatioY) {
        AspectRatio oriAspectRatio = new AspectRatio(getString(R.string.ucrop_label_original).toUpperCase(),
                originAspectRatioX, originAspectRatioY);

        ArrayList<AspectRatio> aspectRatioList = new ArrayList<>();
        aspectRatioList.add(oriAspectRatio);
        aspectRatioList.add(new AspectRatio(null, 3, 4));
        aspectRatioList.add(new AspectRatio(null, 1, 1));
        aspectRatioList.add(new AspectRatio(null, 4, 3));

        LinearLayout wrapperAspectRatioList = findViewById(R.id.layout_aspect_ratio);

        FrameLayout wrapperAspectRatio;
        AspectRatioTextView aspectRatioTextView;

        int size = aspectRatioList.size();

        int perWitdh = mScreenWidth / (2 * size);//平均宽度为屏幕宽

        for (int i = 0; i < size; i++) {
            AspectRatio aspectRatio = aspectRatioList.get(i);

            wrapperAspectRatio = (FrameLayout) getLayoutInflater().inflate(R.layout.yx_ucrop_aspect_ratio, null);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            float mAspectRatioX = aspectRatio.getAspectRatioX();
            float mAspectRatioY = aspectRatio.getAspectRatioY();
            String title = aspectRatio.getAspectRatioTitle();
            int width;
            int height;

            if (!TextUtils.isEmpty(title)) {
                height = perWitdh;
                width = perWitdh * 3 / 5;
            } else {
                if (mAspectRatioX >= mAspectRatioY) {
                    width = perWitdh;
                    height = (int) (perWitdh * mAspectRatioY / mAspectRatioX);
                } else {
                    height = perWitdh;
                    width = (int) (perWitdh * mAspectRatioX / mAspectRatioY);
                }
            }

            lp.height = height;
            lp.width = width;
            lp.gravity = Gravity.CENTER;

            if (i == size - 1) {
                lp.rightMargin = 0;
            } else {
                lp.rightMargin = perWitdh;
            }

            wrapperAspectRatio.setLayoutParams(lp);

            aspectRatioTextView = ((AspectRatioTextView) wrapperAspectRatio.getChildAt(0));
            aspectRatioTextView.setAspectRatio(aspectRatio);
            aspectRatioTextView.setActiveColor(this.getResources().getColor(R.color.white));
            aspectRatioTextView.setTextColor(this.getResources().getColor(R.color.white));

            wrapperAspectRatioList.addView(wrapperAspectRatio);
            mCropAspectRatioViews.add(wrapperAspectRatio);
        }

        //默认原始比例-----根据宽度高度、比例--------
        mCropAspectRatioViews.get(0).setSelected(true);
        mGestureCropImageView.setTargetAspectRatio(originAspectRatioX / originAspectRatioY);
        mGestureCropImageView.setImageToWrapCropBounds();


        int index = -1;
        for (ViewGroup cropAspectRatioView : mCropAspectRatioViews) {
            index++;
            cropAspectRatioView.setTag(index);
            cropAspectRatioView.setOnClickListener(v -> {
                mGestureCropImageView.setTargetAspectRatio(
                        ((AspectRatioTextView) ((ViewGroup) v).getChildAt(0)).getAspectRatio(v.isSelected()));
                mGestureCropImageView.setImageToWrapCropBounds();
                if (!v.isSelected()) {
                    for (ViewGroup cropAspectRatioView1 : mCropAspectRatioViews) {
                        cropAspectRatioView1.setSelected(cropAspectRatioView1 == v);
                    }
                }
            });
        }
    }

    private void setupRotateWidget() {
        mTextViewRotateAngle = findViewById(R.id.text_view_rotate);
        ((HorizontalProgressWheelView) findViewById(R.id.rotate_scroll_wheel))
                .setScrollingListener(new HorizontalProgressWheelView.ScrollingListener() {
                    @Override
                    public void onScroll(float delta, float totalDistance) {
                        mGestureCropImageView.postRotate(delta / ROTATE_WIDGET_SENSITIVITY_COEFFICIENT);
                    }

                    @Override
                    public void onScrollEnd() {
                        mGestureCropImageView.setImageToWrapCropBounds();
                    }

                    @Override
                    public void onScrollStart() {
                        mGestureCropImageView.cancelAllAnimations();
                    }
                });
        ((HorizontalProgressWheelView) findViewById(R.id.rotate_scroll_wheel)).setLinesColor(ContextCompat.getColor(this, R.color.white), ContextCompat.getColor(this, R.color.white));
    }

    private void setAngleText(float angle) {
        if (mTextViewRotateAngle != null) {
            int ang = (int) angle;

            if (Math.abs(ang) == 0) {
                ang = 0;
                Vibrator vibrator = (Vibrator) this.getSystemService(this.VIBRATOR_SERVICE);
                vibrator.vibrate(50);
            }

            mTextViewRotateAngle.setText(String.format(Locale.getDefault(), "旋转角度%d°", ang));
        }
    }

    protected void cropAndSaveImage() {
        mShowLoader = true;
        supportInvalidateOptionsMenu();

        mGestureCropImageView.cropAndSaveImage(mCompressFormat, mCompressQuality, new BitmapCropCallback() {
            @Override
            public void onBitmapCropped(@NonNull Uri resultUri, int offsetX, int offsetY, int imageWidth, int imageHeight) {
                setResultUri(resultUri, mGestureCropImageView.getTargetAspectRatio(), offsetX, offsetY, imageWidth, imageHeight);
                Activity currentActivity = getCurrentActivity();
                if (currentActivity instanceof PictureMultiCuttingActivity) {
                    // 如果是多图裁剪则返回逻辑交给PictureMultiCuttingActivity.java去处理
                } else {
                    onBackPressed();
                }
            }

            @Override
            public void onCropFailure(@NonNull Throwable t) {
                setResultError(t);
                onBackPressed();
            }
        });
    }

    protected void setResultUri(Uri uri, float resultAspectRatio, int offsetX, int offsetY, int imageWidth, int imageHeight) {
        setResult(RESULT_OK, new Intent()
                .putExtra(UCrop.EXTRA_OUTPUT_URI, uri)
                .putExtra(UCrop.EXTRA_OUTPUT_CROP_ASPECT_RATIO, resultAspectRatio)
                .putExtra(UCrop.EXTRA_OUTPUT_IMAGE_WIDTH, imageWidth)
                .putExtra(UCrop.EXTRA_OUTPUT_IMAGE_HEIGHT, imageHeight)
                .putExtra(UCrop.EXTRA_OUTPUT_OFFSET_X, offsetX)
                .putExtra(UCrop.EXTRA_OUTPUT_OFFSET_Y, offsetY)
                .putExtra(UCrop.EXTRA_EDITOR_IMAGE, true)
        );
    }

    protected void setResultError(Throwable throwable) {
        setResult(UCrop.RESULT_ERROR, new Intent().putExtra(UCrop.EXTRA_ERROR, throwable));
    }

    protected Activity getCurrentActivity() {
        return this;
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * exit activity
     */
    protected void closeActivity() {
        finish();
        exitAnimation();
    }

    protected void exitAnimation() {
        overridePendingTransition(R.anim.ucrop_anim_fade_in, R.anim.ucrop_close);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);

                    ToastUtils.showShort("您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。");
                    return;
                }
                // 判断是不是栈顶的activity，如果不是就不弹框
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                // 被踢下线
                AlertPopup alertPopup = new AlertPopup(this, "下线通知", "您的账号已在另一台设备登录，您被踢下线！如果不是本人操作，请重新登录后修改登录密码。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup)
                        .show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");

                    PageJumpUtil.firstIsLoginThenJump(() -> {});
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    new Handler().postDelayed(() -> ActivityPageManager.getInstance().finishMiddleActivities(), 200);
                });
                break;
            case CommonConstant.CONN_USER_BLOCKED:
                UserManager.getInstance().setLogin(false);
                UserManager.getInstance().setLoginToken("");
                //后台运行不弹
                if (ActivityPageManager.isBackground(this)) {
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                    return;
                }
                if (this != ActivityPageManager.getInstance().currentActivity()) {
                    return;
                }
                AlertPopup alertPopup1 = new AlertPopup(this, "提示", "您的账号已被封禁，如有疑问，请联系平台客服。", "确定", true);
                new XPopup.Builder(this)
                        .isViewMode(true)
                        .dismissOnTouchOutside(false)
                        .dismissOnBackPressed(false)
                        .autoDismiss(false)
                        .setPopupCallback(new SimpleCallback())
                        .asCustom(alertPopup1)
                        .show();
                alertPopup1.setOnConfirmListener(() -> {
                    alertPopup1.dismiss();
                    UserManager.getInstance().setLogin(false);
                    UserManager.getInstance().setLoginToken("");
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
                });
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ActivityPageManager.getInstance().removeActivity(this);
    }
}
