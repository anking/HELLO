package com.yanhua.media.view;

import android.content.Context;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.tencent.qcloud.ugckit.module.effect.VideoEditerSDK;
import com.tencent.qcloud.ugckit.module.effect.bgm.TCMusicInfo;
import com.tencent.qcloud.ugckit.utils.BackgroundTasks;
import com.tencent.ugc.TXVideoEditer;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.media.R;
import com.yanhua.media.adapter.MusicItemAdapter;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.presenter.VideoBgMusicPresenter;
import com.yanhua.media.presenter.contract.VideoBgMusicContract;
import com.yanhua.media.utils.YXMusicManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MusicBottomPopup extends BaseMvpBottomPopupView<VideoBgMusicPresenter> implements VideoBgMusicContract.IView, SeekBar.OnSeekBarChangeListener {

    private Context mContext;
    private CommonTabLayout ctlMusic;
    private CommonTabLayout ctlMusicBottom;

    private LinearLayout llSuggest, llVolume;
    private ImageButton ibtMusicEdit, ibtMusicCollect;

    private RecyclerView recyclerView;
    private TextView tvCollectTip;

    private SeekBar sbAxVolume, sbOriVolume;
    private HashMap<String, Object> params;

    private MusicInfoModel selectMusic;

    private boolean isFavirate;//标记是收藏
    private List<MusicInfoModel> musicSuggestList;
    private List<MusicInfoModel> musicFaviteList;
    private MusicItemAdapter mAdapter;

    private int mMicVolume = 100;
    private int mBGMVolume = 100;

    private MusicInfoModel currentMusic;
    private int currentPos = 0;

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    private MediaPlayer mediaPlayer;
    private YXMusicManager.LoadMusicListener mLoadMusicListener;

    public MusicBottomPopup(@NonNull Context context) {
        super(context);

        mContext = context;
    }

    public MusicBottomPopup(@NonNull Context context, MusicInfoModel selectItem) {
        super(context);

        mContext = context;
        selectMusic = selectItem;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new VideoBgMusicPresenter();
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        ctlMusic = findViewById(R.id.ctl_music);

        llSuggest = findViewById(R.id.ll_suggest);
        llVolume = findViewById(R.id.ll_volume);

        ibtMusicEdit = findViewById(R.id.ibt_music_edit);
        ibtMusicCollect = findViewById(R.id.ibt_music_collect);

        recyclerView = findViewById(R.id.recyclerView);
        tvCollectTip = findViewById(R.id.tv_collect_tip);

        sbAxVolume = findViewById(R.id.seekbar_ax_volume);
        sbOriVolume = findViewById(R.id.seekbar_ori_volume);


        sbAxVolume.setOnSeekBarChangeListener(this);
        sbOriVolume.setOnSeekBarChangeListener(this);

        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("推荐", 0, 0));
        tabEntityList.add(new TabEntity("收藏", 0, 0));

        ctlMusic.setTabData(tabEntityList);

        params = new HashMap<>();
        params.put("current", 1);
        params.put("size", 20);

        ctlMusic.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    isFavirate = false;

                    tvCollectTip.setVisibility(GONE);

                    basePresenter.getMusicList(params);
                } else {
                    isFavirate = true;
                    tvCollectTip.setVisibility(GONE);
                    basePresenter.getFollowMusicList(params);
                }

                mAdapter.setTabType(isFavirate);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        ctlMusic.setCurrentTab(0);

        ctlMusicBottom = findViewById(R.id.ctl_music_bottom);
        ArrayList<CustomTabEntity> tabBottomList = new ArrayList<>();
        tabBottomList.add(new TabEntity("配乐", 0, 0));
        tabBottomList.add(new TabEntity("音量", 0, 0));

        ctlMusicBottom.setTabData(tabBottomList);
        ctlMusicBottom.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    llSuggest.setVisibility(VISIBLE);
                    llVolume.setVisibility(GONE);
                } else {
                    llSuggest.setVisibility(GONE);
                    llVolume.setVisibility(VISIBLE);
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        ctlMusicBottom.setCurrentTab(0);
        llSuggest.setVisibility(VISIBLE);
        llVolume.setVisibility(GONE);

        //数据初始化
        basePresenter.getMusicList(params);

        musicSuggestList = new ArrayList<>();
        musicFaviteList = new ArrayList<>();

        initRecyclerView();

        VideoEditerSDK.getInstance().releaseSDK();
        VideoEditerSDK.getInstance().clear();
        VideoEditerSDK.getInstance().initSDK();

        mediaPlayer = new MediaPlayer();

        mLoadMusicListener = new YXMusicManager.LoadMusicListener() {
            @Override
            public void onBgmDownloadSuccess(MusicInfoModel model, final String filePath) {
                BackgroundTasks.getInstance().postDelayed(() -> {
                    if (model == null) return;

                    model.setStatus(MusicInfoModel.STATE_DOWNLOADED);
                    model.setLocalPath(filePath);

                    // 播放音乐
                    try {
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(model.getMusicUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        // 设置播放的时候一直让屏幕变亮
                        mediaPlayer.setScreenOnWhilePlaying(true);
                        mediaPlayer.setOnCompletionListener(mp -> {
                            model.setPlaying(false);
                            mAdapter.notifyDataSetChanged();
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    model.setPlaying(true);

                    if (null != musicSelectListener) {
                        musicSelectListener.selectResult(model, currentPos);
                    }
                    mAdapter.notifyDataSetChanged();
                }, 200);
            }

            @Override
            public void onDownloadFail(MusicInfoModel model, final String errorMsg) {
                BackgroundTasks.getInstance().runOnUiThread(() -> {
                    if (model == null) return;
                    model.setStatus(MusicInfoModel.STATE_UNDOWNLOAD);
                    mAdapter.notifyDataSetChanged();
                    ToastUtils.showShort("下载失败");
                });
            }

            @Override
            public void onDownloadProgress(MusicInfoModel model, final int progress) {
                    if (model == null) return;
                    model.setStatus(MusicInfoModel.STATE_DOWNLOADING);
            }
        };

        mAdapter.setOnItemClickOperateListener(new MusicItemAdapter.OnItemClickOperateListener() {
            @Override
            public void selectMusic(View view, int pos) {
                if (pos == 0 && !isFavirate) {
                    if (null != musicSelectListener) {
                        musicSelectListener.findMore();
                    }
                    dismiss();
                    return;
                }

                currentPos = pos;
                List dataList = mAdapter.getmDataList();
                currentMusic = (MusicInfoModel) dataList.get(pos);

                if (currentMusic.isPlaying()) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                    }
                } else {
                    if (currentMusic.status == TCMusicInfo.STATE_UNDOWNLOAD) {
                        currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
                        downloadMusic();
                    } else if (currentMusic.status == TCMusicInfo.STATE_DOWNLOADED) {
                        //选中
                        musicSelectListener.selectResult(currentMusic, currentPos);
                        // 播放音乐
                        try {
                            mediaPlayer.reset();
                            mediaPlayer.setDataSource(currentMusic.getMusicUrl());
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                            // 设置播放的时候一直让屏幕变亮
                            mediaPlayer.setScreenOnWhilePlaying(true);
                            mediaPlayer.setOnCompletionListener(mp -> {
                                currentMusic.setPlaying(false);
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    setCutEdit(pos);
                }

                currentMusic.setPlaying(!currentMusic.isPlaying());

                //-----------------切换背景------------------------
                for (int i = 0; i < dataList.size(); i++) {
                    MusicInfoModel musicInfoModel = (MusicInfoModel) dataList.get(i);
                    if (i == pos) {
                        musicInfoModel.setSelect(true);
                    } else {
                        musicInfoModel.setSelect(false);
                    }
                }

                mAdapter.notifyDataSetChanged();

//                if (isFavirate) {
//                    musicSuggestList = mAdapter.getmDataList();
//                } else {
//                    musicFaviteList = mAdapter.getmDataList();
//                }
            }
        });

        ibtMusicEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null == currentMusic) {
                    //假如此时没有选音乐，点击将提示请先选取背景音乐
                    ToastUtils.showShort( "请先选取背景音乐");
                } else {
                    //编辑
                    if (null != musicSelectListener) {
                        musicSelectListener.editMusic(currentMusic, currentPos);
                    }
                }
            }
        });

        ibtMusicCollect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null == currentMusic) {
                    //假如此时没有选音乐，点击将提示请先选取背景音乐
                    ToastUtils.showShort( "请先选取背景音乐");
                } else {
                    //
                    basePresenter.followMusicOrNot(currentMusic.getId(), selectPos);
                }
            }
        });
    }


    private int selectPos;

    /**
     * 当前选中的音乐
     *
     * @param selectItem
     */
    public void setCurrentMusct(MusicInfoModel selectItem) {
        selectMusic = selectItem;

        if (null != selectMusic) {
            List<MusicInfoModel> dataList = mAdapter.getmDataList();
            for (int i = 0; i < dataList.size(); i++) {
                MusicInfoModel musicInfoModel = (MusicInfoModel) dataList.get(i);
                if (musicInfoModel.getId().equals(selectMusic.getId())) {
                    selectPos = i;
                    musicInfoModel.setSelect(true);
                } else {
                    musicInfoModel.setSelect(false);
                }
            }

            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 更新
     *
     * @param pos
     */
    private void setCutEdit(int pos) {
        if (null != currentMusic) {
            int isFollow = currentMusic.getIsFollow();//是否收藏

            ibtMusicCollect.setImageResource(isFollow == 1 ? R.drawable.collect_selected : R.drawable.ic_music_collect);
        }
    }

    /**
     * 下载音乐
     */
    private void downloadMusic() {
        YXMusicManager.getInstance().setOnLoadMusicListener(mLoadMusicListener);

        if (TextUtils.isEmpty(currentMusic.getLocalPath())) {
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
            return;
        }
        File file = new File(currentMusic.getLocalPath());
        if (!file.isFile()) {
            currentMusic.setLocalPath("");
            currentMusic.status = TCMusicInfo.STATE_DOWNLOADING;
            YXMusicManager.getInstance().downloadMusicInfo(currentMusic);
        }
    }

    @Override
    public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
        TXVideoEditer editer = VideoEditerSDK.getInstance().getEditer();//处理音量

        if (seekBar.getId() == R.id.seekbar_ori_volume) {
            mMicVolume = progress;

            editer.setVideoVolume(mMicVolume);
        } else if (seekBar.getId() == R.id.seekbar_ax_volume) {
            mBGMVolume = progress;

            editer.setBGMVolume(mBGMVolume);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MusicItemAdapter(getContext());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_pop_music;
    }

    @Override
    public void handleSuccessMusicList(ListResult<MusicInfoModel> result) {
        musicSuggestList.clear();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        if (result != null && result.getRecords() != null) {
            List<MusicInfoModel> records = result.getRecords();
            if (records != null && !records.isEmpty()) {
                YXMusicManager.getInstance().getLocalPath(records);
                musicSuggestList.addAll(records);
            }
        }

        //假如一个 + type
        MusicInfoModel item = new MusicInfoModel();
        item.setName("更多音乐");
        item.setId("0");
        musicSuggestList.add(0, item);


        if (null != selectMusic) {
            for (int i = 0; i < musicSuggestList.size(); i++) {
                MusicInfoModel musicInfoModel = (MusicInfoModel) musicSuggestList.get(i);
                if (musicInfoModel.getId().equals(selectMusic.getId())) {
                    selectPos = i;
                    musicInfoModel.setSelect(true);

                    currentMusic = musicInfoModel;
                    setCutEdit(selectPos);
                } else {
                    musicInfoModel.setSelect(false);
                }
            }
        }

        if (!musicSuggestList.isEmpty()) {
            mAdapter.setItems(musicSuggestList);
        }
    }

    @Override
    public void handleSuccessFollowMusicList(ListResult<MusicInfoModel> result) {
        musicFaviteList.clear();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        if (result != null && result.getRecords() != null) {
            List<MusicInfoModel> records = result.getRecords();
            if (records != null && !records.isEmpty()) {
                YXMusicManager.getInstance().getLocalPath(records);
                musicFaviteList.addAll(records);

                if (null != selectMusic) {
                    for (int i = 0; i < musicFaviteList.size(); i++) {
                        MusicInfoModel musicInfoModel = (MusicInfoModel) musicFaviteList.get(i);
                        if (musicInfoModel.getId().equals(selectMusic.getId())) {
                            selectPos = i;
                            musicInfoModel.setSelect(true);

                            currentMusic = musicInfoModel;

                            setCutEdit(selectPos);
                        } else {
                            musicInfoModel.setSelect(false);
                        }
                    }
                }
            }
        }
        if (musicFaviteList.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            tvCollectTip.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            tvCollectTip.setVisibility(View.GONE);
            mAdapter.setItems(musicFaviteList);
        }
    }

    @Override
    public void handleSuccessFollowMusicOrNot(int pos) {
        if (currentMusic.isFollow() == 1) {
            ToastUtils.showShort( "取消收藏");
            currentMusic.setIsFollow(0);
        } else {
            ToastUtils.showShort("收藏成功");
            currentMusic.setIsFollow(1);
        }

        setCutEdit(pos);
    }

    private OnMusicSelectListener musicSelectListener;

    public void setOnMusicSelectListener(OnMusicSelectListener listener) {
        this.musicSelectListener = listener;
    }

    public interface OnMusicSelectListener {
        void findMore();

        void selectResult(MusicInfoModel item, int pos);

        void editMusic(MusicInfoModel item, int pos);

        void onDismiss();
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();

        if (null != musicSelectListener) {
            musicSelectListener.onDismiss();
        }
//        currentMusic = null;
//        currentPos = 0;

        pauseMediaPlayer();
    }

    public void pauseMediaPlayer() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();

            currentMusic.setPlaying(false);

            mAdapter.notifyItemChanged(currentPos);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
