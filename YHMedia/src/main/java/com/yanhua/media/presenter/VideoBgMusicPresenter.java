package com.yanhua.media.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.media.api.MediaHttpMethod;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.model.MusicTypeModel;
import com.yanhua.media.presenter.contract.VideoBgMusicContract;

import java.util.HashMap;

public class VideoBgMusicPresenter extends MvpPresenter<VideoBgMusicContract.IView> implements VideoBgMusicContract.Presenter {

    @Override
    public void getMusicTypeList(HashMap<String, Object> params) {
        addSubscribe(MediaHttpMethod.getInstance().getMusicTypeList(new SimpleSubscriber<HttpResult<ListResult<MusicTypeModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MusicTypeModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessMusicTypeList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getMusicList(HashMap<String, Object> params) {
        addSubscribe(MediaHttpMethod.getInstance().getMusicList(new SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MusicInfoModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessMusicList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getFollowMusicList(HashMap<String, Object> params) {
        addSubscribe(MediaHttpMethod.getInstance().getFollowMusicList(new SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MusicInfoModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessFollowMusicList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void followMusicOrNot(String musicId,int pos) {
        addSubscribe(MediaHttpMethod.getInstance().followMusicOrNot(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleSuccessFollowMusicOrNot(pos);
                        } else {
                            baseView.handleFailFollowMusicOrNot(pos);
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, musicId));
    }
}
