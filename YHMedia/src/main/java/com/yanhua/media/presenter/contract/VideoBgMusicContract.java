package com.yanhua.media.presenter.contract;


import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.model.MusicTypeModel;

import java.util.HashMap;

public interface VideoBgMusicContract {

    interface IView extends BaseView {
        default void handleSuccessMusicTypeList(ListResult<MusicTypeModel> result) {
        }

        default void handleSuccessMusicList(ListResult<MusicInfoModel> result) {
        }

        default void handleSuccessFollowMusicList(ListResult<MusicInfoModel> result) {

        }

        default void handleSuccessFollowMusicOrNot(int pos) {
        }

        default void handleFailFollowMusicOrNot(int pos) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        /**
         * 获取音乐类型列表
         *
         * @param params
         */
        void getMusicTypeList(HashMap<String, Object> params);

        /**
         * 获取音乐列表
         *
         * @param params
         */
        void getMusicList(HashMap<String, Object> params);

        /**
         * 获取收藏的音乐列表
         *
         * @param params
         */
        void getFollowMusicList(HashMap<String, Object> params);

        /**
         * 关注或取消关注音乐
         *
         * @param musicId
         */
        void followMusicOrNot(String musicId,int pos);
    }
}
