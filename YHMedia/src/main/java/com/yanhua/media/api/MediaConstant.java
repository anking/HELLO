package com.yanhua.media.api;

public class MediaConstant {

    // 音乐类型列表
    public static final String GET_MUSICTYPE_LIST = "sysadmin/app/frontend/musicType/page";

    // 音乐列表
    public static final String GET_MUSIC_LIST = "sysadmin/app/frontend/music/page";

    // 收藏音乐列表
    public static final String GET_FOLLOW_MUSIC_LIST = "sysadmin/app/frontend/memberMusicFollow/page";

    // 关注或者取消关注
    public static final String FOLLOW_MUSIC_OR_NOT = "sysadmin/app/frontend/memberMusicFollow/follow/{musicId}";

}
