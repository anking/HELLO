package com.yanhua.media.api;


import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.model.MusicTypeModel;

import java.util.HashMap;

public class MediaHttpMethod extends BaseRtHttpMethod {

    MediaService mediaService;

    public MediaHttpMethod() {
        this.mediaService = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), MediaService.class);
    }

    private static class MediaHttpMethodHolder {
        private static final MediaHttpMethod INSTANCE = new MediaHttpMethod();
    }

    // 获取单例
    public static MediaHttpMethod getInstance() {
        return MediaHttpMethodHolder.INSTANCE;
    }

    public SimpleSubscriber<HttpResult<ListResult<MusicTypeModel>>> getMusicTypeList(SimpleSubscriber<HttpResult<ListResult<MusicTypeModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(mediaService.getMusicTypeList(params), subscriber);
        return subscriber;
    }

    public SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>> getMusicList(SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(mediaService.getMusicList(params), subscriber);
        return subscriber;
    }

    public SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>> getFollowMusicList(SimpleSubscriber<HttpResult<ListResult<MusicInfoModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(mediaService.getFollowMusicList(params), subscriber);
        return subscriber;
    }

    public SimpleSubscriber<HttpResult> followMusicOrNot(SimpleSubscriber<HttpResult> subscriber, String musicId) {
        tocompose(mediaService.followMusicOrNot(musicId), subscriber);
        return subscriber;
    }

}
