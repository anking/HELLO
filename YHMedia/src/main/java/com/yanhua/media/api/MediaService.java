package com.yanhua.media.api;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.media.model.MusicInfoModel;
import com.yanhua.media.model.MusicTypeModel;

import java.util.HashMap;

import io.reactivex.rxjava3.core.Flowable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface MediaService {

    @GET(MediaConstant.GET_MUSICTYPE_LIST)
    Flowable<HttpResult<ListResult<MusicTypeModel>>> getMusicTypeList(@QueryMap HashMap<String, Object> params);

    @GET(MediaConstant.GET_MUSIC_LIST)
    Flowable<HttpResult<ListResult<MusicInfoModel>>> getMusicList(@QueryMap HashMap<String, Object> params);

    @GET(MediaConstant.GET_FOLLOW_MUSIC_LIST)
    Flowable<HttpResult<ListResult<MusicInfoModel>>> getFollowMusicList(@QueryMap HashMap<String, Object> params);

    @POST(MediaConstant.FOLLOW_MUSIC_OR_NOT)
    Flowable<HttpResult> followMusicOrNot(@Path("musicId") String musicId);
}
