## material google
* 使用ShapeableImageView （方式见资料）
* Blankj工具相关代码copy（具体见Blankj的使用说明）
* MMKV 
** MMKV易语言封装调用， 这就需要一个性能非常高的通用 key-value 存储组件，我们参考了 SharedPreferences、NSUserDefaults、SQLite 等常见组件，发现都没能满足如此苛刻的性能要求。考虑到这个防 crash 方案最主要的诉求还是实时写入，而 mmap 内存映射文件刚好满足这种需求，我们尝试通过它来实现一套 key-value 组件。
