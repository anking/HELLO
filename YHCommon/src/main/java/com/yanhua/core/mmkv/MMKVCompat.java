package com.yanhua.core.mmkv;



import com.tencent.mmkv.MMKV;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 由于editor.commit方法是同步的，并且我们很多时候的commit操作都是UI线程中，毕竟是IO操作，所以尽可能采用异步操作；
 *  所以我们使用apply进行替代，apply异步的进行写入
 * @author wzg
 */
public class MMKVCompat {




    /**
     * 反射查找apply的方法
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Method findApplyMethod() {
        try {
            Class clz = MMKV.class;
            return clz.getMethod("apply");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }


    private static final Method mApplyMethod = findApplyMethod();

    /**
     * 如果找到则使用apply执行，否则使用commit
     *
     * @param mmkv
     */
    public static void apply(MMKV mmkv) {
        try {
            if (mApplyMethod != null) {
                mApplyMethod.invoke(mmkv);
                return;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        mmkv.commit();
    }
}