package com.yanhua.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Layout;
import android.text.TextPaint;
import android.util.AttributeSet;

import com.yanhua.core.R;

public class TextViewNoPadding extends androidx.appcompat.widget.AppCompatTextView {
    private final Paint mPaint = new Paint();
    private final Rect mBounds = new Rect();
    private boolean mUserPaddingInXml;

    public TextViewNoPadding(Context context) {
        this(context, null);
    }

    public TextViewNoPadding(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewNoPadding(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextViewNoPadding);
            mUserPaddingInXml = a.getBoolean(R.styleable.TextViewNoPadding_userPaddingInXml, false);
            a.recycle();
        }
    }
//    @Override
//    protected void onDraw(@NonNull Canvas canvas) {
//        if (!mUserPaddingInXml) {
//            final String text = calculateTextParams();
//
//            final int left = mBounds.left;
//
//            final int bottom = mBounds.bottom;
//
//            mBounds.offset(-mBounds.left, -mBounds.top);
//
//            mPaint.setAntiAlias(true);
//
//            mPaint.setColor(getCurrentTextColor());
//
//            canvas.drawText(text, -left, mBounds.bottom - bottom, mPaint);
//        } else {
//            super.onDraw(canvas);
//        }
//    }
//
//    @Override
//
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        if (!mUserPaddingInXml) {
//            calculateTextParams();
//
//            int width = mBounds.right - mBounds.left;
//
//            setMeasuredDimension(width, -mBounds.top + mBounds.bottom);
//        }
//    }
//
//    private String calculateTextParams() {
//        final String text = getText().toString();
//
//        final int textLength = text.length();
//
//        mPaint.setTextSize(getTextSize());
//
//        mPaint.getTextBounds(text, 0, textLength, mBounds);
//
//        if (textLength == 0) {
//            mBounds.right = mBounds.left;
//        }
////
////        int width = mBounds.right - mBounds.left;
////        int textViewWidth = getMeasuredWidth();
////
////        if (width > textViewWidth) {
////            mBounds.right = mBounds.left + textViewWidth;
////        }
//
//        return text;
//    }


    /**
     * 控制measure()方法 刷新测量
     */
    private boolean refreshMeasure = true;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!mUserPaddingInXml) {
            removeSpace(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        // 每次文本内容改变时，需要测量两次，确保计算的高度没有问题
        refreshMeasure = true;
    }

    /**
     * 这里处理文本的上下留白问题
     */
    private void removeSpace(int widthspc, int heightspc) {
        int paddingTop = 0;
        String[] linesText = getLinesText();
        TextPaint paint = getPaint();
        Rect rect = new Rect();
        String text = linesText[0];
        paint.getTextBounds(text, 0, text.length(), rect);

        Paint.FontMetricsInt fontMetricsInt = new Paint.FontMetricsInt();
        paint.getFontMetricsInt(fontMetricsInt);

        paddingTop = (fontMetricsInt.top - rect.top);

        // 设置TextView向上的padding (小于0, 即把TextView文本内容向上移动)
        setPadding(getLeftPaddingOffset()
                , paddingTop + getTopPaddingOffset()
                , getRightPaddingOffset()
                , getBottomPaddingOffset());

        String endText = linesText[linesText.length - 1];
        paint.getTextBounds(endText, 0, endText.length(), rect);

        // 再减去最后一行文本的底部空白，得到的就是TextView内容上线贴边的的高度，到达消除文本上下留白的问题
        setMeasuredDimension(getMeasuredWidth()
                , getMeasuredHeight() - (fontMetricsInt.bottom - rect.bottom) / 2);

        if (refreshMeasure) {
            refreshMeasure = false;
            measure(widthspc, heightspc);
        }
    }

    /**
     * 获取每一行的文本内容
     */
    private String[] getLinesText() {

        int start = 0;
        int end = 0;

        String[] texts = new String[getLineCount()];

        String text = getText().toString();

        Layout layout = getLayout();

        for (int i = 0; i < getLineCount(); i++) {
            end = layout.getLineEnd(i);

            String line = text.substring(start, end); //指定行的内容
            start = end;

            texts[i] = line;
        }

        return texts;
    }
}