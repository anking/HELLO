package com.yanhua.core.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 *使用方式
 <com.yuexin.module_common.view.AliIconFontTextView
 android:layout_width="wrap_content"
 android:layout_height="wrap_content"
 android:textColor="@color/color_F42603"
 android:textSize="@dimen/font_size_28"
 android:text="&#xe610;"/>
 */
public class AliIconFontTextView extends AppCompatTextView {

    public AliIconFontTextView(Context context) {
        this(context, null);
    }

    public AliIconFontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AliIconFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public void initView(Context context, AttributeSet attrs) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "iconfont.ttf");
        setTypeface(tf);
        setIncludeFontPadding(false);
    }
}