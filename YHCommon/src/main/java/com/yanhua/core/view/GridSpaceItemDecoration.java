package com.yanhua.core.view;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int mSpanCount;// 横条目数量
    private int mRowSpacing;// 行间距
    private int mColumnSpacing;// 列间距

    /**
     * @param spanCount     列数
     * @param rowSpacing    行间距
     * @param columnSpacing 列间距
     */
    public GridSpaceItemDecoration(int spanCount, int rowSpacing, int columnSpacing) {
        this.mSpanCount = spanCount;
        this.mRowSpacing = rowSpacing;
        this.mColumnSpacing = columnSpacing;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        // 获取view在adapter中的位置
        int position = parent.getChildAdapterPosition(view);

        if (position / mSpanCount == 0) {
            // 第一行不设置top
            outRect.top = 0;
        } else {
            outRect.top = mRowSpacing;
        }
        if (position % mSpanCount == 0) {
            outRect.right = mColumnSpacing;
        } else if (position % mSpanCount == mSpanCount - 1) {
            outRect.left = mColumnSpacing;
        } else {
            outRect.left = mColumnSpacing;
            outRect.right = mColumnSpacing;
        }
    }
}