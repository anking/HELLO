package com.yanhua.core.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yanhua.core.R;

public class EmptyLayout extends LinearLayout implements
        View.OnClickListener {

    //网络加载失败布局
    public static final int NETWORK_ERROR = 1;
    //网络加载中布局
    public static final int NETWORK_LOADING = 2;
    //网络没有数据布局
    public static final int NODATA = 3;
    //隐藏当前布局
    public static final int HIDE_LAYOUT = 4;
    //网络没有数据，开启点击事件，您可以做重新加载
    public static final int NODATA_ENABLE_CLICK = 5;
    //没有登录布局
    public static final int NO_LOGIN = 6;
    //加载失败布局
    public static final int LOAD_FAILD = 7;
    //类似对话框加载布局
    public static final int TRANSPARENT_LOADING = 8;

    //类似对话框加载布局
    public static final int SEARCH_INPUT_STATUS = 9;

    private boolean clickEnable = true;
    private final Context context;
    public ImageView img;
    private OnClickListener listener;
    private int mErrorState;
    private String strNoDataContent = "";
    private int noDataImageResource;
    private TextView tv;
    private TextView tv_error_desc;
    private Button retryBtn;
    private RelativeLayout layout_error;
    private String buttonText = "";

    public EmptyLayout(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public EmptyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_empty_view, this, false);
        layout_error = view.findViewById(R.id.layout_error);
        img = view.findViewById(R.id.img_error_layout);
        tv = view.findViewById(R.id.tv_error_layout);
        tv_error_desc = view.findViewById(R.id.tv_error_desc);
        retryBtn = view.findViewById(R.id.btn_Retry);
        setOnClickListener(this);
        retryBtn.setOnClickListener(v -> {
            if (clickEnable) {
                if (listener != null) {
                    listener.onClick(v);
                }
            }
        });
        addView(view);
    }

    public void dismiss() {
        mErrorState = HIDE_LAYOUT;
        setVisibility(View.GONE);
    }

    public int getErrorState() {
        return mErrorState;
    }

    public boolean isLoadError() {
        return mErrorState == NETWORK_ERROR;
    }

    public boolean isLoading() {
        return mErrorState == NETWORK_LOADING;
    }

    @Override
    public void onClick(View v) {
        if (clickEnable) {
            if (listener != null) {
                listener.onClick(v);
            }
        }
    }

    public void setErrorMessage(String msg) {
        tv.setText(msg);
    }


    public void setErrorMessage(String msg, String desc) {
        tv.setText(msg);
        tv.setVisibility(VISIBLE);
        if (TextUtils.isEmpty(desc)) {
            tv_error_desc.setVisibility(GONE);
        } else {
            tv_error_desc.setVisibility(VISIBLE);
            tv_error_desc.setText(desc);
        }
    }

    /**
     * 新添设置背景
     */
    public void setErrorImag(int imgResource) {
        img.setVisibility(View.VISIBLE);
        img.setImageResource(imgResource);
    }

    public void setErrorType(int i) {
        setVisibility(View.VISIBLE);
        switch (i) {
            case NETWORK_ERROR:
                mErrorState = NETWORK_ERROR;
                tv.setText("您现在断开网络了");
                img.setImageResource(R.drawable.picture_icon_no_data);
                retryBtn.setVisibility(VISIBLE);
                retryBtn.setText("重新刷新页面");
                img.setVisibility(View.VISIBLE);
                setVisibility(VISIBLE);
                clickEnable = true;
                break;
            case NETWORK_LOADING:
                mErrorState = NETWORK_LOADING;
                layout_error.setBackgroundResource(R.color.common1_bg);
                retryBtn.setVisibility(GONE);
                img.setVisibility(View.GONE);
                tv.setText("");
                tv_error_desc.setVisibility(GONE);
                clickEnable = false;
                setVisibility(VISIBLE);
                break;
            case TRANSPARENT_LOADING:
                mErrorState = TRANSPARENT_LOADING;
                layout_error.setBackgroundResource(R.color.transparent);
                retryBtn.setVisibility(GONE);
                img.setVisibility(View.GONE);
                tv.setText("");
                tv_error_desc.setVisibility(GONE);
                clickEnable = false;
                setVisibility(VISIBLE);
                break;
            case SEARCH_INPUT_STATUS:
                mErrorState = SEARCH_INPUT_STATUS;
                retryBtn.setVisibility(GONE);
                img.setVisibility(View.GONE);
                tv.setText("");
                tv_error_desc.setVisibility(GONE);
                layout_error.setBackgroundResource(R.color.black);
                layout_error.setAlpha(0.2f);
                clickEnable = false;
                setVisibility(VISIBLE);
                break;

            case NODATA:
                mErrorState = NODATA;
                img.setVisibility(View.VISIBLE);
                setTvNoDataContent();
                retryBtn.setVisibility(GONE);
                setVisibility(VISIBLE);

                break;
            case LOAD_FAILD:
                mErrorState = LOAD_FAILD;
                img.setImageResource(R.drawable.picture_icon_no_data);
                setFaildImageResource();
                img.setVisibility(View.VISIBLE);
                retryBtn.setVisibility(VISIBLE);
                retryBtn.setText("重新加载");
                tv.setText("加载失败");
                clickEnable = true;
                setVisibility(VISIBLE);
                break;
            case HIDE_LAYOUT:
                setVisibility(View.GONE);
                break;
            case NODATA_ENABLE_CLICK:
                mErrorState = NODATA_ENABLE_CLICK;
                img.setVisibility(View.VISIBLE);
                setTvNoDataContent();
                setButtonShow();
                clickEnable = true;
                setVisibility(VISIBLE);
                break;
            default:
                break;
        }
    }

    public void setNoDataContent(String noDataContent) {
        strNoDataContent = noDataContent;
    }

    public void setNoDataImageResource(int imageResource) {
        noDataImageResource = imageResource;
    }

    public void setButtonText(String txt) {
        this.buttonText = txt;
    }

    public void setOnLayoutClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void setTvNoDataContent() {
        if (!strNoDataContent.equals("")) {
            tv.setText(strNoDataContent);
        } else {
            tv.setText("暂无数据");
        }
        tv.setVisibility(VISIBLE);
    }

    public void setFaildImageResource() {
        if (noDataImageResource != 0) {
            img.setBackgroundResource(noDataImageResource);
        } else {
            img.setBackgroundResource(R.drawable.picture_icon_no_data);
        }
        img.setVisibility(VISIBLE);
    }

    public void setButtonShow() {
        if (!buttonText.equals("")) {
            retryBtn.setText(buttonText);
        } else {
            retryBtn.setText("重新加载");
        }
        retryBtn.setVisibility(VISIBLE);
    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == View.GONE) {
            mErrorState = HIDE_LAYOUT;
        }
        super.setVisibility(visibility);
    }
}
