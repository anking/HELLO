package com.yanhua.core.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class TitleBoldTextView extends AppCompatTextView {
    /**
     * 字体的选择
     */
    private int type;

    public TitleBoldTextView(Context context) {
        this(context, null);
    }

    public TitleBoldTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs, defStyleAttr);
    }

    void initAttrs(Context context, AttributeSet attrs, int defStyleAttr) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "titleBold.ttf");
        setTypeface(tf);
        setIncludeFontPadding(false);
    }
}
