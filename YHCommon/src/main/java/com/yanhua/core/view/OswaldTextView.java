package com.yanhua.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.yanhua.core.R;

/**
 * 三合一
 */
public class OswaldTextView extends AppCompatTextView {
    /**
     * 字体的选择
     */
    private int type;
    public static final int TYPE_REGULAR = 0;
    public static final int TYPE_MEDIUM = 1;
    public static final int TYPE_BOLD = 2;

    public OswaldTextView(Context context) {
        this(context, null);
    }

    public OswaldTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OswaldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs, defStyleAttr);
    }

    void initAttrs(Context context, AttributeSet attrs, int defStyleAttr) {

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OswaldTextView, defStyleAttr, 0);

        type = a.getInt(R.styleable.OswaldTextView_text_type, TYPE_REGULAR);

        a.recycle();

        String fontPath = "OswaldRegular.ttf";//OswaldRegular.ttf OswaldSemiBold.ttf

        switch (type) {
            case TYPE_REGULAR:
                fontPath = "OswaldRegular.ttf";
                break;

            case TYPE_MEDIUM:
                fontPath = "OswaldMedium.ttf";
                break;

            case TYPE_BOLD:
                fontPath = "OswaldSemiBold.ttf";
                break;
        }

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), fontPath);
        setTypeface(tf);
        setIncludeFontPadding(false);
    }
}
