package com.yanhua.core.view;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.core.R;
import com.yanhua.core.util.DisplayUtils;

public class CommonDialog extends CenterPopupView {

    private String content, positiveText, negativeText;
    private TextView btn_cancel, btn_confirm, tv_title;
    private Context mContext;
    private String title;

    private int posiColor;
    private int negColor;
    private View divide;

    private boolean showCountTime;
    private long times;

    public interface OnButtonClickListener {
        void onClick();
    }

    private OnButtonClickListener mConfirmListener;
    private OnButtonClickListener mCancelListener;

    public CommonDialog(@NonNull Context context, String content) {
        super(context);
        this.content = content;
        this.mContext = context;
    }

    public CommonDialog(@NonNull Context context, String content, String positiveText, String negativeText) {
        super(context);
        this.content = content;
        this.positiveText = positiveText;
        this.negativeText = negativeText;
        this.mContext = context;
    }

    public CommonDialog(@NonNull Context context, String content, String positiveText, String negativeText, boolean showCounter, long mil) {
        super(context);
        this.content = content;
        this.positiveText = positiveText;
        this.negativeText = negativeText;
        this.mContext = context;

        showCountTime = showCounter;
        times = mil;
    }

    public CommonDialog(@NonNull Context context, String content, String title, String positiveText, String negativeText) {
        super(context);
        this.content = content;
        this.positiveText = positiveText;
        this.title = title;
        this.negativeText = negativeText;
        this.mContext = context;
    }


    public CommonDialog(@NonNull Context context, String content, String title, String positiveText, String negativeText, int posiColor, int negColor) {
        super(context);
        this.content = content;
        this.positiveText = positiveText;
        this.title = title;
        this.negativeText = negativeText;
        this.mContext = context;
        this.posiColor = posiColor;
        this.negColor = negColor;
    }

    public CommonDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.layout_common_dialog;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_content = findViewById(R.id.tv_content);
        tv_content.setText(content);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_confirm = findViewById(R.id.btn_confirm);
        divide = findViewById(R.id.divide);
        tv_title = findViewById(R.id.tv_title);

        if (TextUtils.isEmpty(title)) {
            tv_title.setVisibility(GONE);
        } else {
            tv_title.setVisibility(VISIBLE);
            tv_title.setText(title);
        }
        if (!TextUtils.isEmpty(positiveText)) {
            btn_confirm.setText(positiveText);
        } else {
            divide.setVisibility(GONE);
            btn_confirm.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(negativeText)) {
            btn_cancel.setText(negativeText);
        } else {
            divide.setVisibility(GONE);
            btn_cancel.setVisibility(GONE);
        }

        if (showCountTime) {
            /**
             * CountDownTimer timer = new CountDownTimer(3000, 1000)中，
             * 第一个参数表示总时间，第二个参数表示间隔时间。
             * 意思就是每隔一秒会回调一次方法onTick，然后1秒之后会回调onFinish方法。
             */
            CountDownTimer timer = new CountDownTimer(times, 1000) {
                public void onTick(long millisUntilFinished) {
                    if (!TextUtils.isEmpty(positiveText)) {
                        btn_confirm.setText(positiveText + "(" + millisUntilFinished / 1000 + "s)");
                    }
                }

                public void onFinish() {
                    if (mConfirmListener != null) {
                        mConfirmListener.onClick();
                    }
                }
            };
//调用 CountDownTimer 对象的 start() 方法开始倒计时，也不涉及到线程处理
            timer.start();
        }

        btn_cancel.setOnClickListener(view -> {
            if (mCancelListener != null) {
                mCancelListener.onClick();
            }
        });

        btn_confirm.setOnClickListener(view -> {
            if (mConfirmListener != null) {
                mConfirmListener.onClick();
            }
        });

        if (posiColor > 0) {
            btn_confirm.setTextColor(this.posiColor);
        }

        if (negColor > 0) {
            btn_cancel.setTextColor(this.negColor);
        }
    }

    @Override
    protected int getMaxWidth() {
        return (int) (DisplayUtils.getScreenWidth(mContext) * 0.8);
    }

    public void setOnConfirmListener(OnButtonClickListener listener) {
        this.mConfirmListener = listener;
    }

    public void setOnCancelListener(OnButtonClickListener listener) {
        this.mCancelListener = listener;
    }

}
