package com.yanhua.core.widget.expandabletext.model;


import com.yanhua.core.widget.expandabletext.app.StatusType;

/**
 * @desc: 为ExpandableTextView添加展开和收回状态的记录
 */
public interface ExpandableStatusFix {
    void setStatus(StatusType status);

    StatusType getStatus();
}
