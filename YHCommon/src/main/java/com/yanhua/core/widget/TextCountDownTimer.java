package com.yanhua.core.widget;

import android.os.CountDownTimer;
import android.widget.TextView;

public class TextCountDownTimer extends CountDownTimer {

    private TextView mTextView;

    private OnTimerFinishListener listener;

    public interface OnTimerFinishListener {
        void onFinish();
    }

    public void setOnTimerFinishListener(OnTimerFinishListener listener) {
        this.listener = listener;
    }

    public TextCountDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.mTextView = textView;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        if (millisUntilFinished / 1000 != 0) {
            mTextView.setText(String.format("跳过 (%ds)", millisUntilFinished / 1000));  //设置倒计时时间
        }
    }

    @Override
    public void onFinish() {
        if (listener != null) {
            listener.onFinish();
        }
    }
}
