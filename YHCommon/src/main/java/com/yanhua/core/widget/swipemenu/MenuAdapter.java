package com.yanhua.core.widget.swipemenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.core.R;
import com.yanhua.core.widget.ItemView;

import java.util.ArrayList;

public class MenuAdapter extends SwipeMenuAdapter<MenuAdapter.DefaultViewHolder> {

    private Context mContext;
    private ArrayList<String> mTCVideoFileInfoList;
    private ItemView.OnDeleteListener mOnDeleteListener;
    public int mBitmapWidth;
    public int mBitmapHeight;
    private int mRemoveIconId;

    public MenuAdapter(Context context, ArrayList<String> fileInfos) {
        mContext = context;
        this.mTCVideoFileInfoList = fileInfos;
    }

    public void removeIndex(int position) {
        this.mTCVideoFileInfoList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mTCVideoFileInfoList.size());
    }

    public void removeAll() {
        this.mTCVideoFileInfoList.clear();
        notifyDataSetChanged();
    }

    public void addItem(String fileInfo) {
        this.mTCVideoFileInfoList.add(fileInfo);
        notifyItemInserted(mTCVideoFileInfoList.size());
    }

    public boolean contains(String fileInfo) {
        return this.mTCVideoFileInfoList.contains(fileInfo);
    }

    public void setOnItemDeleteListener(ItemView.OnDeleteListener onDeleteListener) {
        this.mOnDeleteListener = onDeleteListener;
    }

    @Override
    public int getItemCount() {
        return mTCVideoFileInfoList == null ? 0 : mTCVideoFileInfoList.size();
    }

    public String getItem(int position) {
        return mTCVideoFileInfoList.get(position);
    }

    @Override
    public View onCreateContentView(@NonNull ViewGroup parent, int viewType) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_swipe_menu, parent, false);
    }

    @NonNull
    @Override
    public DefaultViewHolder onCompatCreateViewHolder(@NonNull View realContentView, int viewType) {
        return new DefaultViewHolder(realContentView);
    }

    @Override
    public void onBindViewHolder(@NonNull DefaultViewHolder holder, int position) {
        String fileInfo = mTCVideoFileInfoList.get(position);
        holder.setOnDeleteListener(mOnDeleteListener);
        if (mBitmapHeight != 0 && mBitmapWidth != 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mBitmapWidth, mBitmapHeight);
            holder.ivThumb.setLayoutParams(params);
        }
        Glide.with(mContext).load(fileInfo).into(holder.ivThumb);
    }

    public ArrayList<String> getAll() {
        return mTCVideoFileInfoList;
    }

    static class DefaultViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ShapeableImageView ivThumb;
        TextView tvDuration;
        TextView ivDelete;

        RelativeLayout rlRoot;
        ItemView.OnDeleteListener mOnDeleteListener;

        public DefaultViewHolder(@NonNull View itemView) {
            super(itemView);
            ivThumb = itemView.findViewById(R.id.iv_icon);

            tvDuration = itemView.findViewById(R.id.tv_duration);
            ivDelete = itemView.findViewById(R.id.iv_close);

            rlRoot = itemView.findViewById(R.id.rl_root);

            ivDelete.setOnClickListener(this);
        }

        public void setOnDeleteListener(ItemView.OnDeleteListener onDeleteListener) {
            this.mOnDeleteListener = onDeleteListener;
        }

        public void setDuration(String duration) {
            this.tvDuration.setText(duration);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.iv_close) {
                if (mOnDeleteListener != null) {
                    mOnDeleteListener.onDelete(getAdapterPosition());
                }
            }
        }
    }

    public void setBitmapWidth(int bitmapWidth) {
        mBitmapWidth = bitmapWidth;
    }

    public void setBitmapHeight(int bitmapHeight) {
        mBitmapHeight = bitmapHeight;
    }

    public void setRemoveIconId(int resId) {
        mRemoveIconId = resId;
    }
}
