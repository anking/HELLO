package com.yanhua.core.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.core.R;
import com.yanhua.core.util.DisplayUtils;

/**
 * 提醒样式的弹出层
 */
public class AlertPopup extends CenterPopupView {

    private String title, content, positiveText, negativeText;
    private TextView btn_cancel, btn_confirm;
    private boolean oneBtn = false;
    private Context mContext;

    public interface OnButtonClickListener {
        void onClick();
    }

    private OnButtonClickListener mConfirmListener;
    private OnButtonClickListener mCancelListener;

    public AlertPopup(@NonNull Context context, String content) {
        super(context);
        this.content = content;
        this.mContext = context;
    }

    public AlertPopup(@NonNull Context context, String title, String content, String positiveText, boolean oneBtn) {
        super(context);
        this.title = title;
        this.content = content;
        this.positiveText = positiveText;
        this.oneBtn = oneBtn;
        this.mContext = context;
    }

    public AlertPopup(@NonNull Context context, String content, String positiveText, String negativeText) {
        super(context);
        this.content = content;
        this.positiveText = positiveText;
        this.negativeText = negativeText;
        this.mContext = context;
    }

    public AlertPopup(@NonNull Context context, String title, String content, String positiveText, String negativeText) {
        super(context);
        this.title = title;
        this.content = content;
        this.positiveText = positiveText;
        this.negativeText = negativeText;
        this.mContext = context;
    }

    public AlertPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_alert;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_title = findViewById(R.id.tv_title);
        TextView tv_content = findViewById(R.id.tv_content);
        tv_content.setText(content);
        if (!TextUtils.isEmpty(title)) {
            tv_title.setVisibility(VISIBLE);
            tv_title.setText(title);
            tv_content.setGravity(Gravity.START);
        } else {
            tv_title.setVisibility(GONE);
        }

        btn_cancel = findViewById(R.id.btn_cancel);
        btn_confirm = findViewById(R.id.btn_confirm);
        View midDivideLine = findViewById(R.id.mid_divide);

        if (oneBtn) {
            btn_cancel.setVisibility(GONE);
            btn_confirm.setVisibility(VISIBLE);
            midDivideLine.setVisibility(GONE);
        } else {
            btn_cancel.setVisibility(VISIBLE);
            btn_confirm.setVisibility(VISIBLE);
            midDivideLine.setVisibility(VISIBLE);
        }

        if (!TextUtils.isEmpty(positiveText)) {
            btn_confirm.setText(positiveText);
        }

        if (!TextUtils.isEmpty(negativeText)) {
            btn_cancel.setText(negativeText);
        }

        btn_cancel.setOnClickListener(view -> {
            if (mCancelListener != null) {
                mCancelListener.onClick();
            }
        });

        btn_confirm.setOnClickListener(view -> {
            if (mConfirmListener != null) {
                mConfirmListener.onClick();
            }
        });
    }

    @Override
    protected int getMaxWidth() {
        return (int) (DisplayUtils.getScreenWidth(mContext) * 0.8);
    }

    public void setOnConfirmListener(OnButtonClickListener listener) {
        this.mConfirmListener = listener;
    }

    public void setOnCancelListener(OnButtonClickListener listener) {
        this.mCancelListener = listener;
    }

}
