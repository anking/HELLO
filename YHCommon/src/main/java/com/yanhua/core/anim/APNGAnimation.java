package com.yanhua.core.anim;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.vectordrawable.graphics.drawable.Animatable2Compat;

import com.github.penfeizhou.animation.apng.APNGDrawable;
import com.github.penfeizhou.animation.loader.AssetStreamLoader;

/**
 * apng动画效果
 */
public class APNGAnimation {

    public static void startDoneAnimation(Context context, ImageView srcView, ImageView animView, String assetsName, OnAPNGAnimationListener onLoveAnimationList) {
        AssetStreamLoader assetLoader = new AssetStreamLoader(context, assetsName + "_done.png");

        // Create APNG Drawable
        APNGDrawable apngDrawable = new APNGDrawable(assetLoader);

        // Auto play
        animView.setImageDrawable(apngDrawable);

        // Not needed.default controlled by content
        apngDrawable.setLoopLimit(1);

        // Implement Animatable2Compat
        animView.setVisibility(View.VISIBLE);
        apngDrawable.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationStart(Drawable drawable) {
                super.onAnimationStart(drawable);
                srcView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Drawable drawable) {
                super.onAnimationEnd(drawable);
                srcView.setVisibility(View.VISIBLE);
                animView.setVisibility(View.GONE);

                if (null != onLoveAnimationList) {
                    onLoveAnimationList.onAnimationEnd();
                }
            }
        });
    }

    public static void startRevokeAnimation(Context context, ImageView srcView, ImageView animView, String assetsName, OnAPNGAnimationListener onLoveAnimationList) {
        AssetStreamLoader assetLoader = new AssetStreamLoader(context, assetsName + "_revoke.png");

        // Create APNG Drawable
        APNGDrawable apngDrawable = new APNGDrawable(assetLoader);

        // Auto play
        animView.setImageDrawable(apngDrawable);

        // Not needed.default controlled by content
        apngDrawable.setLoopLimit(1);

        // Implement Animatable2Compat
        animView.setVisibility(View.VISIBLE);
        apngDrawable.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationStart(Drawable drawable) {
                super.onAnimationStart(drawable);
                srcView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Drawable drawable) {
                super.onAnimationEnd(drawable);
                srcView.setVisibility(View.VISIBLE);
                animView.setVisibility(View.GONE);

                if (null != onLoveAnimationList) {
                    onLoveAnimationList.onAnimationEnd();
                }
            }
        });
    }
}
