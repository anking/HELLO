package com.yanhua.core.util.encrypt;

import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

/**
 * from 志诚哥
 */
public class RSACoderUtil {

//    public static String priKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKncwmUTxAxWpkEbigE7Cpfaxky4bB5Bj1dGJ/ykJHpYlLDbggJ27NqBxTGWvXlcn+Ouq2nrGCMAcyEpTglWkxj6U15DvN7XS07XMBF/LW+TrdepCSZoCkU8H6bzRuupNYHQNuVoUS7i42MCwh/vgaxkSockXqw3877QeT0efBlDAgMBAAECgYARa3tgJEvJvCUmri1T/PsUIbKOcadc3vpVR/OX3DmFdoohLRHDMPEPY5mWgVADHzWiguo/XYXHKSelcff3yzIghbFoz9KCD2jJKuVs14Jeh0SnWuwRUeEht59pUn6N7YgEZM5yOUeBcimteYh2DYysmpRUvYprvw+X0T7mFCVdiQJBAOQ6vkKYFHNOFt1NilQWAGd9nxQ7R/ntCZhoibwLfE/nV9MPVpAxF9jp5YSAUEz3/176noBIzX59y9XdUexE4T0CQQC+h+dMoAPLcj71Z+O6faCM7c6h2lCf6wvYsWcc9H8AGgWyPQoTD2rPgJuTb/QMUqsuy06JEuNaYFGjPg2934x/AkEAz6XusxJBEQOMY/8HiyeVaJNv5+1juC2FHGiIYFGm4eAfFMWQwZIZG177VsmTxeOhMd4d1aPna12y1jsOLUQiFQJAZ7cdLrXwOuAe64G5EnxuWCjfkXWA6/yL/1JaugVR2OFRdnwJruR2lSwCBFkt9UNPEfLz9mQExN6nsgAGhPRaywJAHw/pZQHiR7BsaNYg7yfAMlmm4tqlTpbg83DBlb5pXaZXqji2v8ai0B0ctigjJk/kYC4cFDfMqVR/OHetdw+S6Q==";
    public static String pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCp3MJlE8QMVqZBG4oBOwqX2sZMuGweQY9XRif8pCR6WJSw24ICduzagcUxlr15XJ/jrqtp6xgjAHMhKU4JVpMY+lNeQ7ze10tO1zARfy1vk63XqQkmaApFPB+m80brqTWB0DblaFEu4uNjAsIf74GsZEqHJF6sN/O+0Hk9HnwZQwIDAQAB";

    /**
     * 非对称密钥算法
     */
    private static final String KEY_ALGORITHM = "RSA";

    /**
     * 非对称密钥算法
     */
    private static final String CIP_KEY_ALGORITHM = "RSA/NONE/PKCS1Padding";//RSA/NONE/PKCS1Padding
    /**
     * 密钥长度，DH算法的默认密钥长度是1024
     * 密钥长度必须是64的倍数，在512到65536位之间
     */
    private static final int KEY_SIZE = 1024;
    /**
     * 公钥
     */
    private static final String PUBLIC_KEY = "RSAPublicKey";

    /**
     * 私钥
     */
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密明文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /**
     * 初始化密钥对
     *
     * @return Map 甲方密钥的Map
     */
    public static Map<String, Object> initKey() throws Exception {
        //实例化密钥生成器
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        //初始化密钥生成器
        keyPairGenerator.initialize(KEY_SIZE);
        //生成密钥对
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        //甲方公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        //甲方私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        //将密钥存储在map中
        Map<String, Object> keyMap = new HashMap<>(2);
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }


    /**
     * 私钥加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPrivateKey(byte[] data, byte[] key) throws Exception {
        //取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        //数据加密
        Cipher cipher = Cipher.getInstance(CIP_KEY_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);

        // 如果报文长度大于最大长度，则进行分段加密
        byte[] enBytes = null;
        if (data.length > MAX_ENCRYPT_BLOCK) {
            for (int i = 0; i < data.length; i += MAX_ENCRYPT_BLOCK) {
                // 注意要使用2的倍数，否则会出现加密后的内容再解密时为乱码
                byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + MAX_ENCRYPT_BLOCK));
                enBytes = ArrayUtils.addAll(enBytes, doFinal);
            }
        } else {
            enBytes = cipher.doFinal(data);
        }
//        enBytes = cipher.doFinal(data);
        return enBytes;
    }

    /**
     * 公钥加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[] 加密数据
     */
    public static byte[] encryptByPublicKey(byte[] data, byte[] key) throws Exception {

        //实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        //产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

        //数据加密
        Cipher cipher = Cipher.getInstance(CIP_KEY_ALGORITHM);

        cipher.init(Cipher.ENCRYPT_MODE, pubKey);

        // 如果报文长度大于最大长度，则进行分段加密
        byte[] enBytes = null;
        if (data.length > MAX_ENCRYPT_BLOCK) {
            for (int i = 0; i < data.length; i += MAX_ENCRYPT_BLOCK) {
                // 注意要使用2的倍数，否则会出现加密后的内容再解密时为乱码
                byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + MAX_ENCRYPT_BLOCK));
                enBytes = ArrayUtils.addAll(enBytes, doFinal);
            }
        } else {
            enBytes = cipher.doFinal(data);
        }
//        enBytes = cipher.doFinal(data);
        return enBytes;
    }

    /**
     * 私钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPrivateKey(byte[] data, byte[] key) throws Exception {
        //取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //生成私钥
        PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
        //数据解密
        Cipher cipher = Cipher.getInstance(CIP_KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        // 分段解密
        byte[] deBytes = null;
        if (data.length > MAX_DECRYPT_BLOCK) {
            for (int i = 0; i < data.length; i += MAX_DECRYPT_BLOCK) {
                byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + MAX_DECRYPT_BLOCK));
                deBytes = ArrayUtils.addAll(deBytes, doFinal);
            }
        } else {
            deBytes = cipher.doFinal(data);
        }
//        deBytes = cipher.doFinal(data);

        return deBytes;
    }

    /**
     * 公钥解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[] 解密数据
     */
    public static byte[] decryptByPublicKey(byte[] data, byte[] key) throws Exception {
        //实例化密钥工厂
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
        //产生公钥
        PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
        //数据解密
        Cipher cipher = Cipher.getInstance(CIP_KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, pubKey);
        // 分段解密
        byte[] deBytes = null;
        if (data.length > MAX_DECRYPT_BLOCK) {
            for (int i = 0; i < data.length; i += MAX_DECRYPT_BLOCK) {
                byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(data, i, i + MAX_DECRYPT_BLOCK));
                deBytes = ArrayUtils.addAll(deBytes, doFinal);
            }
        } else {
            deBytes = cipher.doFinal(data);
        }

//        deBytes = cipher.doFinal(data);
        return deBytes;
    }

    /**
     * 取得私钥
     *
     * @param keyMap 密钥map
     * @return byte[] 私钥
     */
    public static byte[] getPrivateKey(Map<String, Object> keyMap) {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        return key.getEncoded();
    }

    /**
     * 取得公钥
     *
     * @param keyMap 密钥map
     * @return byte[] 公钥
     */
    public static byte[] getPublicKey(Map<String, Object> keyMap) throws Exception {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        return key.getEncoded();
    }

    /**
     * 公钥加密
     * @param encryptContext
     * @return
     */
    public static String encryptRSA(String encryptContext) {
        String dataRSA = null;

        byte[] publicKey = Base64.decode(pubKey, Base64.DEFAULT);

        try {
            byte[] rsas = RSACoderUtil.encryptByPublicKey(encryptContext.getBytes(StandardCharsets.UTF_8), publicKey);//ISO8859-1

            dataRSA = Base64.encodeToString(rsas, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataRSA;
    }

    public static byte[] decode(String str, int flags) {
        return Base64.decode(str.getBytes(StandardCharsets.UTF_8), flags);
    }

    /**
     * 公钥解密---后台
     * @param decryptContext
     * @return
     */
    public static String decryptRSA(String decryptContext) {
        String dataContent = null;

        byte[] bytes = Base64.decode(decryptContext, Base64.DEFAULT);
        byte[] privateKey = Base64.decode(pubKey, Base64.DEFAULT);

        byte[] contents;
        try {
            contents = RSACoderUtil.decryptByPublicKey(bytes, privateKey);

            dataContent = new String(contents, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataContent;
    }



    public static String decryptRSA(String decryptContext, String prikey) {
        String dataContent = null;

        byte[] bytes = Base64.decode(decryptContext, Base64.DEFAULT);
        byte[] privateKey = Base64.decode(prikey, Base64.DEFAULT);

        byte[] contents;
        try {
            contents = RSACoderUtil.decryptByPrivateKey(bytes, privateKey);

            dataContent = new String(contents, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataContent;
    }

//
//    /**
//     * 生成公钥和秘钥
//     */
//    public static void initRSAKey() throws NoSuchAlgorithmException {
//        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
//        keyGen.initialize(2048);
//        KeyPair keyPair = keyGen.generateKeyPair();
//        PublicKey publicKey = keyPair.getPublic();
//        PrivateKey privateKey = keyPair.getPrivate();
//        String pubKey = BaseEncoding.base64().encode(publicKey.getEncoded());
//        String priKey = BaseEncoding.base64().encode(privateKey.getEncoded());
//        System.out.println("pubKey:\t" + pubKey);
//        System.out.println("priKey:\t" + priKey);
//    }
//
//    /**
//     * 加密
//     *
//     * @param data   加密之前的数据
//     * @param pubKey 公钥
//     * @return 加密之后的数据
//     */
//    public static String encrypt(String data, String pubKey) {
//        String dataRsa = null;
//
//        try {
//            // base64编码的公钥
//            byte[] decoded = BaseEncoding.base64().decode(pubKey);
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            PublicKey publicKey = kf.generatePublic(new X509EncodedKeySpec(decoded));
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//            dataRsa = BaseEncoding.base64().encode(cipher.doFinal(data.getBytes("UTF-8")));
//        } catch (Exception e) {
//
//        }
//
//        return dataRsa;
//    }
//
//    /**
//     * 加密
//     *
//     * @param data 加密之前的数据
//     * @return 加密之后的数据
//     */
//    public static String encrypt(String data) {
//        String dataRsa = null;
//
//        try {
//            // base64编码的公钥
//            byte[] decoded = BaseEncoding.base64().decode(pubKey);
//            KeyFactory kf = KeyFactory.getInstance("RSA");
//            PublicKey publicKey = kf.generatePublic(new X509EncodedKeySpec(decoded));
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//            dataRsa = BaseEncoding.base64().encode(cipher.doFinal(data.getBytes("UTF-8")));
//        } catch (Exception e) {
//
//        }
//
//        return dataRsa;
//    }
//
//
//    /**
//     * 解密
//     *
//     * @param data   解密之前的数据
//     * @param priKey 秘钥
//     * @return 解密之后的数据
//     */
//    public static String decrypt(String data, String priKey) {
//        String dataContent = null;
//        try {
//            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//            KeySpec keySpec = new PKCS8EncodedKeySpec(BaseEncoding.base64().decode(priKey));
//            PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
//            Cipher cipher = Cipher.getInstance("RSA");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            dataContent = new String(cipher.doFinal(BaseEncoding.base64().decode(data)));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return dataContent;
//    }
//
//    /**
//     * 解密
//     *
//     * @param data   解密之前的数据
//     * @return 解密之后的数据
//     */
//    public static String decrypt(String data) {
//        String dataContent = null;
//        try {
//            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//            KeySpec keySpec = new PKCS8EncodedKeySpec(BaseEncoding.base64().decode(priKey));
//            PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
//            Cipher cipher = Cipher.getInstance("RSA");
//            cipher.init(Cipher.DECRYPT_MODE, privateKey);
//            dataContent = new String(cipher.doFinal(BaseEncoding.base64().decode(data)));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return dataContent;
//    }
}
