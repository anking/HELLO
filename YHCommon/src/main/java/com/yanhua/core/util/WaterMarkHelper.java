package com.yanhua.core.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextPaint;
import android.text.format.DateUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WaterMarkHelper {

    public static Bitmap createBitmap(Context context, Bitmap src, Bitmap waterMask, String title) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(newBitmap);
        // 往位图中开始画入src原始图片
        mCanvas.drawBitmap(src, 0, 0, null);
        if (null != waterMask) {
            int ww = waterMask.getWidth();
            int wh = waterMask.getHeight();
            float ratio = (float) (width / 1000.0);
            Matrix matrix = new Matrix();
            matrix.postScale(ratio, ratio);
            Bitmap bitmap = Bitmap.createBitmap(waterMask, 0, 0, ww, wh, matrix, false);
            // 在src的右下角添加水印
            Paint paint = new Paint();
            mCanvas.drawBitmap(bitmap, (float) (width - ww * ratio - 30 * (width / 1000.0)), (float) (height - wh * ratio - 60 * (width / 1000.0)), paint);
        }
        // 开始加入文字
        if (null != title) {
            TextPaint textPaint = new TextPaint();
            //设置字体颜色
            textPaint.setColor(Color.WHITE);
            //设置字体大小
            //根据图片大小设置字体大小
            float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
            textPaint.setTextSize(Math.round((fontScale * 10 + 0.5f) * (width / 1000.0)));
            textPaint.setDither(true); //获取跟清晰的图像采样
            textPaint.setFilterBitmap(true);//过滤一些
            //阴影设置
            textPaint.setShadowLayer(3f, 1, 1, Color.DKGRAY);
            textPaint.setAntiAlias(true);
            Rect bounds = new Rect();
            textPaint.getTextBounds(title, 0, title.length(), bounds);
            mCanvas.drawText(title, (float) (width - bounds.width() - 30 * (width / 1000.0)), height - bounds.height(), textPaint);
        }
        mCanvas.save();
        //保存
        mCanvas.restore();
        return newBitmap;
    }

    public static void saveImageToGallery(Bitmap image, Context context) {
        long timeStamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String sd = sdf.format(new Date(timeStamp));
        String fileName = sd + ".png";
        final ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES
                + File.separator + "yeuxin"); //Environment.DIRECTORY_SCREENSHOTS:截图,图库中显示的文件夹名。"dh"
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
        values.put(MediaStore.MediaColumns.DATE_ADDED, timeStamp / 1000);
        values.put(MediaStore.MediaColumns.DATE_MODIFIED, timeStamp / 1000);
        values.put(MediaStore.MediaColumns.DATE_EXPIRES, (timeStamp + DateUtils.DAY_IN_MILLIS) / 1000);
        values.put(MediaStore.MediaColumns.IS_PENDING, 1);

        ContentResolver resolver = context.getContentResolver();
        final Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        try {
            // First, write the actual data for our screenshot
            try (OutputStream out = resolver.openOutputStream(uri)) {
                if (!image.compress(Bitmap.CompressFormat.PNG, 100, out)) {
                    throw new IOException("Failed to compress");
                }
            }
            // Everything went well above, publish it!
            values.clear();
            values.put(MediaStore.MediaColumns.IS_PENDING, 0);
            values.putNull(MediaStore.MediaColumns.DATE_EXPIRES);
            resolver.update(uri, values, null, null);
        } catch (IOException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                resolver.delete(uri, null);
            }
            e.printStackTrace();
        }
    }
}
