package com.yanhua.core.util.encrypt;

import android.util.Base64;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import Decoder.BASE64Decoder;

/**
 * HR
 */
public class RSAEncrypt {

    public static String priKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKncwmUTxAxWpkEbigE7Cpfaxky4bB5Bj1dGJ/ykJHpYlLDbggJ27NqBxTGWvXlcn+Ouq2nrGCMAcyEpTglWkxj6U15DvN7XS07XMBF/LW+TrdepCSZoCkU8H6bzRuupNYHQNuVoUS7i42MCwh/vgaxkSockXqw3877QeT0efBlDAgMBAAECgYARa3tgJEvJvCUmri1T/PsUIbKOcadc3vpVR/OX3DmFdoohLRHDMPEPY5mWgVADHzWiguo/XYXHKSelcff3yzIghbFoz9KCD2jJKuVs14Jeh0SnWuwRUeEht59pUn6N7YgEZM5yOUeBcimteYh2DYysmpRUvYprvw+X0T7mFCVdiQJBAOQ6vkKYFHNOFt1NilQWAGd9nxQ7R/ntCZhoibwLfE/nV9MPVpAxF9jp5YSAUEz3/176noBIzX59y9XdUexE4T0CQQC+h+dMoAPLcj71Z+O6faCM7c6h2lCf6wvYsWcc9H8AGgWyPQoTD2rPgJuTb/QMUqsuy06JEuNaYFGjPg2934x/AkEAz6XusxJBEQOMY/8HiyeVaJNv5+1juC2FHGiIYFGm4eAfFMWQwZIZG177VsmTxeOhMd4d1aPna12y1jsOLUQiFQJAZ7cdLrXwOuAe64G5EnxuWCjfkXWA6/yL/1JaugVR2OFRdnwJruR2lSwCBFkt9UNPEfLz9mQExN6nsgAGhPRaywJAHw/pZQHiR7BsaNYg7yfAMlmm4tqlTpbg83DBlb5pXaZXqji2v8ai0B0ctigjJk/kYC4cFDfMqVR/OHetdw+S6Q==";
    public static String pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCp3MJlE8QMVqZBG4oBOwqX2sZMuGweQY9XRif8pCR6WJSw24ICduzagcUxlr15XJ/jrqtp6xgjAHMhKU4JVpMY+lNeQ7ze10tO1zARfy1vk63XqQkmaApFPB+m80brqTWB0DblaFEu4uNjAsIf74GsZEqHJF6sN/O+0Hk9HnwZQwIDAQAB";

    /**
     * 密钥长度，DH算法的默认密钥长度是1024
     * 密钥长度必须是64的倍数，在512到65536位之间
     */
    private static final int KEY_SIZE = 1024;
    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 53;

    /**
     * RSA最大解密明文大小
     */
    private static final int MAX_DECRYPT_BLOCK = KEY_SIZE / 8;

    /**
     * 从字符串中加载公钥
     *
     * @param publicKeyStr 公钥数据字符串
     * @throws Exception 加载公钥时产生的异常
     */
    static RSAPublicKey loadPublicKey(String publicKeyStr) throws Exception {
        try {
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] buffer = base64Decoder.decodeBuffer(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("公钥非法");
        } catch (IOException e) {
            throw new Exception("公钥数据内容读取错误");
        } catch (NullPointerException e) {
            throw new Exception("公钥数据为空");
        }
    }

    static RSAPrivateKey loadPrivateKey(String privateKeyStr) throws Exception {
        try {
            BASE64Decoder base64Decoder = new BASE64Decoder();
            byte[] buffer = base64Decoder.decodeBuffer(privateKeyStr);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("私钥非法");
        } catch (IOException e) {
            throw new Exception("私钥数据内容读取错误");
        } catch (NullPointerException e) {
            throw new Exception("私钥数据为空");
        }
    }

    /**
     * 加密
     *
     * @param publicKey
     * @param srcBytes
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    static byte[] encrypt(RSAPublicKey publicKey, byte[] srcBytes) {
        try {
            if (publicKey != null) {
                //Cipher负责完成加密或解密工作，基于RSA
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                //根据公钥，对Cipher对象进行初始化
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//                byte[] resultBytes = cipher.doFinal(srcBytes);
//                return resultBytes;
                // 如果报文长度大于最大长度，则进行分段加密
                byte[] enBytes = null;
                if (srcBytes.length > MAX_ENCRYPT_BLOCK) {
                    for (int i = 0; i < srcBytes.length; i += MAX_ENCRYPT_BLOCK) {
                        // 注意要使用2的倍数，否则会出现加密后的内容再解密时为乱码
                        byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(srcBytes, i, i + MAX_ENCRYPT_BLOCK));
                        enBytes = ArrayUtils.addAll(enBytes, doFinal);
                    }
                } else {
                    enBytes = cipher.doFinal(srcBytes);
                }
                return enBytes;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     *
     * @param privateKey
     * @param srcBytes
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    static byte[] decrypt(RSAPrivateKey privateKey, byte[] srcBytes) {
        try {
            if (privateKey != null) {
                //Cipher负责完成加密或解密工作，基于RSA
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                //根据公钥，对Cipher对象进行初始化
                cipher.init(Cipher.DECRYPT_MODE, privateKey);
//                byte[] resultBytes = cipher.doFinal(srcBytes);
//                return resultBytes;
                // 分段解密
                byte[] deBytes = null;
                if (srcBytes.length > MAX_DECRYPT_BLOCK) {
                    for (int i = 0; i < srcBytes.length; i += MAX_DECRYPT_BLOCK) {
                        byte[] doFinal = cipher.doFinal(ArrayUtils.subarray(srcBytes, i, i + MAX_DECRYPT_BLOCK));
                        deBytes = ArrayUtils.addAll(deBytes, doFinal);
                    }
                } else {
                    deBytes = cipher.doFinal(srcBytes);
                }
                return deBytes;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encryptRSA(String encryptContext) {
        String dataRSA = null;

        try {
            RSAPublicKey publicKey = loadPublicKey(pubKey);

            byte[] bytes = encrypt(publicKey, encryptContext.getBytes());

            dataRSA = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataRSA;
    }

    public static String decryptRSA(String decryptContext) {
        String dataContent = null;

        try {
            RSAPrivateKey privateKey = loadPrivateKey(priKey);

            byte[] bytes = Base64.decode(decryptContext, Base64.DEFAULT);

            byte[] content = decrypt(privateKey, bytes);

            dataContent = new String(content);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataContent;
    }
}
