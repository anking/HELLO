/*
 * Copyright (C) 2010 mAPPn.Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanhua.core.util;

import android.util.Log;

import com.yanhua.core.BuildConfig;


/**
 * Common DateConvertUtils for the application
 */
public class LogUtils {

    public static final boolean sLogShow = BuildConfig.DEBUG;
    public static String sLogTag = "yuexin";

    public static void V(String msg) {
        if (sLogShow && msg != null) {
            Log.v(sLogTag, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (sLogShow && msg != null) {
            Log.v(tag, msg);
        }
    }

    public static void V(String msg, Throwable e) {
        if (sLogShow && msg != null) {
            Log.v(sLogTag, msg, e);
        }
    }

    public static void D(String msg) {
        if (sLogShow && msg != null) {
            Log.d(sLogTag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (sLogShow && msg != null) {
            Log.d(tag, msg);
        }
    }

    public static void D(String msg, Throwable e) {
        if (sLogShow && msg != null) {
            Log.d(sLogTag, msg, e);
        }
    }

    public static void I(String msg) {
        if (sLogShow && msg != null) {
            Log.i(sLogTag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (sLogShow && msg != null) {
            Log.i(tag, msg);
        }
    }

    public static void I(String msg, Throwable e) {
        if (sLogShow && msg != null) {
            Log.i(sLogTag, msg, e);
        }
    }

    public static void W(String msg) {
        if (sLogShow && msg != null) {
            Log.w(sLogTag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (sLogShow && msg != null) {
            Log.w(tag, msg);
        }
    }

    public static void W(String msg, Throwable e) {
        if (sLogShow && msg != null) {
            Log.w(sLogTag, msg, e);
        }
    }

    public static void E(String msg) {
        if (sLogShow && msg != null) {
            Log.e(sLogTag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (sLogShow && msg != null) {
            Log.e(tag, msg);
        }
    }

    public static void E(String msg, Throwable e) {
        if (sLogShow && msg != null) {
            Log.e(sLogTag, msg, e);
        }
    }


}