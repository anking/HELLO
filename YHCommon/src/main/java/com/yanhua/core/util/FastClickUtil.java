package com.yanhua.core.util;

/**
 * 禁止快速点击操作
 */
public class FastClickUtil {
    //两次点击按钮之间的点击间隔不能少于1000毫秒
    public static final int MIN_DURATION_1SECOND_TIME = 1000;
    public static final int MIN_DURATION_2SECOND_TIME = 2000;
    private static final int MIN_CLICK_DELAY_TIME = 6000;
    private static long lastClickTime;

    public static boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }

    public static boolean isFastClick(int minDuration) {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= minDuration) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}
