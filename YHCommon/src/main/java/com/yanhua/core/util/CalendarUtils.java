package com.yanhua.core.util;

import com.blankj.utilcode.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 日期相关工具类
 *
 * @author Administrator
 */
public class CalendarUtils {

    public static String getWeekDay(String dateStr, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        Date date;
        try {
            date = sdf.parse(dateStr);
            cal.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return weekDays[w];
    }

    /**
     * 获取n天内的日期
     *
     * @param day
     * @return
     */
    public static ArrayList<Calendar> getCalendarList(int day) {
        ArrayList<Calendar> list = new ArrayList<>();
        for (int i = 0; i < day; i++) {
            Calendar calendar = Calendar.getInstance(Locale.CHINA);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            list.add(calendar);
        }
        return list;
    }

    /**
     * 根据时间格式化
     *
     * @param date
     * @param format
     * @return
     */
    public static String dateFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String dateStr = sdf.format(date);
        return dateStr;
    }


    /**
     * 解析相关格式的日期
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Date parseDateStr(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr)) {
            return new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    /**
     * 获取Calendar对象
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Calendar getCalendar(String dateStr, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDateStr(dateStr, format));
        return calendar;
    }

    /**
     * 根据选择的日期返回位置
     *
     * @param list
     * @param selectDate
     * @return
     */
    public static int getSelectPosition(ArrayList<Calendar> list, String selectDate) {
        int position = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < list.size(); i++) {
            Calendar calendar = list.get(i);
            String str = sdf.format(calendar.getTime());
            if (str.equals(selectDate)) {
                position = i;
                break;
            }
        }
        return position;
    }


    /**
     * 订单间隔倒计时
     *
     * @param dateStr 下单时间
     * @param format
     * @param count   多少分钟后过期
     * @return
     */
    public static long getMillionSecond(String dateStr, String format, long count) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar startCalendar = Calendar.getInstance();
        Calendar nowCalendar = Calendar.getInstance();
        Date date = null;
        long countdownMillis = 0;
        try {
            date = sdf.parse(dateStr);
            startCalendar.setTime(date);

            long nowTimeMillis = nowCalendar.getTimeInMillis();
            long startTimeMillis = startCalendar.getTimeInMillis();

            long tempTimeMillis = nowTimeMillis - startTimeMillis;
            long countTimeMillis = count * 60 * 1000;
//            LogUtils.D("tempTimeMillis: " + tempTimeMillis);
//            LogUtils.D("countTimeMillis: " + countTimeMillis);
            if (countTimeMillis > tempTimeMillis) {
                countdownMillis = countTimeMillis - tempTimeMillis;
            }
            return countdownMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return countdownMillis;
    }


    /**
     * 获取到当天某个时间的时间戳
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static long getMillionSecond(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = null;
            int year = endCalendar.get(Calendar.YEAR);
            int month = endCalendar.get(Calendar.MONTH) + 1;
            int day = endCalendar.get(Calendar.DAY_OF_MONTH);
            dateStr = year + "-" + (month < 10 ? "0" + month : month) + "-" + day + " " + dateStr;
            date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getEndTimeMillionSecond(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = null;
            date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取当前时间到某个时间的时间戳
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static long getMillionSecondByEndTime(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String handleDurationMinutes(int durationMinutes) {
        int hours = durationMinutes / 60;
        int minutes = durationMinutes % 60;
        return hours + "小时" + minutes + "分钟";
    }

    /**
     * “2016-12-12” 是不是当天，就为“yyyy-MM-dd”，比如需要校验“2016/12/12”的字符串，就为“yyyy/MM/dd”，依次类推即可
     *
     * @param str
     * @param formatStr
     * @return
     */
    public static boolean isToday(String str, String formatStr) {

        SimpleDateFormat format = new SimpleDateFormat(formatStr);

        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        int year1 = c1.get(Calendar.YEAR);
        int month1 = c1.get(Calendar.MONTH) + 1;
        int day1 = c1.get(Calendar.DAY_OF_MONTH);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        int year2 = c2.get(Calendar.YEAR);
        int month2 = c2.get(Calendar.MONTH) + 1;
        int day2 = c2.get(Calendar.DAY_OF_MONTH);
        if (year1 == year2 && month1 == month2 && day1 == day2) {
            return true;
        }
        return false;
    }

    /**
     * 判断日期是否早于今天
     *
     * @param str
     * @param formatStr
     * @return
     */
    public static boolean beforeToday(String str, String formatStr) {
        SimpleDateFormat format = new SimpleDateFormat(formatStr);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        return c1.before(c2);
    }

    /**
     * 计算两个日期相差多少小时多少分钟
     *
     * @param endDate
     * @param nowDate
     * @return
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        hour += day * 24;
        return hour + "时" + min + "分";
    }

    /**
     * 计算两个日期是否相差14天 >14
     * date2比date1多的天数 日期格式yyyy-MM-dd
     */
    public static boolean calDayDiff14(Date date1, Date date2) {
        int calDayDiff = calDayDiff(date1, date2);
        if (calDayDiff > 14) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * date2比date1多的天数 日期格式yyyy-MM-dd
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int calDayDiff(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 != year2) { //不同年
            int timeDistance = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)    //闰年
                {
                    timeDistance += 366;
                } else    //不是闰年
                {
                    timeDistance += 365;
                }
            }
            return timeDistance + (day2 - day1);
        } else {   //同一年
            return day2 - day1;
        }
    }

    /**
     * 计算两个日期相差多少周年（满一年才算一年）
     * date2比date1多的年数 日期格式yyyy-MM-dd
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int calYearDiff(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        // 得到date1时间的年、月、日
        int selectYear = cal1.get(Calendar.YEAR);
        int selectMonth = cal1.get(Calendar.MONTH);
        int selectDay = cal1.get(Calendar.DATE);

        // 得到date2时间的年、月、日
        int yearNow = cal2.get(Calendar.YEAR);
        int monthNow = cal2.get(Calendar.MONTH);
        int dayNow = cal2.get(Calendar.DATE);

        // 用当前年月日减去生日年月日
        int yearMinus = yearNow - selectYear;
        int monthMinus = monthNow - selectMonth;
        int dayMinus = dayNow - selectDay;

        int age = yearMinus;// 先大致赋值
        if (yearMinus <= 0) {// 选了未来的年份或同一年
            age = 0;
        } /*else if (yearMinus > 0) {
            if (monthMinus < 0) {// date2月<date1生日月
                age -= 1;
            } else if (monthMinus == 0) {// 同月份的，再根据日期计算年龄
                if (dayMinus < 0) {
                    age -= 1;
                } else if (dayMinus >= 0) {
                    age = age;
                }
            } else if (monthMinus > 0) {
                age = age;
            }
        }*/
        return age;
    }

    /**
     * 两个日期进行相减
     *
     * @param startDate
     * @param endDate
     * @return 相差多少秒
     */
    public static long minusDate(Date startDate, Date endDate) {
        return (endDate.getTime() - startDate.getTime()) / 1000;
    }

    /**
     * 时间定义（24小时制）
     * <p>
     * 当前手机时间年月日=系统发送消息的年月日，只显示当前时间；
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日=1，显示“昨天 + 消息时间”；
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日=2，显示日期“前天 + 消息时间”
     * <p>
     * 2<当前手机时间年月日-系统发送消息的年月日<=7，显示“星期x + 消息时间”
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日>7，显示“年月日 + 消息时间”
     */
    public static String[] WEEK = {"日", "一", "二", "三", "四", "五", "六"};

    public static String convertTimeToFormat(String dateStr) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date createDate = sdf.parse(dateStr);
            int dDay = calDayDiff(createDate, new Date());// 两个日期相差多少天
            if (dDay == 0) {
                return dateStr.substring(11, 16);
            } else if (dDay == 1) {
                return String.format("昨天 %s", dateStr.substring(11, 16));
            } else if (dDay == 2) {
                return String.format("前天 %s", dateStr.substring(11, 16));
            } else if (dDay <= 7) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(createDate);
                int dayIndex = calendar.get(Calendar.DAY_OF_WEEK);
                if (dayIndex < 1 || dayIndex > 7) {
                    return null;
                }
                return String.format("星期%s %s", WEEK[dayIndex - 1], dateStr.substring(11, 16));
            } else {
                return dateStr.substring(0, 16);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 凌晨时间比较
     */
    public static boolean isEarlyBeforeZero(String departTime, String format) {

        boolean limit = false;
        //2021-09-08 10:00:00 2021-09-09 00:00:00
        SimpleDateFormat sdfYMD = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0); //这是将当天的【秒】设置为0
        calendar.set(Calendar.MINUTE, 0); //这是将当天的【分】设置为0
        calendar.set(Calendar.HOUR_OF_DAY, 0); //这是将当天的【时】设置为0

        calendar.add(Calendar.DATE, 1); //当前日期加一
//
        String currentZeroTime = sdfYMD.format(calendar.getTime()); //

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date departDate = null;
        try {
            departDate = sdf.parse(departTime);

            Calendar calendarZero = Calendar.getInstance();
            calendarZero.setTime(departDate);
            calendarZero.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
            String departTomorrow = sdf.format(calendarZero.getTime()) + " 00:00:00"; //得到明天凌晨的时间

            if (departTomorrow.compareTo(currentZeroTime) < 0) {
                //当天范围内
                limit = true;
            } else {
                limit = false;
            }
        } catch (ParseException e) {
            limit = false;
            e.printStackTrace();
        }

        return limit;
    }

    /**
     * 通过生日计算星座
     *
     * @param birthday 2021-09-02
     *                 星座日期对照：
     *                 * 白羊座：3月21日-4月19日
     *                 * 金牛座：4月20日-5月20日
     *                 * 双子座：5月21日-6月21日
     *                 * 巨蟹座：6月22日-7月22日
     *                 * 狮子座：7月23日-8月22日
     *                 * 处女座：8月23日-9月22日
     *                 * 天秤座：9月23日-10月23日
     *                 * 天蝎座：10月24日-11月22日
     *                 * 射手座：11月23日-12月21日
     *                 * 摩羯座：12月22日-1月19日
     *                 * 水瓶座：1月20日-2月18日
     *                 * 双鱼座：2月19日-3月20日
     * @return
     */
    public static String getConstellation(Date birthday) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthday);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int[] dayArr = new int[]{20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 23, 22};
        String[] constellationArr = new String[]{"摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座",
                "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座"};
        return day < dayArr[month - 1] ? constellationArr[month - 1] : constellationArr[month];
    }
}
