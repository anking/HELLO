package com.yanhua.core.util;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;

/**
 * 价钱特殊化显示控制
 */
public class PriceTextShow {

    /**
     * @param price          价钱
     * @param unitTextSize   单位大小
     * @param intTextSize    整数大小
     * @param doubleTextSize 小数部分大小
     *                       <p>
     *                       eg:unitTextSize = 12 ,int intTextSize = 22 ,int doubleTextSize = 12
     * @return
     */
    public static SpannableString showSepecialPrice(String price, int unitTextSize, int intTextSize, int doubleTextSize) {
        String newCouponPrice = "¥" + price;
        int index = newCouponPrice.indexOf(".");
        if (index == -1) {
            index = newCouponPrice.length();
        }

        SpannableString spannableString = new SpannableString(newCouponPrice);
        spannableString.setSpan(new AbsoluteSizeSpan(unitTextSize, true), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(intTextSize, true), 1, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(doubleTextSize, true), index, newCouponPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    /**
     * @param first          单位前加上前缀
     * @param price          价格
     * @param unitTextSize
     * @param intTextSize
     * @param doubleTextSize
     * @return
     */
    public static SpannableString showSepecialPrice(String first, String price, int unitTextSize, int intTextSize, int doubleTextSize) {
        String newCouponPrice = first + "¥" + price;
        int index = newCouponPrice.indexOf(".");
        if (index == -1) {
            index = newCouponPrice.length();
        }
        int firstLength = first.length();

        SpannableString spannableString = new SpannableString(newCouponPrice);
        spannableString.setSpan(new AbsoluteSizeSpan(unitTextSize, true), 0, firstLength, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(unitTextSize, true), firstLength, firstLength + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(intTextSize, true), firstLength + 1, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(doubleTextSize, true), index, newCouponPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }
}
