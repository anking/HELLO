/**
 * Copyright 2014 Zhenguo Jin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanhua.core.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;

import com.blankj.utilcode.util.ToastUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串操作工具包 结合android.text.TextUtils使用
 */
public final class YHStringUtils {

//    /** * 密码至少包含 数字和英文，长度6-20
//     */
//    String reg = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";

//    /**
//     * 密码包含 数字,英文,字符中的两种以上，长度6-20
//     */
//    String reg = "^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{6,20}$";
//
//    /**
//     * 至少包含数字跟字母，可以有字符
//     */
//    String reg = "(?=.*([a-zA-Z].*))(?=.*[0-9].*)[a-zA-Z0-9-*/+.~!@#$%^&*()]{6,20}$";


    /**
     * 查找字符串中某个特殊字符的个数
     *
     * @param oriStr
     * @param findStr
     * @param count
     * @return
     */
    public static int findStrCount(String oriStr, String findStr, int count) {
        if (oriStr.contains(findStr)) {
            count++;

            count = findStrCount(oriStr.substring(oriStr.indexOf(findStr) + findStr.length()), findStr, count);
        }

        return count;
    }

    /**
     * 特殊符号对
     *
     * @param content
     */
    public static void setTopicText(String content) {
        Pattern p = Pattern.compile("#.*?#");
        Matcher m = p.matcher(content);

//        #333#3344#

        while (m.find()) {
            System.out.println(m.group());
        }
    }

    public static boolean validPassword(String pswd) {
        /** * 密码至少包含 数字和英文，长度6-20
         */
        String reg = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";
        boolean isMatch = Pattern.matches(reg, pswd);
        return isMatch;
    }

    public static String getHtmlContent(String content) {
        String reMatch = "\\s*|\t|\r|\n";
        String regTag = "<[^>]*>";
//        Pattern pattern = Pattern.compile(reMatch);
//        Matcher matcher = pattern.matcher(content);
//        content = matcher.replaceAll( "");

        content = content.replaceAll(reMatch, "").replaceAll(regTag, "");

        return content;
    }

    public static String parseListToString(List<String> list) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <= list.size() - 1; i++) {
            if (i < list.size() - 1) {
                sb.append(list.get(i) + ",");
            } else {
                sb.append(list.get(i));
            }
        }
        return sb.toString();
    }

    /**
     * 手机号脱敏
     *
     * @param phone 11位
     * @return
     */
    public static String phoneDataMasking(String phone) {
        if (TextUtils.isEmpty(phone)) {
            return "";
        } else {
            return String.format("%s****%s", phone.substring(0, 3), phone.substring(phone.length() - 4));
        }
    }

    /**
     * 身份证号脱敏
     *
     * @param
     * @return
     */
    public static String idCardDataMasking(String idCard) {
        return idMask(idCard, 4, 4);
    }

    /**
     * @param
     * @return
     * @首位补充空格
     */
    public static String firstAtAddSpace(String content) {
        content = content.replace("\b", " ");

        if (content.substring(0, 1).equals("@")) {
            content = " " + content;
        }

        return content;
    }


    /**
     * 将字符串除首尾字符外其他字符变为*
     *
     * @param content
     * @return
     */
    public static String hideMiddleStr(String content) {
        if (content.length() < 2) {
            return content + "**";
        }
        if (content.length() < 3) {
            return content.substring(0, 1) + "*" + content.substring(1, 2);
        }
        char[] chars = content.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i != 0 && i != chars.length - 1) {
                chars[i] = '*';
            }
        }
        return String.valueOf(chars);
    }

    public static String idMask(String idCardNum, int front, int end) {
        //身份证不能为空
        if (TextUtils.isEmpty(idCardNum)) {
            return null;
        }
        //需要截取的长度不能大于身份证号长度
        if ((front + end) > idCardNum.length()) {
            return null;
        }
        //需要截取的不能小于0
        if (front < 0 || end < 0) {
            return null;
        }
        //计算*的数量
        int asteriskCount = idCardNum.length() - (front + end);
        StringBuffer asteriskStr = new StringBuffer();
        for (int i = 0; i < asteriskCount; i++) {
            asteriskStr.append("*");
        }
        String regex = "(\\w{" + String.valueOf(front) + "})(\\w+)(\\w{" + String.valueOf(end) + "})";
        return idCardNum.replaceAll(regex, "$1" + asteriskStr + "$3");
    }

    /**
     * 姓名
     *
     * @param
     * @return
     */
    public static String realNameDataMasking(String realName) {
//        if (TextUtils.isEmpty(realName)) {
//            return "";
//        } else {
//            if (realName.length() == 2) {
//                return String.format("%s*", realName.substring(0, 1));
//            } else if (realName.length() > 2) {
//                return String.format("%s**%s", realName.substring(0, 1), realName.substring(3));
//            } else {
//                return String.format("*%s", realName.substring(1));
//            }
//        }
        return hideMiddleStr(realName);
    }

    /**
     * 需要判断的字符串可以用这个
     *
     * @param value
     * @return
     */
    public static String value(String value) {
        if (TextUtils.isEmpty(value)) {
            return "";
        } else {
            return value;
        }
    }

    /**
     * 先获取后者
     *
     * @param first
     * @param last
     * @return
     */
    public static String pickLastFirst(String first, String last) {
        if (!TextUtils.isEmpty(last)) {
            return last;
        }

        if (!TextUtils.isEmpty(first)) {
            return first;
        }
        return "";
    }

    /**
     * @param nickName
     * @param backName
     * @return
     */
    public static String pickName(String nickName, String backName) {
        if (!TextUtils.isEmpty(backName)) {
            return backName;
        }

        if (!TextUtils.isEmpty(nickName)) {
            return nickName;
        }
        return "";
    }

    /**
     * Don't let anyone instantiate this class.
     */
    private YHStringUtils() {
        throw new Error("Do not need instantiate!");
    }

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(CharSequence str) {
        return TextUtils.isEmpty(str);
    }

    /**
     * 字符串转整数
     *
     * @param str
     * @param defValue
     * @return
     */
    public static int toInt(String str, int defValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * byte[]数组转换为16进制的字符串
     *
     * @param data 要转换的字节数组
     * @return 转换后的结果
     */
    public static final String byteArrayToHexString(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length * 2);
        for (byte b : data) {
            int v = b & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase(Locale.getDefault());
    }

    /**
     * 16进制表示的字符串转换为字节数组
     *
     * @param s 16进制表示的字符串
     * @return byte[] 字节数组
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] d = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个进制字节
            d[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
                    .digit(s.charAt(i + 1), 16));
        }
        return d;
    }

    /**
     * 将给定的字符串中所有给定的关键字标红
     *
     * @param sourceString 给定的字符串
     * @param keyword      给定的关键字
     * @return 返回的是带Html标签的字符串，在使用时要通过Html.fromHtml()转换为Spanned对象再传递给TextView对象
     */
    public static String keywordMadeRed(String sourceString, String keyword) {
        String result = "";
        if (sourceString != null && !"".equals(sourceString.trim())) {
            if (keyword != null && !"".equals(keyword.trim())) {
                result = sourceString.replaceAll(keyword,
                        "<font color=\"red\">" + keyword + "</font>");
            } else {
                result = sourceString;
            }
        }
        return result;
    }

    /**
     * 为给定的字符串添加HTML红色标记，当使用Html.fromHtml()方式显示到TextView 的时候其将是红色的
     *
     * @param string 给定的字符串
     * @return
     */
    public static String addHtmlRedFlag(String string) {
        return "<font color=\"red\">" + string + "</font>";
    }

    /**
     * 判断字符中是否包含汉字
     *
     * @param c
     * @return
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        // 4E00-9FBF：CJK 统一表意符号 String expr = "^[\u0391-\uFFE5]+$";
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                //F900-FAFF：CJK 兼容象形文字
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                //3400-4DBF：CJK 统一表意符号扩展 A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                //2000-206F：常用标点
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                //3000-303F：CJK 符号和标点
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                //FF00-FFEF：半角及全角形式
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /**
     * 数字小于10前面补0
     *
     * @param num
     * @return
     */
    public static String firstZero(String num) {
        if (TextUtils.isEmpty(num)) {
            return "00";
        }

        int iNum = Integer.parseInt(num);
        if (iNum < 10) {
            return "0" + num;
        } else {
            return num;
        }
    }

    public static String fitTimeZero(String num) {
        if (TextUtils.isEmpty(num)) {
            return "00:00";
        }
        if ("0".equals(num) || "00".equals(num)) {
            return "00:00";
        }
        int iNum = Integer.parseInt(num);
        if (iNum < 10) {
            return "0" + num + ":00";
        } else {
            return num + ":00";
        }
    }

    public static String quantityEnFormat(int quality) {
        BigDecimal qualityDecimal = new BigDecimal(quality);
        if (quality > 9999999) {
            qualityDecimal = qualityDecimal.divide(new BigDecimal(1000000)).setScale(2, RoundingMode.DOWN);
            return qualityDecimal.doubleValue() + "m";
        } else if (quality > 9999) {
            qualityDecimal = qualityDecimal.divide(new BigDecimal(10000)).setScale(2, RoundingMode.DOWN);
            return qualityDecimal.doubleValue() + "w";
        }
        return quality + "";
    }


    public static String quantityFormat(int quality) {
        BigDecimal qualityDecimal = new BigDecimal(quality);
        if (quality > 999999999) {
            qualityDecimal = qualityDecimal.divide(new BigDecimal(100000000)).setScale(2, RoundingMode.DOWN);
            return qualityDecimal.doubleValue() + "亿";
        } else if (quality > 9999) {
            qualityDecimal = qualityDecimal.divide(new BigDecimal(10000)).setScale(2, RoundingMode.DOWN);
            return qualityDecimal.doubleValue() + "万";
        }
        return quality + "";
    }

    public static String formatCircle(String title) {
        return ("◎" + title);
    }

    public static String formatTopic(String title) {
        return ("#" + title + "#");
    }

    public static void copyContent(Context mActivity, String content) {
        ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(null, content);
        clipboard.setPrimaryClip(clipData);
        ToastUtils.showShort("复制成功");
    }

    //  一月简写是Jan.，
    //二月简写是Feb.，
    //三月简写是Mar.，
    //四月简写是Apr.，
    //五月May英 [meɪ]
    //六月简写是Jun.，June
    //七月简写是Jul.，July
    //八月简写是Aug. ，ugust
    //九月简写是Sep.  ，September
    //十月简写是Oct. ，October
    //十一月简写是Nov.，November
    //十二月简写是Dec.，December
    public static final String MONTH_SHORT_NAME[] = {"Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."};
    public static final String MONTH_CHINA_NAME[] = {"一月", "二月", "三月", "四月", "五月", " 六月", "七月", "八月", "九月", "十月", "十一月", "十二月"};

    public static String monthShortName(int month, boolean showShortName) {
        return showShortName ? MONTH_SHORT_NAME[month] : MONTH_CHINA_NAME[month];
    }


    /**
     * 拼接字符串
     *
     * @param arr
     * @param regex
     * @return
     */
    public static String join(String[] arr, String regex) {
        if (arr.length == 0) return null;
        StringBuilder sb = new StringBuilder();
        for (String str : arr) {
            sb.append(str).append(regex);
        }
        String result = sb.toString();
        return result.substring(0, result.length() - 1);
    }

    public static String formatDistance(int distance) {
        if (distance < 1000) {
            return distance + "m";
        } else if (distance < 10000000) {
            float f = (float) (distance / 1000.0);
            return String.format("%.1fkm", f);
        } else {
            float f = (float) (distance / 10000000.0);
            return String.format("%.1fwkm", f);
        }
    }
}
