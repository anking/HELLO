package com.yanhua.core.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.blankj.utilcode.constant.TimeConstants;
import com.yanhua.core.BuildConfig;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class YXTimeUtils {

    private static final ThreadLocal<Map<String, SimpleDateFormat>> SDF_THREAD_LOCAL
            = new ThreadLocal<Map<String, SimpleDateFormat>>() {
        @Override
        protected Map<String, SimpleDateFormat> initialValue() {
            return new HashMap<>();
        }
    };

    private static SimpleDateFormat getDefaultFormat() {
        return getSafeDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat getSafeDateFormat(String pattern) {
        Map<String, SimpleDateFormat> sdfMap = SDF_THREAD_LOCAL.get();
        //noinspection ConstantConditions
        SimpleDateFormat simpleDateFormat = sdfMap.get(pattern);
        if (simpleDateFormat == null) {
            simpleDateFormat = new SimpleDateFormat(pattern);
            sdfMap.put(pattern, simpleDateFormat);
        }
        return simpleDateFormat;
    }

    public static String getFriendlyTimeSpanByNow(final String time) {
        return getFriendlyTimeSpanByNow(time, getDefaultFormat());
    }

    public static String getFriendlyTimeSpanByNow(final String time,
                                                  @NonNull final DateFormat format) {
        return getFriendlyTimeSpanByNow(string2Millis(time, format));
    }

    /**
     * Formatted time string to the milliseconds.
     *
     * @param time   The formatted time string.
     * @param format The format.
     * @return the milliseconds
     */
    public static long string2Millis(final String time, @NonNull final DateFormat format) {
        try {
            return format.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Return the friendly time span by now.
     *
     * @param millis The milliseconds.
     * @return the friendly time span by now
     * <ul>
     * <li>如果小于 1 秒钟内，显示刚刚</li>
     * <li>如果在 1 分钟内，显示 XXX秒前</li>
     * <li>如果在 1 小时内，显示 XXX分钟前</li>
     * <li>如果在 1 小时外的今天内，显示今天15:32</li>
     * <li>如果是昨天的，显示昨天15:32</li>
     * <li>其余显示，2016-10-15</li>
     * <li>时间不合法的情况全部日期和时间信息，如星期六 十月 27 14:21:20 CST 2007</li>
     * </ul>
     */
    public static String getFriendlyTimeSpanByNow(final long millis) {
        long now = System.currentTimeMillis();
        long span = now - millis;
//        if (span < 0)
//            return String.format("%tc", millis);//时间穿越了
        if (span < 1000) {
            return "刚刚";
        } else if (span < TimeConstants.MIN) {
            return String.format(Locale.getDefault(), "%d秒前", span / TimeConstants.SEC);
        } else if (span < TimeConstants.HOUR) {
            return String.format(Locale.getDefault(), "%d分钟前", span / TimeConstants.MIN);
        }
        // 获取当天 00:00
        long wee = getWeeOfToday();

        boolean sameYear = isSameYear(new Date(millis), new Date(now));

        if (millis >= wee) {
            return String.format(Locale.getDefault(), "%d小时前", span / TimeConstants.HOUR);
        } else if (millis >= wee - TimeConstants.DAY) {
            return String.format("1天前 %tR", millis);
        } else if (millis >= wee - 2 * TimeConstants.DAY) {
            return String.format("2天前 %tR", millis);
        } else if (millis >= wee - 3 * TimeConstants.DAY) {
            return String.format("3天前 %tR", millis);
        } else if (millis >= wee - 4 * TimeConstants.DAY) {
            return String.format("4天前 %tR", millis);
        } else if (millis >= wee - 5 * TimeConstants.DAY) {
            return String.format("5天前 %tR", millis);
        } else if (millis >= wee - 6 * TimeConstants.DAY) {
            return String.format("6天前 %tR", millis);
        } else if (sameYear) {
            return String.format("%tm", millis) + "-" + String.format("%td", millis) + " " + String.format("%tR", millis);//
        } else {
            return String.format("%tF", millis);//
        }
    }

    private static long getWeeOfToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    private static boolean isSameYear(Date d1, Date d2) {

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setFirstDayOfWeek(Calendar.MONDAY);//西方周日为一周的第一天，咱得将周一设为一周第一天
        cal2.setFirstDayOfWeek(Calendar.MONDAY);

        cal1.setTime(d1);
        cal2.setTime(d2);

        int subYear = cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR);

        return subYear == 0;
    }


    /***
     * 获取当前日期距离过期时间的日期差值
     * @param endTime
     * @return
     */
    public static String dateDiff(String endTime) {
        String strTime = null;
        // 按照传入的格式生成一个simpledateformate对象
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
        long nh = 1000 * 60 * 60;// 一小时的毫秒数
        long nm = 1000 * 60;// 一分钟的毫秒数
        long ns = 1000;// 一秒钟的毫秒数
        long diff;
        long day = 0;
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = sd.format(curDate);
        try {
            // 获得两个时间的毫秒时间差异
            diff = sd.parse(endTime).getTime()
                    - sd.parse(str).getTime();
            day = diff / nd;// 计算差多少天
            long hour = diff % nd / nh;// 计算差多少小时
            long min = diff % nd % nh / nm;// 计算差多少分钟
            long sec = diff % nd % nh % nm / ns;// 计算差多少秒
            // 输出结果
            if (day >= 1) {
                strTime = day + "天" + hour + "时";
            } else {
                if (hour >= 1) {
                    strTime = day + "天" + hour + "时" + min + "分";

                } else {
                    if (sec >= 1) {
                        strTime = day + "天" + hour + "时" + min + "分" + sec + "秒";
                    } else {
                        strTime = "显示即将到期";
                    }
                }
            }

            return strTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String inviteTime(String time) {
        String strTime = "";

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        SimpleDateFormat sdC = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分");
        Date date = null;
        try {
            date = sd.parse(time);
            strTime = sdC.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strTime;
    }

    /***
     * 获取当前日期距离过期时间的日期差值
     * @param endTime
     * @return
     */
    public static String inviteDateDiffNow(String endTime) {
        String strTime = null;
        // 按照传入的格式生成一个simpledateformate对象
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
        long nh = 1000 * 60 * 60;// 一小时的毫秒数
        long nm = 1000 * 60;// 一分钟的毫秒数
        long ns = 1000;// 一秒钟的毫秒数
        long diff;
        long day = 0;
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = sd.format(curDate);

        try {
            // 获得两个时间的毫秒时间差异
            diff = sd.parse(endTime).getTime()
                    - sd.parse(str).getTime();
            day = diff / nd;// 计算差多少天
            long hour = diff % nd / nh;// 计算差多少小时
            long min = diff % nd % nh / nm;// 计算差多少分钟
            long sec = diff % nd % nh % nm / ns;// 计算差多少秒
            // 输出结果
            if (day > 2) {
                SimpleDateFormat sdC = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分");
                Date date = sd.parse(endTime);
                strTime = sdC.format(date);
            } else if (day >= 1 && day <= 2) {
                strTime = "距离还有" + day + "天" + hour + "时开始";
            } else {
                if (hour >= 1) {
                    strTime = "距离还有" + hour + "时" + min + "分开始";
                } else {
                    if (sec >= 1) {
                        strTime = "距离还有" + +hour + "时" + min + "分开始";
                    } else {
                        strTime = "即将到期";
                    }
                }
            }

            return strTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获取当前日期距离过期时间的日期差值
     * @param endTime
     * @return
     */
    public static boolean dateDiff(String endTime, int limit) {
        boolean isLimit = false;
        // 按照传入的格式生成一个simpledateformate对象
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss");
        long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
        long nh = 1000 * 60 * 60;// 一小时的毫秒数
        long nm = 1000 * 60;// 一分钟的毫秒数
        long ns = 1000;// 一秒钟的毫秒数
        long diff;
        long day = 0;
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = sd.format(curDate);
        try {
            // 获得两个时间的毫秒时间差异
            diff = sd.parse(endTime).getTime()
                    - sd.parse(str).getTime();
            day = diff / nd;// 计算差多少天
            long hour = diff % nd / nh;// 计算差多少小时
            long min = diff % nd % nh / nm;// 计算差多少分钟
            long sec = diff % nd % nh % nm / ns;// 计算差多少秒
            // 输出结果
            if (day >= 1) {
                isLimit = false;
            } else {
                if (hour >= 1) {
                    isLimit = false;
                } else {
                    if (min > limit) {
                        isLimit = false;
                    } else {
                        isLimit = true;
                    }
                }
            }
        } catch (ParseException e) {
            isLimit = false;
            e.printStackTrace();
        }
        return isLimit;
    }


    /**
     * 内容区域的时间展示方式
     *
     * @param millis The milliseconds.
     * @return the friendly time span by now
     * <ul>
     * <li>如果小于 3分钟内，显示刚刚</li>
     * <li>如果在 1 小时内，显示 XXX分钟前</li>
     *
     * <li>如果在 1 小时外的今天内，显示今天15:32</li>
     * <li>如果是昨天的，显示昨天15:32</li>
     * <li>如果是前天的，显示前天15:32</li>
     * <li>一年以内，显示日期：09-09</li>
     * <li>一年以上，2016-10-15</li>
     * </ul>
     */
    public static String getFriendlyTimeAtContent(final long millis) {
        long now = System.currentTimeMillis();
        long span = now - millis;

        if (span < TimeConstants.MIN * 3) {
            return "刚刚";
        } else if (span < TimeConstants.HOUR) {//3600000
            return String.format(Locale.getDefault(), "%d分钟前", span / TimeConstants.MIN);
        }

        // 获取当天 00:00
        long wee = getWeeOfToday();
        boolean sameYear = isSameYear(new Date(millis), new Date(now));

        if (millis >= wee) {
            return String.format("%tR", millis);
        } else if (millis >= wee - TimeConstants.DAY) {
            return String.format("昨天 %tR", millis);
        } else if (millis >= wee - 2 * TimeConstants.DAY) {
            return String.format("前天 %tR", millis);
        } else if (sameYear) {
            return String.format("%tm", millis) + "-" + String.format("%td", millis);//+ " " + String.format("%tR", millis);//
        } else {
            return String.format("%tF", millis);//
        }
    }

    public static String getFriendlyTimeAtContent(final String time) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        return getFriendlyTimeAtContent(time, getDefaultFormat());
    }

    public static String getFriendlyTimeAtContent(final String time,
                                                  @NonNull final DateFormat format) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        return getFriendlyTimeAtContent(string2Millis(time, format));
    }


    public static String getFriendlyTimeAtContent(final String time, boolean showSameYear) {
        if (TextUtils.isEmpty(time)) {
            return BuildConfig.DEBUG ? getFriendlyTimeAtContent("2022-05-10 22:07:13", getDefaultFormat(), showSameYear) : "";
        }
        return getFriendlyTimeAtContent(time, getDefaultFormat(), showSameYear);
    }

    public static String getFriendlyTimeAtContent(final String time,
                                                  @NonNull final DateFormat format, boolean showSameYear) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        return getFriendlyTimeAtContent(string2Millis(time, format), showSameYear);
    }

    public static String getFriendlyTimeAtContent(final long millis, boolean showSameYear) {
        long now = System.currentTimeMillis();
        long span = now - millis;

        if (span < TimeConstants.MIN * 3) {
            return "刚刚";
        } else if (span < TimeConstants.HOUR) {//3600000
            return String.format(Locale.getDefault(), "%d分钟前", span / TimeConstants.MIN);
        }

        // 获取当天 00:00
        long wee = getWeeOfToday();
        boolean sameYear = isSameYear(new Date(millis), new Date(now));

        if (millis >= wee) {
            return String.format("%tR", millis);
        } else if (millis >= wee - TimeConstants.DAY) {
            return String.format("昨天 %tR", millis);
        } else if (millis >= wee - 2 * TimeConstants.DAY) {
            return String.format("前天 %tR", millis);
        } else if (sameYear) {
            return String.format("%tm", millis) + "月" + String.format("%td", millis) + "日" + (showSameYear ? String.format(" %tR", millis) : "");//+ " " + String.format("%tR", millis);//
        } else {
            return String.format("%tF", millis);//
        }
    }
}
