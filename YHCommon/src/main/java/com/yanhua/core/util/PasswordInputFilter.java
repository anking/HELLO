package com.yanhua.core.util;

import android.text.InputFilter;
import android.widget.EditText;

public class PasswordInputFilter {

    /**
     * 设置密码不可以输入中文
     *
     * @param editText
     */
    public static void setPasswordInputFilter(EditText editText) {

        InputFilter inputFilter = (source, start, end, dest, dstart, dend) -> {
            for (int i = start; i < end; i++) {
                if (YHStringUtils.isChinese(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        };

        editText.setFilters(new InputFilter[]{inputFilter});
    }
}