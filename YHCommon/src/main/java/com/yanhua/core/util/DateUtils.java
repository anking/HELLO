/**
 * Copyright 2014 Zhenguo Jin
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yanhua.core.util;

import android.text.TextUtils;

import com.luck.picture.lib.tools.ValueOf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * 日期工具类
 *
 */
public final class DateUtils {

    /**
     * 日期类型 *
     */
    public static final String yyyyMMDD = "yyyy-MM-dd";
    public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
    public static final String HHmmss = "HH:mm:ss";
    public static final String LOCALE_DATE_FORMAT = "yyyy年M月d日 HH:mm:ss";
    public static final String DB_DATA_FORMAT = "yyyy-MM-DD HH:mm:ss";
    public static final String NEWS_ITEM_DATE_FORMAT = "hh:mm M月d日 yyyy";


    public static String dateToString(Date date, String pattern)
            throws Exception {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static Date stringToDate(String dateStr, String pattern)
            throws Exception {
        return new SimpleDateFormat(pattern).parse(dateStr);
    }

    /**
     * 将Date类型转换为日期字符串
     *
     * @param date Date对象
     * @param type 需要的日期格式
     * @return 按照需求格式的日期字符串
     */
    public static String formatDate(Date date, String type) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(type);
            return df.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将日期字符串转换为Date类型
     *
     * @param dateStr 日期字符串
     * @param type    日期字符串格式
     * @return Date对象
     */
    public static Date parseDate(String dateStr, String type) {
        SimpleDateFormat df = new SimpleDateFormat(type);
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }

    /**
     * 得到年
     *
     * @param date Date对象
     * @return 年
     */
    public static int getYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.YEAR);
    }

    /**
     * 得到月
     *
     * @param date Date对象
     * @return 月
     */
    public static int getMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MONTH) + 1;

    }

    /**
     * 得到日
     *
     * @param date Date对象
     * @return 日
     */
    public static int getDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 转换日期 将日期转为今天, 昨天, 前天, XXXX-XX-XX, ...
     *
     * @param time 时间
     * @return 当前日期转换为更容易理解的方式
     */
    public static String translateDate(Long time) {
        long oneDay = 24 * 60 * 60 * 1000;
        Calendar current = Calendar.getInstance();
        Calendar today = Calendar.getInstance();    //今天

        today.set(Calendar.YEAR, current.get(Calendar.YEAR));
        today.set(Calendar.MONTH, current.get(Calendar.MONTH));
        today.set(Calendar.DAY_OF_MONTH, current.get(Calendar.DAY_OF_MONTH));
        //  Calendar.HOUR——12小时制的小时数 Calendar.HOUR_OF_DAY——24小时制的小时数
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);

        long todayStartTime = today.getTimeInMillis();

        if (time >= todayStartTime && time < todayStartTime + oneDay) { // today
            return "今天";
        } else if (time >= todayStartTime - oneDay && time < todayStartTime) { // yesterday
            return "昨天";
        } else if (time >= todayStartTime - oneDay * 2 && time < todayStartTime - oneDay) { // the day before yesterday
            return "前天";
        } else if (time > todayStartTime + oneDay) { // future
            return "将来某一天";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(time);
            return dateFormat.format(date);
        }
    }

    /**
     * 转换日期 转换为更为人性化的时间
     *
     * @param time 时间
     * @return
     */
    private String translateDate(long time, long curTime) {
        long oneDay = 24 * 60 * 60;
        Calendar today = Calendar.getInstance();    //今天
        today.setTimeInMillis(curTime * 1000);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        long todayStartTime = today.getTimeInMillis() / 1000;
        if (time >= todayStartTime) {
            long d = curTime - time;
            if (d <= 60) {
                return "1分钟前";
            } else if (d <= 60 * 60) {
                long m = d / 60;
                if (m <= 0) {
                    m = 1;
                }
                return m + "分钟前";
            } else {
                SimpleDateFormat dateFormat = new SimpleDateFormat("今天 HH:mm");
                Date date = new Date(time * 1000);
                String dateStr = dateFormat.format(date);
                if (!TextUtils.isEmpty(dateStr) && dateStr.contains(" 0")) {
                    dateStr = dateStr.replace(" 0", " ");
                }
                return dateStr;
            }
        } else {
            if (time < todayStartTime && time > todayStartTime - oneDay) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("昨天 HH:mm");
                Date date = new Date(time * 1000);
                String dateStr = dateFormat.format(date);
                if (!TextUtils.isEmpty(dateStr) && dateStr.contains(" 0")) {

                    dateStr = dateStr.replace(" 0", " ");
                }
                return dateStr;
            } else if (time < todayStartTime - oneDay && time > todayStartTime - 2 * oneDay) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("前天 HH:mm");
                Date date = new Date(time * 1000);
                String dateStr = dateFormat.format(date);
                if (!TextUtils.isEmpty(dateStr) && dateStr.contains(" 0")) {
                    dateStr = dateStr.replace(" 0", " ");
                }
                return dateStr;
            } else {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = new Date(time * 1000);
                String dateStr = dateFormat.format(date);
                if (!TextUtils.isEmpty(dateStr) && dateStr.contains(" 0")) {
                    dateStr = dateStr.replace(" 0", " ");
                }
                return dateStr;
            }
        }
    }

    public static String getWeekDay(String dateStr, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);

        Calendar cal = Calendar.getInstance();
        Date date;
        try {
            date = sdf.parse(dateStr);
            cal.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return weekDays[w];
    }

    /**
     * 获取n天内的日期
     *
     * @param day
     * @return
     */
    public static ArrayList<Calendar> getCalendarList(int day) {
        ArrayList<Calendar> list = new ArrayList<>();
        for (int i = 0; i < day; i++) {
            Calendar calendar = Calendar.getInstance(Locale.CHINA);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            list.add(calendar);
        }
        return list;
    }

    /**
     * 根据时间格式化
     *
     * @param date
     * @param format
     * @return
     */
    public static String dateFormat(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String dateStr = sdf.format(date);
        return dateStr;
    }


    /**
     * 解析相关格式的日期
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Date parseDateStr(String dateStr, String format) {
        if (YHStringUtils.isEmpty(dateStr)) {
            return new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    /**
     * 获取Calendar对象
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static Calendar getCalendar(String dateStr, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDateStr(dateStr, format));
        return calendar;
    }

    /**
     * 根据选择的日期返回位置
     *
     * @param list
     * @param selectDate
     * @return
     */
    public static int getSelectPosition(ArrayList<Calendar> list, String selectDate) {
        int position = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < list.size(); i++) {
            Calendar calendar = list.get(i);
            String str = sdf.format(calendar.getTime());
            if (str.equals(selectDate)) {
                position = i;
                break;
            }
        }
        return position;
    }


    /**
     * 订单间隔倒计时
     *
     * @param dateStr 下单时间
     * @param format
     * @param count   多少分钟后过期
     * @return
     */
    public static long getMillionSecond(String dateStr, String format, long count) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar startCalendar = Calendar.getInstance();
        Calendar nowCalendar = Calendar.getInstance();
        Date date = null;
        long countdownMillis = 0;
        try {
            date = sdf.parse(dateStr);
            startCalendar.setTime(date);

            long nowTimeMillis = nowCalendar.getTimeInMillis();
            long startTimeMillis = startCalendar.getTimeInMillis();

            long tempTimeMillis = nowTimeMillis - startTimeMillis;
            long countTimeMillis = count * 60 * 1000;
//            LogUtils.D("tempTimeMillis: " + tempTimeMillis);
//            LogUtils.D("countTimeMillis: " + countTimeMillis);
            if (countTimeMillis > tempTimeMillis) {
                countdownMillis = countTimeMillis - tempTimeMillis;
            }
            return countdownMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return countdownMillis;
    }


    /**
     * 获取到当天某个时间的时间戳
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static long getMillionSecond(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = null;
            int year = endCalendar.get(Calendar.YEAR);
            int month = endCalendar.get(Calendar.MONTH) + 1;
            int day = endCalendar.get(Calendar.DAY_OF_MONTH);
            dateStr = year + "-" + (month < 10 ? "0" + month : month) + "-" + day + " " + dateStr;
            date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getEndTimeMillionSecond(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = null;
            date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取当前时间到某个时间的时间戳
     *
     * @param dateStr
     * @param format
     * @return
     */
    public static long getMillionSecondByEndTime(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Calendar endCalendar = Calendar.getInstance();
            Calendar nowCalendar = Calendar.getInstance();
            Date date = sdf.parse(dateStr);
            endCalendar.setTime(date);
            long endTimeMillis = endCalendar.getTimeInMillis();
            long nowTimeMillis = nowCalendar.getTimeInMillis();
            return endTimeMillis - nowTimeMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String handleDurationMinutes(int durationMinutes) {
        int hours = durationMinutes / 60;
        int minutes = durationMinutes % 60;
        return hours + "小时" + minutes + "分钟";
    }

    /**
     * “2016-12-12” 是不是当天，就为“yyyy-MM-dd”，比如需要校验“2016/12/12”的字符串，就为“yyyy/MM/dd”，依次类推即可
     *
     * @param str
     * @param formatStr
     * @return
     */
    public static boolean isToday(String str, String formatStr) {

        SimpleDateFormat format = new SimpleDateFormat(formatStr);

        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        int year1 = c1.get(Calendar.YEAR);
        int month1 = c1.get(Calendar.MONTH) + 1;
        int day1 = c1.get(Calendar.DAY_OF_MONTH);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        int year2 = c2.get(Calendar.YEAR);
        int month2 = c2.get(Calendar.MONTH) + 1;
        int day2 = c2.get(Calendar.DAY_OF_MONTH);
        if (year1 == year2 && month1 == month2 && day1 == day2) {
            return true;
        }
        return false;
    }

    /**
     * 判断日期是否早于今天
     * @param str
     * @param formatStr
     * @return
     */
    public static boolean beforeToday(String str, String formatStr) {
        SimpleDateFormat format = new SimpleDateFormat(formatStr);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(new Date());
        return c1.before(c2);
    }

    /**
     * 计算两个日期相差多少小时多少分钟
     *
     * @param endDate
     * @param nowDate
     * @return
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        hour += day * 24;
        return hour + "时" + min + "分";
    }

    /**
     * 计算两个日期是否相差14天 >14
     * date2比date1多的天数 日期格式yyyy-MM-dd
     */
    public static boolean calDayDiff14(Date date1, Date date2) {
        int calDayDiff = calDayDiff(date1, date2);
        if (calDayDiff > 14) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * date2比date1多的天数 日期格式yyyy-MM-dd
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int calDayDiff(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if (year1 != year2) { //不同年
            int timeDistance = 0;
            for (int i = year1; i < year2; i++) {
                if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)    //闰年
                {
                    timeDistance += 366;
                } else    //不是闰年
                {
                    timeDistance += 365;
                }
            }
            return timeDistance + (day2 - day1);
        } else {   //同一年
            return day2 - day1;
        }
    }

    /**
     * 计算两个日期相差多少周年（满一年才算一年）
     * date2比date1多的年数 日期格式yyyy-MM-dd
     *
     * @param date1
     * @param date2
     * @return
     */
    public static int calYearDiff(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        // 得到date1时间的年、月、日
        int selectYear = cal1.get(Calendar.YEAR);
        int selectMonth = cal1.get(Calendar.MONTH);
        int selectDay = cal1.get(Calendar.DATE);

        // 得到date2时间的年、月、日
        int yearNow = cal2.get(Calendar.YEAR);
        int monthNow = cal2.get(Calendar.MONTH);
        int dayNow = cal2.get(Calendar.DATE);

        // 用当前年月日减去生日年月日
        int yearMinus = yearNow - selectYear;
        int monthMinus = monthNow - selectMonth;
        int dayMinus = dayNow - selectDay;

        int age = yearMinus;// 先大致赋值
        if (yearMinus <= 0) {// 选了未来的年份或同一年
            age = 0;
        } else if (yearMinus > 0) {
            if (monthMinus < 0) {// date2月<date1生日月
                age -= 1;
            } else if (monthMinus == 0) {// 同月份的，再根据日期计算年龄
                if (dayMinus < 0) {
                    age -= 1;
                } else if (dayMinus >= 0) {
                    age = age;
                }
            } else if (monthMinus > 0) {
                age = age;
            }
        }
        return age;
    }

    /**
     * 两个日期进行相减
     *
     * @param startDate
     * @param endDate
     * @return 相差多少秒
     */
    public static long minusDate(Date startDate, Date endDate) {
        return (endDate.getTime() - startDate.getTime()) / 1000;
    }

    /**
     * 时间定义（24小时制）
     * <p>
     * 当前手机时间年月日=系统发送消息的年月日，只显示当前时间；
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日=1，显示“昨天 + 消息时间”；
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日=2，显示日期“前天 + 消息时间”
     * <p>
     * 2<当前手机时间年月日-系统发送消息的年月日<=7，显示“星期x + 消息时间”
     * <p>
     * 当前手机时间年月日-系统发送消息的年月日>7，显示“年月日 + 消息时间”
     */
    public static String[] WEEK = {"日", "一", "二", "三", "四", "五", "六"};

    public static String convertTimeToFormat(String dateStr) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date createDate = sdf.parse(dateStr);
            int dDay = calDayDiff(createDate, new Date());// 两个日期相差多少天
            if (dDay == 0) {
                return dateStr.substring(11, 16);
            } else if (dDay == 1) {
                return String.format("昨天 %s", dateStr.substring(11, 16));
            } else if (dDay == 2) {
                return String.format("前天 %s", dateStr.substring(11, 16));
            } else if (dDay <= 7) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(createDate);
                int dayIndex = calendar.get(Calendar.DAY_OF_WEEK);
                if (dayIndex < 1 || dayIndex > 7) {
                    return null;
                }
                return String.format("星期%s %s", WEEK[dayIndex - 1], dateStr.substring(11, 16));
            } else {
                return dateStr.substring(0, 16);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 凌晨时间比较
     */
    public static boolean isEarlyBeforeZero(String departTime, String format) {

        boolean limit = false;
        //2021-09-08 10:00:00 2021-09-09 00:00:00
        SimpleDateFormat sdfYMD = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0); //这是将当天的【秒】设置为0
        calendar.set(Calendar.MINUTE, 0); //这是将当天的【分】设置为0
        calendar.set(Calendar.HOUR_OF_DAY, 0); //这是将当天的【时】设置为0

        calendar.add(Calendar.DATE, 1); //当前日期加一
//
        String currentZeroTime = sdfYMD.format(calendar.getTime()); //

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date departDate = null;
        try {
            departDate = sdf.parse(departTime);

            Calendar calendarZero = Calendar.getInstance();
            calendarZero.setTime(departDate);
            calendarZero.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
            String departTomorrow = sdf.format(calendarZero.getTime()) + " 00:00:00"; //得到明天凌晨的时间

            if (departTomorrow.compareTo(currentZeroTime) < 0) {
                //当天范围内
                limit = true;
            } else {
                limit = false;
            }
        } catch (ParseException e) {
            limit = false;
            e.printStackTrace();
        }

        return limit;
    }


    private static final SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    public static long getCurrentTimeMillis() {
        String timeToString = ValueOf.toString(System.currentTimeMillis());
        return ValueOf.toLong(timeToString.length() > 10 ? timeToString.substring(0, 10) : timeToString);
    }

    /**
     * 判断两个时间戳相差多少秒
     *
     * @param d
     * @return
     */
    public static int dateDiffer(long d) {
        try {
            long l1 = getCurrentTimeMillis();
            long interval = l1 - d;
            return (int) Math.abs(interval);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 时间戳转换成时间格式
     *
     * @param duration
     * @return
     */
    public static String formatDurationTime(long duration) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
    }


    /**
     * 根据时间戳创建文件名
     *
     * @param prefix 前缀名
     * @return
     */
    public static String getCreateFileName(String prefix) {
        long millis = System.currentTimeMillis();
        return prefix + sf.format(millis);
    }

    /**
     * 根据时间戳创建文件名
     *
     * @return
     */
    public static String getCreateFileName() {
        long millis = System.currentTimeMillis();
        return sf.format(millis);
    }

    /**
     * 计算两个时间间隔
     *
     * @param sTime
     * @param eTime
     * @return
     */
    public static String cdTime(long sTime, long eTime) {
        long diff = eTime - sTime;
        return diff > 1000 ? diff / 1000 + "秒" : diff + "毫秒";
    }
}
