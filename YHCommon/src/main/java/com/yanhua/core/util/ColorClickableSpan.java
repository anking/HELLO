package com.yanhua.core.util;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

/**
 * 设置文本点击
 */
public abstract class ColorClickableSpan extends ClickableSpan {
    private int color;

    public ColorClickableSpan(int color) {
        this.color = color;
    }


    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        if (ds != null) {
            ds.setColor(color);
            ds.setUnderlineText(false);
            ds.clearShadowLayer();
        }
    }
}
