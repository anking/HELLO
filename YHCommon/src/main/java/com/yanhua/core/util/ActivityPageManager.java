package com.yanhua.core.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;
import java.util.Stack;

public class ActivityPageManager {

    private static Stack<Activity> activityStack;
    private static Stack<Activity> transactionActivityStack;
    private static ActivityPageManager instance;

    /**
     * constructor
     */
    private ActivityPageManager() {

    }

    /**
     * get the AppManager instance, the AppManager is singleton.
     */
    public static ActivityPageManager getInstance() {
        if (instance == null) {
            instance = new ActivityPageManager();
        }
        return instance;
    }

    /**
     * add Activity to Stack
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.add(activity);
    }

    /**
     * 添加某个事务中的activity到栈中，方便后面统一销毁
     *
     * @param activity
     */
    public void addTransactionActivity(Activity activity) {
        if (transactionActivityStack == null) {
            transactionActivityStack = new Stack<>();
        }
        transactionActivityStack.add(activity);
    }

    /**
     * 销毁事务中指定的某个activity
     *
     * @param activity
     */
    public void removeTransactionActivity(Activity activity) {
        if (transactionActivityStack == null) {
            return;
        }
        if (activity == null) {
            return;
        }
        transactionActivityStack.remove(activity);
    }

    /**
     * 销毁某个事务中的activity
     */
    public void finishTransactionActivity() {
        if (!transactionActivityStack.isEmpty()) {
            for (Activity activity : transactionActivityStack) {
                finishActivity(activity);
            }
        }
    }

    /**
     * remove Activity from Stack
     */
    public void removeActivity(Activity activity) {
        if (activityStack == null) {
            return;
        }
        activityStack.remove(activity);
    }

    /**
     * get current activity from Stack
     */
    public Activity currentActivity() {
        Activity activity = activityStack.lastElement();
        return activity;
    }

    public void finishActivity() {
        Activity activity = activityStack.lastElement();
        finishActivity(activity);
    }

    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    public void finishActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                finishActivity(activity);
            }
        }
    }

    public void finishMiddleActivities() {
        Activity firstElement = activityStack.firstElement();
        Activity lastElement = activityStack.lastElement();
        for (Activity activity : activityStack) {
            if (activity != firstElement && activity != lastElement) {
                activity.finish();
            }
        }
    }

    public void finishAllActivity() {
        for (Activity activity : activityStack) {
            if (null != activity) {
                activity.finish();
            }
        }
        activityStack.clear();
        //杀死该应用进程
//		android.os.Process.killProcess(android.os.Process.myPid());
    }

    public static void unbindReferences(View view) {
        try {
            if (view != null) {
                view.destroyDrawingCache();
                unbindViewReferences(view);
                if (view instanceof ViewGroup) {
                    unbindViewGroupReferences((ViewGroup) view);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private static void unbindViewGroupReferences(ViewGroup viewGroup) {
        int nrOfChildren = viewGroup.getChildCount();
        for (int i = 0; i < nrOfChildren; i++) {
            View view = viewGroup.getChildAt(i);
            unbindViewReferences(view);
            if (view instanceof ViewGroup)
                unbindViewGroupReferences((ViewGroup) view);
        }
        try {
            viewGroup.removeAllViews();
        } catch (Throwable mayHappen) {
            mayHappen.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private static void unbindViewReferences(View view) {
        try {
            view.setOnClickListener(null);
            view.setOnCreateContextMenuListener(null);
            view.setOnFocusChangeListener(null);
            view.setOnKeyListener(null);
            view.setOnLongClickListener(null);
            view.setOnClickListener(null);
        } catch (Throwable mayHappen) {
        }

        // set background to null
        Drawable d = view.getBackground();
        if (d != null) {
            d.setCallback(null);
        }

        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            d = imageView.getDrawable();
            if (d != null) {
                d.setCallback(null);
            }
            imageView.setImageDrawable(null);
            imageView.setBackgroundDrawable(null);
        }

        // destroy WebView
        if (view instanceof WebView) {
            WebView webview = (WebView) view;
            webview.stopLoading();
            webview.clearFormData();
            webview.clearDisappearingChildren();
            webview.setWebChromeClient(null);
            webview.setWebViewClient(null);
            webview.destroyDrawingCache();
            webview.destroy();
            webview = null;
        }

        if (view instanceof ListView) {
            ListView listView = (ListView) view;
            try {
                listView.removeAllViewsInLayout();
            } catch (Throwable mayHappen) {
                mayHappen.printStackTrace();
            }
            view.destroyDrawingCache();
        }
    }

    /**
     * exit System
     *
     * @param context
     */
    public void exit(Context context) {
        exit(context, true);
    }

    /**
     * exit System
     *
     * @param context
     * @param isClearCache
     */
    @SuppressWarnings("deprecation")
    public void exit(Context context, boolean isClearCache) {
        try {
            finishAllActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ActivityManager.RunningTaskInfo taskInfo = tasks.get(0);

            ComponentName topActivity = taskInfo.topActivity;
            ComponentName baseActivity = taskInfo.baseActivity;

            if (!topActivity.getPackageName().equals(context.getPackageName())&&(!baseActivity.getPackageName().equals(context.getPackageName()))) {
                return true;
            }
        }

        return false;
    }
}
