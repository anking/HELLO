package com.yanhua.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

public class NumberUtils {
    /**
     * 根据身份证计算年龄
     *
     * @param idcard
     * @return
     */
    public static Integer idCardToAge(String idcard) {
        Integer selectYear = Integer.valueOf(idcard.substring(6, 10));         //出生的年份
        Integer selectMonth = Integer.valueOf(idcard.substring(10, 12));       //出生的月份
        Integer selectDay = Integer.valueOf(idcard.substring(12, 14));         //出生的日期
        Calendar cal = Calendar.getInstance();
        Integer yearMinus = cal.get(Calendar.YEAR) - selectYear;
        Integer monthMinus = cal.get(Calendar.MONTH) + 1 - selectMonth;
        Integer dayMinus = cal.get(Calendar.DATE) - selectDay;
        Integer age = yearMinus;
        if (yearMinus < 0) {
            age = 0;
        } else if (yearMinus == 0) {
            age = 0;
        } else if (yearMinus > 0) {
            if (monthMinus == 0) {
                if (dayMinus < 0) {
                    age = age - 1;
                }
            } else if (monthMinus > 0) {
                age = age + 1;
            }
        }
        return age;
    }

    /**
     * BigDecimal subtotalDecimal = new BigDecimal(subtotal);
     * tvTotalPrice.setText(BigDecimalUtil.format(totalDecimal));
     * @param b
     * @return
     */
    public static String format(BigDecimal b) {
        b = b.setScale(2, RoundingMode.HALF_UP);
        return b.toString();
    }

}
