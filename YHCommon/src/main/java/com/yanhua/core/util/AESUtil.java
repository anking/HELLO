package com.yanhua.core.util;

import android.text.TextUtils;
import android.util.Base64;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * 加密后：cHDs2Bc3jm8JwaNqnYF02UU+wBplNjwOA/2j8Ox8FOZuAeWR50w9W67i2yfjTrAdZ5zibnddW5aonxHHyUaJ2Q==
 * cHDs2Bc3jm8JwaNqnYF02UU%2BwBplNjwOA%2F2j8Ox8FOZuAeWR50w9W67i2yfjTrAdZ5zibnddW5aonxHHyUaJ2Q%3D%3D
 * 解密后：shopId=1374184955864076290&checkStatus=1&stopStatus=1
 *
 * @description: 加密
 * @author: pengmuhua
 * @create: 2021-03-25 09:31
 */
public class AESUtil {

    public static final String AES = "AES";
    public static final String password = "yuexinguoji2020.";

    public static String urlDecode(String data, String key) {
        String decode = URLDecoder.decode(data.replaceAll("%(?![0-9a-fA-F]{2})", "%25"));
        return ecbDecrypt(decode, key);
    }

    // 加密
    public static String ecbEncrypt(String sSrc, String key) {
        // 判断Key是否为16位
        if (TextUtils.isEmpty(key) || key.length() != 16) {
            LogUtils.E("Key为空null 或 Key长度不是16位");
            return null;
        }

        try {
            byte[] raw = key.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");//"算法/模式/补码方式"
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(sSrc.getBytes(StandardCharsets.UTF_8));
            //此处使用BASE64做转码功能，同时能起到2次加密的作用。
            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 解密
    public static String ecbDecrypt(String data, String key) {
        try {
            // 判断Key是否为16位
            if (TextUtils.isEmpty(key) || key.length() != 16) {
                LogUtils.E("Key为空null 或 Key长度不是16位");
                return null;
            }
            byte[] raw = key.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encrypted1 = Base64.decode(data, Base64.NO_WRAP);
            byte[] original = cipher.doFinal(encrypted1);
            return new String(original, StandardCharsets.UTF_8);
        } catch (Exception e) {
            LogUtils.E("ECB解密失败" + e.getLocalizedMessage());
            return null;
        }
    }

    public static String encrypt(String str, String secretKeyBase) {
        byte[] bytes = encryptAES(str, secretKeyBase);
        String encrypt = parseByte2HexStr(bytes);
        return encrypt;
    }

    public static String decrypt(String encrypt, String secretKeyBase) {
        byte[] bytes = parseHexStr2Byte(encrypt);
        byte[] bytesRes = decryptAES(bytes, secretKeyBase);
        return new String(bytesRes);
    }


    /**
     * @description： AES加密
     * @parameter： str：待加密字符串，secretKeyBase：用于生成密钥的基础字符串
     * @return： 加密字节数组
     **/
    /**
     * 加密
     *
     * @param content  需要加密的内容
     * @param password 加密密码
     * @return
     */
    public static byte[] encryptAES(String content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(AES);
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, AES);
            Cipher cipher = Cipher.getInstance(AES);// 创建密码器
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
            byte[] result = cipher.doFinal(byteContent);
            return result; // 加密
        } catch (Exception e) {
            LogUtils.E("加密失败" + e.getLocalizedMessage());
        }
        return null;
    }


    /**
     * @description： AES解密
     * @parameter： strByteArray：待解密字节数组，
     * @parameter： secretKeyBase：用于生成密钥的基础字符串， 须要注意的是EAS是对称加密，因此secretKeyBase在加密解密时要同样的
     * @return： 解密后字符串
     **/
    public static byte[] decryptAES(byte[] content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(AES);
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, AES);
            Cipher cipher = Cipher.getInstance(AES);// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
            byte[] result = cipher.doFinal(content);
            return result; // 加密
        } catch (Exception e) {
            LogUtils.E("解密失败" + e.getLocalizedMessage());
        }
        return null;
    }


    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr
     * @return
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

}
