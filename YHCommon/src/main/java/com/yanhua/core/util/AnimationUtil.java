package com.yanhua.core.util;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.yanhua.core.R;


public class AnimationUtil {
    public static void rotate(Context context, ImageView imageView) {
        Animation rotateAnimation = AnimationUtils.loadAnimation(context, R.anim.rotate_anim);
        LinearInterpolator lin = new LinearInterpolator();
        rotateAnimation.setInterpolator(lin);
        imageView.startAnimation(rotateAnimation);
    }
}
