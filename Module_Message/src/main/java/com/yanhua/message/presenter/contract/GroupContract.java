package com.yanhua.message.presenter.contract;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupResult;
import com.yanhua.common.model.StsTokenModel;

import java.util.HashMap;
import java.util.List;

public interface GroupContract {
    interface IView extends BaseView {
        default void queryMemberSuccess(List<DiscoGroupMember> model) {
        }

        default void getGroupInfoSuccess(GroupInfo model) {
        }


        default void setGroupInfoSuccess() {
        }

        default void signOutGroupSuccess() {
        }

        default void createGroupSuccess(GroupResult result) {
        }

        default void dealGroupSuccess() {
        }

        /**
         * 获取用户好友列表
         */
        default void handleGetUserFriendSuccess(List<FriendModel> model) {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void commitComplaintSuccess() {
        }

        default void handleCommonReasonList(List<CommonReasonModel> data) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        void commitComplaint(HashMap params);

        /**
         * /rong/cloud/group/queryMember
         * 群聊/查询群成员
         */
        void queryMember(String groupId);

        void getGroupChat(String groupId);

        void signOutGroup(HashMap params);

        void updateGroupOrDismiss(String groupId);

        void setGroupInfo(HashMap params);

        void setGroupSwitch(HashMap params);

        void convertGroupManagerr(HashMap params);

        void setGroupNoticeInfo(HashMap params);

        void createGroup(HashMap params);

        void addMember(HashMap params);

        void deleteMember(HashMap params);

        void getUserFriend(String userId, String nickName, int current, int size);

        void getStsToken();
    }
}
