package com.yanhua.message.presenter;

import android.text.TextUtils;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupResult;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.message.presenter.contract.GroupContract;

import java.util.HashMap;
import java.util.List;


public class GroupPresenter extends MvpPresenter<GroupContract.IView> implements GroupContract.Presenter {

    public void getCommonReasonList(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleCommonReasonList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void commitComplaint(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().addGroupComplaint(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.commitComplaintSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }


    @Override
    public void queryMember(String groupId) {
        addSubscribe(MessageHttpMethod.getInstance().queryGroupMembers(new SimpleSubscriber<HttpResult<List<DiscoGroupMember>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<DiscoGroupMember>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.queryMemberSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, groupId));
    }

    @Override
    public void getGroupChat(String groupId) {
        addSubscribe(MessageHttpMethod.getInstance().getGroupChat(new SimpleSubscriber<HttpResult<GroupInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<GroupInfo> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.getGroupInfoSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, groupId));
    }

    @Override
    public void signOutGroup(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().signOutGroup(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.signOutGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void updateGroupOrDismiss(String groupId) {
        HashMap params = new HashMap<>();
        params.put("groupId", groupId);

        addSubscribe(MessageHttpMethod.getInstance().updateGroupOrDismiss(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.signOutGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getUserFriend(String userId, String nickName, int current, int size) {

        addSubscribe(MomentHttpMethod.getInstance().getFriendUserList(new SimpleSubscriber<HttpResult<ListResult<FriendModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<FriendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<FriendModel> listResult = result.getData();
                        baseView.handleGetUserFriendSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId, nickName, current, size));

    }

    @Override
    public void createGroup(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().addGroup(new SimpleSubscriber<HttpResult<GroupResult>>(baseView, true) {
            @Override
            public void onNext(HttpResult<GroupResult> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.createGroupSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void addMember(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().addMember(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.dealGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void deleteMember(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().deleteMember(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.dealGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }


    public void updateGroupAdminister(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().updateGroupAdminister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.dealGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void convertGroupManagerr(HashMap params) {
        addSubscribe(MessageHttpMethod.getInstance().convertGroupManagerr(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.dealGroupSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    ///rong/cloud/group/getGroupChat
    //群聊/查询群信息/包涵公告
    @Override
    public void setGroupInfo(HashMap params) {//设置群信息
        addSubscribe(MessageHttpMethod.getInstance().setGroupInfo(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.setGroupInfoSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void setGroupSwitch(HashMap params) {//设置群信息
        addSubscribe(MessageHttpMethod.getInstance().setGroupSwitch(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.setGroupInfoSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    //群聊/查询群信息/包涵公告
    @Override
    public void setGroupNoticeInfo(HashMap params) {//设置群信息
        addSubscribe(MessageHttpMethod.getInstance().setGroupNoticeInfo(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.setGroupInfoSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    /**
     * {
     * "groupContent": "string",
     * "groupId": "string",
     * "groupImg": "string",
     * "groupName": "string",
     * "invitation": 0,//此值不传
     * "memberId": "string",
     * "nickName": "string",
     * "showName": 0
     * }
     *
     * @return
     */
    public HashMap buildSetGroupInfo(String groupContent, String groupId, String groupImg, String groupName, String memberId, String nickName, String showName) {
        HashMap params = new HashMap();

        if (null == groupContent) {

        } else {
            params.put("groupContent", groupContent);
        }

        if (!TextUtils.isEmpty(groupId)) {
            params.put("groupId", groupId);
        }
        if (!TextUtils.isEmpty(groupImg)) {
            params.put("groupImg", groupImg);
        }
        if (!TextUtils.isEmpty(groupName)) {
            params.put("groupName", groupName);
        }
        if (!TextUtils.isEmpty(memberId)) {
            params.put("memberId", memberId);
        }
        if (!TextUtils.isEmpty(nickName)) {
            params.put("nickName", nickName);
        }
        if (!TextUtils.isEmpty(showName)) {
            params.put("showName", showName);
        }
        return params;
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

}
