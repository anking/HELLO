package com.yanhua.message.presenter;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgLastModel;
import com.yanhua.common.model.MsgNewFansModel;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.utils.UserManager;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.HashMap;
import java.util.List;

public class MsgPresenter extends MvpPresenter<MsgContract.IView> implements MsgContract.Presenter {

    public void getUserGroups(String userId, String likeName, boolean isSearch) {
        addSubscribe(MessageHttpMethod.getInstance().getUserGroups(new SimpleSubscriber<HttpResult<List<GroupsBean>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<GroupsBean>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        List<GroupsBean> list = result.getData();

                        baseView.handleUserGroupsBeanSuccess(list, isSearch);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId, likeName));
    }


    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleCancelFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }


    public void getInterActSysMsgLastItem() {
        addSubscribe(MessageHttpMethod.getInstance().getInterActMsgLastItem(new SimpleSubscriber<HttpResult<Integer>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Integer> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleInterActMsgLastItem(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    public void dealInteractReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealInteractReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    public void getSysMsgLastItem() {
        addSubscribe(MessageHttpMethod.getInstance().getSysMsgLastItem(new SimpleSubscriber<HttpResult<MsgLastModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MsgLastModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgLastItem(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    public void getBarInteractMsgList(int current, int size, String uid) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        params.put("userId", uid);

        addSubscribe(MessageHttpMethod.getInstance().getBarInteractMsgList(new SimpleSubscriber<HttpResult<ListResult<BarInterActMsgModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BarInterActMsgModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleBarInteractMsyListPage(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }


    public void getSysMsgList(int current, int size) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);

        addSubscribe(MessageHttpMethod.getInstance().getSysMsgList(new SimpleSubscriber<HttpResult<ListResult<MsgSysModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MsgSysModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsyListPage(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void dealReadItem(String id) {
        addSubscribe(MessageHttpMethod.getInstance().dealReadItem(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void getSysMsgOpen(String id) {
        addSubscribe(MessageHttpMethod.getInstance().getSysMsgOpen(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void dealReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    //新增粉丝------start
    public void getNewFansList(int current, int size) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);

        addSubscribe(MessageHttpMethod.getInstance().getNewFansList(new SimpleSubscriber<HttpResult<ListResult<MsgNewFansModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MsgNewFansModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgNewFansList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void dealNewFansUnReadNum() {
        addSubscribe(MessageHttpMethod.getInstance().dealNewFansUnReadNum(new SimpleSubscriber<HttpResult<Integer>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Integer> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgNewFansNum(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    public void dealNewFansReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealNewFansReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    //新增AT 评论分页请求------start
    public void getCommentAtList(int current, int size) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);

        addSubscribe(MessageHttpMethod.getInstance().getCommentAtList(new SimpleSubscriber<HttpResult<ListResult<MsgCommentAtModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MsgCommentAtModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgCommentAtList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void dealCommentAtUnReadNum() {
        addSubscribe(MessageHttpMethod.getInstance().dealCommentAtUnReadNum(new SimpleSubscriber<HttpResult<Integer>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Integer> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgCommentAtNum(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    public void dealCommentAtReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealCommentAtReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    //新增点赞评论分页请求------start
    public void getFabulousList(int current, int size) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);

        addSubscribe(MessageHttpMethod.getInstance().getFabulousList(new SimpleSubscriber<HttpResult<ListResult<MsgFabulousModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MsgFabulousModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsyFabulousListPage(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void dealFabulousReadNum() {
        addSubscribe(MessageHttpMethod.getInstance().dealFabulousReadNum(new SimpleSubscriber<HttpResult<Integer>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Integer> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgFavLikeNum(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    public void dealFabulousReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealFabulousReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    public void getFabulousUserList(int current, int size, int dataType, String id) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        params.put("dataType", dataType);
        params.put("id", id);

        addSubscribe(MessageHttpMethod.getInstance().getFabulousUserList(new SimpleSubscriber<HttpResult<ListResult<MsgUserModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MsgUserModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsyUserListPage(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }


    public void dealInviteReadNum() {
        addSubscribe(MessageHttpMethod.getInstance().dealInviteReadNum(new SimpleSubscriber<HttpResult<Integer>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Integer> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgInviteNum(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }

    public void dealInviteReadAll() {
        addSubscribe(MessageHttpMethod.getInstance().dealInviteReadAll(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    public void dealInviteReadId(String id, int pos) {
        addSubscribe(MessageHttpMethod.getInstance().dealInviteReadId(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleReadedIDSuccess(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void getMessageInviteList(int current, int size) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        params.put("userId", UserManager.getInstance().getUserId());

        addSubscribe(MessageHttpMethod.getInstance().getMessageInviteList(new SimpleSubscriber<HttpResult<ListResult<InviteMsgModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<InviteMsgModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMsgInviteList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }
}
