package com.yanhua.message.presenter;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.RongUserInfo;
import com.yanhua.message.presenter.contract.ChatMessageContract;

import java.util.HashMap;
import java.util.List;

public class ChatMessagePresenter extends MvpPresenter<ChatMessageContract.IView> implements ChatMessageContract.Presenter {

    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }

    public void getUserGroups(String userId, String likeName, boolean isSearch) {
        addSubscribe(MessageHttpMethod.getInstance().getUserGroups(new SimpleSubscriber<HttpResult<List<GroupsBean>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<GroupsBean>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        List<GroupsBean> list = result.getData();

                        baseView.handleUserGroupsBeanSuccess(list, isSearch);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId, likeName));
    }


    @Override
    public void getContentUserDetail(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().getContentUserDetail(new SimpleSubscriber<HttpResult<ContentUserInfoModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ContentUserInfoModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ContentUserInfoModel model = result.getData();
                    baseView.handleContentUserDetailSuccess(model);
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void followChatUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void getChatContentUserDetail(String userId) {//此接口返回的用户信息是融云那边对应的简单信息（头像、昵称）
        addSubscribe(MessageHttpMethod.getInstance().getRongIMUserInfo(new SimpleSubscriber<HttpResult<RongUserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<RongUserInfo> bean) {
                super.onNext(bean);
                if (bean != null) {
                    if (bean.getCode().equals(HttpCode.SUCCESS)) {
                        RongUserInfo userInfo = bean.getData();
                        userInfo.setUid(userId);
                        baseView.handleRongCloudUserInfo(userInfo);
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void getGroupChat(String groupId) {
        addSubscribe(MessageHttpMethod.getInstance().getGroupChat(new SimpleSubscriber<HttpResult<GroupInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<GroupInfo> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.getGroupInfoSuccess(result.getData());
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, groupId));
    }

    @Override
    public void queryMember(String groupId) {
        addSubscribe(MessageHttpMethod.getInstance().queryGroupMembers(new SimpleSubscriber<HttpResult<List<DiscoGroupMember>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<DiscoGroupMember>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.queryMemberSuccess(result.getData());
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, groupId));
    }

    @Override
    public void queryShopService(String storeId, String userId) {
//        addSubscribe(MsgHttpMethod.getInstance().queryShopService(new SimpleSubscriber<HttpResult<MsgUserInfoModel>>(baseView, true) {
//            @Override
//            public void onNext(HttpResult<MsgUserInfoModel> result) {
//                super.onNext(result);
//                String code = result.getCode();
//                if (code.equals(HttpCode.SUCCESS)) {
//                    baseView.handleQueryShopServiceSuccess(result.getData());
//                } else {
//                    baseView.handleErrorMsg(result.getMesg());
//                }
//            }
//        }, storeId, userId));
    }

    @Override
    public void addSession(HashMap<String, Object> params) {
//        addSubscribe(MsgHttpMethod.getInstance().addSession(new SimpleSubscriber<HttpResult>(baseView, false) {
//            @Override
//            public void onNext(HttpResult result) {
//                super.onNext(result);
//                if (result.getCode().equals(HttpCode.SUCCESS)) {
//                    baseView.handleSuccessAddSession();
//                } else {
//                    baseView.handleErrorMsg(result.getMesg());
//                }
//            }
//        }, params));
    }

    @Override
    public void addOrCancelBlack(String userId) {
        addSubscribe(MessageHttpMethod.getInstance().addBlack(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateBlackUserSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void toManualCustomer(HashMap<String, Object> params) {
//        addSubscribe(MsgHttpMethod.getInstance().toManualCustomer(new SimpleSubscriber<HttpResult<ManualCustomerModel>>(baseView, false) {
//            @Override
//            public void onNext(HttpResult<ManualCustomerModel> result) {
//                super.onNext(result);
//                if (result.getCode().equals(HttpCode.SUCCESS)) {
//                    baseView.handleSuccessToManual(result.getData());
//                } else {
//                    baseView.handleFailToManual(result.getMesg());
//                }
//            }
//        }, params));
    }
}
