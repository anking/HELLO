package com.yanhua.message.presenter.contract;


import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.model.MsgCountModel;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgLastModel;
import com.yanhua.common.model.MsgNewFansModel;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.model.MsgUserModel;

import java.util.List;

public interface MsgContract {

    interface IView extends BaseView {
        /**
         * 获取赞\收藏成功
         */
        default void handleMsgFavLikeListSuccess() {
        }

        /**
         * 获取新增粉丝
         */
        default void handleMsgNewFollowListSuccess() {
        }


        /**
         * 获取新增评论
         */
        default void handleMsgBeCommentedListSuccess() {
        }


        /**
         * 获取新增邀约
         */
        default void handleMsgInviteListSuccess() {
        }


        default void handleGetMsgCountSuccess(MsgCountModel model) {
        }

        /**
         * 关注成功
         */
        default void handleFollowUserSuccess() {
        }

        /**
         * 取消关注成功
         */
        default void handleCancelFollowUserSuccess() {
        }

        default void handleReadedSuccess() {
        }

        default void handleMsyListPage(ListResult<MsgSysModel> data) {
        }

        default void handleMsgLastItem(MsgLastModel msg) {
        }

        default void handleMsgNewFansList(ListResult<MsgNewFansModel> data) {
        }

        default void handleMsgNewFansNum(Integer unreadNum) {
        }

        default void handleMsgFavLikeNum(Integer data) {
        }

        default void handleMsgCommentAtNum(Integer data) {
        }

        default void handleMsgCommentAtList(ListResult<MsgCommentAtModel> data) {
        }

        default void handleMsyUserListPage(ListResult<MsgUserModel> data) {
        }

        default void handleMsyFabulousListPage(ListResult<MsgFabulousModel> data) {
        }

        default void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean isSearch) {
        }

        default void handleInterActMsgLastItem(int data) {
        }

        default void handleBarInteractMsyListPage(ListResult<BarInterActMsgModel> data) {
        }

        default void handleMsgInviteNum(Integer data){}

        default void handleMsgInviteList(ListResult<InviteMsgModel> data){}

        default void handleReadedIDSuccess(int pos){}
    }

    interface Presenter extends BasePresenter<IView> {

    }
}
