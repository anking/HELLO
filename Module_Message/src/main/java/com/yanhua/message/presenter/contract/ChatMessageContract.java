package com.yanhua.message.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.RongUserInfo;

import java.util.HashMap;
import java.util.List;

public interface ChatMessageContract {

    interface IView extends BaseView {

        default void updateFollowUserSuccess() {
        }

        default void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        }

        default void queryMemberSuccess(List<DiscoGroupMember> model) {
        }

        default void getGroupInfoSuccess(GroupInfo model) {
        }

        default void handleSuccessAddSession() {

        }

        default void updateBlackUserSuccess() {

        }

        default void handleFailToManual(String msg) {

        }

        default void handleSuccessGoOnWait() {

        }

        default void handleSuccessLineUpNum(int num) {
        }

        default void handleSuccessFinishQueue() {
        }

        default void handleSuccessFinishSession() {
        }

        /**
         * 请求错误
         *
         * @param msg
         */
        default void handleErrorMsg(String msg) {
        }

        default void handleRongCloudUserInfo(RongUserInfo userInfo){}

        default void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean isSearch){}
    }

    interface Presenter extends BasePresenter<IView> {
        void cancelFollowUser(String userId);

        void getContentUserDetail(String userId);

        void followUser(String userId);

        void getChatContentUserDetail(String userId);

        void followChatUser(String userId);

        void getGroupChat(String groupId);


        /**
         * 根据商铺ID,查询客户信息
         *
         * @param storeId 商铺Id
         */
        void queryShopService(String storeId, String userId);

        void addSession(HashMap<String, Object> params);

        void queryMember(String targetId);

        void addOrCancelBlack(String userId);

        void toManualCustomer(HashMap<String, Object> params);

    }

}
