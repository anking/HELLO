package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.FavLikeUserAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 赞和收藏详情页
 *
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_FAVLIKE_DETAIL_ACTIVITY)
public class FavLikeDetailActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {
    @BindView(R2.id.tv_content)
    TextView tvContent;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_comment)
    RecyclerView rvContent;
    @BindView(R2.id.tv_type)
    TextView tv_type;
    @BindView(R2.id.iv_cover)
    ImageView ivCover;
    @BindView(R2.id.tvDate)
    TextView tvDate;

    @BindView(R2.id.rl_to_detail)
    RelativeLayout rl2Detail;// 是否跳转到详情

    @Autowired
    MsgFabulousModel praiseDetail;

    private FavLikeUserAdapter mAdapter;
    private List<MsgUserModel> records = new ArrayList<>();

    private int total = 0;
    private int current = 1;
    private int size = 10;

    private int mPos = -1;

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_favlike_detail;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        //dataType//数据类型(1:赞,2;收藏)
        int dataType = praiseDetail.getDataType();
        setTitle(dataType == 1 ? "赞过的人" : "收藏的人");
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new FavLikeUserAdapter(mContext);
        rvContent.setAdapter(mAdapter);

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getDataList(current, size);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (records.size() < total) {
                current++;
                getDataList(current, size);
            } else {
                refreshLayout.finishLoadMore(1000);
            }
        });

        String createdTime = praiseDetail.getContentCreatedTime();// 此處需要獲取評論時間
        tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

        //封面缩略图处理
        String coverImage = praiseDetail.getContentCoverUrl();
        if (!TextUtils.isEmpty(coverImage)) {
            ImageLoaderUtil.loadImgCenterCrop(mContext, ivCover, coverImage, R.drawable.place_holder, 0);
            ivCover.setVisibility(View.VISIBLE);
        } else {
            ivCover.setVisibility(View.GONE);
        }

        String content = praiseDetail.getContentContent();
        tvContent.setText(YHStringUtils.getHtmlContent(content));

        //dataType//数据类型(1:赞,2;收藏)
        int dataType = praiseDetail.getDataType();
        if (dataType == 1) {
            //点赞列表
            tv_type.setText("点赞列表");
        } else {
            //收藏列表
            tv_type.setText("收藏列表");
        }

        mAdapter.setOnSelectClickListener(pos -> {
            MsgUserModel itemObject = mAdapter.getItemObject(pos);
            int status = itemObject.getRelationState();
            mPos = pos;
            if (status == 2 || status == 4) {
                final AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    basePresenter.cancelFollowUser(itemObject.getUserId());
                });
                alertPopup.setOnCancelListener(() -> alertPopup.dismiss());
            } else {
                basePresenter.followUser(itemObject.getUserId());
            }
        });
    }

    private void getDataList(int current, int size) {
        int dataType = praiseDetail.getDataType();
        String id = praiseDetail.getId();

        basePresenter.getFabulousUserList(current, size, dataType, id);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        refreshLayout.autoRefresh();
    }

    @Override
    public void handleFollowUserSuccess() {
        MsgUserModel itemObject = mAdapter.getItemObject(mPos);
        int status = itemObject.getRelationState();
////            1 未关注 2 已关注 3 被关注 4互相关注
        if (status == 1) {
            itemObject.setRelationState(2);
        }
        if (status == 3) {
            itemObject.setRelationState(4);
        }

        mAdapter.notifyItemChanged(mPos);
        ToastUtils.showShort("关注成功");
    }

    @Override
    public void handleCancelFollowUserSuccess() {
        MsgUserModel itemObject = mAdapter.getItemObject(mPos);
        int status = itemObject.getRelationState();
        ////1 未关注 2 已关注 3 被关注 4互相关注
        if (status == 2) {
            itemObject.setRelationState(1);
        }
        if (status == 4) {
            itemObject.setRelationState(3);
        }

        mAdapter.notifyItemChanged(mPos);

        ToastUtils.showShort("取关成功");
    }

    @Override
    public void handleMsyUserListPage(ListResult<MsgUserModel> model) {
        if (current == 1) {
            records.clear();
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (model != null && CollectionUtils.isNotEmpty(model.getRecords())) {
            refreshLayout.setVisibility(View.VISIBLE);
            total = model.getTotal();
            // 处理日期
            records.addAll(model.getRecords());
            mAdapter.setItems(records);
        } else {
            if (current == 1) {
                refreshLayout.setVisibility(View.GONE);
            }
        }
    }
}
