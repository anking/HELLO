package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.dmingo.optionbarview.OptionBarView;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 群管理
 */
@Route(path = ARouterPath.GROUP_MANAGE_ACTIVITY)
public class GroupManageActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;

    @BindView(R2.id.obvGroupConvert)
    OptionBarView obvGroupConvert;

    @BindView(R2.id.obvSetGroupManager)
    OptionBarView obvSetGroupManager;


    @Autowired(name = "selectedTeamMembers")
    public ArrayList<String> mSelectedTeamMemberAccounts;
    @Autowired(name = "groupID")
    public String groupId = "";
    @Autowired(name = "currentGroupName")
    public String currentGroupName = "";
    @Autowired(name = "convertFriends")
    public ArrayList<FriendModel> convertFriends;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_manage;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("群管理");

        obvGroupConvert.setOnClickListener(view -> {
            ARouter.getInstance().build(ARouterPath.CREATE_GROUP_ACTIVITY)
                    .withSerializable("selectedTeamMembers", mSelectedTeamMemberAccounts)
                    .withString("groupID", groupId)
                    .withString("currentGroupName", currentGroupName)
                    .withSerializable("convertFriends", convertFriends)
                    .withString("type", IM.group.GROUP_MANAGE_CONVERT).navigation();

            this.finish();
        });

        obvSetGroupManager.setOnClickListener(view -> {
            ARouter.getInstance().build(ARouterPath.GROUP_MANAGE_SET_ACTIVITY)
                    .withSerializable("selectedTeamMembers", mSelectedTeamMemberAccounts)
                    .withString("groupID", groupId)
                    .withString("currentGroupName", currentGroupName)
                    .withSerializable("convertFriends", convertFriends).navigation();

            this.finish();
        });
    }
}
