package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;
import com.yanhua.message.view.NoticeAlertPopup;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.IMCenter;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.MentionedInfo;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 群主进入修改公告，非群主只能查看
 */
@Route(path = ARouterPath.GROUP_NOTICE_ACTIVITY)
public class GroupNoticeActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.et_modify)
    EditText modifyContent;
    @BindView(R2.id.ll_bottom_desc)
    LinearLayout bottomDesc;
    @BindView(R2.id.ll_group_owner)
    LinearLayout llGroupOwner;
    @BindView(R2.id.rl_look)
    RelativeLayout rlLook;
    @BindView(R2.id.btnRight)
    TextView btRight;
    @BindView(R2.id.tv_publish_time)
    TextView tvPublishTime;
    @BindView(R2.id.civ_head)
    CircleImageView civ_head;
    @BindView(R2.id.tv_publish_time_desc)
    TextView tvPublishTimeDesc;
    @BindView(R2.id.tv_nickName)
    TextView tv_nickName;

    @Autowired(name = "groupOwner")
    public String groupOwner = "";//群主

    @Autowired(name = "groupID")
    public String groupID = "";
    @Autowired(name = "noticeContent")
    public String noticeContent = "";

    @Autowired(name = "noticeContentTime")
    public String noticeContentTime = "";

    @Autowired(name = "master")
    public DiscoGroupMember master;

    private boolean isGroupOwner;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_notice;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        isGroupOwner = groupOwner.equals(IM.group.GROUP_OWNER);

        modifyContent.setHighlightColor(mContext.getResources().getColor(R.color.color_3395ff));

        setTitle("群公告");//

        if (isGroupOwner) {
            if (TextUtils.isEmpty(noticeContentTime)) {
                tvPublishTime.setVisibility(View.GONE);
            } else {
                tvPublishTime.setVisibility(View.VISIBLE);
                tvPublishTime.setText("发布时间：" + noticeContentTime);
            }

            llGroupOwner.setVisibility(View.VISIBLE);
            rlLook.setVisibility(View.GONE);
            bottomDesc.setVisibility(View.GONE);
            btRight.setVisibility(View.VISIBLE);
            modifyContent.setEnabled(true);

            if (TextUtils.isEmpty(noticeContent)) {
                btRight.setText("发布");
                btRight.setEnabled(false);
                btRight.setTextColor(mContext.getResources().getColor(R.color.mainWord));
                btRight.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_r2_disable));
            } else {
                btRight.setText("编辑");
                modifyContent.setEnabled(false);
                btRight.setTextColor(mContext.getResources().getColor(R.color.mainWord));
                btRight.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_r2_disable));
            }
        } else {
            llGroupOwner.setVisibility(View.GONE);
            rlLook.setVisibility(View.VISIBLE);
            bottomDesc.setVisibility(View.VISIBLE);
            btRight.setVisibility(View.GONE);
            modifyContent.setEnabled(false);

            if (null == master) {

            } else {
                String masterUrl = master.getUserImg();
                String nickName = YHStringUtils.pickName(master.getNickName(), master.getFriendRemark());

                if (TextUtils.isEmpty(noticeContentTime)) {
                    tvPublishTimeDesc.setVisibility(View.GONE);
                } else {
                    tvPublishTimeDesc.setVisibility(View.VISIBLE);
                    tvPublishTimeDesc.setText(noticeContentTime);
                }
                tv_nickName.setText(nickName);

                Glide.with(mContext).load(masterUrl).centerCrop().into(civ_head);
            }
        }

        modifyContent.setText(noticeContent);

        modifyContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String content = charSequence.toString().trim();
                if (content.length() > 0) {
                    btRight.setEnabled(true);
                    btRight.setTextColor(mContext.getResources().getColor(R.color.white));
                    btRight.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_r2_theme));
                } else {
                    if (isDelete) {
                        return;
                    }
                    btRight.setEnabled(false);
                    btRight.setTextColor(mContext.getResources().getColor(R.color.mainWord));
                    btRight.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_r2_disable));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void setGroupInfoSuccess() {
        //还需要做些什么//TODO

        MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("100"));
        EventBus.getDefault().post(event);


        // 获取群公告
        String content = modifyContent.getText().toString().trim();

        if (TextUtils.isEmpty(content)) {
            ToastUtils.showShort("公告已清除");

            onBackPressed();
            return;
        }

        ToastUtils.showShort("修改成功");

        MentionedInfo mentionedInfo = new MentionedInfo(MentionedInfo.MentionedType.ALL, null, null);
        TextMessage messageContent = TextMessage.obtain("@所有人 " + content);
        messageContent.setMentionedInfo(mentionedInfo);
        Message message = Message.obtain(groupID, Conversation.ConversationType.GROUP, messageContent);
        IMCenter.getInstance().sendMessage(message, null, null, new IRongCallback.ISendMessageCallback() {
            @Override
            public void onAttached(Message message) {

            }

            @Override
            public void onSuccess(Message message) {

            }

            @Override
            public void onError(Message message, RongIMClient.ErrorCode errorCode) {

            }
        });

        onBackPressed();
    }


    private boolean isDelete;

    @OnClick({R2.id.btnRight})
    public void showCommitDialog(View view) {
        if ("编辑".equals(btRight.getText().toString())) {
            isDelete = true;
            btRight.setText("发布");
            btRight.setEnabled(false);
            btRight.setTextColor(mContext.getResources().getColor(R.color.mainWord));
            btRight.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_r2_disable));

            KeyboardUtils.showSoftInput();

            modifyContent.requestFocus();
            modifyContent.setFocusable(true);
            modifyContent.setFocusableInTouchMode(true);
            modifyContent.setEnabled(true);

            modifyContent.postDelayed(new Runnable() {
                @Override
                public void run() {
                    modifyContent.setSelection(noticeContent.length());
                }
            }, 1000);
            return;
        }

        String content = modifyContent.getText().toString().trim();

        if ((null == content || "".equals(content)) && (null != noticeContent && noticeContent.equals(content))) {
            onBackPressed();
        } else if ((null == content || "".equals(content)) && null != noticeContent) {
            NoticeAlertPopup noticeAlertPopup = new NoticeAlertPopup(this, "", "确认清空群公告?", "确定");
            new XPopup.Builder(mContext).asCustom(noticeAlertPopup).show();
            noticeAlertPopup.setOnConfirmListener(() -> {
                //提交
                commitInfo("");//提交
            });
        } else {
            NoticeAlertPopup noticeAlertPopup = new NoticeAlertPopup(this, "", "发布后将在群内通知其他成员，全体成员可见，确认发布?", "确定");
            new XPopup.Builder(mContext).asCustom(noticeAlertPopup).show();
            noticeAlertPopup.setOnConfirmListener(() -> {
                //提交
                commitInfo(content);//提交
            });
        }
    }

    private void commitInfo(String content) {
        HashMap params = basePresenter.buildSetGroupInfo(content, groupID, "", "", "", "", "");
        basePresenter.setGroupNoticeInfo(params);
    }
}
