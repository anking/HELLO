package com.yanhua.message.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.GridImageAdapter;
import com.yanhua.common.imagepick.FullyGridLayoutManager;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.widget.TypeWayBottomPopup;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * 群组信息页面->投诉
 */
@Route(path = ARouterPath.GROUP_COMPLAINT_ACTIVITY)
public class GroupComplaintActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {
    @BindView(R2.id.tv_reason_type)
    TextView tvReasonType;
    @BindView(R2.id.ll_reason_type)
    LinearLayout llReasonType;
    @BindView(R2.id.et_reason)
    EditText etReason;
    @BindView(R2.id.btn_commit)
    Button btnCommit;
    @BindView(R2.id.tv_content_num)
    TextView tv_content_num;

    @BindView(R2.id.recycler)
    RecyclerView mRecyclerView;

    private GridImageAdapter mAdapter;
    private int maxSelectNum = 3;

    @Autowired(name = "groupID")
    public String groupID = "";

    @Autowired(name = "groupName")
    public String groupName = "";

    private List<String> describeFile;
    private List<FileResult> mComplaintUrl;

    private List<File> fileList;
    private int updateIndex;
    private static final int CHOOSE_LOGO = 0x005;
    private String reason;
    private List<TypeWayModel> mTypeList;
    private TypeWayModel mSelectType;

    private StsTokenModel stsTokenModel;

    private MyHandler mHandler = new MyHandler();

    private final static int UPLOAD_FILE_FAIL = 1000;
    private final static int UPLOAD_FILE_SUCCESS = 1001;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    if (updateIndex < fileList.size()) {
                        uploadFile(fileList.get(updateIndex));
                    } else {
                        hideLoading();
                        applyCommit();
                    }
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_complaint;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("举报");

        mTypeList = new ArrayList<>();

        etReason.setMaxLines(Integer.MAX_VALUE);
        etReason.setHorizontallyScrolling(false);

        etReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int inputLength = charSequence.length();
                tv_content_num.setText(inputLength + "/200");

                addListenEdit();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // 用户输入完毕后
            }
        });

        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
        mAdapter = new GridImageAdapter(this, onAddPicClickListener);

        mAdapter.setSelectMax(maxSelectNum);
        mRecyclerView.setAdapter(mAdapter);

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String reason = etReason.getText().toString().trim();
        btnCommit.setEnabled(!TextUtils.isEmpty(reason) && null != mSelectType);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        basePresenter.getStsToken();

        HashMap<String, Object> params = new HashMap<>();
        params.put("current", 1);
        params.put("size", 100);
        params.put("type", 7);//
        basePresenter.getCommonReasonList(params);///sysadmin/app/baseConfig/list
    }

    private final static String TAG = "PictureSelectorTag";
    private final GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            checkStoragePermission();
        }
    };


    /**
     * 返回结果回调
     */
    private static class ImagePickResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;

        public ImagePickResultCallback(GridImageAdapter adapter) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }


    @OnClick({R2.id.ll_reason_type, R2.id.btn_commit})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.ll_reason_type) {

            TypeWayBottomPopup popup = new TypeWayBottomPopup(mContext, "投诉选择", mTypeList);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();

                mSelectType = bean;
                tvReasonType.setText(bean.getName());
            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.btn_commit) {
            //测试闲着快速点击
            if (FastClickUtil.isFastClick()) {
                toCommit();
            }
        }
    }

    private void checkStoragePermission() {
        String tipRight = "设备的储存、相机";
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                new PermissionListener() {
                    @Override
                    public void permissionGranted(@NonNull String[] permission) {
                        // 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                        PickImageUtil.chooseImage(GroupComplaintActivity.this, PictureMimeType.ofImage(), true, maxSelectNum, mAdapter.getData(), new ImagePickResultCallback(mAdapter));
                    }

                    @Override
                    public void permissionDenied(@NonNull String[] permission) {
                        ToastUtils.showShort(R.string.denied_storage_permission);
                    }
                });
    }

    /**
     * 提交申请
     */
    @SuppressLint("AutoDispose")
    private void toCommit() {
        List<LocalMedia> listData = mAdapter.getData();

        reason = etReason.getText().toString().trim();
        if (mSelectType == null) {
            ToastUtils.showShort("请选择反馈类型");
            return;
        }

        if (TextUtils.isEmpty(reason)) {

            ToastUtils.showShort("请描述反馈问题的具体原因");
            return;
        }
        if (listData.isEmpty()) {
            ToastUtils.showShort("为了您更好的解决问题，请上传图片作为有效凭证");
            return;
        }

        fileList = new ArrayList<>();
        describeFile = new ArrayList<>();
        mComplaintUrl = new ArrayList<>();

        updateIndex = 0;
        hideSoftKeyboard();
        basePresenter.addSubscribe(Flowable.just(listData)
                .observeOn(Schedulers.io())
                .map(list -> {
                    for (LocalMedia item : list) {
                        try {
                            if (item != null) {
                                File file = new File(item.getRealPath());
                                fileList.add(file);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return fileList;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                    }
                })
                .subscribe(list -> {
                    showLoading();
                    uploadFile(list.get(updateIndex));
                })
        );
    }

    private void uploadFile(File file) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                describeFile.add(httpUrl);

                mComplaintUrl.add(fileResult);

                updateIndex++;
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    private void applyCommit() {
        HashMap params = new HashMap();

        params.put("complaintReasonId", mSelectType.getId());//投诉原因id
        params.put("groupId", groupID);
        params.put("remark", reason);
        params.put("complaintUrl", mComplaintUrl);

        basePresenter.commitComplaint(params);
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);

        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    @Override
    public void commitComplaintSuccess() {
        ToastUtils.showShort("举报成功");

        onBackPressed();//返回
    }

    //    mTypeList
    @Override
    public void handleCommonReasonList(List<CommonReasonModel> data) {
        if (null != data) {
            if (data.size() > 0) {
                mTypeList.clear();
                //轉成通用的的
                for (int i = 0; i < data.size(); i++) {
                    CommonReasonModel item = data.get(i);
                    mTypeList.add(new TypeWayModel(item.getId(), item.getContent()));
                }
            }
        }
    }
}
