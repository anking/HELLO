package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.MsgSystemListAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_SYSTEM_ACTIVITY)
public class MsgSystemActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    private MsgSystemListAdapter mAdapter;
    private int total;
    private List<MsgSysModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_msg_system_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("系统消息");

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(r -> getListData(1));

        refreshLayout.setOnLoadMoreListener(r -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MsgSystemListAdapter(mContext);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<MsgSysModel> list = mAdapter.getmDataList();
            MsgSysModel item = list.get(pos);

            /*detailId	//内容详情id
            jumpType	//跳转类型(0:无,1:打开页面,2:URL,3:显示内容)
            type
            */
            /*通知类型(
                    1:系统通知,2:此刻通知,3:约伴通知,
                    4:评论通知,5:圈子通知,6:爆料/攻略-授权通知,
                    7:爆料/攻略通知
                    9:爆料/攻略-授权通知,
                    10:爆料/攻略通知,11:问答通知,12:举报通知,
                    13:暂停发布通知,14:封禁通知,15:群投诉通知*/
            String detailId = item.getDetailId();
            int jumpType = item.getJumpType();
            int type = item.getType();

            switch (jumpType) {
                case 1:
                    //打开页面
                    if (TextUtils.isEmpty(detailId)) {
                        return;
                    }
                    basePresenter.getSysMsgOpen(item.getId());
                    break;
                case 2:
                    //URL
                    basePresenter.getSysMsgOpen(item.getId());
                    ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                            .withString("title", item.getTitle())
                            .withString("url", item.getJumpContent())
                            .navigation();
                    break;
                case 3:
                    //显示内容
                    basePresenter.getSysMsgOpen(item.getId());

                    ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                            .withString("title", item.getTitle())
                            .withString("content", item.getJumpContent())
                            .navigation();
                    break;
                default:
                    break;
            }

        });

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);
    }

    private void getListData(int page) {
        current = page;

        basePresenter.getSysMsgList(current, size);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        basePresenter.dealReadAll();
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }

    @Override
    public void handleMsyListPage(ListResult<MsgSysModel> listResult) {
        List<MsgSysModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}