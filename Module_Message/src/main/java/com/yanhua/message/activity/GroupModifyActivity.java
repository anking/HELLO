package com.yanhua.message.activity;

import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.userinfo.RongUserInfoManager;
import io.rong.imlib.model.Group;

/**
 * 修改群昵称和自己的昵称
 */
@Route(path = ARouterPath.GROUP_MODIFY_ACTIVITY)
public class GroupModifyActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.tvModifyDesc)
    TextView tvModifyDesc;
    @BindView(R2.id.iv_head)
    CircleImageView iv_head;
    @BindView(R2.id.et_modify_name)
    AutoClearEditText etModifyName;
    @BindView(R2.id.btnCommit)
    Button btnCommit;
    @BindView(R2.id.tvModifyTitle)
    TextView tvModifyTitle;

    @Autowired(name = "modifyType")
    public String modifyType = "";

    @Autowired(name = "groupID")
    public String groupID = "";

    @Autowired(name = "nickName")
    public String nickName = "";
    @Autowired(name = "modifyUrl")
    public String modifyUrl = "";
    private boolean isModifyGroup;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_modify;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        isModifyGroup = modifyType.equals(IM.group.MODIFY_GROUP_NAME);
        tvModifyTitle.setText(isModifyGroup ? "修改群名称" : "修改我在群的昵称");//

        tvModifyDesc.setText(isModifyGroup ? "修改群名称后，将在群内通知其他成员。" : "昵称修改后，只会在此群内显示，群内成员都可以看见。");////
        etModifyName.setHint(isModifyGroup ? "请输入群组名称（2-10字）" : "请输入群组名称（2-10字）");

        etModifyName.setText(nickName);//设置

        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            if (source.equals(" ") || end > 10) {
                return "";
            } else {
                return null;
            }
        };

        etModifyName.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});

        etModifyName.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit(false)));
        addListenEdit(true);

        //设置头像
        iv_head.setEnabled(isModifyGroup);

        //修改头像显示
        ImageLoaderUtil.loadImgHeadCenterCrop(iv_head, modifyUrl, R.drawable.place_holder);
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit(boolean isFirst) {
        String modifyName = etModifyName.getText().toString().trim();
        if (isFirst) {
            nickName = modifyName;
        }


        btnCommit.setEnabled(!TextUtils.isEmpty(modifyName) && !nickName.equals(modifyName));
    }


    @Override
    public void setGroupInfoSuccess() {
        String mofifyNickName = etModifyName.getText().toString();//修改后的群名称

        if (isModifyGroup) {
            MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("100", mofifyNickName));
            EventBus.getDefault().post(event);

            ToastUtils.showShort("修改群昵称成功");

            Uri uri = Uri.parse(modifyUrl);
            Group group = new Group(groupID, mofifyNickName, uri);
            RongUserInfoManager.getInstance().refreshGroupInfoCache(group);//刷新
        } else {
            MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("1000", mofifyNickName));
            EventBus.getDefault().post(event);

            ToastUtils.showShort("修改昵称成功");
        }
        //还需要做些什么//TODO
        onBackPressed();
    }

    @OnClick({R2.id.btnCommit})
    public void onViewCommit(View view) {
        if (view.getId() == R.id.btnCommit) {
            commitInfo();//提交
        }
    }

    private void commitInfo() {
        String inputNickName = etModifyName.getText().toString().trim();

        if (null == inputNickName || "".equals(inputNickName) || nickName.equals(inputNickName)) {
            if (TextUtils.isEmpty(inputNickName)) {

                ToastUtils.showShort("请输入2-10字符");
            }
            onBackPressed();
        } else {
            if (!TextUtils.isEmpty(inputNickName) && inputNickName.length() < 2) {
                ToastUtils.showShort("请输入2-10字符");
                return;
            }

            HashMap params = isModifyGroup ? basePresenter.buildSetGroupInfo(null, groupID, "", inputNickName, "", "", "")
                    : basePresenter.buildSetGroupInfo(null, groupID, "", "", mMMKV.getUserId(), inputNickName, "");
            basePresenter.setGroupInfo(params);
        }
    }
}
