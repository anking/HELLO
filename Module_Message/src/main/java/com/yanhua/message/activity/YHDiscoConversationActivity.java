package com.yanhua.message.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.presenter.ChatMessagePresenter;
import com.yanhua.message.presenter.contract.ChatMessageContract;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.callkit.RongCallKit;
import io.rong.imkit.RongIM;
import io.rong.imkit.conversation.ConversationFragment;
import io.rong.imkit.conversation.MessageListAdapter;
import io.rong.imkit.userinfo.RongUserInfoManager;
import io.rong.imkit.userinfo.model.GroupUserInfo;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Group;
import io.rong.imlib.model.UserInfo;

public class YHDiscoConversationActivity extends BaseMvpActivity<ChatMessagePresenter> implements ChatMessageContract.IView, Observer {
    private Conversation.ConversationType mType;//会话列表页面标题，如果传空，会显示为默认标题 “会话列表”。
    private String mTargetId;//接收方 ID
    private boolean mDisableSystemEmoji;
    private boolean isGroup;

    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;

    @BindView(R2.id.ll_disable_tip)
    LinearLayout llDisableTip;

    @BindView(R2.id.tv_relation_desc)
    TextView tvRelation_desc;
    @BindView(R2.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R2.id.rl_follow)
    RelativeLayout rl_follow;
    @BindView(R2.id.btn_chat_follow)
    Button btnFollow;


    ArrayList<String> membersID = new ArrayList<>();
    ArrayList<UserInfo> userInfos = new ArrayList<>();

    private ConversationFragment conversationFragment;

    @Override
    protected void creatPresent() {
        basePresenter = new ChatMessagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_yhdisco_conversation;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        // 添加会话界面
        conversationFragment = new ConversationFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, conversationFragment);
        transaction.commit();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        RongIM.getInstance().enableUnreadMessageIcon(true);//显示未读消息数目---控件的样式可以在 layout/rc_fr_messagelist.xml 中进行调整。
        RongIM.getInstance().enableNewComingMessageIcon(true);//显示新消息提醒---控件的样式可以在 layout/rc_fr_messagelist.xml 中进行调整。

        //首先都展示、后面有逻辑控制
        iconMore.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        if (null != intent) {
            String typeName = intent.getStringExtra(RouteUtils.CONVERSATION_TYPE);
            mTargetId = intent.getStringExtra(RouteUtils.TARGET_ID);
            mType = RouteUtils.getConversationType(typeName);

            if (UserManager.getInstance().isMyself(mTargetId)) {
                //如果是自己不可以进入设置页面
                iconMore.setVisibility(View.GONE);
            }

            //先判断MType是否为空，不为空进行下一步
            if (null != mType) {
                if (mType == Conversation.ConversationType.PRIVATE) {
                    //私聊
                    isGroup = false;
                    privateChat();
                } else if (mType == Conversation.ConversationType.GROUP) {
                    //群聊
                    isGroup = true;
                    groupChat();
                } else {
                    //to do nothing
                    isGroup = false;
                }
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (isGroup) {
            RongIMClient.getInstance().clearMessagesUnreadStatus(Conversation.ConversationType.GROUP, mTargetId, null);
        } else {
            RongIMClient.getInstance().clearMessagesUnreadStatus(Conversation.ConversationType.PRIVATE, mTargetId, null);
        }
        RongIMClient.getInstance().getTotalUnreadCount(new RongIMClient.ResultCallback<Integer>() {
            @Override
            public void onSuccess(Integer integer) {
                if (integer > 0) {
//                    tvLeft.setText(isGroup ? "消息" : String.format("消息(%d)", integer));
//                    tvLeft.setText(String.format("消息(%d)", integer));
                } else {
//                    tvLeft.setText("消息");
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {

            }
        });
    }

    @OnClick({R2.id.iconMore, R2.id.ibt_close, R2.id.btn_chat_follow})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.iconMore) {
            //获取类型
            if (mType.getName().equals(Conversation.ConversationType.GROUP.getName())) {
                // 群聊
                ARouter.getInstance().build(ARouterPath.GROUP_INFO_ACTIVITY).withString("targetId", mTargetId)
                        .withString("groupName", tvTitle.getText().toString()
                                .trim()).navigation();
            } else if (mType.getName().equals(Conversation.ConversationType.PRIVATE.getName())) {
                ARouter.getInstance().build(ARouterPath.PRIVATE_CHAT_SETTING_ACTIVITY).withString("targetId", mTargetId).navigation();
            }
        } else if (id == R.id.ibt_close) {
            rl_follow.setVisibility(View.GONE);
        } else if (id == R.id.btn_chat_follow) {
            toFollow();//
        }
    }

    private void toFollow() {
        String uId = mMMKV.getUserId();
        if (uId.equals(mTargetId)) {
            ToastUtils.showShort("不能关注自己");
            return;
        }

        basePresenter.followChatUser(mTargetId);
    }


    private void privateChat() {
        basePresenter.getContentUserDetail(mTargetId);
    }


    private void groupChat() {
        basePresenter.getGroupChat(mTargetId);
        basePresenter.queryMember(mTargetId);
    }


    private ContentUserInfoModel mModel;

    @Override
    public void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        if (model != null) {
            mModel = model;

            setTitle(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark()));//mModel

            String inviteCode = model.getInviteCode();

            boolean isFollow = model.isIsFollow();
            btnFollow.setSelected(isFollow);

            setFollowButtonText(model.getFollowStatus());
//            if (model.isInBlackList()) {
//                llDisableTip.setVisibility(View.VISIBLE);
//            } else {
//                llDisableTip.setVisibility(View.GONE);
//            }

            if (TextUtils.isEmpty(model.getPhoto())) {
                ImageLoaderUtil.loadImg(ivAvatar, R.drawable.place_holder);
            } else {
                ImageLoaderUtil.loadImg(ivAvatar, model.getPhoto());
            }
        }
    }


    @Override
    public void updateFollowUserSuccess() {
        rl_follow.setVisibility(View.GONE);

        boolean isFollow = mModel.isIsFollow();
        mModel.setIsFollow(!isFollow);
        btnFollow.setSelected(!isFollow);

        int followStatus = mModel.getFollowStatus();
        switch (followStatus) {
            case 1:
                mModel.setFollowStatus(2);
                break;
            case 2:
                mModel.setFollowStatus(1);
                break;
            case 3:
                mModel.setFollowStatus(4);
                break;
            case 4:
                mModel.setFollowStatus(3);
                break;
        }

        setFollowButtonText(mModel.getFollowStatus());
    }

    private void setFollowButtonText(int followStatus) {
        switch (followStatus) {
            case 1:
                btnFollow.setText("关注");
                rl_follow.setVisibility(View.VISIBLE);

                tvRelation_desc.setText("点关注，方便以后找到对方");
                break;
            case 2:
                btnFollow.setText("已关注");
                rl_follow.setVisibility(View.GONE);

                break;
            case 3:
                btnFollow.setText("回粉");
                rl_follow.setVisibility(View.VISIBLE);

                tvRelation_desc.setText("对方已关注你，回粉以后方便联系");
                break;
            case 4:
                btnFollow.setText("互相关注");
                rl_follow.setVisibility(View.GONE);

                break;
        }
    }

    @Override
    public void getGroupInfoSuccess(GroupInfo data) {
        if (null != data) {
            setTitle(data.getGroupName());

            //0-正常 1-冻结 2-解散
            int status = data.getGroupStatus();
            if (status == 0) {
                iconMore.setVisibility(View.VISIBLE);
            }

            if (status == 1) {
                iconMore.setVisibility(View.GONE);
            }

            if (status == 2) {
                iconMore.setVisibility(View.GONE);
            }

            Uri uri = Uri.parse(data.getGroupImg());
            Group group = new Group(data.getId(), data.getGroupName(), uri);
            RongUserInfoManager.getInstance().refreshGroupInfoCache(group);
        }
    }

    @Override
    public void queryMemberSuccess(List<DiscoGroupMember> listData) {
        if (null != listData && listData.size() > 0) {
            String groupName = "";

            DiscoGroupMember discoGroupMember = listData.get(0);
            if (TextUtils.isEmpty(groupName)) {
                groupName = discoGroupMember.getGroupName();
            } else {
                if ("null".equals(groupName.toLowerCase())) {
                    groupName = discoGroupMember.getGroupName();
                }
            }

            setTitle(groupName + "(" + listData.size() + ")");
            boolean hasLoginUser = false;

            membersID.clear();
            userInfos.clear();

            for (DiscoGroupMember member : listData) {
                if (member.getMemberId().equals(mMMKV.getUserId())) {
                    hasLoginUser = true;

                    int showName = member.getShowGroupName();
                    if (null != conversationFragment) {
                        conversationFragment.getMessageAdapter().setShowName(showName == 0);
                    }
                }

                //设置群
                RongUserInfoManager.getInstance().getGroupUserInfo(mTargetId, member.getMemberId());

                //设置群成员@
                UserInfo userInfo = new UserInfo(member.getMemberId(), YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()), Uri.parse(member.getUserImg()));
                userInfos.add(userInfo);

                //获取群中的ID
                membersID.add(member.getMemberId());

                GroupUserInfo groupUserInfo = new GroupUserInfo(member.getGroupId(), member.getMemberId(), YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
                RongIM.getInstance().refreshGroupUserInfoCache(groupUserInfo);
            }

            //需要设置群组成员信息 IGroupMembersProvider
            RongCallKit.setGroupMemberProvider((s, onGroupMembersResult) -> {
                onGroupMembersResult.onGotMemberList(membersID);
                return null;
            });
//
            //@功能
            RongIM.getInstance().setGroupMembersProvider((groupId, callback) -> {
                callback.onGetGroupMembersResult(userInfos); // 调用 callback 的 onGetGroupMembersResult 回传群组信息
            });

            if (hasLoginUser) {
                iconMore.setVisibility(View.VISIBLE);
            } else {
                iconMore.setVisibility(View.GONE);
                ToastUtils.showShort("您已被移出群聊");
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventFinishThePage(MessageEvent info) {
        super.onMessageEvent(info);
        if (info.getEventName().equals(CommonConstant.IM_GROUP_RELATE)) {
            EventUpdateGroupInfo event = (EventUpdateGroupInfo) info.getSerializable();
            if (null != event.getType()) {
                if (event.getType().equals("101")) {
                    this.finish();
                } else if (event.getType().equals(CommonConstant.IM_GROUP_HISTORY_DELETE)) {
                    MessageListAdapter mListAdapter = conversationFragment.getMessageAdapter();
                    mListAdapter.removeAll();
                    mListAdapter.notifyDataSetChanged();
                } else if (event.getType().equals("100")) {
                    basePresenter.getGroupChat(mTargetId);
                    basePresenter.queryMember(mTargetId);
                } else if (event.getType().equals("0")) {//免打扰
                }
            }
        }
    }
}
