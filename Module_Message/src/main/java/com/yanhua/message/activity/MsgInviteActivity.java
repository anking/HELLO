package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.MsgInviteListAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_INVITE_ACTIVITY)
public class MsgInviteActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    private MsgInviteListAdapter mAdapter;
    private int total;
    private List<InviteMsgModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_msg_invite_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("邀约");
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("全部已读");

        tvRight.setOnClickListener(v -> {
            //请求接口编辑全部已读
            basePresenter.dealInviteReadAll();
        });

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MsgInviteListAdapter(mContext);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<InviteMsgModel> list = mAdapter.getmDataList();
            InviteMsgModel item = list.get(pos);

            int state = item.getState();
            //已读直接跳转
            if (state == 1) {
                ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                        .withString("id", item.getInviteId()).navigation();
            } else {
                basePresenter.dealInviteReadId(item.getId(), pos);
            }
        });

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);
    }

    private void getListData(int page) {
        current = page;

        basePresenter.getMessageInviteList(current, size);
    }

    @Override
    public void handleReadedIDSuccess(int pos) {
        List<InviteMsgModel> list = mAdapter.getmDataList();
        InviteMsgModel item = list.get(pos);

        //private int state;//状态(0:未读, 1:已读)
        item.setState(1);
        mAdapter.notifyItemChanged(pos);

        //跳转到详情
        ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                .withString("id", item.getInviteId()).navigation();
    }

    @Override
    public void handleReadedSuccess() {
        ToastUtils.showShort("标记所有消息为已读");

        getListData(1);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }

    @Override
    public void handleMsgInviteList(ListResult<InviteMsgModel> listResult) {
        List<InviteMsgModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}