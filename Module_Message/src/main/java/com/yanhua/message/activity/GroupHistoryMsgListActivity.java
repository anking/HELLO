package com.yanhua.message.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.HistoryMsg;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.MsgHisAdapter;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import io.rong.imkit.RongIM;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.message.TextMessage;

/**
 * 创建群组或者删除群内人员
 */
@Route(path = ARouterPath.GROUP_HISTORY_MSGLIST_ACTIVITY)
public class GroupHistoryMsgListActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    private MsgHisAdapter mAdapter;
    private List<HistoryMsg> allListData = new ArrayList<>();
    private String keyWord;

    @Autowired(name = "groupID")
    public String groupId = "";
    @Autowired(name = "currentGroupName")
    public String currentGroupName = "";

    @Autowired(name = "groupMembers")
    public ArrayList<DiscoGroupMember> mGroupMembers;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_hismsg_list;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        buildMemberById();

        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        getWindow().getDecorView().postDelayed(() -> {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                etSearch.requestFocus();
                inputManager.showSoftInput(etSearch, 0);
            }
        }, 100);

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new MsgHisAdapter(mContext);
        rvContent.setAdapter(mAdapter);

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
                    RongIMClient.getInstance().searchMessages(Conversation.ConversationType.GROUP, groupId, keyWord, 50, 0, new RongIMClient.ResultCallback<List<Message>>() {
                        @Override
                        public void onSuccess(List<Message> msgList) {
                            historyMsgListData(msgList);
                        }

                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {
                            ToastUtils.showShort(errorCode.getMessage());
                        }
                    });
                } else {
                    allListData.clear();
                    mAdapter.setItems(allListData);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            HistoryMsg item = allListData.get(pos);

            RongIM.getInstance().startConversation(this, Conversation.ConversationType.GROUP, groupId, currentGroupName, item.getSentTime());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //跳转到页面中
                    GroupHistoryMsgListActivity.this.finish();
                    EventBus.getDefault().post(new MessageEvent("close_group_info"));
                }
            }, 200);
        });
    }

    private HashMap<String, DiscoGroupMember> map = null;

    private void buildMemberById() {
        map = new HashMap<>();

        for (DiscoGroupMember member : mGroupMembers) {
            map.put(member.getMemberId(), member);
        }
    }

    private void historyMsgListData(List<Message> msgList) {
        if (null != msgList && msgList.size() > 0) {
            allListData.clear();

            List<HistoryMsg> messages = new ArrayList<>();
            for (Message msg : msgList) {
                MessageContent content = msg.getContent();
                String userId = msg.getSenderUserId();
                if (content instanceof TextMessage) {
                    HistoryMsg msgg = new HistoryMsg();
                    //根据id获取member信息
                    DiscoGroupMember member = map.get(userId);
                    msgg.setMsgContent(((TextMessage) content).getContent());
                    msgg.setHeadUrl(member.getUserImg());
                    msgg.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));

                    msgg.setSentTime(msg.getSentTime());
                    msgg.setReceivedTime(msg.getReceivedTime());
                    msgg.setReadTime(msg.getReadTime());
                    messages.add(msgg);
                }
            }
            allListData.addAll(messages);
            emptyLayout.setVisibility(View.GONE);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            emptyLayout.setErrorType(EmptyLayout.NODATA);
//                emptyLayout.setErrorImag(R.mipmap.empty_point);
//                emptyLayout.setErrorMessage("暂无记录");

//            emptyLayout.setErrorImag(R.mipmap.empty_data);
            emptyLayout.setErrorMessage("这里没有相关记录哦");
        }

        mAdapter.setItems(allListData);
    }

    @Override
    public void showLoading() {
        emptyLayout.setVisibility(View.VISIBLE);
        emptyLayout.setErrorType(EmptyLayout.TRANSPARENT_LOADING);
    }

    @Override
    public void hideLoading() {
        emptyLayout.setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }
}
