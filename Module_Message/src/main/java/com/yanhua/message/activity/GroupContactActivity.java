package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.PinyinUtils;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.core.view.WaveSideBarView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.ContactsGroupAdapter;
import com.yanhua.message.presenter.ChatMessagePresenter;
import com.yanhua.message.presenter.contract.ChatMessageContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.model.Conversation;

/**
 * 我的群组列表
 *
 * @author Administrator
 */
@Route(path = ARouterPath.GROUP_CONTACTS_ACTIVITY)
public class GroupContactActivity extends BaseMvpActivity<ChatMessagePresenter> implements ChatMessageContract.IView {

    @BindView(R2.id.ll_click2search)
    LinearLayout llClickSearch;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;

    @BindView(R2.id.wave_side_bar)
    WaveSideBarView sideBar;

    @BindView(R2.id.ll_search)
    LinearLayout llSearch;

    @BindView(R2.id.et_search)
    EditText etSearch;

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @Autowired(name = "selectMode")
    public int selectMode;
    @Autowired(name = "multiSelect")
    public boolean multiSelect;
    @Autowired(name = "selecteListData")
    public ArrayList<ShareObjectModel> selecteListData;//从外面传入的值---以及返回去的


    private List<GroupsBean> allListData = new ArrayList<>();
    private String keyWord;

    private ArrayList<String> phoneList;

    private ContactsGroupAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new ChatMessagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_contacts;
    }

    private ArrayList<GroupsBean> selecteGroupList = new ArrayList<>();

    /**
     * 从外面赋值
     */
    private void initSelectData() {
        if (null == selecteListData) {
            selecteListData = new ArrayList<>();
        }

        if (selecteListData.size() > 0) {
            for (ShareObjectModel shareObjectModel : selecteListData) {
                GroupsBean model = new GroupsBean();

                String name = shareObjectModel.getNickName();
                model.setName(name);

                String pinyin = PinyinUtils.formatPinyin(name);
                model.setNamePinYin(pinyin);

                String firstLetter = pinyin.substring(0, 1).toUpperCase();
                if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
                    firstLetter = "#";
                }
                model.setFirstLetter(firstLetter);

                model.setSelected(true);
                model.setId(shareObjectModel.getUserId());
                model.setGroupImg(shareObjectModel.getImg());

                selecteGroupList.add(model);
            }
        }

        if (selectMode == 1 && multiSelect) {
            tvRight.setVisibility(View.VISIBLE);

            int size = selecteGroupList.size();
            tvRight.setText(size > 0 ? "确定(" + size + ")" : "确定");

            tvRight.setEnabled(false);
            tvRight.setTextColor(mContext.getResources().getColor(R.color.subWord));
        }
    }

    private void doAddDelete(GroupsBean model, boolean returnBack) {
        boolean selected = model.isSelected();

        ShareObjectModel shareObject = new ShareObjectModel();
        shareObject.setGroup(true);
        shareObject.setNickName(model.getName());
        shareObject.setImg(model.getGroupImg());
        shareObject.setUserId(model.getId());
        shareObject.setSelected(selected);

        if (selected) {
            selecteListData.add(shareObject);
            selecteGroupList.add(model);
        } else {
            selecteListData.remove(shareObject);
            selecteGroupList.remove(model);
        }

        if (returnBack) {
            returnSelectData(returnBack);
            return;
        }

        if (selecteListData.size() > 0) {
            int num = selecteListData.size();
            tvRight.setText("确定(" + num + ")");
            tvRight.setEnabled(true);
            tvRight.setTextColor(mContext.getResources().getColor(R.color.theme));
        } else {
            tvRight.setText("确定");
            tvRight.setEnabled(false);
            tvRight.setTextColor(mContext.getResources().getColor(R.color.subWord));
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("我的群聊");


        //初始通讯录列表
        phoneList = new ArrayList<>();

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new ContactsGroupAdapter(mContext, multiSelect);

        rvContent.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            GroupsBean model = mAdapter.getmDataList().get(pos);

            if (selectMode == 1) {
                //返回到上一个
                if (!multiSelect) {
                    boolean select = model.isSelected();
                    model.setSelected(!select);

                    mAdapter.notifyItemChanged(pos);

                    doAddDelete(model, true);
                } else {
                    if (selecteListData.size() >= 9) {
                        CommonDialog commonDialog = new CommonDialog(mContext, "最多只能选择9个聊天", "", "我知道了", "");
                        new XPopup.Builder(mContext).asCustom(commonDialog).show();
                        commonDialog.setOnConfirmListener(() -> {
                            commonDialog.dismiss();
                        });
                    } else {

                        boolean select = model.isSelected();
                        model.setSelected(!select);

                        mAdapter.notifyItemChanged(pos);

                        doAddDelete(model, false);
                    }
                }
            } else {
                RouteUtils.routeToConversationActivity(this, Conversation.ConversationType.GROUP, model.getId(), null);
            }
        });

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            basePresenter.getUserGroups(mMMKV.getUserId(), TextUtils.isEmpty(keyWord) ? null : keyWord, false);
        });

        basePresenter.getUserGroups(mMMKV.getUserId(), null, false);

        refreshLayout.setEnableLoadMore(false);

        sideBar.setOnTouchLetterChangeListener(letter -> {
            List<GroupsBean> data = mAdapter.getmDataList();
            for (int i = 0; i < data.size(); i++) {
                GroupsBean friend = data.get(i);
                String firstLetter = friend.getFirstLetter();
                if ("#".equals(letter) && "#".equals(firstLetter)) {
                    rvContent.scrollToPosition(i);
                    break;
                } else if (firstLetter.equalsIgnoreCase(letter)) {
                    rvContent.scrollToPosition(i);
                    break;
                }
            }
        });

        initSelectData();

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
//                    String inputContent = etSearch.getText().toString().trim();
//                    List<GroupsBean> user_temp = new ArrayList<GroupsBean>();
//                    for (GroupsBean groupsBean : mAdapter.getmDataList()) {
//                        String groupsBeanName = groupsBean.getName();
//
//                        String nameTemp = groupsBeanName.toLowerCase();
//                        String inputContentTemp = inputContent.toLowerCase();
//                        if (nameTemp.contains(inputContentTemp)) {
//                            user_temp.add(groupsBean);
//                        }
//                        mAdapter.setItems(user_temp);
//                    }
                    basePresenter.getUserGroups(mMMKV.getUserId(), keyWord, true);
                } else {
                    basePresenter.getUserGroups(mMMKV.getUserId(), null, true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        sideBar.setVisibility(View.GONE);
    }

    @OnClick(R2.id.tv_right)
    public void onClickSelectData() {
        returnSelectData(false);

    }

    public void returnSelectData(boolean eventBool) {
        MessageEvent event = new MessageEvent(CommonConstant.SHARE_APP_LIST, selecteListData);
        EventBus.getDefault().post(event);

        onBackPressed();
    }

    @OnClick({R2.id.ll_click2search, R2.id.ll_clicksub2search, R2.id.iv_click_search, R2.id.tv_click_search})
    public void showSearchBar() {
        llSearch.setVisibility(View.VISIBLE);
        // 弹起键盘
        KeyboardUtils.showSoftInput();
        etSearch.requestFocus();

        llClickSearch.setVisibility(View.GONE);

        showSearchView();
    }

    @OnClick(R2.id.tv_cancel)
    public void hideSearchBar() {
        llSearch.setVisibility(View.GONE);
        KeyboardUtils.hideSoftInput(this);
        etSearch.setText("");
        llClickSearch.setVisibility(View.VISIBLE);

        hideSearchView();
    }

    @Override
    public void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean search) {
        refreshLayout.finishRefresh();
        if (CollectionUtils.isNotEmpty(list)) {
            allListData.clear();

            List<GroupsBean> groupList = new ArrayList<>();
            //过滤只有选中的
            for (GroupsBean item : list) {
                String name = item.getName();
                String pinyin = PinyinUtils.formatPinyin(name);
                item.setNamePinYin(pinyin);
                String firstLetter = pinyin.substring(0, 1).toUpperCase();
                if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
                    firstLetter = "#";
                }
                item.setFirstLetter(firstLetter);

                if (selecteGroupList.contains(item)) {
                    item.setSelected(true);
                }

                if (null != item.getId()) {
                    groupList.add(item);
                }
            }

            //整理排序
            Collections.sort(groupList);
            allListData.addAll(groupList);
            mAdapter.setItems(allListData);
            sideBar.setData(allListData, GroupsBean::getFirstLetter, 0);

            rvContent.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);

            sideBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    List<String> letters = sideBar.getLetters();
                    if (null != letters && letters.size() > 8) {
                        sideBar.setVisibility(View.VISIBLE);
                    } else {
                        sideBar.setVisibility(View.GONE);
                    }
                }
            }, 300);

            if (search) {
                hideSearchView();
            }
        } else {
            rvContent.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
            emptyLayout.setErrorType(EmptyLayout.NODATA);

            emptyLayout.setErrorImag(R.drawable.picture_icon_data_error);
            emptyLayout.setErrorMessage("暂无数据");

            if (search) {
                showSearchView();
            }
        }
    }

//    @Override
//    public void showLoading() {
//        emptyLayout.setVisibility(View.VISIBLE);
//        emptyLayout.setErrorType(EmptyLayout.TRANSPARENT_LOADING);
//    }
//
//    @Override
//    public void hideLoading() {
//        emptyLayout.setVisibility(View.GONE);
//        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
//    }

    @Override
    public void handleErrorMsg(String msg) {
        ToastUtils.showShort(msg);

        if ("当前用户没有群组".equals(msg)) {//太无奈了，后台返回的错误码
            emptyLayout.setVisibility(View.VISIBLE);
            emptyLayout.setErrorType(EmptyLayout.NODATA);

//            emptyLayout.setErrorImag(R.drawable.empty);
            emptyLayout.setErrorMessage("暂无数据");
        }
    }

    private void showSearchView() {
        emptyLayout.setVisibility(View.VISIBLE);
        emptyLayout.setErrorType(EmptyLayout.SEARCH_INPUT_STATUS);
    }

    public void hideSearchView() {
        emptyLayout.setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @Override
    protected void onResume() {
        super.onResume();

        KeyboardUtils.registerSoftInputChangedListener(this, height -> {
            if (height < 200) {
                // 键盘收起
                hideSearchView();
            } else {
                // 键盘弹出
            }
        });
    }
}
