package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MsgNewFansModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.FollowUserAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 新增粉丝
 *
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_NEW_FOLLOW_ACTIVITY)
public class NewFollowActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_comment)
    RecyclerView rvContent;

    private FollowUserAdapter mAdapter;
    private List<MsgNewFansModel> records = new ArrayList<>();

    private int total = 0;
    private int current = 1;
    private int size = 10;

    private int mPos = -1;

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_new_follow;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle("新增关注");
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new FollowUserAdapter(mContext);
        rvContent.setAdapter(mAdapter);

        refreshLayout.setOnRefreshListener(r -> {
            current = 1;
            getDataList(current, size);
        });
        refreshLayout.setOnLoadMoreListener(r -> {
            if (records.size() < total) {
                current++;
                getDataList(current, size);
            } else {
                refreshLayout.finishLoadMore(1000);
            }
        });

        mAdapter.setOnSelectClickListener(pos -> {
            MsgNewFansModel itemObject = mAdapter.getItemObject(pos);
            int status = itemObject.getRelationState();
            mPos = pos;
            if (status == 2 || status == 4) {
                final AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    basePresenter.cancelFollowUser(itemObject.getUserId());
                });
                alertPopup.setOnCancelListener(() -> alertPopup.dismiss());
            } else {
                basePresenter.followUser(itemObject.getUserId());
            }
        });

        refreshLayout.autoRefresh();
    }

    private void getDataList(int current, int size) {
        basePresenter.getNewFansList(current, size);
    }


    @Override
    public void handleFollowUserSuccess() {
        MsgNewFansModel itemObject = mAdapter.getItemObject(mPos);
        int status = itemObject.getRelationState();
////            1 未关注 2 已关注 3 被关注 4互相关注
        if (status == 1) {
            itemObject.setRelationState(2);
        }
        if (status == 3) {
            itemObject.setRelationState(4);
        }

        mAdapter.notifyItemChanged(mPos);
        ToastUtils.showShort("关注成功");
    }

    @Override
    public void handleCancelFollowUserSuccess() {
        MsgNewFansModel itemObject = mAdapter.getItemObject(mPos);

        int status = itemObject.getRelationState();
        ////1 未关注 2 已关注 3 被关注 4互相关注
        if (status == 2) {
            itemObject.setRelationState(1);
        }
        if (status == 4) {
            itemObject.setRelationState(3);
        }

        mAdapter.notifyItemChanged(mPos);

        ToastUtils.showShort("取关成功");
    }

    private boolean isFirst;

    @Override
    public void handleMsgNewFansList(ListResult<MsgNewFansModel> model) {
        if (current == 1) {
            records.clear();
            refreshLayout.finishRefresh();

            refreshLayout.resetNoMoreData();
            if (!isFirst) {
                basePresenter.dealNewFansReadAll();
            }

            isFirst = true;
        } else {
            refreshLayout.finishLoadMore();
        }

        if (model != null && CollectionUtils.isNotEmpty(model.getRecords())) {
            refreshLayout.setVisibility(View.VISIBLE);
            total = model.getTotal();
            // 处理日期
            records.addAll(model.getRecords());
            mAdapter.setItems(records);
        } else {
            if (current == 1) {
                refreshLayout.setVisibility(View.GONE);
            }
        }
    }
}
