package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.dmingo.optionbarview.OptionBarView;
import com.lqr.adapter.LQRAdapterForRecyclerView;
import com.lqr.adapter.LQRViewHolderForRecyclerView;
import com.lqr.recyclerview.LQRRecyclerView;
import com.lxj.xpopup.XPopup;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.MemberItem;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.TypeWayListBottomPopup;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;
import com.yanhua.message.view.NoticeAlertPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.IMCenter;
import io.rong.imlib.IRongCoreCallback;
import io.rong.imlib.IRongCoreEnum;
import io.rong.imlib.RongCoreClient;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;

/**
 * 群组信息页面，群成员（群主和其他人员）
 */
@Route(path = ARouterPath.GROUP_INFO_ACTIVITY)
public class GroupInfoActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.obvGroupName)
    OptionBarView obvGroupName;
    @BindView(R2.id.obvNickNameInGroup)
    OptionBarView obvNickNameInGroup;
    @BindView(R2.id.obvComplaint)
    OptionBarView obvComplaint;
    @BindView(R2.id.obvAnnouncement)
    LinearLayout obvAnnouncement;
    @BindView(R2.id.tvShowGroupNotice)
    TextView tvShowGroupNotice;
    @BindView(R2.id.obvTeamManage)
    OptionBarView obvTeamManage;
    @BindView(R2.id.oivfindMsgRecord)
    OptionBarView oivfindMsgRecord;
    @BindView(R2.id.obv_isAvoid)
    OptionBarView obv_isAvoid;
    @BindView(R2.id.obv_isTop)
    OptionBarView obv_isTop;
    @BindView(R2.id.obv_showNick)
    OptionBarView obv_showNick;
    @BindView(R2.id.btnQuit)
    Button btnQuit;
    @BindView(R2.id.tvLookMoreMembers)
    TextView tvLookMoreMembers;

    @BindView(R2.id.rvMember)
    LQRRecyclerView rvMember;

    @Autowired(name = "targetId")
    String targetId;
    @Autowired(name = "groupName")
    String groupName;

    private LQRAdapterForRecyclerView<DiscoGroupMember> mAdapter;
    private ArrayList<DiscoGroupMember> mGroupMembers = new ArrayList<>();
    private List<DiscoGroupMember> mData = new ArrayList<>();
    private boolean isMaster;
    private boolean isManager;
    private String mNoticeContent;
    private String mNoticeContentTime;

    private GroupInfo currentData;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_info;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("聊天信息");

        // 获取免打扰状态
        RongIMClient.getInstance().getConversationNotificationStatus(Conversation.ConversationType.GROUP, targetId, new RongIMClient.ResultCallback<Conversation.ConversationNotificationStatus>() {
            /**
             * 成功回调
             * @param status 消息提醒状态
             */
            @Override
            public void onSuccess(Conversation.ConversationNotificationStatus status) {
                if (status == Conversation.ConversationNotificationStatus.DO_NOT_DISTURB) {
                    obv_isAvoid.setChecked(true);
                } else {
                    obv_isAvoid.setChecked(false);
                }

                MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("0"));
                EventBus.getDefault().post(event);
            }

            /**
             * 错误回调
             * @param errorCode 错误码
             */
            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
            }
        });

        // 获取会话置顶状态
        RongCoreClient.getInstance().getConversationTopStatus(targetId, Conversation.ConversationType.GROUP,
                new IRongCoreCallback.ResultCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        obv_isTop.setChecked(aBoolean);
                    }

                    @Override
                    public void onError(IRongCoreEnum.CoreErrorCode coreErrorCode) {

                    }
                });

        //obv_isTop,obv_isAvoid
        obv_isTop.setSplitMode(true);
        obv_isTop.setOnSwitchCheckedChangeListener((view, isChecked) ->
                IMCenter.getInstance().setConversationToTop(
                        Conversation.ConversationType.GROUP,
                        targetId,
                        isChecked,//是否置顶, true 为置顶, false 为取消置顶
                        true,//会话不存在时，是否创建会话
                        new RongIMClient.ResultCallback<Boolean>() {
                            /**
                             * 成功回调
                             */
                            @Override
                            public void onSuccess(Boolean success) {
                            }

                            /**
                             * 错误回调
                             * @param errorCode 错误码
                             */
                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        }));

        obv_isAvoid.setSplitMode(true);

        //免打扰
        obv_isAvoid.setOnSwitchCheckedChangeListener((view, isChecked) -> {
            IMCenter.getInstance().setConversationNotificationStatus(
                    Conversation.ConversationType.GROUP,
                    targetId,
                    isChecked ? Conversation.ConversationNotificationStatus.DO_NOT_DISTURB : Conversation.ConversationNotificationStatus.NOTIFY,
                    new RongIMClient.ResultCallback<Conversation.ConversationNotificationStatus>() {

                        /**
                         * 成功回调
                         * @param status 消息提醒状态
                         */
                        @Override
                        public void onSuccess(Conversation.ConversationNotificationStatus status) {
                        }

                        /**
                         * 错误回调
                         * @param errorCode 错误码
                         */
                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });
        });

        obv_showNick.setOnClickListener((view) -> showNickSwitch(obv_showNick.isChecked()));

        //事件处理
        obvGroupName.setRightText(groupName.length() > 10 ? groupName.substring(0, 10) + "..." : groupName);

        obvGroupName.setOnClickListener(view -> {
            ARouter.getInstance().build(ARouterPath.GROUP_MODIFY_ACTIVITY)
                    .withString("modifyType", IM.group.MODIFY_GROUP_NAME)
                    .withString("groupID", targetId)
                    .withString("modifyUrl", currentData.getGroupImg())
                    .withString("nickName", groupName).navigation();//修改成功需要回显
        });

        obvNickNameInGroup.setOnClickListener(view -> {
            String img = UserManager.getInstance().getUserInfo().getImg();

            ARouter.getInstance().build(ARouterPath.GROUP_MODIFY_ACTIVITY)
                    .withString("modifyType", IM.group.MODIFY_PERSONAL_NAME)
                    .withString("groupID", targetId)
                    .withString("modifyUrl", img)
                    .withString("nickName", obvNickNameInGroup.getRightText()).navigation();//修改成功需要回显
        });

        obvComplaint.setOnClickListener(view -> ARouter.getInstance()
                .build(ARouterPath.GROUP_COMPLAINT_ACTIVITY)
                .withString("groupID", targetId)
                .withString("groupName", groupName)
                .navigation());
        obvAnnouncement.setOnClickListener(view -> ARouter.getInstance()
                .build(ARouterPath.GROUP_NOTICE_ACTIVITY)
                .withString("groupOwner", (isMaster || isManager) ? IM.group.GROUP_OWNER : IM.group.GROUP_OTHER)
                .withString("noticeContent", mNoticeContent)
                .withString("noticeContentTime", mNoticeContentTime)
                .withSerializable("master", master)
                .withString("groupID", targetId).navigation());//mNoticeContent

        obvTeamManage.setOnClickListener(view -> groupManageMember());
        oivfindMsgRecord.setOnClickListener(view -> {
            ARouter.getInstance().build(ARouterPath.GROUP_HISTORY_MSGLIST_ACTIVITY)
                    .withString("groupID", targetId)
                    .withSerializable("groupMembers", mGroupMembers)
                    .withString("currentGroupName", groupName).navigation();
        });
    }

    /**
     * 展示昵称
     *
     * @param isChecked
     */
    private void showNickSwitch(boolean isChecked) {
        HashMap params = basePresenter.buildSetGroupInfo(null, targetId, "", "", mMMKV.getUserId(), "", isChecked ? "1" : "0");

        // setGroupInfo-->setGroupSwitch
        basePresenter.setGroupInfo(params);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventUpdateGroupInfo(MessageEvent info) {
        super.onMessageEvent(info);
        String mesg = info.getEventName();
        if ("close_group_info".equals(mesg)) {
            GroupInfoActivity.this.finish();
        } else {
            EventUpdateGroupInfo event = (EventUpdateGroupInfo) info.getSerializable();

            if (null != event && null != event.getType()) {
                loadData();
            }
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
//        loadData();
    }

    private void loadData() {
        basePresenter.getGroupChat(targetId);
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new LQRAdapterForRecyclerView<DiscoGroupMember>(mContext, mData, R.layout.item_member_info) {
                @Override
                public void convert(LQRViewHolderForRecyclerView helper, DiscoGroupMember item, int position) {
                    ImageView ivHeader = helper.getView(R.id.ivHeader);

                    if ((isMaster || isManager) && position >= mData.size() - 2) {//+和-
                        if (position == mData.size() - 2) {//+
                            ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                        } else {//-
                            ivHeader.setImageResource(R.mipmap.ic_delete_team_member);
                        }
                        helper.setText(R.id.tvName, "");
                    } else if (!isMaster && !isManager && position >= mData.size() - 1) {//+
                        ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                        helper.setText(R.id.tvName, "");
                    } else {
                        Glide.with(mContext).load(item.getUserImg()).centerCrop().into(ivHeader);
                        helper.setText(R.id.tvName, YHStringUtils.pickName(item.getNickName(), item.getFriendRemark()));
                    }

                    int type = item.getType();

                    switch (type) {
                        case 1:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.VISIBLE);
                            helper.setText(R.id.tvGroupMasterTag, "群主");
                            helper.setBackgrounResource(R.id.tvGroupMasterTag, R.drawable.shape_bg_rff7f02_stroke1);
                            break;
                        case 2:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.GONE);
                            break;
                        case 3:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.VISIBLE);
                            helper.setText(R.id.tvGroupMasterTag, "管理员");
                            helper.setBackgrounResource(R.id.tvGroupMasterTag, R.drawable.shape_bg_r33c35c_stroke1);
                            break;
                        default:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.GONE);
                            break;
                    }
                }
            };
            mAdapter.setOnItemClickListener((helper, parent, itemView, position) -> {
                if ((isMaster || isManager) && position >= mData.size() - 2) {//+和-
                    if (position == mData.size() - 2) {//+
                        inviteOrDeleteMember(IM.group.GROUP_INVITE_MEMBER);
                    } else {//-
                        inviteOrDeleteMember(IM.group.GROUP_DELETE_MEMBER);
                    }
                } else if (!isMaster && !isManager && position >= mData.size() - 1) {//
                    //非群主只能邀请成员
                    inviteOrDeleteMember(IM.group.GROUP_INVITE_MEMBER);
                } else {
                    ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", mData.get(position).getMemberId())
                            .navigation();
                }
            });
            rvMember.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChangedWrapper();
        }
    }


    /**
     * 群管理
     */
    private void groupManageMember() {
        ArrayList<String> selectedTeamMembers = new ArrayList<String>();

        ArrayList<FriendModel> convertFriends = new ArrayList<>();

        for (int i = 0; i < mGroupMembers.size(); i++) {
            DiscoGroupMember member = mGroupMembers.get(i);

            selectedTeamMembers.add(member.getMemberId());
            FriendModel friendModel = new FriendModel();

            friendModel.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
            friendModel.setUserId(member.getMemberId());
            friendModel.setUserPhoto(member.getUserImg());

            convertFriends.add(friendModel);
        }

        ARouter.getInstance().build(ARouterPath.GROUP_MANAGE_ACTIVITY)
                .withString("groupID", targetId)
                .withSerializable("convertFriends", convertFriends)
                .withSerializable("selectedTeamMembers", selectedTeamMembers)
                .withString("currentGroupName", groupName).navigation();
    }

    /**
     * 邀请/删除成员
     */
    private void inviteOrDeleteMember(String type) {
        ArrayList<String> selectedTeamMembers = new ArrayList<String>();

        ArrayList<FriendModel> members = new ArrayList<>();
        for (int i = 0; i < mGroupMembers.size(); i++) {
            DiscoGroupMember member = mGroupMembers.get(i);

            if (isManager && type.equals(IM.group.GROUP_DELETE_MEMBER)) {//群管理员---删除非群主和非管理员的成员
                int groupRole = member.getType();
                if (groupRole == 2) {
                    selectedTeamMembers.add(member.getMemberId());
                    FriendModel friendModel = new FriendModel();

                    friendModel.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
                    friendModel.setUserId(member.getMemberId());
                    friendModel.setUserPhoto(member.getUserImg());

                    members.add(friendModel);
                }
            } else {//群主
                selectedTeamMembers.add(member.getMemberId());
                FriendModel friendModel = new FriendModel();

                friendModel.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
                friendModel.setUserId(member.getMemberId());
                friendModel.setUserPhoto(member.getUserImg());

                members.add(friendModel);
            }
        }

        ARouter.getInstance().build(ARouterPath.CREATE_GROUP_ACTIVITY)
                .withSerializable("selectedTeamMembers", selectedTeamMembers)
                .withString("groupID", targetId)
                .withSerializable("convertFriends", members)
                .withString("currentGroupName", groupName)
                .withString("type", type).navigation();
    }

    @Override
    public void queryMemberSuccess(List<DiscoGroupMember> listDataAll) {
        if (null != listDataAll && listDataAll.size() > 0) {
            //获取群成员列表成功
            mGroupMembers.clear();
            mGroupMembers.addAll(listDataAll);

            setTitle("聊天信息(" + mGroupMembers.size() + ")");

            List<DiscoGroupMember> listData = new ArrayList<>();

            mData.clear();

            for (DiscoGroupMember discoGroupMember : mGroupMembers) {
                String uid = discoGroupMember.getMemberId();
                int type = discoGroupMember.getType();

                if (uid.equals(mMMKV.getUserId()) && type == 3) {
                    isManager = true;
                    master = discoGroupMember;
                    break;
                }
            }

            if (isManager || isMaster) {
                if (mGroupMembers.size() > 13) {
                    for (int i = 0; i < 13; i++) {
                        listData.add(mGroupMembers.get(i));
                    }
                    tvLookMoreMembers.setVisibility(View.VISIBLE);
                } else {
                    listData.addAll(mGroupMembers);
                    tvLookMoreMembers.setVisibility(View.GONE);
                }

                //再加两个
                DiscoGroupMember inviteIcon = new DiscoGroupMember();
                inviteIcon.setType(100);//+
                DiscoGroupMember deleteIcon = new DiscoGroupMember();
                deleteIcon.setType(101);//-
                listData.add(inviteIcon);
                listData.add(deleteIcon);
            } else {
                if (mGroupMembers.size() > 14) {
                    for (int i = 0; i < 14; i++) {
                        listData.add(mGroupMembers.get(i));
                    }
                    tvLookMoreMembers.setVisibility(View.VISIBLE);
                } else {
                    listData.addAll(mGroupMembers);
                    tvLookMoreMembers.setVisibility(View.GONE);
                }
                //加一个
                DiscoGroupMember inviteIcon = new DiscoGroupMember();
                inviteIcon.setType(100);//+
                listData.add(inviteIcon);
            }

            if (!isManager && !isMaster) {
                obvAnnouncement.setVisibility(TextUtils.isEmpty(mNoticeContent) ? View.GONE : View.VISIBLE);//不是群主的话根据后台返回的，公告内容，有就显示
            } else {
                obvAnnouncement.setVisibility(View.VISIBLE);
            }


            mData.addAll(listData);//负责显示的，数据不完整，完整的群成员数据请看 mGroupMembers

            setAdapter();

            String id = mMMKV.getUserId();

            for (DiscoGroupMember member : mGroupMembers) {
                String memberId = member.getMemberId();
                if (null != memberId && memberId.equals(id)) {
                    int showName = member.getShowGroupName();

                    boolean showNick = showName == 0;
                    obv_showNick.setChecked(showNick);//0显示 1 不显

                    String nickName = YHStringUtils.pickName(member.getNickName(), member.getFriendRemark());

                    obvNickNameInGroup.setRightText(nickName.length() > 10 ? nickName.substring(0, 10) + "..." : nickName);
                }
                if (member.getType() == 1) {
                    //群主
                    master = member;
                }
            }
        }
    }

    private DiscoGroupMember master;

    @Override
    public void signOutGroupSuccess() {
        //Toast some words
        onBackPressed();
        MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("101"));
        EventBus.getDefault().post(event);

        //删除列表显示
        RongIMClient.getInstance().removeConversation(Conversation.ConversationType.GROUP, targetId, new RongIMClient.ResultCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {

            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {

            }
        });
    }

    @Override
    public void getGroupInfoSuccess(GroupInfo data) {
        //群信息
        if (null != data) {
            currentData = data;

            groupName = data.getGroupName();
            obvGroupName.setRightText(groupName.length() > 10 ? groupName.substring(0, 10) + "..." : groupName);

            mNoticeContent = data.getNoticeContent();//
            mNoticeContentTime = data.getNoticeTime();

            String groupMaster = data.getGroupMaster();
            isMaster = groupMaster.equals(mMMKV.getUserId());
            if (!isMaster) {
                obvTeamManage.setVisibility(View.GONE);
//                obvAnnouncement.setVisibility(TextUtils.isEmpty(mNoticeContent) ? View.GONE : View.VISIBLE);//不是群主的话根据后台返回的，公告内容，有就显示
            } else {
                obvTeamManage.setVisibility(View.VISIBLE);
//                obvAnnouncement.setVisibility(View.VISIBLE);
            }

            tvShowGroupNotice.setVisibility(TextUtils.isEmpty(mNoticeContent) ? View.GONE : View.VISIBLE);
            tvShowGroupNotice.setText(TextUtils.isEmpty(mNoticeContent) ? "" : mNoticeContent);

            btnQuit.setText(isMaster ? "解散并退出" : "删除并退出");

            //获取群成员信息
            basePresenter.queryMember(targetId);
        }
    }

    @Override
    public void setGroupInfoSuccess() {
        MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("100"));
        EventBus.getDefault().post(event);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick({R2.id.tvLookMoreMembers})
    public void onClickLookMoreMembers(View view) {
        if (view.getId() == R.id.tvLookMoreMembers) {
            //跳转到查看更多详情
            ARouter.getInstance().build(ARouterPath.GROUP_MEMBERS_ACTIVITY).withString("targetId", targetId).withString("groupName", groupName).navigation();
        }
    }

    @OnClick({R2.id.btnQuit})
    public void onClickQuitGroup(View view) {
        if (view.getId() == R.id.btnQuit) {
            NoticeAlertPopup noticeAlertPopup = new NoticeAlertPopup(this, "", "确认" + (isMaster ? "解散并退出" : "删除并退出") + "?", "确定");
            new XPopup.Builder(mContext).asCustom(noticeAlertPopup).show();
            noticeAlertPopup.setOnConfirmListener(() -> {
                if (isMaster) {
                    basePresenter.updateGroupOrDismiss(targetId);//app/rong/cloud/updateGroupOrDismiss
                } else {
                    HashMap params = new HashMap<>();
                    params.put("groupId", targetId);
                    params.put("groupName", obvGroupName.getRightText());
                    params.put("groupImg", currentData.getGroupImg());

                    ArrayList<MemberItem> groupMemberList = new ArrayList<>();

                    MemberItem item = new MemberItem();

                    item.setMemberId(mMMKV.getUserId());
                    item.setNickName(obvNickNameInGroup.getRightText());

                    ////类型：1群主、2群员、3群管理
                    if (isMaster) {
                        item.setType(1);
                    } else if (isManager) {
                        item.setType(3);
                    } else {
                        item.setType(2);
                    }

                    groupMemberList.add(item);

                    params.put("groupMemberList", groupMemberList);

                    basePresenter.signOutGroup(params);//app/rong/cloud/group/signOut"
                }
            });
        }
    }

    @OnClick({R2.id.oivClearMsgRecord})
    public void onClickClearMsgRecord(View view) {
        if (view.getId() == R.id.oivClearMsgRecord) {

            ArrayList<TypeWayModel> list = new ArrayList<>();

            list.add(new TypeWayModel("1", "清空聊天记录"));

            TypeWayListBottomPopup popup = new TypeWayListBottomPopup(mContext, list);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();

                IMCenter.getInstance().deleteMessages(Conversation.ConversationType.GROUP, targetId, new RongIMClient.ResultCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean bool) {
                        ToastUtils.showShort("已清空聊天记录");

                        MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo(CommonConstant.IM_GROUP_HISTORY_DELETE));
                        EventBus.getDefault().post(event);
                    }

                    @Override
                    public void onError(RongIMClient.ErrorCode errorCode) {
                        ToastUtils.showShort(errorCode + "");
                    }
                });

            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        }
    }
}
