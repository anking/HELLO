package com.yanhua.message.activity;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.CollectionUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.common.reflect.TypeToken;
import com.luck.picture.lib.tools.BitmapUtils;
import com.lxj.xpopup.XPopup;
import com.othershe.combinebitmap.CombineBitmap;
import com.othershe.combinebitmap.layout.WechatLayoutManager;
import com.othershe.combinebitmap.listener.OnProgressListener;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.GroupResult;
import com.yanhua.common.model.MemberItem;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.PinyinUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.core.view.WaveSideBarView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.GroupFriendItemAdapter;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;
import com.yanhua.message.view.NoticeAlertPopup;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.RongIM;
import io.rong.imkit.userinfo.RongUserInfoManager;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Group;

/**
 * 创建群组或者删除群内人员
 */
@Route(path = ARouterPath.CREATE_GROUP_ACTIVITY)
public class CreateGroupActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.ll_right)
    LinearLayout llRight;
    @BindView(R2.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;
    @BindView(R2.id.et_search)
    EditText etSearch;
    @BindView(R2.id.wave_side_bar)
    WaveSideBarView sideBar;
    @BindView(R2.id.ll_add)
    LinearLayout menuLinerLayout;// 可滑动的显示选中用户的View
    @BindView(R2.id.ll_top)
    LinearLayout llTop;
    @BindView(R2.id.ll_search)
    LinearLayout llSearch;

    private GroupFriendItemAdapter mAdapter;
    private List<FriendModel> selecteListData = new ArrayList<>();
    private List<FriendModel> allListData = new ArrayList<>();
    private String keyWord;

    private StringBuffer groupName;
    private int deleteKeyCount;//删除键的次数
    @Autowired(name = "selectedTeamMembers")
    public ArrayList<String> mSelectedTeamMemberAccounts;
    @Autowired(name = "type")
    public String type = "";
    @Autowired(name = "groupID")
    public String groupId = "";
    @Autowired(name = "currentGroupName")
    public String currentGroupName = "";

    @Autowired(name = "convertFriends")
    public ArrayList<FriendModel> convertFriends;

    private String doBtContent;

    private StsTokenModel stsTokenModel;

    private MyHandler mHandler = new MyHandler();

    private final static int UPLOAD_FILE_FAIL = 1000;
    private final static int UPLOAD_FILE_SUCCESS = 1001;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    if (null != file && file.exists()) {
                        file.delete();
                    }
                    httpUrl = (String) msg.obj;
                    if (!TextUtils.isEmpty(httpUrl)) {
                        doGroupMethod();
                    } else {
                        ToastUtils.showShort("创建失败");
                    }
                    break;
                case UPLOAD_FILE_FAIL:
                    hideLoading();

                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_create_group;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        if (type.equals(IM.group.GROUP_DELETE_MEMBER)) {
            tvHeader.setText("删除成员");
        } else if (type.equals(IM.group.GROUP_INVITE_MEMBER)) {
            tvHeader.setText("邀请成员");
        } else if (type.equals(IM.group.GROUP_MANAGE_CONVERT)) {
            tvHeader.setText("群转让");
        } else if (type.equals(IM.group.GROUP_CREATE)) {
            tvHeader.setText("创建群组");
        } else if (type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)) {
            tvHeader.setText("删除群管理员");
        } else if (type.equals(IM.group.GROUP_MANAGE_MANAGER_ADD)) {
            tvHeader.setText("添加群管理员");
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        basePresenter.getStsToken();
        if (type.equals(IM.group.GROUP_DELETE_MEMBER) || type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)) {
            doBtContent = "删除";
        } else if (type.equals(IM.group.GROUP_CREATE)) {
            doBtContent = "发起群聊";
        } else {
            doBtContent = "确定";
        }
        groupName = new StringBuffer();
        //设置按钮
//        tvConfirm.setSelected(true);
        tvConfirm.setText(doBtContent);

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
                    String inputContent = etSearch.getText().toString().trim();
                    List<FriendModel> user_temp = new ArrayList<FriendModel>();
                    for (FriendModel user : mAdapter.getmDataList()) {
                        String uesrname = YHStringUtils.pickName(user.getNickName(), user.getFriendRemark());

                        String nameTemp = uesrname.toLowerCase();
                        String inputContentTemp = inputContent.toLowerCase();
                        if (nameTemp.contains(inputContentTemp)) {
                            user_temp.add(user);
                        }
                        mAdapter.setItems(user_temp);
                    }
                } else {
                    mAdapter.setItems(allListData);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                String etContent = etSearch.getText().toString();
                if (TextUtils.isEmpty(etContent)) {
                    //监听点击的delete的次数
                    deleteKeyListener();
                }
            }
            return false;
        });

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new GroupFriendItemAdapter(mContext, mSelectedTeamMemberAccounts, type);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<FriendModel> itemModels = mAdapter.getmDataList();
            if (pos > itemModels.size() - 1) return;
            FriendModel model = mAdapter.getmDataList().get(pos);
            if (null != mSelectedTeamMemberAccounts && mSelectedTeamMemberAccounts.contains(model.getUserId()) && !type.equals(IM.group.GROUP_DELETE_MEMBER)
                    && !type.equals(IM.group.GROUP_MANAGE_CONVERT)
                    && !type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)) {
            } else {
                if (null != type && type.equals(IM.group.GROUP_MANAGE_CONVERT)) {
                    //不处理
                    convertRightToOther(model);
                } else {
                    doAddDelete(model);
                }
            }
        });

        sideBar.setOnTouchLetterChangeListener(letter -> {
            List<FriendModel> data = mAdapter.getmDataList();
            for (int i = 0; i < data.size(); i++) {
                FriendModel friend = data.get(i);
                String firstLetter = friend.getFirstLetter();
                if ("#".equals(letter) && "#".equals(firstLetter)) {
                    rvContent.scrollToPosition(i);
                    break;
                } else if (firstLetter.equalsIgnoreCase(letter)) {
                    rvContent.scrollToPosition(i);
                    break;
                }
            }
        });

        if (null != type && (type.equals(IM.group.GROUP_MANAGE_CONVERT) || type.equals(IM.group.GROUP_DELETE_MEMBER)
                || type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE) || type.equals(IM.group.GROUP_MANAGE_MANAGER_ADD))) {
            List<FriendModel> friends = convertFriends;

            List<FriendModel> friendList = new ArrayList<>();
            //过滤只有选中的
            for (FriendModel friend : friends) {
                String name = YHStringUtils.pickName(friend.getNickName(), friend.getFriendRemark());
                String pinyin = PinyinUtils.formatPinyin(name);
                friend.setNamePinYin(pinyin);
                String firstLetter = pinyin.substring(0, 1).toUpperCase();
                if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
                    firstLetter = "#";
                }
                friend.setFirstLetter(firstLetter);
                if (null != friend.getUserId() && !friend.getUserId().equals(mMMKV.getUserId())) {
                    friendList.add(friend);
                }
            }

            //整理排序
            Collections.sort(friendList);
            allListData.addAll(friendList);
            mAdapter.setItems(allListData);
            sideBar.setData(allListData, FriendModel::getFirstLetter, 0);
        } else {
            basePresenter.getUserFriend(mMMKV.getUserId(), "", 1, 10000);
        }
    }

    @OnClick(R2.id.ll_right)
    public void showSearchBar() {
        llSearch.setVisibility(View.VISIBLE);
        // 弹起键盘
        KeyboardUtils.showSoftInput();
        etSearch.requestFocus();
    }

    @OnClick(R2.id.tv_cancel)
    public void hideSearchBar() {
        llSearch.setVisibility(View.GONE);
        KeyboardUtils.hideSoftInput(this);
        etSearch.setText("");
    }

    private void convertRightToOther(FriendModel model) {

        List<FriendModel> dataList = mAdapter.getmDataList();

        for (FriendModel m : dataList) {
            if (m.equals(model)) {
                m.setSelected(true);
            } else {
                m.setSelected(false);
            }
        }

        selecteListData.clear();
        selecteListData.add(model);

        if (selecteListData.size() > 0) {
            tvConfirm.setText(doBtContent);
            tvConfirm.setEnabled(true);
        } else {
            tvConfirm.setText(doBtContent);
            tvConfirm.setEnabled(false);
        }

        mAdapter.notifyDataSetChanged();
    }

    private void doAddDelete(FriendModel model) {
        boolean selected = model.isSelected();

        model.setSelected(!selected);

        if (!selected) {
            selecteListData.add(model);
            showCheckImage(model);
        } else {
            selecteListData.remove(model);
            deleteImage(model);
        }

        if (selecteListData.size() > 0) {
            String num = (selecteListData.size() > 99) ? "99+" : String.valueOf(selecteListData.size());
            tvConfirm.setText(doBtContent + "(已选" + num + "人)");
            tvConfirm.setEnabled(true);
        } else {
            tvConfirm.setText(doBtContent);
            tvConfirm.setEnabled(false);
        }
        mAdapter.notifyDataSetChanged();
        if (!selecteListData.isEmpty()) {
            llTop.setVisibility(View.VISIBLE);
        } else {
            llTop.setVisibility(View.GONE);
        }
    }

    long clickTime = System.currentTimeMillis();

    private void deleteKeyListener() {
        if ((System.currentTimeMillis() - clickTime) > 3000) {
            clickTime = System.currentTimeMillis();

            deleteKeyCount = 0;
        } else {
            deleteKeyCount++;
            if (deleteKeyCount == 2) {
                deleteLastSelectList();
            }
        }
    }

    private void deleteLastSelectList() {
        if (selecteListData.size() > 0) {
            FriendModel model = selecteListData.get(0);//取出最后一个

            if (null != type && type.equals(IM.group.GROUP_MANAGE_CONVERT)) {
                //不处理
            } else {
                doAddDelete(model);
            }
        }
        clickTime = System.currentTimeMillis();
        deleteKeyCount = 0;
    }

    //显示选择的头像
    private void showCheckImage(FriendModel model) {
        // 包含TextView的LinearLayout
        // 参数设置
        LinearLayout.LayoutParams menuLinerLayoutParames = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(this).inflate(R.layout.header_item, null);
        CircleImageView images = view.findViewById(R.id.civ_avatar);
        menuLinerLayoutParames.setMargins(0, 0, DisplayUtils.dip2px(this, 12), 0);
        ImageLoaderUtil.loadImgCenterCrop(images, model.getUserPhoto(), R.drawable.place_holder);
        // 设置id，方便后面删除
        view.setTag(model);
        menuLinerLayout.addView(view, 0, menuLinerLayoutParames);
    }

    //删除选择的头像
    private void deleteImage(FriendModel model) {
        View view = menuLinerLayout.findViewWithTag(model);
        if (null != view) {
            menuLinerLayout.removeView(view);
        }
    }

    @Override
    public void handleGetUserFriendSuccess(List<FriendModel> friends) {
        if (CollectionUtils.isNotEmpty(friends)) {
            allListData.clear();
            for (FriendModel friend : friends) {
                String name = YHStringUtils.pickName(friend.getNickName(), friend.getFriendRemark());
                String pinyin = PinyinUtils.formatPinyin(name);
                friend.setNamePinYin(pinyin);
                String firstLetter = pinyin.substring(0, 1).toUpperCase(); // 获取拼音首字母并转成大写
                if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
                    firstLetter = "#";
                }
                friend.setFirstLetter(firstLetter);

                //从私聊中创建群组方式
                if (type.equals(IM.group.GROUP_CREATE) && (null != mSelectedTeamMemberAccounts && mSelectedTeamMemberAccounts.size() > 0)) {
                    for (String uid : mSelectedTeamMemberAccounts) {
                        if (uid.equals(friend.getUserId())) {
                            doAddDelete(friend);//顶部已选显示头衔
                        }
                    }
                }
            }

            //整理排序
            Collections.sort(friends);
            allListData.addAll(friends);
            mAdapter.setItems(allListData);
            sideBar.setData(allListData, FriendModel::getFirstLetter, 0);

            rvContent.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
        } else {
            rvContent.setVisibility(View.GONE);
            emptyLayout.setVisibility(View.VISIBLE);
            emptyLayout.setErrorType(EmptyLayout.NODATA);
//                emptyLayout.setErrorImag(R.mipmap.empty_point);
//                emptyLayout.setErrorMessage("暂无记录");

//            emptyLayout.setErrorImag(R.mipmap.empty_data);
            emptyLayout.setErrorMessage("这里没有相关记录哦");
        }
    }

    @Override
    public void handleErrorMessage(String message) {
        super.handleErrorMessage(message);
        if (null != file && file.exists()) {
            file.delete();
        }

        ToastUtils.showShort(message);
    }

    @OnClick({R2.id.iv_left})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.iv_left) {
            onBackPressed();
        }
    }

    /**
     * 先处理头像逻辑
     */
    private void doCreateGroupPhoto() {
        if (type.equals(IM.group.GROUP_CREATE)) {
            //发起创建群组
            //创建群组的头像，然后进行上传
            createGroupPhoto();
        } else if (type.equals(IM.group.GROUP_DELETE_MEMBER)) {
            deleteMemberGroupPhoto();
        } else if (type.equals(IM.group.GROUP_INVITE_MEMBER)) {
            inviteMemberGroupPhoto();
        }
    }

    /**
     * 开始邀请新的群成员
     */
    private void inviteMemberGroupPhoto() {
        ///此处的all为自己的好友+ selecteListData + 自己
        if (null != convertFriends && convertFriends.size() > 0) {

            convertFriends.addAll(selecteListData);
            buildGroupInfo(convertFriends, false);
        }
    }

    private File file;//上传的文件

    private void buildGroupInfo(List<FriendModel> listData, boolean isCreate) {
        int size = listData.size();

        ArrayList photos = new ArrayList();//群头像构建

        if (isCreate) {
            //群主
            UserInfo userInfo = UserManager.getInstance().getUserInfo();

            String masterNickName = userInfo.getNickName();
            String masterImg = userInfo.getImg();

            photos.add(masterImg);
            groupName.append(masterNickName).append("、");
        }
        if (size < IM.group.MAX_HEADER_PHOTO) {//根据组合头像的最多（1+8）9个
            for (int i = 0; i < listData.size(); i++) {
                FriendModel model = listData.get(i);
                photos.add(model.getUserPhoto());

                groupName.append(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark())).append("、");
            }
        } else {
            for (int i = 0; i < IM.group.MAX_HEADER_PHOTO; i++) {//0-7
                FriendModel model = listData.get(i);
                photos.add(model.getUserPhoto());

                groupName.append(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark())).append("、");
            }
        }

        //初始化需要得到的数组
        String[] array = new String[photos.size()];

        //使用for循环得到数组
        for (int i = 0; i < photos.size(); i++) {
            array[i] = (String) photos.get(i);
        }

        CombineBitmap.init(mContext)
                .setLayoutManager(new WechatLayoutManager())
                .setSize(180)
                .setGap(3)
                .setGapColor(Color.parseColor("#e5e5e5"))
                .setUrls(array)
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onComplete(Bitmap bitmap) {
                        try {
                            file = BitmapUtils.compressImage(mContext, bitmap);
                            uploadFile(file);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .build();
    }

    private void uploadFile(File file) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                msg.obj = httpUrl;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    /**
     * 开始删除群成员
     */
    private void deleteMemberGroupPhoto() {
        ///此处的all为群成员（不包含自己）- selecteListData + 自己
        if (null != convertFriends && convertFriends.size() > 0) {
            int size = selecteListData.size();

            FriendModel first = selecteListData.get(0);
            String names = YHStringUtils.pickName(first.getNickName(), first.getFriendRemark());
            String content = "";
            if (size > 1) {
                content = names + "等" + size + "人";
            } else {
                content = names;
            }

            CommonDialog commonDialog = new CommonDialog(mContext, "确定要删除群成员" + content + "?", "", "确定", "取消");
            new XPopup.Builder(mContext).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();

                convertFriends.removeAll(selecteListData);

                buildGroupInfo(convertFriends, false);
            });

            commonDialog.setOnCancelListener(() -> {
                commonDialog.dismiss();
            });
        }
    }

    /**
     * 开始创建群组第一步
     */
    private void createGroupPhoto() {
        if (selecteListData.size() > 1) {

            buildGroupInfo(selecteListData, true);
        } else {
            ToastUtils.showShort("至少还需选择一人组成群");
        }
    }

    private void checkStoragePermission() {
        String tipRight = "设备的储存、相机";

        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                doCreateGroupPhoto();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
//                ToastUtil.makeCustomToast(mContext, R.string.denied_createstorage_permission, Toast.LENGTH_LONG);
                ToastUtils.showShort(R.string.denied_createstorage_permission);
            }
        });
    }

    @OnClick({R2.id.tv_confirm})
    public void onCommitClicked(View view) {
        if (selecteListData.size() > 0) {
            if (FastClickUtil.isFastClick(FastClickUtil.MIN_DURATION_1SECOND_TIME)) {
                //假如为群转让
                if (null != type && type.equals(IM.group.GROUP_MANAGE_CONVERT)) {
                    //转让群
                    if (selecteListData.size() > 0) {
                        FriendModel model = selecteListData.get(0);
                        NoticeAlertPopup convertAlertPopup = new NoticeAlertPopup(this, "转让该群", "确定要把群转让给 " + YHStringUtils.pickName(model.getNickName(), model.getFriendRemark()), "确定");
                        new XPopup.Builder(mContext).asCustom(convertAlertPopup).show();
                        convertAlertPopup.setOnConfirmListener(() -> {
                            //提交  转让该群
                            HashMap params = new HashMap();
                            params.put("groupId", groupId);
                            params.put("memberId", model.getUserId());
                            basePresenter.convertGroupManagerr(params);
                        });
                    }
                } else if (type.equals(IM.group.GROUP_MANAGE_MANAGER_ADD)) {
                    //添加群管理员
                    inviteGroupManager();
                } else if (type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)) {
                    //删除群管理员
                    deleteGroupManager();
                } else {
                    //动态获取读写权限，邀请，新建、删除，需要更改头像
                    checkStoragePermission();
                }
            }
        }
    }

    private void doGroupMethod() {
        if (type.equals(IM.group.GROUP_CREATE)) {
            //发起创建群组
            //创建群组的头像，然后进行上传
            basePresenter.createGroup(buildCreateGroupParams(httpUrl));
        } else if (type.equals(IM.group.GROUP_DELETE_MEMBER)) {
            deleteGroupMembers();
        } else if (type.equals(IM.group.GROUP_INVITE_MEMBER)) {
            inviteGroupMembers();
        }
    }

    private void inviteGroupManager() {
        basePresenter.updateGroupAdminister(buildGroupManageParams(3));
    }

    private void deleteGroupManager() {
        int size = selecteListData.size();

        FriendModel first = selecteListData.get(0);
        String names = YHStringUtils.pickName(first.getNickName(), first.getFriendRemark());
        String content = "";
        if (size > 1) {
            content = names + "等" + size + "人";
        } else {
            content = names;
        }

        CommonDialog commonDialog = new CommonDialog(mContext, "要删除群管理员" + content + "?", "", "确定", "取消");
        new XPopup.Builder(mContext).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(() -> {
            commonDialog.dismiss();

            basePresenter.updateGroupAdminister(buildGroupManageParams(2));
        });

        commonDialog.setOnCancelListener(() -> {
            commonDialog.dismiss();
        });
    }

    private HashMap buildGroupManageParams(int type) {
        HashMap map = new HashMap();

        ArrayList<String> groupMemberList = new ArrayList<>();

        for (FriendModel model : selecteListData) {
            groupMemberList.add(model.getUserId());
        }

        map.put("type", type);
        map.put("groupId", groupId);
        map.put("memberIds", groupMemberList);

        return map;
    }


    private void inviteGroupMembers() {
        basePresenter.addMember(buildGroupParams());
    }

    private void deleteGroupMembers() {
        basePresenter.deleteMember(buildGroupParams());
    }

    private HashMap buildGroupParams() {
        HashMap map = new HashMap();

        //TODO处理groupMemberList
        ArrayList<MemberItem> groupMemberList = new ArrayList<>();

        for (FriendModel model : selecteListData) {
            MemberItem item = new MemberItem();
            item.setMemberId(model.getUserId());
            item.setNickName(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark()));
            item.setType(2);//类型：1群主、2群员、3群管理
            groupMemberList.add(item);
        }

        map.put("groupImg", httpUrl);
        map.put("groupId", groupId);
        map.put("groupName", currentGroupName);
        map.put("groupMemberList", groupMemberList);

        return map;
    }

    String httpUrl;

    /**
     * 构建请求参数
     *
     * @return
     */
    private HashMap buildCreateGroupParams(String url) {
        HashMap map = new HashMap();

        //TODO 处理groupMemberList
        ArrayList<MemberItem> groupMemberList = new ArrayList<>();
        //群主
        UserInfo userInfo = UserManager.getInstance().getUserInfo();


        MemberItem groupMaster = new MemberItem();
        groupMaster.setMemberId(userInfo.getId());
        groupMaster.setNickName(userInfo.getNickName());
        groupMaster.setType(1);//类型：1群主、2群员、3群管理
        groupMemberList.add(groupMaster);
        for (FriendModel model : selecteListData) {
            MemberItem item = new MemberItem();
            item.setMemberId(model.getUserId());
            item.setNickName(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark()));
            item.setType(2);//类型：1群主、2群员、3群管理
            groupMemberList.add(item);
        }

        String createGroupName;
        if (groupName.length() > 15) {
            createGroupName = groupName.substring(0, 15);//取15
        } else {
            createGroupName = groupName.substring(0, groupName.length() - 1);
        }

        if (createGroupName.endsWith("、")) {//去掉尾部的顿号
            createGroupName = createGroupName.substring(0, createGroupName.length() - 1);
        }

        map.put("groupImg", url);
        map.put("groupMemberList", groupMemberList);
        map.put("groupName", createGroupName);
//        map.put("showName",0);
        return map;
    }

    @Override
    public void setGroupInfoSuccess() {
        updateGroupInfoResult(currentGroupName);
    }

    /**
     * 处理成功后更改
     */
    private void updateGroupInfo(String groupName) {
        HashMap params = basePresenter.buildSetGroupInfo(null, groupId, httpUrl, "", "", "", "");
        basePresenter.setGroupInfo(params);
    }

    /**
     * 处理成功后更改
     */
    private void updateGroupInfoResult(String groupName) {
        Uri uri = Uri.parse(httpUrl);
        Group group = new Group(groupId, groupName, uri);

        RongUserInfoManager.getInstance().refreshGroupInfoCache(group);//刷新

        // 删除或者邀请成员的时候
        this.finish();
        MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("100"));
        EventBus.getDefault().post(event);
    }

    @Override
    public void dealGroupSuccess() {
        if (type.equals(IM.group.GROUP_DELETE_MEMBER) || type.equals(IM.group.GROUP_INVITE_MEMBER)) {
            String createGroupName;
            if (TextUtils.isEmpty(currentGroupName)) {
                if (groupName.length() > 15) {
                    createGroupName = groupName.substring(0, 15);//取15
                } else {
                    createGroupName = groupName.substring(0, groupName.length() - 1);
                }

                if (createGroupName.endsWith("、")) {//去掉尾部的顿号
                    createGroupName = createGroupName.substring(0, createGroupName.length() - 1);
                }
            } else {
                createGroupName = currentGroupName;
            }
            updateGroupInfo(createGroupName);
        } else {
            MessageEvent event = new MessageEvent(CommonConstant.IM_GROUP_RELATE, new EventUpdateGroupInfo("100"));
            EventBus.getDefault().post(event);

            this.finish();
        }
    }

    @Override
    public void createGroupSuccess(GroupResult result) {
        Uri uri = Uri.parse(httpUrl);
        RongIM.setGroupInfoProvider(groupId -> {
                    Group groupinfo = new Group(result.getGroupId(), result.getGroupName(), uri);
                    RongUserInfoManager.getInstance().refreshGroupInfoCache(groupinfo);//刷新
                    return groupinfo;
//                    return null;
                }
                , true);
        this.finish();
        //创建成功后跳转至聊天页面
        RouteUtils.routeToConversationActivity(mContext, Conversation.ConversationType.GROUP, result.getGroupId(), null);
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);

        DiscoCacheUtils.getInstance().setStsToken(json);
    }
}
