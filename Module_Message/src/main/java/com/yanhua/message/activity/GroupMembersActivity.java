package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.lqr.adapter.LQRAdapterForRecyclerView;
import com.lqr.adapter.LQRViewHolderForRecyclerView;
import com.lqr.recyclerview.LQRRecyclerView;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.EventUpdateGroupInfo;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

/**
 * 查看群成员（群主和其他人员）
 */
@Route(path = ARouterPath.GROUP_MEMBERS_ACTIVITY)
public class GroupMembersActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.rvMember)
    LQRRecyclerView rvMember;

    @BindView(R2.id.et_search)
    EditText etSearch;

    @Autowired(name = "targetId")
    String targetId;
    @Autowired(name = "groupName")
    String groupName;


    private LQRAdapterForRecyclerView<DiscoGroupMember> mAdapter;
    private ArrayList<DiscoGroupMember> mGroupMembers = new ArrayList<>();
    private List<DiscoGroupMember> mData = new ArrayList<>();
    private boolean isMaster;
    private String keyWord;

    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_members;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventUpdateGroupInfo(MessageEvent info) {
        EventUpdateGroupInfo event = (EventUpdateGroupInfo) info.getSerializable();
        if (null != event.getType()) {
            loadData();
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("群成员");

        loadData();

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
                    String inputContent = etSearch.getText().toString().trim();
                    ArrayList<DiscoGroupMember> groupMembers = new ArrayList<>();
                    for (DiscoGroupMember user : mAdapter.getData()) {
                        String name = YHStringUtils.pickName(user.getNickName(),user.getFriendRemark());
                        if (null != name && name.contains(inputContent)) {
                            groupMembers.add(user);
                        }
                        mAdapter.setData(groupMembers);
                    }
                } else {
                    mAdapter.setData(mGroupMembers);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void loadData() {
        //获取群成员信息
        basePresenter.getGroupChat(targetId);
        basePresenter.queryMember(targetId);
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new LQRAdapterForRecyclerView<DiscoGroupMember>(mContext, mData, R.layout.item_member_info) {
                @Override
                public void convert(LQRViewHolderForRecyclerView helper, DiscoGroupMember item, int position) {
                    ImageView ivHeader = helper.getView(R.id.ivHeader);

                    if (isMaster && position >= mData.size() - 2) {//+和-
                        if (position == mData.size() - 2) {//+
                            ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                        } else {//-
                            ivHeader.setImageResource(R.mipmap.ic_delete_team_member);
                        }
                        helper.setText(R.id.tvName, "");
                    } else if (!isMaster && position >= mData.size() - 1) {//+
                        ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                        helper.setText(R.id.tvName, "");
                    } else {
                        Glide.with(mContext).load(item.getUserImg()).centerCrop().into(ivHeader);
                        helper.setText(R.id.tvName, YHStringUtils.pickName(item.getNickName(),item.getFriendRemark()));
                    }

                    int type = item.getType();

                    switch (type) {
                        case 1:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.VISIBLE);
                            helper.setText(R.id.tvGroupMasterTag, "群主");
                            helper.setBackgrounResource(R.id.tvGroupMasterTag, R.drawable.shape_bg_rff7f02_stroke1);
                            break;
                        case 2:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.GONE);
                            break;
                        case 3:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.VISIBLE);
                            helper.setText(R.id.tvGroupMasterTag, "管理员");
                            helper.setBackgrounResource(R.id.tvGroupMasterTag, R.drawable.shape_bg_r33c35c_stroke1);
                            break;
                        default:
                            helper.setViewVisibility(R.id.tvGroupMasterTag, View.GONE);
                            break;
                    }
                }
            };
            mAdapter.setOnItemClickListener((helper, parent, itemView, position) -> {
                if (isMaster && position >= mData.size() - 2) {//+和-
                    if (position == mData.size() - 2) {//+
                        inviteOrDeleteMember(IM.group.GROUP_INVITE_MEMBER);
                    } else {//-
                        inviteOrDeleteMember(IM.group.GROUP_DELETE_MEMBER);
                    }
                } else if (!isMaster && position >= mData.size() - 1) {//
                    //非群主只能邀请成员
                    inviteOrDeleteMember(IM.group.GROUP_INVITE_MEMBER);
                } else {
//                    seeUserInfo //查看用户信息
                    ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", mData.get(position).getMemberId())
                            .navigation();
                }
            });
            rvMember.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChangedWrapper();
        }
    }


    /**
     * 邀请/删除成员
     */
    private void inviteOrDeleteMember(String type) {
        ArrayList<String> selectedTeamMembers = new ArrayList<String>();

        ArrayList<FriendModel> members = new ArrayList<>();
        for (int i = 0; i < mGroupMembers.size(); i++) {
            DiscoGroupMember member = mGroupMembers.get(i);

            selectedTeamMembers.add(member.getMemberId());
            FriendModel friendModel = new FriendModel();

            friendModel.setNickName(YHStringUtils.pickName(member.getNickName(),member.getFriendRemark()));
            friendModel.setUserId(member.getMemberId());
            friendModel.setUserPhoto(member.getUserImg());

            members.add(friendModel);
        }

        ARouter.getInstance().build(ARouterPath.CREATE_GROUP_ACTIVITY)
                .withSerializable("selectedTeamMembers", selectedTeamMembers)
                .withString("groupID", targetId)
                .withSerializable("convertFriends",members)
                .withString("currentGroupName", groupName)
                .withString("type", type).navigation();
    }

    @Override
    public void getGroupInfoSuccess(GroupInfo data) {
        //群信息
        if (null != data) {
            groupName = data.getGroupName();

            String groupMaster = data.getGroupMaster();
            isMaster = groupMaster.equals(mMMKV.getUserId());
        }
    }

    @Override
    public void queryMemberSuccess(List<DiscoGroupMember> listData) {
        if (null != listData && listData.size() > 0) {
            //获取群成员列表成功
            mGroupMembers.clear();
            mGroupMembers.addAll(listData);

            setTitle("群成员(" + mGroupMembers.size() + ")");

            mData.clear();

            if (isMaster) {
                //再加两个
                DiscoGroupMember inviteIcon = new DiscoGroupMember();
                inviteIcon.setType(100);//+
                DiscoGroupMember deleteIcon = new DiscoGroupMember();
                deleteIcon.setType(101);//-
                listData.add(inviteIcon);
                listData.add(deleteIcon);
            } else {
                //加一个
                DiscoGroupMember inviteIcon = new DiscoGroupMember();
                inviteIcon.setType(100);//+
                listData.add(inviteIcon);
            }

//            //要不要群主放在第一位
            Collections.sort(listData);

            mData.addAll(listData);

            setAdapter();
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
