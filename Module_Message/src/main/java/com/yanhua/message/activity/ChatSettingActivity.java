package com.yanhua.message.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.dmingo.optionbarview.OptionBarView;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MemberItem;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.TypeWayListBottomPopup;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.ChatMessagePresenter;
import com.yanhua.message.presenter.contract.ChatMessageContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.IMCenter;
import io.rong.imlib.IRongCoreCallback;
import io.rong.imlib.IRongCoreEnum;
import io.rong.imlib.RongCoreClient;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;

@Route(path = ARouterPath.PRIVATE_CHAT_SETTING_ACTIVITY)
public class ChatSettingActivity extends BaseMvpActivity<ChatMessagePresenter> implements ChatMessageContract.IView {

    @BindView(R2.id.ivAvatar)
    CircleImageView ivAvatar;
    @BindView(R2.id.tvNickname)
    TextView tvNickname;
    @BindView(R2.id.oivfindMsgRecord)
    OptionBarView oivfindMsgRecord;
    @BindView(R2.id.oivClearMsgRecord)
    OptionBarView oivClearMsgRecord;
    @BindView(R2.id.switch_top)
    OptionBarView switchTop;
    @BindView(R2.id.switch_notify)
    OptionBarView switchNotify;
    @BindView(R2.id.switchBlock)
    OptionBarView switchBlock;
    @BindView(R2.id.obvComplaint)
    OptionBarView obvComplaint;

    @BindView(R2.id.btn_chat_set_follow)
    Button btnFollow;

    @Autowired(name = "targetId")
    String targetId;
    private ContentUserInfoModel mModel;

    @Override
    protected void creatPresent() {
        basePresenter = new ChatMessagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_chat_setting;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, btnFollow);

        setTitle("聊天设置");

        //聊天记录
        setChatHistory();
        //免打扰设置
        avoidNotify();
        // 获取会话置顶状态
        setConversionTop();

        //投诉
        obvComplaint.setOnClickListener(view -> {
            /**
             * 举报用户主页
             */
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                        .withString("businessID", targetId)
                        .withInt("reportType", YXConfig.reportType.person)
                        .navigation();
            });
        });
    }


    private void setChatHistory() {
        oivfindMsgRecord.setOnClickListener(view -> {
            String currentName = YHStringUtils.pickName(mModel.getNickName(), mModel.getFriendRemark());//

            ArrayList<MemberItem> chatMembers = new ArrayList<>();

            //当前登录者
            MemberItem currentUser = new MemberItem();
            UserInfo userInfo = UserManager.getInstance().getUserInfo();
            currentUser.setNickName(userInfo.getNickName());
            currentUser.setImgHead(userInfo.getImg());
            currentUser.setMemberId(userInfo.getId());

            //当前聊天对象
            MemberItem chatUser = new MemberItem();
            chatUser.setNickName(currentName);
            chatUser.setImgHead(mModel.getPhoto());
            chatUser.setMemberId(mModel.getId());

            chatMembers.add(currentUser);
            chatMembers.add(chatUser);

            ARouter.getInstance().build(ARouterPath.CHAT_HISTORY_MSGLIST_ACTIVITY)
                    .withString("targetId", targetId)
                    .withSerializable("chatMembers", chatMembers)
                    .withString("currentName", currentName)

                    .navigation();
        });
    }

    private void setBlackList() {
        //查询用户是否在黑名单中
        RongIMClient.getInstance().getBlacklistStatus(targetId, new RongIMClient.ResultCallback<RongIMClient.BlacklistStatus>() {
            @Override
            public void onSuccess(RongIMClient.BlacklistStatus blacklistStatus) {
                boolean isINBlackList = blacklistStatus == RongIMClient.BlacklistStatus.IN_BLACK_LIST;

                if (isINBlackList) {
                    switchBlock.setChecked(true);
                } else {
                    switchBlock.setChecked(false);
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {

            }
        });


        //黑名单
        switchBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (switchBlock.isChecked()) {
                    RongIMClient.getInstance().removeFromBlacklist(targetId, new RongIMClient.OperationCallback() {
                        @Override
                        public void onSuccess() {
                            //不再调用黑名单，后台通过融云调用
                            basePresenter.addOrCancelBlack(targetId);
                            switchBlock.setChecked(false);
                        }

                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });
                } else {
                    CommonDialog commonDialog = new CommonDialog(mContext, "屏蔽后，对方将无法搜索到你，也不能再给你发私信", "", "确定", "取消",
                            mContext.getResources().getColor(R.color.color_F42603),
                            mContext.getResources().getColor(R.color.color_EEEEEE));
                    new XPopup.Builder(mContext).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();


                        RongIMClient.getInstance().addToBlacklist(targetId, new RongIMClient.OperationCallback() {
                            @Override
                            public void onSuccess() {
                                basePresenter.addOrCancelBlack(targetId);
                                switchBlock.setChecked(true);
                            }

                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        });
                    });

                    commonDialog.setOnCancelListener(() -> commonDialog.dismiss());

                }
            }
        });
    }

    private void setConversionTop() {
        // 获取会话置顶状态
        //obv_isTop,obv_isAvoid
        switchTop.setSplitMode(true);
        RongCoreClient.getInstance().getConversationTopStatus(targetId, Conversation.ConversationType.PRIVATE,
                new IRongCoreCallback.ResultCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean aBoolean) {
                        switchTop.setChecked(aBoolean);
                    }

                    @Override
                    public void onError(IRongCoreEnum.CoreErrorCode coreErrorCode) {

                    }
                });
        switchTop.setOnSwitchCheckedChangeListener((view, isChecked) ->
                IMCenter.getInstance().setConversationToTop(
                        Conversation.ConversationType.PRIVATE,
                        targetId,
                        isChecked,//是否置顶, true 为置顶, false 为取消置顶
                        true,//会话不存在时，是否创建会话
                        new RongIMClient.ResultCallback<Boolean>() {
                            /**
                             * 成功回调
                             */
                            @Override
                            public void onSuccess(Boolean success) {
                            }

                            /**
                             * 错误回调
                             * @param errorCode 错误码
                             */
                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        }));
    }

    private void avoidNotify() {
        //免打扰
        switchNotify.setSplitMode(true);
        // 获取免打扰状态
        RongIMClient.getInstance().getConversationNotificationStatus(Conversation.ConversationType.PRIVATE, targetId, new RongIMClient.ResultCallback<Conversation.ConversationNotificationStatus>() {
            /**
             * 成功回调
             * @param status 消息提醒状态
             */
            @Override
            public void onSuccess(Conversation.ConversationNotificationStatus status) {
                if (status == Conversation.ConversationNotificationStatus.DO_NOT_DISTURB) {
                    switchNotify.setChecked(true);
                } else {
                    switchNotify.setChecked(false);
                }
            }

            /**
             * 错误回调
             * @param errorCode 错误码
             */
            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
            }
        });
        switchNotify.setOnSwitchCheckedChangeListener((view, isChecked) -> {
            IMCenter.getInstance().setConversationNotificationStatus(
                    Conversation.ConversationType.PRIVATE,
                    targetId,
                    isChecked ? Conversation.ConversationNotificationStatus.DO_NOT_DISTURB : Conversation.ConversationNotificationStatus.NOTIFY,
                    new RongIMClient.ResultCallback<Conversation.ConversationNotificationStatus>() {

                        /**
                         * 成功回调
                         * @param status 消息提醒状态
                         */
                        @Override
                        public void onSuccess(Conversation.ConversationNotificationStatus status) {
                        }

                        /**
                         * 错误回调
                         * @param errorCode 错误码
                         */
                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });
        });
    }


    @OnClick({R2.id.oivClearMsgRecord, R2.id.llCreateGroup})
    public void onClickClearMsgRecord(View view) {
        if (view.getId() == R.id.oivClearMsgRecord) {
            ArrayList<TypeWayModel> list = new ArrayList<>();

            list.add(new TypeWayModel("1", "清空聊天记录"));

            TypeWayListBottomPopup popup = new TypeWayListBottomPopup(mContext, list);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();

                IMCenter.getInstance().deleteMessages(Conversation.ConversationType.PRIVATE, targetId, new RongIMClient.ResultCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean bool) {
                        ToastUtils.showShort("已清空聊天记录");
                    }

                    @Override
                    public void onError(RongIMClient.ErrorCode errorCode) {
                        ToastUtils.showShort(errorCode + "");
                    }
                });

            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        } else if (view.getId() == R.id.llCreateGroup) {

            ArrayList<String> selectedTeamMembers = new ArrayList<>();

            selectedTeamMembers.add(UserManager.getInstance().getUserId());
            selectedTeamMembers.add(mModel.getId());

            //创建群组
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.CREATE_GROUP_ACTIVITY)
                    .withSerializable("selectedTeamMembers", selectedTeamMembers)
                    .withString("type", IM.group.GROUP_CREATE)
                    .navigation());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //查询用户是否在黑名单中
        setBlackList();

        basePresenter.getContentUserDetail(targetId);
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
    }

    @Override
    public void updateBlackUserSuccess() {
        //查询用户是否在黑名单中

    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.btn_chat_set_follow) {//
            toFollow();//
        }
    }

    @OnClick({R2.id.ivAvatar})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.ivAvatar) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", targetId)
                    .navigation();
        }
    }

    private void setFollowButtonText(int followStatus) {
        switch (followStatus) {
            case 1:
                btnFollow.setText("关注");
                break;
            case 2:
                btnFollow.setText("已关注");
                break;
            case 3:
                btnFollow.setText("回粉");
                break;
            case 4:
                btnFollow.setText("互相关注");
                break;
        }
    }

    @Override
    public void updateFollowUserSuccess() {
        boolean isFollow = mModel.isIsFollow();
        mModel.setIsFollow(!isFollow);
        btnFollow.setSelected(!isFollow);

        int followStatus = mModel.getFollowStatus();
        switch (followStatus) {
            case 1:
                mModel.setFollowStatus(2);
                break;
            case 2:
                mModel.setFollowStatus(1);
                break;
            case 3:
                mModel.setFollowStatus(4);
                break;
            case 4:
                mModel.setFollowStatus(3);
                break;
        }

        setFollowButtonText(mModel.getFollowStatus());

        if (isFollow) {
            // 取消关注
            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_CHAT_UNFOLLOW, mModel.getId()));
        } else {
            // 关注
            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_CHAT_FOLLOW, mModel.getId()));
        }
    }

    private void toFollow() {
        String uId = mMMKV.getUserId();
        if (uId.equals(targetId)) {
            ToastUtils.showShort("不能关注自己");

            return;
        }
        boolean isFollow = mModel.isIsFollow();

        if (isFollow) {
            AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
            new XPopup.Builder(mContext).asCustom(alertPopup).show();
            alertPopup.setOnConfirmListener(() -> {
                alertPopup.dismiss();
                basePresenter.cancelFollowUser(targetId);
            });
            alertPopup.setOnCancelListener(alertPopup::dismiss);
        } else {
            basePresenter.followUser(targetId);
        }
    }

    @Override
    public void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        if (model != null) {
            mModel = model;
            String inviteCode = model.getInviteCode();

            boolean isFollow = model.isIsFollow();
            btnFollow.setSelected(isFollow);

            setFollowButtonText(model.getFollowStatus());

            if (TextUtils.isEmpty(model.getPhoto())) {
                ImageLoaderUtil.loadImg(ivAvatar, R.drawable.place_holder);
            } else {
                ImageLoaderUtil.loadImg(ivAvatar, model.getPhoto());
            }
            tvNickname.setText(YHStringUtils.pickName(model.getNickName(), model.getFriendRemark()));//mModel
        }
    }
}