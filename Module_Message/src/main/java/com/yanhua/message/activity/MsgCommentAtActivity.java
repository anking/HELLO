package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.MsgCommentListAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_COMMENT_AT_ACTIVITY)
public class MsgCommentAtActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    private MsgCommentListAdapter mAdapter;
    private int total;
    private List<MsgCommentAtModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_msg_comment_at;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("评论和@");

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MsgCommentListAdapter(mContext);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<MsgCommentAtModel> list = mAdapter.getmDataList();
            MsgCommentAtModel item = list.get(pos);


            int type = item.getType();
            switch (type) {
                case YXConfig.TYPE_MOMENT:
                    int contentType = item.getContentType();

                    if (contentType == 2) {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentModel", null)
                                .withSerializable("contentId", item.getContentId())
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withInt("module", 0)
                                .navigation();
                    } else {
                        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                .withString("id", item.getContentId())
                                .navigation();
                    }
                    break;

                case YXConfig.TYPE_INVITE:
                    ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                            .withString("id", item.getContentId()).navigation();
                    break;
                case YXConfig.TYPE_BREAK_NEWS:
                case YXConfig.TYPE_STRATEGY:
                    PageJumpUtil.jumpNewsDetailPage(item.getContentId(), item.getType());
                    break;
                case YXConfig.TYPE_BEST:
                    ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                            .withSerializable("contentId", item.getContentId())
                            .withInt("discoBest", 1)//电音集锦
                            .withString("categoryId", "")
                            .withString("city", "")
                            .withInt("module", 0)
                            .navigation();
                    break;
                case YXConfig.TYPE_9BAR:
                    //去酒吧
                    ARouter.getInstance()
                            .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                            .withString("barId", item.getContentId())
                            .navigation();
                    break;
                default:
                    break;
            }
        });

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);

        //长按
        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
//            currentPosition = pos;
//            CommentModel model = mCommentList.get(pos);
//            boolean myself = UserManager.getInstance().isMyself(model.getUserId());
            boolean myself = pos % 6 == 3;

            CommentBoardPopup popup = new CommentBoardPopup(mActivity, false, true, true, !myself, myself, true, true);
            popup.setOnActionItemListener(actionType -> {
                switch (actionType) {
                    case CommentBoardPopup.ACTION_TOP:
//                        toTop(pos);
                        break;
                    case CommentBoardPopup.ACTION_REPLY:
                        //回复
//                      replyComment(model);
                        break;
                    case CommentBoardPopup.ACTION_COPY:
//                            String content = model.getCommentDesc();
//                            YHStringUtils.copyContent(mActivity, content);
                        break;
                    case CommentBoardPopup.ACTION_REPORT:
//                            toReport(pos);
                        break;
                    case CommentBoardPopup.ACTION_LIKE:
//                            toLike(pos);
                    case CommentBoardPopup.ACTION_DETAIL:
//                            toDetail(pos);
                        break;
                    case CommentBoardPopup.ACTION_DELETE:
//                        toDelete(pos);
                        break;
                }
            });

            new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
        });

    }

    private void getListData(int page) {
        current = page;

        basePresenter.getCommentAtList(current, size);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }

    @Override
    public void handleMsgCommentAtList(ListResult<MsgCommentAtModel> listResult) {
        List<MsgCommentAtModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        basePresenter.dealCommentAtReadAll();
    }
}