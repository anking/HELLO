package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.lqr.adapter.LQRAdapterForRecyclerView;
import com.lqr.adapter.LQRViewHolderForRecyclerView;
import com.lqr.recyclerview.LQRRecyclerView;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.GroupPresenter;
import com.yanhua.message.presenter.contract.GroupContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 群管理
 */
@Route(path = ARouterPath.GROUP_MANAGE_SET_ACTIVITY)
public class GroupManageSetActivity extends BaseMvpActivity<GroupPresenter> implements GroupContract.IView {

    @BindView(R2.id.rvMember)
    LQRRecyclerView rvMember;

    //这两是从外面传入的，先放此处
//    @Autowired(name = "selectedTeamMembers")
//    public ArrayList<String> mSelectedTeamMemberAccounts;
//    @Autowired(name = "convertFriends")
//    public ArrayList<FriendModel> convertFriends;

    @Autowired(name = "groupID")
    public String groupId = "";
    @Autowired(name = "currentGroupName")
    public String currentGroupName = "";

    private LQRAdapterForRecyclerView<DiscoGroupMember> mAdapter;
    private List<DiscoGroupMember> mData = new ArrayList<>();


    @Override
    protected void creatPresent() {
        basePresenter = new GroupPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_group_manage_set;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle("设置群管理员");
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //获取群成员信息
        basePresenter.queryMember(groupId);

    }

    private ArrayList<DiscoGroupMember> mGroupMembers = new ArrayList<>();

    @Override
    public void queryMemberSuccess(List<DiscoGroupMember> listDataAll) {
        if (null != listDataAll && listDataAll.size() > 0) {
            //获取群成员列表成功
            mGroupMembers.clear();
            mGroupMembers.addAll(listDataAll);

            if (null != mGroupMembers) {
                List<DiscoGroupMember> listData = new ArrayList<>();
                mData.clear();

                //这里需要根据群成员列表进行区分是群主还是群管理员
                for (int i = 0; i < mGroupMembers.size(); i++) {
                    DiscoGroupMember discoGroupMember = mGroupMembers.get(i);

                    int type = discoGroupMember.getType();
                    switch (type) {//群主
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3://管理员
                            listData.add(discoGroupMember);
                            break;
                    }
                }

                //再加两个
                DiscoGroupMember inviteIcon = new DiscoGroupMember();
                inviteIcon.setType(100);//+
                DiscoGroupMember deleteIcon = new DiscoGroupMember();
                deleteIcon.setType(101);//-

                if (listData.size() == 0) {
                    listData.add(inviteIcon);
                } else {
                    listData.add(inviteIcon);
                    listData.add(deleteIcon);
                }

                mData.addAll(listData);

                setAdapter();
            }
        }
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new LQRAdapterForRecyclerView<DiscoGroupMember>(mContext, mData, R.layout.item_member_info) {
                @Override
                public void convert(LQRViewHolderForRecyclerView helper, DiscoGroupMember item, int position) {
                    ImageView ivHeader = helper.getView(R.id.ivHeader);

                    if (position >= mData.size() - 2) {//+和-
                        if (position == mData.size() - 2) {//+
                            ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                        } else {//-
                            if (mData.size() == 1) {
                                ivHeader.setImageResource(R.mipmap.ic_add_team_member);
                            } else {
                                ivHeader.setImageResource(R.mipmap.ic_delete_team_member);
                            }
                        }
                        helper.setText(R.id.tvName, "");
                    } else {
                        Glide.with(mContext).load(item.getUserImg()).centerCrop().into(ivHeader);
                        helper.setText(R.id.tvName, YHStringUtils.pickName(item.getNickName(), item.getFriendRemark()));
                    }
                }
            };
            mAdapter.setOnItemClickListener((helper, parent, itemView, position) -> {
                if (position >= mData.size() - 2) {//+和-
                    if (position == mData.size() - 2) {//+
                        inviteOrDeleteMember(IM.group.GROUP_MANAGE_MANAGER_ADD);
                    } else {//-
                        if (mData.size() == 1) {
                            inviteOrDeleteMember(IM.group.GROUP_MANAGE_MANAGER_ADD);
                        } else {
                            inviteOrDeleteMember(IM.group.GROUP_MANAGE_MANAGER_DELETE);
                        }
                    }
                } else {
                    ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", mData.get(position).getMemberId())
                            .navigation();
                }
            });
            rvMember.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChangedWrapper();
        }
    }

    /**
     * 邀请/删除成员
     */
    private void inviteOrDeleteMember(String type) {
        ArrayList<String> selectedTeamMembers = new ArrayList<String>();
        ArrayList<FriendModel> members = new ArrayList<>();

        for (int i = 0; i < mGroupMembers.size(); i++) {
            DiscoGroupMember member = mGroupMembers.get(i);

            int groupRole = member.getType();
            switch (groupRole) {//群主
                case 1:
                    break;
                case 2:
                    break;
                case 3://管理员
                    selectedTeamMembers.add(member.getMemberId());

                    if (type.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)) {
                        FriendModel friendModel = new FriendModel();
                        friendModel.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
                        friendModel.setUserId(member.getMemberId());
                        friendModel.setUserPhoto(member.getUserImg());

                        members.add(friendModel);
                    }
                    break;
            }

            if (type.equals(IM.group.GROUP_MANAGE_MANAGER_ADD)) {
                FriendModel friendModel = new FriendModel();
                friendModel.setNickName(YHStringUtils.pickName(member.getNickName(), member.getFriendRemark()));
                friendModel.setUserId(member.getMemberId());
                friendModel.setUserPhoto(member.getUserImg());

                members.add(friendModel);
            }
        }

        ARouter.getInstance().build(ARouterPath.CREATE_GROUP_ACTIVITY)
                .withSerializable("selectedTeamMembers", selectedTeamMembers)
                .withString("groupID", groupId)
                .withSerializable("convertFriends", members)
                .withString("currentGroupName", currentGroupName)
                .withString("type", type).navigation();

        finish();
    }
}
