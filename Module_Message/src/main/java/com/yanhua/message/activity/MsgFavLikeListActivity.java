package com.yanhua.message.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.adapter.MsgFavLikeListAdapter;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MSG_FAVLIKE_LIST_ACTIVITY)
public class MsgFavLikeListActivity extends BaseMvpActivity<MsgPresenter> implements MsgContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    private MsgFavLikeListAdapter mAdapter;
    private int total;
    private List<MsgFabulousModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_msg_favlike_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("赞和收藏");

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(r -> getListData(1));

        refreshLayout.setOnLoadMoreListener(r -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MsgFavLikeListAdapter(mContext);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<MsgFabulousModel> list = mAdapter.getmDataList();
            MsgFabulousModel item = list.get(pos);

            int type = item.getType();
            switch (type) {

                case YXConfig.TYPE_MOMENT:
                    int contentType = item.getContentType();

                    if (contentType == 2) {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentModel", null)
                                .withSerializable("contentId", item.getContentId())
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withInt("module", 0)
                                .navigation();
                    } else {
                        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                .withString("id", item.getContentId())
                                .navigation();
                    }
                    break;

                case YXConfig.TYPE_INVITE:
                    ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                            .withString("id", item.getContentId()).navigation();
                    break;
                case YXConfig.TYPE_BREAK_NEWS:
                case YXConfig.TYPE_STRATEGY:
                    PageJumpUtil.jumpNewsDetailPage(item.getContentId(), item.getType());
                    break;
                case YXConfig.TYPE_BEST:
                    ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                            .withSerializable("contentId", item.getContentId())
                            .withInt("discoBest", 1)//电音集锦
                            .withString("categoryId", "")
                            .withString("city", "")
                            .withInt("module", 0)
                            .navigation();
                    break;
                case YXConfig.TYPE_9BAR:
                    //去酒吧
                    ARouter.getInstance()
                            .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                            .withString("barId", item.getContentId())
                            .navigation();
                    break;
                default:
                    break;
            }
        });

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        basePresenter.dealFabulousReadAll();
    }

    private void getListData(int page) {
        current = page;

        basePresenter.getFabulousList(current, size);
    }


    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }


    @Override
    public void handleMsyFabulousListPage(ListResult<MsgFabulousModel> listResult) {
        List<MsgFabulousModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}