package com.yanhua.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;

import java.util.ArrayList;

import butterknife.BindView;

public class GroupFriendItemAdapter extends BaseRecyclerAdapter<FriendModel, GroupFriendItemAdapter.ContractsHolder> {
    private Context mContext;

    public ArrayList<String> mSelectedTeamMemberAccounts;
    private String mtype;

    public GroupFriendItemAdapter(Context context, ArrayList<String> selectItems, String type) {
        super(context);
        mContext = context;
        mSelectedTeamMemberAccounts = selectItems;
        mtype = type;
    }

    @NonNull
    @Override
    protected ContractsHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ContractsHolder(inflater.inflate(R.layout.item_group_friend, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ContractsHolder holder, @NonNull FriendModel item) {
        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getUserPhoto(), R.drawable.place_holder);

        holder.tvName.setText(YHStringUtils.pickName(item.getNickName(), item.getFriendRemark()));
        int position = getPosition(holder);
        //如果添加群成员的话，需要判断是否已经在群中
        if (null != mSelectedTeamMemberAccounts && mSelectedTeamMemberAccounts.contains(item.getUserId()) &&
                !mtype.equals(IM.group.GROUP_DELETE_MEMBER) && !mtype.equals(IM.group.GROUP_MANAGE_CONVERT)
                && !mtype.equals(IM.group.GROUP_MANAGE_MANAGER_DELETE)
        ) {
            holder.cbItem.setChecked(true);
            holder.cbItem.setEnabled(false);

            holder.cbItem.setTextColor(mContext.getResources().getColor(R.color.subWordB4B8BC));
            holder.cbItem.setBackgroundResource(R.drawable.bg_tran_circle_no_stroke);

            holder.ll_item.setEnabled(false);
        } else {
            boolean select = item.isSelected();
            holder.cbItem.setChecked(select);
            holder.cbItem.setEnabled(false);

            if (select) {
                holder.cbItem.setTextColor(mContext.getResources().getColor(R.color.theme));
                holder.cbItem.setBackgroundResource(R.drawable.bg_tran_circle_no_stroke);
            } else {
                holder.cbItem.setTextColor(mContext.getResources().getColor(R.color.white));
                holder.cbItem.setBackgroundResource(R.drawable.shape_bg_stroke2_r_gray);
            }
            holder.ll_item.setEnabled(true);
            //没有在已有群中的联系人，根据当前的选中结果判断
        }

        String str = "";
        //得到当前字母
        String currentLetter = item.getFirstLetter();
        if (position == 0) {
            str = currentLetter;
        } else {
            //得到上一个字母
            String preLetter = mDataList.get(position - 1).getFirstLetter();
            //如果和上一个字母的首字母不同则显示字母栏
            if (!preLetter.equalsIgnoreCase(currentLetter)) {
                str = currentLetter;
            }

            int nextIndex = position + 1;
            if (nextIndex < mDataList.size() - 1) {
                //得到下一个字母
                String nextLetter = mDataList.get(nextIndex).getFirstLetter();
                //如果和下一个字母的首字母不同则隐藏下划线
                if (!nextLetter.equalsIgnoreCase(currentLetter)) {
                    holder.vLine.setVisibility(View.INVISIBLE);
                } else {
                    holder.vLine.setVisibility(View.VISIBLE);
                }
            } else {
                holder.vLine.setVisibility(View.INVISIBLE);
            }
        }
        if (position == mDataList.size() - 1) {
            holder.vLine.setVisibility(View.GONE);
        }

        //根据str是否为空决定字母栏是否显示
        if (TextUtils.isEmpty(str)) {
            holder.tvIndex.setVisibility(View.GONE);
            holder.vLine.setVisibility(View.VISIBLE);
        } else {
            holder.tvIndex.setVisibility(View.VISIBLE);
            holder.vLine.setVisibility(View.GONE);
            holder.tvIndex.setText(str.toUpperCase());//转大写
        }
    }

    static class ContractsHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.cb_item)
        ITCheckBox cbItem;
        @BindView(R2.id.ll_item)
        LinearLayout ll_item;
        @BindView(R2.id.tvIndex)
        TextView tvIndex;
        @BindView(R2.id.vLine)
        View vLine;

        public ContractsHolder(View itemView) {
            super(itemView);
        }
    }
}
