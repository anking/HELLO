package com.yanhua.message.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.shuyu.textutillib.RichTextBuilder;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 评论At列表
 */
public class MsgCommentListAdapter extends BaseRecyclerAdapter<MsgCommentAtModel, MsgCommentListAdapter.ViewHolder> {

    private Context mContext;

    public MsgCommentListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_comment, parent, false));

        //具体的@点击效果参考此刻列表项中的
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MsgCommentAtModel item) {
        int position = getPosition(holder);

        String userPhoto = item.getUserPhoto();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(holder.civAvatar, userPhoto, R.drawable.place_holder);
        } else {
            holder.civAvatar.setImageResource(R.drawable.place_holder);
        }

        if (item.getGender() == -1) {
            holder.ivSex.setVisibility(View.GONE);
        } else {
            holder.ivSex.setVisibility(View.VISIBLE);
            holder.ivSex.setImageResource(item.getGender() == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
        }


        int commentType = -1;//0为AT -1评论 其他为Type
        int replyStatus = item.getReplyStatus();//回复状态;(回复状态,0-评论内容，1-回复评论 2 At)
        String replyContent = YHStringUtils.value(item.getCommentDesc());
        if (replyStatus == 0) {
            commentType = item.getType();
            //獲取評論的内容
            holder.tvReply.setText(replyContent);
        } else if (replyStatus == 1) {
            commentType = -1;//评论
            replyContent = "回复" + YXConfig.SPACE + "@" + YHStringUtils.value(item.getCommentUserNickName()) + YXConfig.SPACE + ":" + replyContent;

            List<TopicModel> topicList = new ArrayList<>();
            //文本内容展示
            List<FriendModel> parentList = new ArrayList<>();//At的用户
            FriendModel parentModel = new FriendModel();
            parentModel.setUserId(UserManager.getInstance().getUserId());
            parentModel.setNickName(item.getCommentUserNickName());
            parentList.add(parentModel);
            //直接使用RichTextView
            SpanAtUserCallBack spanAtUserCallBack = (view, userModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userModel.getUserId()).navigation();
            };

            RichTextBuilder richTextBuilder = new RichTextBuilder(mContext);
            richTextBuilder.setContent(replyContent)
                    .setListUser(parentList)
                    .setListTopic(topicList)
                    .setTextView(holder.tvReply)
                    .setSpanAtUserCallBack(spanAtUserCallBack)
                    .build();
        } else if (replyStatus == 2) {
            commentType = 0;//At
        }

        int followStatus = item.getRelationState();
        String relationShip = "";
        switch (followStatus) {
            case 1:
                relationShip = "";
                break;
            case 2:
                relationShip = "(你关注的人)";
                break;
            case 3:
                relationShip = "(粉丝)";
                break;
            case 4:
                relationShip = "(朋友)";
                break;
        }
        holder.tvName.setText(YHStringUtils.value(item.getNickName()) + relationShip);

        int contentType = item.getContentType();
        String coverImage = item.getContentCoverUrl();
        if (!TextUtils.isEmpty(coverImage)) {
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 0);
            holder.ivCover.setVisibility(View.VISIBLE);
            holder.ivPlay.setVisibility(contentType == 2 ? View.VISIBLE : View.GONE);
        } else {
            holder.ivCover.setVisibility(View.GONE);
            holder.ivPlay.setVisibility(View.GONE);
        }

        //评论了你的XX
        //在发布内容中@了你

//        回复：回复了你的评论；
//        评论：评论了你的（此刻、爆料、攻略）
//        /评论了你的留言
//        评论了你的约伴
//        @：@了你；----内容中的@


        String desc = "";
        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間

        switch (commentType) {
            //类型(1:此刻,2:约伴,3:爆料,4:攻略,5:集锦,6:新闻,7:酒吧新闻)
            //    类型  1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻   点赞 收藏  评论   暂定这七种类型哈  如果有新增或者觉得不对的  根据自己负责的业务自己增删
            case 0:
                desc = "在发布作品时@了你 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_MOMENT:
                desc = "评论了你的此刻 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_INVITE:
                desc = "评论了你的约伴 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_BREAK_NEWS:
                desc = "评论了你的爆料 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_STRATEGY:
                desc = "评论了你的攻略 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_BEST:
                desc = "评论了你的电音集锦 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            case YXConfig.TYPE_9BAR:
                desc = "评论了你的留言 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
            default:
                desc = "回复了你的评论 " + YXTimeUtils.getFriendlyTimeAtContent(createdTime, true);
                break;
        }
        holder.tvDate.setText(desc);
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civAvatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvReply)
        RichTextView tvReply;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        @BindView(R2.id.iv_cover)
        ImageView ivCover;
        @BindView(R2.id.iv_play)
        ImageView ivPlay;
        @BindView(R2.id.ivSex)
        ImageView ivSex;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
