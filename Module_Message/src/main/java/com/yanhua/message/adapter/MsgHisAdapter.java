package com.yanhua.message.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.HistoryMsg;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

public class MsgHisAdapter extends BaseRecyclerAdapter<HistoryMsg, MsgHisAdapter.ContractsHolder> {
    private Context mContext;


    public MsgHisAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @NonNull
    @Override
    protected ContractsHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ContractsHolder(inflater.inflate(R.layout.item_msg_hislist, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ContractsHolder holder, @NonNull HistoryMsg message) {
        HistoryMsg item = message;
        holder.tvName.setText(YHStringUtils.pickName(item.getNickName(), item.getFriendRemark()));
        holder.tv_desc.setText(item.getMsgContent());
        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getHeadUrl(), R.drawable.place_holder);

        long sendTime = message.getSentTime();
        String showSendTime = YXTimeUtils.getFriendlyTimeAtContent(sendTime);
        holder.tvTime.setText(showSendTime);
    }

    static class ContractsHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;

        @BindView(R2.id.tv_time)
        TextView tvTime;

        @BindView(R2.id.tv_desc)
        TextView tv_desc;

        public ContractsHolder(View itemView) {
            super(itemView);
        }
    }
}
