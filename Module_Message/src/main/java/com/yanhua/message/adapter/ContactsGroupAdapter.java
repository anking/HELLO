package com.yanhua.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.GroupMemberInfo;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 通讯录列表
 *
 * @author Administrator
 */
public class ContactsGroupAdapter extends BaseRecyclerAdapter<GroupsBean, ContactsGroupAdapter.ContractsHolder> {

    private Context mContext;
    private boolean multiSelect;

    public ContactsGroupAdapter(Context context, boolean multi) {
        super(context);
        mContext = context;
        multiSelect = multi;
    }

    @NonNull
    @Override
    protected ContractsHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ContractsHolder(inflater.inflate(R.layout.item_group_contact_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ContractsHolder holder, @NonNull GroupsBean item) {
        if (TextUtils.isEmpty(item.getGroupImg())) {
            ImageLoaderUtil.loadImg(holder.civAvatar, R.drawable.place_holder);
        } else {
            ImageLoaderUtil.loadImg(holder.civAvatar, item.getGroupImg());
        }


        holder.tvName.setText(item.getName());

        ArrayList<GroupMemberInfo> listMember = item.getGroupMemberVoList();

        if (null != listMember && listMember.size() > 0) {
            holder.tvNameDesc.setVisibility(View.VISIBLE);
            holder.tvNameDesc.setText("包含：" + listMember.get(0).getNickName());
        } else {
            holder.tvNameDesc.setVisibility(View.GONE);
        }

        int position = getPosition(holder) - getHeaderCount();
        String str = "";
        //得到当前字母
        String currentLetter = item.getFirstLetter();
        if (position == 0) {
            str = currentLetter;
        } else {
            //得到上一个字母
            String preLetter = mDataList.get(position - 1).getFirstLetter();
            //如果和上一个字母的首字母不同则显示字母栏
            if (!preLetter.equalsIgnoreCase(currentLetter)) {
                str = currentLetter;
            }
        }

        //根据str是否为空决定字母栏是否显示
        if (TextUtils.isEmpty(str)) {
            holder.tvIndex.setVisibility(View.GONE);
        } else {
            holder.tvIndex.setVisibility(View.VISIBLE);
            holder.tvIndex.setText(str.toUpperCase());//转大写
        }

        if (multiSelect) {
            holder.cbItem.setVisibility(View.VISIBLE);
            boolean select = item.isSelected();
            if (select) {
                holder.cbItem.setTextColor(mContext.getResources().getColor(R.color.theme));
                holder.cbItem.setBackgroundResource(R.drawable.bg_tran_circle_no_stroke);
            } else {
                holder.cbItem.setTextColor(mContext.getResources().getColor(R.color.white));
                holder.cbItem.setBackgroundResource(R.drawable.shape_bg_stroke2_r_gray);
            }
        }else {
            holder.cbItem.setVisibility(View.GONE);
        }
    }

    static class ContractsHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;

        @BindView(R2.id.tv_name_desc)
        TextView tvNameDesc;

        @BindView(R2.id.tvIndex)
        TextView tvIndex;

        @BindView(R2.id.cb_item)
        ITCheckBox cbItem;

        public ContractsHolder(View itemView) {
            super(itemView);
        }
    }
}
