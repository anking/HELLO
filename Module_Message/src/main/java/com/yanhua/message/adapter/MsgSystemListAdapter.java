package com.yanhua.message.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.utils.GlideRoundTransform;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

public class MsgSystemListAdapter extends BaseRecyclerAdapter<MsgSysModel, MsgSystemListAdapter.ViewHolder> {

    private Context mContext;

    public MsgSystemListAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_system, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MsgSysModel item) {
        int position = getPosition(holder);

        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間
        holder.tvDate.setVisibility(TextUtils.isEmpty(createdTime) ? View.GONE : View.VISIBLE);
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

        String title = item.getTitle();
        String content = item.getContent();
        String desc = "";
        String detailId = item.getDetailId();
        if (!TextUtils.isEmpty(detailId)) {
            if (content.length() > 30) {
                content = content.substring(0, 30);

                desc = "<font color=\"#36384A\" >" + content + "</font>" + "<font color=\"#4367B4\" >" + "，点击查看详情>>" + "</font>";
            } else {
                desc = "<font color=\"#36384A\" >" + content + "</font>";
            }
        }else {
            desc = content;
        }

        holder.tvDesc.setVisibility(TextUtils.isEmpty(content) ? View.GONE : View.VISIBLE);
        //#4367B4
        holder.tvTitle.setText(title);
        holder.tvDesc.setText(Html.fromHtml(desc));

        String coverImage = item.getCoverUrl();
        if (!TextUtils.isEmpty(coverImage)) {
            //此处封面的展示逻辑需要根据返回过来的封面的尺寸大小进行设置具体的大小
            Glide.with(mContext)
                    .asBitmap()
                    .load(coverImage)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picWidth = resource.getWidth();
                            int picHeight = resource.getHeight();

                            int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
                            int height = width * picHeight / picWidth;

                            ViewGroup.LayoutParams layoutParams = holder.ivCover.getLayoutParams();
                            layoutParams.width = width;
                            layoutParams.height = height;
                            holder.ivCover.setLayoutParams(layoutParams);
                            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 12, GlideRoundTransform.CornerType.TOP);
                        }
                    });
            holder.ivCover.setVisibility(View.VISIBLE);
        } else {
            holder.ivCover.setVisibility(View.GONE);
        }
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvTitle)
        TextView tvTitle;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
