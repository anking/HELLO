package com.yanhua.message.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

/**
 * 赞和收藏汇总列表
 */
public class FavLikeUserAdapter extends BaseRecyclerAdapter<MsgUserModel, FavLikeUserAdapter.NewFollowHolder> {
    private Context mContext;

    public FavLikeUserAdapter(Context context) {
        super(context);
        mContext = context;
    }

    public interface OnSelectClickListener {
        void onClick(int pos);
    }

    private OnSelectClickListener listener;

    public void setOnSelectClickListener(OnSelectClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected NewFollowHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new NewFollowHolder(inflater.inflate(R.layout.item_favlike_user, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull NewFollowHolder holder, @NonNull MsgUserModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getUserPhoto(), R.drawable.place_holder);

        holder.tvName.setText(YHStringUtils.value(item.getNickName()));

        if (item.getGender() == -1) {
            holder.ivSex.setVisibility(View.GONE);
        } else {
            holder.ivSex.setVisibility(View.VISIBLE);
            holder.ivSex.setImageResource(item.getGender() == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
        }

//       关注状态 1 未关注 2 已关注 3 被关注 4互相关注
        int followStatus = item.getRelationState();

        switch (followStatus) {
            case 1:
                holder.btnFollow.setStatus(false, "关注");
                break;
            case 2:
                holder.btnFollow.setStatus(true, "已关注");
                break;
            case 3:
                holder.btnFollow.setStatus(false, "回粉");
                break;
            case 4:
                holder.btnFollow.setStatus(true, "互相关注");
                break;
        }

        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

        holder.btnFollow.setOnClickListener(v -> {
            if (null != listener) {
                listener.onClick(position);
            }
        });

        holder.civAvatar.setOnClickListener(v -> {
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", item.getUserId())
                    .navigation();
        });
    }

    static class NewFollowHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.btn_follow)
        StatusView btnFollow;
        @BindView(R2.id.ivSex)
        ImageView ivSex;

        @BindView(R2.id.tv_date)
        TextView tvDate;

        public NewFollowHolder(View itemView) {
            super(itemView);
        }
    }
}
