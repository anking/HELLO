package com.yanhua.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

/**
 * 酒吧互动消息列表
 */
public class MsgBarInteractListAdapter extends BaseRecyclerAdapter<BarInterActMsgModel, MsgBarInteractListAdapter.ViewHolder> {

    private Context mContext;

    public MsgBarInteractListAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_barinteract, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BarInterActMsgModel item) {
        String userName = item.getUserName();
        String userImg = item.getUserImg();
        ImageLoaderUtil.loadImgCenterCrop(holder.tvPublisherPic, userImg, R.drawable.place_holder);
        holder.tvPublisherName.setText(userName);//将后台返回数据进行加工处理

        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間
        holder.tvDate.setVisibility(TextUtils.isEmpty(createdTime) ? View.GONE : View.VISIBLE);
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

        int contentDel = item.getContentDel();
        String parentAskedDesc = item.getParentAskedDesc();
        String parentAskedId = item.getParentAskedId();
        String askedId = item.getAskedId();

        int type = item.getType();//类型（0回答，1评论）

        String askedDesc = item.getAskedDesc();
        //#4367B4
        holder.tvAnswer.setText(contentDel == 1 ? "该内容已删除" : askedDesc);//如果被删除的话显示 “该内容已删除”

        if (type == 1) {
            holder.tvQuestionType.setText("答");// 问：shape_bg_ask 答：shape_bg_answer
            holder.tvQuestionType.setBackgroundResource(R.drawable.shape_bg_answer);
        } else {
            holder.tvQuestionType.setText("问");// 问：shape_bg_ask 答：shape_bg_answer
            holder.tvQuestionType.setBackgroundResource(R.drawable.shape_bg_ask);
        }

        holder.itemContainer.setOnClickListener(view -> ARouter.getInstance()
                .build(type == 0 ? ARouterPath.BAR_ANSWER_DETAIL_ACTIVITY : ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                .withString("askId", askedId)
                .navigation());

        holder.tvASK.setText(parentAskedDesc);//
        holder.tvBarName.setText(item.getBarInfoName());//
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.itemContainer)
        LinearLayout itemContainer;

        @BindView(R2.id.tvPublisherPic)
        CircleImageView tvPublisherPic;
        @BindView(R2.id.tvPublisherName)
        TextView tvPublisherName;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        @BindView(R2.id.tvAnswer)
        TextView tvAnswer;

        @BindView(R2.id.tvQuestionType)
        TextView tvQuestionType;
        @BindView(R2.id.tvASK)
        TextView tvASK;
        @BindView(R2.id.tvBarName)
        TextView tvBarName;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
