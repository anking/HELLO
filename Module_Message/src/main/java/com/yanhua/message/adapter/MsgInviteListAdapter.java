package com.yanhua.message.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

/**
 * 邀约列表
 */
public class MsgInviteListAdapter extends BaseRecyclerAdapter<InviteMsgModel, MsgInviteListAdapter.ViewHolder> {

    private Context mContext;

    public MsgInviteListAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_invite, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InviteMsgModel item) {
        int position = getPosition(holder);

        //类型(1:报名, 2:取消报名, 3:邀约确认, 4:邀约移除, 5:邀约取消)
        int type = item.getType();
        String msgType = "通知";
        switch (type) {
            case 1:
                msgType = "报名" + msgType;
                break;
            case 2:
                msgType = "取消报名" + msgType;
                break;
            case 3:
                msgType = "邀约确认" + msgType;
                break;
            case 4:
                msgType = "邀约移除" + msgType;
                break;
            case 5:
                msgType = "邀约取消" + msgType;
                break;
        }
        holder.tvStatus.setText(msgType);
        int state = item.getState();
        holder.viewUnread.setVisibility(state == 1 ? View.GONE : View.VISIBLE);

        String createdTime = item.getCreatedTime();//
        holder.tvDate.setVisibility(TextUtils.isEmpty(createdTime) ? View.GONE : View.VISIBLE);
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

        String coverURL = item.getInviteCoverUrl();
        ImageLoaderUtil.loadImgCenterCrop(holder.ivCover, coverURL, R.drawable.place_holder);

        String desc = item.getContent();
        String title = item.getInviteTitle();

    /*      String userName = "齐天大圣孙悟空";
        String actionName = "取消报名了你发布的";
        String rejectReason = "不想参加了";

        String reason = TextUtils.isEmpty(rejectReason) ? "" : "，取消原因：" + rejectReason;
        //根据类型不通获取不同的描述,,下面简化，具体需要根据原型、业务进行
        String desc = "<font color=\"#4367B4\" >" + "“" + userName + "”" + "</font>"
                + "<font color=\"#36384A\" >" + actionName + "</font>"
                + "<font color=\"#4367B4\" >" + "“" + title + "”" + "</font>"
                + "<font color=\"#36384A\" >" + "邀约活动" + reason + "</font>";*/

        holder.tvTitle.setText(title);
        holder.tvDesc.setText(Html.fromHtml(desc));
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvStatus)
        TextView tvStatus;
        @BindView(R2.id.viewUnread)
        View viewUnread;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;
        @BindView(R2.id.tvTitle)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
