package com.yanhua.message.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import butterknife.BindView;

public class MsgHeadPicAdapter extends BaseRecyclerAdapter<String, MsgHeadPicAdapter.ViewHolder> {
    public MsgHeadPicAdapter(Context context) {
        super(context);
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_head_pic, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        int position = getPosition(holder);

        String coverUrl = item;

        ImageLoaderUtil.loadImgCenterCrop(holder.civAvatar, coverUrl, R.drawable.place_holder);

    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.civAvatar)
        CircleImageView civAvatar;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
