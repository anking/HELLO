package com.yanhua.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.message.R;
import com.yanhua.message.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 收藏和点赞列表
 */
public class MsgFavLikeListAdapter extends BaseRecyclerAdapter<MsgFabulousModel, MsgFavLikeListAdapter.ViewHolder> {

    private Context mContext;

    public MsgFavLikeListAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_msg_favlike, parent, false));
        //具体的@点击效果参考此刻列表项中的
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MsgFabulousModel item) {
        int position = getPosition(holder);

        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理


        int size = 0;
        //處理用戶個數
        List<MsgUserModel> userList = item.getUserList();
        if (null != userList && userList.size() > 0) {
            size = userList.size();

            MsgUserModel user = userList.get(0);//第一个
            String userPhoto = user.getUserPhoto();
            if (!TextUtils.isEmpty(userPhoto)) {
                ImageLoaderUtil.loadImgCenterCrop(holder.civAvatar, userPhoto, R.drawable.place_holder);
            } else {
                holder.civAvatar.setImageResource(R.drawable.place_holder);
            }

            int gender = user.getGender();
            if (gender == -1) {
                holder.ivSex.setVisibility(View.GONE);
            } else {
                holder.ivSex.setVisibility(View.VISIBLE);
                holder.ivSex.setImageResource(gender == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
            }
            holder.tvName.setText(YHStringUtils.value(user.getNickName()));
        } else {
            size = 0;
            holder.civAvatar.setImageResource(R.drawable.place_holder);
            holder.ivSex.setVisibility(View.GONE);
            holder.tvName.setText(YHStringUtils.value(""));
        }

        //封面缩略图处理
        int contentType = item.getContentType();
        String coverImage = item.getContentCoverUrl();
        if (!TextUtils.isEmpty(coverImage)) {
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 0);
            holder.ivPlay.setVisibility(contentType == 2 ? View.VISIBLE : View.GONE);
            holder.ivCover.setVisibility(View.VISIBLE);
        } else {
            holder.ivPlay.setVisibility(View.GONE);
            holder.ivCover.setVisibility(View.GONE);
        }

//        dataType//数据类型(1:赞,2;收藏)
//        fabulousType	//点赞类型(1:内容,2:评论)
        int dataType = item.getDataType();
        int fabulousType = item.getFabulousType();
        int type = item.getType();

        String typeDesc = "";
        switch (type) {
            case YXConfig.TYPE_MOMENT:
                typeDesc = "此刻";
                break;
            case YXConfig.TYPE_INVITE:
                typeDesc = "约伴";
                break;
            case YXConfig.TYPE_BREAK_NEWS:
                typeDesc = "爆料";
                break;
            case YXConfig.TYPE_STRATEGY:
                typeDesc = "攻略";
                break;
            case YXConfig.TYPE_BEST:
                typeDesc = "电音集锦";
                break;
            case YXConfig.TYPE_9BAR:
                typeDesc = "留言";
                break;
            default:
                typeDesc = "内容";
                break;
        }

        String desc = (dataType == 1 ? "赞" : "收藏") + "了你的" + (fabulousType == 1 ? typeDesc : "评论");
        holder.tvType.setText(desc);

        //操作5个才可以点击
        if (size > 5) {
            holder.iconNext.setVisibility(View.VISIBLE);
            holder.llReplyContent.setVisibility(View.VISIBLE);
            holder.llReplyContent.setOnClickListener(view -> {
                jumpToDetail(item);
            });
        } else if (size <= 5 && size > 1) {
            holder.iconNext.setVisibility(View.GONE);
            holder.llReplyContent.setVisibility(View.VISIBLE);
            holder.llReplyContent.setOnClickListener(null);
        } else {
            holder.llReplyContent.setVisibility(View.GONE);
            holder.llReplyContent.setOnClickListener(null);
        }

        if (size > 0) {
            LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
            MsgHeadPicAdapter picAdapter = new MsgHeadPicAdapter(mContext);
            holder.rvHeadPics.setLayoutManager(manager);
            holder.rvHeadPics.setAdapter(picAdapter);

            ArrayList<String> headPics = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                headPics.add(userList.get(i).getUserPhoto());
            }

            picAdapter.setItems(headPics);
        }
    }

    private void jumpToDetail(MsgFabulousModel item) {
        ARouter.getInstance().build(ARouterPath.MSG_FAVLIKE_DETAIL_ACTIVITY)
                .withSerializable("praiseDetail", item)
                .navigation();
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civAvatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvType)
        TextView tvType;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        @BindView(R2.id.ivSex)
        ImageView ivSex;
        @BindView(R2.id.iv_cover)
        ImageView ivCover;
        @BindView(R2.id.iv_play)
        ImageView ivPlay;

        @BindView(R2.id.llReplyContent)
        LinearLayout llReplyContent;
        @BindView(R2.id.rvHeadPics)
        RecyclerView rvHeadPics;
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
