package com.yanhua.message.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.message.R;

public class NoticeAlertPopup extends CenterPopupView {
    private String mTitle;
    private String mContent;
    private String mText;
    private boolean isCenter;

    public interface OnButtonClickListener {
        void onClick();
    }

    private OnButtonClickListener mConfirmListener;
    private OnButtonClickListener mCancelListener;


    public NoticeAlertPopup(@NonNull Context context) {
        super(context);
        mContent = "";
    }

    public NoticeAlertPopup(@NonNull Context context, String content) {
        super(context);
        mContent = content;
    }

    public NoticeAlertPopup(@NonNull Context context, String title, String content) {
        super(context);
        mTitle = title;
        mContent = content;
    }

    public NoticeAlertPopup(@NonNull Context context, String title, String content, boolean center) {
        super(context);
        mTitle = title;
        mContent = content;
        isCenter = center;
    }

    public NoticeAlertPopup(@NonNull Context context, String title, String content, String btnText) {
        super(context);
        mTitle = title;
        mContent = content;
        mText = btnText;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_group_notice_alert;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Button btn_commit = findViewById(R.id.btn_commit);
        TextView btn_close = findViewById(R.id.btn_close);

        TextView tv_desc = findViewById(R.id.tv_desc);

        TextView tv_title = findViewById(R.id.tv_title);

        if (TextUtils.isEmpty(mTitle)) {
            tv_title.setVisibility(GONE);
        } else {
            tv_title.setText(mTitle);
            tv_title.setVisibility(VISIBLE);
        }

        tv_desc.setText(mContent);
        btn_close.setText(isCenter ? "确定" : "取消");
        btn_close.setTextColor(Color.parseColor( "#ffffff" ));
        btn_close.setBackgroundResource(isCenter ? R.drawable.shape_gradient_theme : R.drawable.shape_bg_r2_deep_disable);
        btn_close.setOnClickListener(view -> {
            dismiss();
            if (mCancelListener != null) {
                mCancelListener.onClick();
            }
        });

        btn_commit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (mConfirmListener != null) {
                    mConfirmListener.onClick();
                }
            }
        });

        if (!TextUtils.isEmpty(mText)) {
            btn_commit.setText(mText);
        } else {
            btn_commit.setVisibility(GONE);
        }
    }

    public void setOnConfirmListener(OnButtonClickListener listener) {
        this.mConfirmListener = listener;
    }

    public void setOnCancelListener(OnButtonClickListener listener) {
        this.mCancelListener = listener;
    }
}
