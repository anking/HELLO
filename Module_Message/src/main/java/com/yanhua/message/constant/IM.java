package com.yanhua.message.constant;

public final class IM {
    public final class group {
        public final static String MODIFY_GROUP_NAME = "1";//群昵称
        public final static String MODIFY_PERSONAL_NAME = "2";//群中自己的昵称

        public final static String GROUP_OWNER = "1";//群zh
        public final static String GROUP_OTHER = "2";
        public final static String GROUP_CREATE = "groupCreate"; // 创建群
        public final static String GROUP_MANAGE_CONVERT = "groupManageConvert";//群zh转让
        public final static String GROUP_MANAGE_MANAGER_ADD = "groupManageManagerAdd";//群zh管理员
        public final static String GROUP_MANAGE_MANAGER_DELETE = "groupManageManagerDelete";//群zh管理员
        public final static String GROUP_INVITE_MEMBER = "invite";
        public final static String GROUP_DELETE_MEMBER = "delete";
        public final static int MAX_HEADER_PHOTO = 8;//加上自己的头像，最多9个
    }
}
