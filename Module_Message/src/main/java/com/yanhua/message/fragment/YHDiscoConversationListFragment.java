package com.yanhua.message.fragment;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.CollectionUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.MsgLastModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.MessageTopMenu;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.message.R;
import com.yanhua.message.R2;
import com.yanhua.message.constant.IM;
import com.yanhua.message.presenter.MsgPresenter;
import com.yanhua.message.presenter.contract.MsgContract;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.common.RLog;
import io.rong.imkit.IMCenter;
import io.rong.imkit.config.ConversationListBehaviorListener;
import io.rong.imkit.config.RongConfigCenter;
import io.rong.imkit.conversationlist.ConversationListAdapter;
import io.rong.imkit.conversationlist.model.BaseUiConversation;
import io.rong.imkit.conversationlist.model.GatheredConversation;
import io.rong.imkit.conversationlist.viewmodel.ConversationListViewModel;
import io.rong.imkit.event.Event;
import io.rong.imkit.model.NoticeContent;
import io.rong.imkit.userinfo.RongUserInfoManager;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imkit.widget.adapter.BaseAdapter;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imkit.widget.dialog.OptionsPopupDialog;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Group;


public class YHDiscoConversationListFragment extends BaseMvpFragment<MsgPresenter> implements BaseAdapter.OnItemClickListener, MsgContract.IView {
    /*
     * 连接通知状态延迟显示时间。
     * 为了防止连接闪断，不会在断开连接时立即显示连接通知状态，而是在延迟一定时间后显示。
     */
    protected final long NOTICE_SHOW_DELAY_MILLIS = 4000L;
    private final String TAG = YHDiscoConversationListFragment.class.getSimpleName();
    protected ConversationListAdapter mAdapter;
    protected RecyclerView mList;
    protected View mNoticeContainerView;
    protected TextView mNoticeContentTv;
    protected ImageView mNoticeIconIv;
    protected ConversationListViewModel mConversationListViewModel;
    protected SmartRefreshLayout mRefreshLayout;
    protected Handler mHandler = new Handler(Looper.getMainLooper());

    {
        mAdapter = onResolveAdapter();
    }


    @BindView(R2.id.scrollView)
    NestedScrollView scrollView;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.llFavLike)
    MessageTopMenu llFavLike;
    @BindView(R2.id.llFans)
    MessageTopMenu llFans;
    @BindView(R2.id.llComment)
    MessageTopMenu llComment;
    @BindView(R2.id.llInvite)
    MessageTopMenu llInvite;

    @BindView(R2.id.rl_sys_msg)
    RelativeLayout rlSysMsg;
    @BindView(R2.id.tv_sys_msg_time)
    TextView tvSysMsgTime;
    @BindView(R2.id.tv_sys_msg_title)
    TextView tvSysMsgTitle;
    @BindView(R2.id.tv_unread_sys_msg)
    OswaldTextView tvSysMsgUnreadNum;


    @BindView(R2.id.rl_interact)
    RelativeLayout rlInteract;
    @BindView(R2.id.tv_unread_interact_msg)
    OswaldTextView tvInterActUnreadNum;
    @BindView(R2.id.tvNewsInterAct)
    TextView tvNewsInterAct;
    @BindView(R2.id.tv_interact_msg_time)
    TextView tvInterActtime;

    @BindView(R2.id.msgTitle)
    TextView msgTitle;
    @BindView(R2.id.tvContact)
    AliIconFontTextView tvContact;
    @BindView(R2.id.tvCreateGroup)
    AliIconFontTextView tvCreateGroup;

    private Runnable mTimeCounterRunnable = new Runnable() {
        @Override
        public void run() {//在此添加需轮寻的接口
            refreshData();//执行的任务
            mHandler.postDelayed(this, 20 * 1000);
        }
    };

    @Override
    protected void creatPresent() {
        basePresenter = new MsgPresenter();
    }


    @Override
    public int bindLayout() {
        return R.layout.rc_conversationlist_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!IMCenter.getInstance().isInitialized()) {
            RLog.e(TAG, "Please init SDK first!");
            return;
        }
        mList = view.findViewById(R.id.rc_conversation_list);
        mRefreshLayout = view.findViewById(R.id.rc_refresh);

        mAdapter.setItemClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mList.setLayoutManager(layoutManager);
        mList.setAdapter(mAdapter);

        mNoticeContainerView = view.findViewById(R.id.rc_conversationlist_notice_container);
        mNoticeContentTv = view.findViewById(R.id.rc_conversationlist_notice_tv);
        mNoticeIconIv = view.findViewById(R.id.rc_conversationlist_notice_icon_iv);

        //-------------------------------自定义UI----------------------------
        applyDebouncingClickListener(true, llFavLike, llFans, llComment, llInvite,
                tvContact, tvCreateGroup, rlSysMsg, rlInteract);//顶部menu操作点击

        msgTitle.setText("消息");

        UltimateBarX.statusBarOnly(this)
                .transparent();
        UltimateBarX.addStatusBarTopPadding(toolbar);

        int halfBarHeight = UltimateBarX.getStatusBarHeight() / 2;
        //先设置为透明的
        toolbar.setBackgroundColor(Color.argb(0, 255, 255, 255));

        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int toolbarHeight = toolbar.getHeight();
            int alpha = (scrollY + halfBarHeight) * 255 / toolbarHeight;

            alpha = alpha > 255 ? 255 : alpha;

            if (scrollY >= 0 && scrollY < halfBarHeight) {
                toolbar.setBackgroundColor(Color.argb(0, 255, 255, 255));
            } else if (scrollY >= halfBarHeight && scrollY <= toolbarHeight) {
                toolbar.setBackgroundColor(Color.argb(alpha, 255, 255, 255));
            }
        });

        mRefreshLayout.setEnableRefresh(true);
        mRefreshLayout.setEnableLoadMore(false);

        /**
         * 初始化刷新模块
         */
        mRefreshLayout.setNestedScrollingEnabled(false);
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                onConversationListRefresh(refreshLayout);

                //判断是否链接根据MainActivity中的链接状态，进行判断，断开进行重连，否则请求其他的数据
                refreshData();
            }
        });
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                onConversationListLoadMore();
            }
        });

        subscribeUi();

    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mTimeCounterRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mConversationListViewModel != null) {
            mConversationListViewModel.clearAllNotification();
        }

        mHandler.postDelayed(mTimeCounterRunnable, 100);
    }

    private void refreshData() {
        if (UserManager.getInstance().isLogin()) {
            //获取最近的系统消息
            basePresenter.getSysMsgLastItem();
            //酒吧互动
            basePresenter.getInterActSysMsgLastItem();
            basePresenter.getBarInteractMsgList(1, 10, mMMKV.getUserId());

            //邀约
            basePresenter.dealInviteReadNum();
            //新增点赞和收藏
            basePresenter.dealFabulousReadNum();

            //新增粉丝
            basePresenter.dealNewFansUnReadNum();

            //新增评论
            basePresenter.dealCommentAtUnReadNum();

            basePresenter.getUserGroups(mMMKV.getUserId(), null, false);
        }
    }


    @Override
    public void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean search) {
        mRefreshLayout.finishRefresh();
        if (CollectionUtils.isNotEmpty(list)) {
            //过滤只有选中的
            for (GroupsBean item : list) {
                Uri uri = Uri.parse(item.getGroupImg());
                Group group = new Group(item.getId(), item.getName(), uri);
                RongUserInfoManager.getInstance().refreshGroupInfoCache(group);
            }
        }
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
        if (id == R.id.llFavLike) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_FAVLIKE_LIST_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.llFans) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_NEW_FOLLOW_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.llComment) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_COMMENT_AT_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.llInvite) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_INVITE_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.rl_sys_msg) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_SYSTEM_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.rl_interact) {
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.MSG_INTERACT_ACTIVITY)
                    .navigation(getContext()));
        } else if (id == R.id.tvContact) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.FRIENDS_LIST_ACTIVITY)
                        .withString("userId", UserManager.getInstance().getUserId())
                        .withBoolean("isSelect", false)
                        .navigation(mActivity, -1);
            });
        } else if (id == R.id.tvCreateGroup) {
            //创建群组
            PageJumpUtil.firstIsLoginThenJump(() -> ARouter.getInstance()
                    .build(ARouterPath.CREATE_GROUP_ACTIVITY)
                    .withString("type", IM.group.GROUP_CREATE)
                    .navigation(getContext()));
        }
    }

    @Override
    public void handleBarInteractMsyListPage(ListResult<BarInterActMsgModel> listResult) {
        List<BarInterActMsgModel> list = listResult.getRecords();
        if (current == 1) {
            if (null != list && list.size() > 0) {
                BarInterActMsgModel firstItem = list.get(0);

                String time = firstItem.getCreatedTime();
                String title = firstItem.getAskedDesc();

                tvNewsInterAct.setText(YHStringUtils.value(title));
                tvInterActtime.setText(YXTimeUtils.getFriendlyTimeAtContent(time));
            }
        }
    }


    @Override
    public void handleInterActMsgLastItem(int data) {
        int unreadNum = data;

        if (unreadNum > 0) {
            tvInterActUnreadNum.setVisibility(View.VISIBLE);

            tvInterActUnreadNum.setText(unreadNum > 99 ? "99+" : unreadNum + "");
        } else {
            tvInterActUnreadNum.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleMsgLastItem(MsgLastModel msg) {
        if (null != msg) {
            int unreadNum = msg.getUnreadCount();
            String time = msg.getCreatedTime();
            String title = msg.getTitle();

            tvSysMsgTitle.setText(YHStringUtils.value(title));
            tvSysMsgTime.setText(YXTimeUtils.getFriendlyTimeAtContent(time));

            if (unreadNum > 0) {
                tvSysMsgUnreadNum.setVisibility(View.VISIBLE);

                tvSysMsgUnreadNum.setText(unreadNum > 99 ? "99+" : unreadNum + "");
            } else {
                tvSysMsgUnreadNum.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void handleMsgInviteNum(Integer unreadNum) {
        if (null != unreadNum) {
            llInvite.setNewMessage(unreadNum > 0);
        }
    }

    @Override
    public void handleMsgNewFansNum(Integer unreadNum) {
        if (null != unreadNum) {
            llFans.setNewMessage(unreadNum > 0);
        }
    }

    @Override
    public void handleMsgCommentAtNum(Integer unreadNum) {
        if (null != unreadNum) {
            llComment.setNewMessage(unreadNum > 0);
        }
    }

    @Override
    public void handleMsgFavLikeNum(Integer unreadNum) {
        if (null != unreadNum) {
            llFavLike.setNewMessage(unreadNum > 0);
        }
    }


    /**
     * 观察 view model 各数据以便进行页面刷新操作。
     */
    protected void subscribeUi() {
        //会话列表数据监听
        mConversationListViewModel = new ViewModelProvider(this)
                .get(ConversationListViewModel.class);
        mConversationListViewModel.getConversationList(false, false);
        mConversationListViewModel.getConversationListLiveData().observe(getViewLifecycleOwner(), new Observer<List<BaseUiConversation>>() {
            @Override
            public void onChanged(List<BaseUiConversation> uiConversations) {
                RLog.d(TAG, "conversation list onChanged.");
                mAdapter.setDataCollection(uiConversations);
            }
        });
        //连接状态监听
        mConversationListViewModel.getNoticeContentLiveData().observe(getViewLifecycleOwner(), new Observer<NoticeContent>() {
            @Override
            public void onChanged(NoticeContent noticeContent) {
                // 当连接通知没有显示时，延迟进行显示，防止连接闪断造成画面闪跳。
                if (mNoticeContainerView.getVisibility() == View.GONE) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // 刷新时使用最新的通知内容
                            updateNoticeContent(mConversationListViewModel.getNoticeContentLiveData().getValue());
                        }
                    }, NOTICE_SHOW_DELAY_MILLIS);
                } else {
                    updateNoticeContent(noticeContent);
                }
            }
        });
        //刷新事件监听
        mConversationListViewModel.getRefreshEventLiveData().observe(getViewLifecycleOwner(), new Observer<Event.RefreshEvent>() {
            @Override
            public void onChanged(Event.RefreshEvent refreshEvent) {
                if (refreshEvent.state.equals(RefreshState.LoadFinish)) {
                    mRefreshLayout.finishLoadMore();
                } else if (refreshEvent.state.equals(RefreshState.RefreshFinish)) {
                    mRefreshLayout.finishRefresh();
                }
            }
        });
    }

    protected void onConversationListRefresh(RefreshLayout refreshLayout) {
        if (mConversationListViewModel != null) {
            mConversationListViewModel.getConversationList(false, true);
        }
    }

    protected void onConversationListLoadMore() {
        if (mConversationListViewModel != null) {
            mConversationListViewModel.getConversationList(true, true);
        }
    }

    /**
     * 更新连接状态通知栏
     *
     * @param content
     */
    protected void updateNoticeContent(NoticeContent content) {
        if (content == null) return;

        if (content.isShowNotice()) {
            mNoticeContainerView.setVisibility(View.VISIBLE);
            mNoticeContentTv.setText(content.getContent());
            if (content.getIconResId() != 0) {
                mNoticeIconIv.setImageResource(content.getIconResId());
            }
        } else {
            mNoticeContainerView.setVisibility(View.GONE);
        }
    }

    /**
     * 获取 adapter. 可复写此方法实现自定义 adapter.
     *
     * @return 会话列表 adapter
     */
    protected ConversationListAdapter onResolveAdapter() {
        mAdapter = new ConversationListAdapter();
        mAdapter.setEmptyView(R.layout.rc_conversationlist_empty_view);
        return mAdapter;
    }

    /**
     * 会话列表点击事件回调
     *
     * @param view     点击 view
     * @param holder   {@link ViewHolder}
     * @param position 点击位置
     */
    @Override
    public void onItemClick(View view, ViewHolder holder, int position) {
        if (position < 0) {
            return;
        }
        BaseUiConversation baseUiConversation = mAdapter.getItem(position);
        ConversationListBehaviorListener listBehaviorListener = RongConfigCenter.conversationListConfig().getListener();
        if (listBehaviorListener != null && listBehaviorListener.onConversationClick(view.getContext(), view, baseUiConversation)) {
            RLog.d(TAG, "ConversationList item click event has been intercepted by App.");
            return;
        }
        if (baseUiConversation != null && baseUiConversation.mCore != null) {
            if (baseUiConversation instanceof GatheredConversation) {
                RouteUtils.routeToSubConversationListActivity(view.getContext(), ((GatheredConversation) baseUiConversation).mGatheredType, baseUiConversation.mCore.getConversationTitle());
            } else {
                RouteUtils.routeToConversationActivity(view.getContext(), baseUiConversation.mCore.getConversationType(), baseUiConversation.mCore.getTargetId());
            }
        } else {
            RLog.e(TAG, "invalid conversation.");
        }
    }

    /**
     * 会话列表长按事件回调
     *
     * @param view     点击 view
     * @param holder   {@link ViewHolder}
     * @param position 点击位置
     * @return 事件是否被消费
     */
    @Override
    public boolean onItemLongClick(final View view, ViewHolder holder, int position) {
        if (position < 0) {
            return false;
        }
        final BaseUiConversation baseUiConversation = mAdapter.getItem(position);
        ConversationListBehaviorListener listBehaviorListener = RongConfigCenter.conversationListConfig().getListener();
        if (listBehaviorListener != null && listBehaviorListener.onConversationLongClick(view.getContext(), view, baseUiConversation)) {
            RLog.d(TAG, "ConversationList item click event has been intercepted by App.");
            return true;
        }
        final ArrayList<String> items = new ArrayList<>();
        final String removeItem = view.getContext().getResources().getString(R.string.rc_conversation_list_dialog_remove);
        final String setTopItem = view.getContext().getResources().getString(R.string.rc_conversation_list_dialog_set_top);
        final String cancelTopItem = view.getContext().getResources().getString(R.string.rc_conversation_list_dialog_cancel_top);

        if (!(baseUiConversation instanceof GatheredConversation)) {
            if (baseUiConversation.mCore.isTop()) {
                items.add(cancelTopItem);
            } else {
                items.add(setTopItem);
            }
        }
        items.add(removeItem);
        int size = items.size();
        OptionsPopupDialog.newInstance(view.getContext(), items.toArray(new String[size]))
                .setOptionsPopupDialogListener(new OptionsPopupDialog.OnOptionsItemClickedListener() {
                    @Override
                    public void onOptionsItemClicked(final int which) {
                        if (items.get(which).equals(setTopItem) || items.get(which).equals(cancelTopItem)) {
                            IMCenter.getInstance().setConversationToTop(baseUiConversation.mCore.getConversationType(), baseUiConversation.mCore.getTargetId(),
                                    !baseUiConversation.mCore.isTop(), false, new RongIMClient.ResultCallback<Boolean>() {
                                        @Override
                                        public void onSuccess(Boolean value) {
                                            Toast.makeText(view.getContext(), items.get(which), Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onError(RongIMClient.ErrorCode errorCode) {

                                        }
                                    });
                        } else if (items.get(which).equals(removeItem)) {
                            IMCenter.getInstance().removeConversation(baseUiConversation.mCore.getConversationType(), baseUiConversation.mCore.getTargetId(), null);
                        }
                    }
                }).show();
        return true;
    }

    /**
     * @param view 自定义列表 header view
     */
    public void addHeaderView(View view) {
        mAdapter.addHeaderView(view);
    }

    /**
     * @param view 自定义列表 footer view
     */
    public void addFooterView(View view) {
        mAdapter.addFootView(view);
    }

    /**
     * @param view 自定义列表 空数据 view
     */
    public void setEmptyView(View view) {
        mAdapter.setEmptyView(view);
    }

    /**
     * @param emptyId 自定义列表 空数据的 LayoutId
     */
    public void setEmptyView(@LayoutRes int emptyId) {
        mAdapter.setEmptyView(emptyId);
    }
}
