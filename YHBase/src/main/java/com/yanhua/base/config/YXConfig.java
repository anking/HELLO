package com.yanhua.base.config;


import android.text.TextUtils;

import com.yanhua.base.BaseApplication;
import com.yanhua.core.BuildConfig;
import com.yanhua.core.mmkv.MMKVUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;

/**
 * 生产环境和非生产环境的常量key配置
 */
public class YXConfig {
    public final class imRongMsg {
        public static final String invite = "RC:InviteMsg";//约伴详情
        public static final String momentContent = "RC:MomentContentMsg";//此刻内容（精华日志、头条、图文等）
        public static final String momentVideo = "RC:MomentVideoMsg";//此刻视频、电影集锦
        public static final String bar = "RC:Bar9Msg";//酒吧
        public static final String news = "RC:NewsMsg";//爆料、攻略
        public static final String topicCircle = "RC:TopicCircleMsg";//圈子、话题
        public static final String userPage = "RC:UserPageMsg";//个人主页
    }

    public static final int SHOW_SIDEBAR_MIN_NUM = 8;
    public static final boolean USE_WEB_SHOW_PRIVACY = true;

    public static final int HOME_BREAK_NEWS_SIZE = 6;
    public static final String ALL666 = "6666666666666666666";//爆料中的活动分类ID
    public static final int VIDEO_SELECT_MAX_SECOND = 60 * 6;
    //    public static final String APP_PKG_ID = "com.yanhua.discodev";
    //com.yuexin
    public static final String APP_PKG_ID = "com.yuexin";
    public static final int UNUSE_SET_PRIVACY = -100;
    public static final int PUBLISH_MIN_TIME = 30;
    public static final int ACT_TOP_MIN_NUM = 3;
    public static int SHOW_ME_MIN = 8;//超过最小展示数量，展示更多
    public static boolean ONEKYE_LOGIN = false;

    //短信模板
    public static final class smsWay {
        public static final String modify = "MODIFY";
        public static final String bindingWx = "BINDING_WEIXIN";
        public static final String bindingQQ = "BINDING_QQ";
        public static final String bindingWeibo = "BINDING_WEIBO";
        public static final String login = "LOGIN";
        public static final String logoff = "LOGOFF";
        public static final String register = "REGISTER";
        public static final String payment = "PAYMENT";
    }


    public static final class thirdPlatform {
        //微信
        public static final int wechat = 1;
        //QQ
        public static final int qq = 2;
        //微博
        public static final int weibo = 3;
    }

    public static final class protocol {
        //隐私协议
        public static final String privacyAgreement = "privacyAgreement";
        //用户协议
        public static final String userAgreement = "userAgreement";
        //音圈发布规范
        public static final String publishAgreement = "publishAgreement";
        //移动服务条款用于一键登录（三方）
        public static final String oneKeyLoginProtocol = "oneKeyLoginProtocol";
    }

    public static final class pivacy {
        public static final int privacy_public = 0;//0公开
        public static final int privacy_home = 1;//1仅主页可见
        public static final int privacy_person = 2;//2私密
    }

    public static final class reportType {
        public static final int person = 1;//用户举报
        public static final int moment = 2;//此刻举报
        public static final int disco_best = 3;//集锦举报
        public static final int breakNews = 4;//爆料举报
        public static final int strategy = 5;//攻略举报
        public static final int invite = 6;//约伴举报
        public static final int topic = 7;//话题举报
        public static final int circle = 8;//圈子举报
        public static final int comment = 9;//评论举报
        public static final int question_answer = 10;//问答举报
    }

    public static final String SPACE = "";

    public static final String IMAGE_COVER_RESIZE = ",p_50/format,webp/quality,q_80";////url + "?x-oss-process=image/resize,p_50/format,webp/quality,q_80"
    public static final String IMAGE_RESIZE = "?x-oss-process=image/resize,p_50/format,webp/quality,q_80";////url + "?x-oss-process=image/resize,p_50/format,webp/quality,q_80"
    public static final String REPLACE_YX_DEFAULT_URL = "https://oss.yuexinguoji.com/material/image/2021/09/e62aa9ff2d6a4a23b5644970991c8b24.png";

    //默认城市定位
    public static final String city = "广州市";
    public static final String longitude = "113.280637";
    public static final String latitude = "23.125178";
    public static final int RANGE = 3000;//定位详情

    public static final int PASSWORD_MIN_BIT = 6;
    public static final long VIDEO_CUT_MAX_TIME = 3 * 60;

    public static boolean needShowNoWifi = false;
    public static boolean isOver3Hour = false;
    public static boolean isWIFION = false;
    public static final long DURATION_TIME = 5000;//非WiFi提示框倒计时
    //开关控制处
//    public static final String isEncrypt = BuildConfig.VERSION_NAME;//假如这个为空的话那么不加密，否则加密
    public static final String isEncrypt = "";//假如这个为空的话那么不加密，否则加密

    public static final String LOGIN_TYPE = "APP";

    // 基础设置-移动端: 类型(1:群组设置, 2:约伴设置, 3:消息设置, 4:酒吧设置, 5:文章审核设置, 6:动态设置)
    public static final int BASE_CONFIG_BAR = 4;

    //    类型  1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻   点赞 收藏  评论   暂定这七种类型哈  如果有新增或者觉得不对的  根据自己负责的业务自己增删
    public static final int TYPE_MOMENT = 1;
    public static final int TYPE_INVITE = 2;
    public static final int TYPE_BREAK_NEWS = 3;
    public static final int TYPE_STRATEGY = 4;
    public static final int TYPE_BEST = 5;
    public static final int TYPE_NEWS = 6;
    public static final int TYPE_9BAR = 7;
    public static final int TYPE_TOPIC = 8;
    public static final int TYPE_CIRCLE = 9;
    public static final int TYPE_USER_PAGE = 10;

    public static String getTypeName(int type) {
        String typeName;

        switch (type) {
            case TYPE_MOMENT:
                typeName = "[此刻]";
                break;
            case TYPE_INVITE:
                typeName = "[约伴]";
                break;
            case TYPE_BREAK_NEWS:
                typeName = "[爆料]";
                break;
            case TYPE_STRATEGY:
                typeName = "[攻略]";
                break;
            case TYPE_BEST:
                typeName = "[电音集锦]";
                break;
            case TYPE_NEWS:
                typeName = "[新闻]";
                break;
            case TYPE_9BAR:
                typeName = "[酒吧]";
                break;
            default:
                typeName = "[音圈]";
                break;
        }
        return typeName;
    }


    public static final class adType {
        //广告类型(1:URL,2:搜索字段,3:话题,4:圈子,5:电音集锦、6此刻内容、7此刻视频,8:页面,9:视频,10:酒吧,11:爆料,12:攻略,13:约伴)
        public static final int url = 1;
        public static final int keyword = 2;
        public static final int topic = 3;
        public static final int circle = 4;
        public static final int dynamic_disco_best = 5;
        public static final int dynamic_moment_content = 6;
        public static final int dynamic_moment_video = 7;
        public static final int page = 8;
        public static final int video = 9;
        public static final int bar = 10;
        public static final int breakNews = 11;
        public static final int strategy = 12;
        public static final int invite = 13;
    }

    // attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
    public static final int ATTR_TYPE_BESTEST = 1;
    public static final int ATTR_TYPE_HOT = 2;
    public static final int ATTR_TYPE_RECOMENT = 3;//recommend
    public static final int ATTR_TYPE_NOTE = 4;
    public static final int ATTR_TYPE_TOP = 5;
    public static final int ATTR_TYPE_NEWS_HOT = 6;//爆料 攻略用6
    public static final int ATTR_TYPE_RANKING = 7;

    public static final class AD {
        public static final String main_page = "main_page";// 首页轮播图
        public static final String s_adv = "s_adv";// 开屏广告
        public static final String n_adv = "n_adv";// 此刻
        public static final String nc_adv = "nc_adv";// 此刻评论
        public static final String nv_adv = "nv_adv";// 此刻视频
        public static final String fp_popup_adv = "fp_popup_adv";// 首页弹窗
    }

    public static final int MAX_SHOW_CONFIG = 6;
    public static final int MAX_HOT_LIST = 9;

    public static final int CONTENT_TYPE_TEXT = 4;
    public static final int CONTENT_TYPE_PICTURE = 1;
    public static final int CONTENT_TYPE_VIDEO = 2;
    public static final int CONTENT_TYPE_LONG_TEXT = 3;

    /**
     * 判断时间是否在时间段内*
     *
     * @param strDateBegin 开始时间 00:00:00
     * @param strDateEnd   结束时间 00:05:00
     * @return
     */

    public static boolean isInDate(String strDateBegin,
                                   String strDateEnd) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strDate = sdf.format(date);
        // 截取当前时间时分秒
        int strDateH = Integer.parseInt(strDate.substring(11, 13));
        int strDateM = Integer.parseInt(strDate.substring(14, 16));
        int strDateS = Integer.parseInt(strDate.substring(17, 19));

        // 截取开始时间时分秒
        int strDateBeginH = Integer.parseInt(strDateBegin.substring(0, 2));
        int strDateBeginM = Integer.parseInt(strDateBegin.substring(3, 5));
        int strDateBeginS = Integer.parseInt(strDateBegin.substring(6, 8));
        // 截取结束时间时分秒
        int strDateEndH = Integer.parseInt(strDateEnd.substring(0, 2));
        int strDateEndM = Integer.parseInt(strDateEnd.substring(3, 5));
        int strDateEndS = Integer.parseInt(strDateEnd.substring(6, 8));

        if ((strDateH >= strDateBeginH && strDateH <= strDateEndH)) {
            // 当前时间小时数在开始时间和结束时间小时数之间
            if (strDateH > strDateBeginH && strDateH < strDateEndH) {
                return true;
                // 当前时间小时数等于开始时间小时数，分钟数在开始和结束之间
            } else if (strDateH == strDateBeginH && strDateM >= strDateBeginM
                    && strDateM <= strDateEndM) {
                return true;
                // 当前时间小时数等于开始时间小时数，分钟数等于开始时间分钟数，秒数在开始和结束之间
            } else if (strDateH == strDateBeginH && strDateM == strDateBeginM
                    && strDateS >= strDateBeginS && strDateS <= strDateEndS) {
                return true;
            }
            // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数小等于结束时间分钟数
            else if (strDateH >= strDateBeginH && strDateH == strDateEndH

                    && strDateM <= strDateEndM) {
                return true;
                // 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数等于结束时间分钟数，秒数小等于结束时间秒数

            } else if (strDateH >= strDateBeginH && strDateH == strDateEndH

                    && strDateM == strDateEndM && strDateS <= strDateEndS) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isNewYear() {
        return isEffectiveDate("2022-01-21 00:00:00", "2022-02-21 00:00:00");
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @return
     * @author jqlin
     */
    public static boolean isEffectiveDate(String startTimeStr, String endTimeStr) {
        boolean duringTime = false;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date startTime = null, endTime = null;

        try {
            startTime = sdf.parse(startTimeStr);
            endTime = sdf.parse(endTimeStr);

            Calendar date = Calendar.getInstance();

            Calendar begin = Calendar.getInstance();
            begin.setTime(startTime);

            Calendar end = Calendar.getInstance();
            end.setTime(endTime);

            if (date.after(begin) && date.before(end)) {
                duringTime = true;
            } else {
                duringTime = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            duringTime = false;
        }

        return duringTime;
    }

    public static String getBaseUrl() {
        //从
        if (BaseApplication.getInstance().getPackageName().equals(APP_PKG_ID)) {//此处只针对非生产可操作
            String serverUrl = MMKVUtil.getInstance().getServerUrl();

            if (TextUtils.isEmpty(serverUrl)) {
                serverUrl = BuildConfig.BASE_URL;
            }

            return serverUrl;
        }
        return BuildConfig.BASE_URL;
    }

    public static String getShareUrl() {
        if (BaseApplication.getInstance().getPackageName().equals(APP_PKG_ID)) {//此处只针对非生产可操作
            String serverUrl = MMKVUtil.getInstance().getServerUrl();

            if (TextUtils.isEmpty(serverUrl)) {
                serverUrl = BuildConfig.BASE_URL;
            }

            String SHARE_URL = "";
            if ("http://8.134.13.164:8443".equals(serverUrl)) {
                SHARE_URL = "http://h5-uat.yuexinguoji.com";
            } else if ("http://8.134.53.118:9443".equals(serverUrl)) {
                SHARE_URL = "http://8.134.53.118:9628/#/";
            } else if ("http://121.40.204.59:9443".equals(serverUrl)) {
                SHARE_URL = "http://121.40.204.59:9628/#/";
            } else {
                SHARE_URL = BuildConfig.SHARE_URL;
            }

            return SHARE_URL;
        }
        return BuildConfig.SHARE_URL;
    }

//
//    电音 视频
//    http://121.40.204.59:9628/#/videoDetail?id=1522499242481639425&type=1
//    攻略 视频
//    http://121.40.204.59:9628/#/videoDetail?id=1518919557994762241&type=2
//    视频详情
//    http://121.40.204.59:9628/#/videoDetail?id=1506234930603732994
//
//    攻略、爆料图文
//    http://121.40.204.59:9628/#/glDetail?id=1518848422009884673
//
//    图文
//    http://121.40.204.59:9628/#/newsDetail?id=1507208489312423938
//    头条详情
//    http://121.40.204.59:9628/#/newsDetail?id=1506188590947446786
//
//
//    约伴
//    http://121.40.204.59:9628/#/companion?id=1522517182064898049
//    个人主页
//    http://121.40.204.59:9628/#/personalDetail?id=1514500516672548865
//
//
//    酒吧ktv
//    http://121.40.204.59:9628/#/ba?id=1506234930603732994

    //话题
    public static String getShareTopicUrl(String id, String title) {
        return getShareUrl() + "topic?id=" + id + "&title=" + title;
    }

    //圈子
    public static String getShareCircleListUrl(String id) {
        return getShareUrl() + "qzList?id=" + id;
    }

    //图文\头条详情
    public static String getShareNewsDetailUrl(String id) {
        return getShareUrl() + "newsDetail?id=" + id;
    }

    //约伴详情
    public static String getShareInviteDetailUrl(String id) {
        return getShareUrl() + "companion?id=" + id;
    }

    /**
     * 视频详情\电音集锦 \攻略 视频
     *
     * @param id   内容id
     * @param type {"视频详情 type = 0, 电音集锦 视频 type=1 ","攻略 视频   type = 2"}
     * @return
     */
    public static String getShareVideoDetailUrl(String id, int type) {
        String typeAppend = "";
        switch (type) {
            case 0:
                typeAppend = "";
                break;
            default:
                typeAppend = "&type=" + type;
                break;
        }

        return getShareUrl() + "videoDetail?id=" + id + typeAppend;
    }

    /**
     * 攻略图文
     *
     * @param id 内容id
     * @return
     */
    public static String getShareStrategyDetailUrl(String id) {
        return getShareUrl() + "glDetail?id=" + id;
    }

    /**
     * 爆料文字内容
     *
     * @param id 内容id
     * @return
     */
    public static String getShareBreakNewsDetailUrl(String id) {
        return getShareUrl() + "bao?id=" + id;
    }

    /**
     * 个人主页
     *
     * @param id
     * @return
     */
    public static String getSharPersonalDetailUrl(String id) {
        return getShareUrl() + "personalDetail?id=" + id;
    }

    /**
     * 酒吧ktv
     *
     * @param id
     * @return
     */
    public static String getShar9BarDetaillUrl(String id) {
        return getShareUrl() + "ba?id=" + id;
    }

    public static String getShareDownloadUrl() {
        return getShareUrl() + "downLoad";
    }

    public static String getUserAgreement() {
        return getShareUrl() + "protocol?type=userAgreement";
    }

    public static String getPrivacyAgreement() {
        return getShareUrl() + "protocol?type=privacyAgreement";
    }

    public static final String JPUSH_APPKEY = "ec5db1c545db55dbd0237c6e";
    public static final String JPUSH_MSK = "2e22a88420a3a3976dc00c76";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public final static String prikey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANrOoHPbL6cXvAhkeu4T4y27JxvFXEEjMvKvgHCs73E0+psa6iivu+7Vu8CLBiXpLyM7V5LZ3LSmG+Qfp+/902dmJfRF2v/E6IYQaASO9JbKnu38wyRthWfEz/n+e8hyyZLEGCJtziCYiNcP0oMHnbh1gCbnHJlfcD3k4n4o9f//AgMBAAECgYEAtqjDvbP+xejEaeUn0lP71JNmBILvjtkJkRr9iBJ2GPV3N8ByNeDubGU0xXnJeiB931WMt+TzoEMDCZ97nJcywJUDjfUXfp01B8dqccaHPG/50wfHF5hPK3kOM4sHfeYD4U5c5Uvov7JauhEMfkYlmp5WQ0Cgg10a19u7ZYXuzbECQQD52EEYbjRGcs3Y8T2pIfRzVRe+ZfUAHpU7ETuUxgzlZo70FgMMoLt4J4H7H4KXbFfBwQE7MSn4yVwDLjxtZyETAkEA4DKfFPSKVleGUL10pFPlz0Mo3JrvZlUboFtJ9c0DFymmnr6aYhNVaJQb96XkylpFQQ4PNQf8384i8FW4qawu5QJAd58Uu6+4E6rP5/jI/vuk4LtzHNQQE7iQ1rEPh9GzRvpto2wOlbM3TvYLg8K4ceuLsNbJ0h9d6yHtjhBLGk5wRQJAJPoUZMdkXZM38u8cYtH3kDSac7AWFgGpecw0qZazkMls0rfLDKv1pTbwWesBjiJPU8h2159GpL8B4jOxLkLYxQJBAObwtZrjLgpO8mk6aeJhlJMyi0Fr+gyM7HBzdDAjP89IY/7BgFmFyBz+TZZRyoRs01bwKRZNpvZ45e6QpETEAxM=";
}