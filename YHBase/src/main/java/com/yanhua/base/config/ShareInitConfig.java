package com.yanhua.base.config;

import android.content.Context;

import com.yanhua.core.BuildConfig;

import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.android.api.PlatformConfig;

public class ShareInitConfig {
    public final static String UMENG = "Umeng";

    public void init(Context context) {
/*
        //友盟初始化组件化基础库
        UMConfigure.init(context, BuildConfig.UM_AK, UMENG, UMConfigure.DEVICE_TYPE_PHONE, "");

        String FileProvider = context.getPackageName() + ".FileProvider";
        PlatformConfig.setWeixin(BuildConfig.WX_APPID, BuildConfig.WX_AK);
        PlatformConfig.setWXFileProvider(FileProvider);

        //配置回调地址需要和微博开放平台的授权回调页一致 1531444439
        PlatformConfig.setSinaWeibo(BuildConfig.WEIBO_AK, BuildConfig.WEIBO_APPID, BuildConfig.WEIBO_INVOKE_URL);//appId appkey  redirectUrl http://www.sharesdk.cn
        PlatformConfig.setSinaFileProvider(FileProvider);

        PlatformConfig.setQQZone(BuildConfig.QQ_APPID, BuildConfig.QQ_AK);
        PlatformConfig.setQQFileProvider(FileProvider);

        //QQ官方sdk授权
        Tencent.setIsPermissionGranted(true);*/

        JShareInterface.setDebugMode(BuildConfig.DEBUG);
        PlatformConfig platformConfig = new PlatformConfig()
                .setWechat(BuildConfig.WX_APPID, BuildConfig.WX_AK)
                .setQQ(BuildConfig.QQ_APPID, BuildConfig.QQ_AK)
                .setSinaWeibo(BuildConfig.WEIBO_AK, BuildConfig.WEIBO_APPID, BuildConfig.WEIBO_INVOKE_URL);
        /**
         * since 1.5.0，1.5.0版本后增加API，支持在代码中设置第三方appKey等信息，当PlatformConfig为null时，或者使用JShareInterface.init(Context)时需要配置assets目录下的JGShareSDK.xml
         **/
        JShareInterface.init(context, platformConfig);
    }
}

