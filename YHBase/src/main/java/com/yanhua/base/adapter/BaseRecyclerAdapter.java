package com.yanhua.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;
    private static final int ITEM_VIEW_TYPE_FOOTER = 2;
    protected View header;
    protected View footer;
    private int headerCount = 0;
    private int footerCount = 0;

    protected List<T> mDataList = new ArrayList<>();
    protected Context context;
    private int lastPosition = -1;
    protected LayoutInflater mInflater;

    private RecyclerView recyclerView;
    private OnItemClickListener mClickListener;
    private OnItemLongClickListener mLongClickListener;

    public void addAll(List<T> data) {
        this.mDataList.clear();
        this.mDataList.addAll(data);
        this.notifyDataSetChanged();
    }

    public BaseRecyclerAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager gridManager = ((GridLayoutManager) manager);
            gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return (isHeader(position) || isFooter(position)) ? gridManager.getSpanCount() : 1;
                }
            });
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
            StaggeredGridLayoutManager.LayoutParams p = (StaggeredGridLayoutManager.LayoutParams) lp;
            p.setFullSpan(isHeader(holder.getLayoutPosition()) || isFooter(holder.getLayoutPosition()));
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
    }

    public void addHeaderView(View header) {
        this.header = header;
        headerCount = 1;
    }

    public int getHeaderCount() {
        return headerCount;
    }

    public void addFooterView(View footer) {
        this.footer = footer;
        footerCount = 1;
    }

    public void setItems(List<T> data) {
        this.mDataList = data;
        notifyDataSetChanged();
    }

    /**
     * 注意这个是未关注时候的瀑布流刷新数据
     * 上拉加载更多时，建议刷新上拉加载那一部分数据即可，不用刷新所有数据
     *
     * @param list 瀑布流集合
     */
    public void setMoreData(List<T> list) {
        int start = mDataList.size();
        if (list != null && list.size() > 0) {
            mDataList.addAll(list);

            //去重---
//            //list的原始顺序
//            ArrayList<Object> result = new ArrayList<Object>();
//            for (int i = 0; i < mDataList.size(); i++) {
//                if (!result.contains(list.get(i))) {//如果没有重复元素 ,才添加进结果集合result2
//                    result.add(list.get(i));
//                }
//            }
//            int end = result.size();

            int end = mDataList.size();

            notifyItemRangeInserted(start, end);
        }
    }

    public List<T> getmDataList() {
        return mDataList;
    }

    public T getItemObject(int pos) {
        return mDataList.get(pos);
    }

    public boolean isHeader(int position) {
        if (headerCount == 0) {
            return false;
        }
        return position == 0;
    }

    public boolean isFooter(int position) {
        if (footerCount == 0) {
            return false;
        }
        return position == mDataList.size() + headerCount;
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? headerCount + footerCount : mDataList.size() + headerCount + footerCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return ITEM_VIEW_TYPE_HEADER;
        }
        if (isFooter(position)) {
            return ITEM_VIEW_TYPE_FOOTER;
        }
        return ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isHeader(position)) {
            return;
        }
        if (isFooter(position)) {
            return;
        }
        holder.itemView.setOnClickListener(view -> {
            if (mClickListener != null && view != null && recyclerView != null) {
                int position1 = recyclerView.getChildAdapterPosition(view);
                mClickListener.onItemClick(view, position1);
            }
        });

        holder.itemView.setOnLongClickListener(view -> {
            if (mLongClickListener != null && view != null && recyclerView != null) {
                int position12 = recyclerView.getChildAdapterPosition(view);
                mLongClickListener.onItemLongClick(view, position12);
                return true;
            }
            return false;
        });
        final T object = mDataList.get(position - headerCount);
        onBindViewHolder((V) holder, object);
//        setAnimation(holder.lie_Root, position);
    }

    protected abstract @NonNull
    V onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent);

    protected abstract void onBindViewHolder(@NonNull V holder, @NonNull T item);

    protected final int getPosition(@NonNull final RecyclerView.ViewHolder holder) {
        return holder.getAdapterPosition();
    }

    public void addData(T data) {
        this.mDataList.add(data);
    }

    public void add(int pos, T item) {
        mDataList.add(pos, item);
        notifyItemInserted(pos);
    }

    public void changeValue(int pos, T item) {
        mDataList.set(pos, item);
        notifyItemChanged(pos);
    }

    public void delete(int pos) {
        mDataList.remove(pos);
        notifyItemRemoved(pos);
        if (pos != mDataList.size()) {
            notifyItemRangeChanged(pos, mDataList.size() - pos);
        }
    }

    public void removeAll() {
        mDataList.clear();
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow((V) holder);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mLongClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_HEADER) {
            return new BaseViewHolder(header);
        } else if (viewType == ITEM_VIEW_TYPE_FOOTER) {
            return new BaseViewHolder(footer);
        }
        return onCreateViewHolder(mInflater, parent);
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int pos);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(View itemView, int pos);
    }

    private void setAnimation(View viewToAnimate, int position, int animResID) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, animResID);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
