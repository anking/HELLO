package com.yanhua.base.net;


import androidx.annotation.CallSuper;


import com.yanhua.base.mvp.BaseView;
import com.yanhua.core.util.LogUtils;

import java.io.File;

import io.reactivex.rxjava3.subscribers.ResourceSubscriber;

public abstract class SimpleSubscriber<T> extends ResourceSubscriber<T> {

    private BaseView baseView;
    private boolean showLoading = false;

    public SimpleSubscriber(BaseView baseView) {
        this.baseView = baseView;
    }

    public SimpleSubscriber(BaseView baseView, boolean showLoading) {
        this.baseView = baseView;
        this.showLoading = showLoading;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null != baseView && showLoading) {
            baseView.showLoading();
        }
    }

    @Override
    public void onError(Throwable t) {
        if (null == baseView) {
            return;
        }
        baseView.hideLoading();

        if (t instanceof DataResultException) {
            DataResultException exception = (DataResultException) t;
            baseView.handSystemError(exception.getCode(),exception.getMsg());
            return;
        }

        if (t instanceof ApiException) {
            ApiException exception = (ApiException) t;
            baseView.handSystemError(exception.getCode(),exception.getMessage());
            return;
        }

        String errorMessage = t.getMessage();
        LogUtils.E("HttpResult.errorMessage=" + errorMessage);
        if (errorMessage == null) {
            baseView.handSystemError(HttpCode.SERVER_ERROR,errorMessage);
            return;
        }
        if (errorMessage.startsWith("failed to connect to") || errorMessage.startsWith("Failed to connect to")) {
            // 连不上服务器
            baseView.handSystemError(HttpCode.NET_ERROR,errorMessage);
        } else if (errorMessage.startsWith("timeout")) {
            // 请求超时
            baseView.handSystemError(HttpCode.TIME_OUT,errorMessage);
        } else if (errorMessage.startsWith("HTTP 401 Unauthorized")) {
            // token过期
            baseView.handSystemError(HttpCode.TOKEN_INVALID,errorMessage);
        } else if (errorMessage.startsWith("HTTP 503 Service Unavailable")) {
            // 服务器异常
            baseView.handSystemError(HttpCode.SERVER_ERROR,errorMessage);
        } else if (errorMessage.startsWith("HTTP 500 Internal Server Error")) {
            // 服务器异常
            baseView.handSystemError(HttpCode.SERVER_ERROR,errorMessage);
        }else {
            baseView.handSystemError(HttpCode.SYSTEM_EXCEPTION,errorMessage);
        }

//        errorMessage=Attempt to invoke virtual method 'boolean java.lang.String.equals(java.lang.Object)' on a null object reference
    }

    @CallSuper
    @Override
    public void onNext(T t) {
        if (baseView != null && showLoading) {
            baseView.hideLoading();
        }
    }

    @Override
    public void onComplete() {
        if (file != null) {
            file.delete();
        }
    }

    private File file;

    public void setFile(File file) {
        this.file = file;
    }

}
