package com.yanhua.base.net;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * 自定义GsonResponseBodyConverter
 */

public class GsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private final Gson gson;
    private final Type type;

    public GsonResponseBodyConverter(Gson gson, Type type) {
        this.gson = gson;
        this.type = type;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        String response = value.string();
        try {
            HttpResult result = gson.fromJson(response, HttpResult.class);
            if (result.getCode().equals(HttpCode.SYSTEM_EXCEPTION)) {
                if (result.getData() instanceof String) {
                    String data = (String) result.getData();
                    throw new DataResultException(!TextUtils.isEmpty(data) ? data : result.getMesg(), result.getCode());
                }
            }
            if (result.getCode().equals(HttpCode.SYSTEM_ERROR) || result.getCode().equals(HttpCode.SYSTEM_500)) {
                throw new ApiException(result.getCode(), result.getMesg());
            }
            if (result.getCode().equals(HttpCode.LOGIN_EXPIRATION)) {
                throw new ApiException(result.getCode(), result.getMesg());
            }

            return gson.fromJson(response, type);
        } finally {
            value.close();
        }
    }
}
