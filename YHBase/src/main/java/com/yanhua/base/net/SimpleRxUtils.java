package com.yanhua.base.net;


import com.yanhua.base.model.HttpResult;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.FlowableTransformer;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SimpleRxUtils {

    /**
     * 从其他线程转到主线程.
     *
     * @param scheduler Schedulers.io()等等
     * @param <T>       t
     * @return FlowableTransformer
     */
    public static <T> FlowableTransformer<T, T> toMain(Scheduler scheduler) {
        return upstream -> upstream.subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread());
    }


    public static <T> FlowableTransformer<HttpResult<T>, T> handleTokenResult() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(tHttpResult -> {
                    if (tHttpResult.getCode().equals(HttpCode.SUCCESS)) {
                        return Flowable.just(tHttpResult.getData());
                    } else {
                        return Flowable.error(new ApiException(tHttpResult.getCode(), tHttpResult.getMesg()));
                    }
                });
    }

    public static <T> FlowableTransformer<HttpResult<T>, T> handResult() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(tHttpResult -> {
                    if (tHttpResult.getCode().equals(HttpCode.SUCCESS)) {
                        return Flowable.just(tHttpResult.getData());
                    } else {
                        return Flowable.error(new ApiException(tHttpResult.getCode(), tHttpResult.getMesg()));
                    }
                });
    }

    private static <T> Flowable<T> createData(final T data) {
        return Flowable.create(e -> {
            e.onNext(data);
            e.onComplete();
        }, BackpressureStrategy.ERROR);
    }

}
