package com.yanhua.base.net;


import java.io.IOException;

public class ApiException extends IOException {

    private String code;
    private String message;

    public ApiException(String code) {
      this.code = code;
    }

    public ApiException(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
      return code;
    }

    public ApiException setCode(String code) {
      this.code = code;
      return this;
    }

}
