package com.yanhua.base.net;

import java.io.IOException;

/**
 * @author Administrator
 */
public class DataResultException extends IOException {
    private String msg;
    private String code;

    public DataResultException(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
