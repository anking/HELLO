package com.yanhua.base.net;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.RefreshTokenResult;
import com.yanhua.core.mmkv.MMKVUtil;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Token过期拦截器
 */
public class AuthenticatorInterceptor implements Interceptor {

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        // 获取请求
        Request request = chain.request();
        // 获取响应
        Response response = chain.proceed(request);
        // 在这里判断是不是是token失效
        // 当然，判断条件不会这么简单，会有更多的判断条件的
        if (response.code() == 500) {//根据系统定义
            String responseBody = response.body().string();

            MediaType mediaType;

            if (response.body() != null && response.body().contentType() != null) {
                mediaType = response.body().contentType();
            } else {
                mediaType = MediaType.parse("application/json;charset=utf-8");
            }

            HttpResult result = new Gson().fromJson(responseBody, HttpResult.class);

            response = response.newBuilder()
                    .body(ResponseBody.create(mediaType, responseBody))
                    .build();

            if (result == null) {
                return response;
            }
            if (result.getCode().equals(HttpCode.LOGIN_EXPIRATION)) {
                // 这里应该调用自己的刷新token的接口
                // 这里发起的请求是同步的，刷新完成token后再增加到header中
                // 这里抛出的错误会直接回调 onError
                String token = refreshToken();
                if (!TextUtils.isEmpty(token)) {
                    // 创建新的请求，并增加header
                    Request retryRequest = chain.request()
                            .newBuilder()
                            .header("Authorization", getToken())
                            .build();

                    // 再次发起请求
                    return chain.proceed(retryRequest);
                }
            }
        }

        return response;
    }

    private String refreshToken() {
        String newtoken = null;

        String url = YXConfig.getBaseUrl() + "/authorization-server/oauth/token?";

        /**
         * 必须使用同步请求
         */
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("type", "APP")
                .add("grant_type", "refresh_token")
                .add("scope", "read")
                .add("client_id", "app_client")
                .add("client_secret", "test_secret")
                .add("refresh_token", MMKVUtil.getInstance().getRefreshToken())
                .build();

        Request request = new Request.Builder().url(url).post(requestBody).build();
        okhttp3.Call call = client.newCall(request);

        try {
            Response response = call.execute();

            Type type = new TypeToken<HttpResult<RefreshTokenResult>>() {
            }.getType();

            String content = response.body().string();

            HttpResult<RefreshTokenResult> resultHttpResult = new Gson().fromJson(content, type);

            RefreshTokenResult result = (RefreshTokenResult) resultHttpResult.getData();

            if (null != result.getAccess_token() && null != result.getRefresh_token()) {
                newtoken = result.getAccess_token();
                MMKVUtil.getInstance().setLoginToken(result.getAccess_token());
                MMKVUtil.getInstance().setRefreshToken(result.getRefresh_token());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newtoken;
    }

    private String getToken() {
        return "Bearer " + MMKVUtil.getInstance().getLoginToken();
    }

    private String getRefreshToken() {
        return MMKVUtil.getInstance().getRefreshToken();
    }
}


