package com.yanhua.base.net;

import com.yanhua.base.config.YXConfig;

public class ApiServiceFactory {
    public static <T> T createAPIService(Class<T> clz) {
        //此处到时根据环境不同进行切换
//        String baseUrl = "https://mobile.yuexinguoji.com";//http://121.40.204.59:9443/authorization-server/oauth/token?username=13288666111&password=123456&grant_type=password&scope=read&type=APP&client_id=test_client&client_secret=test_secret
        return GpRetrofitManager.getInstance().getRetrofit(YXConfig.getBaseUrl()).create(clz);
    }

    public static <T> T createAPIService(String baseUrl, Class<T> clz) {
        return GpRetrofitManager.getInstance().getRetrofit(baseUrl).create(clz);
    }

    public static <T> T createAPIService(String baseUrl, Class<T> clz,int uPloadTime) {
        return GpRetrofitManager.getInstance().getRetrofit(baseUrl,uPloadTime).create(clz);
    }
}
