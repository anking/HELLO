package com.yanhua.base.net;

import android.text.TextUtils;


import com.yanhua.base.BuildConfig;
import com.yanhua.core.mmkv.MMKVUtil;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;

public class GpRetrofitManager {
    private static final int DEFAULT_TIMEOUT = 60;
    private static final int DEFAULT_WRITE_TIMEOUT = 60;

    private HashMap<String, Retrofit> mRetrofits = new HashMap<>();

    public Retrofit getRetrofit(String baseUrl) {
        Retrofit retrofit = mRetrofits.get(baseUrl);
        if (retrofit == null) {
            OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();

            okBuilder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
            okBuilder.readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
            okBuilder.writeTimeout(DEFAULT_WRITE_TIMEOUT, TimeUnit.SECONDS);
            okBuilder.addInterceptor(new AuthenticatorInterceptor());//token过期
            okBuilder.addInterceptor(new DataRSAInterceptor());//加解密
            setLoggingLevel(okBuilder);

            retrofit = new Retrofit.Builder().client(okBuilder.build())
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(ResponseConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .baseUrl(baseUrl)
                    .build();
        }
        mRetrofits.put(baseUrl, retrofit);
        return retrofit;
    }

    public Retrofit getRetrofit(String baseUrl, int time) {
        Retrofit retrofit = mRetrofits.get(baseUrl);
        if (retrofit == null) {
            OkHttpClient.Builder okBuilder = new OkHttpClient.Builder().addInterceptor(chain -> {
                Request.Builder builder = chain.request()
                        .newBuilder()
                        .addHeader("Content-Type", "application/json;charset=UTF-8")
                        .addHeader("Connection", "keep-alive");

                boolean isLogin = MMKVUtil.getInstance().isLogin();
                if (isLogin && !TextUtils.isEmpty(MMKVUtil.getInstance().getLoginToken())) {
                    builder.addHeader("Authorization", "Bearer " + MMKVUtil.getInstance().getLoginToken());
                }
                Request request = builder.build();
                return chain.proceed(request);
            });

            okBuilder.connectTimeout(time, TimeUnit.SECONDS);
            okBuilder.readTimeout(time, TimeUnit.SECONDS);
            okBuilder.writeTimeout(time, TimeUnit.SECONDS);
            setLoggingLevel(okBuilder);

            retrofit = new Retrofit.Builder().client(okBuilder.build())
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(ResponseConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .baseUrl(baseUrl)
                    .build();
        }
        mRetrofits.put(baseUrl, retrofit);
        return retrofit;
    }

    /**
     * 设置日志拦截
     *
     * @param builder
     */
    private void setLoggingLevel(OkHttpClient.Builder builder) {
        //okhttp日志拦截器
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        //是否开启日志
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        //添加日志拦截器
        builder.addInterceptor(loggingInterceptor);
    }

    private static class SingletonHolder {
        private static final GpRetrofitManager INSTANCE = new GpRetrofitManager();
    }

    //获取单例
    public static GpRetrofitManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
