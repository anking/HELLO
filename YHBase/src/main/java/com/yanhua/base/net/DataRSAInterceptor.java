package com.yanhua.base.net;

import android.text.TextUtils;

import com.yanhua.base.config.YXConfig;
import com.yanhua.core.BuildConfig;
import com.yanhua.core.mmkv.MMKVUtil;
import com.yanhua.core.util.LogUtils;
import com.yanhua.core.util.encrypt.RSACoderUtil;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class DataRSAInterceptor implements Interceptor {

    public static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    //首字母转大写
    public static String toUpperCaseFirstOne(String s) {
        if (Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        //请求
        Request request = chain.request();
        Response response = null;

        String method = request.method();
        HttpUrl url = request.url();

//        String path = url.encodedPath();
//        String scheme = url.scheme();
//        String host = url.host();
//        int port = url.port();

//        String httpurl = url.url().toString();

        if (method.equals("POST") || method.equals("PUT") || method.equals("PATCH")) {
            RequestBody oldRequestBody = request.body();
            MediaType contentType = oldRequestBody.contentType();

            /*如果是二进制上传  则不进行加密*/
            if (contentType != null && contentType.type().toLowerCase().equals("multipart")) {
                Request.Builder builder = request
                        .newBuilder()
                        .addHeader("Content-Type", "application/json;charset=UTF-8")
                        .addHeader("Connection", "keep-alive");

                boolean isLogin = MMKVUtil.getInstance().isLogin();
                if (isLogin && !TextUtils.isEmpty(MMKVUtil.getInstance().getLoginToken())) {
                    builder.addHeader("Authorization", "Bearer " + MMKVUtil.getInstance().getLoginToken());
                }

                request = builder.build();
                response = chain.proceed(request);

                //---------------------
                if (BuildConfig.DEBUG) {
                    ResponseBody body = response.body();
                    String contend = body.string();
                    MediaType contentTypeLog = body.contentType();

                    String urlApi = response.request().url().encodedPath();
                    logResponse(urlApi, contend, !TextUtils.isEmpty(YXConfig.isEncrypt));

                    ResponseBody responseBody = ResponseBody.create(contentTypeLog, contend);
                    response = response.newBuilder().body(responseBody).build();
                }
                //---------------------

                return response;
            }
            String encodedQuery = url.encodedQuery();

            String encryptQueryparamNames = encodedQuery;
            if (!TextUtils.isEmpty(encodedQuery) && !TextUtils.isEmpty(YXConfig.isEncrypt)) {
                encryptQueryparamNames = RSACoderUtil.encryptRSA(encodedQuery);
            }

            Buffer requestBuffer = new Buffer();

            oldRequestBody.writeTo(requestBuffer);
            String oldBodyStr = requestBuffer.readUtf8();
            requestBuffer.close();

            LogUtils.E("加密前参数body：" + oldBodyStr + "===query" + encodedQuery);

            String encryptData = oldBodyStr;
            if (!TextUtils.isEmpty(oldBodyStr) && !TextUtils.isEmpty(YXConfig.isEncrypt)) {
//                String requestData = URLDecoder.decode(oldBodyStr, "utf-8");
                encryptData = RSACoderUtil.encryptRSA(oldBodyStr);
            }

            LogUtils.E("加密后参数body：" + encryptData + "===query" + encryptQueryparamNames);

            /*构建新的请求体*/
            RequestBody newRequestBody = RequestBody.create(contentType, encryptData);
            /*构建新的requestBuilder*/
            Request.Builder newRequestBuilder = request.newBuilder();
            //根据请求方式构建相应的请求
            if (method.equals("POST")) {
                newRequestBuilder.post(newRequestBody);
            } else if (method.equals("PUT")) {
                newRequestBuilder.put(newRequestBody);
            } else {
                newRequestBuilder.post(newRequestBody);
            }

            newRequestBuilder.url(request.url()
                    .newBuilder().encodedQuery(encryptQueryparamNames) //data是需要的字段
                    .build());

            newRequestBuilder.addHeader("Content-Type", "application/json;charset=UTF-8");
            newRequestBuilder.addHeader("Connection", "keep-alive");

            if (!TextUtils.isEmpty(YXConfig.isEncrypt)) {
                newRequestBuilder.addHeader("version", YXConfig.isEncrypt);
            }

            boolean isLogin = MMKVUtil.getInstance().isLogin();
            if (isLogin && !TextUtils.isEmpty(MMKVUtil.getInstance().getLoginToken())) {
                newRequestBuilder.addHeader("Authorization", "Bearer " + MMKVUtil.getInstance().getLoginToken());
            }

            request = newRequestBuilder.build();

            response = chain.proceed(request);
            if (!TextUtils.isEmpty(YXConfig.isEncrypt)) {
                response = decrypt(response);
            }

            //---------------------
            if (BuildConfig.DEBUG) {
                ResponseBody body = response.body();
                String contend = body.string();
                MediaType contentTypeLog = body.contentType();

                String urlApi = response.request().url().encodedPath();
                logResponse(urlApi, contend, !TextUtils.isEmpty(YXConfig.isEncrypt));

                ResponseBody responseBody = ResponseBody.create(contentTypeLog, contend);
                response = response.newBuilder().body(responseBody).build();
            }
            //---------------------
            return response;
        }

        Request.Builder builder = request
                .newBuilder()
                .addHeader("Content-Type", "application/json;charset=UTF-8")
                .addHeader("Connection", "keep-alive");

        boolean isLogin = MMKVUtil.getInstance().isLogin();
        if (isLogin && !TextUtils.isEmpty(MMKVUtil.getInstance().getLoginToken())) {
            builder.addHeader("Authorization", "Bearer " + MMKVUtil.getInstance().getLoginToken());
        }

        request = builder.build();

        response = chain.proceed(request);
        //---------------------
        if (BuildConfig.DEBUG) {
            ResponseBody body = response.body();
            String contend = body.string();
            MediaType contentTypeLog = body.contentType();

            String urlApi = response.request().url().encodedPath();
            logResponse(urlApi, contend, !TextUtils.isEmpty(YXConfig.isEncrypt));

            ResponseBody responseBody = ResponseBody.create(contentTypeLog, contend);
            response = response.newBuilder().body(responseBody).build();
        }
        //---------------------
        return response;
    }

    /**
     * 解密过程
     *
     * @param response
     * @return
     * @throws IOException
     */
    private Response decrypt(Response response) throws IOException {
        ResponseBody body = response.body();
        String contend = body.string();

        MediaType contentType = body.contentType();

        String decryptStr = RSACoderUtil.decryptRSA(contend);

        LogUtils.E("解密后数据：" + response.request().url().encodedPath() + "===" + decryptStr);

        ResponseBody responseBody = ResponseBody.create(contentType, decryptStr);
        response = response.newBuilder().body(responseBody).build();
        return response;
    }

    /**
     * debug 展示
     * @param url
     * @param contend
     * @param isde
     */
    private void logResponse(String url, String contend, boolean isde) {
        LogUtils.E("---logResponse---" + (isde ? "解密后数据：" : "请求后数据：") + url + "===" + contend);
    }
}