package com.yanhua.base.net;


public interface HttpCode {
    /**
     * 成功.
     */
    String SUCCESS = "000000";


    String SYSTEM_500 = "500";

    /**
     * 系统异常
     */
    String SYSTEM_ERROR = "040000";

    /**
     * token过期退出
     */
    String LOGIN_EXPIRATION = "040001";

    /**
     * 系统异常
     */
    String SYSTEM_EXCEPTION = "-1";

    /**
     * 网络异常
     */
    String NET_ERROR = "network_error";

    /**
     * 请求超时
     */
    String TIME_OUT = "time_out";

    /**
     * token过期
     */
    String TOKEN_INVALID = "token_invalid";

    /**
     * 服务器异常
     */
    String SERVER_ERROR = "server_error";
}
