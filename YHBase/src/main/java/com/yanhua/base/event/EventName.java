package com.yanhua.base.event;

/**
 * 定义事件名称
 */
public class EventName {
    public static final String LOGIN_PAGE_CLOSE = "LOGIN_PAGE_CLOSE";

    public static final String SELECT_CITY = "select_city";

    public static final String SELECT_BAR_CITY = "select_bar_city";
}
