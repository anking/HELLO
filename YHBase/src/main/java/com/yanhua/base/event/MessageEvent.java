package com.yanhua.base.event;

import java.io.Serializable;

public class MessageEvent {

    public  String eventName;
    private String value;
    private int  type;
    private Serializable serializable;


    public MessageEvent(String eventName) {
        this.eventName = eventName;
    }
    public MessageEvent(String eventName, int type) {
        this.eventName = eventName;
        this.type = type;
    }

    public MessageEvent(String eventName, String value) {
        this.eventName = eventName;
        this.value = value;
    }

    public MessageEvent(String eventName, Serializable serializable) {
        this.eventName = eventName;
        this.serializable = serializable;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Serializable getSerializable() {
        return serializable;
    }


    @Override
    public String toString() {
        return "MessageEvent{" +
                "eventName='" + eventName + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

}
