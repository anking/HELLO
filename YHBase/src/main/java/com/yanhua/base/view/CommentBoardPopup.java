package com.yanhua.base.view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.base.R;


/**
 * 评论弹窗
 * <p>
 * 第一行为站内好友（App内好友）
 * 第二行站外分享（私信和群、微信、qq、新浪）
 * 第三行操作栏（权限设置、拉黑、举报、删除等）
 *
 * @author Administrator
 */
public class CommentBoardPopup extends BottomPopupView implements View.OnClickListener {

    private LinearLayout llTop, llReply, llCopy, llReport, llDelete, llDetail, llLike;
    private TextView tvCancel;
    private Context mContext;
    private boolean isTop, isReply, isCopy, isReport, isDelete, isDetail, isLike;

    public CommentBoardPopup(@NonNull Context context, boolean top, boolean reply, boolean copy, boolean report, boolean delete) {
        super(context);
        this.mContext = context;
        this.isTop = top;
        this.isReply = reply;
        this.isCopy = copy;
        this.isReport = report;
        this.isDelete = delete;
    }

    public CommentBoardPopup(@NonNull Context context, boolean top, boolean reply, boolean copy, boolean report, boolean delete, boolean detail, boolean like) {
        this(context, top, reply, copy, report, delete);

        this.isDetail = detail;
        this.isLike = like;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.comment_board_popup;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        llTop = findViewById(R.id.llTop);
        llReply = findViewById(R.id.llReply);
        llCopy = findViewById(R.id.llCopy);
        llReport = findViewById(R.id.llReport);
        llDelete = findViewById(R.id.llDelete);
        tvCancel = findViewById(R.id.tvCancel);

        llLike = findViewById(R.id.llLike);
        llDetail = findViewById(R.id.llDetail);


        llTop.setVisibility(isTop ? VISIBLE : GONE);
//        llTop.setVisibility(GONE);//先屏蔽置顶

        llReply.setVisibility(isReply ? VISIBLE : GONE);
        llCopy.setVisibility(isCopy ? VISIBLE : GONE);
        llReport.setVisibility(isReport ? VISIBLE : GONE);
        llDelete.setVisibility(isDelete ? VISIBLE : GONE);
        llLike.setVisibility(isLike ? VISIBLE : GONE);
        llDetail.setVisibility(isDetail ? VISIBLE : GONE);

        llTop.setOnClickListener(this);
        llReply.setOnClickListener(this);
        llCopy.setOnClickListener(this);
        llReport.setOnClickListener(this);
        llDelete.setOnClickListener(this);
        llLike.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.llTop) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_TOP);
            }
        } else if (view.getId() == R.id.llReply) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_REPLY);
            }
        } else if (view.getId() == R.id.llCopy) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_COPY);
            }
        } else if (view.getId() == R.id.llReport) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_REPORT);
            }
        } else if (view.getId() == R.id.llLike) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_LIKE);
            }
        } else if (view.getId() == R.id.llDetail) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_DETAIL);
            }
        } else if (view.getId() == R.id.llDelete) {
            if (listener != null) {
                listener.onActionItemClick(ACTION_DELETE);
            }

        } else if (view.getId() == R.id.tvCancel) {
        }

        dismiss();
    }

    public final static int ACTION_TOP = 1;
    public final static int ACTION_REPLY = 2;
    public final static int ACTION_COPY = 3;
    public final static int ACTION_REPORT = 4;
    public final static int ACTION_DELETE = 5;
    public final static int ACTION_LIKE = 6;
    public final static int ACTION_DETAIL = 7;
    private OnActionItemListener listener;

    public void setOnActionItemListener(OnActionItemListener l) {
        listener = l;
    }

    public interface OnActionItemListener {
        void onActionItemClick(int actionType);
    }
}
