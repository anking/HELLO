package com.yanhua.base.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.base.R;
import com.yanhua.base.config.YXConfig;


/**
 * 分享弹窗
 * <p>
 * 第一行为站内好友（App内好友）
 * 第二行站外分享（私信和群、微信、qq、新浪）
 * 第三行操作栏（权限设置、拉黑、举报、删除等）
 *
 * @author Administrator
 */
public class ShareBoardPopup extends BottomPopupView implements View.OnClickListener {
    private Context mContext;
    private boolean isBlock;
    private boolean hideShareWays;
    private boolean hideStopInvite;
    private boolean hideDelete;

    private boolean hideBlack;
    private boolean isMyself;
    private int auditStatus;// 内容的审核状态
    private int pricavyType;// 内容隐私权限
    private TextView tvTitle;
    private HorizontalScrollView llRecentlyPart;//音圈内好友
    private LinearLayout ll_recently_chat;
    private HorizontalScrollView llShareWay;
    private HorizontalScrollView llOtherWay;

    public static final int BOARD_SHARE_SYSTEM = -1;
    public static final int BOARD_SHARE_APP = 0;
    public static final int BOARD_SHARE_WECHAT_MOMENT = 1;
    public static final int BOARD_SHARE_WECHAT_FRIEND = 2;
    public static final int BOARD_SHARE_SINA = 3;
    public static final int BOARD_SHARE_QQ_ZONE = 4;
    public static final int BOARD_SHARE_QQ_FRIEND = 5;
    public static final int BOARD_SET_PRIVATE = 6;
    public static final int BOARD_SET_COPY = 7;
    public static final int BOARD_SET_DELETE = 8;
    public static final int BOARD_SET_REPORT = 9;
    public static final int BOARD_SET_BLACK = 10;
    public static final int BOARD_STOP_INVITE = 11;

    public interface OnShareItemClickListener {
        /**
         * 点击分享Item
         *
         * @param position 0 qq好友
         *                 1 qq空间
         *                 2 微信好友
         *                 3 微信朋友圈
         *                 4 微博
         */
        void onShareItemClick(int position);
    }

    private OnShareItemClickListener listener;

    /**
     * 设置分享点击接口
     *
     * @param listener
     */
    public void setOnShareItemClickListener(OnShareItemClickListener listener) {
        this.listener = listener;
    }


    /**
     * 用于邀约中
     *
     * @param context
     * @param isBlock
     * @param hideBlack
     * @param isMyself
     * @param auditStatus
     * @param pricavyType
     * @param hideShareWays
     * @param hideStopInvite
     * @param hideDelete
     */
    public ShareBoardPopup(Activity context, boolean isBlock, boolean hideBlack, boolean isMyself, int auditStatus, int pricavyType,
                           boolean hideShareWays, boolean hideStopInvite, boolean hideDelete) {
        super(context);
        this.mContext = context;
        this.isBlock = isBlock;
        this.hideBlack = hideBlack;
        this.isMyself = isMyself;
        this.auditStatus = auditStatus;
        this.pricavyType = pricavyType;
        this.hideShareWays = hideShareWays;
        this.hideStopInvite = hideStopInvite;
        this.hideDelete = hideDelete;
    }


    public ShareBoardPopup(Activity context, boolean isBlock, boolean hideBlack, boolean isMyself, int auditStatus, int pricavyType) {
        super(context);
        this.mContext = context;
        this.isBlock = isBlock;
        this.hideBlack = hideBlack;
        this.isMyself = isMyself;
        this.auditStatus = auditStatus;
        this.pricavyType = pricavyType;
        this.hideShareWays = false;
        this.hideStopInvite = true;
        this.hideDelete = false;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_board_share;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        tvTitle = findViewById(R.id.tvTitle);
        llRecentlyPart = findViewById(R.id.llRecentlyPart);
        ll_recently_chat = findViewById(R.id.ll_recently_chat);

        llShareWay = findViewById(R.id.llShareWay);

        findViewById(R.id.llShareApp).setOnClickListener(this);
        findViewById(R.id.ll_qq_friends).setOnClickListener(this);
        findViewById(R.id.ll_qq_zone).setOnClickListener(this);
        findViewById(R.id.ll_wechat_friends).setOnClickListener(this);
        findViewById(R.id.ll_wechat_line).setOnClickListener(this);
        findViewById(R.id.ll_sina).setOnClickListener(this);

        //其他设置---需要根据配置进行控制
        LinearLayout ll_copy_url = findViewById(R.id.ll_copy_url);
        LinearLayout ll_delete_content = findViewById(R.id.ll_delete_content);
        LinearLayout ll_privacy_setting = findViewById(R.id.ll_privacy_setting);
        LinearLayout ll_pull_black = findViewById(R.id.ll_pull_black);
        LinearLayout ll_report = findViewById(R.id.ll_report);
        LinearLayout llShowStopInvite = findViewById(R.id.llShowStopInvite);


        TextView tv_cancel = findViewById(R.id.tv_cancel);
        TextView tv_block = findViewById(R.id.tv_block);
        ll_copy_url.setOnClickListener(this);
        ll_delete_content.setOnClickListener(this);
        ll_privacy_setting.setOnClickListener(this);
        ll_pull_black.setOnClickListener(this);
        ll_report.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        llShowStopInvite.setOnClickListener(this);


        if (hideBlack) {
            ll_pull_black.setVisibility(GONE);
        } else {
            tv_block.setText(isBlock ?
                    mContext.getResources().getString(R.string.share_cancel_pull_back)
                    : mContext.getResources().getString(R.string.share_pull_back));
        }

        if (hideDelete) {
            ll_delete_content.setVisibility(GONE);
        } else {
            ll_delete_content.setVisibility(isMyself ? VISIBLE : GONE);
        }


        ll_report.setVisibility(isMyself ? GONE : VISIBLE);
        ll_privacy_setting.setVisibility(isMyself ? (pricavyType != YXConfig.UNUSE_SET_PRIVACY ? VISIBLE : GONE) : GONE);

        llShowStopInvite.setVisibility(hideStopInvite ? GONE : VISIBLE);
        llShareWay.setVisibility(hideShareWays ? GONE : VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ll_qq_friends) {
            dismiss();

            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_QQ_FRIEND);
            }
        } else if (view.getId() == R.id.ll_qq_zone) {
            dismiss();

            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_QQ_ZONE);
            }
        } else if (view.getId() == R.id.ll_wechat_friends) {
            dismiss();

            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_WECHAT_FRIEND);
            }
        } else if (view.getId() == R.id.ll_wechat_line) {
            dismiss();

            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_WECHAT_MOMENT);
            }
        } else if (view.getId() == R.id.ll_sina) {
            dismiss();

            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_SINA);
            }
        } else if (view.getId() == R.id.ll_pull_black) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SET_BLACK);
            }
        } else if (view.getId() == R.id.ll_report) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SET_REPORT);
            }
        } else if (view.getId() == R.id.ll_delete_content) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SET_DELETE);
            }
        } else if (view.getId() == R.id.ll_copy_url) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SET_COPY);
            }
        } else if (view.getId() == R.id.tv_cancel) {
            dismiss();
        } else if (view.getId() == R.id.llShareApp) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SHARE_APP);
            }
        } else if (view.getId() == R.id.ll_privacy_setting) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_SET_PRIVATE);
            }
        } else if (view.getId() == R.id.llShowStopInvite) {
            dismiss();
            if (listener != null) {
                listener.onShareItemClick(BOARD_STOP_INVITE);
            }
        }
    }
}
