package com.yanhua.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ClickUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.github.dfqin.grantor.PermissionsUtil;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.utils.AppStatusConstant;
import com.yanhua.base.utils.AppStatusManager;
import com.yanhua.core.util.ActivityPageManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity
        implements IBusinessView {

    protected String TAG;
    protected String className;
    public View mContentView;
    public Activity mActivity;
    public Context mContext;

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onDebouncingClick(v);
        }
    };

    protected boolean isCheckAppStatus() {
        return false;
    }

    private void validateAppStatus() {
        int appStatus = AppStatusManager.getInstance().getAppStatus();
        Log.e("--TAG--", TAG + "=======getAppStatus=======");

        if (appStatus == AppStatusConstant.STATUS_FORCE_KILLED) {
            // 异常退出
            Log.e("--TAG--", TAG + "=======validateAppStatus=======异常退出");
            restartApp(mContext);
        } else if (appStatus == AppStatusConstant.STATUS_NORMAL) {
            //不需要处理或者初始方法调用
        }
    }


    /**
     * 内存不足回收，重新启动APP
     */
    public void restartApp(Context context) {
        ActivityPageManager.getInstance().exit(context);

        Class aClass = null;
        try {
            aClass = Class.forName(AppStatusManager.getInstance().getSplashPageName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, aClass);
        //清空任务栈，重启新的任务栈
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mActivity = this;
        mContext = this;
        super.onCreate(savedInstanceState);

        TAG = this.getClass().getSimpleName();
        boolean isCheckStatus = isCheckAppStatus();
        if (!isCheckStatus) {
            //校验APP状态
            validateAppStatus();
        }

        Log.v("--TAG--", TAG + "=======onCreate=======" + isCheckStatus);

        setContentView();

        ARouter.getInstance().inject(this);
        ActivityPageManager.getInstance().addActivity(this);
        ButterKnife.bind(this);

        initView(savedInstanceState, mContentView);//view初始化

        EventBus.getDefault().register(this);

        initData(getIntent().getExtras());//数据传递
        doBusiness();//业务逻辑
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        ARouter.getInstance().inject(this);
    }


    /**
     * @param debouncing 点击效果
     * @param views
     */
    public void applyDebouncingClickListener(boolean debouncing, View... views) {
        ClickUtils.applyGlobalDebouncing(views, mClickListener);

        if (debouncing) {
            ClickUtils.applyPressedViewScale(views);
        }
    }

    //保留以前操作方式，避免浪费没必要的时间
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityPageManager.getInstance().removeActivity(this);

        EventBus.getDefault().unregister(this);
    }

    /**
     * 隐藏软键盘
     */
    public void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 检查权限
     *
     * @param permission
     * @param listener
     */
    public void requestCheckPermission(String[] permission, PermissionListener listener) {
        if (PermissionsUtil.hasPermission(this, permission)) {
            if (listener != null) {
                listener.permissionGranted(permission);
            }
        } else {
            PermissionsUtil.requestPermission(this, listener, permission);
        }
    }
}

