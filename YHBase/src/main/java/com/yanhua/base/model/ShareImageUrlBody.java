package com.yanhua.base.model;

import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.Serializable;

/**
 * 分享体
 */
public class ShareImageUrlBody implements Serializable {
    private String id;
    private String webUrl;
    private String title;
    private String desc;
    private int position;
    private Bitmap shareBitmap;

    public Bitmap getShareBitmap() {
        return shareBitmap;
    }

    public void setShareBitmap(Bitmap shareBitmap) {
        this.shareBitmap = shareBitmap;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        //假如这里title为空怎么处理》》》
        if (TextUtils.isEmpty(title)) {
            this.title = "悦享生活";
        } else {
            this.title = title;
        }
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        if (TextUtils.isEmpty(desc)) {
            this.desc = "您的好友正在与您分享精彩的内容，赶快来…";
        } else {
            this.desc = desc;
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
