package com.yanhua.base.model;

import java.io.Serializable;
import java.util.List;

/**
 * 列表公共结果返回
 */
public class ListResult<T> implements Serializable {

    /**
     * records : []
     * total : 0
     * size : 10
     * current : 1
     * orders : []
     * searchCount : true
     * pages : 0
     */

    private int total;
    private int size;
    private int current;
    private boolean searchCount;
    private int pages;
    private List<T> records;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
}
