package com.yanhua.base.model;

import java.io.Serializable;


public class HttpResult<T> implements Serializable {

    private String code;

    private T data;

    private String mesg;

    private String time;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMesg() {
        return mesg;
    }

    public void setMesg(String mesg) {
        this.mesg = mesg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "HttpResult{" + "code=" + code + ", mesg='" + mesg + '\'' + ", data=" + data + '}';
    }
}
