/*
 * Copyright (C) 2015 Drakeet <drakeet.me@gmail.com>
 *
 * This file is part of Meizhi
 *
 * Meizhi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Meizhi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Meizhi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.yanhua.base.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;


import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.yanhua.base.R;
import com.yanhua.base.model.ShareImageUrlBody;
import com.yanhua.base.model.ShareWebUrlBody;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.core.util.BitmapUtil;
import com.yanhua.core.util.SystemShareUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.android.api.PlatActionListener;
import cn.jiguang.share.android.api.Platform;
import cn.jiguang.share.android.api.ShareParams;
import cn.jiguang.share.qqmodel.QQ;
import cn.jiguang.share.qqmodel.QZone;
import cn.jiguang.share.wechat.Wechat;
import cn.jiguang.share.wechat.WechatMoments;
import cn.jiguang.share.weibo.SinaWeibo;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SharesUtils {

    public static void share(Context context, int stringRes) {
        share(context, context.getString(stringRes));
    }


    public static void shareImage(Context context, Uri uri, String title) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/jpeg");
        context.startActivity(Intent.createChooser(shareIntent, title));
    }


    public static void share(Context context, String extraText) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "分享");
        intent.putExtra(Intent.EXTRA_TEXT, extraText);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(
                Intent.createChooser(intent, "分享"));
    }

    /**
     * 检测是否安装微信
     *
     * @param context
     * @return
     */
    public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 检测是否安装QQ
     *
     * @param context
     * @return
     */
    public static boolean isQQClientAvailable(Context context) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mobileqq")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 检测是否安装weibo
     *
     * @param context
     * @return
     */
    public static boolean isSinaClientAvailable(Context context) {
        return AppUtils.isPKGInstall(context, "com.sina.weibo");
    }

    /**
     * @param activity
     * @param body     分享内容
     */
    public static void shareWeb(final Activity activity, ShareWebUrlBody body) {
        if (null == body) {
            return;
        }

        ShareParams shareParams = new ShareParams();

        int pos = body.getPosition();
        String webUrl = body.getWebUrl();

        String title = body.getTitle();
        String description = body.getDesc();
        String imageUrl = body.getImageUrl();

        String platform;
        //根据按钮的位置进行平台区分
        //QQ.Name, QZone.Name, Wechat.Name, WechatMoments.Name,
        //WechatFavorite.Name, SinaWeibo.Name, SinaWeiboMessage.Name, Twitter.Name,
        //Facebook.Name, FbMessenger.Name
        switch (pos) {
            case ShareBoardPopup.BOARD_SHARE_QQ_FRIEND:
                platform = QQ.Name;
                //最长 30 个字符
                if (title.length() > 30) {
                    title = title.substring(0, 30);
                }

                //最长 40 个字符
                if (description.length() > 40) {
                    description = title.substring(0, 40);
                }

                if (!TextUtils.isEmpty(imageUrl)) {
                    shareParams.setImageUrl(imageUrl);
                }

                shareParams.setTitle(title);
                shareParams.setText(description);
                break;
            case ShareBoardPopup.BOARD_SHARE_QQ_ZONE:
                platform = QZone.Name;
                //最长 200 个字符
                if (title.length() > 200) {
                    title = title.substring(0, 200);
                }

                //最长 600 个字符
                if (description.length() > 600) {
                    description = title.substring(0, 600);
                }

                shareParams.setTitle(title);
                shareParams.setText(description);
                break;
            case ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND:
                platform = Wechat.Name;
                //长度不能超过 512，注：Title 和 Text 字段微信和微信收藏必须要有其一
                if (title.length() > 500) {
                    title = title.substring(0, 500);
                }

                //长度不能超过 1K，朋友圈不显示该字段内容，注：Title 和 Text 字段微信和微信收藏必须要有其一
                if (description.length() > 1000) {
                    description = title.substring(0, 1000);
                }

                shareParams.setTitle(title);
                shareParams.setText(description);
                break;
            case ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT:
                platform = WechatMoments.Name;
//                长度不能超过 512，注：Title 和 Text 字段微信和微信收藏必须要有其一
                if (title.length() > 500) {
                    title = title.substring(0, 500);
                }

//                长度不能超过 1K，朋友圈不显示该字段内容，注：Title 和 Text 字段微信和微信收藏必须要有其一
                if (description.length() > 1000) {
                    description = title.substring(0, 1000);
                }
                shareParams.setTitle(title);
                shareParams.setText(description);
                break;
            case ShareBoardPopup.BOARD_SHARE_SINA:
                platform = SinaWeibo.Name;

                //沒有title  长度不能超过 512
                if (title.length() > 500) {
                    title = title.substring(0, 500);
                }

                // 不超过 1999 字符
                if (description.length() > 1800) {
                    description = title.substring(0, 1800);
                }

                shareParams.setTitle(title);
                shareParams.setText(description);
                break;

            case ShareBoardPopup.BOARD_SHARE_SYSTEM:
                SystemShareUtil.shareText(activity, webUrl, title);
                return;
            default:
                throw new IllegalStateException("Unexpected value: " + pos);
        }

        if ((pos == ShareBoardPopup.BOARD_SHARE_QQ_ZONE || pos == ShareBoardPopup.BOARD_SHARE_QQ_FRIEND) && !isQQClientAvailable(activity)) {
            ToastUtils.showShort(activity.getString(R.string.qq_unavailable));
            return;
        }

        if ((pos == ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND || pos == ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT) && !isWeixinAvilible(activity)) {
            ToastUtils.showShort(activity.getString(R.string.wechat_unavailable));
            return;
        }

        if (pos == ShareBoardPopup.BOARD_SHARE_SINA && !isSinaClientAvailable(activity)) {
            ToastUtils.showShort(activity.getString(R.string.weibo_unavailable));
            return;
        }
        shareParams.setShareType(Platform.SHARE_WEBPAGE);
        shareParams.setUrl(webUrl);

        if (!TextUtils.isEmpty(imageUrl)) {
            if (pos == ShareBoardPopup.BOARD_SHARE_QQ_FRIEND) {
                //这里有可能还需要补充分享的统计数量
                JShareInterface.share(platform, shareParams, new SharePlatActionListener(null));

                return;
            }
            Observable.create((ObservableOnSubscribe<File>) e -> {
                //通过gilde下载得到file文件,这里需要注意android.permission.INTERNET权限
                e.onNext(Glide.with(activity)
                        .load(imageUrl)
                        .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .get());
                e.onComplete();
            }).subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.newThread())
                    .subscribe(new Consumer<File>() {
                        @Override
                        public void accept(File file) throws Exception {
                            //获取到下载得到的图片，进行本地保存
                            File pictureFolder = Environment.getExternalStorageDirectory();

                            //第二个参数为你想要保存的目录名称
                            File appDir = new File(pictureFolder, "disco_cache");
                            if (!appDir.exists()) {
                                appDir.mkdirs();
                            }
                            String fileName = System.currentTimeMillis() + ".jpg";
                            File destFile = new File(appDir, fileName);
                            //把gilde下载得到图片复制到定义好的目录中去
                            FileInputStream fileInputStream = null;
                            FileOutputStream fileOutputStream = null;
                            try {
                                fileInputStream = new FileInputStream(file);
                                fileOutputStream = new FileOutputStream(destFile);
                                byte[] buffer = new byte[1024];
                                while (fileInputStream.read(buffer) > 0) {
                                    fileOutputStream.write(buffer);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    fileInputStream.close();
                                    fileOutputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            shareParams.setImagePath(destFile.getAbsolutePath());

                            //这里有可能还需要补充分享的统计数量
                            JShareInterface.share(platform, shareParams, new SharePlatActionListener(destFile));
                        }
                    });
        } else {
            //这里有可能还需要补充分享的统计数量
            JShareInterface.share(platform, shareParams, new SharePlatActionListener(null));
        }
    }

    /**
     * @param activity
     * @param body     分享内容
     */
    public static void shareImage(final Activity activity, ShareImageUrlBody body) {
        if (null == body) {
            return;
        }

        ShareParams shareParams = new ShareParams();

        int pos = body.getPosition();
        String webUrl = body.getWebUrl();

        String title = body.getTitle();
        String description = body.getDesc();

        String platform;
        //根据按钮的位置进行平台区分
        //QQ.Name, QZone.Name, Wechat.Name, WechatMoments.Name,
        //WechatFavorite.Name, SinaWeibo.Name, SinaWeiboMessage.Name, Twitter.Name,
        //Facebook.Name, FbMessenger.Name
        switch (pos) {
            case ShareBoardPopup.BOARD_SHARE_QQ_FRIEND:
                platform = QQ.Name;
                break;
            case ShareBoardPopup.BOARD_SHARE_QQ_ZONE:
                platform = QZone.Name;
                break;
            case ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND:
                platform = Wechat.Name;
                break;
            case ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT:
                platform = WechatMoments.Name;
                break;
            case ShareBoardPopup.BOARD_SHARE_SINA:
                platform = SinaWeibo.Name;
                break;
            case ShareBoardPopup.BOARD_SHARE_SYSTEM:
                SystemShareUtil.shareText(activity, webUrl, title);
                return;
            default:
                throw new IllegalStateException("Unexpected value: " + pos);
        }

        if ((pos == ShareBoardPopup.BOARD_SHARE_QQ_ZONE || pos == ShareBoardPopup.BOARD_SHARE_QQ_FRIEND) && !isQQClientAvailable(activity)) {
            ToastUtils.showShort(activity.getString(R.string.qq_unavailable));
            return;
        }

        if ((pos == ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND || pos == ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT) && !isWeixinAvilible(activity)) {
            ToastUtils.showShort(activity.getString(R.string.wechat_unavailable));
            return;
        }

        if (pos == ShareBoardPopup.BOARD_SHARE_SINA && !isSinaClientAvailable(activity)) {
            ToastUtils.showShort(activity.getString(R.string.weibo_unavailable));
            return;
        }

/*        if (pos == ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT || pos == ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND) {
            shareParams.setUrl(webUrl);

            shareParams.setTitle("新社交元宇宙，打造Z时代神器");
            shareParams.setText("您的好友正在与您分享精彩的内容，赶快来下载吧");
//            shareParams.setShareType(Platform.SHARE_WEBPAGE);
        } else {
//            shareParams.setShareType(Platform.SHARE_IMAGE);
        }*/

        shareParams.setShareType(Platform.SHARE_IMAGE);

        Bitmap shareBitmap = body.getShareBitmap();
        if (null != shareBitmap) {
            if (pos == ShareBoardPopup.BOARD_SHARE_SINA) {
                shareParams.setImageData(shareBitmap);

                //这里有可能还需要补充分享的统计数量
                JShareInterface.share(platform, shareParams, new SharePlatActionListener(null));
                return;
            }

            Observable.create((ObservableOnSubscribe<File>) e -> {
                e.onNext(BitmapUtil.bitmapToFile(shareBitmap, "shareCode"));
                e.onComplete();
            }).subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.newThread())
                    .subscribe(file -> {
                        //QQ weixin 截图分享
                        String filePath = file.getAbsolutePath();

                        shareParams.setImagePath(filePath);
                        JShareInterface.share(platform, shareParams, new SharePlatActionListener(file));
                    });
        } else {
            //这里有可能还需要补充分享的统计数量
            JShareInterface.share(platform, shareParams, new SharePlatActionListener(null));
        }
    }

    /**
     * 复制文件
     *
     * @param source 输入文件
     * @param target 输出文件
     */
    public void copy(File source, File target) {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            while (fileInputStream.read(buffer) > 0) {
                fileOutputStream.write(buffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static class SharePlatActionListener implements PlatActionListener {

        private File destFile;

        public SharePlatActionListener(File file) {
            destFile = file;
        }

        @Override
        public void onComplete(Platform platform, int action, HashMap<String, Object> data) {
            ToastUtils.showShort("分享成功");

            if (null != destFile && destFile.exists() && destFile.isFile()) {
                destFile.delete();
            }
        }

        @Override
        public void onError(Platform platform, int action, int errorCode, Throwable error) {
            ToastUtils.showShort("分享失败" + (error != null ? error.getMessage() : "") + "---" + errorCode);

            if (null != destFile && destFile.exists() && destFile.isFile()) {
                destFile.delete();
            }
        }

        @Override
        public void onCancel(Platform platform, int action) {
            ToastUtils.showShort("分享取消");

            if (null != destFile && destFile.exists() && destFile.isFile()) {
                destFile.delete();
            }
        }
    }
}
