package com.yanhua.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public interface IBusinessView {

    void initData(@Nullable Bundle bundle);

    int bindLayout();

    void setContentView();

    void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView);

    void doBusiness();

    void onDebouncingClick(@NonNull View view);
}
