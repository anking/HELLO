package com.yanhua.base.mvp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.LifecycleObserver;

import com.blankj.swipepanel.SwipePanel;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.BuildConfig;
import com.yanhua.base.R;
import com.yanhua.base.activity.CommonActivity;
import com.yanhua.base.dialog.CommonDialogLoading;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.net.HttpCode;
import com.yanhua.core.mmkv.MMKVUtil;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.rong.imkit.utils.PermissionCheckUtil;
import io.rong.imkit.utils.language.RongConfigurationManager;

public abstract class BaseMvpActivity<T extends MvpPresenter> extends CommonActivity implements BaseView {




    private CommonDialogLoading mDialogLoading;
    protected T basePresenter;

    SmartRefreshLayout refreshLayout;
    protected int size = 20;
    protected int current = 1;

    protected abstract void creatPresent();

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        if (null != basePresenter) {
            if (basePresenter instanceof MvpPresenter) {
                getLifecycle().addObserver((LifecycleObserver) basePresenter);
            }
            basePresenter.attachView(this);
        }

        findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(R.color.lightGrayDark));

        initSwipeBack();
    }

    @Override
    public int bindLayout() {
        return View.NO_ID;
    }

    @Override
    public void setContentView() {
        if (bindLayout() <= 0) return;
        mContentView = LayoutInflater.from(this).inflate(bindLayout(), null);
        setContentView(mContentView);
    }

    private void initSwipeBack() {
        if (isSwipeBack()) {
            final SwipePanel swipeLayout = new SwipePanel(this);
            swipeLayout.setLeftDrawable(R.drawable.common_back);
            swipeLayout.setLeftEdgeSize(SizeUtils.dp2px(16));
            swipeLayout.setLeftSwipeColor(getResources().getColor(R.color.colorPrimary));
            swipeLayout.wrapView(findViewById(android.R.id.content));
            swipeLayout.setOnFullSwipeListener(direction -> {
                swipeLayout.close(direction);
                finish();
            });
        }
    }


    public RelativeLayout mTitleContainer;
    public TextView mLeftBack, tvTitle;


    /**
     * 设置状态栏的背景颜色
     *
     * @return
     */
    public int getStatusColor() {
        return R.color.transparent;
    }

    /**
     * 没事不要重写
     *
     * @return
     */
    public boolean getIsPadding() {
        return false;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        creatPresent();
        UltimateBarX.statusBarOnly(this)
                .fitWindow(false)
                .color(getResources().getColor(getStatusColor()))
                .light(true)
                .lvlColorRes(R.color.alphaBlack)
                .apply();

        if (getIsPadding()) {
            UltimateBarX.addStatusBarTopPadding(mContentView);
            UltimateBarX.addNavigationBarBottomPadding(mContentView);
        }

        int titleContain = getResources().getIdentifier("titleContain", "id", getPackageName());
        int tv_header = getResources().getIdentifier("tv_header", "id", getPackageName());
        int tv_left = getResources().getIdentifier("tv_left", "id", getPackageName());

        if (titleContain > 0) {
            mTitleContainer = findViewById(titleContain);
        }
        if (tv_header > 0) {
            tvTitle = findViewById(tv_header);
        }
        if (tv_left > 0) {
            mLeftBack = findViewById(tv_left);
        }
        if (mLeftBack != null) {
            mLeftBack.setOnClickListener(v -> onBackPressed());
        }

        int refreshLayoutID = getResources().getIdentifier("refreshLayout", "id", getPackageName());
        if (refreshLayoutID > 0) {
            refreshLayout = findViewById(refreshLayoutID);
        }
    }

    /**
     * @param colorRes    可以设置为为0或者-1
     * @param drawableRes
     */
    public void setTitleContainBg(int colorRes, int drawableRes) {
        if (null == mTitleContainer) {
            return;
        }

        if (colorRes > 0) {
            mTitleContainer.setBackgroundColor(getResources().getColor(colorRes));
            return;
        }

        if (drawableRes > 0) {
            mTitleContainer.setBackgroundResource(drawableRes);
        }
    }


    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        if (null == tvTitle) {
            return;
        }

        tvTitle.setText(title);
    }

    @Override
    public void doBusiness() {
    }

    @Override
    public void onDebouncingClick(@NonNull View view) {
    }

    @Override
    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(Runnable listener) {
        if (mDialogLoading != null) {
            return;
        }
        mDialogLoading = new CommonDialogLoading().init(this, listener);
        mDialogLoading.show();
    }

    public void hideLoading() {
        if (mDialogLoading != null) {
            mDialogLoading.dismiss();
            mDialogLoading = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (basePresenter != null) {
            basePresenter.detachView();
        }

        basePresenter = null;
    }

    @Override
    public void handSystemError(String action, String mesg) {
        switch (action) {
            case HttpCode.TIME_OUT:
                ToastUtils.showShort("请求超时~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.NET_ERROR:
                ToastUtils.showShort("网络异常~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.TOKEN_INVALID:
                MMKVUtil.getInstance().setLogin(false);
                MMKVUtil.getInstance().setLoginToken("");

                ToastUtils.showShort("登录过期，请重新登录~", Toast.LENGTH_SHORT);

                //跳转到登陆
                onReLogin();
                break;

            case HttpCode.SYSTEM_500:
            case HttpCode.SYSTEM_ERROR:
                ToastUtils.showShort(mesg, Toast.LENGTH_SHORT);
                break;
            case HttpCode.SERVER_ERROR:
                ToastUtils.showShort("系统升级中~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.LOGIN_EXPIRATION:
                //重新刷新token
                break;
            default:
                if(BuildConfig.DEBUG) {
                    ToastUtils.showShort("服务异常~" + mesg, Toast.LENGTH_SHORT);
                }
                break;
        }
        finishRefreshLayout();
    }

    private void finishRefreshLayout() {
        if (null != refreshLayout) {
            refreshLayout.finishRefresh();
            refreshLayout.finishLoadMore();
        }
    }

    //保留以前操作方式，避免浪费没必要的时间
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
    }

    @Override
    public void onReLogin() {
        MMKVUtil.getInstance().setLogin(false);
        MMKVUtil.getInstance().setLoginToken("");

        try {
            Class cls = Class.forName("com.yanhua.user.view.LoginActivity");


            Intent intent = new Intent(mContext, cls);

            this.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 检测是否安装支付宝
     *
     * @param context
     * @return
     */
    public static boolean isAliPayInstalled(Context context) {
        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }

    /**
     * 检测是否安装微信
     *
     * @param context
     * @return
     */
    public boolean isWeixinAvilible(Context context) {
        return AppUtils.isPKGInstall(this, "com.tencent.mm");
    }

    /**
     * 检测是否安装QQ
     *
     * @param context
     * @return
     */
    public boolean isQQClientAvailable(Context context) {
        return AppUtils.isPKGInstall(this, "com.tencent.mobileqq");
    }

    /**
     * 检测是否安装weibo
     *
     * @param context
     * @return
     */
    public boolean isSinaClientAvailable(Context context) {
        return AppUtils.isPKGInstall(this, "com.sina.weibo");
    }

    @Override
    public void handleErrorMessage(String message) {
        ToastUtils.showShort(message);

        finishRefreshLayout();
    }

    @Override
    protected void onPause() {
        super.onPause();

        KeyboardUtils.hideSoftInput(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Context context = RongConfigurationManager.getInstance().getConfigurationContext(newBase);
        super.attachBaseContext(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (PermissionCheckUtil.checkPermissionResultIncompatible(permissions, grantResults)) {
            ToastUtils.showShort(getString(R.string.rc_permission_request_failed));
            return;
        }

        if (!PermissionCheckUtil.checkPermissions(this, permissions)) {
            PermissionCheckUtil.showRequestPermissionFailedAlter(this, permissions, grantResults);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
