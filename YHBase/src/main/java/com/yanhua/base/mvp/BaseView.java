package com.yanhua.base.mvp;


public interface BaseView<T>  {

    /**
     * 显示加载中
     */
    void showLoading();

    /**
     * 隐藏加载
     */
    void hideLoading();



    /**
     * 数据获取失败
     * 网络错误
     * 处理040000和-1这种异常情况
     * @param action
     */
    void handSystemError(String action,String mesg);
    /**
     * 数据获取失败
     * @param message
     */
    void handleErrorMessage(String message);

    default void onReLogin(){}

    default void handActionSuccess(int type){}

}
