package com.yanhua.base.mvp;

public interface BasePresenter<T extends BaseView> {

    void attachView(T baseView);

    void detachView();
}

