package com.yanhua.base.mvp;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.core.BottomPopupView;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.BuildConfig;
import com.yanhua.base.net.HttpCode;
import com.yanhua.core.mmkv.MMKVUtil;


public abstract class BaseMvpBottomPopupView<T extends BasePresenter> extends BottomPopupView implements BaseView {

    protected T basePresenter;

    SmartRefreshLayout refreshLayout;
    protected abstract void creatPresent();

    private Context mContext;

    protected int size = 20;
    protected int current = 1;
    protected String className;

    public BaseMvpBottomPopupView(@NonNull Context context) {
        super(context);
        this.mContext = context;

        className = this.getClass().getSimpleName();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    private void finishRefreshLayout(){
        if (null != refreshLayout) {
            refreshLayout.finishRefresh();
            refreshLayout.finishLoadMore();
        }
    }
    @Override
    public void handleErrorMessage(String message) {
        ToastUtils.showShort(message);

        finishRefreshLayout();
    }


    @Override
    public void handSystemError(String action,String mesg) {
        switch (action) {
            case HttpCode.TIME_OUT:
                ToastUtils.showShort("请求超时~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.NET_ERROR:
                ToastUtils.showShort("网络异常~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.TOKEN_INVALID:
                MMKVUtil.getInstance().setLogin(false);
                MMKVUtil.getInstance().setLoginToken("");

                ToastUtils.showShort("登录过期，请重新登录~", Toast.LENGTH_SHORT);

                //跳转到登陆
                onReLogin();
                break;

            case HttpCode.SYSTEM_500:
            case HttpCode.SYSTEM_ERROR:
                ToastUtils.showShort(mesg, Toast.LENGTH_SHORT);
                break;
            case HttpCode.LOGIN_EXPIRATION:
                //重新刷新token
            case HttpCode.SERVER_ERROR:
                ToastUtils.showShort("系统升级中~", Toast.LENGTH_SHORT);
                break;
            default:
                if(BuildConfig.DEBUG) {
                    ToastUtils.showShort("服务异常~" + mesg, Toast.LENGTH_SHORT);
                }
                break;
        }

        finishRefreshLayout();
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        creatPresent();
        if (null != basePresenter) {
            basePresenter.attachView(this);
        }

        int refreshLayoutID = getResources().getIdentifier("refreshLayout", "id", mContext.getPackageName());
        if (refreshLayoutID > 0) {
            refreshLayout = findViewById(refreshLayoutID);
        }
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (basePresenter != null) {
            basePresenter.detachView();
        }

        basePresenter = null;
    }


    @Override
    public void onReLogin() {
        MMKVUtil.getInstance().setLogin(false);
        MMKVUtil.getInstance().setLoginToken("");

        try {
            Class cls = Class.forName("com.yanhua.user.view.LoginActivity");
            Intent intent = new Intent(mContext, cls);

            mContext.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
