package com.yanhua.base.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleObserver;

import com.blankj.utilcode.util.ToastUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.BuildConfig;
import com.yanhua.base.R;
import com.yanhua.base.activity.CommonFragment;
import com.yanhua.base.dialog.CommonDialogLoading;
import com.yanhua.base.net.HttpCode;
import com.yanhua.core.mmkv.MMKVUtil;

import org.jetbrains.annotations.NotNull;


public abstract class BaseMvpFragment<T extends MvpPresenter> extends CommonFragment implements BaseView {

    protected int size = 20;
    protected int current = 1;

    SmartRefreshLayout refreshLayout;
    private CommonDialogLoading mDialogLoading;
    protected T basePresenter;

    protected abstract void creatPresent();

    @CallSuper
    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
    }

    /**
     * 设置状态栏的背景颜色
     *
     * @return
     */
    public int getStatusColor() {
        return R.color.transparent;
    }

    /**
     * 没事不要重写
     *
     * @return
     */
    public boolean getIsPadding() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        creatPresent();
        if (basePresenter instanceof MvpPresenter) {
            getLifecycle().addObserver((LifecycleObserver) basePresenter);
        }
        if (null != basePresenter) {
            basePresenter.attachView(this);
        }
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int refreshLayoutID = getResources().getIdentifier("refreshLayout", "id", mContext.getPackageName());
        if (refreshLayoutID > 0) {
            refreshLayout = findViewById(refreshLayoutID);
        }
    }

    @CallSuper
    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (basePresenter != null) {
            basePresenter.detachView();
            basePresenter = null;
        }
    }

    @Override
    public void doBusiness() {
    }

    @Override
    public void onDebouncingClick(@NonNull View view) {
    }

    @Override
    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(Runnable listener) {
        if (mDialogLoading != null) {
            return;
        }
        mDialogLoading = new CommonDialogLoading().init(mContext, listener);
        mDialogLoading.show();
    }

    public void hideLoading() {
        if (mDialogLoading != null) {
            mDialogLoading.dismiss();
            mDialogLoading = null;
        }
    }


    @Override
    public void handSystemError(String action, String mesg) {
        switch (action) {
            case HttpCode.TIME_OUT:
                ToastUtils.showShort("请求超时~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.NET_ERROR:
                ToastUtils.showShort("网络异常~", Toast.LENGTH_SHORT);
                break;
            case HttpCode.TOKEN_INVALID:
                MMKVUtil.getInstance().setLogin(false);
                MMKVUtil.getInstance().setLoginToken("");

                ToastUtils.showShort("登录过期，请重新登录~", Toast.LENGTH_SHORT);

                //跳转到登陆
                onReLogin();
                break;

            case HttpCode.SYSTEM_500:
            case HttpCode.SYSTEM_ERROR:
                ToastUtils.showShort(mesg, Toast.LENGTH_SHORT);
                break;
            case HttpCode.LOGIN_EXPIRATION:
                //重新刷新token
            case HttpCode.SERVER_ERROR:
                ToastUtils.showShort("系统升级中~", Toast.LENGTH_SHORT);
                break;
            default:
                if(BuildConfig.DEBUG) {
                    ToastUtils.showShort("服务异常~" + mesg, Toast.LENGTH_SHORT);
                }
                break;
        }
        finishRefreshLayout();
    }

    private void finishRefreshLayout() {
        if (null != refreshLayout) {
            refreshLayout.finishRefresh();
            refreshLayout.finishLoadMore();
        }
    }

    @Override
    public void handleErrorMessage(String message) {
        ToastUtils.showShort(message);
        finishRefreshLayout();
    }


    @Override
    public void onReLogin() {
        MMKVUtil.getInstance().setLogin(false);
        MMKVUtil.getInstance().setLoginToken("");
        try {
            Class cls = Class.forName("com.yanhua.user.view.LoginActivity");
            Intent intent = new Intent(mContext, cls);

            this.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
