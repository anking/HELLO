package com.yanhua.base.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yanhua.base.BaseFragment;
import com.yanhua.base.dialog.CommonDialogLoading;
import com.yanhua.core.mmkv.MMKVUtil;

public abstract class CommonFragment extends BaseFragment {

    private CommonDialogLoading mDialogLoading;

    protected MMKVUtil mMMKV;

    protected String className;

    @CallSuper
    @Override
    public void initData(@Nullable Bundle bundle) {
        mMMKV = MMKVUtil.getInstance();
    }

    @Override
    public int bindLayout() {
        return View.NO_ID;
    }

    @Override
    public void setContentView() {
        if (bindLayout() <= 0) return;
        mContentView = LayoutInflater.from(mContext).inflate(bindLayout(), null);

        className = this.getClass().getSimpleName();
    }


    @CallSuper
    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
    }

    @Override
    public void doBusiness() {
    }

    @Override
    public void onDebouncingClick(@NonNull View view) {
    }

    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(Runnable listener) {
        if (mDialogLoading != null) {
            return;
        }
        mDialogLoading = new CommonDialogLoading().init(mContext, listener);
        mDialogLoading.show();
    }

    public void dismissLoading() {
        if (mDialogLoading != null) {
            mDialogLoading.dismiss();
            mDialogLoading = null;
        }
    }

}
