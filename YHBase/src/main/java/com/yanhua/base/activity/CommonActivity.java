package com.yanhua.base.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.blankj.swipepanel.SwipePanel;
import com.blankj.utilcode.util.SizeUtils;
import com.yanhua.base.BaseActivity;
import com.yanhua.base.R;
import com.yanhua.base.dialog.CommonDialogLoading;
import com.yanhua.core.mmkv.MMKVUtil;

/**
 * 作为一个参考基类
 */
public abstract class CommonActivity extends BaseActivity {

    private CommonDialogLoading mDialogLoading;

    protected MMKVUtil mMMKV;


    /**
     * setContentView
     * 控制是否可以滑动返回
     *
     * @return
     */
    public boolean isSwipeBack() {
        return false;
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @CallSuper
    @Override
    public void initData(@Nullable Bundle bundle) {
        mMMKV = MMKVUtil.getInstance();

        findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(R.color.lightGrayDark));
        initSwipeBack();

        className = TAG;

        Log.e("--TAG--", "=======onCreate=======" + TAG);
    }

    @Override
    public int bindLayout() {
        return View.NO_ID;
    }

    @Override
    public void setContentView() {
        if (bindLayout() <= 0) return;
        mContentView = LayoutInflater.from(this).inflate(bindLayout(), null);
        setContentView(mContentView);
    }

    private void initSwipeBack() {
        if (isSwipeBack()) {
            final SwipePanel swipeLayout = new SwipePanel(this);
            swipeLayout.setLeftDrawable(R.drawable.common_back);
            swipeLayout.setLeftEdgeSize(SizeUtils.dp2px(16));
            swipeLayout.setLeftSwipeColor(getResources().getColor(R.color.colorPrimary));
            swipeLayout.wrapView(findViewById(android.R.id.content));
            swipeLayout.setOnFullSwipeListener(direction -> {
                swipeLayout.close(direction);
                finish();
            });
        }
    }

    @CallSuper
    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
    }

    @Override
    public void doBusiness() {
    }

    @Override
    public void onDebouncingClick(@NonNull View view) {
    }

    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(Runnable listener) {
        if (mDialogLoading != null) {
            return;
        }
        mDialogLoading = new CommonDialogLoading().init(this, listener);
        mDialogLoading.show();
    }

    public void dismissLoading() {
        if (mDialogLoading != null) {
            mDialogLoading.dismiss();
            mDialogLoading = null;
        }
    }
}