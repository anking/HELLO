package com.yanhua.disco;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.camera2.Camera2Config;
import androidx.camera.core.CameraXConfig;

import com.yanhua.base.BaseApplication;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.utils.AppStatusManager;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.utils.AppPlatformUtil;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.mmkv.MMKVUtil;

import org.greenrobot.eventbus.EventBus;


public class DiscoApplication extends BaseApplication implements CameraXConfig.Provider {

    private int mFinalCount;
    protected MMKVUtil mMMKV;

    @Override
    public void onCreate() {
        super.onCreate();

        AppStatusManager.init(this,"com.yanhua.user.view.SplashActivity ");

        //UserManager init----
        UserManager.init(this);
        //DiscoCache init ------
        DiscoCacheUtils.init(this);

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {
                mFinalCount++;
                if (mFinalCount == 1) {
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.COME_FOREGROUND));
                }
            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {

            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {
                mFinalCount--;
                if (mFinalCount == 0) {
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.COME_BACKGROUND));
                }
            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });

        mMMKV = MMKVUtil.getInstance();
        boolean isShownPrivateProtocol = mMMKV.isShowPrivateProtocol();

        String currnetProcessName = getCurrnetProcessName();
        //用户已接受隐私协议，进行初始化
        if (isShownPrivateProtocol && getApplicationInfo().packageName.equals(currnetProcessName)) {
            AppPlatformUtil.initPlatformSDK(this);
        }
    }


    @NonNull
    @Override
    public CameraXConfig getCameraXConfig() {
        return Camera2Config.defaultConfig();
    }

}
