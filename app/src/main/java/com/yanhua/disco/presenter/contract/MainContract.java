package com.yanhua.disco.presenter.contract;

import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.RongUserInfo;
import com.yanhua.common.model.UpgradeInfoModel;

public interface MainContract {

    interface IView extends BaseView {

        default void handleRongCloudInit(String rongCloudToken) {
        }

        void handleSuccessAppVersionInfo(UpgradeInfoModel data);

        void handleRongCloudUserInfo(RongUserInfo data);
    }

    interface Presenter extends BasePresenter<IView> {
    }
}
