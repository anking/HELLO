package com.yanhua.disco.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.model.RongUserInfo;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.disco.presenter.contract.MainContract;
import com.yanhua.user.api.UserHttpMethod;

import java.util.HashMap;

public class MainPresenter extends MvpPresenter<MainContract.IView> implements MainContract.Presenter {

    public void getRongIMUserInfo(String uid) {
        addSubscribe(MessageHttpMethod.getInstance().getRongIMUserInfo(new SimpleSubscriber<HttpResult<RongUserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<RongUserInfo> bean) {
                super.onNext(bean);
                if (bean != null) {
                    if (bean.getCode().equals(HttpCode.SUCCESS)) {
                        RongUserInfo userInfo = bean.getData();
                        userInfo.setUid(uid);

                        baseView.handleRongCloudUserInfo(userInfo);
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, uid));
    }


    public void fetchAppVersionInfo() {
        addSubscribe(UserHttpMethod.getInstance().fetchAppVersionInfo(new SimpleSubscriber<HttpResult<UpgradeInfoModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<UpgradeInfoModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessAppVersionInfo(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, 0));
    }


    public void visitRecord(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().visitRecord(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
            }
        }, params));
    }

    public void getRongCloudToken() {
        addSubscribe(MessageHttpMethod.getInstance().getRongCloudToken(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                if (bean != null) {
                    if (bean.getCode().equals(HttpCode.SUCCESS)) {
                        if (bean.getData() instanceof String) {
                            baseView.handleRongCloudInit((String) bean.getData());
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }));
    }

}
