package com.yanhua.disco.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.MapsInitializer;
import com.amap.api.services.core.ServiceSettings;
import com.blankj.utilcode.util.LogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.github.dfqin.grantor.PermissionListener;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.utils.AppStatusConstant;
import com.yanhua.base.utils.AppStatusManager;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.RongUserInfo;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UpdateShowDialog;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.NoScrollViewPager;
import com.yanhua.common.widget.navigation.OneBottomNavigationBar;
import com.yanhua.core.mmkv.MMKVUtil;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.OSUtils;
import com.yanhua.core.util.SystemUtil;
import com.yanhua.disco.R;
import com.yanhua.disco.R2;
import com.yanhua.disco.presenter.MainPresenter;
import com.yanhua.disco.presenter.contract.MainContract;
import com.yanhua.home.fragment.HomeFragment;
import com.yanhua.message.activity.DiscoMentionMemberSelectActivity;
import com.yanhua.message.activity.YHDiscoConversationActivity;
import com.yanhua.message.fragment.YHDiscoConversationListFragment;
import com.yanhua.mine.fragment.MineFragment;
import com.yanhua.moment.fragment.MomentFragment;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import io.rong.imkit.GlideKitImageEngine;
import io.rong.imkit.IMCenter;
import io.rong.imkit.RongIM;
import io.rong.imkit.config.DataProcessor;
import io.rong.imkit.config.RongConfigCenter;
import io.rong.imkit.conversation.extension.RongExtensionManager;
import io.rong.imkit.feature.mention.IExtensionEventWatcher;
import io.rong.imkit.feature.mention.RongMentionManager;
import io.rong.imkit.userinfo.RongUserInfoManager;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.MentionedInfo;
import io.rong.imlib.model.Message;

@Route(path = ARouterPath.MAIN_ACTIVITY)
public class MainActivity extends BaseMvpActivity<MainPresenter> implements MainContract.IView {

    @BindView(R2.id.bottomTab)
    OneBottomNavigationBar oneBottomLayout;

    @BindView(R2.id.viewPager)
    NoScrollViewPager viewPager;
    List<Fragment> listFragment = new ArrayList<>();

    private boolean isConnected = false;
    private int reConnectCount = 0;
    private NetworkChange networkChange;


    @Override
    protected boolean isCheckAppStatus() {
        return true;
    }

    private void setAppStatus() {
        //app状态改为正常
        AppStatusManager.getInstance().setAppStatus(AppStatusConstant.STATUS_NORMAL);
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setAppStatus();

        //判断是否同意隐私协议，uminit为1时为已经同意，直接初始化umsdk
        if (MMKVUtil.getInstance().isShowPrivateProtocol()) {
            // 表示用户同意定位功能隐私协议
            AMapLocationClient.updatePrivacyShow(this, true, true);
            AMapLocationClient.updatePrivacyAgree(this, true);

            // 表示用户同意地图功能隐私协议
            MapsInitializer.updatePrivacyShow(this, true, true);
            MapsInitializer.updatePrivacyAgree(this, true);

            // 表示用户同意搜索功能隐私协议
            ServiceSettings.updatePrivacyShow(this, true, true);
            ServiceSettings.updatePrivacyAgree(this, true);
        }

        checkPermission();

        initOneBottom();

        clearBadgeStatu();
    }

    // 清除华为的角标
    private void clearBadgeStatu() {
        if (Build.MANUFACTURER.equalsIgnoreCase("HUAWEI")) {
            try {
                String packageName = getPackageName();
                String launchClassName =
                        getPackageManager()
                                .getLaunchIntentForPackage(packageName)
                                .getComponent()
                                .getClassName();
                Bundle bundle = new Bundle(); // 需要存储的数据
                bundle.putString("package", packageName); // 包名
                bundle.putString("class", launchClassName); // 启动的Activity完整名称
                bundle.putInt("badgenumber", 0); // 未读信息条数清空
                getContentResolver()
                        .call(
                                Uri.parse("content://com.huawei.android.launcher.settings/badge/"),
                                "change_badge",
                                null,
                                bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPermission() {
        String[] permissionsO = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA};

        requestCheckPermission(permissionsO, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
            }
        });
    }

    private void initOneBottom() {
        Fragment home = new HomeFragment();
        Fragment moment = new MomentFragment();
        Fragment message = new YHDiscoConversationListFragment();
        Fragment mine = new MineFragment();

        listFragment.add(home);
        listFragment.add(moment);
        listFragment.add(message);
        listFragment.add(mine);

        oneBottomLayout.setMenu(R.menu.navigation_menu);
        oneBottomLayout.attachViewPager(getSupportFragmentManager(), viewPager, listFragment);
        oneBottomLayout.setReplace(false);
        oneBottomLayout.setFloatingEnable(true);

        //设置消息数量
//        oneBottomLayout.setItemColorStateList(R.drawable.item_check);
//        oneBottomLayout.setMsgCount(0, 32);
//        oneBottomLayout.setMsgCount(2, 1);
//        oneBottomLayout.setMsgCount(1, -1);

        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);

        oneBottomLayout.setOnItemSelectedListener((item, pos) -> {
            //获取menu
            ArrayList<OneBottomNavigationBar.Item> menus = (ArrayList<OneBottomNavigationBar.Item>) oneBottomLayout.getItemList();
            int size = menus.size();

            if (pos == 2 && !UserManager.getInstance().isLogin() && FastClickUtil.isFastClick(FastClickUtil.MIN_DURATION_2SECOND_TIME)) {
                Log.e(TAG, "firstIsLoginThenJump");

                PageJumpUtil.firstIsLoginThenJump(null);

                int curPos = viewPager.getCurrentItem();

                for (int i = 0; i < size; i++) {
                    OneBottomNavigationBar.Item it = menus.get(i);
                    if (i == pos) {//将当前点击的未选中
                        it.setChecked(false);
                        it.setFloating(false);
                    }

                    if (i == curPos) {
                        it.setChecked(true);
                        it.setFloating(true);
                        viewPager.setCurrentItem(curPos);
                    }
                }

                return;
            }

            for (int i = 0; i < size; i++) {
                OneBottomNavigationBar.Item it = menus.get(i);
                if (i == pos) {
                    it.setChecked(true);
                    it.setFloating(true);
                    viewPager.setCurrentItem(pos);
                } else {
                    it.setChecked(false);
                    it.setFloating(false);
                }
            }
        });

    }


    /**
     * 切换到此刻圈子
     */
    public void toCircle() {
        viewPager.setCurrentItem(1);
        MomentFragment momentFragment = (MomentFragment) listFragment.get(1);

        momentFragment.toIndexPage(4);
    }

    /**
     * 切换到此刻发布
     */
    public void toPublishMoment() {
        PageJumpUtil.firstIsLoginThenJump(new PageJumpUtil.OnPageJumpListener() {
            @Override
            public void onPageListener() {
                MomentFragment momentFragment = (MomentFragment) listFragment.get(1);
                momentFragment.checkPermission();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        viewPager.setCurrentItem(1);
                    }
                }, 500);
            }
        });
    }

    public void onDebouncingClick(View v) {

    }

    @Override
    public void doBusiness() {
        super.doBusiness();

//        basePresenter.fetchAppVersionInfo();

        //网络状态改变的广播
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        networkChange = new NetworkChange();
        registerReceiver(networkChange, intentFilter);

        if (UserManager.getInstance().isLogin()) {
            basePresenter.getRongCloudToken();
        }
    }

    /*NETWORK_UNAVAILABLE	-1	网络不可用
    CONNECTED	0	连接成功
    CONNECTING	1	连接中
    UNCONNECTED	2	未连接状态，即应用没有调用过连接方法
    KICKED_OFFLINE_BY_OTHER_CLIENT	3	用户账号在其它设备登录，此设备被踢下线
    TOKEN_INCORRECT	4	Token 过期时触发此状态
    CONN_USER_BLOCKED	6	用户被开发者后台封禁
    SIGN_OUT	12	用户主动断开连接的状态，见断开连接
    SUSPEND	13	连接暂时挂起（多是由于网络问题导致），SDK 会在合适时机进行自动重连
    TIMEOUT	14	连接超时，SDK 将停止连接，用户需要做超时处理，再自行调用连接接口进行连接*/
    private RongIMClient.ConnectionStatusListener connectionStatusListener = new RongIMClient.ConnectionStatusListener() {
        @Override
        public void onChanged(ConnectionStatus status) {
            //开发者需要根据连接状态码，进行不同业务处理
            if (status == ConnectionStatus.KICKED_OFFLINE_BY_OTHER_CLIENT) {
                EventBus.getDefault().post(new MessageEvent(CommonConstant.KICKED_OFFLINE_BY_OTHER_CLIENT));
            } else if (status.equals(ConnectionStatus.CONN_USER_BLOCKED)) {
                //用户被封禁，请提示用户并做出对应处理
                EventBus.getDefault().post(new MessageEvent(CommonConstant.CONN_USER_BLOCKED));
            } else if (status.equals(ConnectionStatus.TOKEN_INCORRECT)) {
                basePresenter.getRongCloudToken();
            }
        }
    };


    private void initRongIM() {
        RongIM.getInstance().setVoiceMessageType(IMCenter.VoiceMessageType.HighQuality);
        // 设置监听状态
        RongIM.setConnectionStatusListener(connectionStatusListener);

        RouteUtils.registerActivity(RouteUtils.RongActivityType.ConversationListActivity, MainActivity.class);
        RouteUtils.registerActivity(RouteUtils.RongActivityType.ConversationActivity, YHDiscoConversationActivity.class);
        RouteUtils.registerActivity(RouteUtils.RongActivityType.MentionMemberSelectActivity, DiscoMentionMemberSelectActivity.class);


        Conversation.ConversationType[] supportedTypes = {
                Conversation.ConversationType.PRIVATE,
                Conversation.ConversationType.GROUP,
                Conversation.ConversationType.SYSTEM
        };
        RongConfigCenter.conversationListConfig()
                .setDataProcessor(
                        new DataProcessor<Conversation>() {
                            @Override
                            public Conversation.ConversationType[] supportedTypes() {
                                return supportedTypes;
                            }

                            @Override
                            public List<Conversation> filtered(List<Conversation> data) {
                                return data;
                            }

                            @Override
                            public boolean isGathered(Conversation.ConversationType type) {
                                if (type.equals(Conversation.ConversationType.SYSTEM)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        });
        //

        // 初始化用户和群组信息内容提供者
        initInfoProvider();
        sendAllPerson();
        // 设置头像信息
        initRongPortrait();
    }

    private void connectRongCloud(String rongCloudToken) {
        // 初始化融云连接
        RongIM.connect(rongCloudToken, new RongIMClient.ConnectCallback() {
            @Override
            public void onSuccess(String uid) {
                // 连接成功的回调，返回当前连接的用户 ID。
                isConnected = true;
                LogUtils.e("融云IM连接成功=" + uid);
//
//Home
//
//                if (!TextUtils.isEmpty(pushType) && null != pushNotificationMessage) {
//                    Conversation.ConversationType conversationType = Conversation.ConversationType.setValue(pushNotificationMessage.getConversationType().getValue());
//                    String targetId = pushNotificationMessage.getTargetId();
//                    String title = pushNotificationMessage.getTargetUserName();
//
//                    RongIM.getInstance().startConversation(mContext, conversationType, targetId, title, null);
//                }
            }

            @Override
            public void onError(RongIMClient.ConnectionErrorCode errorCode) {
                isConnected = false;

                if (reConnectCount > 3) {
                    return;
                }

                if (errorCode.equals(RongIMClient.ConnectionErrorCode.RC_CONN_TOKEN_EXPIRE)) {
                    //从 APP 服务请求新 token，获取到新 token 后重新 connect()
                    reConnectCount++;
                    //从 APP 服务获取新 token，并重连
                    basePresenter.getRongCloudToken();
                } else if (errorCode.equals(RongIMClient.ConnectionErrorCode.RC_CONNECT_TIMEOUT)) {
                    //连接超时，弹出提示，可以引导用户等待网络正常的时候再次点击进行连接
                } else {
                    //其它业务错误码，请根据相应的错误码作出对应处理。
                }
            }

            @Override
            public void onDatabaseOpened(RongIMClient.DatabaseOpenStatus databaseOpenStatus) {
                if (RongIMClient.DatabaseOpenStatus.DATABASE_OPEN_SUCCESS.equals(databaseOpenStatus)) {
                    //本地数据库打开，跳转到会话列表页面
                } else {
                    //数据库打开失败，可以弹出 toast 提示。
                }
            }
        });
    }

    class NetworkChange extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();//获取网络状态
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                basePresenter.fetchAppVersionInfo();
                if (!isConnected && UserManager.getInstance().isLogin()) {
                    basePresenter.getRongCloudToken();
                }

                if (SystemUtil.isWifi(mContext)) {
                    YXConfig.needShowNoWifi = false;
                    YXConfig.isWIFION = true;
                } else {
                    YXConfig.needShowNoWifi = true;
                    YXConfig.isWIFION = false;
                }
            } else {
                YXConfig.needShowNoWifi = true;
                YXConfig.isWIFION = false;
            }
        }
    }

    private void sendAllPerson() {
        RongExtensionManager.getInstance()
                .addExtensionEventWatcher(
                        new IExtensionEventWatcher() {
                            @Override
                            public void onTextChanged(
                                    Context context,
                                    Conversation.ConversationType type,
                                    String targetId,
                                    int cursorPos,
                                    int count,
                                    String text) {
                            }

                            @Override
                            public void onSendToggleClick(Message message) {
                                if (message != null
                                        && message.getContent() != null
                                        && message.getContent().getMentionedInfo() != null
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        != null
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        .size()
                                        > 0
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        .get(0)
                                        .equals(String.valueOf(-1))) {
                                    message.getContent()
                                            .getMentionedInfo()
                                            .setType(MentionedInfo.MentionedType.ALL);
                                }
                            }

                            @Override
                            public void onDeleteClick(
                                    Conversation.ConversationType type,
                                    String targetId,
                                    EditText editText,
                                    int cursorPos) {
                            }

                            @Override
                            public void onDestroy(
                                    Conversation.ConversationType type, String targetId) {
                            }
                        });
    }

    /**
     * 设置保存用户头像等信息
     */
    public void initInfoProvider() {
        // 单聊用户信息提供者
        RongUserInfoManager.getInstance().setUserInfoProvider(userId -> {
            // 此处应异步调用，从应用后台获取数据
            // 通过userId获取用户信息
            basePresenter.getRongIMUserInfo(userId);//先请求然后再通过下面发的方法更新
            return null;
        }, true);

        // 群聊用户信息提供者---群组信息提供者接口
        RongUserInfoManager.getInstance().setGroupInfoProvider(groupId -> {
            // 此处为异步请求群信息逻辑，需要开发者实现---根据groupId请求获取群信息
//                Group group = new Group(groupId, "group" + groupId, Uri.parse("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp5.itc.cn%2Fq_70%2Fimages03%2F20200816%2F074283744b4b46f3aaeb9d93d2d3fb5f.jpeg&refer=http%3A%2F%2Fp5.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1657867592&t=6698d1d70ee8e953860728a71ff55d27"));
//                RongUserInfoManager.getInstance().refreshGroupInfoCache(group);
            return null;
        }, true);

        // 设置群成员昵称---设置群成员用户信息提供者
        RongUserInfoManager.getInstance().setGroupUserInfoProvider((groupId, userId) -> {
            //basePresenter.queryMember(groupId);
            //异步获取群昵称，需要开发者实现---根据groupId请求，获取群成员
//            GroupUserInfo groupUserInfo = new GroupUserInfo(groupId, userId, "nickname" + userId);
//            RongUserInfoManager.getInstance().refreshGroupUserInfoCache(groupUserInfo);
            return null; //同步返回群成员昵称。
        }, true);

        // 设置群组成员信息---
        RongMentionManager.getInstance().setGroupMembersProvider((groupId, callback) -> {
            //异步获取群昵称，需要开发者实现---根据groupId请求，获取群成员
//            List<GroupMember> data = new ArrayList<>();
//            GroupMember groupMember = new GroupMember(groupId, "-1", "所有人");

//            data.add(groupMember);
            /*HttpQUtils.queryGroup(groupId, new HttpQUtils.QueryGroupCallBack() {
                @Override
                public void onSuccess(List<GroupMemberInfo> groupMemberInfos) {
                    List<UserInfo> userInfoList =
                            new ArrayList<>();
                    userInfoList.add(new UserInfo(
                            "-1",
                            "所有人",
                            null));
                    for (GroupMemberInfo groupMemberInfo : groupMemberInfos) {
                        String name = RongUserInfoManager.getInstance().getGroupUserInfo(groupId, groupMemberInfo.getId()).getNickname();
                        if (TextUtils.isEmpty(name)) {
                            name = "name" + groupMemberInfo.getId();
                        }

                        UserInfo info =
                                new UserInfo(
                                        groupMemberInfo.getId(),
                                        name,
                                        null);
                        userInfoList.add(info);
                    }
                    //此处为应用层获取群组成员逻辑
                    callback.onGetGroupMembersResult(userInfoList); // 调用 callback 的 onGetGroupMembersResult 回传群组信息
                }

                @Override
                public void onFailed(@NonNull String err) {
                    Looper.prepare();
                    ToastUtils.showShort("获取群成员列表 err : " + err);

                    Looper.loop();
                }
            });*/

        });
    }

    /**
     * 融云头像设置圆形
     */
    public void initRongPortrait() {
        RongConfigCenter.featureConfig().setKitImageEngine(new GlideKitImageEngine() {
            @Override
            public void loadConversationPortrait(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView, Message message) {
                Glide.with(imageView).load(url)

                        .apply(RequestOptions.bitmapTransform(new CircleCrop()))

                        .into(imageView);
            }

            @Override
            public void loadConversationListPortrait(@NonNull Context context, @NonNull String url, @NonNull ImageView imageView, Conversation conversation) {
                Glide.with(imageView).load(url)

                        .apply(RequestOptions.bitmapTransform(new CircleCrop()))

                        .into(imageView);
            }
        });
    }

    @Override
    public void handleRongCloudInit(String rongCloudToken) {
        initRongIM();

        connectRongCloud(rongCloudToken);
    }

/*    RongIMClient.ResultCallback<Integer> callback = new RongIMClient.ResultCallback<Integer>() {

        @Override
        public void onSuccess(Integer i) {
        }

        @Override
        public void onError(RongIMClient.ErrorCode errorCode) {

        }
    };*/


    @Override
    protected void creatPresent() {
        basePresenter = new MainPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void handleSuccessAppVersionInfo(UpgradeInfoModel upgradeInfoModel) {
        if (upgradeInfoModel != null) {
            int versionCode = upgradeInfoModel.getVersionNo();
            if (OSUtils.getAppVersion(this) < versionCode) {
                // 本地版本号<服务端配置的版本号，说明有新版本，弹出升级提示框
                UpdateShowDialog.showUpdateDialog(mContext, upgradeInfoModel);
            }
        } else {
            // 没有更新弹框时 判断权限申请弹框
            EventBus.getDefault().post(new MessageEvent(CommonConstant.REQUEST_PERMISSIONS));
        }
    }

    @Override
    public void handleRongCloudUserInfo(RongUserInfo user) {
        String portraitUrl = user.getUserPortrait();
        Uri portraitUri = portraitUrl == null ? null : Uri.parse(portraitUrl);

        io.rong.imlib.model.UserInfo userInfo = new io.rong.imlib.model.UserInfo(user.getUid(), user.getUserName(), portraitUri);
        RongUserInfoManager.getInstance().refreshUserInfoCache(userInfo);
    }

    @Override
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        if (event.getEventName().equals(ARouterPath.RANKING_LIST_ACTIVITY)) {
            toCircle();
        } else if (event.getEventName().equals(CommonConstant.TO_MOMENT_PUBLISH)) {
            toPublishMoment();
        } else if (event.getEventName().equals(CommonConstant.LOGIN_SUCCESS)) {
            // 访问记录
            visitRecord(1);
            basePresenter.getRongCloudToken();
        } else if (event.getEventName().equals(CommonConstant.LOGINED_GET_USERINFO)) {
            // 访问记录
            visitRecord(1);
        } else if (event.getEventName().equals(CommonConstant.LOGIN_OUT)) {
            // 访问记录
            visitRecord(0);
        } else if (event.getEventName().equals(CommonConstant.COME_BACKGROUND)) {
            // 访问记录
            visitRecord(0);
        } else if (event.getEventName().equals(CommonConstant.COME_FOREGROUND)) {
            // 访问记录
            visitRecord(1);
        } else if (event.getEventName().equals(CommonConstant.RECONNECT_IM)) {
            basePresenter.getRongCloudToken();
        }
    }

    private void visitRecord(int type) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("mobileBrand", Build.BRAND);
        params.put("mobileModel", Build.MODEL);
        params.put("mobileSystem", "Android " + Build.VERSION.RELEASE);
        params.put("systemType", 1);
        params.put("type", type);
        if (UserManager.getInstance().isLogin() || type == 0) {
            params.put("userId", UserManager.getInstance().getUserId());

            UserInfo userInfo = UserManager.getInstance().getUserInfo();
            if (null != userInfo) {
                params.put("username", UserManager.getInstance().getUserInfo().getNickName());
            }
        }
        if (!TextUtils.isEmpty(DiscoCacheUtils.getInstance().getLat()) && !TextUtils.isEmpty(DiscoCacheUtils.getInstance().getLng())) {
            params.put("visitCity", DiscoCacheUtils.getInstance().getCurrentCity());
            params.put("visitLatitude", DiscoCacheUtils.getInstance().getLat());
            params.put("visitLongitude", DiscoCacheUtils.getInstance().getLng());
        }
        basePresenter.visitRecord(params);
    }
}