package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class BarConfigModel implements Serializable {

    private int barQueryOpenAudit;
    private int barAskedOpenAudit;
    private int barRiskStatementOpenAudit;
    private String barRiskStatementOpenAuditTip;
}
