package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MomentModel implements Serializable {

    private String id;
    private String contentUrl;

}
