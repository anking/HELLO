package com.yanhua.common.model;

import java.io.Serializable;

public class InviteTemplateModel implements Serializable {

    private String meetPartnerTypeId;
    private String url;
    private String id;
    private String name;

    public InviteTemplateModel(String meetPartnerTypeId, String id, String url, String name) {
        this.meetPartnerTypeId = meetPartnerTypeId;
        this.url = url;
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeetPartnerTypeId() {
        return meetPartnerTypeId;
    }

    public void setMeetPartnerTypeId(String meetPartnerTypeId) {
        this.meetPartnerTypeId = meetPartnerTypeId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
