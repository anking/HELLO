package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

public class ValueContentModel implements Serializable {

    private List<SelectConditionModel> value_count;

    public List<SelectConditionModel> getValue_count() {
        return value_count;
    }

    public void setValue_count(List<SelectConditionModel> value_count) {
        this.value_count = value_count;
    }
}
