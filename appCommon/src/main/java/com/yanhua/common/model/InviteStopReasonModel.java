package com.yanhua.common.model;

import java.io.Serializable;

public class InviteStopReasonModel implements Serializable {

    private String id;
    private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
