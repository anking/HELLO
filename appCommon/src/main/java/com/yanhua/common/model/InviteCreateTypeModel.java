package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

public class InviteCreateTypeModel implements Serializable {

    private List<TypeWayModel> partnerType;
    private List<TypeWayModel> paymentType;

    public List<TypeWayModel> getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(List<TypeWayModel> partnerType) {
        this.partnerType = partnerType;
    }

    public List<TypeWayModel> getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(List<TypeWayModel> paymentType) {
        this.paymentType = paymentType;
    }
}
