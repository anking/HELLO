package com.yanhua.common.model;

import java.io.Serializable;

/**
 * 分享对象
 * Created by shuyu on 2016/11/10.
 */

public class ShareObjectModel implements Serializable {
    private String nickName;    // 昵称
    private String img; //
    private String userId;//
    private boolean isSelected;//用户选择
    private boolean isGroup;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            ShareObjectModel friendInfo = (ShareObjectModel) o;
            return (getUserId() != null && getUserId().equals(friendInfo.getUserId()));
        } else {
            return false;
        }
    }
}
