package com.yanhua.common.model;

import java.io.Serializable;

public class InviteConfigModel implements Serializable {
    private String id;
    private int meetPartnerLimit;//约伴限制(-1:不限制)
    private int meetPartnerOpenAudit;//开启约伴审核(0:关闭, 1:开启)
    private int meetPartnerOpenTip;//开启约伴提示语(0:关闭, 1:开启)
    private String meetPartnerTip;//约伴提示语
    private int messageOpenAudit;//开启留言审核(0:关闭, 1:开启)

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMeetPartnerLimit() {
        return meetPartnerLimit;
    }

    public void setMeetPartnerLimit(int meetPartnerLimit) {
        this.meetPartnerLimit = meetPartnerLimit;
    }

    public int getMeetPartnerOpenAudit() {
        return meetPartnerOpenAudit;
    }

    public void setMeetPartnerOpenAudit(int meetPartnerOpenAudit) {
        this.meetPartnerOpenAudit = meetPartnerOpenAudit;
    }

    public int getMeetPartnerOpenTip() {
        return meetPartnerOpenTip;
    }

    public void setMeetPartnerOpenTip(int meetPartnerOpenTip) {
        this.meetPartnerOpenTip = meetPartnerOpenTip;
    }

    public String getMeetPartnerTip() {
        return meetPartnerTip;
    }

    public void setMeetPartnerTip(String meetPartnerTip) {
        this.meetPartnerTip = meetPartnerTip;
    }

    public int getMessageOpenAudit() {
        return messageOpenAudit;
    }

    public void setMessageOpenAudit(int messageOpenAudit) {
        this.messageOpenAudit = messageOpenAudit;
    }
}
