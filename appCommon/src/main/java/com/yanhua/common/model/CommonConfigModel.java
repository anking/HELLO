package com.yanhua.common.model;

import java.io.Serializable;

public class CommonConfigModel implements Serializable {

    private String content;
    private String id;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
