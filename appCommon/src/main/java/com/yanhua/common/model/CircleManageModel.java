package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

public class CircleManageModel implements Serializable {

    private String circleCode;
    private String circleDesc;
    private List<MyCircleComputerModel> circleStatVOList;
    private String coverUrl;
    private int dynamicCount;
    private int fansCount;
    private String id;
    private String notice;
    private String subtitle;
    private String title;
    private int viewCount;

    private boolean showMore;

    public boolean isShowMore() {
        return showMore;
    }

    public void setShowMore(boolean showMore) {
        this.showMore = showMore;
    }
    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getCircleDesc() {
        return circleDesc;
    }

    public void setCircleDesc(String circleDesc) {
        this.circleDesc = circleDesc;
    }

    public List<MyCircleComputerModel> getCircleStatVOList() {
        return circleStatVOList;
    }

    public void setCircleStatVOList(List<MyCircleComputerModel> circleStatVOList) {
        this.circleStatVOList = circleStatVOList;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getDynamicCount() {
        return dynamicCount;
    }

    public void setDynamicCount(int dynamicCount) {
        this.dynamicCount = dynamicCount;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }
}
