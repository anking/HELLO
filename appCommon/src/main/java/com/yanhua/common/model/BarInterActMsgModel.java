package com.yanhua.common.model;

import java.io.Serializable;

public class BarInterActMsgModel implements Serializable {

    private String askedDesc;//回答信息内容


    private String askedId;//回答id

    private int barInfoDel;//所属酒吧是否被删除(0:正常, 1:删除)

    private String barInfoId;//所属酒吧ID

    private String barInfoImg;//所属酒吧主图

    private String barInfoName;//所属酒吧名称

    private String barInfoType;// 所属酒吧类型

    private int commentContentParentDel;//评论内容的父级（提问） 0:未删除 1: 删除 ）

    private int commentContentParentStatus;//评论内容的父级（提问）（审核状态;(0:待审核 1:审核通过 2:隐藏 3:删除 -1:驳回)）

    private int contentDel;// 内容是否被删除 (0:未删除 1 :删除)

    private int contentStatus;//内容是否被删除 状态;(0:待审核 1:审核通过 2:隐藏 3:删除 -1:驳回)

    private String createdTime;//创建日期

    private int isShow;//上级提问是否显示(0: 显示, 1: 匿名)

    private int parentAskedDel;// 提问内容是否被删除(0 未删除 1 删除)

    private String parentAskedDesc;//提问信息内容

    private String parentAskedId;//提问id

    private int parentAskedStatus;//提问内容状态

    private int type;//类型（0回答，1评论）

    private String userId;//回答用户id

    private String userImg;//回答用户昵称

    private String userName;//回答用户昵称


    public String getAskedDesc() {
        return askedDesc;
    }

    public void setAskedDesc(String askedDesc) {
        this.askedDesc = askedDesc;
    }

    public String getAskedId() {
        return askedId;
    }

    public void setAskedId(String askedId) {
        this.askedId = askedId;
    }

    public int getBarInfoDel() {
        return barInfoDel;
    }

    public void setBarInfoDel(int barInfoDel) {
        this.barInfoDel = barInfoDel;
    }

    public String getBarInfoId() {
        return barInfoId;
    }

    public void setBarInfoId(String barInfoId) {
        this.barInfoId = barInfoId;
    }

    public String getBarInfoImg() {
        return barInfoImg;
    }

    public void setBarInfoImg(String barInfoImg) {
        this.barInfoImg = barInfoImg;
    }

    public String getBarInfoName() {
        return barInfoName;
    }

    public void setBarInfoName(String barInfoName) {
        this.barInfoName = barInfoName;
    }

    public String getBarInfoType() {
        return barInfoType;
    }

    public void setBarInfoType(String barInfoType) {
        this.barInfoType = barInfoType;
    }

    public int getCommentContentParentDel() {
        return commentContentParentDel;
    }

    public void setCommentContentParentDel(int commentContentParentDel) {
        this.commentContentParentDel = commentContentParentDel;
    }

    public int getCommentContentParentStatus() {
        return commentContentParentStatus;
    }

    public void setCommentContentParentStatus(int commentContentParentStatus) {
        this.commentContentParentStatus = commentContentParentStatus;
    }

    public int getContentDel() {
        return contentDel;
    }

    public void setContentDel(int contentDel) {
        this.contentDel = contentDel;
    }

    public int getContentStatus() {
        return contentStatus;
    }

    public void setContentStatus(int contentStatus) {
        this.contentStatus = contentStatus;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getIsShow() {
        return isShow;
    }

    public void setIsShow(int isShow) {
        this.isShow = isShow;
    }

    public int getParentAskedDel() {
        return parentAskedDel;
    }

    public void setParentAskedDel(int parentAskedDel) {
        this.parentAskedDel = parentAskedDel;
    }

    public String getParentAskedDesc() {
        return parentAskedDesc;
    }

    public void setParentAskedDesc(String parentAskedDesc) {
        this.parentAskedDesc = parentAskedDesc;
    }

    public String getParentAskedId() {
        return parentAskedId;
    }

    public void setParentAskedId(String parentAskedId) {
        this.parentAskedId = parentAskedId;
    }

    public int getParentAskedStatus() {
        return parentAskedStatus;
    }

    public void setParentAskedStatus(int parentAskedStatus) {
        this.parentAskedStatus = parentAskedStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
