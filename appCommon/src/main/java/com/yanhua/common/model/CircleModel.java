package com.yanhua.common.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * 包含圈子记录、圈子列表
 */
public class CircleModel implements Serializable {

    /* {
         "auditRemark": "string",
             "circleCode": "string",
             "circleDesc": "string",
             "circleMaster": "string",
             "coverUrl": "string",
             "dynamicCount": 0,
             "id": "string",
             "notice": "string",
             "status": 0,
             "subtitle": "string",
             "title": "string",
             "viewCount": 0
             auditRemark	string
 驳回原因
 circleCode	string
 圈子编号
 circleDesc	string
 圈子描述
 circleMaster	string
 圈主
 coverUrl	string
 封面图url
 dynamicCount	integer($int32)
 动态量
 id	string
 圈子id
 notice	string
 公告
 status	integer($int32)
 状态;0正常 1待审核 2禁用 3删除
 subtitle	string
 圈子副标题
 title	string
 圈子标题
 viewCount	integer($int32)
 浏览量
     }*/

    private int fansCount;

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    private Integer masterApplyStatus;//value = "圈主申请状态:0未申请 1已申请"
    private Integer auditStatus;//圈主审核状态;0待审核 1已有圈主 2暂无圈主
    private String applyId;


    public Integer getMasterApplyStatus() {
        return masterApplyStatus;
    }

    public void setMasterApplyStatus(Integer masterApplyStatus) {
        this.masterApplyStatus = masterApplyStatus;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    private String circleId;
    private String createdTime;//申请时间
    private String auditTime;//审核时间
    private String userId;//申请人(用户id)
    private String status;//状态;0待审核 1已通过 2未通过
    private String id;
    private String title;
    private String subtitle;//圈子副标题
    private String circleCode;//圈子编号
    private String circleDesc;//圈子描述
    private String notice;//公告
    private String coverUrl;//封面图url
    private int statusCode;//0待审核 1已通过 2未通过
    private int joinStatus;
    private String auditRemark;//驳回原因
    private String circleMaster;
    private int dynamicCount;
    private int viewCount;


    private int deleted;// "逻辑删除;0正常,1删除"
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }


    private AuditRemarkModel auditReason;//驳回原因

    public AuditRemarkModel getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(AuditRemarkModel auditReason) {
        this.auditReason = auditReason;
    }

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String statusText) {
        this.status = statusText;
    }

    public int getJoinStatus() {
        return joinStatus;
    }

    public void setJoinStatus(int joinStatus) {
        this.joinStatus = joinStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getCircleCode() {
        return circleCode;
    }

    public void setCircleCode(String circleCode) {
        this.circleCode = circleCode;
    }

    public String getCircleDesc() {
        return circleDesc;
    }

    public void setCircleDesc(String circleDesc) {
        this.circleDesc = circleDesc;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public String getCircleMaster() {
        return circleMaster;
    }

    public void setCircleMaster(String circleMaster) {
        this.circleMaster = circleMaster;
    }

    public int getDynamicCount() {
        return dynamicCount;
    }

    public void setDynamicCount(int dynamicCount) {
        this.dynamicCount = dynamicCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CircleModel that = (CircleModel) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }
}
