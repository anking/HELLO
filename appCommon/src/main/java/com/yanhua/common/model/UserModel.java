package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserModel implements Serializable {
    /**
     * "id": "1386503227078823937",
     * "identityId": "8278583960",
     * "img": "https://yinquan-public.oss-cn-guangzhou.aliyuncs.com/image/2022/04/1649411683968.png",
     * "gender": 1,
     * "address": "广东省,深圳市",
     * "birthday": "1988-09-26",
     * "constellationState": 1,
     * "height": 170,
     * "weight": 75,
     * "hometown": "广西壮族自治区,南宁市",
     * "personalSignature": "Hbj",
     * "emotionTypeName": "已婚",
     * "relationState": 2,
     * "fansCount": 4,
     * "followCount": 0
     */

    private String id;
    private String identityId;
    private String nickName;
    private String img;
    private int gender;
    private String address;
    private String birthday;
    private int constellationState;
    private int height;
    private int weight;
    private String hometown;
    private String personalSignature;
    private String emotionTypeName;
    private int relationState;
    private int fansCount;
    private int followCount;

}
