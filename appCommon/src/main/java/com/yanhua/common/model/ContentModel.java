package com.yanhua.common.model;

import java.io.Serializable;

public class ContentModel implements Serializable {

    /**
     * auditRemark : string
     * collectCount : 0
     * commentStatus : 0
     * content : string
     * contentCategoryId : string
     * contentType: int
     * contentTitle : string
     * contentUrl : string
     * createdTime : 2020-12-12T07:37:59.348Z
     * fabulousCount : 0
     * id : string
     * issueAddress : string
     * issueCity : string
     * issueLatitude : string
     * issueLongitude : string
     * nickName : string
     * replyCount : 0
     * userId : string
     * userPhoto : string
     * viewCount : 0
     * isCollect : true
     * isFabulous : true
     */

    private String auditRemark;
    private int collectCount;
    private int commentStatus;
    private String content;
    private String contentCategoryId;
    private int contentType;
    private String contentTitle;
    private String urls;
    private String coverUrl;//视频封面
    private String createdTime;
    private int fabulousCount;
    private String id;
    private String issueAddress;
    private String issueAddressDetail;
    private String issueCity;
    private String issueLatitude;
    private String issueLongitude;
    private String nickName;

    private String friendRemark;
    private int replyCount;
    private String userId;
    private String userPhoto;
    private int viewCount;
    private String distanceResult;
    private String inviteCode;// 用户悦鑫码

    private boolean isCollect;
    private boolean isFabulous;
    private boolean isFollow;

    private int auditStatus;
    private boolean needAnim;

    private String width;
    private String height;

    private int privacyType;//内容隐私类型 默认公开(0公开 1朋友 2私密) --- 公开(所有人可见) 朋友(互相关注好友可见) 私密(仅自己可见)
    private boolean isView;// 是否可见


    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public boolean isView() {
        return isView;
    }

    public void setView(boolean view) {
        isView = view;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    private int videoTime;
    private FindContentCategory findContentCategory;

    public FindContentCategory getFindContentCategory() {
        return findContentCategory;
    }

    public void setFindContentCategory(FindContentCategory findContentCategory) {
        this.findContentCategory = findContentCategory;
    }

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    //内容包含类型 1:图片2:视频

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public int getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(int collectCount) {
        this.collectCount = collectCount;
    }

    public int getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(int commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getFabulousCount() {
        return fabulousCount;
    }

    public void setFabulousCount(int fabulousCount) {
        this.fabulousCount = fabulousCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(int replyCount) {
        this.replyCount = replyCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    public boolean isIsCollect() {
        return isCollect;
    }

    public void setIsCollect(boolean isCollect) {
        this.isCollect = isCollect;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public boolean isIsFabulous() {
        return isFabulous;
    }

    public void setIsFabulous(boolean isFabulous) {
        this.isFabulous = isFabulous;
    }

    public String getDistanceResult() {
        return distanceResult;
    }

    public void setDistanceResult(String distanceResult) {
        this.distanceResult = distanceResult;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getIssueAddressDetail() {
        return issueAddressDetail;
    }

    public void setIssueAddressDetail(String issueAddressDetail) {
        this.issueAddressDetail = issueAddressDetail;
    }

    public boolean isCollect() {
        return isCollect;
    }

    public void setCollect(boolean collect) {
        isCollect = collect;
    }

    public boolean isFabulous() {
        return isFabulous;
    }

    public void setFabulous(boolean fabulous) {
        isFabulous = fabulous;
    }

    public boolean isNeedAnim() {
        return needAnim;
    }

    public void setNeedAnim(boolean needAnim) {
        this.needAnim = needAnim;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ContentModel) {
            ContentModel u = (ContentModel) obj;
            return this.id.equals(u.id);
        }
        return super.equals(obj);
    }
//    @Override
//    public boolean equals(Object obj) {
//        if (!(obj instanceof ContentModel)) {
//            return false;
//        }
//
//        ContentModel u = (ContentModel) obj;
//        return this.id.equals(u.id);
//    }
}
