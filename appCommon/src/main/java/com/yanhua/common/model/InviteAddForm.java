package com.yanhua.common.model;

import java.io.Serializable;
import java.util.ArrayList;

public class InviteAddForm implements Serializable {
    //    约伴-邀约信息表单
    private int activityNumber;//
    private String activityTime;
    private String city;
    private String coverUrl;
    private String destination;
    private String destinationLatitude;
    private String destinationLongitude;
    private String id;
    private ArrayList<String> imagesUrl;
    private String introduction;
    private String meetPartnerTypeId;
    private String meetPartnerTypeName;
    private String partnerTypeName;
    private String paymentTypeName;
    private String partnerTypeId;
    private String paymentTypeId;
    private String address;
    private String meetPartnerTypeTemplateId;


    private String title;
    private String userId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMeetPartnerTypeName() {
        return meetPartnerTypeName;
    }

    public void setMeetPartnerTypeName(String meetPartnerTypeName) {
        this.meetPartnerTypeName = meetPartnerTypeName;
    }

    public int getActivityNumber() {
        return activityNumber;
    }

    public void setActivityNumber(int activityNumber) {
        this.activityNumber = activityNumber;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(String destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public String getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(String destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(ArrayList<String> imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getMeetPartnerTypeId() {
        return meetPartnerTypeId;
    }

    public void setMeetPartnerTypeId(String meetPartnerTypeId) {
        this.meetPartnerTypeId = meetPartnerTypeId;
    }

    public String getPartnerTypeName() {
        return partnerTypeName;
    }

    public void setPartnerTypeName(String partnerTypeName) {
        this.partnerTypeName = partnerTypeName;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getPartnerTypeId() {
        return partnerTypeId;
    }

    public void setPartnerTypeId(String partnerTypeId) {
        this.partnerTypeId = partnerTypeId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getMeetPartnerTypeTemplateId() {
        return meetPartnerTypeTemplateId;
    }

    public void setMeetPartnerTypeTemplateId(String meetPartnerTypeTemplateId) {
        this.meetPartnerTypeTemplateId = meetPartnerTypeTemplateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
