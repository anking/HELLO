package com.yanhua.common.model;

/**
 * 热榜数据
 */
public class MomentHotListModel {
    public final static int TOPIC = 1;
    public final static int CIRCLE = 2;

    private int hotListType;// 1--话题 2---圈子

    private String id;
    private String title;

    public int getHotListType() {
        return hotListType;
    }

    public void setHotListType(int hotListType) {
        this.hotListType = hotListType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
