package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;
public class CityObjModel implements Serializable {

    private List<CityModel> cities;
    private String id;
    private String key;

    public List<CityModel> getCities() {
        return cities;
    }

    public void setCities(List<CityModel> cities) {
        this.cities = cities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
