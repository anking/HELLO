package com.yanhua.common.model;

import java.io.Serializable;
import java.util.ArrayList;

public class InviteModel implements Serializable {
//    activityNumber	// 活动人数
//    activityTime	//活动时间
//    browseNum	//浏览量
//    city	// 目的地所在城市
//    coverUrl	//组局封面
//    destination	//目的地
//    destinationLatitude	// 目的地纬度
//    destinationLongitude	// 目的地经度
//    enrollNumber	// 报名人数
//    id	//
//    imagesUrl	// 组局图片[url, url]
//    introduction	//简介内容
//    inviteNumber	//已邀约人数
//    inviteState	//当前用户的邀约状态(0:未邀约, 1:已邀约)
//    meetPartnerTypeId	//约伴类型id
//    meetPartnerTypeName	//约伴类型名称
//    partnerType	//邀约的伙伴类别
//    paymentType	//买单方式
//    status	//审核状态(0:审核中, 1:审核未通过, 2:审核通过, 3:已停约, 4:用户删除, 5:平台删除, 6:已取消)
//    tagType	//标记类型(0:浏览者, 1:邀约者)
//    title	//标题内容
//    userGender	//邀约用户性别(-1:保密, 1:男, 2:女)
//    userId	//邀约用户id
//    userImage	//邀约用户头像
//    userName	//邀约用户名称
// rejectReason 未通过原因
    //collectState //收藏类型(0:为收藏, 1:已收藏)

    private int showOpenSet;//0：关闭，1：开启
    private int showOpenSetEnroll;//  0:关闭，1：金头像 2：头衔昵称 3：个人主页

    private int activityNumber;
    private String activityTime;
    private int browseNum;
    private String city;
    private String coverUrl;
    private String destination;
    private String destinationLatitude;
    private String destinationLongitude;
    private int enrollNumber;
    private String id;
    private String introduction;
    private int inviteNumber;
    private int inviteState;
    private int enrollState;
    private String enrollId;
    private String meetPartnerTypeId;
    private String meetPartnerTypeName;
    private ArrayList<String> imagesUrl;
    private String meetPartnerTypeTemplateId;

    private String partnerTypeId;
    private String partnerTypeName;
    private String paymentTypeId;
    private String paymentTypeName;

    private int status;
    private int messageNum;
    private int tagType;
    private int collectState;//详情中使用
    private String title;
    private int userGender;
    private String userId;
    private String userImage;
    private String userName;
    private String rejectReason;

    private String browseTime;

    private String address;

    private String browseDay;

    public int getMessageNum() {
        return messageNum;
    }

    public void setMessageNum(int messageNum) {
        this.messageNum = messageNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBrowseDay() {
        return browseDay;
    }

    public void setBrowseDay(String browseDay) {
        this.browseDay = browseDay;
    }

    public String getBrowseTime() {
        return browseTime;
    }

    public void setBrowseTime(String browseTime) {
        this.browseTime = browseTime;
    }

    public int getCollectState() {
        return collectState;
    }

    public void setCollectState(int collectState) {
        this.collectState = collectState;
    }

    public String getMeetPartnerTypeTemplateId() {
        return meetPartnerTypeTemplateId;
    }

    public void setMeetPartnerTypeTemplateId(String meetPartnerTypeTemplateId) {
        this.meetPartnerTypeTemplateId = meetPartnerTypeTemplateId;
    }

    public int getEnrollState() {
        return enrollState;
    }

    public String getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(String enrollId) {
        this.enrollId = enrollId;
    }

    public void setEnrollState(int enrollState) {
        this.enrollState = enrollState;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public int getActivityNumber() {
        return activityNumber;
    }

    public void setActivityNumber(int activityNumber) {
        this.activityNumber = activityNumber;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public int getBrowseNum() {
        return browseNum;
    }

    public void setBrowseNum(int browseNum) {
        this.browseNum = browseNum;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(String destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public String getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(String destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public int getEnrollNumber() {
        return enrollNumber;
    }

    public void setEnrollNumber(int enrollNumber) {
        this.enrollNumber = enrollNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getInviteNumber() {
        return inviteNumber;
    }

    public void setInviteNumber(int inviteNumber) {
        this.inviteNumber = inviteNumber;
    }

    public int getInviteState() {
        return inviteState;
    }

    public void setInviteState(int inviteState) {
        this.inviteState = inviteState;
    }

    public String getMeetPartnerTypeId() {
        return meetPartnerTypeId;
    }

    public void setMeetPartnerTypeId(String meetPartnerTypeId) {
        this.meetPartnerTypeId = meetPartnerTypeId;
    }

    public String getMeetPartnerTypeName() {
        return meetPartnerTypeName;
    }

    public void setMeetPartnerTypeName(String meetPartnerTypeName) {
        this.meetPartnerTypeName = meetPartnerTypeName;
    }

    public ArrayList<String> getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(ArrayList<String> imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public String getPartnerTypeId() {
        return partnerTypeId;
    }

    public void setPartnerTypeId(String partnerTypeId) {
        this.partnerTypeId = partnerTypeId;
    }

    public String getPartnerTypeName() {
        return partnerTypeName;
    }

    public void setPartnerTypeName(String partnerTypeName) {
        this.partnerTypeName = partnerTypeName;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTagType() {
        return tagType;
    }

    public void setTagType(int tagType) {
        this.tagType = tagType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserGender() {
        return userGender;
    }

    public void setUserGender(int userGender) {
        this.userGender = userGender;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getShowOpenSet() {
        return showOpenSet;
    }

    public void setShowOpenSet(int showOpenSet) {
        this.showOpenSet = showOpenSet;
    }

    public int getShowOpenSetEnroll() {
        return showOpenSetEnroll;
    }

    public void setShowOpenSetEnroll(int showOpenSetEnroll) {
        this.showOpenSetEnroll = showOpenSetEnroll;
    }
}
