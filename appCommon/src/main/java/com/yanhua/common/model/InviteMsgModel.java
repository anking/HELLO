package com.yanhua.common.model;

import java.io.Serializable;

public class InviteMsgModel implements Serializable {

    private String content;//内容
    private String createdTime;//创建时间
    private String id;//内容
    private String inviteCoverUrl;//邀约主图url
    private String inviteId;//邀约id
    private String inviteTitle;//邀约标题

    private int state;//状态(0:未读, 1:已读)
    private int type;//类型(1:报名, 2:取消报名, 3:邀约确认, 4:邀约移除, 5:邀约取消)
    private String userId;//用户id
    private String userName;



    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInviteCoverUrl() {
        return inviteCoverUrl;
    }

    public void setInviteCoverUrl(String inviteCoverUrl) {
        this.inviteCoverUrl = inviteCoverUrl;
    }

    public String getInviteId() {
        return inviteId;
    }

    public void setInviteId(String inviteId) {
        this.inviteId = inviteId;
    }

    public String getInviteTitle() {
        return inviteTitle;
    }

    public void setInviteTitle(String inviteTitle) {
        this.inviteTitle = inviteTitle;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
