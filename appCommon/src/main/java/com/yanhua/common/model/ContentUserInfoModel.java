package com.yanhua.common.model;

import java.io.Serializable;

public class ContentUserInfoModel implements Serializable {


    /**
     * collectCount : 0
     * fabulousCount : 0
     * fansCount : 0
     * followCount : 0
     * id : string
     * name : string
     * photo : string
     */
    private int collectCount;
    private int fabulousCount;
    private int fansCount;
    private int followCount;

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    private int gender = -1;// "会员性别 -1保密,2女,1男"

    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    private int followStatus;
    private String id;
    private String name;
    private String photo;

    private int hideFollowersFanList;// 是否隐藏自己的关注和粉丝列表 1是0否

    public int getHideFollowersFanList() {
        return hideFollowersFanList;
    }

    public void setHideFollowersFanList(int hideFollowersFanList) {
        this.hideFollowersFanList = hideFollowersFanList;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    private String inviteCode;
    /**
     * address : string
     * follow : true
     * isFollow : true
     * nickName : string
     * realName : string
     * userId : string
     * userPhoto : string
     */
    private String address;
    private boolean follow;
    private boolean isFollow;
    private String nickName;
    private String realName;
    private String userId;
    private String userPhoto;

    private String introduction;

    private int articleCount;
    private int videoCount;
    private boolean isBlock;

    private String lastContentTime;
    private String blackgroundImg;//"背景图

    public boolean isBlock() {
        return isBlock;
    }

    public void setBlock(boolean block) {
        isBlock = block;
    }

    public String getBlackgroundImg() {
        return blackgroundImg;
    }

    public void setBlackgroundImg(String blackgroundImg) {
        this.blackgroundImg = blackgroundImg;
    }

    public int getIsAudit() {
        return isAudit;
    }

    public void setIsAudit(Integer isAudit) {
        this.isAudit = isAudit;
    }

    private int isAudit = 0;//是否待审核图片，0-否，1-是


    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    private String friendRemark;

    public boolean isIsBlock() {
        return isBlock;
    }

    public void setIsBlock(boolean isBlock) {
        this.isBlock = isBlock;
    }

    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(int collectCount) {
        this.collectCount = collectCount;
    }

    public int getFabulousCount() {
        return fabulousCount;
    }

    public void setFabulousCount(int fabulousCount) {
        this.fabulousCount = fabulousCount;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    public int getFollowCount() {
        return followCount;
    }

    public void setFollowCount(int followCount) {
        this.followCount = followCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    public boolean isIsFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getLastContentTime() {
        return lastContentTime;
    }

    public void setLastContentTime(String lastContentTime) {
        this.lastContentTime = lastContentTime;
    }
}
