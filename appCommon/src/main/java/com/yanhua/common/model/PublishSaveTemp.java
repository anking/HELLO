package com.yanhua.common.model;


import com.luck.picture.lib.entity.LocalMedia;
import com.shuyu.textutillib.model.TopicModel;
import com.shuyu.textutillib.model.FriendModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 地址实体对象
 *
 * @author Administrator
 */
public class PublishSaveTemp implements Serializable {
    private ArrayList<String> pathList;
    private String videoPath;
    private String coverPath;
    private int type;
    private int videoTime;

    private String videoCoverPath;


    private ArrayList<TopicModel> topicModelsEd;
    private ArrayList<FriendModel> nameListEd;

    private ArrayList<LocalMedia> dataList;
    public ArrayList<LocalMedia> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<LocalMedia> dataList) {
        this.dataList = dataList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String title;
    private String content;

    public ArrayList<String> getPathList() {
        return pathList;
    }

    public void setPathList(ArrayList<String> pathList) {
        this.pathList = pathList;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    public String getVideoCoverPath() {
        return videoCoverPath;
    }

    public void setVideoCoverPath(String videoCoverPath) {
        this.videoCoverPath = videoCoverPath;
    }

    public ArrayList<TopicModel> getTopicModelsEd() {
        return topicModelsEd;
    }

    public void setTopicModelsEd(ArrayList<TopicModel> topicModelsEd) {
        this.topicModelsEd = topicModelsEd;
    }

    public ArrayList<FriendModel> getNameListEd() {
        return nameListEd;
    }

    public void setNameListEd(ArrayList<FriendModel> nameListEd) {
        this.nameListEd = nameListEd;
    }
}
