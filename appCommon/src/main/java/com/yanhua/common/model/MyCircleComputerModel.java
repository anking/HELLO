package com.yanhua.common.model;

import java.io.Serializable;

public class MyCircleComputerModel implements Serializable {
    private int addContentCount;
    private int addMemberCount;
    private String dateType;

    public int getAddContentCount() {
        return addContentCount;
    }

    public void setAddContentCount(int addContentCount) {
        this.addContentCount = addContentCount;
    }

    public int getAddMemberCount() {
        return addMemberCount;
    }

    public void setAddMemberCount(int addMemberCount) {
        this.addMemberCount = addMemberCount;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }
}