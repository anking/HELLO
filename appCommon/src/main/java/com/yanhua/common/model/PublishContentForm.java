package com.yanhua.common.model;

import java.util.ArrayList;

public class PublishContentForm {

    private String content;
    private String contentCategoryId;
    private String contentTitle;

    //内容包含类型 1:图片 2:视频
    private int contentType;

    // 内容隐私类型 默认公开(0公开 1朋友 2私密) --- 公开(所有人可见) 朋友(互相关注好友可见) 私密(仅自己可见)
    private int privacyType;
    private String contentUrl;
    //发布位置
    private String issueAddress;
    private String issueAddressDetail;
    private String issueCity;
    private String issueLatitude;
    private String issueLongitude;
    private String introduce;

    private ArrayList<String> topicSaveFormList;//创建话题数据集合
    private ArrayList<String> topicIdList;//选中话题id集合
    private ArrayList<String> circleIdList;
    private ArrayList<String> userIdList;
    private int videoTime;

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public ArrayList<String> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(ArrayList<String> userIdList) {
        this.userIdList = userIdList;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueAddressDetail() {
        return issueAddressDetail;
    }

    public void setIssueAddressDetail(String issueAddressDetail) {
        this.issueAddressDetail = issueAddressDetail;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public ArrayList<String> getTopicSaveFormList() {
        return topicSaveFormList;
    }

    public void setTopicSaveFormList(ArrayList<String> topicSaveFormList) {
        this.topicSaveFormList = topicSaveFormList;
    }

    public ArrayList<String> getTopicIdList() {
        return topicIdList;
    }

    public void setTopicIdList(ArrayList<String> topicIdList) {
        this.topicIdList = topicIdList;
    }

    public ArrayList<String> getCircleIdList() {
        return circleIdList;
    }

    public void setCircleIdList(ArrayList<String> circleIdList) {
        this.circleIdList = circleIdList;
    }
}
