package com.yanhua.common.model;

import java.io.Serializable;

public class ProtocolVersionModel implements Serializable {

    private String id;
    private String typeId;
    private String typeName;
    private String code;
    private String title;
    private int version;
    private int forceState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getForceState() {
        return forceState;
    }

    public void setForceState(int forceState) {
        this.forceState = forceState;
    }
}
