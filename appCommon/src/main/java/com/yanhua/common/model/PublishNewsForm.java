package com.yanhua.common.model;

import java.io.Serializable;

public class PublishNewsForm implements Serializable {

    private String content;//内容
    private String contentCategoryId;//所属文章分类主键ID
    private String contentDraftId;//草稿箱id
    private String contentTitle;//内容标题
    private String coverUrl;//封面地址
    private String introduction;// 导语
    private String isDelete;//是否删除 1 = 已删除 0 = 未删除
    private String issueAddress;//发布位置
    private String issueCity;//发布城市
    private String issueLatitude;//发布纬度
    private String issueLongitude;//发布经度


    private String activityEndTime;//活动结束时间
    private String activityStartTime;//活动开始时间
    private String keyword;//关键词
    private String prePublishTime;// 预计发布时间
    private int publishType;//1 立即发布 2 定时发布
    private int type;//类型 3:爆料 4:攻略
    private String username;
    private String vedioCoverUrl;//封面如果是视频的话,需要传视频某一帧的截图
    private int videoTime;//视频时长

    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentDraftId() {
        return contentDraftId;
    }

    public void setContentDraftId(String contentDraftId) {
        this.contentDraftId = contentDraftId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPrePublishTime() {
        return prePublishTime;
    }

    public void setPrePublishTime(String prePublishTime) {
        this.prePublishTime = prePublishTime;
    }

    public int getPublishType() {
        return publishType;
    }

    public void setPublishType(int publishType) {
        this.publishType = publishType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVedioCoverUrl() {
        return vedioCoverUrl;
    }

    public void setVedioCoverUrl(String vedioCoverUrl) {
        this.vedioCoverUrl = vedioCoverUrl;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }
}
