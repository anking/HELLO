package com.yanhua.common.model;

import java.io.Serializable;

public class MsgSysModel implements Serializable {

    private String createdTime;
    private String content;//内容
    private String coverUrl;//封面图
    private String detailId;//内容详情id
    private String id;
    private String jumpContent;
    private int jumpType;//跳转类型(0:无,1:打开页面,2:URL,3:显示内容)
    private int state;//(0:未读, 1:已读)
    private String title;//标题
    private int type;//    通知类型(1:系统通知,2:此刻通知,3:约伴通知,
//            4:评论通知,5:圈子通知,6:爆料/攻略-授权通知,
//            7:爆料/攻略通知,8:问答通知,9:举报通知,
//            10:暂停发布通知,11:封禁通知,12:群投诉通知)
    private String userId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJumpContent() {
        return jumpContent;
    }

    public void setJumpContent(String jumpContent) {
        this.jumpContent = jumpContent;
    }

    public int getJumpType() {
        return jumpType;
    }

    public void setJumpType(int jumpType) {
        this.jumpType = jumpType;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
