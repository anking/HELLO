package com.yanhua.common.model;

import java.io.Serializable;

/**
 * 内容信息表
 */
public class NewsDetailModel implements Serializable {

    private String accountInfo;//账号信息

    private String activityEndTime;// 活动结束时间

    private String activityStartTime;//活动开始时间

    private String attributeName;//属性名称

    private NewsContentAttribute attributeVO;

    private String auditRemark;//驳回原因

    private int auditStatus;//内容审核状态(-1 驳回 0:待审核 1:已通过 2:隐藏 3:删除(用户不可见,管理员可见)) 4 沉帖 5 用户删除

    private String auditStatusName;//内容审核状态名称

    private String auditTime;// 审核时间

    private int collectCount;//收藏数

    private int commentCount;//评论数

    private int commentStatus;//是否关闭评论(0:否 1:是)

    private String content;//内容

    private String contentCategoryId;// 内容分类id

    private String contentId;//内容ID,ct + 时间戳 + 随机4位数字,展示使用

    private String contentTitle;//内容标题
    private int contentType;//内容类型;1图片，2文本，3视频
    private String coverUrl;//封面
    private String vedioCoverUrl;
    private String createdTime;//创建时间（发表时间）
    private int deleted;//1 = 已删除 0 = 未删除
    private int fabulousCount;//点赞数
    private String id;// 主键id
    private String introduction;// 导语
    private String issueAddress;//发布位置
    private String issueCity;//发布城市
    private String issueLatitude;//发布纬度
    private String issueLongitude;//发布经度
    private String keyword;//关键词

    private String mobile;//用户账号

    private String nickName;//用户昵称
    private String personalSignature;//个性签名

    private int followStatus;//关注转台
    private int pageViewCount;//浏览量

    private String prePublishTime;//预发布时间

    private int privacyType;//内容隐私类型 0公开 1朋友 2私密 --- 公开(所有人可见) 朋友(互相关注好友可见) 私密(仅自己可见)

    private String privacyTypeName;// 内容隐私类型 公开 朋友 私密

    private String publishTime;//发布时间

    private int publishType;//发布类型

    private int recommend;//是否是推荐内容1-是，0-否

    private String reportName;//内容分类名称

    private String userId;//所属用户ID

    private String userName;//用户账号
    private String img;

    private int userType;//用户类型 app用户 2后端用户
    private String userTypeName;//用户类型名称
    private boolean isCollect;
    private boolean isFabulous;

    public boolean isFabulous() {
        return isFabulous;
    }

    public void setFabulous(boolean fabulous) {
        isFabulous = fabulous;
    }

    public boolean isCollect() {
        return isCollect;
    }

    public void setCollect(boolean collect) {
        isCollect = collect;
    }

    public String getVedioCoverUrl() {
        return vedioCoverUrl;
    }

    public void setVedioCoverUrl(String vedioCoverUrl) {
        this.vedioCoverUrl = vedioCoverUrl;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPersonalSignature() {
        return personalSignature;
    }

    public void setPersonalSignature(String personalSignature) {
        this.personalSignature = personalSignature;
    }

    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    public String getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(String accountInfo) {
        this.accountInfo = accountInfo;
    }

    public String getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(String activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public String getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(String activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public NewsContentAttribute getAttributeVO() {
        return attributeVO;
    }

    public void setAttributeVO(NewsContentAttribute attributeVO) {
        this.attributeVO = attributeVO;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatusName() {
        return auditStatusName;
    }

    public void setAuditStatusName(String auditStatusName) {
        this.auditStatusName = auditStatusName;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public int getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(int collectCount) {
        this.collectCount = collectCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(int commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getFabulousCount() {
        return fabulousCount;
    }

    public void setFabulousCount(int fabulousCount) {
        this.fabulousCount = fabulousCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getPageViewCount() {
        return pageViewCount;
    }

    public void setPageViewCount(int pageViewCount) {
        this.pageViewCount = pageViewCount;
    }

    public String getPrePublishTime() {
        return prePublishTime;
    }

    public void setPrePublishTime(String prePublishTime) {
        this.prePublishTime = prePublishTime;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public String getPrivacyTypeName() {
        return privacyTypeName;
    }

    public void setPrivacyTypeName(String privacyTypeName) {
        this.privacyTypeName = privacyTypeName;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public int getPublishType() {
        return publishType;
    }

    public void setPublishType(int publishType) {
        this.publishType = publishType;
    }

    public int getRecommend() {
        return recommend;
    }

    public void setRecommend(int recommend) {
        this.recommend = recommend;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUserTypeName() {
        return userTypeName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }
}
