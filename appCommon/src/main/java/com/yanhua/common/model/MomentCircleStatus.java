package com.yanhua.common.model;

import java.io.Serializable;

public class MomentCircleStatus implements Serializable {

    private boolean circleMaster;
    private boolean joinCircle;

    public boolean isCircleMaster() {
        return circleMaster;
    }

    public void setCircleMaster(boolean circleMaster) {
        this.circleMaster = circleMaster;
    }

    public boolean isJoinCircle() {
        return joinCircle;
    }

    public void setJoinCircle(boolean joinCircle) {
        this.joinCircle = joinCircle;
    }
}
