package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class BarAnswerModel implements Serializable {
    /**
     * accountName	string
     * 账号名称
     * <p>
     * askedDesc	string
     * 提问/回答信息内容
     * <p>
     * askedFabulousCount	integer($int32)
     * 回答点赞总数量
     * <p>
     * askedId	string
     * 问答id
     * <p>
     * askedWorseCount	integer($int32)
     * 回答踩赞总数量
     * <p>
     * createdTime	string
     * 创建日期
     * <p>
     * fabulousCount	integer($int32)
     * 评论点赞总数量
     * <p>
     * img	string
     * 账号头像
     * <p>
     * userId	string
     * 账号id
     * <p>
     * worseCount	integer($int32)
     * 评论踩赞总数量
     */

    private String accountName;
    private String askedDesc;
    private int askedFabulousCount;
    private String askedId;
    private int askedWorseCount;
    private String createdTime;
    private int fabulousCount;
    private String img;
    private String userId;
    private int worseCount;
    private int isShow;
    private int isFollow;
    private int commentCount;
    private int fabulousStatus;
    private int worseStatus;

}
