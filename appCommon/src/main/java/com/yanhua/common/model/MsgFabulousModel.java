package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

public class MsgFabulousModel implements Serializable {

    private String contentContent;
    private String contentCoverUrl;
    private String contentCreatedTime;
    private String contentId;
    private int contentType;
    private String createdTime;
    private int dataType;
    private int fabulousType;
    private String id;
    private int type;
    private String userIds;
    private List<MsgUserModel> userList;

    public String getContentContent() {
        return contentContent;
    }

    public void setContentContent(String contentContent) {
        this.contentContent = contentContent;
    }

    public String getContentCoverUrl() {
        return contentCoverUrl;
    }

    public void setContentCoverUrl(String contentCoverUrl) {
        this.contentCoverUrl = contentCoverUrl;
    }

    public String getContentCreatedTime() {
        return contentCreatedTime;
    }

    public void setContentCreatedTime(String contentCreatedTime) {
        this.contentCreatedTime = contentCreatedTime;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public int getFabulousType() {
        return fabulousType;
    }

    public void setFabulousType(int fabulousType) {
        this.fabulousType = fabulousType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public List<MsgUserModel> getUserList() {
        return userList;
    }

    public void setUserList(List<MsgUserModel> userList) {
        this.userList = userList;
    }
}
