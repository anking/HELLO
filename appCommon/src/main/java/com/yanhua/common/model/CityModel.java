package com.yanhua.common.model;

import com.contrarywind.wheelview.interfaces.IPickerViewData;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class CityModel implements Serializable, IPickerViewData {

    private String agencyName;
    private String areaName;
    private String code;
    private String first;
    private String id;
    private String lat;
    private String lng;
    private String pinyin;
    private String zipCode;
    private List<CityModel> subCity;

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<CityModel> getSubCity() {
        return subCity;
    }

    public void setSubCity(List<CityModel> subCity) {
        this.subCity = subCity;
    }

    @Override
    public String getPickerViewText() {
        return areaName;
    }
}
