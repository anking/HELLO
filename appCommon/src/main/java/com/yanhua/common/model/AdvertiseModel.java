package com.yanhua.common.model;

import java.io.Serializable;

public class AdvertiseModel implements Serializable {

    private String videoCoverUrl;//视频封面url
    private String name;//广告名称
    private String content;//广告内容
    private String imageUrl;//广告图url
    private String id;
    private int type;// 广告类型(1:URL,2:搜索字段,3:话题,4:圈子,5:动态,6:页面,7:视频,8:酒吧,9:爆料/攻略,10:约伴)

    public String getVideoCoverUrl() {
        return videoCoverUrl;
    }

    public void setVideoCoverUrl(String videoCoverUrl) {
        this.videoCoverUrl = videoCoverUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
