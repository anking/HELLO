package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

/**
 * 举报参数实体
 *
 * @author Administrator
 */
public class ReportForm implements Serializable {

    private String businessId;
    private String reportDesc;//手寫舉報原因
    private List<FileResult> reportUrl;
    private int reportType;
    private String reportReasonId;//举报原因id

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public List<FileResult> getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(List<FileResult> reportUrl) {
        this.reportUrl = reportUrl;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getReportReasonId() {
        return reportReasonId;
    }

    public void setReportReasonId(String reportReasonId) {
        this.reportReasonId = reportReasonId;
    }
}
