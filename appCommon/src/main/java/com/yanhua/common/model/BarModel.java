package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class BarModel implements Serializable {

    private String barId;
    private String address;// 目的地详细地址
    private String barPrimaryImg;// 酒吧主图
    private String barTrait;// 酒吧特色
    private String barType;// 酒吧类型
    private String barTypeId;
    private int browseNum;// 浏览数
    private String businessTime;// 营业时间
    private int consumerNum;// 消费人数
    private String contact;// 联系方式
    private String distanceResult;// 距离
    private int distance;// 距离
    private String latitude;
    private String longitude;
    private String location;// 目的地所在城市
    private String name;// 酒吧名字
    private String rotationImgs;// 酒吧轮播图
    private int popularityNum;// 喜欢数
    private int popularityStatus;// 点赞状态 0未点赞 1已点赞
    private int collectStatus;// 收藏状态 0未收藏  1已收藏
}
