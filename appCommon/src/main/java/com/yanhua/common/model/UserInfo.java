package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class UserInfo implements Serializable {

    private String id;
    private String nickName;
    private String realName;
    private String idCard;
    private int gender;
    private String introduction;
    private String address;// 所在地
    private String hometown;// 故乡
    private String img;
    private int imgStatus;
    private String lastLoginTime;
    private String inviteCode;
    private String inBlacklist;
    private String username;
    private String mobile;
    private int userGrade;
    private int points;
    private int realNameValid;
    private int yuexinBean;
    private int frozenYuexinBean;
    private int likes;
    private int fans;
    private int favoriteGoods;
    private int favoriteStore;
    private int favoriteContent;
    private int saleReturn;
    private int footmark;
    private int friendNums;
    private int invitedFriend;
    private int meetPartnerNum;
    private int orders;
    private String createdTime;
    private String payPassword;
    private boolean loginPasswordStatus;//true 设置 false：未设置
    private int isSetPay;
    private int collectContentCount;
    private int status;// 用户状态 1启用 2禁用 3黑名单 4已注销

    private String unionid;// 绑定微信的unionid
    private String weixinNickname;// 绑定微信的昵称
    private String qqNickname;// 绑定QQ的昵称
    private String openid;// 绑定QQ的openid

    private String appleId;

    private int hideSetStatus;//隐私设置-消息联系设置0(显示)，1（不显示）")
    private int dynamicCount;//动态总数

    private String weiboOpenid;
    private String weiboNickname;

    public int getHideSetStatus() {
        return hideSetStatus;
    }

    public void setHideSetStatus(int hideSetStatus) {
        this.hideSetStatus = hideSetStatus;
    }

    public int getDynamicCount() {
        return dynamicCount;
    }

    public void setDynamicCount(int dynamicCount) {
        this.dynamicCount = dynamicCount;
    }

    public int getBlockUserNums() {
        return blockUserNums;
    }

    public void setBlockUserNums(int blockUserNums) {
        this.blockUserNums = blockUserNums;
    }

    private int blockUserNums;
    public String getAppleId() {
        return appleId;
    }

    public void setAppleId(String appleId) {
        this.appleId = appleId;
    }

    public String getWeiboOpenid() {
        return weiboOpenid;
    }

    public void setWeiboOpenid(String weiboOpenid) {
        this.weiboOpenid = weiboOpenid;
    }

    public String getWeiboNickname() {
        return weiboNickname;
    }

    public void setWeiboNickname(String weiboNickname) {
        this.weiboNickname = weiboNickname;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getWeixinNickname() {
        return weixinNickname;
    }

    public void setWeixinNickname(String weixinNickname) {
        this.weixinNickname = weixinNickname;
    }

    public String getQqNickname() {
        return qqNickname;
    }

    public void setQqNickname(String qqNickname) {
        this.qqNickname = qqNickname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public int getJoinDays() {
        return joinDays;
    }

    public void setJoinDays(int joinDays) {
        this.joinDays = joinDays;
    }

    public boolean isLoginPasswordStatus() {
        return loginPasswordStatus;
    }

    public void setLoginPasswordStatus(boolean loginPasswordStatus) {
        this.loginPasswordStatus = loginPasswordStatus;
    }

    public int getMeetPartnerNum() {
        return meetPartnerNum;
    }

    public void setMeetPartnerNum(int meetPartnerNum) {
        this.meetPartnerNum = meetPartnerNum;
    }

    private String makeFriendType;// 交友目的
    private String personalSignature;// 个性签名
    private String emotionTypeName;// 情感状态
    private String emotionTypeId;// 情感状态id
    private String occupationTypeId;// 职业类型id
    private String occupationTypeName;// 职业类型名称
    private String birthday;// 出生日期
    private String constellation;// 星座
    private int constellationState;// 星座显示(0:不显示,1:显示)
    private int height;
    private int weight;
    private List<PersonTagModel> userAloneTagVOList;// 个性标签
    private List<PersonTagModel> userHobbyTagVOList;// 兴趣爱好
    private List<PersonTagModel> userTagVOList;// 后台给用户打的用户标签
    private String imagesUrl;// 动态图片
    private String identityId;// 用户唯一标识(随机生成10位数字)
    private int joinDays;// 加入天数
    private List<MomentModel> momentContentList;// 发表过的动态
    private int relationState;// 1:未关注,2:已关注,3:被关注,4:互相关注
    private String memoName;// 备注名

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getImgStatus() {
        return imgStatus;
    }

    public void setImgStatus(int imgStatus) {
        this.imgStatus = imgStatus;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getInBlacklist() {
        return inBlacklist;
    }

    public void setInBlacklist(String inBlacklist) {
        this.inBlacklist = inBlacklist;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getUserGrade() {
        return userGrade;
    }

    public void setUserGrade(int userGrade) {
        this.userGrade = userGrade;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRealNameValid() {
        return realNameValid;
    }

    public void setRealNameValid(int realNameValid) {
        this.realNameValid = realNameValid;
    }

    public int getYuexinBean() {
        return yuexinBean;
    }

    public void setYuexinBean(int yuexinBean) {
        this.yuexinBean = yuexinBean;
    }

    public int getFrozenYuexinBean() {
        return frozenYuexinBean;
    }

    public void setFrozenYuexinBean(int frozenYuexinBean) {
        this.frozenYuexinBean = frozenYuexinBean;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getFans() {
        return fans;
    }

    public void setFans(int fans) {
        this.fans = fans;
    }

    public int getFavoriteGoods() {
        return favoriteGoods;
    }

    public void setFavoriteGoods(int favoriteGoods) {
        this.favoriteGoods = favoriteGoods;
    }

    public int getFavoriteStore() {
        return favoriteStore;
    }

    public void setFavoriteStore(int favoriteStore) {
        this.favoriteStore = favoriteStore;
    }

    public int getFavoriteContent() {
        return favoriteContent;
    }

    public void setFavoriteContent(int favoriteContent) {
        this.favoriteContent = favoriteContent;
    }

    public int getSaleReturn() {
        return saleReturn;
    }

    public void setSaleReturn(int saleReturn) {
        this.saleReturn = saleReturn;
    }

    public int getFootmark() {
        return footmark;
    }

    public void setFootmark(int footmark) {
        this.footmark = footmark;
    }

    public int getFriendNums() {
        return friendNums;
    }

    public void setFriendNums(int friendNums) {
        this.friendNums = friendNums;
    }

    public int getInvitedFriend() {
        return invitedFriend;
    }

    public void setInvitedFriend(int invitedFriend) {
        this.invitedFriend = invitedFriend;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public int getIsSetPay() {
        return isSetPay;
    }

    public void setIsSetPay(int isSetPay) {
        this.isSetPay = isSetPay;
    }

    public int getCollectContentCount() {
        return collectContentCount;
    }

    public void setCollectContentCount(int collectContentCount) {
        this.collectContentCount = collectContentCount;
    }

    public String getMakeFriendType() {
        return makeFriendType;
    }

    public void setMakeFriendType(String makeFriendType) {
        this.makeFriendType = makeFriendType;
    }

    public String getPersonalSignature() {
        return personalSignature;
    }

    public void setPersonalSignature(String personalSignature) {
        this.personalSignature = personalSignature;
    }

    public String getEmotionTypeName() {
        return emotionTypeName;
    }

    public void setEmotionTypeName(String emotionTypeName) {
        this.emotionTypeName = emotionTypeName;
    }

    public String getEmotionTypeId() {
        return emotionTypeId;
    }

    public void setEmotionTypeId(String emotionTypeId) {
        this.emotionTypeId = emotionTypeId;
    }

    public String getOccupationTypeId() {
        return occupationTypeId;
    }

    public void setOccupationTypeId(String occupationTypeId) {
        this.occupationTypeId = occupationTypeId;
    }

    public String getOccupationTypeName() {
        return occupationTypeName;
    }

    public void setOccupationTypeName(String occupationTypeName) {
        this.occupationTypeName = occupationTypeName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getConstellation() {
        return constellation;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public int getConstellationState() {
        return constellationState;
    }

    public void setConstellationState(int constellationState) {
        this.constellationState = constellationState;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<PersonTagModel> getUserAloneTagVOList() {
        return userAloneTagVOList;
    }

    public void setUserAloneTagVOList(List<PersonTagModel> userAloneTagVOList) {
        this.userAloneTagVOList = userAloneTagVOList;
    }

    public List<PersonTagModel> getUserHobbyTagVOList() {
        return userHobbyTagVOList;
    }

    public void setUserHobbyTagVOList(List<PersonTagModel> userHobbyTagVOList) {
        this.userHobbyTagVOList = userHobbyTagVOList;
    }

    public String getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public List<MomentModel> getMomentContentList() {
        return momentContentList;
    }

    public void setMomentContentList(List<MomentModel> momentContentList) {
        this.momentContentList = momentContentList;
    }

    public int getRelationState() {
        return relationState;
    }

    public void setRelationState(int relationState) {
        this.relationState = relationState;
    }

    public String getMemoName() {
        return memoName;
    }

    public void setMemoName(String memoName) {
        this.memoName = memoName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<PersonTagModel> getUserTagVOList() {
        return userTagVOList;
    }

    public void setUserTagVOList(List<PersonTagModel> userTagVOList) {
        this.userTagVOList = userTagVOList;
    }
}
