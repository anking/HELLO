package com.yanhua.common.model;

import java.io.Serializable;
public class MsgCommentAtModel implements Serializable {
    private String commentDesc;//评论内容
    private String commentUserId;//被评论的用户id;(上级评论用户id)
    private String commentUserNickName;//被评论的用户昵称;(上级评论用户昵称)
    private String contentCoverUrl;//内容封面图url
    private String contentId;//内容Id
    private String createdTime;//评论时间
    private int gender;// 性别(-1保密,1男,2女)
    private String id;
    private String nickName;
    private String personalSignature;//个性签名
    private int relationState;
    private int replyStatus;//回复状态;(回复状态,0-评论内容，1-回复评论)
    private int type;// 类型(1:此刻,2:约伴,3:爆料,4:攻略,5:集锦,6:新闻,7:酒吧新闻)
    private String userId;
    private int contentType;
    private String userPhoto;

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    public String getCommentUserNickName() {
        return commentUserNickName;
    }

    public void setCommentUserNickName(String commentUserNickName) {
        this.commentUserNickName = commentUserNickName;
    }

    public String getContentCoverUrl() {
        return contentCoverUrl;
    }

    public void setContentCoverUrl(String contentCoverUrl) {
        this.contentCoverUrl = contentCoverUrl;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPersonalSignature() {
        return personalSignature;
    }

    public void setPersonalSignature(String personalSignature) {
        this.personalSignature = personalSignature;
    }

    public int getRelationState() {
        return relationState;
    }

    public void setRelationState(int relationState) {
        this.relationState = relationState;
    }

    public int getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(int replyStatus) {
        this.replyStatus = replyStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
