package com.yanhua.common.model;

import java.io.Serializable;

public class MineCommentModel implements Serializable {

    /*评论样式说明：按照评论时间倒序

    评论人信息：头像+昵称
    评论内容：如果是回复指定人，显示回复@用户昵称
    被回复的内容：
    直接评论内容：不显示
    回复评论：显示评论的整体内容，包含：“评论者昵称（如有）” +“回复” + “被回复昵称（如有）” + “评论内容”
    图片：显示评论所属的动态/爆料/攻略/集锦/约伴，点击可跳转对应详情
    动态：
    图文动态选取第一张图片
    视频选取第一帧，带播放标识
            纯文字内容不显示
    爆料、攻略、约伴：封面
    集锦：视频选取第一帧，带播放标识
    发表时间：通用时间规则，时分/昨天时分/前天时分/星期x+时分/月日时分/年月日时分*/

    private int commentAudit;
    private String commentAuditName;
    private String commentDesc;
    private String commentId;
    private String contentId;
    private String contentTitle;
    private int contentType;
    private String contentUrl;
    private String coverUrl;
    private String createdTime;
    private int deleted;
    private String grandNickName;
    private String id;
    private String img;
    private String mobile;
    private String nickName;
    private String parentCommentDesc;
    private String parentCommentId;
    private String parentNickName;
    private String parentReplyStatus;
    private String rejectDesc;
    private int replyStatus;
    private int type;
    private String userId;
    private String parentUserId;
    private String grandUserId;
    private String vedioCoverUrl;
    private int videoTime;
    private int weightValue;

    public String getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(String parentUserId) {
        this.parentUserId = parentUserId;
    }

    public String getGrandUserId() {
        return grandUserId;
    }

    public void setGrandUserId(String grandUserId) {
        this.grandUserId = grandUserId;
    }

    public int getCommentAudit() {
        return commentAudit;
    }

    public void setCommentAudit(int commentAudit) {
        this.commentAudit = commentAudit;
    }

    public String getCommentAuditName() {
        return commentAuditName;
    }

    public void setCommentAuditName(String commentAuditName) {
        this.commentAuditName = commentAuditName;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getGrandNickName() {
        return grandNickName;
    }

    public void setGrandNickName(String grandNickName) {
        this.grandNickName = grandNickName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getParentCommentDesc() {
        return parentCommentDesc;
    }

    public void setParentCommentDesc(String parentCommentDesc) {
        this.parentCommentDesc = parentCommentDesc;
    }

    public String getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public String getParentNickName() {
        return parentNickName;
    }

    public void setParentNickName(String parentNickName) {
        this.parentNickName = parentNickName;
    }

    public String getParentReplyStatus() {
        return parentReplyStatus;
    }

    public void setParentReplyStatus(String parentReplyStatus) {
        this.parentReplyStatus = parentReplyStatus;
    }

    public String getRejectDesc() {
        return rejectDesc;
    }

    public void setRejectDesc(String rejectDesc) {
        this.rejectDesc = rejectDesc;
    }

    public int getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(int replyStatus) {
        this.replyStatus = replyStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVedioCoverUrl() {
        return vedioCoverUrl;
    }

    public void setVedioCoverUrl(String vedioCoverUrl) {
        this.vedioCoverUrl = vedioCoverUrl;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    public int getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(int weightValue) {
        this.weightValue = weightValue;
    }
}
