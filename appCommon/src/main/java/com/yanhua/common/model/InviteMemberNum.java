package com.yanhua.common.model;

import java.io.Serializable;

public class InviteMemberNum implements Serializable {
    private String id;
    private int inviteNumber;
    private int waitInviteNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getInviteNumber() {
        return inviteNumber;
    }

    public void setInviteNumber(int inviteNumber) {
        this.inviteNumber = inviteNumber;
    }

    public int getWaitInviteNumber() {
        return waitInviteNumber;
    }

    public void setWaitInviteNumber(int waitInviteNumber) {
        this.waitInviteNumber = waitInviteNumber;
    }
}
