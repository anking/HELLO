package com.yanhua.common.model;

import java.io.Serializable;

public class EventUpdateGroupInfo implements Serializable {
    private String type;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String content;

    public EventUpdateGroupInfo(String type) {
        this.type = type;
    }

    public EventUpdateGroupInfo(String type, String content) {
        this.type = type;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
