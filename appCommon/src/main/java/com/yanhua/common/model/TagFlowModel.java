package com.yanhua.common.model;

import java.io.Serializable;

public class TagFlowModel implements Serializable {

    public static final int ADDRESS = 1;
    public static final int CIRCLE = 2;
    public static final int TOPIC = 3;

    private int type;//类型
    private String id;//id
    private String name;
    private int drawable;
    private String issueLatitude;
    private String issueLongitude;



    public TagFlowModel(int type, String id, String name, int drawable) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.drawable = drawable;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }
}
