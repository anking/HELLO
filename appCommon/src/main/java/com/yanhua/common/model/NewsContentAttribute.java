package com.yanhua.common.model;

import java.io.Serializable;

public class NewsContentAttribute implements Serializable {
    private String attributeId;//主键id

    private int attributeType;//属性类型(1:精华 2:热门 3:推荐 4:日记,5置顶 6,热 7排行)

    private String attributeTypeName;//属性类型名称

    private String endTime;
    private int recommendType;//推荐类型 1:手动推荐 2:系统推荐

    private String recommendTypeName;//推荐类型名称 1:手动推荐 2:系统推荐

    private String relationId;//所属关联主键ID

    private int relationType;//属性设置类型(1:内容 2:评论)

    private int sort;//排序

    private String startTime;//
    private int type;//类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻

    private int virtualHot;// 虚拟热度

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public int getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(int attributeType) {
        this.attributeType = attributeType;
    }

    public String getAttributeTypeName() {
        return attributeTypeName;
    }

    public void setAttributeTypeName(String attributeTypeName) {
        this.attributeTypeName = attributeTypeName;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getRecommendType() {
        return recommendType;
    }

    public void setRecommendType(int recommendType) {
        this.recommendType = recommendType;
    }

    public String getRecommendTypeName() {
        return recommendTypeName;
    }

    public void setRecommendTypeName(String recommendTypeName) {
        this.recommendTypeName = recommendTypeName;
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    public int getRelationType() {
        return relationType;
    }

    public void setRelationType(int relationType) {
        this.relationType = relationType;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getVirtualHot() {
        return virtualHot;
    }

    public void setVirtualHot(int virtualHot) {
        this.virtualHot = virtualHot;
    }
}
