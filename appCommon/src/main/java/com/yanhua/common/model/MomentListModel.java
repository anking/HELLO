package com.yanhua.common.model;

import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;

import java.io.Serializable;
import java.util.List;

public class MomentListModel implements Serializable {

    private int resultType = 1;//结果类型(1:内容,2:广告)
    private List<AdvertiseModel> advertisingList;
    private String auditRemark;
    private int auditStatus;// 1是审核通过，0是未审核，-1是审核不通过
    private List<CircleModel> circleList;
    private int collectCount;
    private int commentStatus;
    private String content;
    private String introduce;
    private String contentCategoryId;
    private String contentTitle;
    private int contentType;
    private int type;
    private String contentUrl;
    private String createdTime;
    private String viewTime;
    private String fabulousTime;

    private int userType;//用户类型 app用户 2后端用户
    private String distanceResult;
    private int fabulousCount;
    private String followerId;
    private String friendRemark;
    private String id;
    private String inviteCode;
    private boolean isCollect;
    private boolean isFabulous;
    private int discoBest;
    private int deleted;//0未删除 1删除
    private int fabulousType;
    //    private boolean isFollow;
    //    /(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
    private int followStatus;

    private boolean isView;
    private String issueAddress;
    private String issueAddressDetail;
    private String issueCity;
    private String issueLatitude;
    private String issueLongitude;
    private String nickName;
    private int privacyType;
    private int replyCount;
    private List<TopicModel> topicList;
    private List<FriendModel> userList;
    private String userId;
    private String userPhoto;
    private int videoTime;
    private int pageViewCount;
    private int sex;// (-1:保密, 1:男, 2:女)
    private String month;
    private String browseDay;

    private String coverUrl;
    private String vedioCoverUrl;

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getVedioCoverUrl() {
        return vedioCoverUrl;
    }

    public void setVedioCoverUrl(String vedioCoverUrl) {
        this.vedioCoverUrl = vedioCoverUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFabulousType() {
        return fabulousType;
    }

    public void setFabulousType(int fabulousType) {
        this.fabulousType = fabulousType;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getDiscoBest() {
        return discoBest;
    }

    public void setDiscoBest(int discoBest) {
        this.discoBest = discoBest;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    public List<AdvertiseModel> getAdvertisingList() {
        return advertisingList;
    }

    public void setAdvertisingList(List<AdvertiseModel> advertisingList) {
        this.advertisingList = advertisingList;
    }

    public String getBrowseDay() {
        return browseDay;
    }

    public void setBrowseDay(String browseDay) {
        this.browseDay = browseDay;
    }

    public String getViewTime() {
        return viewTime;
    }

    public void setViewTime(String viewTime) {
        this.viewTime = viewTime;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public List<FriendModel> getUserList() {
        return userList;
    }

    public void setUserList(List<FriendModel> userList) {
        this.userList = userList;
    }

    public String getFabulousTime() {
        return fabulousTime;
    }

    public void setFabulousTime(String fabulousTime) {
        this.fabulousTime = fabulousTime;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAuditRemark() {
        return auditRemark;
    }

    public void setAuditRemark(String auditRemark) {
        this.auditRemark = auditRemark;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public List<CircleModel> getCircleList() {
        return circleList;
    }

    public void setCircleList(List<CircleModel> circleList) {
        this.circleList = circleList;
    }

    public int getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(int collectCount) {
        this.collectCount = collectCount;
    }

    public int getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(int commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getDistanceResult() {
        return distanceResult;
    }

    public void setDistanceResult(String distanceResult) {
        this.distanceResult = distanceResult;
    }

    public int getFabulousCount() {
        return fabulousCount;
    }

    public void setFabulousCount(int fabulousCount) {
        this.fabulousCount = fabulousCount;
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public boolean isCollect() {
        return isCollect;
    }

    public void setCollect(boolean collect) {
        isCollect = collect;
    }

    public boolean isFabulous() {
        return isFabulous;
    }

    public void setFabulous(boolean fabulous) {
        isFabulous = fabulous;
    }
//
//    public boolean isFollow() {
//        return isFollow;
//    }
//
//    public void setFollow(boolean follow) {
//        isFollow = follow;
//    }


    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    public boolean isView() {
        return isView;
    }

    public void setView(boolean view) {
        isView = view;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueAddressDetail() {
        return issueAddressDetail;
    }

    public void setIssueAddressDetail(String issueAddressDetail) {
        this.issueAddressDetail = issueAddressDetail;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public int getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(int replyCount) {
        this.replyCount = replyCount;
    }

    public List<TopicModel> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<TopicModel> topicList) {
        this.topicList = topicList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    public int getPageViewCount() {
        return pageViewCount;
    }

    public void setPageViewCount(int pageViewCount) {
        this.pageViewCount = pageViewCount;
    }
}
