package com.yanhua.common.model;

import java.io.Serializable;

public class SystemMsgModel implements Serializable {
    /**
     * id : 20210225119958134588706816
     * code : M8706817
     * titleName : 撒地方
     * sendStatus : 1
     * content : <p>士大夫士大夫</p>
     * photo : http://oss.yuexinguoji.com/default/image/2021/02/3983f1a2600a4449826d1ce1512b0770.png
     * sendTime : 2021-02-25 17:15:17
     */

    private String id;
    private String code;
    private String titleName;
    private int sendStatus;
    private String content;
    private String photo;
    private String sendTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public int getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(int sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }
}
