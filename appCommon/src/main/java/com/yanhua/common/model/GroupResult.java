package com.yanhua.common.model;

import java.io.Serializable;

public class GroupResult implements Serializable {

    /**
     * groupName : 陆柏林、出发吧、欧文骑扣詹姆斯
     * groupId : 20210512147394909707702272
     */
    private String groupName;
    private String groupId;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
