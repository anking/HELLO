package com.yanhua.common.model;

import java.io.Serializable;

/**
 * @author Administrator
 */
public class ConditionPartModel implements Serializable {
    private String name;
    private boolean selected;
    private String param;
    private int changeType;
    private String sourceType;

    public ConditionPartModel(String name, boolean selected, String param) {
        this.name = name;
        this.selected = selected;
        this.param = param;
    }

    public ConditionPartModel(String name, boolean selected, int changeType, String sourceType) {
        this.name = name;
        this.selected = selected;
        this.changeType = changeType;
        this.sourceType = sourceType;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getChangeType() {
        return changeType;
    }

    public void setChangeType(int changeType) {
        this.changeType = changeType;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }
}
