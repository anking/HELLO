package com.yanhua.common.model;

public class PublishCommentForm {

    /**
     * commentDesc : string 评论内容
     * contentId : string  所属内容主键ID
     * parentCommentId : string 上级评论主键ID,回复评论才需要传(评论的是内容则不传)
     */

    private String commentDesc;
    private String contentId;
    private String parentCommentId;
    private String userId;
//    ApiModelProperty("回复类型  1回复评论  2回复用户")
    private int replyType = 1;
    private int type;
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getReplyType() {
        return replyType;
    }

    public void setReplyType(int replyType) {
        this.replyType = replyType;
    }
}
