package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class PersonTagModel implements Serializable {

    private String id;
    private String tagName;
    private String name;// 用户标签使用
}
