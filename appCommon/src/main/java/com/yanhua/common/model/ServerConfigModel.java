package com.yanhua.common.model;

import java.io.Serializable;

public class ServerConfigModel implements Serializable {

    private String icon;
    private String key;
    private String memo;
    private int type;//(1:电话,2:QQ,3:微信)
    private String value;

    public ServerConfigModel() {
    }
    public ServerConfigModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public ServerConfigModel(String icon, String key, String memo, int type, String value) {
        this.icon = icon;
        this.key = key;
        this.memo = memo;
        this.type = type;
        this.value = value;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
