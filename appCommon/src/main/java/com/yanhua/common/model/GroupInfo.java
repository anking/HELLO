package com.yanhua.common.model;

import java.io.Serializable;

public class GroupInfo implements Serializable {
    /**
     * createdBy : string
     * createdTime : 2021-05-10T08:14:27.634Z
     * frozenTime : 2021-05-10T08:14:27.634Z
     * groupImg : string
     * groupMaster : string
     * groupName : string
     * groupNo : string
     * groupStatus : 0
     * id : string
     * invitation : 0
     * maxManageCount : 0
     * maxMemberCount : 0
     * memberCount : 0
     * messageDay : 0
     * noticeContent : string
     * noticeTime : 2021-05-10T08:14:27.634Z
     * showName : 0
     * thawTime : 2021-05-10T08:14:27.634Z
     * updateControlId : string
     * updatedBy : string
     * updatedTime : 2021-05-10T08:14:27.634Z
     */

    private String createdBy;
    private String createdTime;
    private String frozenTime;
    private String groupImg;
    private String groupMaster;
    private String groupName;
    private String groupNo;
    private int groupStatus;
    private String id;
    private int invitation;
    private int maxManageCount;
    private int maxMemberCount;
    private int memberCount;
    private int messageDay;
    private String noticeContent;
    private String noticeTime;
    private int showName;
    private String thawTime;
    private String updateControlId;
    private String updatedBy;
    private String updatedTime;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getFrozenTime() {
        return frozenTime;
    }

    public void setFrozenTime(String frozenTime) {
        this.frozenTime = frozenTime;
    }

    public String getGroupImg() {
        return groupImg;
    }

    public void setGroupImg(String groupImg) {
        this.groupImg = groupImg;
    }

    public String getGroupMaster() {
        return groupMaster;
    }

    public void setGroupMaster(String groupMaster) {
        this.groupMaster = groupMaster;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public int getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(int groupStatus) {
        this.groupStatus = groupStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getInvitation() {
        return invitation;
    }

    public void setInvitation(int invitation) {
        this.invitation = invitation;
    }

    public int getMaxManageCount() {
        return maxManageCount;
    }

    public void setMaxManageCount(int maxManageCount) {
        this.maxManageCount = maxManageCount;
    }

    public int getMaxMemberCount() {
        return maxMemberCount;
    }

    public void setMaxMemberCount(int maxMemberCount) {
        this.maxMemberCount = maxMemberCount;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public int getMessageDay() {
        return messageDay;
    }

    public void setMessageDay(int messageDay) {
        this.messageDay = messageDay;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }

    public String getNoticeTime() {
        return noticeTime;
    }

    public void setNoticeTime(String noticeTime) {
        this.noticeTime = noticeTime;
    }

    public int getShowName() {
        return showName;
    }

    public void setShowName(int showName) {
        this.showName = showName;
    }

    public String getThawTime() {
        return thawTime;
    }

    public void setThawTime(String thawTime) {
        this.thawTime = thawTime;
    }

    public String getUpdateControlId() {
        return updateControlId;
    }

    public void setUpdateControlId(String updateControlId) {
        this.updateControlId = updateControlId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

}
