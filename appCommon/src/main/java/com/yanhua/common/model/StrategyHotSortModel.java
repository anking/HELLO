package com.yanhua.common.model;

import java.io.Serializable;

public class StrategyHotSortModel implements Serializable {
    private boolean isSelect;
    private String contentTitle;//内容标题
    private String coverUrl;//封面路径
    private String id;//主键id
    private String reportName;//分类名称
    private String vedioCoverUrl;//视频封面路径
    private int vedioTime;//封面视频时长
    private int weightValue;//权重值

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getVedioCoverUrl() {
        return vedioCoverUrl;
    }

    public void setVedioCoverUrl(String vedioCoverUrl) {
        this.vedioCoverUrl = vedioCoverUrl;
    }

    public int getVedioTime() {
        return vedioTime;
    }

    public void setVedioTime(int vedioTime) {
        this.vedioTime = vedioTime;
    }

    public int getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(int weightValue) {
        this.weightValue = weightValue;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
