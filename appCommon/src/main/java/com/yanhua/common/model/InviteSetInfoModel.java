package com.yanhua.common.model;

import java.io.Serializable;

public class InviteSetInfoModel implements Serializable {
    private int showOpenSet;//0：关闭，1：开启

    private int showOpenSetEnroll;//  0:关闭，1：金头像 2：头衔昵称 3：个人主页

    public int getShowOpenSetEnroll() {
        return showOpenSetEnroll;
    }

    public void setShowOpenSetEnroll(int showOpenSetEnroll) {
        this.showOpenSetEnroll = showOpenSetEnroll;
    }

    private int showOpenSetUnregistered;//未报名  0:关闭，1：金头像 2：头衔昵称 3：个人主页
    private int showOpenSetUnEnrolled;//已报名，未确认
    private int showOpenSetConfirmedEnrolled;//已报名，已确认

    public int getShowOpenSet() {
        return showOpenSet;
    }

    public void setShowOpenSet(int showOpenSet) {
        this.showOpenSet = showOpenSet;
    }

    public int getShowOpenSetUnregistered() {
        return showOpenSetUnregistered;
    }

    public void setShowOpenSetUnregistered(int showOpenSetUnregistered) {
        this.showOpenSetUnregistered = showOpenSetUnregistered;
    }

    public int getShowOpenSetUnEnrolled() {
        return showOpenSetUnEnrolled;
    }

    public void setShowOpenSetUnEnrolled(int showOpenSetUnEnrolled) {
        this.showOpenSetUnEnrolled = showOpenSetUnEnrolled;
    }

    public int getShowOpenSetConfirmedEnrolled() {
        return showOpenSetConfirmedEnrolled;
    }

    public void setShowOpenSetConfirmedEnrolled(int showOpenSetConfirmedEnrolled) {
        this.showOpenSetConfirmedEnrolled = showOpenSetConfirmedEnrolled;
    }
}
