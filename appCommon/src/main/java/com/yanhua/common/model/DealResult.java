package com.yanhua.common.model;

import java.io.Serializable;

public class DealResult implements Serializable {
    private int ivResultSize;
    private int ivResultColor;
    private String ivResultText;

    private String tvResultText;
    private String tvResultDescText;
    private String tvGoPageText;
    private String tvGoPage;
    private Object goToObj;
    private Object subObj;
    private String subText;
    private String subPage;

    public String getSubPage() {
        return subPage;
    }

    public void setSubPage(String subPage) {
        this.subPage = subPage;
    }

    public Object getSubObj() {
        return subObj;
    }

    public void setSubObj(Object subObj) {
        this.subObj = subObj;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public int getIvResultSize() {
        return ivResultSize;
    }

    public void setIvResultSize(int ivResultSize) {
        this.ivResultSize = ivResultSize;
    }

    public int getIvResultColor() {
        return ivResultColor;
    }

    public void setIvResultColor(int ivResultColor) {
        this.ivResultColor = ivResultColor;
    }

    public String getIvResultText() {
        return ivResultText;
    }

    public void setIvResultText(String ivResultText) {
        this.ivResultText = ivResultText;
    }

    public String getTvResultText() {
        return tvResultText;
    }

    public void setTvResultText(String tvResultText) {
        this.tvResultText = tvResultText;
    }

    public String getTvResultDescText() {
        return tvResultDescText;
    }

    public void setTvResultDescText(String tvResultDescText) {
        this.tvResultDescText = tvResultDescText;
    }

    public String getTvGoPageText() {
        return tvGoPageText;
    }

    public void setTvGoPageText(String tvGoPageText) {
        this.tvGoPageText = tvGoPageText;
    }

    public String getTvGoPage() {
        return tvGoPage;
    }

    public void setTvGoPage(String tvGoPage) {
        this.tvGoPage = tvGoPage;
    }

    public Object getGoToObj() {
        return goToObj;
    }

    public void setGoToObj(Object goToObj) {
        this.goToObj = goToObj;
    }
}
