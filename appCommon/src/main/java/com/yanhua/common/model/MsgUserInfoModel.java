package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MsgUserInfoModel implements Serializable {

    private String img;         // 用户头像
    private String nickName;    // 用户昵称
    private String friendRemark;// 用户昵称
    private String userId;      // 用户ID
    private String serviceName; // 客服昵称
    private String storeId;     // 客服所属的店铺id
    private String shopName;    // 客服所属店铺名称

    private int serviceType;//客服类型（1机器人，2人工客服）
    private String accountNumber;//客服账号
    private String storeType;//商铺类型（0平台端，1商铺端）

    private boolean inBlackList;// 好友用户是否在黑名单
    private int autoLineUp;// 机器人关闭，人工客服忙碌情况下，1表示要自动转人工排队
    private int isAllServiceOutLine;// 所有客服不在线

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getServiceType() {
        return serviceType;
    }

    public void setServiceType(int serviceType) {
        this.serviceType = serviceType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public boolean isInBlackList() {
        return inBlackList;
    }

    public void setInBlackList(boolean inBlackList) {
        this.inBlackList = inBlackList;
    }

    public int getAutoLineUp() {
        return autoLineUp;
    }

    public void setAutoLineUp(int autoLineUp) {
        this.autoLineUp = autoLineUp;
    }

    public int isAllServiceOutLine() {
        return isAllServiceOutLine;
    }

    public void setAllServiceOutLine(int isAllServiceOutLine) {
        this.isAllServiceOutLine = isAllServiceOutLine;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
