package com.yanhua.common.model;

import java.io.Serializable;

public class ApplyCircleForm implements Serializable {

    private String circleDesc;
    private String coverUrl;
    private String notice;
    private String subtitle;
    private String title;

    public String getCircleDesc() {
        return circleDesc;
    }

    public void setCircleDesc(String circleDesc) {
        this.circleDesc = circleDesc;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
