package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class FeedbackForm implements Serializable {

    private String email;
    private String feedbackContent;
    private String feedbackTypeId;
    private String phone;
    private List<String> feedbackPhotoUrl;

}
