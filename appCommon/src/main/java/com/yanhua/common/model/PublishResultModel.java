package com.yanhua.common.model;

import java.io.Serializable;

public class PublishResultModel implements Serializable {

    private String id;
    private String userId;
    private String contentTitle;
    private Boolean isFabulous;
    private Boolean isFollow;
    private String createdTime;
    private String issueLongitude;
    private String issueLatitude;
    private String issueCity;
    private String issueAddress;
    private Integer viewCount;
    private Boolean isCollect;
    private Integer replyCount;
    private String content;
    private Integer auditStatus;
    private Integer videoTime;
    private Boolean isView;
    private String coverUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public Boolean getFabulous() {
        return isFabulous;
    }

    public void setFabulous(Boolean fabulous) {
        isFabulous = fabulous;
    }

    public Boolean getFollow() {
        return isFollow;
    }

    public void setFollow(Boolean follow) {
        isFollow = follow;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Boolean getCollect() {
        return isCollect;
    }

    public void setCollect(Boolean collect) {
        isCollect = collect;
    }

    public Integer getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Integer replyCount) {
        this.replyCount = replyCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(Integer videoTime) {
        this.videoTime = videoTime;
    }

    public Boolean getView() {
        return isView;
    }

    public void setView(Boolean view) {
        isView = view;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }
}
