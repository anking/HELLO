package com.yanhua.common.model;

import java.io.Serializable;

/**
 * 处理邀请或者创建群的时候
 */
public class MemberItem implements Serializable {

    /**
     * memberId : string
     * nickName : string
     * type : 0
     */

    private String memberId;
    private String nickName;
    private String imgHead;
    private int type;

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    private String friendRemark;
    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getType() {
        return type;
    }

    public String getImgHead() {
        return imgHead;
    }

    public void setImgHead(String imgHead) {
        this.imgHead = imgHead;
    }

    public void setType(int type) {
        this.type = type;
    }
}
