package com.yanhua.common.model;

import com.shuyu.textutillib.model.TopicModel;

import java.io.Serializable;

import lombok.Data;

@Data
public class HomeSearchResultModel implements Serializable {

    private int type;//类型(1:用户模块,2:圈子模块,3:话题模块,4:内容模块)
    private MomentListModel content;// 发现内容信息
    private CircleModel circle;// 此刻-圈子
    private TopicModel topic;// 此刻-话题
    private UserModel user;// 用户

}
