package com.yanhua.common.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class CommonReasonModel implements Serializable {

    private String content;
    private String createdTime;
    private String extendData;
    private List<CommonTypeModel> extendDataList;
    private String id;
    private String remarks;
    private Integer sort;
    private Integer state;
    private String updatedTime;
    private boolean selected;// Android业务使用，是否选中

}
