package com.yanhua.common.model;

import java.io.Serializable;

public class UpgradeInfoModel implements Serializable {

    /**
     * id : 1356180784647626754
     * serialNumber : 5
     * type : 0
     * versionName : 2.2.0
     * versionNo : 1
     * downloadUrl : http://fir.yuexinguoji.com/yqfr?release_id=60176b23f945482aae63b0d0
     * forcedUpdate : 0
     * status : 1
     * createdBy : system
     * updatedBy : system
     * createdTime : 2021-02-01 18:01:10
     * updatedTime : 2021-02-01 18:20:55
     */
    private String createdBy;
    private String createdTime;
    private String downloadUrl;
    private int forcedUpdate;
    private String id;
    private int serialNumber;
    private int status;
    private int type;
    private String updateContent;
    private String updateControlId;
    private String updatedBy;
    private String updatedTime;
    private String versionName;
    private int versionNo;

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public int getForcedUpdate() {
        return forcedUpdate;
    }

    public void setForcedUpdate(int forcedUpdate) {
        this.forcedUpdate = forcedUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent;
    }

    public String getUpdateControlId() {
        return updateControlId;
    }

    public void setUpdateControlId(String updateControlId) {
        this.updateControlId = updateControlId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }
}
