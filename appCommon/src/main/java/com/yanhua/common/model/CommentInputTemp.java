package com.yanhua.common.model;

public class CommentInputTemp {

    private static String nick;
    private static String content;

    public static String getNick() {
        return nick;
    }

    public static void setNick(String nick) {
        CommentInputTemp.nick = nick;
    }

    public static String getContent() {
        return content;
    }

    public static void setContent(String content) {
        CommentInputTemp.content = content;
    }
}
