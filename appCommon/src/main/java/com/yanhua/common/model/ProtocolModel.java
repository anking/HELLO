package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProtocolModel implements Serializable {

    private String id;
    private String title;
    private String code;
    private String content;
    private int forceState;
    private String typeId;
    private String typeName;
    private int version;


    /**
     * "id": "1516667324875186178",
     * 		"title": "图片上传规范",
     * 		"code": "userPicture",
     * 		"remarks": "",
     * 		"state": 1,
     * 		"content": "图片上传规范",
     "typeId": "string",
     "typeName": "string",
     "version": 0
     * 		"createdTime": "2022-04-20 14:37:23",
     * 		"updatedTime": "2022-04-20 14:37:23"
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getForceState() {
        return forceState;
    }

    public void setForceState(int forceState) {
        this.forceState = forceState;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
