package com.yanhua.common.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupsBean implements Comparable<GroupsBean>, Serializable {

    private String id;
    private String name;
    private String groupImg;
    private boolean isSelected;//用户选择

    private String namePinYin = "";   // pinyin
    private String firstLetter; // 拼音的首字母

    private ArrayList<GroupMemberInfo> groupMemberVoList;

    public ArrayList<GroupMemberInfo> getGroupMemberVoList() {
        return groupMemberVoList;
    }

    public void setGroupMemberVoList(ArrayList<GroupMemberInfo> groupMemberVoList) {
        this.groupMemberVoList = groupMemberVoList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupImg() {
        return groupImg;
    }

    public void setGroupImg(String groupImg) {
        this.groupImg = groupImg;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getNamePinYin() {
        return namePinYin;
    }

    public void setNamePinYin(String namePinYin) {
        this.namePinYin = namePinYin;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }


    @Override
    public boolean equals(Object o) {
        if (o != null) {
            GroupsBean info = (GroupsBean) o;
            return (getId() != null && getId().equals(info.getId()));
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@NonNull GroupsBean group) {
        if (firstLetter.equals("#") && !group.firstLetter.equals("#")) {
            return 1;
        } else if (!firstLetter.equals("#") && group.firstLetter.equals("#")) {
            return -1;
        } else {
            return firstLetter.compareTo(group.firstLetter);
        }
    }
}
