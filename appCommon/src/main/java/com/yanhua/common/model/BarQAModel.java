package com.yanhua.common.model;

import com.yanhua.base.model.ListResult;

import java.io.Serializable;

import lombok.Data;

@Data
public class BarQAModel implements Serializable {
    /**
     * askedDesc	string
     * 回答信息内容
     * <p>
     * askedId	string
     * 回答id
     * <p>
     * askedNum	integer($int32)
     * 全部回答数
     * <p>
     * browseNum	integer($int32)
     * 全部浏览量
     * <p>
     * createdTime	string($date-time)
     * 创建日期
     * <p>
     * parentAskedDesc	string
     * 上级提问信息内容
     * <p>
     * parentAskedId	string
     * 上级提问id
     */
    private String askedDesc;
    private String askedId;
    private String parentId;
    private int askedNum;
    private int browseNum;
    private int barBrowseCount;
    private String createdTime;
    private String parentAskedDesc;
    private String parentAskedId;// 上级提问id
    private int parentIsFollow;// 提问用户关注(0: 未关注, 1: 关注)
    private int parentIsShow;// 是否显示(0: 显示, 1: 匿名)
    private int isShow;
    private String barId;
    private String barImg;
    private String barName;
    private String parentAccountName;
    private String parentImg;
    private String parentUserId;// 提问账号id
    private String userName;
    private String userId;
    private ListResult<BarAnswerModel> appBarAnswerVo;

}
