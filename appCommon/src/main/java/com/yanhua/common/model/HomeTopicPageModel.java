package com.yanhua.common.model;

import com.shuyu.textutillib.model.TopicModel;

import java.io.Serializable;
import java.util.List;

public class HomeTopicPageModel implements Serializable {
    //
    private List<TopicModel> topicList;
    private int page;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<TopicModel> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<TopicModel> topicList) {
        this.topicList = topicList;
    }
}
