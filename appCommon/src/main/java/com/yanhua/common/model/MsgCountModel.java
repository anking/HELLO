package com.yanhua.common.model;

public class MsgCountModel {
    private int careCount;      // 关注数量
    private int collectCount;   // 收藏数量
    private int commentCount;   // 评论数量
    private int fabulosCount;   // 点赞数量

    public int getCareCount() {
        return careCount;
    }

    public void setCareCount(int careCount) {
        this.careCount = careCount;
    }

    public int getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(int collectCount) {
        this.collectCount = collectCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getFabulosCount() {
        return fabulosCount;
    }

    public void setFabulosCount(int fabulosCount) {
        this.fabulosCount = fabulosCount;
    }
}
