package com.yanhua.common.model;

import java.io.Serializable;

public class FileResult implements Serializable {

    private String id;
    private String fileType;
//    private String filePath;
//    private String fileToken;
    private String httpUrl;
    private String width;
    private String height;

    private int httpUrlType;

    public int getHttpUrlType() {
        return httpUrlType;
    }

    public void setHttpUrlType(int httpUrlType) {
        this.httpUrlType = httpUrlType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

//    public String getFilePath() {
//        return filePath;
//    }
//
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
//
//    public String getFileToken() {
//        return fileToken;
//    }
//
//    public void setFileToken(String fileToken) {
//        this.fileToken = fileToken;
//    }

    public String getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
