package com.yanhua.common.model;

import java.io.Serializable;

public class NewsTypeModel implements Serializable {
    private int appShowStatus;//app显示状态(0:不显示 1;显示)
    private String appShowStatusName;//app显示状态(0:不显示 1;显示)
    private int contentSum;//内容数量

    private String reportName	;//分类名称
    private int reportStatus	;//分类状态(0:禁用 1;启用)
    private String reportStatusName	;// 分类状态(0:禁用 1;启用)
    private String logo ;//"logo": "http://thirdqq.qlogo.cn/g?b=oidb&k=pw50RSDZCyiaGYxujBOTUSg&s=100&t=1556617756"
    private String id;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getAppShowStatus() {
        return appShowStatus;
    }

    public void setAppShowStatus(int appShowStatus) {
        this.appShowStatus = appShowStatus;
    }

    public String getAppShowStatusName() {
        return appShowStatusName;
    }

    public void setAppShowStatusName(String appShowStatusName) {
        this.appShowStatusName = appShowStatusName;
    }

    public int getContentSum() {
        return contentSum;
    }

    public void setContentSum(int contentSum) {
        this.contentSum = contentSum;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public int getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(int reportStatus) {
        this.reportStatus = reportStatus;
    }

    public String getReportStatusName() {
        return reportStatusName;
    }

    public void setReportStatusName(String reportStatusName) {
        this.reportStatusName = reportStatusName;
    }
}
