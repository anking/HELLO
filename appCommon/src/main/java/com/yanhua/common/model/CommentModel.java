package com.yanhua.common.model;

import java.util.List;

/**
 * 评论模块
 *
 * @author Administrator
 */
public class CommentModel {
    /**
     * childrens : []
     * commentDesc : string
     * commentId : string
     * contentId : string
     * createdTime : 2020-12-12T09:09:53.998Z
     * fabulousCount : 0
     * id : string
     * nickName : string
     * parentCommentId : string
     * userId : string
     * userPhoto : string
     * isFabulous: boolean
     * commentUserId : string
     * isFabulous : true
     * replyName : string,
     * childCount: int
     */
    private int resultType;//结果类型(1:内容,2:广告)
    private List<AdvertiseModel> advertisingList;
    private int childCount;
    private String commentDesc;
    private String commentId;
    private String contentId;
    private String createdTime;
    private int fabulousCount;
    private String id;
    private String nickName;
    private int parentChildCount;

    private int childType;//1:first 2:last 3:only one 0:其他

    private int worseCount;
    private boolean isWorse;

    public int getChildType() {
        return childType;
    }

    public void setChildType(int childType) {
        this.childType = childType;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }

    public List<AdvertiseModel> getAdvertisingList() {
        return advertisingList;
    }

    public void setAdvertisingList(List<AdvertiseModel> advertisingList) {
        this.advertisingList = advertisingList;
    }

    public boolean isFabulous() {
        return isFabulous;
    }

    public void setFabulous(boolean fabulous) {
        isFabulous = fabulous;
    }

    public int getParentChildCount() {
        return parentChildCount;
    }

    public void setParentChildCount(int parentChildCount) {
        this.parentChildCount = parentChildCount;
    }

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    private String friendRemark;
    private String parentCommentId;
    private String userId;
    private String userPhoto;
    private List<CommentModel> childrens;
    private String commentUserId;
    private boolean isFabulous;
    private String replyName;
    private int commentAudit;//后台开启评论审核  0开启了 1关闭了

//    ApiModelProperty("回复类型  1回复评论  2回复用户")
    private int replyType = 1;

    public int getReplyType() {
        return replyType;
    }

    public void setReplyType(int replyType) {
        this.replyType = replyType;
    }

    //回复状态 0-评论内容 1-回复评论
    private int replyStatus;

    public int getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(int replyStatus) {
        this.replyStatus = replyStatus;
    }

    // 评论条目的类型  0是父评论 1是子评论 2是可以显示加载更多的子评论
    private int type;

    // 子评论在父评论列表的索引
    private int groupPosition;

    //子评论组的id
    private String groupCommentId;

    public String getGroupCommentId() {
        return groupCommentId;
    }

    public void setGroupCommentId(String groupCommentId) {
        this.groupCommentId = groupCommentId;
    }

    public int getGroupPosition() {
        return groupPosition;
    }

    public void setGroupPosition(int groupPosition) {
        this.groupPosition = groupPosition;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public int getFabulousCount() {
        return fabulousCount;
    }

    public void setFabulousCount(int fabulousCount) {
        this.fabulousCount = fabulousCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(String parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public List<CommentModel> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<CommentModel> childrens) {
        this.childrens = childrens;
    }

    public String getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    public boolean isIsFabulous() {
        return isFabulous;
    }

    public void setIsFabulous(boolean isFabulous) {
        this.isFabulous = isFabulous;
    }

    public String getReplyName() {
        return replyName;
    }

    public void setReplyName(String replyName) {
        this.replyName = replyName;
    }

    public int getCommentAudit() {
        return commentAudit;
    }

    public void setCommentAudit(int commentAudit) {
        this.commentAudit = commentAudit;
    }

    public int getWorseCount() {
        return worseCount;
    }

    public void setWorseCount(int worseCount) {
        this.worseCount = worseCount;
    }

    public boolean isWorse() {
        return isWorse;
    }

    public void setWorse(boolean worse) {
        isWorse = worse;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "id='" + id + '\'' +
                ", groupPosition=" + groupPosition +
                '}';
    }
}
