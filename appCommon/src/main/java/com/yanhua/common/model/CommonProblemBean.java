package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class CommonProblemBean implements Serializable {

    private String createdTime;
    private String id;
    private String typeId;
    private String content;
    private int state;
    private String title;
    private String updatedTime;

}
