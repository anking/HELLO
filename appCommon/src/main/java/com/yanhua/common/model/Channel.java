package com.yanhua.common.model;

import java.io.Serializable;
import java.util.Objects;

public class Channel implements Serializable {
// 一般 id

    private String id;
    private String circleId;
    private String tipicId;
    private String remarks;
    private String reportName;
    private int type;

//    public Channel(String id, String remarks, String reportName, int sort) {
//        this.id = id;
//        this.remarks = remarks;
//        this.reportName = reportName;
//    }

    public Channel(String id, String remarks, String reportName, int type) {
        this.id = id;
        this.remarks = remarks;
        this.reportName = reportName;
        this.type = type;
    }
    public Channel(String id, String objId, boolean topic, String remarks, String reportName, int type) {
        this.id = id;
        if (topic) {
            this.tipicId = objId;
        } else {

            this.circleId = objId;
        }
        this.remarks = remarks;
        this.reportName = reportName;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getTipicId() {
        return tipicId;
    }

    public void setTipicId(String tipicId) {
        this.tipicId = tipicId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel that = (Channel) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
