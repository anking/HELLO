package com.yanhua.common.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MyQAModel implements Serializable {

    private String id;
    private String userId;
    private String accountName;
    private String img;
    private String barId;
    private String barName;
    private String barImg;
    private int barBrowseCount;
    private String parentAskedId;
    private String parentAskedDesc;// 提问
    private String askedId;
    private int replyStatus;// 0提问 1回答
    private String askedDesc;// 回答
    private int askedAudit;// 审核状态(-1:驳回 0:待审核 1:审核通过 2:隐藏 3:删除 )
    private int askedNum;
    private int browseCount;
    private int worseCount;
    private int fabulousCount;
    private int fabulousStatus;
    private int worseStatus;
    private String createdTime;
    private int parentIsShow;
    private int isShow;// 回答者的匿名状态
    private int isFollow;// 对回答者的关注状态

}
