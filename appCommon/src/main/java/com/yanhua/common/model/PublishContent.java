package com.yanhua.common.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PublishContent implements Serializable {


    /**
     * content : string
     * contentCategoryId : string
     * contentTitle : string
     * contentType : 0
     * contentUrl : string
     * fileIds : string
     * issueAddress : string
     * issueCity : string
     * issueLatitude : string
     * issueLongitude : string
     * videoTime:0
     */

    private String content;
    private String contentCategoryId;
    private String contentTitle;

    //内容包含类型 1:图片 2:视频
    private int contentType;

    // 内容隐私类型 默认公开(0公开 1朋友 2私密) --- 公开(所有人可见) 朋友(互相关注好友可见) 私密(仅自己可见)
    private int privacyType;

    //发布位置
    private String issueAddress;
    private String issueAddressDetail;
    private String issueCity;
    private String issueLatitude;
    private String issueLongitude;
    private ArrayList<String> filePahtList ;
    private ArrayList<String> namesList ;
    private ArrayList<String> userIdList ;
    private String videoCoverPath;


    private ArrayList<String> topicSaveFormList;
    private ArrayList<String> topicIdList;//
    private ArrayList<String> circleIdList;

    public ArrayList<String> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(ArrayList<String> userIdList) {
        this.userIdList = userIdList;
    }

    public ArrayList<String> getTopicSaveFormList() {
        return topicSaveFormList;
    }

    public void setTopicSaveFormList(ArrayList<String> topicSaveFormList) {
        this.topicSaveFormList = topicSaveFormList;
    }

    public ArrayList<String> getTopicIdList() {
        return topicIdList;
    }

    public void setTopicIdList(ArrayList<String> topicIdList) {
        this.topicIdList = topicIdList;
    }

    public ArrayList<String> getCircleIdList() {
        return circleIdList;
    }

    public void setCircleIdList(ArrayList<String> circleIdList) {
        this.circleIdList = circleIdList;
    }

    public String getVideoCoverPath() {
        return videoCoverPath;
    }

    public void setVideoCoverPath(String videoCoverPath) {
        this.videoCoverPath = videoCoverPath;
    }

    public ArrayList<String> getNamesList() {
        return namesList;
    }

    public void setNamesList(ArrayList<String> namesList) {
        this.namesList = namesList;
    }

    public ArrayList<String> getFilePahtList() {
        return filePahtList;
    }

    public void setFilePahtList(ArrayList<String> filePahtList) {
        this.filePahtList = filePahtList;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    private int videoTime;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCategoryId() {
        return contentCategoryId;
    }

    public void setContentCategoryId(String contentCategoryId) {
        this.contentCategoryId = contentCategoryId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public String getIssueAddress() {
        return issueAddress;
    }

    public void setIssueAddress(String issueAddress) {
        this.issueAddress = issueAddress;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public String getIssueLatitude() {
        return issueLatitude;
    }

    public void setIssueLatitude(String issueLatitude) {
        this.issueLatitude = issueLatitude;
    }

    public String getIssueLongitude() {
        return issueLongitude;
    }

    public void setIssueLongitude(String issueLongitude) {
        this.issueLongitude = issueLongitude;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public String getIssueAddressDetail() {
        return issueAddressDetail;
    }

    public void setIssueAddressDetail(String issueAddressDetail) {
        this.issueAddressDetail = issueAddressDetail;
    }
}
