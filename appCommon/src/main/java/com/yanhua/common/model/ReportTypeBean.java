package com.yanhua.common.model;

import java.io.Serializable;

/**
 * 举报类型实体
 *
 * @author Administrator
 */
public class ReportTypeBean implements Serializable {
    /*{
        "id": "1522777699232305153",
            "sort": 1,
            "content": "举报原因001",
            "remarks": "举报原因001",
            "state": 1,
            "extendData": "[{\"id\":\"1\",\"name\":\"此刻\"},{\"id\":\"2\",\"name\":\"约伴\"}]",
            "extendDataList": [
        {
            "id": "1",
                "name": "此刻"
        },
        {
            "id": "2",
                "name": "约伴"
        }
      ],
        "createdTime": "2022-05-07 11:17:50",
            "updatedTime": "2022-05-07 11:17:50"
    }*/

        /*{
            "content": "举报原因001",
            "remarks": "举报原因001",

    }*/

    private String id;
    private int sort;
    private int state;
    private String remarks;
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
