package com.yanhua.common.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * 群成员信息
 */
public class DiscoGroupMember implements Comparable<DiscoGroupMember>, Serializable {

    /**
     * id : 1387693772958523393
     * createdBy : 13265033776
     * createdTime : 2021-04-29T17:02:32.000+0800
     * updatedBy : 17876565237
     * updatedTime : 2021-05-08T18:21:16.000+0800
     * groupId : 20210429142785362858872832
     * groupName : 我韩信贼6四群
     * memberId : 1379613938000850945
     * nickName : 出发吧
     * memberStatus : 0
     * gender : 1
     * type : 1
     * mobile : 13265033776
     * joinTime : 2021-04-29T17:02:32.000+0800
     * userImg : http://oss.yuexinguoji.com/default/image/2021/04/fbdf27da0a3746e983077e63eff13a8a.png
     */

    private String id;
    private String createdBy;
    private String createdTime;
    private String updatedBy;
    private String updatedTime;
    private String groupId;
    private String groupName;
    private String memberId;
    private String nickName;

    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }

    private String friendRemark;
    private int memberStatus;
    private int gender;
    private int type;
    private String mobile;
    private String joinTime;
    private String userImg;

    public int getShowGroupName() {
        return showGroupName;
    }

    public void setShowGroupName(int showGroupName) {
        this.showGroupName = showGroupName;
    }

    private int showGroupName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(int memberStatus) {
        this.memberStatus = memberStatus;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            DiscoGroupMember member = (DiscoGroupMember) o;
            return (getMemberId() != null && getMemberId().equals(member.getMemberId()));
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@NonNull DiscoGroupMember member) {
        return Integer.valueOf(this.getType()).compareTo(Integer.valueOf(member.getType()));
    }
}
