package com.yanhua.common.presenter;

import com.google.gson.Gson;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.presenter.contract.CommonAppContract;

import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;


public class CommonAppPresenter extends MvpPresenter<CommonAppContract.IView> implements CommonAppContract.Presenter {
    public void getUserFriend(String userId,String nickName ,int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getFriendUserList(new SimpleSubscriber<HttpResult<ListResult<FriendModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<FriendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<FriendModel> listResult = result.getData();
                        baseView.handleUserListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId,nickName, current, size));
    }

    public void getUserGroups(String userId, String likeName, boolean isSearch) {
        addSubscribe(MessageHttpMethod.getInstance().getUserGroups(new SimpleSubscriber<HttpResult<List<GroupsBean>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<GroupsBean>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        List<GroupsBean> list = result.getData();

                        baseView.handleUserGroupsBeanSuccess(list, isSearch);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId, likeName));
    }

    @Override
    public void getCommentList(String contentId, int current, int childSize) {
        addSubscribe(CommonHttpMethod.getInstance().getCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        int totalSum = result.getData().getTotal();
                        baseView.handleCommentList(listResult.getRecords(), totalSum);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, contentId, current, childSize));
    }

    @Override
    public void publishComment(PublishCommentForm form, int type) {
        if (null != form) {
            form.setType(type);
        }

        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(CommonHttpMethod.getInstance().publishComment(new SimpleSubscriber<HttpResult<CommentModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<CommentModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handlePublishCommentSuccess(result.getData());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, body));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void updateStarComment(String id) {
        addSubscribe(CommonHttpMethod.getInstance().starComment(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarCommentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    @Override
    public void getChildCommentList(String commentId, int current, int size) {
        addSubscribe(CommonHttpMethod.getInstance().getChildCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        baseView.handleChildCommentList(listResult);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, commentId, current, size));
    }

    public void getTopicList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getTopicList(new SimpleSubscriber<HttpResult<ListResult<TopicModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<TopicModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<TopicModel> listResult = result.getData();
                        baseView.handleTopicList(listResult);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }

    public void deleteContent(String id,int pos) {
        addSubscribe(CommonHttpMethod.getInstance().deleteCommentItem(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteCommentSuccess(pos);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void dealCommentItem2Top(String commentId,int type,int pos) {
        addSubscribe(CommonHttpMethod.getInstance().dealCommentItem2Top(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDealItemTopSuccess(pos);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        },type, commentId));
    }

    @Override
    public void getContentUserDetail(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().getContentUserDetail(new SimpleSubscriber<HttpResult<ContentUserInfoModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ContentUserInfoModel> result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ContentUserInfoModel model = result.getData();
                        baseView.handleContentUserDetailSuccess(model);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }


    @Override
    public void getUserDetail(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().getUserDetail(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        UserInfo userInfo = bean.getData();
                        if (userInfo != null) {
                            baseView.handleUserDetail(userInfo);
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, userId));
    }

    public void getDiscoContentDetail(boolean isFirst, String id) {
        addSubscribe(MomentHttpMethod.getInstance().getDiscoContentDetail(new SimpleSubscriber<HttpResult<MomentListModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MomentListModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    MomentListModel model = result.getData();
                    baseView.handleContentDetailSuccess(isFirst, model);
                } else {
                    baseView.handleContentErrorMsg(isFirst, result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void getContentDetail(boolean isFirst, String id) {
        addSubscribe(MomentHttpMethod.getInstance().getContentDetail(new SimpleSubscriber<HttpResult<MomentListModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MomentListModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    MomentListModel model = result.getData();
                    baseView.handleContentDetailSuccess(isFirst, model);
                } else {
                    baseView.handleContentErrorMsg(isFirst, result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void getContentDetail(String id, int i) {
        addSubscribe(MomentHttpMethod.getInstance().getContentDetail(new SimpleSubscriber<HttpResult<MomentListModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MomentListModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    MomentListModel model = result.getData();
                    baseView.handleSuccessContentDetail(model, i);
                } else {
                    baseView.handleContentErrorMsg(false, result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void getContentList(HashMap<String, Object> params) {
        addSubscribe(MomentHttpMethod.getInstance().getContentList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleContentListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, params));
    }

    @Override
    public void getContentList(String title, int type, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getMomentPageList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleContentListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, type, current, size));
    }


    @Override
    public void getUserContent(int contentType, String userId, int current) {
        addSubscribe(MomentHttpMethod.getInstance().getUserContentList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleContentListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, contentType, userId, current));
    }

    @Override
    public void addViewCount(int type, String id) {
        addSubscribe(CommonHttpMethod.getInstance().addViewCount(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, type, id));
    }

    @Override
    public void updatePrivacyType(HashMap<String, Object> params) {
        addSubscribe(MomentHttpMethod.getInstance().updatePrivacyTyoe(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessUpdatePrivacy();
                } else {
                    baseView.handleErrorMsg(httpResult.getMesg());
                }
            }
        }, params));
    }

}
