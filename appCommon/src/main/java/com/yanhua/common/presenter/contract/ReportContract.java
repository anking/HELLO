package com.yanhua.common.presenter.contract;

import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ReportForm;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.model.StsTokenModel;

import java.util.List;

public interface ReportContract {

    interface IView extends BaseView {

        void getReportReasonListSuccess(List<ReportTypeBean> list);

        void reportSuccess();

        default void handleStsToken(StsTokenModel model) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        void getReportReasonList(int  exID);

        void reportContent(ReportForm form);

        void getStsToken();
    }
}
