package com.yanhua.common.presenter.contract;


import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;

import java.util.HashMap;
import java.util.List;

public interface CommonAppContract {
    interface IView extends BaseView {
        default void handleUserListSuccess(List<FriendModel> list) {
        }
        default void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean isSearch){}
        default void handleDeleteCommentSuccess(int pos) {
        }

        default void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        }

        default void handleUserDetail(UserInfo userInfo) {
        }

        default void handleContentDetailSuccess(boolean isFirst, MomentListModel model) {
        }

        default void handleSuccessContentDetail(MomentListModel model, int i) {

        }

        default void handleContentErrorMsg(boolean isFirst, String msg) {
        }

        default void handleErrorMsg(String msg) {
        }

        default void handleCommentList(List<CommentModel> list, int totalSum) {
        }

        default void handleChildCommentList(ListResult<CommentModel> listResult) {
        }

        default void handlePublishCommentSuccess(CommentModel model) {
        }

        default void updateCollectContentSuccess() {
        }

        default void updateStarContentSuccess() {
        }

        default void updateStarCommentSuccess() {
        }

        default void updateFollowUserSuccess() {
        }

        default void handleContentListSuccess(List<MomentListModel> list) {
        }

        default void handleSuccessUpdatePrivacy() {

        }

        default void handleTopicList(ListResult<TopicModel> listResult) {
        }

        default void handleDealItemTopSuccess(int pos) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        void getCommentList(String contentId, int current, int childSize);

        void publishComment(PublishCommentForm form, int type);

        void updateCollectContent(String id, int type);

        void updateStarContent(String id, int type);

        void updateStarComment(String id);

        void getChildCommentList(String commentId, int current, int size);

        void getContentList(String title, int type, int current, int size);

        void getUserDetail(String userId);

        void deleteContent(String id, int pos);

        void getContentUserDetail(String userId);

        void getContentDetail(boolean isFirst, String id);

        void getContentDetail(String id, int i);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void getContentList(HashMap<String, Object> params);

        void getUserContent(int contentType, String userId, int current);

        void addViewCount(int type, String id);

        void updatePrivacyType(HashMap<String, Object> params);
    }
}
