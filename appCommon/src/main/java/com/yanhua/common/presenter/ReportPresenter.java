package com.yanhua.common.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.ReportForm;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.presenter.contract.ReportContract;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ReportPresenter extends MvpPresenter<ReportContract.IView> implements ReportContract.Presenter {

    @Override
    public void getReportReasonList(int extendId) {
        addSubscribe(CommonHttpMethod.getInstance().getReportReasonList(new SimpleSubscriber<HttpResult<List<ReportTypeBean>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<List<ReportTypeBean>> result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.getReportReasonListSuccess(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, extendId));
    }

    @Override
    public void reportContent(ReportForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(CommonHttpMethod.getInstance().reportContent(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.reportSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, body));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }
}
