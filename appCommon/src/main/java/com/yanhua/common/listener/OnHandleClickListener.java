package com.yanhua.common.listener;


public interface OnHandleClickListener<T> {
    int REJECT = 1;//驳回
    int COLLECT = 2;//收藏
    int COMMENT = 3;//评论
    int THUMB_UP = 4;//点赞
    int TO_DETAIL = 0;//详情
    int MORE_ACTION = 5;//更多
    int FOLLOW = 6;//关注
    int DELETE = 7;//删除


    void onHandleClick(int type, T item, int pos);
}