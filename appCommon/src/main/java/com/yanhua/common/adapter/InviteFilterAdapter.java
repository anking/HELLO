package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.InviteTypeModel;

import java.util.ArrayList;

import butterknife.BindView;

public class InviteFilterAdapter extends BaseRecyclerAdapter<InviteTypeModel, InviteFilterAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<InviteTypeModel> mFilterList;

    public InviteFilterAdapter(Context context, ArrayList<InviteTypeModel> filterList) {
        super(context);
        this.mContext = context;
        this.mFilterList = filterList;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite_type_filter, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InviteTypeModel item) {
        String name = item.getName();

        holder.tvTitle.setText(name);
        if (mFilterList.contains(item)) {
            holder.llItem.setSelected(true);
        } else {
            holder.llItem.setSelected(false);
        }
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_title)
        TextView tvTitle;

        @BindView(R2.id.ll_item)
        LinearLayout llItem;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
