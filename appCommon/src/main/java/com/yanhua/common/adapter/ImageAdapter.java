package com.yanhua.common.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AliIconFontTextView;

import butterknife.BindView;

public class ImageAdapter extends BaseRecyclerAdapter<String, ImageAdapter.ViewHolder> {

    private Context context;
    private OnDeleteImageListen mListener;
    private boolean onlyShow;
    private int col = 3;
    private final static float divide = 2.5f;
    private int paadding = 34;

    public ImageAdapter(Context context, OnDeleteImageListen listener) {
        super(context);
        this.context = context;

        this.mListener = listener;
    }

    public ImageAdapter(Context context, int c, int paadding, OnDeleteImageListen listener) {
        super(context);
        this.context = context;
        this.col = c;
        this.paadding = paadding;
        this.mListener = listener;
    }

    public ImageAdapter(Context context, boolean onlyShow, OnDeleteImageListen listener) {
        super(context);
        this.context = context;
        this.onlyShow = onlyShow;
        this.mListener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_image, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {

        int pos = getPosition(holder);

        int itemWidth = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, paadding) - (col - 1) * 2 * DisplayUtils.dip2px(context, divide);//设置图片的宽
        //24+5X2+
        int width = itemWidth / col;

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.upload.getLayoutParams();
//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);

        layoutParams.width = width;
        layoutParams.height = width;

        holder.rlItem.setLayoutParams(layoutParams);
        if (TextUtils.isEmpty(item)) {
            holder.iconDelete.setVisibility(View.GONE);
            holder.upload.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);
        } else {
            ImageLoaderUtil.loadImgFillCenter(context, holder.imageView, item, 0);
            holder.iconDelete.setVisibility(onlyShow ? View.GONE : View.VISIBLE);
            holder.upload.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);
        }

        holder.iconDelete.setOnClickListener(v -> mListener.onDeleteImage(pos));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_pic)
        ImageView imageView;
        @BindView(R2.id.iconDelete)
        AliIconFontTextView iconDelete;

        @BindView(R2.id.upload)
        FrameLayout upload;

        @BindView(R2.id.rlItem)
        RelativeLayout rlItem;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
