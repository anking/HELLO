package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.core.util.YHStringUtils;

import butterknife.BindView;

public class PickTopicListAdapter extends BaseRecyclerAdapter<TopicModel, PickTopicListAdapter.ViewHolder> {
    public PickTopicListAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_pick_topic_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull TopicModel item) {
        int position = getPosition(holder);

        holder.tvTopicName.setText(YHStringUtils.formatTopic(item.getTitle()));
        holder.tvTopicNum.setText(YHStringUtils.quantityFormat(item.getDynamicCount()) + "内容");
    }


    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvTopicName)
        TextView tvTopicName;

        @BindView(R2.id.tvTopicNum)
        TextView tvTopicNum;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
