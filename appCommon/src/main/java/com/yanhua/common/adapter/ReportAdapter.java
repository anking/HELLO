package com.yanhua.common.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.ReportTypeBean;

import butterknife.BindView;

/**
 * 举报列表适配器
 *
 * @author Administrator
 */
public class ReportAdapter extends BaseRecyclerAdapter<ReportTypeBean, ReportAdapter.ViewHolder> {

    public ReportAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_report, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull ReportTypeBean item) {
        String reportName = item.getContent();
        holder.tvTitle.setText(!TextUtils.isEmpty(reportName) ? reportName : "");
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tv_title)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
