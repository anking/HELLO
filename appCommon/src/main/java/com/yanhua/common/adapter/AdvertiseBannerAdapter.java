package com.yanhua.common.adapter;

import android.content.Context;
import android.text.TextUtils;

import com.yanhua.common.R;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;

import java.util.List;

/**
 * 公共的广告轮播适配器
 */
public class AdvertiseBannerAdapter extends BannerImageAdapter<AdvertiseModel> {
    private Context mContext;

    public AdvertiseBannerAdapter(List<AdvertiseModel> mData, Context context) {
        super(mData);
        mContext = context;
    }

    @Override
    public void onBindView(BannerImageHolder holder, AdvertiseModel model, int position, int size) {

        String imgUrl = model.getImageUrl();
        String videoCoverUrl = model.getVideoCoverUrl();
        String advertisingImgUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

        if (!TextUtils.isEmpty(advertisingImgUrl)) {
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.imageView, advertisingImgUrl, R.drawable.place_holder, 0);
        } else {
            holder.imageView.setImageResource(R.drawable.place_holder);
        }
    }
}
