package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.view.CircleImageView;

import butterknife.BindView;


public class RongRecentListAdapter extends BaseRecyclerAdapter<ShareObjectModel, RongRecentListAdapter.ViewHolder> {

    private boolean multiSelect;

    public RongRecentListAdapter(Context context) {
        super(context);
    }

    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_recent_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull ShareObjectModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getImg(), R.drawable.place_holder);
        holder.tvName.setText(item.getNickName());


        if (multiSelect) {
            holder.cbItem.setVisibility(View.VISIBLE);
            boolean select = item.isSelected();
            if (select) {
                holder.cbItem.setTextColor(context.getResources().getColor(R.color.theme));
                holder.cbItem.setBackgroundResource(R.drawable.shape_bg_r_white);
            } else {
                holder.cbItem.setTextColor(context.getResources().getColor(R.color.white));
                holder.cbItem.setBackgroundResource(R.drawable.shape_bg_stroke2_r_gray);
            }
        } else {
            holder.cbItem.setVisibility(View.GONE);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.cb_item)
        ITCheckBox cbItem;
        @BindView(R2.id.tv_friend_name)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
