package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.core.util.YHStringUtils;

import butterknife.BindView;

/**
 * 列表适配器
 *
 * @author Administrator
 */
public class SelectTagAdapter extends BaseRecyclerAdapter<NewsTypeModel, SelectTagAdapter.ViewHolder> {

    private int mSlectStrategyTypePos;

    public SelectTagAdapter(Context context) {
        super(context);
    }

    public void setSelect(int select) {
        mSlectStrategyTypePos = select;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_select_type, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull NewsTypeModel item) {
        int pos = getPosition(holder);

        String name = item.getReportName();
        holder.tvTitle.setText(YHStringUtils.value(name));

        boolean isSelect = false;
        if (mSlectStrategyTypePos == pos) {
            isSelect = true;
        }
        holder.tvTitle.setSelected(isSelect);

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvTitle)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
