package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.YXImageViewerPopup;
import com.youth.banner.adapter.BannerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 默认实现的图片适配器，图片加载需要自己实现
 */
public class YXDiscoverImageAdapter extends BannerAdapter<String, YXDiscoverImageAdapter.YXDiscoverHolder> {

    private Context mContext;
    private List<String> bannerList;
    private boolean isClickJupm;

    private OnItemClickEventListener listener;

    public YXDiscoverImageAdapter(List<String> datas, Context context) {
        super(datas);

        this.bannerList = datas;
        this.mContext = context;
    }

    public YXDiscoverImageAdapter(List<String> datas, Context context, boolean isClickJupm, OnItemClickEventListener listener) {
        super(datas);

        this.bannerList = datas;
        this.mContext = context;

        this.isClickJupm = isClickJupm;

        this.listener = listener;
    }


    @Override
    public YXDiscoverHolder onCreateHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_discover_baner, parent, false);
        return new YXDiscoverHolder(itemView);
    }

    @Override
    public void onBindView(YXDiscoverHolder holder, String data, int position, int size) {
        Glide.with(mContext)
                .load(data)
                .thumbnail(Glide.with(mContext).load(data + YXConfig.IMAGE_RESIZE))
                .apply(new RequestOptions()
                        .centerInside()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))

                .placeholder(R.mipmap.bg_bluer)
//                                            .override((int) showWidth, (int) showHeight)
                .into(holder.imageView);


        holder.imageView.setOnClickListener(view -> {
            if (isClickJupm) {
                if (null != this.listener) {
                    this.listener.onItemClick();
                }
            } else {

                //srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
                                        /*new XPopup.Builder(mContext).asImageViewer(holder.imageView, position, urlList, (popupView, position1) -> {

                                        }, new SmartGlideImageLoader()).show();*/
                List<Object> urlList = new ArrayList<>();
                for (String url : bannerList) {
                    urlList.add(url);
                }


                //自定义的弹窗需要用asCustom来显示，之前的asImageViewer这些方法当然不能用了。
                YXImageViewerPopup viewerPopup = new YXImageViewerPopup(mContext, UserManager.getInstance().getUserId());
                //自定义的ImageViewer弹窗需要自己手动设置相应的属性，必须设置的有srcView，url和imageLoader。
//                                        viewerPopup.setSingleSrcView(holder.imageView, urlList);
                viewerPopup.setImageUrls(urlList);
                viewerPopup.setSrcView(holder.imageView, position);
                viewerPopup.setXPopupImageLoader(new SmartGlideImageLoader());
                viewerPopup.setSrcViewUpdateListener((popupView, position1) -> {
//                    popupView.updateSrcView(holder.imageView);
                });
                new XPopup.Builder(mContext).asCustom(viewerPopup).show();
            }
        });
    }


    static class YXDiscoverHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public YXDiscoverHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_discover);
        }
    }


    public interface OnItemClickEventListener {
        void onItemClick();
    }
}
