package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.core.util.YHStringUtils;

import butterknife.BindView;

/**
 * 列表适配器
 *
 * @author Administrator
 */
public class KeyAdapter extends BaseRecyclerAdapter<String, KeyAdapter.ViewHolder> {

    private OnDeleteImageListen mListener;
    private String mSelect;

    public KeyAdapter(Context context, OnDeleteImageListen listener) {
        super(context);

        mListener = listener;
    }

    public void setSelect(String select) {
        mSelect = select;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_key, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        int pos = getPosition(holder);

        holder.tvTitle.setText(YHStringUtils.value(item));
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvTitle)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
