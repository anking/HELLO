package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.view.CircleImageView;

import butterknife.BindView;

public class ShareFriendItemAdapter extends BaseRecyclerAdapter<ShareObjectModel, ShareFriendItemAdapter.ContractsHolder> {
    private Context mContext;


    private boolean multiSelect;


    public void setMultiSelect(boolean multiSelect) {
        this.multiSelect = multiSelect;

        notifyDataSetChanged();
    }


    public ShareFriendItemAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @NonNull
    @Override
    protected ContractsHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ContractsHolder(inflater.inflate(R.layout.item_share_friend, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ContractsHolder holder, @NonNull ShareObjectModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getImg(), R.drawable.place_holder);

        holder.tvName.setText(item.getNickName());


        if (multiSelect) {
            holder.cbItem.setVisibility(View.VISIBLE);
            boolean select = item.isSelected();
            if (select) {
                holder.cbItem.setTextColor(context.getResources().getColor(R.color.theme));
                holder.cbItem.setBackgroundResource(R.drawable.bg_tran_circle_no_stroke);
            } else {
                holder.cbItem.setTextColor(context.getResources().getColor(R.color.white));
                holder.cbItem.setBackgroundResource(R.drawable.shape_bg_stroke2_r_gray);
            }
        }else {
            holder.cbItem.setVisibility(View.GONE);
        }
    }

    static class ContractsHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.cb_item)
        ITCheckBox cbItem;
        @BindView(R2.id.ll_item)
        LinearLayout ll_item;

        public ContractsHolder(View itemView) {
            super(itemView);
        }
    }
}
