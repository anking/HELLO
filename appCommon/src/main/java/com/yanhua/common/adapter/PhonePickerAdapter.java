package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;

import butterknife.BindView;

public class PhonePickerAdapter extends BaseRecyclerAdapter<String, PhonePickerAdapter.PhonePickerViewHolder> {

    public PhonePickerAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected PhonePickerViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new PhonePickerViewHolder(inflater.inflate(R.layout.item_phone_picker, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull PhonePickerAdapter.PhonePickerViewHolder holder, @NonNull String item) {
        holder.tvTitle.setText(item);
    }

    static class PhonePickerViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_title)
        TextView tvTitle;

        public PhonePickerViewHolder(View itemView) {
            super(itemView);
        }
    }
}