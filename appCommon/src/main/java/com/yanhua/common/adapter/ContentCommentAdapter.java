package com.yanhua.common.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.RectangleIndicator;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import butterknife.BindView;

/**
 * 评论列表项
 *
 * @author Administrator
 */
public class ContentCommentAdapter extends BaseRecyclerAdapter<CommentModel, ContentCommentAdapter.ViewHolder> {

    private OnItemCellClickListener listener;
    private OnADClickListener mAdListener;

    public interface OnItemCellClickListener {
        /**
         * @param pos
         * @param cellType 0为点击用户相关 1为点赞相关 2展示更多子评论
         */
        void onItemCellClick(int pos, int cellType);
    }

    public interface OnADClickListener {
        void onADClick(AdvertiseModel clickItem);
    }

    public void setOnADClickListener(OnADClickListener listener) {
        this.mAdListener = listener;
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    private Context mContext;
    private String mUserId;

    public ContentCommentAdapter(Context context, String userId) {
        super(context);
        this.mContext = context;
        this.mUserId = userId;
    }

    public String getPublishUserId() {
        return mUserId;
    }

    public void setPublishUserId(String userId) {
        this.mUserId = userId;

        notifyDataSetChanged();
    }

    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_content_comment, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        CommentModel item = mDataList.get(position);

        //0是父评论 1是子评论 2是可以显示加载更多的子评论
        int type = item.getType();
        if (type == 0) {
            return 0;
        } else if (type == 1) {
            return 1;
        } else if (type == 2) {
            return 2;
        }

        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolder(mInflater.inflate(R.layout.item_comment_list, parent, false));
        } else if (viewType == 2) {
            return new ViewHolder(mInflater.inflate(R.layout.item_comment_list, parent, false));
        } else {
            return onCreateViewHolder(mInflater, parent);
        }
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommentModel item) {
        int resultType = item.getResultType();
        if (resultType == 1) {
            holder.flAd.setVisibility(View.GONE);
            holder.llComment.setVisibility(View.VISIBLE);
            setContentDetail(holder, item);
        } else if (resultType == 2) {
            holder.flAd.setVisibility(View.VISIBLE);
            holder.llComment.setVisibility(View.GONE);

            setAdContent(holder, item);
        } else {
            holder.flAd.setVisibility(View.GONE);
            holder.llComment.setVisibility(View.GONE);
        }
    }

    private void setAdContent(ViewHolder holder, CommentModel item) {
        List<AdvertiseModel> advertiseList = item.getAdvertisingList();

        if (null != advertiseList && advertiseList.size() > 0) {
            //取出第一个获取长高
            AdvertiseModel firstItem = advertiseList.get(0);

            String imgUrl = firstItem.getImageUrl();
            String videoCoverUrl = firstItem.getVideoCoverUrl();
            String showUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

            Glide.with(mContext)
                    .asBitmap()
                    .load(showUrl)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picWidth = resource.getWidth();
                            int picHeight = resource.getHeight();

                            int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
                            int height = width * picHeight / picWidth;

                            ViewGroup.LayoutParams layoutParams = holder.flAd.getLayoutParams();
                            layoutParams.width = width;
                            layoutParams.height = height;

                            holder.flAd.setLayoutParams(layoutParams);

                            AdvertiseBannerAdapter adapter = new AdvertiseBannerAdapter(advertiseList, mContext);
                            holder.bannerAd.setAdapter(adapter);

                            if (null != advertiseList && advertiseList.size() > 1) {
                                holder.bannerAd
                                        .setLoopTime(3000)
                                        .isAutoLoop(false)
                                        .setIndicator(new RectangleIndicator(mContext))
                                        .setIndicatorRadius(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                                        .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.sub_first))
                                        .setIndicatorHeight(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorWidth(DisplayUtils.dip2px(mContext, 6), DisplayUtils.dip2px(mContext, 12))
                                        .setIndicatorGravity(IndicatorConfig.Direction.CENTER)
                                        .setIndicatorSpace(DisplayUtils.dip2px(mContext, 3))
                                        .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 9)));
                            }

                            holder.bannerAd.setOnBannerListener((data, adPos) -> {
                                AdvertiseModel clickItem = (AdvertiseModel) data;
                                if (null != mAdListener) {
                                    mAdListener.onADClick(clickItem);
                                }
                            });
                        }
                    });
        }
    }

    private void setContentDetail(ViewHolder holder, CommentModel item) {
        int position = getPosition(holder);

        String time = YXTimeUtils.getFriendlyTimeAtContent(item.getCreatedTime());

        //1小时前 . 回复
        String replyTime = "<font color=\"#B4B8BC\" >" + time + " . </font>"
                + "<font color=\"#36404A\" >回复</font>";

        int fabulousCount = item.getFabulousCount();
        boolean isFabulous = item.isIsFabulous();
        String nickName = YHStringUtils.pickName(item.getNickName(), item.getFriendRemark());
        String userPhoto = item.getUserPhoto();
        String comment = item.getCommentDesc();

        //0是父评论 1是子评论 2是可以显示加载更多的子评论
        int type = item.getType();
/*
        if (type == 0) {
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(mContext, 32), DisplayUtils.dip2px(mContext, 32));
//            holder.civAvatar.setLayoutParams(params);
//
//            holder.llComment.setPadding(0, 0, 0, 0);
//            holder.tvExpand.setVisibility(View.GONE);
            holder.llExpand.setVisibility(View.GONE);
        } else if (type == 1) {
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(mContext, 24), DisplayUtils.dip2px(mContext, 24));
//            holder.civAvatar.setLayoutParams(params);
//
//            holder.llComment.setPadding(DisplayUtils.dip2px(mContext, 40), 0, 0, 0);
//            holder.tvExpand.setVisibility(View.GONE);
            holder.llExpand.setVisibility(View.GONE);
        } else if (type == 2) {
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(mContext, 24), DisplayUtils.dip2px(mContext, 24));
//            holder.civAvatar.setLayoutParams(params);
//
//            holder.llComment.setPadding(DisplayUtils.dip2px(mContext, 40), 0, 0, 0);
//            holder.tvExpand.setVisibility(View.VISIBLE);
            holder.llExpand.setVisibility(View.VISIBLE);
            int childCount = item.getParentChildCount();

            holder.tvExpand.setText("展开" + (childCount - 2) + "条评论");
            holder.llExpand.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemCellClick(position, 2);
                }
            });
        }*/

        int childType = item.getChildType();

        if (type == 2) {
            holder.llExpand.setVisibility(View.VISIBLE);
            int childCount = item.getParentChildCount();

            holder.tvExpand.setText("展开" + (childCount - 2) + "条评论");
            holder.llExpand.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemCellClick(position, 2);
                }
            });

            holder.tvReplyTime.setText(time);

            holder.llComment.setBackgroundResource(R.drawable.bg_shape_bottom_comment);
        } else {
            holder.llExpand.setVisibility(View.GONE);
            holder.llExpand.setOnClickListener(null);

            if (type == 0) {
                holder.tvReplyTime.setText(Html.fromHtml(replyTime));
                holder.llComment.setBackgroundResource(R.drawable.shape_bg_white);
            } else {
                holder.tvReplyTime.setText(time);
                if (childType == 1) {
                    holder.llComment.setBackgroundResource(R.drawable.bg_shape_top_comment);
                } else if (childType == 2) {
                    holder.llComment.setBackgroundResource(R.drawable.bg_shape_bottom_comment);
                } else if (childType == 3) {
                    holder.llComment.setBackgroundResource(R.drawable.bg_shape_comment);
                } else {
                    holder.llComment.setBackgroundResource(R.drawable.bg_shape_bg_ui);
                }
            }
        }

        if (!TextUtils.isEmpty(comment)) {
            try {
                comment = URLDecoder.decode(comment.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        String replyName = item.getReplyName();
        String commentUserId = item.getCommentUserId();
        /*int replyStatus = item.getReplyStatus();

        if (replyStatus == 1 && !TextUtils.isEmpty(replyName) && !TextUtils.isEmpty(commentUserId)) {
            SpannableString applyComment = makeReplyCommentSpan(replyName, commentUserId, comment, position);
            holder.tvComment.setText(applyComment);
        } else {
            holder.tvComment.setText(comment);
        }*/

        int replyType = item.getReplyType();
        String userId = item.getUserId();

        if (replyType == 2 && !TextUtils.isEmpty(replyName) && !TextUtils.isEmpty(commentUserId)) {
            String name = makeReplyName(replyName, nickName, commentUserId, userId);

            holder.tvName.setText(Html.fromHtml(name));
        } else {
            String name = makeReplyName(replyName, nickName, "", userId);
            holder.tvName.setText(Html.fromHtml(name));
        }

        holder.tvComment.setText(comment);

        holder.tvStar.setText(YHStringUtils.quantityFormat(fabulousCount));

        holder.ivStar.setSelected(isFabulous);
        holder.ivStar.setText(isFabulous ? "\ue772" : "\ue71b");

        holder.tvStar.setSelected(isFabulous);

        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(holder.civAvatar, userPhoto, R.drawable.place_holder);
        } else {
            holder.civAvatar.setImageResource(R.drawable.place_holder);
        }

        holder.civAvatar.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 0);
            }
        });


        holder.llStar.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 1);
            }
        });
    }

    public String makeReplyName(final String atSomeone, final String nickName, final String commentUserId, String userId) {
        String richText = "";
        if (TextUtils.isEmpty(commentUserId)) {
            richText = String.format("%s%s", nickName, !TextUtils.isEmpty(mUserId) && mUserId.equals(userId) ? "(作者)" : "");
        } else {
            //圆规大师(作者) 回复 大民
            String beReplycomment = mUserId.equals(commentUserId) ? "<font color=\"#36404A\" >" + atSomeone + "</font>" + "<font color=\"#B4B8BC\" >(作者)</font>" : "<font color=\"#36404A\" >" + atSomeone + "</font>";

            String replycomment = mUserId.equals(userId) ? "<font color=\"#36404A\" >" + nickName + "</font>" + "<font color=\"#B4B8BC\" >(作者)</font>" : "<font color=\"#36404A\" >" + nickName + "</font>";

            richText = String.format("%s 回复 %s", replycomment, beReplycomment);
        }

        return richText;
    }

    public SpannableString makeReplyCommentSpan(final String atSomeone, final String commentUserId, String commentContent, int position) {

        String richText = String.format("回复 %s : %s", atSomeone, commentContent);

        SpannableString builder = new SpannableString(richText);

        if (!TextUtils.isEmpty(atSomeone)) {
            builder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(@NonNull View view) {
                    if (listener != null) {
                        listener.onItemCellClick(position, 0);
                    }
                }

                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(ContextCompat.getColor(mContext, R.color.color_5B83E1));
                    ds.setTextSize(DisplayUtils.dip2px(mContext, 14));
                    ds.setFlags(Paint.FAKE_BOLD_TEXT_FLAG);
                }
            }, 2, atSomeone.length() + 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);//设置回复的人的名字
        }
        return builder;
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ll_comment)
        LinearLayout llComment;

        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.tv_comment)
        TextView tvComment;
        @BindView(R2.id.ll_star)
        LinearLayout llStar;
        @BindView(R2.id.iv_star)
        AliIconFontTextView ivStar;


        @BindView(R2.id.tv_star)
        TextView tvStar;
        @BindView(R2.id.tv_expand)
        TextView tvExpand;
        @BindView(R2.id.ll_expand)
        LinearLayout llExpand;
        @BindView(R2.id.tvReplyTime)
        TextView tvReplyTime;

        @BindView(R2.id.flAd)
        FrameLayout flAd;
        @BindView(R2.id.bannerAd)
        Banner bannerAd;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
