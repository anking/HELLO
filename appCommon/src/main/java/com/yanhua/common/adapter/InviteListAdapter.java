package com.yanhua.common.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.view.CircleImageView;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


public class InviteListAdapter extends BaseRecyclerAdapter<InviteModel, InviteListAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mListener;

    public InviteListAdapter(Context context, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    public interface OnHandleClickListener {

        void onHandleClick(int type, InviteModel item, int pos);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull @NotNull InviteListAdapter.ViewHolder holder, @NonNull @NotNull InviteModel item) {
        int position = getPosition(holder);

        String time = item.getActivityTime();//"activityTime": "2022-04-17 10:33:00",
        //约伴时间-固定时间＞14天：显示年月日-时分；
        //24小时＜约伴时间-固定时间＜14天，显示“距离还有x天x小时开始”
        //1小时＜约伴时间-固定时间＜24小时，显示“距离还有x小时xx分开始”
        //0＜约伴时间-固定时间＜1小时，显示“距离还有xx分开始”
        holder.tvTime.setText(YXTimeUtils.inviteDateDiffNow(time));
        holder.tvName.setText(YHStringUtils.value(item.getTitle()));

        String inviteType = item.getMeetPartnerTypeName();
        if (TextUtils.isEmpty(inviteType)) {
            holder.tvInviteType.setVisibility(View.GONE);
        } else {
            holder.tvInviteType.setVisibility(View.VISIBLE);
            holder.tvInviteType.setText(inviteType);
        }

        //根据UI拼接字段
        String payWay = item.getPaymentTypeName();
        holder.tvAddress.setText(YHStringUtils.value(item.getDestination()));
        //发起人信息
        int userGender = item.getUserGender();//邀约用户性别(-1:保密, 1:男, 2:女)
        String userImage = item.getUserImage();//邀约用户头像
        String userName = item.getUserName();//邀约用户名称
        ImageLoaderUtil.loadImgHeadCenterCrop(holder.ivUserHead, userImage, R.drawable.place_holder);
        switch (userGender) {
            case -1:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
            case 1:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_male);
                break;
            case 2:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_female);
                break;
            default:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
        }
        holder.tvUserName.setText(userName);
        String coverImage = item.getCoverUrl();
        if (TextUtils.isEmpty(coverImage)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.place_holder, options);

            holder.ivActivityCover.setSrcBmp(bitmap);
        } else {
            Glide.with(mContext)
                    .asBitmap()
                    .override(DisplayUtils.dip2px(context,120),DisplayUtils.dip2px(context,144)) //图片显示的分辨率 ，像素值 可以转化为DP再设置
                    .load(item.getCoverUrl())
                    .centerCrop()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            holder.ivActivityCover.setSrcBmp(resource);
                        }
                    });
        }

        holder.tvPayWay.setText("\uD83D\uDC49" + YHStringUtils.value(payWay));
        int numLimit = item.getActivityNumber();
        int inviteNumber = item.getInviteNumber();//已邀请
        int enrollNumber = item.getEnrollNumber();//已报名
        if (inviteNumber >= numLimit) {
            holder.tvNum.setText(numLimit + "人局\u3000（人员已满）");
        } else {
            holder.tvNum.setText(numLimit + "人局\u3000（" + inviteNumber + "/" + numLimit + "）");
        }


        int mType = 2;
        String people = item.getPartnerTypeName();
        if (people.contains("仅限男")) {
            mType = 0;
        } else if (people.contains("仅限女")) {
            mType = 1;
        } else {
            mType = 2;
        }
        switch (mType) {
            case 0://仅限男生
                holder.tvNum.setBackgroundResource(R.drawable.shape_gradient_onlyman);
                holder.ivPeople.setBackgroundResource(R.mipmap.ic_only_man);
                break;
            case 1://仅限女生
                holder.tvNum.setBackgroundResource(R.drawable.shape_gradient_onlywoman);
                holder.ivPeople.setBackgroundResource(R.mipmap.ic_only_woman);
                break;
            case 2://男女不限
                holder.tvNum.setBackgroundResource(R.drawable.shape_gradient_people);
                holder.ivPeople.setBackgroundResource(R.mipmap.ic_people);
                break;
        }

        holder.rlItem.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onHandleClick(0, item, position);
            }
        });
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvNum)
        TextView tvNum;
        @BindView(R2.id.ivPeople)
        ImageView ivPeople;
        @BindView(R2.id.rlItem)
        RelativeLayout rlItem;


        @BindView(R2.id.tvInviteType)
        TextView tvInviteType;

        @BindView(R2.id.ivActivityCover)
        AnyShapeImageView ivActivityCover;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;
        @BindView(R2.id.tvPayWay)
        TextView tvPayWay;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
