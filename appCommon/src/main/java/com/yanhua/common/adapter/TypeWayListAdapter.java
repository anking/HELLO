package com.yanhua.common.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.common.model.TypeWayModel;

import butterknife.BindView;

/**
 * 举报列表适配器
 *
 * @author Administrator
 */
public class TypeWayListAdapter extends BaseRecyclerAdapter<TypeWayModel, TypeWayListAdapter.ViewHolder> {

    public TypeWayListAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_type_way_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull TypeWayModel item) {
        String name = item.getName();
        holder.tvTitle.setText(!TextUtils.isEmpty(name) ? name : "");
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_title)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
