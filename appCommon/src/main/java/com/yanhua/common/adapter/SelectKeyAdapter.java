package com.yanhua.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.R2;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;

import butterknife.BindView;

/**
 * 列表适配器
 *
 * @author Administrator
 */
public class SelectKeyAdapter extends BaseRecyclerAdapter<String, SelectKeyAdapter.ViewHolder> {

    private OnDeleteImageListen mListener;
    private String mSelect;

    public SelectKeyAdapter(Context context,OnDeleteImageListen listener) {
        super(context);

        mListener = listener;
    }

    public void setSelect(String select) {
        mSelect = select;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_select_key, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        int pos = getPosition(holder);

        holder.tvTitle.setText(YHStringUtils.value(item));

        if (pos == 0) {
            holder.itemContainer.setSelected(false);
            holder.iconNext.setVisibility(View.VISIBLE);
            holder.iconDelete.setVisibility(View.GONE);
        } else {
            holder.itemContainer.setSelected(true);
            holder.iconNext.setVisibility(View.GONE);
            holder.iconDelete.setVisibility(View.VISIBLE);
        }


        holder.iconDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDeleteImage(pos);
            }
        });
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvTitle)
        TextView tvTitle;
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;
        @BindView(R2.id.iconDelete)
        AliIconFontTextView iconDelete;

        @BindView(R2.id.itemContainer)
        FrameLayout itemContainer;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
