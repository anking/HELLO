package com.yanhua.common.imagepick.listener;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @describe：长按事件
 */
public interface OnItemLongClickListener {
    void onItemLongClick(RecyclerView.ViewHolder holder, int position, View v);
}
