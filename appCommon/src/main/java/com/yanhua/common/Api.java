package com.yanhua.common;

public class Api {

    //评论置顶
    public static final String COMMENT_LILST_ITEM_TOP = "sysadmin/app/content/comment/top/{type}/{id}";

    //评论删除
    public static final String COMMENT_LILST_ITEM_DELETE = "sysadmin/app/content/comment/remove/{commentId}";

    //举报
    public static final String POST_REPORT_CONTENT = "sysadmin/app/report";

    //获取举报原因列表 extendId举报原因(1用户举报 2此刻举报 3集锦举报 4爆料举报 5攻略举报 6约伴举报 7话题举报 8圈子举报 9评论举报 10问答举报)
    public static final String GET_REPORT_TYPE_LIST = "sysadmin/app/report/list";

    //联系客服
    public static final String APP_CONTACT_CS = "sysadmin/app/other/customer";

    //获取最新版本的用户协议和隐私协议
    public static final String QUERY_PROTOCOL_INFO = "sysadmin/app/protocol/query";

    //联系我们
    public static final String APP_CONTACT_US = "sysadmin/app/other/about";

    //根据类型编码获取协议信息
    public static final String GET_APP_PROTOCOL_BY_CODE = "sysadmin/app/protocol/get/{code}";

    //校验手机号是否当前会员的手机
    public static final String GET_APP_USER_PHONE = "sysadmin/app/appUser/check/mobile/{mobile}";

    //用户注销 true-成功 false-失败
    public static final String LOGOFF_USER = "sysadmin/app/user/cancel";

    // 点击广告
    public static final String ADD_MARKETING_CLICK = "sysadmin/app/advertising/click";

    public static final String PROTOCOL_SUER_SIGN = "sysadmin/app/protocol/userSign";

    // 获取广告列表
    public static final String QUERY_AD_LIST = "sysadmin/app/advertising/queryList";
    // 根据编码获取APP说明信息
    public static final String GET_APP_EXPLAIN = "sysadmin/app/appExplain/get/{code}";

    //增加内容浏览量
    public static final String ADD_VIEW_COUNT = "sysadmin/app/content/addViewCount/{type}/{id}";
    public static final String COMMON_CONFIG_BY_TYPE = "sysadmin/app/baseSetting/getForType/{type}";

    public static final String COMMON_REASON_LIST = "sysadmin/app/baseConfig/list";

    //内容收藏
    public static final String UPDATE_CONTENT_COLLECT = "sysadmin/app/content/collect/{type}/{id}";

    //删除用户已经删除的内容
    public static final String DELETE_LIKE_NULL_CONTENT = "sysadmin/app/content/deleted/{id}";

    //内容评论列表
    public static final String GET_COMMENT_LIST = "sysadmin/app/content/comment/list";

    //子评论（二级评论）评论列表
    public static final String GET_CHILD_COMMENT_LIST = "sysadmin/app/content/comment/child/list";

    //发表评论 /app/content/comment/publish
    public static final String PUBLISH_COMMENT = "sysadmin/app/content/comment/publish";

    //评论点赞或者取消点赞
    public static final String UPDATE_COMMENT_FABULOUS = "sysadmin/app/content/comment/fabulous/{commentId}";

    // 评论踩赞或者取消踩赞
    public static final String UPDATE_COMMENT_WORSE = "sysadmin/app/content/comment/worse/{commentId}";

    //内容点赞
    public static final String UPDATE_CONTENT_FABULOUS = "sysadmin/app/content/fabulous/{type}/{id}";

    // 获取省市数据
    public static final String GET_ALL_CITY_LIST = "sysadmin/system/area/all";
    public static final String GET_ALL_AREA = "sysadmin/system/area/queryArea";
    // 酒吧获取省级和市级
    public static final String GET_BAR_ALL_AREA = "sysadmin/system/area/queryAreaBar";
    // 获取市级或者区级
    public static final String GET_CITY_OR_AREA = "sysadmin/system/area/queryDistrict";

    public final static class user {
        //用户定位保存接口
        public static final String UPLOAD_USER_LOCATION_INFO = "sysadmin/app/appUser/location";
        // 用户登录
        public static final String USER_LOGIN = "authorization-server/oauth/token";

        // QQ授权注册
        public static final String QQ_REGISTER = "sysadmin/app/appUser/qqRegister";

        // 微信授权注册
        public static final String WEIXIN_REGISTER = "sysadmin/app/appUser/weixinRegister";

        ///appUser/weiboRegister
        public static final String WEIBO_REGISTER = "sysadmin/app/appUser/weiboRegister";

        // 手机号绑定
        public static final String BIND_PHONE = "sysadmin/app/appUser/bindingPhone";

        // 判断手机号是否存在
        public static final String EXIST_PHONE = "sysadmin/app/appUser/existPhone/{phone}";

        // 获取验证码
        public static final String GET_SMS_CODE = "opc/msg";

        // 验证码校验
        public static final String VALIDATE_CODE = "opc/msg/validate";

        // 修改用户密码
        public static final String MODIFY_USER_PASSWORD = "sysadmin/app/appUser/modify";
        // 查询会员的隐私设置
        public static final String GET_PRIVACY_SETTING = "sysadmin/app/findPrivacySet/getFindPrivacySet";


        // 隐私设置-消息联系设置
        public static final String UPDATE_CHAT2ME_RIGHT = "sysadmin/app/userDetail/setHiddenMessage";

        // 更新用户的隐私设置
        public static final String UPDATE_PRIVACY_SETTING = "sysadmin/app/findPrivacySet/update";
        // app获取最新的版本信息
        public static final String FETCH_APP_VERSION_INFO = "sysadmin/app/appVersion";

        // 记录登录登出
        public static final String VISIT_RRCORD = "sysadmin/visitRecord";

        // 微信解绑
        public static final String WEIXIN_UNTIE = "sysadmin/app/appUser/weixinUntie";
        // 微博解绑
        public static final String WEIBO_UNTIE = "sysadmin/app/appUser/weiboUntie";

        // QQ解绑
        public static final String QQ_UNTIE = "sysadmin/app/appUser/qqUntie";
        //修改用户背景图
        public static final String PERSON_INFO_SET_BACKGROUND = "sysadmin/app/findUser/blackground";
        // 获取阿里oss的签名token
        public static final String GET_STS_TOKEN = "opc/oss/getToken";

        // 获取实名认证所需faceId
        public static final String GET_FACE_ID = "sysadmin/app/faceAuth/getFaceId";

        // 身份证姓名二要素验证
        public static final String IDCARD_VERIFICATION = "sysadmin/app/faceAuth/idCardVerification";

        // 身份证图片识别
        public static final String CHECK_IDCARD_INFORMATION = "sysadmin/app/faceAuth/checkIdCardInformation";

        // 获取开启的个人标签/兴趣爱好列表
        public static final String GET_USER_ALONE_TAG = "sysadmin/app/userAloneTag/list/{type}";

        // 批量设置个人标签/兴趣爱好
        public static final String SET_USER_ALONE_TAG = "sysadmin/app/userAloneTag";

        // 获取用户信息
        public static final String GET_USER_INFO = "sysadmin/app/group/chat/getUserInfo";
    }

    public final static class mine {
        // demo
        public static final String GET_MY_COMMENT_LIST = "sysadmin/app/content/comment/appAdvanceQuery";

        public static final String MY_LIKE_LIST = "sysadmin/app/content/fabulous/queryFabulousContentList";

    }

    public final static class moment {
        //精华日记列表
        public static final String BEST_NOTE_LIST = "sysadmin/app/moment/home/content/elite/page";

        //电音集锦
        public static final String BEST_DISCO_CITY_LIST = "sysadmin/app/wonderful/stat/city";

        //电音集锦我的收藏
        public static final String BEST_DISCO_COLLECT_LIST = "sysadmin/app/wonderful/user/collect/page";

        //此刻流浪历史
        public static final String MOMENT_BROWE_LIST = "sysadmin/app/moment/home/user/view/content/page";

        //精华日记---城市
        public static final String BEST_NOTE_CITY_LIST = "sysadmin/app/moment/home/content/elite/city";

        //内容信息详情
        public static final String GET_CONTENT_DETAIL = "sysadmin/app/moment/home/content/{id}";

        //电音集锦内容信息详情
        public static final String GET_DISCO_CONTENT_DETAIL = "sysadmin/app/wonderful/{id}";

        //删除内容信息
        public static final String DELETE_CONTENT = "sysadmin/app/content/{id}";

        //内容列表
        public static final String GET_CONTENT_LIST = "sysadmin/app/content/list";

        //发布内容
        public static final String PUBLISH_CONTENT = "sysadmin/app/content";//内容发表

        //app/circle/apply--申请圈子
        public static final String CIRCLE_APPLY = "sysadmin/app/circle/apply";//

        //圈主申请
        public static final String CIRCLE_MASTER_APPLY = "sysadmin/app/circleMasterApply/{circleId}";//

        //圈主申请记录
        public static final String CIRCLE_MASTER_APPLY_LIST = "sysadmin/app/circleMasterApply/detail/page";//


        //发现地址分类 /content/queryCategory
        public static final String GET_ADDRESS_CATEGORY_LIST = "sysadmin/app/content/queryCategory";

        // 更新帖子隐私权限
        public static final String UPDATE_PRIVACY_TYPE = "sysadmin/app/content/updatePrivacyTypeById";

        //话题列表获取
        public static final String MOMENT_TOPIC_LIST = "sysadmin/app/topic/page";

        //会员加入圈子
        public static final String MOMENT_CIRCLE_JOIN = "sysadmin/app/user/circle/join/{circleId}";
        //会员退出圈子
        public static final String MOMENT_CIRCLE_QUIT = "sysadmin/app/user/circle/quit/{circleId}";
        //圈主转让圈子
        public static final String MOMENT_CIRCLE_TRANSFER_MASTER = "sysadmin//app/user/circle/transfer/master/{circleId}/{userId}";

        //圈子加入列表获取
        public static final String MOMENT_CIRCLE_JOINED_LIST = "sysadmin/app/user/circle/join/page";

        //圈子加入列表获取
        public static final String MOMENT_CIRCLE_MANAGE_LIST = "sysadmin/app/user/circle/master/page";

        //圈子推荐列表获取
        public static final String MOMENT_CIRCLE_LIST = "sysadmin/app/circle/page";

        //热门圈子列表
        public static final String MOMENT_CIRCLE_HOT_LIST = "sysadmin/app/circle/search/hot/page";

        //热门话题列表
        public static final String MOMENT_TOPIC_HOT_LIST = "sysadmin/app/topic/search/hot/page";

        //首页推荐话题列表 /sysadmin/app/moment/home/topic/recommend/list
        //首页推荐话题列表
        public static final String MOMENT_TOPIC_RECOMMEND_LIST = "sysadmin/app/moment/home/topic/recommend/list";

        //电音集锦列表
        public static final String DISCO_BEST_PAGE_LIST = "sysadmin/app/wonderful/page";

        //此刻列表
        public static final String MOMENT_HOME_PAGE_LIST = "sysadmin/app/moment/home/content/page";///app/moment/home/content/page

        //自己或者他人此刻的相关内容
        public static final String MOMENT_USER_PAGE_LIST = "sysadmin/app/moment/home/user/content/page";

        //会员自己收藏的所有内容列表
        public static final String MOMENT_USER_COLLECT_PAGE_LIST = "sysadmin/app/moment/home/user/collect/content/page";

        //会员自己点赞的所有内容列表
        @Deprecated
        public static final String MOMENT_USER_LIKE_LIST = "sysadmin/app/moment/home/user/fabulous/content/page";

        //圈子浏览量
        public static final String ADD_CIRCLE_VIEWCOUNT = "sysadmin/app/circle/viewCount/{id}";
        //话题浏览量
        public static final String ADD_TOPIC_VIEWCOUNT = "sysadmin/app/topic/viewCount/{id}";


        //话题列表
        public static final String MOMENT_TOPIC_PAGE_LIST = "sysadmin/app/moment/home/content/topic/page";
        public static final String MOMENT_TOPIC_DETAIL = "sysadmin/app/topic/{id}";

        //圈子列表
        public static final String MOMENT_CIRCLE_PAGE_LIST = "sysadmin/app/moment/home/content/circle/page";
        //圈子成员列表
        public static final String MOMENT_CIRCLE_MEMBER_LIST = "sysadmin/app/circle/member/page";

        //圈子申请记录
        public static final String MOMENT_CIRCLE_APPLY_HISTORY = "sysadmin/app/circle/apply/page";

        //圈子详情
        public static final String MOMENT_CIRCLE_DETAIL = "sysadmin/app/circle/{id}";

        //圈子详情---申请id编辑 圈子申请详情  /sysadmin/app/circle/apply/{id}
        public static final String MOMENT_CIRCLE_APPLY_EDIT_DETAIL = "sysadmin/app/circle/apply/{id}";

        //圈子详情状态
        public static final String MOMENT_CIRCLE_STATUS = "sysadmin/app/user/circle/status/{circleId}";
    }

    public final static class message {
        public static final String MESSAGE_SYS_NOTICE_OPEN = "/sysadmin/app/message/notice/user/open/{id}";

        //获取系统消息详情--GET
        public static final String MESSAGE_SYS_NOTICE_DETAIL = "/sysadmin/app/message/notice/user/get/{id}";
        //获取最近的通知(包含未读数量)-GET
        public static final String MESSAGE_SYS_NOTICE_LIST_LAST = "sysadmin/app/message/notice/user/getForLast";
        //系统消息分页请求-POST "current": 0, "size": 0,
        public static final String MESSAGE_SYS_NOTICE_LIST_PAGE = "sysadmin/app/message/notice/user/page";
        //已读-GET
        public static final String MESSAGE_SYS_NOTICE_READ_ITEM = "sysadmin/app/message/notice/user/read/{id}";
        //全部已读-GET
        public static final String MESSAGE_SYS_NOTICE_READ_ALL = "/sysadmin/app/message/notice/user/readAll";


        public static final String MESSAGE_NEW_FANS_LIST_PAGE = "sysadmin/app/message/fans/page";
        public static final String MESSAGE_NEW_FANS_UNREAD_NUM = "sysadmin/app/message/fans/unreadCount";
        //全部已读-GET
        public static final String MESSAGE_NEW_FANS_READ_ALL = "sysadmin/app/message/fans/readAll";


        public static final String MESSAGE_AT_COMMENT_LIST_PAGE = "sysadmin/app/message/comment/page";
        public static final String MESSAGE_AT_COMMENT_UNREAD_NUM = "sysadmin/app/message/comment/unreadCount";
        //全部已读-GET
        public static final String MESSAGE_AT_COMMENT_READ_ALL = "sysadmin/app/message/comment/readAll";

        //分页--POST
        public static final String MESSAGE_FABULOUS_LIST_PAGE = "sysadmin/app/message/fabulousAndCollect/page";
        //赞收藏未读数---GET
        public static final String MESSAGE_FABULOUS_UNREAD_NUM = "sysadmin/app/message/fabulousAndCollect/unreadCount";
        //全部已读-GET
        public static final String MESSAGE_FABULOUS_READ_ALL = "sysadmin/app/message/fabulousAndCollect/readAll";
        //分页用户列表---POST
        public static final String MESSAGE_FABULOUS_USER_LIST = "sysadmin/app/message/fabulousAndCollect/user";

        //邀约消息-----start-----------
        //获取邀约列表--POST
        public static final String MESSAGE_INVITE_LIST = "sysadmin/app/meetPartner/notice/page";
        //获取未读数量--GET
        public static final String MESSAGE_INVITE_UNREAD_NUM = "sysadmin/app/meetPartner/notice/queryCountForUnread";
        //已读单条--GET
        public static final String MESSAGE_INVITE_SET_READ_ID = "sysadmin/app/meetPartner/notice/read/{id}";
        //全部已读--GET
        public static final String MESSAGE_INVITE_SET_READ_ALL = "sysadmin/app/meetPartner/notice/readAll";
        //邀约消息-----end-----------
        //拉黑列表
        public static final String GET_BLACK_LIST = "sysadmin/app/userBlock/blockUsers";//

        //-----------------融云 begin--消息服务-APP-群聊接口--------------
        // 获取用户融云token
        public static final String GET_RONG_CLOUD_TOKEN = "sysadmin/rong/cloud/token";
        //同步消息
        public static final String SYN_RONG_CLOUD_MSG = "sysadmin/rong/cloud/synMsg";
        public static final String GET_USER_GROUP = "sysadmin/app/group/chat/getUserGroups";//GET - 查询用户所在群组

        public static final String GET_BAR_MSG_LIST = "sysadmin/app/message/bar/list";//获取酒吧互动-消息
        //获取最近的酒吧互动(包含未读数量)-GET
        public static final String MESSAGE_INTERACT_LIST_LAST = "sysadmin/app/message/bar/user/getForLast";
        //酒吧互动全部已读-GET
        public static final String MESSAGE_INTERACT_READ_ALL = "/sysadmin/app/message/bar/user/readAll";

        public static final String RONGIM_SEND_SMALL_BAR = "sysadmin/app/group/chat/adminSendMessageByGroupId/{userId}/{groupId}/{message}";//GET--管理员解散群组发送小灰条消息

        // 获取用户信息
        public static final String GET_USER_INFO = "sysadmin/app/group/chat/getUserInfo";//GET 查询用户信息

        // 群信息
        public static final String GROUP_INFO = "sysadmin/app/rong/cloud/group/getGroupChat";
        public static final String RONGIM_GROUP_DETAIL_INFO = "sysadmin/app/group/chat/group/getGroupChat";//GET--群聊/查询群信息/包涵公告

        public static final String RONGIM_GROUP_CREATE = "sysadmin/app/group/chat/group/addGroup";//POST-群聊/创建群组

        public static final String RONGIM_GROUP_ADD_MEMBER = "sysadmin/app/group/chat/group/addMember";//POST--群聊/添加群成员
        public static final String RONGIM_GROUP_REMOVE_MEMBER = "sysadmin/app/group/chat/group/deleteMember";//POST--群聊/移除群成员

        public static final String RONGIM_GROUP_UPDATE_MANAGER = "sysadmin/app/group/chat/updateGroupAdminister";//POST--群聊/添加群管理员 群聊/移除管理员

        public static final String RONGIM_GROUP_EDITE_NOTICE = "sysadmin/app/group/chat/group/notice";//POST--群聊/编辑群公告
        public static final String GROUP_ADD_COMPLAINT = "sysadmin/app/group/complaint/group/complaint";////群聊/投诉--

        public static final String RONGIM_GROUP_MEMBERLIST = "sysadmin/app/group/chat/group/queryMember";//GET--群聊/查询群成员

        public static final String RONGIM_GROUP_SIGNOUT = "sysadmin/app/group/chat/group/signOut";//POST--群聊/退出群组-如果退出的是群主 就会解散
        public static final String RONGIM_GROUP_UPDATE_MSG_HISTORY = "sysadmin/app/group/chat/group/updateGroupMessageHistory";//POST--群组管理/更新聊天记录

        public static final String RONGIM_GROUP_SWITCH_INVITATION = "sysadmin/app/group/chat/group/updateInvitation";//POST--  群聊/群开关
        public static final String RONGIM_GROUP_UPDATE_INFO = "sysadmin/app/group/chat/group/updateName";//POST--  群聊/编辑群昵称、用户昵称/头像
        public static final String RONGIM_GROUP_CONVERT_MASTER = "sysadmin/app/group/chat/updateGroupMasterConvert";//POST--  群组管理/群主转让
        public static final String RONGIM_GROUP_DISMISS = "sysadmin/app/group/chat/updateGroupOrDismiss";//POST--  群组管理/解散
        public static final String RONGIM_GROUP_CONFIG_SET = "sysadmin/app/group/chat/updateRongCloudConfig";//POST--  群主设置/保存
    }

    public final static class home {
        // 首页顶部搜索
        public static final String HOME_SEARCH = "sysadmin/app/home/search/queryPageForSynthesize";

        // 用户搜索
        public static final String USER_SEARCH = "sysadmin/app/home/search/queryPageForUser";

        //详情
        public static final String NEWS_DETAIL = "sysadmin/news/content/{id}";

        //首页---大家都在看 GET current size
        public static final String HOME_BREAK_NEWS_LIST = "sysadmin/news/content/homePageBreakNewsList";

        //最新爆料滚动活动条栏 GET current size city contentCategoryId type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
        public static final String BREAK_NEWS_RECENT_ACTIVE = "sysadmin/news/content/recentActive";
        //最新爆料 推荐置顶 GET current size city contentCategoryId
        // queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
        // type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7:酒吧新闻
        public static final String BREAK_NEWS_RECOMMEND = "sysadmin/news/content/recommend";

        //获取爆料分类--和攻略分类 GET
        public static final String BREAK_NEWS_TYPE_LIST = "sysadmin/newsContentCategory/list/{type}";

        //app 查询有活动的城市 GET
        public static final String BREAK_NEWS_ACTIVITY_CITY_LIST = "sysadmin/news/content/recentActiveCityList";
        //app 城市内容区 GET current size city contentCategoryId type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
        public static final String BREAK_NEWS_CITY_CONTENT_LIST = "sysadmin/news/content/cityContent";

        //上头
        //app  上头攻略 玩转夜娱乐 GET current size  contentCategoryId type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
        public static final String STRATEGY_CONTENT_LIST = "sysadmin/news/content/strategyList";
        //app  上头攻略 玩转夜娱乐 GET attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
        // contentCategoryId type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
        public static final String STRATEGY_HOTSORT_CONTENT_LIST = "sysadmin/news/content/hotSortList";

        //发表内容
        public static final String NEWS_CONTENT_PUBLISH = "sysadmin/news/content/publish";

        //爆料或攻略流量历史
        public static final String NEWS_CONTENT_VIEW_HISTORY = "sysadmin/news/content/queryAppUserViewArticleVO";

        //我的动态爆料或攻略
        public static final String MY_DYNAMIC_NEWS = "sysadmin/news/content/queryAppUserPublishArticleVO";
        //我的收藏爆料或攻略
        public static final String MY_FAV_NEWS = "sysadmin/news/content/queryAppUserCollectArticleVO";

        //删除爆料或攻略
        public static final String DELETE_NEWS_CONTENT = "sysadmin/news/content/batchUpdateStatus";
    }

    //约伴相关接口
    public final static class meetPartner {
        /*  浏览历史
         */
        public static final String MEET_PARTNER_HISTORY_PAGE = "sysadmin/app/meetPartner/history/page";

        //约伴类型
        public static final String MEET_PARTNER_TYPE_LIST = "sysadmin/app/meetPartner/type/list";
        //约伴类型模版
        public static final String MEET_PARTNER_TEMPLATE_LIST = "sysadmin/app/meetPartner/type/template/list";

        /*  发起邀约
         */
        public static final String MEET_PARTNER_INVITE_ADD = "sysadmin/app/meetPartner/invite/add";

        /*  取消邀约
         */
        public static final String MEET_PARTNER_INVITE_CANCEL = "sysadmin/app/meetPartner/invite/cancel";

        /*  删除邀约cancel/{id}
         */
        public static final String MEET_PARTNER_INVITE_DELETE = "sysadmin/app/meetPartner/invite/delete/{id}";
        /*  邀约详情
         */
        public static final String MEET_PARTNER_INVITE_DETAIL = "sysadmin/app/meetPartner/invite/get/{id}";

        /*  邀约详情设置
         */
        public static final String MEET_PARTNER_INVITE_SET_INFO = "sysadmin/app/meetPartner/enroll/get/confirmed/range";

        /*  邀约详情
         */
        public static final String MEET_PARTNER_UPDATE_STATUS = "sysadmin/app/meetPartner/invite/updateForCheck/{id}";

        /*  邀约分页请求
         */
        public static final String MEET_PARTNER_INVITE_PAGE = "sysadmin/app/meetPartner/invite/page";


        /*  我的邀约分页请求
         */
        public static final String MEET_PARTNER_MYINVITE_PAGE = "sysadmin/app/meetPartner/invite/pageForMy";

        /*  修改我的邀约
         */
        public static final String MEET_PARTNER_INVITE_INFO_UPDATE = "sysadmin/app/meetPartner/invite/update";
        /*  报名邀约
         */
        public static final String MEET_PARTNER_INVITE_SINGIN = "sysadmin/app/meetPartner/enroll/add";
        /*  取消报名邀约
         */
        public static final String MEET_PARTNER_INVITE_SINGOUT = "sysadmin/app/meetPartner/enroll/cancel";
        /*  查询当前已加入和待加入数量
         */
        public static final String MEET_PARTNER_INVITE_NUM = "sysadmin/app/meetPartner/enroll/get/{id}";

        /*  查询当前已加入和待加入分页
         */
        public static final String MEET_PARTNER_INVITE_MEMBER_LIST = "sysadmin/app/meetPartner/enroll/page";
        /*  同意加入邀约申请
         */
        public static final String MEET_PARTNER_INVITE_AGREE_SINGIN = "sysadmin/app/meetPartner/enroll/agree/{id}";

        /*  移除同意加入邀约
         */
        public static final String MEET_PARTNER_INVITE_REMOVE_SINGED = "sysadmin/app/meetPartner/enroll/remove";

        //获取新建约伴配置信息(伙伴类型、买单类型)
        public static final String MEET_PARTNER_CREATE_TYPE = "sysadmin/app/meetPartner/config/getForInviteCreate";
        //获取约伴取消原因
        public static final String MEET_PARTNER_CANCEL_REASON = "sysadmin/app/meetPartner/config/queryListForCancelReason";


    }

    // 酒吧模块接口
    public final static class bar {
        // 酒吧列表查询
        public static final String GET_BAR_LIST = "sysadmin/app/bar/page";

        // 酒吧详情
        public static final String GET_BAR_DETAIL = "sysadmin/app/bar/get/{id}";

        // 酒吧列表上的点赞或取消点赞
        public static final String DO_LOVE_OR_NOT_BAR = "sysadmin/app/bar/fabulous/{barId}";

        // 获取酒吧全部问答
        public static final String GET_BAR_QA_LIST = "sysadmin/app/asked/page/baraskanswer";

        // 问题详情列表
        public static final String GET_BAR_QA_DETAIL = "sysadmin/app/asked/page/askDetail";

        // 回答详情
        public static final String GET_BAR_ANSWER_DETAIL = "sysadmin/app/asked/page/answerDetail/{askId}";

        // 我的问答列表
        public static final String GET_MY_BAR_QA_LIST = "sysadmin/app/asked/page/askanswer";

        // 发布提问/回答
        public static final String POST_MY_QA = "sysadmin/app/asked/publish/askanswer";

        // 编辑提问
        public static final String UPDATE_MY_QUESTION = "sysadmin/app/asked/update/askanswer";

        // 发布评论
        public static final String POST_MY_COMMENT = "sysadmin/app/asked/comment/publish";

        // 删除提问
        public static final String DELETE_MY_QUESTION = "sysadmin/app/asked/deleteask/{askedId}";

        // 删除回答
        public static final String DELETE_MY_ANSWER = "sysadmin/app/asked/deleteanswer/{askedId}";

        // 个人中心的酒吧收藏列表
        public static final String GET_COLLECT_BAR_LIST = "sysadmin/app/bar/collect/page";
    }

    public final static class app {
        // 常见问题
        public static final String GET_PROBLEM_LIST = "sysadmin/app/problem/content/page";

        // 意见反馈
        public static final String FEED_BACK = "sysadmin/app/feedback/content/save";


        //拉黑或者取消用户
        public static final String ADD_BLACK = "sysadmin/app/userBlock/blockUser/{userId}";

        //获取发现用户主页信息
        public static final String GET_USER_DETAIL = "sysadmin/app/findUser/{userId}";

        //获取用户信息
        public static final String USER_INFO_DETAIL = "sysadmin/app/userDetail/{id}";

        //内容信息列表获取
        public static final String GET_USER_CONTENT_LIST = "sysadmin/app/findUser/conentList";

        //关注用户
        public static final String FOLLOW_USER = "sysadmin/app/findUser/followUser/{userId}";

        //取消关注用户
        public static final String CANCEL_FOLLOW_USER = "sysadmin/app/findUser/unFollowUser/{userId}";

        //获取关注的用户列表
        public static final String GET_FOLLOW_USER_LIST = "sysadmin/app/findUser/userFollowList";

        // 获取关注我的列表
        public static final String MSG_CARE = "sysadmin/app/findMsg/care";

        //获取粉丝列表
        public static final String GET_FANS_USER_LIST = "sysadmin/app/findUser/userFansList";

        // 获取好友列表 findUser/userFriendList
        public static final String GET_USER_FRIEND = "sysadmin/app/findUser/userFriendList";

        //收藏内容列表
        public static final String GET_FAVORITE_CONTENT = "sysadmin/app/findCollect/list";

        //修改好友备注
        public static final String PERSON_INFO_SET_FRIENDREMARK = "sysadmin/app/findUser/setFriendRemark";

        //修改用户背景图
        public static final String PERSON_INFO_SET_BACKGROUND = "sysadmin/app/findUser/blackground";

        //发现分类
        public static final String GET_CONTENT_CATEGORY_LIST = "sysadmin/app/findContentCategory/list";
    }
}
