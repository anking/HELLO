package com.yanhua.common.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.GridSpaceItemDecoration;

import java.util.List;


public class SelectPartShadowPopupView extends PartShadowPopupView {

    public interface OnSelectTypeListener {
        void onItemSelected(SelectConditionModel selectItem, int pos);
    }

    private RecyclerView rclCondition;
    private Button btnReset, btnConfirm;
    private LinearLayout llContainer;

    private ConditionAdapter adapter;
    private List<SelectConditionModel> listData;
    private SelectConditionModel selectItem;
    private OnSelectTypeListener listener;
    private Context mContext;
    private int mSelectPos;
    private int margin, bgRes;

    public void setOnSelectTypeListener(OnSelectTypeListener listener) {
        this.listener = listener;
    }

    public SelectPartShadowPopupView(@NonNull Context context, List<SelectConditionModel> listData, SelectConditionModel select, int margin, int bgRes) {
        super(context);
        this.mContext = context;
        this.listData = listData;
        this.selectItem = select;
        this.margin = margin;
        this.bgRes = bgRes;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_select_partshadow;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

        initData();
    }

    private void initData() {
        if (null != selectItem) {
            //
        }
    }

    public void refreshData(int selectPos) {
        mSelectPos = selectPos;
        adapter.notifyDataSetChanged();
    }

    private void initView() {
        rclCondition = findViewById(R.id.rcl_condition);
        btnReset = findViewById(R.id.btn_reset);
        btnConfirm = findViewById(R.id.btn_confirm);

        llContainer = findViewById(R.id.llContainer);

        FrameLayout.LayoutParams layoutParams = (LayoutParams) llContainer.getLayoutParams();

        layoutParams.leftMargin = DisplayUtils.dip2px(mContext, margin);
        layoutParams.rightMargin = DisplayUtils.dip2px(mContext, margin);

        llContainer.setLayoutParams(layoutParams);
        llContainer.setBackgroundResource(bgRes);

        GridLayoutManager mSubContentManager = new GridLayoutManager(mContext, 4, GridLayoutManager.VERTICAL, false);
        rclCondition.setLayoutManager(mSubContentManager);
        rclCondition.addItemDecoration(new GridSpaceItemDecoration(4, DisplayUtils.dip2px(mContext, 12), DisplayUtils.dip2px(mContext, 6)));

        adapter = new ConditionAdapter(mContext);
        rclCondition.setAdapter(adapter);

        adapter.setItems(listData);

        adapter.setOnItemClickListener((itemView, pos) -> {
            for (int i = 0; i < listData.size(); i++) {
                SelectConditionModel item = listData.get(i);

                if (i == pos) {
                    item.setSelected(true);
                    selectItem = item;
                    mSelectPos = pos;
                } else {
                    item.setSelected(false);
                }
            }

            adapter.notifyDataSetChanged();
        });

        btnReset.setOnClickListener(v -> {
            for (int i = 0; i < listData.size(); i++) {
                SelectConditionModel item = listData.get(i);

                item.setSelected(false);
            }
            selectItem = listData.get(0);

            adapter.notifyDataSetChanged();

            if (null != listener) {
                dismiss();
//                btnReset.postDelayed(() -> listener.onItemSelected(selectItem, 0), 200);
            }
        });

        btnConfirm.setOnClickListener(v -> {
            if (null != listener) {
                dismiss();
//                btnConfirm.postDelayed(() -> listener.onItemSelected(selectItem, mSelectPos), 200);
            }
        });
    }

    class ConditionAdapter extends BaseRecyclerAdapter<SelectConditionModel, ViewHolder> {

        private Context mContext;

        public ConditionAdapter(Context context) {
            super(context);
            mContext = context;
        }

        @NonNull
        @Override
        protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new ViewHolder(inflater.inflate(R.layout.popup_select_partshadow_item, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull SelectConditionModel item) {
            holder.tvItem.setText(item.getFieldValue());

            boolean isSelect = item.isSelected();
            if (isSelect) {
                holder.tvItem.setSelected(true);
            } else {
                holder.tvItem.setSelected(false);
            }
        }
    }


    class ViewHolder extends BaseViewHolder {

        public TextView tvItem;

        public ViewHolder(View itemView) {
            super(itemView);

            tvItem = itemView.findViewById(R.id.tv_item);
        }
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();

        if (null != listener) {
            dismiss();
            btnReset.postDelayed(() -> listener.onItemSelected(selectItem, mSelectPos), 200);
        }
    }
}
