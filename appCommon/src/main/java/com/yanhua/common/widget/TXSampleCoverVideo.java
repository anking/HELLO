package com.yanhua.common.widget;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.lxj.xpopup.XPopup;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.model.VideoOptionModel;
import com.shuyu.gsyvideoplayer.utils.CommonUtil;
import com.shuyu.gsyvideoplayer.utils.Debuger;
import com.shuyu.gsyvideoplayer.utils.GSYVideoType;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.gsyvideoplayer.video.base.GSYBaseVideoPlayer;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.SystemUtil;
import com.yanhua.core.view.CommonDialog;

import java.util.ArrayList;
import java.util.List;

import moe.codeest.enviews.ENDownloadView;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;


/**
 * 带封面
 */
public class TXSampleCoverVideo extends StandardGSYVideoPlayer {

    ImageView mCoverImage;

    String mCoverOriginUrl;

    int mCoverOriginId = 0;

    int mDefaultRes;

    private LinearLayout llBottomProgressbar;
    private LinearLayout llShowTime;
    private LinearLayout llSeekbar;
    private Context mContext;

    public TXSampleCoverVideo(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public TXSampleCoverVideo(Context context) {
        super(context);
    }

    public TXSampleCoverVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void init(Context context) {
        super.init(context);

        mContext = context;

        mCoverImage = findViewById(R.id.thumbImage);
        llShowTime = findViewById(R.id.ll_show_time);
        llBottomProgressbar = findViewById(R.id.ll_bottom_progressbar);
        llSeekbar = findViewById(R.id.ll_seekbar);

        if (mThumbImageViewLayout != null &&
                (mCurrentState == -1 || mCurrentState == CURRENT_STATE_NORMAL || mCurrentState == CURRENT_STATE_ERROR)) {
            mThumbImageViewLayout.setVisibility(VISIBLE);
        }

        VideoOptionModel videoOptionModel = new VideoOptionModel(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "enable-accurate-seek", 1);
        List<VideoOptionModel> list = new ArrayList<>();
        list.add(videoOptionModel);
        GSYVideoManager.instance().setOptionModelList(list);

        mProgressBar.setFocusable(true);
        mProgressBar.setFocusableInTouchMode(true);

        llSeekbar.setFocusable(true);
        llSeekbar.setFocusableInTouchMode(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            mProgressBar.setMinHeight(DisplayUtils.dip2px(mContext, 1f));
            mProgressBar.setMaxHeight(DisplayUtils.dip2px(mContext, 1f));
        }
        mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.yx_video_progress));
        mProgressBar.setThumb(mContext.getResources().getDrawable(R.drawable.seek_thumb_normal));

        llSeekbar.setOnTouchListener(this);

        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(llShowTime, GONE);
    }

    /**
     * 亮度、进度、音频
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        float x = event.getX();
        float y = event.getY();

        if (mIfCurrentIsFullscreen && mLockCurScreen && mNeedLockFull) {
            onClickUiToggle(event);
            startDismissControlViewTimer();
            return true;
        }

        if (id == R.id.fullscreen) {
            return false;
        }

        if (id == R.id.surface_container) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    touchSurfaceDown(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    float deltaX = x - mDownX;
                    float deltaY = y - mDownY;
                    float absDeltaX = Math.abs(deltaX);
                    float absDeltaY = Math.abs(deltaY);

                    if ((mIfCurrentIsFullscreen && mIsTouchWigetFull)
                            || (mIsTouchWiget && !mIfCurrentIsFullscreen)) {
                        if (!mChangePosition && !mChangeVolume && !mBrightness) {
                            touchSurfaceMoveFullLogic(absDeltaX, absDeltaY);
                        }
                    }
                    touchSurfaceMove(deltaX, deltaY, y);

                    break;
                case MotionEvent.ACTION_UP:

                    startDismissControlViewTimer();

                    touchSurfaceUp();

                    startProgressTimer();

                    //不要和隐藏虚拟按键后，滑出虚拟按键冲突
                    if (mHideKey && mShowVKey) {
                        return true;
                    }
                    break;
            }
            gestureDetector.onTouchEvent(event);
        } else if (id == R.id.progress) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    cancelDismissControlViewTimer();

                    mProgressBar.setFocusable(true);
                    mProgressBar.setFocusableInTouchMode(true);

                case MotionEvent.ACTION_MOVE:
                    cancelProgressTimer();
                    ViewParent vpdown = getParent();
                    while (vpdown != null) {
                        vpdown.requestDisallowInterceptTouchEvent(true);
                        vpdown = vpdown.getParent();
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        mProgressBar.setMinHeight(DisplayUtils.dip2px(mContext, 3f));
                        mProgressBar.setMaxHeight(DisplayUtils.dip2px(mContext, 3f));
                    }
                    mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.seek_progress));
                    mProgressBar.setThumb(mContext.getResources().getDrawable(R.drawable.yx_video_seek_thumb));

                    if (null != mOnDataNetPlayListener) {
                        mOnDataNetPlayListener.hideInfo(true);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    startDismissControlViewTimer();

                    startProgressTimer();
                    ViewParent vpup = getParent();
                    while (vpup != null) {
                        vpup.requestDisallowInterceptTouchEvent(false);
                        vpup = vpup.getParent();
                    }
                    mBrightnessData = -1f;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        mProgressBar.setMinHeight(DisplayUtils.dip2px(mContext, 1f));
                        mProgressBar.setMaxHeight(DisplayUtils.dip2px(mContext, 1f));
                    }
                    mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.yx_video_progress));
                    mProgressBar.setThumb(mContext.getResources().getDrawable(R.drawable.seek_thumb_normal));

                    if (null != mOnDataNetPlayListener) {
                        mOnDataNetPlayListener.hideInfo(false);
                    }
                    break;
            }
        } else if (id == R.id.ll_seekbar) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mProgressBar.setFocusable(true);
                    mProgressBar.setFocusableInTouchMode(true);

//                    if (null != mOnDataNetPlayListener) {
//                        mOnDataNetPlayListener.hideInfo(true);
//                    }

                    onStartTrackingTouch(mProgressBar);
                case MotionEvent.ACTION_MOVE:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        mProgressBar.setMinHeight(DisplayUtils.dip2px(mContext, 3f));
                        mProgressBar.setMaxHeight(DisplayUtils.dip2px(mContext, 3f));
                    }
                    mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.seek_progress));
                    mProgressBar.setThumb(mContext.getResources().getDrawable(R.drawable.yx_video_seek_thumb));

                    onStartTrackingTouch(mProgressBar);

                    showDragProgressTextOnSeekBar(true, mProgressBar.getProgress());
                    break;
                case MotionEvent.ACTION_UP:
//                    if (null != mOnDataNetPlayListener) {
//                        mOnDataNetPlayListener.hideInfo(false);
//                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        mProgressBar.setMinHeight(DisplayUtils.dip2px(mContext, 1f));
                        mProgressBar.setMaxHeight(DisplayUtils.dip2px(mContext, 1f));
                    }
                    mProgressBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.yx_video_progress));
                    mProgressBar.setThumb(mContext.getResources().getDrawable(R.drawable.seek_thumb_normal));

                    onStopTrackingTouch(mProgressBar);
                    break;
            }

            Rect seekRect = new Rect();
            mProgressBar.getHitRect(seekRect);
            if ((event.getY() >= (seekRect.top - 500)) && (event.getY() <= (seekRect.bottom + 500))) {
                float llSeekBary = seekRect.top + seekRect.height() / 2;
                //seekBar only accept relative x
                float llSeekBarx = event.getX() - seekRect.left;
                if (llSeekBarx < 0) {
                    llSeekBarx = 0;
                } else if (llSeekBarx > seekRect.width()) {
                    llSeekBarx = seekRect.width();
                }
                MotionEvent me = MotionEvent.obtain(event.getDownTime(), event.getEventTime(),
                        event.getAction(), llSeekBarx, llSeekBary, event.getMetaState());
                return mProgressBar.onTouchEvent(me);
            }
        }

        return false;
    }


    public void hideLoadingProgressBar() {
        setViewShowState(mLoadingProgressBar, GONE);
    }

    @Override
    public int getLayoutId() {
        return R.layout.tx_video_layout_cover;
    }

    public void loadCoverImage(String url, int res, int w, int h) {
        mCoverOriginUrl = url;
        mDefaultRes = res;

        /*int sw = DisplayUtils.getScreenWidth(this.getContext());//设置图片的宽
        int sh;

        if (w > 0 && h > 0) {
            sh = DisplayUtils.getScreenHeight(mContext) - DisplayUtils.dip2px(mContext, 56);

            sw = sh * w / h;
        } else {
            sh = sw * 3 / 4;
        }

        Glide.with(this)
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .override(sw, sh)
                )
                .into(mCoverImage);*/

        int sw = DisplayUtils.getScreenWidth(this.getContext());//设置图片的宽
        int sh;

        if (w > 0 && h > 0) {
            sh = DisplayUtils.getScreenHeight(mContext) - DisplayUtils.dip2px(mContext, 56);

            if (h > w) {
            } else {
                sw = sh * w / h;
            }

        } else {
            sh = sw * 3 / 4;
        }

        Glide.with(this)
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .override(sw, sh)
                )
                .into(mCoverImage);
    }

    public void loadCoverImageBy(int id, int res) {
        mCoverOriginId = id;
        mDefaultRes = res;
        mCoverImage.setImageResource(id);
    }

    @Override
    public GSYBaseVideoPlayer startWindowFullscreen(Context context, boolean actionBar, boolean statusBar) {
        GSYBaseVideoPlayer gsyBaseVideoPlayer = super.startWindowFullscreen(context, actionBar, statusBar);
        TXSampleCoverVideo sampleCoverVideo = (TXSampleCoverVideo) gsyBaseVideoPlayer;
        if (mCoverOriginUrl != null) {
            int width = getCurrentVideoWidth();
            int height = getCurrentVideoHeight();
            sampleCoverVideo.loadCoverImage(mCoverOriginUrl, mDefaultRes, width, height);
        } else if (mCoverOriginId != 0) {
            sampleCoverVideo.loadCoverImageBy(mCoverOriginId, mDefaultRes);
        }

        return gsyBaseVideoPlayer;
    }

    @Override
    public GSYBaseVideoPlayer showSmallVideo(Point size, boolean actionBar, boolean statusBar) {
        //下面这里替换成你自己的强制转化
        TXSampleCoverVideo sampleCoverVideo = (TXSampleCoverVideo) super.showSmallVideo(size, actionBar, statusBar);
        sampleCoverVideo.mStartButton.setVisibility(GONE);
        sampleCoverVideo.mStartButton = null;
        return sampleCoverVideo;
    }

    @Override
    protected void cloneParams(GSYBaseVideoPlayer from, GSYBaseVideoPlayer to) {
        super.cloneParams(from, to);
        TXSampleCoverVideo sf = (TXSampleCoverVideo) from;
        TXSampleCoverVideo st = (TXSampleCoverVideo) to;
        st.mShowFullAnimation = sf.mShowFullAnimation;
    }

    /**
     * 退出window层播放全屏效果
     */
    @SuppressWarnings("ResourceType")
    @Override
    protected void clearFullscreenLayout() {
        if (!mFullAnimEnd) {
            return;
        }
        mIfCurrentIsFullscreen = false;
        int delay = 0;
        if (mOrientationUtils != null) {
            delay = mOrientationUtils.backToProtVideo();
            mOrientationUtils.setEnable(false);
            if (mOrientationUtils != null) {
                mOrientationUtils.releaseListener();
                mOrientationUtils = null;
            }
        }

        if (!mShowFullAnimation) {
            delay = 0;
        }

        final ViewGroup vp = (CommonUtil.scanForActivity(getContext())).findViewById(Window.ID_ANDROID_CONTENT);
        final View oldF = vp.findViewById(getFullId());
        if (oldF != null) {
            //此处fix bug#265，推出全屏的时候，虚拟按键问题
            TXSampleCoverVideo gsyVideoPlayer = (TXSampleCoverVideo) oldF;
            gsyVideoPlayer.mIfCurrentIsFullscreen = false;
        }

        if (delay == 0) {
            backToNormal();
        } else {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    backToNormal();
                }
            }, delay);
        }
    }


    /******************* 下方两个重载方法，在播放开始前不屏蔽封面，不需要可屏蔽 ********************/
    @Override
    public void onSurfaceUpdated(Surface surface) {
        super.onSurfaceUpdated(surface);
        if (mThumbImageViewLayout != null && mThumbImageViewLayout.getVisibility() == VISIBLE) {
            mThumbImageViewLayout.setVisibility(INVISIBLE);
        }
    }

    @Override
    protected void setViewShowState(View view, int visibility) {
        if (view == mThumbImageViewLayout && visibility != VISIBLE) {
            return;
        }
        super.setViewShowState(view, visibility);
    }

    @Override
    public void onSurfaceAvailable(Surface surface) {
        super.onSurfaceAvailable(surface);
        if (GSYVideoType.getRenderType() != GSYVideoType.TEXTURE) {
            if (mThumbImageViewLayout != null && mThumbImageViewLayout.getVisibility() == VISIBLE) {
                mThumbImageViewLayout.setVisibility(INVISIBLE);
            }
        }
    }

    @Override
    protected void touchDoubleUp(MotionEvent e) {
        if (null != mOnDataNetPlayListener) {
            mOnDataNetPlayListener.onTouchDoubleUp(e);
        }
    }

    /**
     * 定义开始按键显示
     */
    @Override
    protected void updateStartImage() {
        if (mStartButton instanceof ImageView) {
            ImageView imageView = (ImageView) mStartButton;
            if (mCurrentState == CURRENT_STATE_PLAYING) {
//                imageView.setImageResource(R.drawable.ic_discover_pause);
                imageView.setImageResource(R.drawable.bg_tran_circle_no_stroke);
            } else if (mCurrentState == CURRENT_STATE_ERROR) {
                imageView.setImageResource(R.drawable.video_click_error_selector);
            } else {
                imageView.setImageResource(R.mipmap.ic_discover_play);
            }
        }
    }

    /******************* 下方重载方法，在播放开始不显示底部进度和按键，不需要可屏蔽 ********************/

    protected boolean byStartedClick;

    @Override
    protected void onClickUiToggle(MotionEvent e) {
//        super.onClickUiToggle(e);
        if (mIfCurrentIsFullscreen && mLockCurScreen && mNeedLockFull) {
            setViewShowState(mLockScreen, VISIBLE);
            return;
        }
        byStartedClick = true;

        if (mIfCurrentIsFullscreen && mLockCurScreen && mNeedLockFull) {
            setViewShowState(mLockScreen, VISIBLE);
            return;
        }

        if (mCurrentState == CURRENT_STATE_PREPAREING) {
            if (mBottomContainer != null) {
                if (mBottomContainer.getVisibility() == View.VISIBLE) {
                    changeUiToPrepareingClear();
                } else {
                    changeUiToPreparingShow();
                }

                llShowTime.setVisibility(GONE);
            }
        } else if (mCurrentState == CURRENT_STATE_PLAYING) {
            if (mBottomContainer != null) {
                if (mBottomContainer.getVisibility() == View.VISIBLE) {
                    changeUiToPlayingClear();
                } else {
                    changeUiToPlayingShow();
                }

                llShowTime.setVisibility(VISIBLE);
            }
        } else if (mCurrentState == CURRENT_STATE_PAUSE) {
            if (mBottomContainer != null) {
                if (mBottomContainer.getVisibility() == View.VISIBLE) {
                    changeUiToPauseClear();
                } else {
                    changeUiToPauseShow();
                }

                llShowTime.setVisibility(GONE);
            }
        } else if (mCurrentState == CURRENT_STATE_AUTO_COMPLETE) {
            if (mBottomContainer != null) {
                if (mBottomContainer.getVisibility() == View.VISIBLE) {
                    changeUiToCompleteClear();
                } else {
                    changeUiToCompleteShow();
                }

                llShowTime.setVisibility(GONE);
            }
        } else if (mCurrentState == CURRENT_STATE_PLAYING_BUFFERING_START) {
            if (mBottomContainer != null) {
                if (mBottomContainer.getVisibility() == View.VISIBLE) {
                    changeUiToPlayingBufferingClear();
                } else {
                    changeUiToPlayingBufferingShow();
                }

                llShowTime.setVisibility(GONE);
            }
        }

        clickStartIcon();
    }

    @Override
    protected void changeUiToPlayingClear() {
        Debuger.printfLog("changeUiToPlayingClear");
        changeUiToClear();
//        setViewShowState(mBottomProgressBar, VISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
    }

    /**
     * 小窗口
     **/
    @Override
    protected void setSmallVideoTextureView() {
        if (mProgressBar != null) {
            mProgressBar.setOnTouchListener(null);
            mProgressBar.setVisibility(VISIBLE);
        }
        if (mFullscreenButton != null) {
            mFullscreenButton.setOnTouchListener(null);
            mFullscreenButton.setVisibility(INVISIBLE);
        }
        if (mCurrentTimeTextView != null) {
            mCurrentTimeTextView.setVisibility(INVISIBLE);
        }
        if (mTextureViewContainer != null) {
            mTextureViewContainer.setOnClickListener(null);
        }
        if (mSmallClose != null) {
            mSmallClose.setVisibility(VISIBLE);
            mSmallClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideSmallVideo();
                    releaseVideos();
                }
            });
        }
    }

    @Override
    protected void hideAllWidget() {
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mTopContainer, INVISIBLE);
//        setViewShowState(mBottomProgressBar, VISIBLE);

        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
    }


    @Override
    protected void changeUiToNormal() {
        Debuger.printfLog("changeUiToNormal");

        setViewShowState(mTopContainer, VISIBLE);
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, VISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        updateStartImage();
        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        byStartedClick = false;
    }

    @Override
    protected void changeUiToPreparingShow() {
        Debuger.printfLog("changeUiToPreparingShow");

        setViewShowState(mTopContainer, VISIBLE);
        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mLoadingProgressBar, VISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ENDownloadView enDownloadView = (ENDownloadView) mLoadingProgressBar;
            if (enDownloadView.getCurrentState() == ENDownloadView.STATE_PRE) {
                ((ENDownloadView) mLoadingProgressBar).start();
            }
        }

        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
//        setViewShowState(mLoadingProgressBar, INVISIBLE);
    }


    @Override
    protected void changeUiToPauseShow() {
        Debuger.printfLog("changeUiToPauseShow");

        setViewShowState(mTopContainer, VISIBLE);
        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        updateStartImage();
        updatePauseCover();
    }


    @Override
    protected void changeUiToCompleteShow() {
        Debuger.printfLog("changeUiToCompleteShow");

        setViewShowState(mTopContainer, VISIBLE);
        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, VISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        updateStartImage();

        setViewShowState(llShowTime, INVISIBLE);
    }

    @Override
    protected void changeUiToError() {
        Debuger.printfLog("changeUiToError");

        setViewShowState(mTopContainer, INVISIBLE);
        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        updateStartImage();
    }

    @Override
    protected void changeUiToPlayingBufferingShow() {
        Debuger.printfLog("changeUiToPlayingBufferingShow");

        setViewShowState(mTopContainer, VISIBLE);
        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mLoadingProgressBar, VISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ENDownloadView enDownloadView = (ENDownloadView) mLoadingProgressBar;
            if (enDownloadView.getCurrentState() == ENDownloadView.STATE_PRE) {
                ((ENDownloadView) mLoadingProgressBar).start();
            }
        }

        if (!byStartedClick) {
            setViewShowState(llShowTime, INVISIBLE);
            setViewShowState(mStartButton, INVISIBLE);
        }
        setViewShowState(mLoadingProgressBar, INVISIBLE);
//        setViewShowState(llShowTime, INVISIBLE);
    }

    private void changeUI() {
        int width = getCurrentVideoWidth();
        int height = getCurrentVideoHeight();

        int sw = DisplayUtils.getScreenWidth(this.getContext());//设置图片的宽
        int sh = DisplayUtils.getScreenHeight(this.getContext());
        int marginBottom = DisplayUtils.dip2px(this.getContext(), 56);
        int screenRealHeight = sh - marginBottom;

        if (width == 0 || height == 0) {
            //避免为0的时候
            GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT);
        } else {
            if (height > width) {
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_FULL);
            } else {
                GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT);
            }
        }

//        GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT);
        changeTextureViewShowType();
        if (mTextureView != null)
            mTextureView.requestLayout();
    }

    @Override
    protected void changeUiToPlayingShow() {
        Debuger.printfLog("changeUiToPlayingShow");

        setViewShowState(mTopContainer, VISIBLE);
        setViewShowState(mBottomContainer, VISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        updateStartImage();

        if (!byStartedClick) {
            setViewShowState(llShowTime, INVISIBLE);
            setViewShowState(mStartButton, INVISIBLE);
        }

//        setViewShowState(llShowTime, INVISIBLE);
    }

    @Override
    public void startAfterPrepared() {
        super.startAfterPrepared();
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
//        setViewShowState(mBottomProgressBar, VISIBLE);

        setViewShowState(mBottomProgressBar, INVISIBLE);
        changeUI();

        showVedioWifiDialog();

        //初始时候信息可见
        if (null != mOnDataNetPlayListener) {
            mOnDataNetPlayListener.hideInfo(false);
        }

        //显示进度条动起来
        mBottomContainer.postDelayed(new Runnable() {
            @Override
            public void run() {
                startProgressTimer();
            }
        }, 3000);

        mTouchingProgressBar = false;
    }

    @Override
    public void changeUiToPrepareingClear() {

        setViewShowState(mTopContainer, INVISIBLE);
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
    }

    @Override
    public void changeUiToPlayingBufferingClear() {
        Debuger.printfLog("changeUiToPlayingBufferingClear");

        setViewShowState(mTopContainer, INVISIBLE);
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mLoadingProgressBar, VISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
//        setViewShowState(mBottomProgressBar, VISIBLE);

        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ENDownloadView enDownloadView = (ENDownloadView) mLoadingProgressBar;
            if (enDownloadView.getCurrentState() == ENDownloadView.STATE_PRE) {
                ((ENDownloadView) mLoadingProgressBar).start();
            }
        }
        updateStartImage();
    }


    @Override
    public void changeUiToClear() {
        Debuger.printfLog("changeUiToClear");

        setViewShowState(mTopContainer, INVISIBLE);
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, INVISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
    }

    @Override
    public void changeUiToCompleteClear() {
        Debuger.printfLog("changeUiToCompleteClear");

        setViewShowState(mTopContainer, INVISIBLE);
//        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(llShowTime, INVISIBLE);
        setViewShowState(mStartButton, VISIBLE);
        setViewShowState(mLoadingProgressBar, INVISIBLE);
        setViewShowState(mThumbImageViewLayout, VISIBLE);
//        setViewShowState(mBottomProgressBar, VISIBLE);
        setViewShowState(mBottomProgressBar, INVISIBLE);
        setViewShowState(mLockScreen, (mIfCurrentIsFullscreen && mNeedLockFull) ? VISIBLE : GONE);

        if (mLoadingProgressBar instanceof ENDownloadView) {
            ((ENDownloadView) mLoadingProgressBar).reset();
        }
        updateStartImage();
    }

    /**
     * 暂停状态
     */
    @Override
    public void onVideoPause() {
        super.onVideoPause();

        llShowTime.setVisibility(GONE);
    }

    /**
     * 开始播放
     */
    @Override
    public void startPlayLogic() {
        if (mVideoAllCallBack != null) {
            Debuger.printfLog("onClickStartThumb");
            mVideoAllCallBack.onClickStartThumb(mOriginUrl, mTitle, this);
        }

        GSYVideoManager.instance().setNeedMute(false);

        prepareVideo();
        startDismissControlViewTimer();

        //显示进度条动起来
        mBottomContainer.postDelayed(new Runnable() {
            @Override
            public void run() {
                startProgressTimer();
            }
        }, 3000);
    }

    /**
     * 显示wifi确定框，如需要自定义继承重写即可
     */
    protected void showVedioWifiDialog() {
        if (!YXConfig.isWIFION
                && NetworkUtils.isConnected() && YXConfig.needShowNoWifi && !YXConfig.isOver3Hour) {
            ToastUtils.showShort(mContext.getResources().getString(R.string.tips_not_wifi));

            startPlayLogic();

            YXConfig.needShowNoWifi = false;
            return;
        }

        if (canPlayLogic()) {
            CommonDialog commonDialog = new CommonDialog(mContext, getResources().getString(R.string.tips_not_wifi),
                    getResources().getString(R.string.tips_not_wifi_confirm),
                    getResources().getString(R.string.tips_not_wifi_cancel),
                    true, YXConfig.DURATION_TIME);
            new XPopup.Builder(mContext).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                startPlayLogic();

                mOnDataNetPlayListener.play();
            });
            commonDialog.setOnCancelListener(() -> {
                commonDialog.dismiss();
                mOnDataNetPlayListener.stop();
            });

            this.onVideoPause();
        } else if (!NetworkUtils.isConnected()) {
//            ToastUtil.makeCustomToast(mContext, mContext.getResources().getString(R.string.no_net), Toast.LENGTH_SHORT);
        }
    }

    /**
     * 播放按键点击
     */
    @Override
    protected void clickStartIcon() {
        if (canPlayLogic()) {
            showVedioWifiDialog();

            return;
        } else if (!NetworkUtils.isConnected()) {
            ToastUtils.showShort(mContext.getResources().getString(R.string.no_net));
            return;
        }

        super.clickStartIcon();
    }

    private OnDataNetPlayListener mOnDataNetPlayListener;

    public void setOnDataNetPlayListener(OnDataNetPlayListener listener) {
        mOnDataNetPlayListener = listener;
    }

    private boolean canPlayLogic() {
        //不是WiFi，超过3小时，数据网络
        return null != mOnDataNetPlayListener && !SystemUtil.isWifi(mContext) && YXConfig.needShowNoWifi && NetworkUtils.isConnected() && YXConfig.isOver3Hour;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        showDragProgressTextOnSeekBar(fromUser, progress);
    }

    @Override
    protected void showDragProgressTextOnSeekBar(boolean fromUser, int progress) {
        if (fromUser) {
            int duration = getDuration();
            if (mCurrentTimeTextView != null)
                mCurrentTimeTextView.setText(CommonUtil.stringForTime(progress * duration / 100));

            llShowTime.setVisibility(VISIBLE);
            mBottomProgressBar.setProgress(progress);
            mProgressBar.setProgress(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        byStartedClick = true;
        super.onStartTrackingTouch(seekBar);

        int duration = getDuration();
        int progress = seekBar.getProgress();

        mBottomProgressBar.setProgress(progress);
        mProgressBar.setProgress(progress);

        if (mCurrentTimeTextView != null)
            mCurrentTimeTextView.setText(CommonUtil.stringForTime(progress * duration / 100));

        llShowTime.setVisibility(VISIBLE);
        if (null != mOnDataNetPlayListener) {
            mOnDataNetPlayListener.hideInfo(true);
        }
        //暂停播放
        if (getGSYVideoManager() != null && getGSYVideoManager().isPlaying()) {
            clickStartIcon();
        }
    }

    /***
     * 拖动进度条
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mVideoAllCallBack != null && isCurrentMediaListener()) {
            if (isIfCurrentIsFullscreen()) {
                Debuger.printfLog("onClickSeekbarFullscreen");
                mVideoAllCallBack.onClickSeekbarFullscreen(mOriginUrl, mTitle, this);
            } else {
                Debuger.printfLog("onClickSeekbar");
                mVideoAllCallBack.onClickSeekbar(mOriginUrl, mTitle, this);
            }
        }
        if (getGSYVideoManager() != null && mHadPlay) {
            try {
                int time = seekBar.getProgress() * getDuration() / 100;
                getGSYVideoManager().seekTo(time);
            } catch (Exception e) {
                Debuger.printfWarning(e.toString());
            }
        }

        mHadSeekTouch = false;

        //新增自定义逻辑---------------------------------------------
        //继续播放
        if (getGSYVideoManager() != null && !getGSYVideoManager().isPlaying()) {
            clickStartIcon();
        }

        int progress = seekBar.getProgress();
        int duration = getDuration();

        mBottomProgressBar.setProgress(progress);
        mProgressBar.setProgress(progress);

        if (mCurrentTimeTextView != null)
            mCurrentTimeTextView.setText(CommonUtil.stringForTime(progress * duration / 100));

        llShowTime.postDelayed(() -> {
            setViewShowState(llShowTime, View.VISIBLE);
            setViewShowState(mProgressBar, View.VISIBLE);
            llShowTime.setVisibility(VISIBLE);

            if (null != mOnDataNetPlayListener) {
                mOnDataNetPlayListener.hideInfo(false);
            }

            llShowTime.postDelayed(() -> {
//                if (null != mOnDataNetPlayListener) {
//                    mOnDataNetPlayListener.hideInfo(false);
//                }
//                setViewShowState(mProgressBar, View.GONE);
                setViewShowState(llShowTime, View.GONE);
                llShowTime.setVisibility(GONE);
            }, 3000);
        }, 100);
        //新增自定义逻辑---------------------------------------------
    }

    /**
     * 非wifi情况下播放提示
     */
    public interface OnDataNetPlayListener {
        void play();

        void stop();

        void hideInfo(boolean isHideInfo);

        void onTouchDoubleUp(MotionEvent e);
    }
}
