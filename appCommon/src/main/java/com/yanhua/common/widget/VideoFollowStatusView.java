package com.yanhua.common.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.yanhua.common.R;
import com.yanhua.core.view.AliIconFontTextView;

public class VideoFollowStatusView extends LinearLayout {

    private LinearLayout llBind;
    private AliIconFontTextView ivIcon;


    @SuppressLint("ResourceAsColor")
    public VideoFollowStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_follow_status, this, true);

        llBind = findViewById(R.id.llBind);
        ivIcon = findViewById(R.id.ivIcon);
    }

    public void setStatus(boolean status) {
        llBind.setSelected(status);
        ivIcon.setSelected(status);
        ivIcon.setText(status ? "\ue732" : "\ue73b");
    }
}
