package com.yanhua.common.widget;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.CollectionUtils;
import com.lxj.xpopup.XPopup;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.RongRecentListAdapter;
import com.yanhua.common.adapter.ShareFriendItemAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.presenter.CommonAppPresenter;
import com.yanhua.common.presenter.contract.CommonAppContract;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.EmptyLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;

/**
 * 火车票时刻列表页面
 *
 * @author Administrator
 */
public class SelectShareListBottomPop<T> extends BaseMvpBottomPopupView<CommonAppPresenter> implements CommonAppContract.IView {
    private Context mContext;
    TextView tvHeader;
    TextView tvRight;
    TextView ivLeft;
    EditText etSearch;
    RecyclerView rvTrain;
    EmptyLayout emptyLayout;

    LinearLayout menuLinerLayout;// 可滑动的显示选中用户的View
    //最近联系人
    LinearLayout llRecentlyContainer;
    RecyclerView rvRecentList;
    RecyclerView rvFriendList;

    private RongRecentListAdapter mRongRecentAdapter;
    private List<ShareObjectModel> mConversations;//最近回话列表数据

    private ShareFriendItemAdapter mAdapter;

    boolean multiSelect;//多选
    private ArrayList<ShareObjectModel> selecteListData;

    T mObj;
    int mShareType;

    @Override
    protected void creatPresent() {
        basePresenter = new CommonAppPresenter();
    }

    private OnShareActionListener mListener;

    public interface OnShareActionListener {
        void onShareAction(ArrayList<ShareObjectModel> selecteListData, String msg);
    }

    public SelectShareListBottomPop(@NonNull Context context, T obj, int shareType, OnShareActionListener listener) {
        super(context);
        mContext = context;

        mListener = listener;

        mObj = obj;
        mShareType = shareType;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_popup_select_share_list;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

        initData();

        setHeadTitleState();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    //保留以前操作方式，避免浪费没必要的时间
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (null != event) {
            String eventName = event.getEventName();
            if (CommonConstant.SHARE_APP_LIST.equals(eventName) && null != event.getSerializable()) {
                ArrayList<ShareObjectModel> selectData = (ArrayList) event.getSerializable();

                if (multiSelect) {
                    //多选还需要在这个页面点击发送，弹框
                    for (ShareObjectModel item : selectData) {
                        if (!selecteListData.contains(item)) {
                            doAddDelete(item, false);
                        }
                    }
                } else {
                    //弹出分享框
                    shareSelectChatData();
                }
            }
        }
    }

    private void setHeadTitleState() {
        if (!multiSelect) {
            tvHeader.setText("选择一个聊天");
            tvRight.setText("多选");
        } else {
            tvHeader.setText("选择多个聊天");
            tvRight.setText("单选");
        }
    }

    private void initView() {
        tvHeader = findViewById(R.id.tv_header);
        tvRight = findViewById(R.id.tv_right);
        ivLeft = findViewById(R.id.iv_left);

        etSearch = findViewById(R.id.et_search);

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
                    String inputContent = etSearch.getText().toString().trim();
                    List<ShareObjectModel> user_temp = new ArrayList<ShareObjectModel>();
                    for (ShareObjectModel user : mAdapter.getmDataList()) {
                        String uesrname = user.getNickName();

                        String nameTemp = uesrname.toLowerCase();
                        String inputContentTemp = inputContent.toLowerCase();
                        if (nameTemp.contains(inputContentTemp)) {
                            user_temp.add(user);
                        }
                        mAdapter.setItems(user_temp);
                    }
                } else {
                    mAdapter.setItems(mFriends);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        menuLinerLayout = findViewById(R.id.ll_add);

        llRecentlyContainer = findViewById(R.id.llRecentlyContainer);
        rvRecentList = findViewById(R.id.rvRecentList);
        rvFriendList = findViewById(R.id.rv_content);

        rvFriendList.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvFriendList);

        rvRecentList.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvRecentList);

        findViewById(R.id.llMygroups).setOnClickListener(v -> {
            int selectMode = 1;

            ARouter.getInstance().build(ARouterPath.GROUP_CONTACTS_ACTIVITY)
                    .withInt("selectMode", selectMode)
                    .withSerializable("selecteListData", selecteListData)
                    .withBoolean("multiSelect", multiSelect).navigation();//我的群组
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        mRongRecentAdapter = new RongRecentListAdapter(mContext);
        rvRecentList.setLayoutManager(manager);
        rvRecentList.setAdapter(mRongRecentAdapter);

        mRongRecentAdapter.setOnItemClickListener((itemView, pos) -> {
            ShareObjectModel model = mRongRecentAdapter.getmDataList().get(pos);

            //选择或者取消
            if (!multiSelect) {
                boolean select = model.isSelected();
                model.setSelected(!select);

                doAddDelete(model, true);
            } else {
                if (selecteListData.size() >= 9) {
                    CommonDialog commonDialog = new CommonDialog(mContext, "最多只能选择9个聊天", "", "我知道了", "");
                    new XPopup.Builder(mContext).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();
                    });
                } else {
                    boolean select = model.isSelected();
                    model.setSelected(!select);

                    doAddDelete(model, false);
                }
            }
        });

        emptyLayout = findViewById(R.id.empty_view);

        rvFriendList.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new ShareFriendItemAdapter(mContext);

        rvFriendList.addItemDecoration(new SpaceItemDecoration(2));

        rvFriendList.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            ShareObjectModel model = mAdapter.getmDataList().get(pos);

            //选择或者取消
            if (!multiSelect) {
                boolean select = model.isSelected();
                model.setSelected(!select);

                doAddDelete(model, true);
            } else {
                if (selecteListData.size() >= 9) {
                    CommonDialog commonDialog = new CommonDialog(mContext, "最多只能选择9个聊天", "", "我知道了", "");
                    new XPopup.Builder(mContext).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();
                    });
                } else {
                    boolean select = model.isSelected();
                    model.setSelected(!select);

                    doAddDelete(model, false);
                }
            }
        });

        ivLeft.setOnClickListener(v -> dismiss());
        tvRight.setOnClickListener(v -> {
            if (selecteListData.size() == 0) {
                multiSelect = !multiSelect;
                setHeadTitleState();

                mRongRecentAdapter.setMultiSelect(multiSelect);
                mAdapter.setMultiSelect(multiSelect);
            } else {
                if (!multiSelect) {
                    multiSelect = !multiSelect;
                    setHeadTitleState();

                    mRongRecentAdapter.setMultiSelect(multiSelect);
                    mAdapter.setMultiSelect(multiSelect);
                    return;
                }
                shareSelectChatData();
            }
        });
    }

    //显示选择的头像
    private void showCheckImage(ShareObjectModel model) {
        // 包含TextView的LinearLayout
        // 参数设置
        LinearLayout.LayoutParams menuLinerLayoutParames = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        View view = LayoutInflater.from(mContext).inflate(R.layout.select_share_item, null);
        CircleImageView images = view.findViewById(R.id.civ_avatar);
        menuLinerLayoutParames.setMargins(0, 0, DisplayUtils.dip2px(mContext, 12), 0);
        ImageLoaderUtil.loadImgCenterCrop(images, model.getImg(), R.drawable.place_holder);
        // 设置id，方便后面删除
        view.setTag(model);
        menuLinerLayout.addView(view, 0, menuLinerLayoutParames);
    }

    //删除选择的头像
    private void deleteImage(ShareObjectModel model) {
        View view = menuLinerLayout.findViewWithTag(model);
        if (null != view) {
            menuLinerLayout.removeView(view);
        }
    }

    private void doAddDelete(ShareObjectModel model, boolean shareBack) {

        boolean selected = model.isSelected();

        if (selected) {
            if (selecteListData.contains(model)) {
                int rPos = selecteListData.indexOf(model);

                selecteListData.remove(rPos);
                selecteListData.add(rPos, model);
            } else {
                selecteListData.add(model);
            }

            if (multiSelect) {
                showCheckImage(model);
            }
        } else {
            selecteListData.remove(model);
            if (multiSelect) {
                deleteImage(model);
            }
        }

        if (shareBack) {
            shareSelectChatData();
            return;
        }

        if (mRecentList.contains(model)) {
            int rPos = mRecentList.indexOf(model);

            mRecentList.remove(rPos);
            mRecentList.add(rPos, model);
        }

        if (mFriends.contains(model)) {
            int rPos = mFriends.indexOf(model);

            mFriends.remove(rPos);
            mFriends.add(rPos, model);
        }

        setRecentListData();
        mAdapter.setItems(mFriends);

        if (selecteListData.size() > 0) {
            int num = selecteListData.size();
            tvRight.setText("完成(" + num + ")");
            tvRight.setEnabled(true);
            tvRight.setTextColor(mContext.getResources().getColor(R.color.theme));
        }
    }

    /**
     * 先弹框，确认后再发送
     */
    private void shareSelectChatData() {
        RongIMShareContentDialog shareContentDialog = new RongIMShareContentDialog(mContext, selecteListData, mObj, mShareType);

        new XPopup.Builder(mContext).asCustom(shareContentDialog).show();
        shareContentDialog.setOnConfirmListener((msg) -> {
            shareContentDialog.dismiss();

            if (null != mListener) {
                mListener.onShareAction(selecteListData, msg);

                SelectShareListBottomPop.this.delayDismiss(1000);
            }
        });
        shareContentDialog.setOnCancelListener((msg) -> shareContentDialog.dismiss());

    }


    private String userId;
    private List<ShareObjectModel> mRecentList;

    protected void initData() {
        selecteListData = new ArrayList<>();

        userId = UserManager.getInstance().getUserId();

        mConversations = new ArrayList<>();
        mRecentList = new ArrayList<>();
        // 获取本地会话列表
        RongIMClient.getInstance().getConversationListByPage(new RongIMClient.ResultCallback<List<Conversation>>() {
            @Override
            public void onSuccess(List<Conversation> conversations) {
                // 获取关注的好友
                if (null != conversations && conversations.size() > 0) {
                    mConversations.clear();

                    for (Conversation conversation : conversations) {
                        ShareObjectModel shareObjectModel = new ShareObjectModel();

                        shareObjectModel.setUserId(conversation.getTargetId());

                        mConversations.add(shareObjectModel);
                    }

                    llRecentlyContainer.setVisibility(VISIBLE);
                } else {
                    llRecentlyContainer.setVisibility(GONE);
                }

                //获取好友
                basePresenter.getUserFriend(userId, "", current, 10000);//一次性请求全部好友
            }

            @Override
            public void onError(RongIMClient.ErrorCode ErrorCode) {
                llRecentlyContainer.setVisibility(GONE);

                //获取好友
                basePresenter.getUserFriend(userId, "", current, 10000);//一次性请求全部好友
            }
        }, 0, 20, Conversation.ConversationType.PRIVATE, Conversation.ConversationType.GROUP);
    }

    private List<ShareObjectModel> mFriends;
    private List<ShareObjectModel> mGroudList;

    @Override
    public void handleUserListSuccess(List<FriendModel> friends) {
        if (CollectionUtils.isNotEmpty(friends)) {
            mFriends = new ArrayList<>();
            //从好友列表中选取用户信息
            if (null != mConversations && mConversations.size() > 0) {
                for (int i = 0; i < friends.size(); i++) {
                    FriendModel friendModel = friends.get(i);

                    ShareObjectModel shareObjectModel = new ShareObjectModel();
                    shareObjectModel.setImg(friendModel.getUserPhoto());
                    shareObjectModel.setNickName(friendModel.getNickName());
                    shareObjectModel.setUserId(friendModel.getUserId());
                    shareObjectModel.setGroup(false);

                    if (mConversations.contains(shareObjectModel) && !mRecentList.contains(shareObjectModel)) {
                        mRecentList.add(shareObjectModel);
                    }

                    mFriends.add(shareObjectModel);
                }

                mAdapter.setItems(mFriends);
            }

            setRecentListData();

            //获取群
            basePresenter.getUserGroups(userId, "", true);
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            emptyLayout.setErrorType(EmptyLayout.NODATA);

//            emptyLayout.setErrorImag(R.drawable.empty);
            emptyLayout.setErrorMessage("暂无数据");
        }
    }


    @Override
    public void handleUserGroupsBeanSuccess(List<GroupsBean> list, boolean search) {
        if (CollectionUtils.isNotEmpty(list)) {
            mGroudList = new ArrayList<>();
            //从好友列表中选取用户信息
            if (null != mConversations && mConversations.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    GroupsBean groupsBean = list.get(i);

                    ShareObjectModel shareObjectModel = new ShareObjectModel();
                    shareObjectModel.setImg(groupsBean.getGroupImg());
                    shareObjectModel.setNickName(groupsBean.getName());
                    shareObjectModel.setUserId(groupsBean.getId());
                    shareObjectModel.setGroup(true);

                    if (mConversations.contains(shareObjectModel) && !mGroudList.contains(shareObjectModel)) {
                        mRecentList.add(shareObjectModel);
                    }

                    mGroudList.add(shareObjectModel);
                }
            }

            setRecentListData();
        }
    }

    private void setRecentListData() {
        if (null != mRecentList && mRecentList.size() > 0) {
            mRongRecentAdapter.setItems(mRecentList);
            llRecentlyContainer.setVisibility(VISIBLE);
        } else {
            llRecentlyContainer.setVisibility(GONE);
        }

    }


    @Override
    public void showLoading() {
        emptyLayout.setVisibility(View.VISIBLE);
        emptyLayout.setErrorType(EmptyLayout.TRANSPARENT_LOADING);
    }

    @Override
    public void hideLoading() {
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }


    @Override
    protected int getPopupHeight() {
        return DisplayUtils.getScreenHeight(mContext);
    }


    private class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        private int mSpace;

        @Override
        public void getItemOffsets(@NonNull Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.left = 0;
            outRect.right = 0;
            outRect.bottom = mSpace;
            outRect.top = 0;

        }

        public SpaceItemDecoration(int space) {
            this.mSpace = space;
        }
    }
}
