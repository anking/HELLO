package com.yanhua.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanhua.common.R;
import com.yanhua.core.view.AliIconFontTextView;

public class BindStatusView extends LinearLayout {

    private LinearLayout llBind;
    private AliIconFontTextView ivIcon;
    private TextView tvStatus;

    public BindStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_bind_status, this, true);

        llBind = findViewById(R.id.llBind);
        ivIcon = findViewById(R.id.ivIcon);
        tvStatus = findViewById(R.id.tvStatus);


    }

    public void setBindStatus(boolean isBind) {
        llBind.setSelected(isBind);
        ivIcon.setVisibility(isBind ? GONE : VISIBLE);

        tvStatus.setText(isBind ? "解绑" : "绑定");
    }

    public boolean getBindStatus() {
        return llBind.isSelected();
    }
}
