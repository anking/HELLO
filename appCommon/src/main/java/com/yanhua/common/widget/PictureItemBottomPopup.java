package com.yanhua.common.widget;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;


public class PictureItemBottomPopup extends BottomPopupView {

    private Context mContext;
    private OnPictureActionListener mListener;

    public PictureItemBottomPopup(@NonNull Context context, OnPictureActionListener listener) {
        super(context);
        this.mContext = context;

        this.mListener = listener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_picture_item;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_edit = findViewById(R.id.tv_edit);
        TextView tv_delete = findViewById(R.id.tv_delete);
        TextView tv_cancel = findViewById(R.id.tv_cancel);

        tv_edit.setOnClickListener(view -> {
            dismiss();
            if (null != mListener) {
                mListener.onEditPicture();
            }
        });
        tv_delete.setOnClickListener(view -> {
            dismiss();
            if (null != mListener) {
                mListener.onDeletePicture();
            }
        });
        tv_cancel.setOnClickListener(view -> dismiss());
    }

    public interface OnPictureActionListener {
        void onEditPicture();

        void onDeletePicture();
    }
}
