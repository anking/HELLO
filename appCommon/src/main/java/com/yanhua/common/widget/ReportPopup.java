package com.yanhua.common.widget;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.ReportAdapter;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.presenter.ReportPresenter;
import com.yanhua.common.presenter.contract.ReportContract;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AliIconFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * 举报弹窗
 *
 * @author Administrator
 */
public class ReportPopup extends BaseMvpBottomPopupView<ReportPresenter> implements ReportContract.IView {

    private Context mContext;
    private ReportAdapter mAdapter;
    private List<ReportTypeBean> mList = new ArrayList<>();
    private int mType;

    public interface OnItemClickListener {
        void onItemClick(ReportTypeBean bean);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new ReportPresenter();
    }

    public ReportPopup(@NonNull Context context, int type) {
        super(context);
        mContext = context;
        mType = type;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        AliIconFontTextView iv_close = findViewById(R.id.iv_close);

        TextView tvCancel = findViewById(R.id.tvCancel);

        RecyclerView rvReport = findViewById(R.id.rv_report);
        TextView tv_title = findViewById(R.id.tv_title);

        tv_title.setText("举报原因");

        tvCancel.setOnClickListener(view -> {
            dismiss();
        });
        iv_close.setOnClickListener(view -> {
            dismiss();
        });

        LinearLayoutManager mManager = new LinearLayoutManager(mContext);
        mAdapter = new ReportAdapter(mContext);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                ReportTypeBean bean = mList.get(pos);
                listener.onItemClick(bean);
            }
        });

        rvReport.setLayoutManager(mManager);
        rvReport.setAdapter(mAdapter);

        //根据类型获取举报类型
//        int reportType = getReportTypeByType(mType);
        if (mType == 0) {
            ToastUtils.showShort("获取举报原因失败，稍后再试！");
            return;
        }

        basePresenter.getReportReasonList(mType);
    }

    /**
     * 因为这个type需要对应
     *
     * @param type
     * @return
     */
    private int getReportTypeByType(int type) {
        int reportType = 0;
        switch (type) {
            case YXConfig.TYPE_MOMENT:
                reportType = YXConfig.reportType.moment;
                break;
            case YXConfig.TYPE_INVITE:
                reportType = YXConfig.reportType.invite;
                break;
            case YXConfig.TYPE_BREAK_NEWS:
                reportType = YXConfig.reportType.breakNews;
                break;
            case YXConfig.TYPE_STRATEGY:
                reportType = YXConfig.reportType.strategy;
                break;
            case YXConfig.TYPE_BEST:
                reportType = YXConfig.reportType.disco_best;
                break;
            case YXConfig.TYPE_9BAR:
                reportType = YXConfig.reportType.question_answer;
                break;
            default:
                reportType = type;
                break;
        }

        return reportType;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_report;
    }

    @Override
    public void getReportReasonListSuccess(List<ReportTypeBean> list) {
        if (list != null && !list.isEmpty()) {
            mList = list;
            mAdapter.setItems(mList);
        }
    }


    @Override
    protected int getMaxHeight() {
        return DisplayUtils.getScreenHeight(mContext) / 2;
    }

    @Override
    public void reportSuccess() {

    }
}
