package com.yanhua.common.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.PickTopicListAdapter;
import com.yanhua.common.presenter.CommonAppPresenter;
import com.yanhua.common.presenter.contract.CommonAppContract;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 选择话题
 *
 * @author Administrator
 */
public class PickTopicBottomPop extends BaseMvpBottomPopupView<CommonAppPresenter> implements CommonAppContract.IView {
    private final int PICK_MAX = 5;
    private Context mContext;
    TextView tvCancel;
    TextView tvOK, decelerate;
    DataObserverRecyclerView rvContent;
    TagFlowLayout tflHistory, tflPicked;
    private LinearLayout llHeaderView;

    SmartRefreshLayout refreshLayout;
    EditText etInput;
    TextView tvPickedNum;
    LinearLayout llPickedHistory, llPicked;

    private PickTopicListAdapter mAdapter;
    private OnSelectResultListener mListener;
    private String keyWord;

    List<TopicModel> historyList;

    @Override
    protected void creatPresent() {
        basePresenter = new CommonAppPresenter();
    }

    public PickTopicBottomPop(@NonNull Context context, OnSelectResultListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_popup_pick_topic;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

        initData();
    }

    private void initView() {
        rvContent = findViewById(R.id.rvContent);
        tvCancel = findViewById(R.id.tvCancel);
        tvOK = findViewById(R.id.tvOK);
        etInput = findViewById(R.id.etInput);

        refreshLayout = findViewById(R.id.refreshLayout);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);
//        rvContent.setEmptyView();

        tvOK.setVisibility(GONE);
        tvOK.setOnClickListener(v -> {
            dismiss();
        });
        tvCancel.setOnClickListener(v -> dismiss());

        etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                keyWord = s.toString().trim();

                if (!TextUtils.isEmpty(keyWord)) {
                    basePresenter.getTopicList(keyWord, 1, 20);//搜索
                } else {
                    current = 1;
                    basePresenter.getTopicList(keyWord, current, size);//搜索
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            keyWord = "";

            etInput.setText(keyWord);
            basePresenter.getTopicList(keyWord, current, size);
        });


        mAdapter = new PickTopicListAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            TopicModel item = mAdapter.getItemObject(pos - 1);

            mListener.selectResult(item);

            dismiss();
        });
    }

    protected void initData() {
        View header = LayoutInflater.from(mContext).inflate(R.layout.head_popup_pick_topic, rvContent, false);
        mAdapter.addHeaderView(header);
        tflHistory = header.findViewById(R.id.tflHistory);
        tflPicked = header.findViewById(R.id.tflPicked);
        tvPickedNum = header.findViewById(R.id.tvPickedNum);
        llPickedHistory = header.findViewById(R.id.llPickedHistory);
        llPicked = header.findViewById(R.id.llPicked);
        decelerate = header.findViewById(R.id.decelerate);
        llHeaderView = header.findViewById(R.id.llHeaderView);

        basePresenter.getTopicList("", current, size);

        historyList = new ArrayList<>();
        Gson gson = new Gson();
        String json = DiscoCacheUtils.getInstance().getTopicHistoryList();

        Type listType = new TypeToken<List<TopicModel>>() {
        }.getType();
        if (!TextUtils.isEmpty(json)) {
            historyList.addAll(gson.fromJson(json, listType));
        }

        if (historyList.size() > 0) {
            llPickedHistory.setVisibility(VISIBLE);
        } else {
            llPickedHistory.setVisibility(GONE);
        }

        TagAdapter tagAdapterHistory = new TagAdapter<TopicModel>(historyList) {
            @Override
            public View getView(FlowLayout parent, int position, TopicModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_topic_history, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                tvTagName.setText("#" + tagItem.getTitle() + "#");
                return view;
            }
        };

        //控制显示5个
        tflHistory.setAdapter(tagAdapterHistory);
        tflHistory.setOnTagClickListener((view, position, parent) -> {
            TopicModel item = historyList.get(position);

            mListener.selectResult(item);
            dismiss();
            return true;
        });
    }

    @Override
    protected int getMaxHeight() {
        return DisplayUtils.getScreenHeight(mContext) * 7 / 8;
    }


    public interface OnSelectResultListener {
        void selectResult(TopicModel topicModel);
    }

    @Override
    public void handleTopicList(ListResult<TopicModel> listResult) {
        int total = listResult.getTotal();

        List<TopicModel> dataList = new ArrayList<>();

        if (!TextUtils.isEmpty(keyWord)) {
            //搜索状态
            if (total == 0) {
                decelerate.setText("创建新话题");

                TopicModel newTopic = new TopicModel();
                newTopic.setTitle(keyWord);
                newTopic.setId("-1");

                dataList.add(newTopic);
            } else {
                decelerate.setText("\uD83D\uDC4D推荐话题");

                dataList.addAll(listResult.getRecords());
            }
            mAdapter.setItems(dataList);
        } else {
            decelerate.setText("\uD83D\uDC4D推荐话题");

            dataList.addAll(listResult.getRecords());
            //
            if (current == 1) {
                mAdapter.setItems(dataList);
            } else {
                mAdapter.setMoreData(dataList);
            }

            if (mAdapter.getItemCount() + dataList.size() < total) {
                current++;
            } else {
                refreshLayout.setNoMoreData(true);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();

        mListener.selectResult(null);
    }
}
