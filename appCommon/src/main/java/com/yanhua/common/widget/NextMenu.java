package com.yanhua.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanhua.common.R;

/**
 * 消息顶部menu
 */
public class NextMenu extends LinearLayout {

    private View viewDot;
    private ImageView ivIcon;
    private TextView tvName;

    public NextMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_next_menu, this, true);

        viewDot = findViewById(R.id.viewDot);
        ivIcon = findViewById(R.id.ivIcon);
        tvName = findViewById(R.id.tvTitle);
        View dividerLine = findViewById(R.id.dividerLine);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.NextMenu);
        if (attributes != null) {
            boolean isVisible = attributes.getBoolean(R.styleable.NextMenu_isVisible, false);
            boolean dividerVisible = attributes.getBoolean(R.styleable.NextMenu_dividerVisible, false);


            String menuName = attributes.getString(R.styleable.NextMenu_menuName);
            int menuIcon = attributes.getResourceId(R.styleable.NextMenu_menuIcon, R.mipmap.ic_msg_fans);

            ivIcon.setImageResource(menuIcon);
            tvName.setText(menuName);
            viewDot.setVisibility(isVisible ? VISIBLE : GONE);

            dividerLine.setVisibility(dividerVisible ? VISIBLE : GONE);

            attributes.recycle();
        }
    }

    /**
     * 控制是否有新消息
     * @param isVisible
     */
    public void setNewMessage(boolean isVisible) {
        viewDot.setVisibility(isVisible ? VISIBLE : GONE);
    }
}
