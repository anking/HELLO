package com.yanhua.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yanhua.common.R;

public class HotTabView extends RelativeLayout {
    private TextView tvTitle;
    private ImageView ivLeft;
    private ImageView ivRight;
    private boolean isSelect;

    public HotTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_hot_tab, this, true);

        tvTitle = findViewById(R.id.tvTitle);
        ivLeft = findViewById(R.id.ivLeft);
        ivRight = findViewById(R.id.ivRight);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.HotTabView);
        if (attributes != null) {
            isSelect = attributes.getBoolean(R.styleable.HotTabView_isSelect, false);
            String tabTitle = attributes.getString(R.styleable.HotTabView_tabTitle);

            tvTitle.setText(tabTitle);

            ivLeft.setVisibility(isSelect ? VISIBLE : GONE);
            ivRight.setVisibility(isSelect ? VISIBLE : GONE);
            tvTitle.setSelected(isSelect);
            attributes.recycle();
        }
    }

    public void setTabSelected(boolean select) {
        ivLeft.setVisibility(select ? VISIBLE : GONE);
        ivRight.setVisibility(select ? VISIBLE : GONE);
        tvTitle.setSelected(select);

        isSelect = select;
    }

    public boolean isTabSelected() {
        return isSelect;
    }
}