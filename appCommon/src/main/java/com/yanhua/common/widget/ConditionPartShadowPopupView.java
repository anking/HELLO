package com.yanhua.common.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R;
import com.yanhua.common.model.ConditionPartModel;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.GridSpaceItemDecoration;

import java.util.ArrayList;

public class ConditionPartShadowPopupView extends PartShadowPopupView {

    public interface OnSelectTypeListener {
        void onItemSelected(ConditionPartModel selectItem);
    }

    private RecyclerView rclCondition;
    private Button btnReset, btnConfirm;

    private ConditionAdapter adapter;
    private ArrayList<ConditionPartModel> listData;
    private ConditionPartModel selectItem;
    private OnSelectTypeListener listener;
    private Context mContext;

    public void setOnSelectTypeListener(OnSelectTypeListener listener) {
        this.listener = listener;
    }

    public ConditionPartShadowPopupView(@NonNull Context context, ArrayList<ConditionPartModel> listData, ConditionPartModel select) {
        super(context);
        this.mContext = context;
        this.listData = listData;
        this.selectItem = select;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_condition_partshadow;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

        initData();
    }

    private void initData() {
        if (null != selectItem) {
            //
        }
    }


    private void initView() {
        rclCondition = findViewById(R.id.rcl_condition);
        btnReset = findViewById(R.id.btn_reset);
        btnConfirm = findViewById(R.id.btn_confirm);

        GridLayoutManager mSubContentManager = new GridLayoutManager(mContext, 3, GridLayoutManager.VERTICAL, false);
        rclCondition.setLayoutManager(mSubContentManager);
        rclCondition.addItemDecoration(new GridSpaceItemDecoration(3, DisplayUtils.dip2px(mContext, 12), DisplayUtils.dip2px(mContext, 6)));

        adapter = new ConditionAdapter(mContext);
        rclCondition.setAdapter(adapter);

        adapter.setItems(listData);

        adapter.setOnItemClickListener((itemView, pos) -> {
            for (int i = 0; i < listData.size(); i++) {
                ConditionPartModel item = listData.get(i);

                if (i == pos) {
                    item.setSelected(true);
                    selectItem = item;
                } else {
                    item.setSelected(false);
                }
            }

            adapter.notifyDataSetChanged();
        });

        btnReset.setOnClickListener(v -> {
            for (int i = 0; i < listData.size(); i++) {
                ConditionPartModel item = listData.get(i);

                item.setSelected(false);
            }
            selectItem = null;

            adapter.notifyDataSetChanged();

            if (null != listener) {
                dismiss();
                btnReset.postDelayed(() -> listener.onItemSelected(selectItem), 200);
            }
        });

        btnConfirm.setOnClickListener(v -> {
            if (null != listener) {
                dismiss();
                btnConfirm.postDelayed(() -> listener.onItemSelected(selectItem), 200);
            }
        });
    }

    class ConditionAdapter extends BaseRecyclerAdapter<ConditionPartModel, ViewHolder> {

        private Context mContext;

        public ConditionAdapter(Context context) {
            super(context);
            mContext = context;
        }

        @NonNull
        @Override
        protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new ViewHolder(inflater.inflate(R.layout.popup_condition_partshadow_item, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull ConditionPartModel item) {
            holder.tvItem.setText(item.getName());

            boolean isSelect = item.isSelected();
            if (isSelect) {
                holder.tvItem.setSelected(true);
            } else {
                holder.tvItem.setSelected(false);
            }
        }
    }

    class ViewHolder extends BaseViewHolder {

        public TextView tvItem;

        public ViewHolder(View itemView) {
            super(itemView);

            tvItem = itemView.findViewById(R.id.tv_item);
        }
    }

}
