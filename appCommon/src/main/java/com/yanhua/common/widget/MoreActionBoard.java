package com.yanhua.common.widget;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.TextUtils;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ShareWebUrlBody;
import com.yanhua.base.utils.SharesUtils;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.widget.AlertPopup;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 更多操作面板
 */
public class MoreActionBoard {
/*
    此刻样式：

    标题：
    有正文：直接显示正文内容，最多两行，超出显示“...“
    无正文：图片动态显示”分享图片“，视频动态显示”分享视频“
    左侧文字内容：来自【发布用户昵称】的此刻
    图片：图片动态显示第一张，视频动态显示第一帧缩略图
    底部：app图标及名称（所有站外分享通用）

    新闻样式：
    标题：新闻标题，最多显示两行
    左侧文字内容：新闻正文内容，最多三行
    图片：新闻封面图

    话题样式：
    标题：话题名称，最多显示两行
    图片：后台有配置显示话题封面图，如无设置使用话题分享默认图

    圈子样式：
    标题：圈子名称，最多显示两行
    图片：圈子封面图

    酒吧样式：
    标题：酒吧名称，最多显示两行
    左侧文字内容：酒吧具体地址，最多显示三行
    图片：酒吧主图

    约伴样式：
    标题：约伴标题，最多显示两行
    左侧文字内容：【发布用户昵称】发布了一条有趣的约伴，快来参加吧
    图片：约伴封面图

    用户样式：
    标题：用户昵称
    左侧文字内容：固定文字”分享音圈用户“
    图片：用户头像

    App分享
    标题：固定文字：“嘿，快来下载音圈！”
    左侧文字内容：固定文字”有趣又好玩的夜经济社交，更多新鲜玩法，Z时代神器“
    图片：app logo图分享app样式：*/

    public static void toShare(Activity context, boolean isBlock, boolean hideBlack, boolean isMyself, int auditStatus, int pricavyType, ShareBoardPopup.OnShareItemClickListener listener) {
        ShareBoardPopup popup = new ShareBoardPopup(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType);
        popup.setOnShareItemClickListener(listener);
        new XPopup.Builder(context).autoFocusEditText(false).asCustom(popup).show();
    }

    public static void sharePersonPageDetail(Activity context, String id, String nickName, String headPic,boolean isBlock, OnShareRealActionListener realActionListener) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            boolean isMyself = UserManager.getInstance().isMyself(id);
            boolean hideBlack = isMyself;

            // 1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = 1;//话题默认通过，通过后app才可见
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;//无效设置

            String url = YXConfig.getSharPersonalDetailUrl(id);

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);

                    //                用户样式：
//                标题：用户昵称
//                左侧文字内容：固定文字”分享音圈用户“
//                图片：用户头像
                    shareBody.setTitle(nickName);
                    shareBody.setDesc("分享音圈用户");

                    if (!TextUtils.isEmpty(headPic)) {
                        shareBody.setImageUrl(headPic);
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            ShareBoardPopup popup = new ShareBoardPopup(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, false, true, true);
            popup.setOnShareItemClickListener(listener);
            new XPopup.Builder(context).autoFocusEditText(false).asCustom(popup).show();
        });
    }


    public static void share9BarDetail(Activity context, String id, String barTitle, String barAddress, String barPicUrl, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            boolean isBlock = false;
            boolean hideBlack = true;
            // 1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = 1;//话题默认通过，通过后app才可见
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;//无效设置

            String url = YXConfig.getShar9BarDetaillUrl(id);

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);

//                酒吧样式：
//                标题：酒吧名称，最多显示两行
//                左侧文字内容：酒吧具体地址，最多显示三行
//                图片：酒吧主图
                    shareBody.setTitle(barTitle);
                    shareBody.setDesc(barAddress);

                    if (!TextUtils.isEmpty(barPicUrl)) {
                        shareBody.setImageUrl(barPicUrl);
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            ShareBoardPopup popup = new ShareBoardPopup(context, isBlock, hideBlack, true, auditStatus, pricavyType, false, true, true);
            popup.setOnShareItemClickListener(listener);
            new XPopup.Builder(context).autoFocusEditText(false).asCustom(popup).show();
        });
    }

    public static void shareMyDynamicNews(Activity context, NewsDetailModel model, int kind, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (null == model) {
                return;
            }

            boolean isBlock = false;
            boolean hideBlack = true;
            boolean isMyself = UserManager.getInstance().isMyself(model.getUserId());
            //        1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = model.getAuditStatus();
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;//无效设置

            String id = model.getId();
            String pathUrl = model.getCoverUrl();

            int contentType = 1;//1：图文，其他 2:视频
            if (TextUtils.isEmpty(pathUrl)) {
                contentType = 1;
            } else {
                boolean isVideo = FileUtils.isVideo(pathUrl);
                contentType = isVideo ? 2 : 1;
            }

            String url = "";
            int cateType = 0;//分享参数type
            if (kind == YXConfig.TYPE_BREAK_NEWS) {
                cateType = 2;
                url = contentType == 2 ? YXConfig.getShareVideoDetailUrl(id, cateType) : YXConfig.getShareBreakNewsDetailUrl(id);
            } else if (kind == YXConfig.TYPE_STRATEGY) {
                cateType = 3;
                url = contentType == 2 ? YXConfig.getShareVideoDetailUrl(id, cateType) : YXConfig.getShareStrategyDetailUrl(id);
            }

            String finalUrl = url;
            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, finalUrl);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(finalUrl);
                    shareBody.setTitle(model.getContentTitle());
                    shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

                    String videoCover = model.getVedioCoverUrl();
                    boolean isVideo = FileUtils.isVideo(pathUrl);
                    String shareImgUrl = "";
                    if (isVideo) {
                        shareImgUrl = videoCover;
                    } else {
                        shareImgUrl = pathUrl;
                    }

                    if (!TextUtils.isEmpty(shareImgUrl)) {
                        shareBody.setImageUrl(shareImgUrl);
                    }
//
                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            toShare(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, listener);
        });
    }

    //和电音集锦相关
    public static void shareMyDynamic(Activity context, MomentListModel model, int discoBest, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (null == model) {
                return;
            }

//        boolean isBlock = model.isBlock();
            boolean isBlock = false;
            boolean hideBlack = true;
            boolean isMyself = UserManager.getInstance().isMyself(model.getUserId());
//        1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = model.getAuditStatus();
            int pricavyType = model.getPrivacyType();

            String id = model.getId();
            int contentType = model.getContentType();

            String url = contentType == 2 ? YXConfig.getShareVideoDetailUrl(id, discoBest) : YXConfig.getShareNewsDetailUrl(id);

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);

                    String title = "";
                    String content = YHStringUtils.pickLastFirst(model.getIntroduce(), model.getContent());
                    content = YHStringUtils.getHtmlContent(content);
                    if (TextUtils.isEmpty(content)) {
                        title = contentType == 2 ? "分享视频" : "分享图片";
                    } else {
                        title = content;
                    }
                    shareBody.setTitle(title);

                    String desc = "来自 " + model.getNickName() + (model.getDiscoBest() == 1 ? " 的电音集锦" : " 的此刻");
                    shareBody.setDesc(desc);

                    String contentUrls = model.getContentUrl();
                    if (!TextUtils.isEmpty(contentUrls)) {
                        Type listType = new TypeToken<ArrayList<FileResult>>() {
                        }.getType();
                        ArrayList<FileResult> fileResults = GsonUtils.fromJson(contentUrls, listType);

                        List<String> imageUrl = new ArrayList<>();
                        for (FileResult fileResult : fileResults) {
                            imageUrl.add(fileResult.getHttpUrl());
                        }

                        if (!imageUrl.isEmpty()) {
                            shareBody.setImageUrl(imageUrl.get(0));
                        }
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            toShare(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, listener);
        });
    }


    /**
     * 已处理（此刻和电音集锦相关）
     *
     * @param context
     * @param model
     * @param realActionListener
     */
    public static void shareMyDynamic(Activity context, MomentListModel model, OnShareRealActionListener realActionListener) {
        shareMyDynamic(context, model, 0, realActionListener);
    }

    public static void shareInviteDetail(Activity context, InviteModel model, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (null == model) {
                return;
            }

//        boolean isBlock = model.isBlock();
            boolean isBlock = false;
            boolean hideBlack = true;
            boolean isMyself = UserManager.getInstance().isMyself(model.getUserId());
//        1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = 1;
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;

            String id = model.getId();
            String url = YXConfig.getShareInviteDetailUrl(id);

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_STOP_INVITE) {
                    String msg = "<font color=\"#36384A\" >" + "取消邀约后，所有报名人会收到邀约取消通知，" + "</font>"
                            + "<font color=\"#783AD8\" >" + "确认是否取消邀约" + "</font>"
                            + "<font color=\"#36384A\" >" + "？" + "</font>";

                    AlertPopup alertPopup = new AlertPopup(context, "确定取消邀约吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_STOP_INVITE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);
                    shareBody.setTitle(model.getTitle());
                    shareBody.setDesc(model.getIntroduction());

                    //"您的好友正在与您分享精彩的内容，赶快来…"
                    String coverUrl = model.getCoverUrl();
                    if (!TextUtils.isEmpty(coverUrl)) {
                        shareBody.setImageUrl(coverUrl);
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            //        分享面板说明：
//
//        取消邀约：
//        待审核/进行中的约伴显示，其余状态不显示；取消后该约伴不显示在约伴列表，状态变更为“已取消”

//        取消原因：从后台返回，选中其他时，下方的具体原因输入框必填（未填写点击确认提示”请输入具体原因“，否则非必填
//        点击确认，需弹窗二次确认用户是否取消，确认后发送通知至所有报名者
//
//        删除：邀约者可直接删除约伴信息
//        适用状态：已结束、已停约、审核未通过、已取消，这四种状态下才显示”删除“按钮
//        分享：以下几种约伴状态时，不显示分享组件
//            审核未通过
//        已取消
//        已停约
            int status = model.getStatus();

            boolean hideShareWays = false;
            boolean hideStopInvite = false;
            boolean hideDelete = false;

            //2 4 5 状态 App不显示 2换成（ 7 8 ）
            ////审核状态(0:审核中, 1:审核未通过, 2:审核通过, 3:已停约, 4:用户删除, 5:平台删除, 6:已取消,7:进行中,8:已结束)
            switch (status) {
                case 0://审核中
                    hideDelete = true;
                    hideStopInvite = isMyself ? false : true;
                    hideShareWays = true;
                    break;
                case 2://审核通过
                    break;
                case 1://审核未通过
                case 3://已停约
                case 6://已取消
                case 8://已结束
                    hideDelete = false;
                    hideShareWays = true;
                    hideStopInvite = true;
                    break;
                case 7://进行中
                    hideStopInvite = isMyself ? false : true;
                    hideDelete = true;
                    hideShareWays = false;
                    break;
                default:
                    break;
            }

            ShareBoardPopup popup = new ShareBoardPopup(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, hideShareWays, hideStopInvite, hideDelete);
            popup.setOnShareItemClickListener(listener);
            new XPopup.Builder(context).autoFocusEditText(false).asCustom(popup).show();
        });
    }

    public static void shareCircleDetail(Activity context, CircleModel model, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (null == model) {
                return;
            }

//        boolean isBlock = model.isBlock();
            boolean isBlock = false;
            boolean hideBlack = true;
            boolean isMyself = UserManager.getInstance().isMyself(model.getUserId());
            // 1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = 1;//话题默认通过，通过后app才可见
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;//无效设置

            String id = model.getId();
            String url = YXConfig.getShareCircleListUrl(id);

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);

                    //                圈子样式：
//                标题：圈子名称，最多显示两行
//                图片：圈子封面图
                    String title = model.getTitle();
                    shareBody.setTitle(YHStringUtils.formatCircle(title));

                    String desc = model.getCircleDesc();
                    shareBody.setDesc(desc + "   ");

                    String contentUrls = model.getCoverUrl();
                    if (!TextUtils.isEmpty(contentUrls)) {
                        shareBody.setImageUrl(contentUrls);
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            toShare(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, listener);
        });
    }

    public static void shareTopicDetail(Activity context, TopicModel model, OnShareRealActionListener realActionListener) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (null == model) {
                return;
            }

//        boolean isBlock = model.isBlock();
            boolean isBlock = false;
            boolean hideBlack = true;
            boolean isMyself = false;//话题不提供删除//
            // 1是审核通过，0是未审核，-1是审核不通过
            int auditStatus = 1;//话题默认通过，通过后app才可见
            int pricavyType = YXConfig.UNUSE_SET_PRIVACY;//无效设置

            String id = model.getId();
            String url = YXConfig.getShareTopicUrl(id, model.getTitle());

            ShareBoardPopup.OnShareItemClickListener listener = position -> {
                if (position == ShareBoardPopup.BOARD_SET_PRIVATE) {
                    if (pricavyType == YXConfig.UNUSE_SET_PRIVACY) {
                        return;
                    }
                    PrivacyPopup privacyPopup = new PrivacyPopup(context, pricavyType, true, id);
                    privacyPopup.setOnItemSelectListener(type -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_PRIVATE, type);
//                    privacyPopup.dismiss();
                    });
                    new XPopup.Builder(context).asCustom(privacyPopup).show();
                } else if (position == ShareBoardPopup.BOARD_SET_DELETE) {
                    AlertPopup alertPopup = new AlertPopup(context, "确定删除吗？");
                    new XPopup.Builder(context).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_DELETE, -1);
                        alertPopup.dismiss();
                    });
                    alertPopup.setOnCancelListener(alertPopup::dismiss);
                } else if (position == ShareBoardPopup.BOARD_SET_REPORT) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_REPORT, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_BLACK) {
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SET_BLACK, -1);
                } else if (position == ShareBoardPopup.BOARD_SHARE_APP) {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }
                    realActionListener.onShareRealAction(ShareBoardPopup.BOARD_SHARE_APP, -1);
                } else if (position == ShareBoardPopup.BOARD_SET_COPY) {
                    // 复制链接
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText(null, url);
                    clipboard.setPrimaryClip(clipData);
                    ToastUtils.showShort("复制成功");
                } else {
                    // 待审核和审核不通过的不可分享
                    // 1是审核通过，0是未审核，-1是审核不通过
                    if (auditStatus == 0) {
                        ToastUtils.showShort("内容待审核，不可分享");
                        return;
                    } else if (auditStatus == -1) {
                        ToastUtils.showShort("内容审核被驳回，不可分享");
                        return;
                    }

                    ShareWebUrlBody shareBody = new ShareWebUrlBody();
                    shareBody.setPosition(position);
                    shareBody.setWebUrl(url);
//
//                话题样式：
//                标题：话题名称，最多显示两行
//                图片：后台有配置显示话题封面图，如无设置使用话题分享默认图
                    String title = model.getTitle();
                    shareBody.setTitle(YHStringUtils.formatTopic(title));

                    String desc = model.getTopicDesc();
                    shareBody.setDesc(desc + "   ");
                    String contentUrls = model.getTitleUrl();
                    if (!TextUtils.isEmpty(contentUrls)) {
                        shareBody.setImageUrl(contentUrls);
                    }

                    SharesUtils.shareWeb(context, shareBody);
                }
            };

            toShare(context, isBlock, hideBlack, isMyself, auditStatus, pricavyType, listener);
        });
    }


    public interface OnShareRealActionListener {
        void onShareRealAction(int position, int type);
    }
}
