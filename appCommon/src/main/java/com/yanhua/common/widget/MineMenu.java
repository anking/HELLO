package com.yanhua.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanhua.common.R;

/**
 * 消息顶部menu
 */
public class MineMenu extends LinearLayout {

    private View viewDot;
    private ImageView ivIcon;
    private TextView tvName;
    private LinearLayout llMenuContainer;

    public MineMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_mine_menu, this, true);

        llMenuContainer = findViewById(R.id.llMenuContainer);
        viewDot = findViewById(R.id.viewDot);
        ivIcon = findViewById(R.id.ivIcon);
        tvName = findViewById(R.id.tvName);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.MineMenu);
        if (attributes != null) {

            boolean isVisible = attributes.getBoolean(R.styleable.MineMenu_isVisible, false);
            String menuName = attributes.getString(R.styleable.MineMenu_menuName);
            int menuIcon = attributes.getResourceId(R.styleable.MineMenu_menuIcon, R.mipmap.ic_msg_fans);

            ivIcon.setImageResource(menuIcon);
            tvName.setText(menuName);
            viewDot.setVisibility(isVisible ? VISIBLE : GONE);
            attributes.recycle();
        }
    }

    /**
     * 控制是否有新消息
     * @param isVisible
     */
    public void setNewMessage(boolean isVisible) {
        viewDot.setVisibility(isVisible ? VISIBLE : GONE);
    }
}
