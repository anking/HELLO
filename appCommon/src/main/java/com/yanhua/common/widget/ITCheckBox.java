package com.yanhua.common.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.yanhua.common.R;
import com.yanhua.core.view.AliIconFontTextView;

/**
 * checkbox_pro
 *
 * <com.yanhua.common.widget.ITCheckBox
 *                 android:id="@+id/checkbox_pro"
 *                 android:layout_centerVertical="true"
 *                 android:layout_height="wrap_content"
 *                 android:layout_width="wrap_content"
 *                 android:textSize="16sp"
 *                 android:text="&#xe727;"
 *                 android:textColor="@color/disable"
 *                 android:layout_alignParentLeft="true"/>
 */
public class ITCheckBox extends AliIconFontTextView {
    private Context mContext;

    public ITCheckBox(Context context) {
        super(context);
        mContext = context;
    }

    public ITCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ITCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public void setChecked(boolean isCheck) {
        if (isCheck) {
            setTextColor(mContext.getResources().getColor(R.color.theme));
        } else {
            setTextColor(mContext.getResources().getColor(R.color.disable));
        }
        setSelected(isCheck);
    }

    public boolean isChecked() {
        return isSelected();
    }
}
