package com.yanhua.common.widget;

import android.content.Context;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;

public class PickMapPopup extends BottomPopupView {

    public PickMapPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_pick_thirdmap;
    }

    public interface OnPickListener {
        void pick(int pos);
    }

    private OnPickListener listener;

    public void setOnPickListener(OnPickListener l) {
        this.listener = l;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.tv_cancel).setOnClickListener(v -> dismiss());
        findViewById(R.id.tv_gaode).setOnClickListener(v -> {
            if (listener != null) {
                listener.pick(1);
                dismiss();
            }
        });
        findViewById(R.id.tv_baidu).setOnClickListener(v -> {
            if (listener != null) {
                listener.pick(2);
                dismiss();
            }
        });
    }
}
