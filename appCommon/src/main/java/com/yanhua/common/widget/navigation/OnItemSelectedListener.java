package com.yanhua.common.widget.navigation;

import com.yanhua.common.widget.navigation.OneBottomNavigationBar;

public interface OnItemSelectedListener {

        void onClick(OneBottomNavigationBar.Item item, int position);

}
