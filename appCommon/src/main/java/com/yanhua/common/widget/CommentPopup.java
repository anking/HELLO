package com.yanhua.common.widget;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.common.R;
import com.yanhua.common.adapter.ContentCommentAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.presenter.CommonAppPresenter;
import com.yanhua.common.presenter.contract.CommonAppContract;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * 评论弹窗
 *
 * @author Administrator
 */
public class CommentPopup extends BaseMvpBottomPopupView<CommonAppPresenter> implements CommonAppContract.IView {
    private Context mContext;

    //父评论列表
    private List<CommentModel> mList = new ArrayList<>();

    //组装后的评论列表
    private List<CommentModel> mCommentList = new ArrayList<>();

    private ContentCommentAdapter mAdapter;
    private int current = 1;

    private String mContentId;
    private RelativeLayout ll_container;
    private TextView et_comment;
    private TextView tv_send;
    private String mUserId;
    private int currentPosition;
    private boolean isReply = false;
    private TextView tv_title;
    private RecyclerView rvComment;
    private int type;
    private int replyCount;

    @Override
    protected void creatPresent() {
        basePresenter = new CommonAppPresenter();
    }

    public CommentPopup(@NonNull Context context, String contentId, String userId) {
        super(context);
        mContext = context;
        mContentId = contentId;
        mUserId = userId;
    }

    public CommentPopup(@NonNull Context context, String contentId, String userId, int tp) {
        super(context);
        mContext = context;
        mContentId = contentId;
        mUserId = userId;
        type = tp;
    }


    public CommentPopup(@NonNull Context context, String contentId, String userId, int tp, int replyNum) {
        super(context);
        mContext = context;
        mContentId = contentId;
        mUserId = userId;
        type = tp;
        replyCount = replyNum;
    }


    /**
     * 回复动作
     *
     * @param model
     */
    private void replyComment(CommentModel model) {
        String commentId = model.getId();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        int childType = model.getChildType();
        showInputPopup(commentId, nickName, childType);
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rvComment = findViewById(R.id.rv_comment);
        SmartRefreshLayout refreshLayout = findViewById(R.id.refreshLayout);
        et_comment = findViewById(R.id.et_comment);
        ll_container = findViewById(R.id.ll_container);
        tv_send = findViewById(R.id.tv_send);

        AliIconFontTextView iv_close = findViewById(R.id.iv_close);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setText("评论 " + replyCount);

        iv_close.setOnClickListener(view -> {
            dismiss();
        });


        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContentCommentAdapter(mContext, mUserId);
        rvComment.setLayoutManager(manager);
        rvComment.setAdapter(mAdapter);

        refreshLayout.setOnRefreshListener(rl -> {
            current = 1;
            refreshLayout.finishRefresh(1000);

            getContentDetail(true, mContentId);

            basePresenter.getCommentList(mContentId, current, 1);
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            current = current + 1;
            if (mList.isEmpty() || mList.size() % 10 != 0) {
                refreshLayout.finishLoadMore(1000, true, true);

//                View footview = LayoutInflater.from(mContext).inflate(R.layout.the_end, rvComment, false);
//                mAdapter.addFooterView(footview);
                return;
            }
            refreshLayout.finishLoadMore(1000);
            basePresenter.getCommentList(mContentId, current, 1);
        });

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);

            replyComment(model);
        });

        mAdapter.setOnADClickListener(clickItem -> {
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);
            PageJumpUtil.adJumpContent(clickItem, (Activity) mContext, -1);
        });

        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            boolean myself = UserManager.getInstance().isMyself(model.getUserId());

            boolean isMyContent = UserManager.getInstance().isMyself(mUserId);

            if (type == YXConfig.TYPE_MOMENT
                    || type == YXConfig.TYPE_INVITE
                    || type == YXConfig.TYPE_BREAK_NEWS
                    || type == YXConfig.TYPE_STRATEGY
                    || type == YXConfig.TYPE_BEST
                    || type == YXConfig.TYPE_NEWS) {
                isMyContent = isMyContent && true;
            } else {
                isMyContent = isMyContent && false;
            }

            CommentBoardPopup popup = new CommentBoardPopup(mContext, isMyContent, true, true, !myself, myself);
            popup.setOnActionItemListener(actionType -> {
                switch (actionType) {
                    case CommentBoardPopup.ACTION_TOP:
                        toTop(pos);
                        break;
                    case CommentBoardPopup.ACTION_REPLY:
                        //回复
                        replyComment(model);
                        break;
                    case CommentBoardPopup.ACTION_COPY:
                        String content = model.getCommentDesc();

                        YHStringUtils.copyContent(mContext, content);
                        break;
                    case CommentBoardPopup.ACTION_REPORT:
                        toReport(pos);
                        break;
                    case CommentBoardPopup.ACTION_DELETE:
                        toDelete(pos);
                        break;
                }
            });

            new XPopup.Builder(mContext).autoFocusEditText(false).asCustom(popup).show();
        });

        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            if (cellType == 0) {
                String userId = model.getUserId();
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userId)
                        .navigation();
            } else if (cellType == 1) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    String commentId = model.getId();
                    basePresenter.updateStarComment(commentId);
                });
            } else if (cellType == 2) {
                int groupPosition = model.getGroupPosition();
                String parentCommentId = mList.get(groupPosition).getId();
                List<CommentModel> childCommentList = mList.get(groupPosition).getChildrens();
                int size = childCommentList.size() + 5;
                basePresenter.getChildCommentList(parentCommentId, 1, size);
            }
        });

        et_comment.setOnClickListener(v -> {
            showInputPopup("", "", 0);
        });

        tv_send.setOnClickListener(view -> {
            showInputPopup("", "", 0);
        });

        // 通过接口获取详情
        getContentDetail(true, mContentId);

        basePresenter.getCommentList(mContentId, current, 1);
    }

    private void getContentDetail(boolean isFirst, String id) {
        if (type == YXConfig.TYPE_MOMENT) {
            basePresenter.getContentDetail(isFirst, id);
        } else if (type == YXConfig.TYPE_BEST) {
            basePresenter.getDiscoContentDetail(isFirst, id);
        }
    }

    private void showInputPopup(String commentId, String nickName, int childType) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!TextUtils.isEmpty(commentId)) {
                isReply = true;
            } else {
                isReply = false;
            }
            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);
            popup.setOnSendCommentListener((comment, ct) -> {
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(mContentId);
                commentForm.setParentCommentId(commentId);

                if (ct != 0) {
                    commentForm.setReplyType(2);
                } else {
                    commentForm.setReplyType(1);
                }
                commentForm.setUserId(UserManager.getInstance().getUserId());
                basePresenter.publishComment(commentForm, type);
            }, childType);

            popup.setOnInputStateListener((focuse) -> {
                setContainerHeight(focuse);
            });
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    /**
     * 置顶评论
     *
     * @param pos
     */
    private void toTop(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();

        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.dealCommentItem2Top(contentId, type, pos);
        });
    }

    @Override
    public void handleDealItemTopSuccess(int pos) {
        if (mAdapter != null) {
            List list = mAdapter.getmDataList();
            Collections.swap(list, pos, 0);
            mAdapter.notifyItemMoved(pos, 0);
        }
    }

    /**
     * 删除评论
     *
     * @param pos
     */
    private void toDelete(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();


        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (!UserManager.getInstance().isMyself(currentUserId)) {
                ToastUtils.showShort("只能删除自己的评论");
                return;
            }

            basePresenter.deleteContent(contentId, pos);
        });
    }

    @Override
    public void handleDeleteCommentSuccess(int pos) {
        if (mAdapter != null) {
            mAdapter.notifyItemRemoved(pos);
            ToastUtils.showShort("评论删除成功");

            replyCount = replyCount - 1;
            tv_title.setText("评论 " + replyCount);
        }
    }

    /**
     * 举报评论
     *
     * @param pos
     */
    private void toReport(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();


        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (UserManager.getInstance().isMyself(currentUserId)) {
                ToastUtils.showShort("不能举报自己的评论");
                return;
            }

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withInt("reportType", YXConfig.reportType.comment)
                    .withString("businessID", contentId)
                    .navigation();
        });
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_comment;
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int sum) {
        if (current == 1) {
            mList.clear();
            if (list != null && !list.isEmpty()) {
                mList = list;

                rvComment.setVisibility(View.VISIBLE);
//                emptyLayout.setVisibility(View.GONE);
            } else {
//                rvComment.setVisibility(View.GONE);
//                emptyLayout.setVisibility(View.VISIBLE);
//                emptyLayout.setErrorType(EmptyLayout.NODATA);
//                emptyLayout.setErrorImag(R.mipmap.ic_nodata_show);
//                emptyLayout.setErrorMessage("还没有人评论我～");
            }

            if (mList.isEmpty() || mList.size() % 10 != 0) {
//                View footview = LayoutInflater.from(mContext).inflate(R.layout.the_end, rvComment, false);
//                mAdapter.addFooterView(footview);
            }
        } else {
            if (list != null && !list.isEmpty()) {
                mList.addAll(list);
            }
        }
        mCommentList = toBuildCommentList();
        mAdapter.setItems(mCommentList);
        setContainerHeight(false);

        if (null != mCommentList && mCommentList.size() > 0 && current == 1) {
            rvComment.scrollToPosition(0);//第一个
        }
    }

    void setContainerHeight(boolean input) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.height = DisplayUtils.getScreenHeight(mContext) * 2 / 3;
        ll_container.setLayoutParams(layoutParams);
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();

        setContainerHeight(false);
    }

    @Override
    public void handleChildCommentList(ListResult<CommentModel> listResult) {
        if (listResult != null) {
            int total = listResult.getTotal();
            CommentModel model = mCommentList.get(currentPosition);
            int groupPosition = model.getGroupPosition();
            mList.get(groupPosition).setChildCount(total);
            List<CommentModel> list = listResult.getRecords();
            if (list != null && !list.isEmpty()) {
                mList.get(groupPosition).setChildrens(list);
            }
            mCommentList = toBuildCommentList();
            mAdapter.setItems(mCommentList);
            setContainerHeight(false);
        }
    }

    private List<CommentModel> toBuildCommentList() {
        List<CommentModel> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            CommentModel model = mList.get(i);

            int resultType = model.getResultType();
            if (resultType == 1) {

                //这是父评论
                model.setType(0);
                model.setGroupPosition(i);
                list.add(model);
                List<CommentModel> childrenList = model.getChildrens();
                int childCount = model.getChildCount();
                int count = 0;

                int childSize = childrenList.size();
                for (int j = 0; j < childSize; j++) {
                    count = count + 1;
                    CommentModel childrenComment = childrenList.get(j);
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());

                    if (j == 0) {
                        if (childSize == 1) {
                            childrenComment.setChildType(3);
                        } else {
                            childrenComment.setChildType(1);
                        }
                    } else if (j == childSize - 1) {
                        childrenComment.setChildType(2);
                    } else {
                        childrenComment.setChildType(0);
                    }

                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }
            } else if (resultType == 2) {//广告
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");
            EventBus.getDefault().post(new MessageEvent(CommonConstant.REFRESH_CONTENT_COMMENTS_COUNT));
            if (!isReply) {
                current = 1;

                replyCount = replyCount + 1;
                tv_title.setText("评论 " + replyCount);

//                getContentDetail(false, mContentId);

                basePresenter.getCommentList(mContentId, current, 1);
            } else {
                CommentModel preModel = mCommentList.get(currentPosition);
                int groupPosition = preModel.getGroupPosition();
                int type = preModel.getType();
                if (type == 0) {
                    model.setType(1);
                } else {
                    preModel.setType(1);
                    model.setType(type);
                    //设置回复
                    model.setReplyStatus(1);
                    String preNick = YHStringUtils.pickName(preModel.getNickName(), preModel.getFriendRemark());
                    model.setReplyName(preNick);
                }
                model.setGroupPosition(groupPosition);


                UserInfo userInfo = UserManager.getInstance().getUserInfo();
                String nickName = userInfo.getNickName();
                String userPhoto = userInfo.getImg();
                String userId = userInfo.getId();

                if (TextUtils.isEmpty(model.getUserPhoto())) {
                    model.setUserPhoto(userPhoto);
                }

                if (TextUtils.isEmpty(model.getNickName())) {
                    model.setNickName(nickName);
                }

                if (TextUtils.isEmpty(model.getUserId())) {
                    model.setUserId(userId);
                }

                mCommentList.add(currentPosition + 1, model);
                mAdapter.setItems(mCommentList);
                setContainerHeight(false);

            }
        }
    }

    @Override
    public void updateCollectContentSuccess() {

    }

    @Override
    public void updateStarContentSuccess() {

    }

    @Override
    public void updateStarCommentSuccess() {
        CommentModel model = mCommentList.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setIsFabulous(!model.isIsFabulous());

        model.setFabulousCount(model.isIsFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowUserSuccess() {

    }

    @Override
    public void handleContentDetailSuccess(boolean isFirst, MomentListModel model) {
        if (null != model) {
//            this.replyCount = model.getReplyCount();
//
//            tv_title.setText("评论 " + replyCount);
        }
    }

    @Override
    protected int getMaxHeight() {
        int sh = DisplayUtils.getScreenHeight(mContext);

        return sh * 2 / 3;
    }

    @Override
    public void handleErrorMsg(String msg) {
        ToastUtils.showShort(msg);
    }

}
