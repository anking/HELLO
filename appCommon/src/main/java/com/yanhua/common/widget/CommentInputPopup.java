package com.yanhua.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.model.CommentInputTemp;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.core.view.AutoClearEditText;

/**
 * @author Administrator
 */
public class CommentInputPopup extends BottomPopupView {

    private Context mContext;
    private String mNickName;


    public interface OnSendCommentListener {
        void onSendComment(String comment,int childType);
    }

    private OnSendCommentListener mListener;
    private int mChildType;

    public void setOnSendCommentListener(OnSendCommentListener listener,int childType) {
        this.mListener = listener;
        this.mChildType = childType;
    }

    public interface OnInputStateListener {
        void inputSate(boolean focuse);
    }

    private OnInputStateListener inputSateListener;

    public void setOnInputStateListener(OnInputStateListener listener) {
        this.inputSateListener = listener;
    }

    public CommentInputPopup(@NonNull Context context, String nickName) {
        super(context);
        this.mContext = context;
        this.mNickName = nickName;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_comment_input;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        AutoClearEditText et_comment = findViewById(R.id.et_comment);
        et_comment.requestFocus();

        et_comment.setOnFocusChangeListener((v, hasFocus) -> {
            if (null != inputSateListener) {
                inputSateListener.inputSate(hasFocus);
            }
        });

        TextView tv_send = findViewById(R.id.tv_send);

        String nickTemp = CommentInputTemp.getNick();//获取上次的昵称
        if (null != nickTemp && nickTemp.equals(this.mNickName)) {
            String contentTemp = CommentInputTemp.getContent();
            if (null != nickTemp && !TextUtils.isEmpty(contentTemp)) {
                et_comment.setText(contentTemp);
                et_comment.setSelection(contentTemp.length());
            }
        }

        if (!TextUtils.isEmpty(mNickName)) {
            et_comment.setHint("回复@" + mNickName);
        } else {
            et_comment.setHint("说点好玩的");
        }

        tv_send.setOnClickListener(view -> {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                String comment = et_comment.getText().toString().trim();
                if (TextUtils.isEmpty(comment)) {
                    ToastUtils.showShort("请输入评论内容~");
                    return;
                }
                dismiss();

                if (mListener != null) {
                    mListener.onSendComment(comment,mChildType);
                    //发送成功那里
                    et_comment.setText("");
                    CommentInputTemp.setNick("");
                    CommentInputTemp.setContent("");
                }
            });
        });
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
        if (null != inputSateListener) {
            inputSateListener.inputSate(false);
        }
        AutoClearEditText et_comment = findViewById(R.id.et_comment);
        CommentInputTemp.setNick(this.mNickName);
        String content = et_comment.getText().toString().trim();
        if (!TextUtils.isEmpty(content)) {
            CommentInputTemp.setContent(et_comment.getText().toString().trim());
        }
    }
}
