package com.yanhua.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.core.DrawerPopupView;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.R;
import com.yanhua.common.adapter.InviteFilterAdapter;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.GridSpaceItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class InviteFilterDrawerPopup extends DrawerPopupView {

    private Context mContext;

    private TextView tvCurrentCity, tvHisCity1, tvHisCity2;
    private RecyclerView rvContent;

    private RelativeLayout btnCancel;
    private Button btnConfirm;
    private AutoClearEditText etStart, etEnd;

    private ArrayList<InviteTypeModel> mFilterList;
    private ArrayList<InviteTypeModel> mList;
    private String mSelectCity, mStartTime, mEndTime;

    private InviteFilterAdapter mAdapter;

    private OnButtonConfirmClickListener listener;

    public void setOnButtonConfirmClickListener(OnButtonConfirmClickListener l) {
        listener = l;
    }

    public InviteFilterDrawerPopup(@NonNull Context context, String selectCity, ArrayList<InviteTypeModel> list, ArrayList<InviteTypeModel> filterList, String startTime, String endTime) {
        super(context);
        mContext = context;
        this.mSelectCity = selectCity;
        this.mList = list;
        this.mFilterList = filterList;
        this.mStartTime = startTime;
        this.mEndTime = endTime;
    }

    /**
     * 更新选中的内容列表
     *
     * @param filterList
     */
    public void setFilterList(ArrayList<InviteTypeModel> filterList) {
        this.mFilterList = filterList;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.invite_filter_condition;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        tvCurrentCity = findViewById(R.id.tvCurrentCity);
        tvHisCity1 = findViewById(R.id.tvHisCity1);
        tvHisCity2 = findViewById(R.id.tvHisCity2);

        etStart = findViewById(R.id.etStart);
        etEnd = findViewById(R.id.etEnd);

        tvCurrentCity.setText(mSelectCity);
        String cityContent = DiscoCacheUtils.getInstance().getCityHistoryList();
        String[] cities = cityContent.split(",");

        List<String> cityList = new ArrayList<>();
        if (cities.length > 0) {
            for (int i = 0; i < cities.length; i++) {
                if (!mSelectCity.equals(cities[i])) {
                    cityList.add(cities[i]);
                }
            }
        }

        int length = cityList.size();

        if (length >= 2) {
            tvHisCity1.setText(cityList.get(0));
            tvHisCity2.setText(cityList.get(1));
        } else if (length < 2 && length > 0 && !TextUtils.isEmpty(cityList.get(0))) {
            tvHisCity1.setText(cityList.get(0));
            tvHisCity2.setVisibility(INVISIBLE);
        } else {
            tvHisCity1.setVisibility(INVISIBLE);
            tvHisCity2.setVisibility(INVISIBLE);
        }

        tvCurrentCity.setOnClickListener(v -> {
            mSelectCity = tvCurrentCity.getText().toString().trim();
            confirmSelect();
        });

        tvHisCity1.setOnClickListener(v -> {
            mSelectCity = tvHisCity1.getText().toString().trim();
            confirmSelect();
        });

        tvHisCity2.setOnClickListener(v -> {
            mSelectCity = tvHisCity2.getText().toString().trim();
            confirmSelect();
        });
        //////////////////////////////////////////////////////////////////////////////////////////////////
        btnConfirm = findViewById(R.id.btnConfirm);
        btnCancel = findViewById(R.id.btnCancel);
        rvContent = findViewById(R.id.rvContent);

        if (TextUtils.isEmpty(mStartTime)) {
            etStart.setHint("不限制");
        } else {
            etStart.setText(mStartTime);
        }
        if (TextUtils.isEmpty(mEndTime)) {
            etEnd.setHint("不限制");
        } else {
            etEnd.setText(mEndTime);
        }

        GridLayoutManager mSubContentManager = new GridLayoutManager(mContext, 3, GridLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mSubContentManager);

        mAdapter = new InviteFilterAdapter(mContext, mFilterList);
        rvContent.setAdapter(mAdapter);
        rvContent.addItemDecoration(new GridSpaceItemDecoration(3, DisplayUtils.dip2px(mContext, 12), DisplayUtils.dip2px(mContext, 12)));
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setItems(mList);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            InviteTypeModel item = mList.get(pos);
            if (mFilterList.contains(item)) {
                mFilterList.remove(item);
            } else {
                mFilterList.add(item);
            }
            setFilterList(mFilterList);
        });

        btnConfirm.setOnClickListener(v -> {
            mSelectCity = tvCurrentCity.getText().toString().trim();
            confirmSelect();
        });

        btnCancel.setOnClickListener(v -> {
            if (null != listener) {
                listener.onBtnConfirm(mSelectCity, true, mFilterList, mStartTime, mEndTime);
            }
            dismissOrHideSoftInput();
        });

        findViewById(R.id.llReLocation).setOnClickListener(v -> ARouter.getInstance().build(ARouterPath.CITY_ACTIVITY)
                .withString("city", tvCurrentCity.getText().toString())
                .withString("location", DiscoCacheUtils.getInstance().getCurrentCity())
                .withString("plongitude", DiscoCacheUtils.getInstance().getLongitude())
                .withString("platitude", DiscoCacheUtils.getInstance().getLatitude())
                .navigation());

        EventBus.getDefault().register(this);
    }

    private void confirmSelect() {
        String startTime = etStart.getText().toString().trim();
        String endTime = etEnd.getText().toString().trim();

        if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
            if (startTime.compareTo(endTime) >= 0) {
                ToastUtils.showShort("开始时间需要比结束时间早");
                return;
            }
            mStartTime = startTime;
            mEndTime = endTime;
        } else if (!TextUtils.isEmpty(startTime) && TextUtils.isEmpty(endTime)) {

            mStartTime = startTime;
            mEndTime = null;
        } else if (TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
            mStartTime = null;
            mEndTime = endTime;
        } else {
            mStartTime = null;
            mEndTime = null;
        }

        if (null != listener) {

            listener.onBtnConfirm(mSelectCity, false, mFilterList, mStartTime, mEndTime);
        }
        dismissOrHideSoftInput();
    }

    public interface OnButtonConfirmClickListener {
        void onBtnConfirm(String selectCity, boolean reset, ArrayList<InviteTypeModel> filterList, String startTime, String endTime);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventName().equals(EventName.SELECT_CITY)) {
            CityModel cityModel = (CityModel) event.getSerializable();

            tvCurrentCity.setText(cityModel.getAreaName());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }
}
