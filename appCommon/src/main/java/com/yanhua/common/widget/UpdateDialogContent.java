package com.yanhua.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.xuexiang.xupdate._XUpdate;
import com.xuexiang.xupdate.entity.UpdateEntity;
import com.xuexiang.xupdate.proxy.IUpdateProxy;
import com.xuexiang.xupdate.service.OnFileDownloadListener;
import com.xuexiang.xupdate.utils.UpdateUtils;
import com.xuexiang.xupdate.widget.IDownloadEventHandler;
import com.xuexiang.xupdate.widget.WeakFileDownloadListener;
import com.yanhua.base.dialog.BaseDialogFragment;
import com.yanhua.base.dialog.DialogLayoutCallback;
import com.yanhua.common.R;

import java.io.File;

public class UpdateDialogContent extends BaseDialogFragment {

    private RelativeLayout cdcTitleRl;
    private TextView cdcTitleTv;
    private RelativeLayout cdcContentRl;
    private TextView cdcContentTv;
    private RelativeLayout cdcBottomRl;
    private TextView cdcBottomPositiveTv;
    private TextView cdcBottomNegativeTv;


    public UpdateDialogContent init(Context context, UpdateEntity updateEntity, final @NonNull IUpdateProxy updateProxy) {
        super.init(context, new DialogLayoutCallback() {
            @Override
            public int bindTheme() {
                return R.style.CommonContentDialogStyle;
            }

            @Override
            public int bindLayout() {
                return R.layout.update_dialog_content;
            }

            @Override
            public void initView(final BaseDialogFragment dialog, View contentView) {
                cdcTitleRl = contentView.findViewById(R.id.cdcTitleRl);
                cdcTitleTv = contentView.findViewById(R.id.cdcTitleTv);
                cdcContentRl = contentView.findViewById(R.id.cdcContentRl);
                cdcContentTv = contentView.findViewById(R.id.cdcContentTv);
                cdcBottomRl = contentView.findViewById(R.id.cdcBottomRl);
                cdcBottomPositiveTv = contentView.findViewById(R.id.cdcBottomPositiveTv);
                cdcBottomNegativeTv = contentView.findViewById(R.id.cdcBottomNegativeTv);


                String updateInfo = UpdateUtils.getDisplayUpdateInfo(updateProxy.getContext(), updateEntity);

                CharSequence title = "版本更新";//String.format("是否升级到%s版本？", updateEntity.getVersionName())
                CharSequence content = updateInfo;
                Pair<CharSequence, View.OnClickListener> positiveBtnAction = new Pair<>("升级", v -> {
                    if (updateEntity != null && updateEntity.isForce()) {
                        updateProxy.startDownload(updateEntity, new OnFileDownloadListener() {
                            @Override
                            public void onStart() {
                                HProgressDialogUtils.showHorizontalProgressDialog(updateProxy.getContext(), "下载进度", false);
                            }

                            @Override
                            public void onProgress(float progress, long total) {
                                HProgressDialogUtils.setProgress(Math.round(progress * 100));
                            }

                            @Override
                            public boolean onCompleted(File file) {
                                HProgressDialogUtils.cancel();
                                return true;
                            }

                            @Override
                            public void onError(Throwable throwable) {
                                HProgressDialogUtils.cancel();
                            }
                        });
                    } else {
                        //点击后台更新按钮
                        if (updateProxy != null) {
                            updateProxy.startDownload(updateEntity, new WeakFileDownloadListener(new IDownloadEventHandler() {
                                @Override
                                public void handleStart() {
                                    ToastUtils.showShort("新版本客户端正在下载");

                                    updateProxy.backgroundDownload();
                                }

                                @Override
                                public void handleProgress(float progress) {

                                }

                                @Override
                                public boolean handleCompleted(File apkFile) {
                                    if (updateEntity.isForce()) {
                                        _XUpdate.startInstallApk(getContext(), apkFile, updateEntity.getDownLoadEntity());
                                    } else {
                                        UpdateDialogContent.this.dismissAllowingStateLoss();
                                    }

                                    // 返回true，自动进行apk安装
                                    return true;
                                }

                                @Override
                                public void handleError(Throwable throwable) {
                                    ToastUtils.showShort("更新出现异常，请退出后再打开重试");
                                }
                            }));
                        }
                        dismissAllowingStateLoss();
                    }
                });
                Pair<CharSequence, View.OnClickListener> negativeBtnAction = new Pair<>("取消", v -> {
                    if (updateEntity != null && updateEntity.isForce()) {
                        // 如果是强制更新，退出app
                        dismissAllowingStateLoss();

                        System.exit(0);
                    } else {
                        dismissAllowingStateLoss();
                    }
                });
                setCancelable(!updateEntity.isForce());

                if (TextUtils.isEmpty(title)) {
                    cdcTitleRl.setVisibility(View.GONE);
                } else {
                    cdcTitleTv.setText(title);
                }
                if (TextUtils.isEmpty(content)) {
                    cdcContentRl.setVisibility(View.GONE);
                } else {
                    cdcContentTv.setText(content);
                }

                if (positiveBtnAction == null && negativeBtnAction == null) {
                    cdcBottomRl.setVisibility(View.GONE);
                } else {
                    if (positiveBtnAction != null) {
                        ClickUtils.applyPressedBgDark(cdcBottomPositiveTv);
                        cdcBottomPositiveTv.setText(positiveBtnAction.first);
                        cdcBottomPositiveTv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dismiss();
                                positiveBtnAction.second.onClick(v);
                            }
                        });
                    }
                    if (negativeBtnAction != null) {
                        ClickUtils.applyPressedBgDark(cdcBottomNegativeTv);
                        cdcBottomNegativeTv.setText(negativeBtnAction.first);
                        cdcBottomNegativeTv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dismiss();
                                negativeBtnAction.second.onClick(v);
                            }
                        });
                    }
                }
            }

            @Override
            public void setWindowStyle(Window window) {
            }

            @Override
            public void onCancel(BaseDialogFragment dialog) {

            }

            @Override
            public void onDismiss(BaseDialogFragment dialog) {

            }
        });
        return this;
    }

}
