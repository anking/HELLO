package com.yanhua.common.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.reflect.TypeToken;
import com.lqr.adapter.LQRAdapterForRecyclerView;
import com.lqr.adapter.LQRViewHolderForRecyclerView;
import com.lqr.recyclerview.LQRRecyclerView;
import com.lxj.xpopup.core.CenterPopupView;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AutoClearEditText;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 这里其实可以将各个大类的分享逻辑统一在这里处理即可
 *
 * @author Administrator
 */
public class RongIMShareContentDialog<T> extends CenterPopupView {

    private Context mContext;

    LQRRecyclerView rvMember;
    ImageView ivCover;
    ImageView ivCoverType;
    TextView tvTitle;
    TextView tvDesc;
    AutoClearEditText sendMsy;
    TextView btn_cancel;
    TextView btn_confirm;
    T mObj;
    int shareType;
    private List<ShareObjectModel> mDataList;

    public interface OnButtonClickListener {
        void onClick(String msg);
    }

    private OnButtonClickListener mConfirmListener;
    private OnButtonClickListener mCancelListener;

    public RongIMShareContentDialog(@NonNull Context context, List<ShareObjectModel> dataList, T t, int type) {
        super(context);
        mContext = context;
        mDataList = dataList;
        mObj = t;

        shareType = type;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.layout_rongim_share_content_dialog;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rvMember = findViewById(R.id.rvMember);

        ivCover = findViewById(R.id.ivCover);
        ivCoverType = findViewById(R.id.ivCoverType);
        tvTitle = findViewById(R.id.tvTitle);
        tvDesc = findViewById(R.id.tvDesc);
        sendMsy = findViewById(R.id.sendMsy);

        btn_cancel = findViewById(R.id.btn_cancel);
        btn_confirm = findViewById(R.id.btn_confirm);

        btn_cancel.setOnClickListener(view -> {
            if (mCancelListener != null) {
                mCancelListener.onClick(sendMsy.getText().toString());
            }
        });

        btn_confirm.setOnClickListener(view -> {
            if (mConfirmListener != null) {
                mConfirmListener.onClick(sendMsy.getText().toString());
            }
        });

        setAdapter();

        initData(mObj);
    }

    private void initData(T tt) {
        if (tt instanceof MomentListModel) {//此刻
            MomentListModel model = (MomentListModel) tt;
            int contentType = model.getContentType();

            String content = "";
            if (contentType == 3) {
                content = YHStringUtils.pickLastFirst(model.getIntroduce(), model.getContent());
                content = YHStringUtils.getHtmlContent(content);
            } else {
                content = model.getContent();

                String urlJson = model.getContentUrl();
                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    String coverUrl = fileResults.get(0).getHttpUrl();

                    if (contentType == 2) {
                        ivCoverType.setVisibility(VISIBLE);
                    } else {
                        ivCoverType.setVisibility(GONE);
                    }

                    ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl, R.drawable.place_holder);
                }
            }

            String title = model.getNickName();
            if (shareType == YXConfig.TYPE_INVITE) {
                title = title + "的此刻";
            } else if (shareType == YXConfig.TYPE_BEST) {
                title = title + "的电音集锦";
            }

            tvTitle.setText(title);
            if (TextUtils.isEmpty(content)) {
                content = "分享" + (contentType == 2 ? "视频" : "图片");
            }
            tvDesc.setText(content);
        } else if (tt instanceof NewsDetailModel) {
            NewsDetailModel model = (NewsDetailModel) tt;

            String content = "";
            String introduce = model.getIntroduction();
            if (TextUtils.isEmpty(introduce)) {
                content = model.getContent();
                content = YHStringUtils.getHtmlContent(content);
            } else {
                content = introduce;
            }

            tvTitle.setMaxLines(2);
            tvTitle.setText(model.getContentTitle());
            tvDesc.setText(content);


            String coverUrl = model.getCoverUrl();
            if (!TextUtils.isEmpty(coverUrl)) {
                boolean isVideo = FileUtils.isVideo(coverUrl);

                if (!isVideo) {
                    ivCoverType.setVisibility(View.GONE);
                    ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl, R.drawable.place_holder);
                } else {
                    ivCoverType.setVisibility(View.VISIBLE);

                    RequestOptions options = new RequestOptions();

                    options.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .frame(1000000)
                            .centerCrop();

                    Glide.with(mContext)
                            .setDefaultRequestOptions(options)
                            .asBitmap()
                            .load(coverUrl)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    ivCover.setImageBitmap(resource);
                                }
                            });
                }
            } else {
                ivCover.setImageResource(R.drawable.place_holder);
            }

            tvDesc.setVisibility(GONE);
        } else if (tt instanceof TopicModel) {
            TopicModel mTopic = (TopicModel) tt;
            ImageLoaderUtil.loadImgCenterCrop(ivCover, mTopic.getTitleUrl(), R.drawable.place_holder);

            tvTitle.setText("#" + YHStringUtils.value(mTopic.getTitle()) + "#");
            tvDesc.setVisibility(GONE);
        } else if (tt instanceof CircleModel) {
            CircleModel mModle = (CircleModel) tt;
            ImageLoaderUtil.loadImgCenterCrop(ivCover, mModle.getCoverUrl(), R.drawable.place_holder);

            tvTitle.setText("◎" + mModle.getTitle());
            tvDesc.setVisibility(GONE);
        } else if (tt instanceof UserInfo) {
            UserInfo mModle = (UserInfo) tt;

            ImageLoaderUtil.loadImgCenterCrop(ivCover, mModle.getImg(), R.drawable.place_holder);

            tvTitle.setText("分享用户");
            tvDesc.setText(mModle.getNickName());
        } else if (tt instanceof InviteModel) {
            InviteModel mModel = (InviteModel) tt;

            String coverUrl = mModel.getCoverUrl();
            if (!TextUtils.isEmpty(coverUrl)) {
                boolean isVideo = FileUtils.isVideo(coverUrl);

                if (!isVideo) {
                    ivCoverType.setVisibility(View.GONE);
                    ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl, R.drawable.place_holder);
                } else {
                    ivCoverType.setVisibility(View.VISIBLE);

                    RequestOptions options = new RequestOptions();

                    options.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .frame(1000000)
                            .centerCrop();

                    Glide.with(mContext)
                            .setDefaultRequestOptions(options)
                            .asBitmap()
                            .load(coverUrl)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    ivCover.setImageBitmap(resource);
                                }
                            });
                }
            } else {
                ivCover.setImageResource(R.drawable.place_holder);
            }

            tvTitle.setText("分享" + mModel.getUserName() + "的约伴");
            tvDesc.setText(mModel.getTitle());
        } else if (tt instanceof BarModel) {
            BarModel barModel = (BarModel) tt;

            ImageLoaderUtil.loadImgCenterCrop(ivCover, barModel.getBarPrimaryImg(), R.drawable.place_holder);

            tvTitle.setText(barModel.getName());
            tvDesc.setText(barModel.getAddress());
        }
    }

    private void setAdapter() {
        LQRAdapterForRecyclerView<ShareObjectModel> mAdapter = new LQRAdapterForRecyclerView<ShareObjectModel>(mContext, mDataList, R.layout.item_share_member_info) {
            @Override
            public void convert(LQRViewHolderForRecyclerView helper, ShareObjectModel item, int position) {
                ImageView ivHeader = helper.getView(R.id.ivHeader);

                Glide.with(mContext).load(item.getImg()).centerCrop().into(ivHeader);

                TextView tvName = helper.getView(R.id.tvName);
                if (mDataList.size() < 2) {
                    tvName.setVisibility(VISIBLE);
                    tvName.setText(item.getNickName());
                } else {
                    tvName.setVisibility(GONE);
                }
            }
        };
        rvMember.setAdapter(mAdapter);
    }


    public void setOnConfirmListener(OnButtonClickListener listener) {
        this.mConfirmListener = listener;
    }

    public void setOnCancelListener(OnButtonClickListener listener) {
        this.mCancelListener = listener;
    }
}
