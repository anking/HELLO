package com.yanhua.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.core.view.AutoClearEditText;

/**
 * @author Administrator
 */
public class AddKeyWordPopup extends BottomPopupView {

    private Context mContext;

    public interface OnSendCommentListener {
        void onSendComment(String comment);
    }

    private OnSendCommentListener mListener;

    public void setOnSendCommentListener(OnSendCommentListener listener) {
        this.mListener = listener;
    }

    public AddKeyWordPopup(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_add_key_word;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        AutoClearEditText et_comment = findViewById(R.id.et_comment);
        et_comment.requestFocus();

        TextView tv_send = findViewById(R.id.tv_send);
        tv_send.setOnClickListener(view -> {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                String comment = et_comment.getText().toString().trim();
                if (TextUtils.isEmpty(comment)) {
                    ToastUtils.showShort("请输入关键字");
                    return;
                }
                dismiss();

                if (mListener != null) {
                    mListener.onSendComment(comment);
                    //发送成功那里
                    et_comment.setText("");
                }
            });
        });
    }
}
