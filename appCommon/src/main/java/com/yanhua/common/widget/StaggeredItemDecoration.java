package com.yanhua.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class StaggeredItemDecoration extends RecyclerView.ItemDecoration {

    private int specLeft;
    private int specTop;
    private int specRight;
    private int specBottom;

    public StaggeredItemDecoration(Context context, int dpLeftValue, int dpTopValue, int dpRightValue, int dpBottomValue) {
        specLeft = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpLeftValue, context.getResources().getDisplayMetrics());
        specTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpTopValue, context.getResources().getDisplayMetrics());
        specRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpRightValue, context.getResources().getDisplayMetrics());
        specBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpBottomValue, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
    }


    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
    }


    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.set(specLeft, specTop, specRight, specBottom);
    }
}


