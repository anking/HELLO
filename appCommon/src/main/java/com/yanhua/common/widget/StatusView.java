package com.yanhua.common.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanhua.common.R;
import com.yanhua.core.view.AliIconFontTextView;

public class StatusView extends LinearLayout {

    private LinearLayout llBind;
    private AliIconFontTextView ivIcon;
    private TextView tvStatus;

    private String statusNormalName, statusSelectName;

    @SuppressLint("ResourceAsColor")
    private ColorStateList statusTextBindColor = ColorStateList.valueOf(R.color.text_bind_status);
    private int statusBindBgRes;

    @SuppressLint("ResourceAsColor")
    public StatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_status, this, true);

        llBind = findViewById(R.id.llBind);
        ivIcon = findViewById(R.id.ivIcon);
        tvStatus = findViewById(R.id.tvStatus);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.StatusView);
        if (attributes != null) {
            boolean status = attributes.getBoolean(R.styleable.StatusView_status, false);

            statusNormalName = attributes.getString(R.styleable.StatusView_statusNormalName);
            statusSelectName = attributes.getString(R.styleable.StatusView_statusSelectName);

            statusTextBindColor = attributes.getColorStateList(R.styleable.StatusView_statusTextBindColor);

            statusBindBgRes = attributes.getResourceId(R.styleable.StatusView_statusBindBgRes, R.drawable.selector_bind_status);

            String statusIcon = attributes.getString(R.styleable.StatusView_statusIcon);

            ivIcon.setText(statusIcon);
            tvStatus.setText(status ? statusSelectName : statusNormalName);

            ivIcon.setTextColor(statusTextBindColor);
            tvStatus.setTextColor(statusTextBindColor);
            llBind.setBackgroundResource(statusBindBgRes);

            attributes.recycle();

            setStatus(status);
        }
    }

    public void setStatus(boolean status) {
        llBind.setSelected(status);
        ivIcon.setVisibility(status ? GONE : VISIBLE);

        ivIcon.setSelected(status);
        tvStatus.setSelected(status);

        tvStatus.setText(status ? statusSelectName : statusNormalName);
    }

    public void setStatus(boolean status, String statusText) {
        llBind.setSelected(status);
        ivIcon.setVisibility(status ? GONE : VISIBLE);

        ivIcon.setSelected(status);
        tvStatus.setSelected(status);

        tvStatus.setText(statusText);
    }

    public boolean getStatus() {
        return llBind.isSelected();
    }
}
