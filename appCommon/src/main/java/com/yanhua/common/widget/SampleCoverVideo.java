package com.yanhua.common.widget;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lxj.xpopup.XPopup;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.utils.CommonUtil;
import com.shuyu.gsyvideoplayer.utils.Debuger;
import com.shuyu.gsyvideoplayer.utils.GSYVideoType;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.gsyvideoplayer.video.base.GSYBaseVideoPlayer;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.SystemUtil;
import com.yanhua.core.view.CommonDialog;

/**
 * 带封面
 */

public class SampleCoverVideo extends StandardGSYVideoPlayer {

    ImageView mCoverImage;
    TextView mTvTime;
    FrameLayout surfaceContainer;

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;

        mTvTime.setText(CommonUtil.stringForTime(videoTime * 1000));//后台数据是秒的

        GSYVideoManager.instance().setNeedMute(true);
    }

    private int videoTime;
    String mCoverOriginUrl;

    int mCoverOriginId = 0;

    int mDefaultRes;

    public SampleCoverVideo(Context context) {
        super(context);
    }

    public SampleCoverVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        mCoverImage = (ImageView) findViewById(R.id.thumbImage);

        mTvTime = findViewById(R.id.tv_time);
        surfaceContainer = findViewById(R.id.surface_container);

        if (mThumbImageViewLayout != null &&
                (mCurrentState == -1 || mCurrentState == CURRENT_STATE_NORMAL || mCurrentState == CURRENT_STATE_ERROR)) {
            mThumbImageViewLayout.setVisibility(VISIBLE);
        }

        mTextureViewContainer.setOnClickListener(v -> {
            if (null != mOnDataNetPlayListener) {// 点击视频的其他区域进行跳转详情
                mOnDataNetPlayListener.detail();
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.video_layout_cover;
    }

    public void loadCoverImage(String url, int res) {
        mCoverOriginUrl = url;
        mDefaultRes = res;
        ImageLoaderUtil.loadImg(mCoverImage, url);
    }

    public void loadCoverImage(String url, int res, int w, int h) {
        mCoverOriginUrl = url;
        mDefaultRes = res;

        int sw = DisplayUtils.getScreenWidth(this.getContext());//设置图片的宽
        int sh = sw * 3 / 4;

        if (w > 0 && h > 0) {
            //width, height
            float rate = DisplayUtils.getImageRate(w, h);//4:3 1:1 3:4
            sh = (int) (sw / rate);
        }

        Glide.with(getContext().getApplicationContext())
                .setDefaultRequestOptions(
                        new RequestOptions()
                                .frame(1000000)
                                .centerCrop()
                                .error(res)
                                .placeholder(res)
                                .override(sw, sh))
                .load(url)
                .into(mCoverImage);
    }

    public void loadCoverImageBy(int id, int res) {
        mCoverOriginId = id;
        mDefaultRes = res;
        mCoverImage.setImageResource(id);
    }

    @Override
    protected void updateStartImage() {
        /*ImageView imageView = (ImageView) mStartButton;
        if (mCurrentState == CURRENT_STATE_PLAYING) {
            imageView.setImageResource(R.drawable.video_click_pause_selector);
        } else if (mCurrentState == CURRENT_STATE_ERROR) {
            imageView.setImageResource(R.drawable.video_click_error_selector);
        } else {
            imageView.setImageResource(R.drawable.video_play_pressed);
        }*/
        if (mStartButton instanceof ImageView) {
            ImageView imageView = (ImageView) mStartButton;
            if (mCurrentState == CURRENT_STATE_PLAYING) {
//                imageView.setImageResource(R.drawable.ic_discover_pause);
                imageView.setImageResource(R.drawable.bg_tran_circle_no_stroke);
            } else if (mCurrentState == CURRENT_STATE_ERROR) {
                imageView.setImageResource(R.drawable.video_click_error_selector);
            } else {
                imageView.setImageResource(R.mipmap.ic_discover_play);
            }
        }
    }

    @Override
    protected void setProgressAndTime(int progress, int secProgress, int currentTime, int totalTime, boolean forceChange) {
        if (mGSYVideoProgressListener != null && mCurrentState == CURRENT_STATE_PLAYING) {
            mGSYVideoProgressListener.onProgress(progress, secProgress, currentTime, totalTime);
        }

        if (mProgressBar == null || mTotalTimeTextView == null || mCurrentTimeTextView == null) {
            return;
        }
        if (mHadSeekTouch) {
            return;
        }
        if (!mTouchingProgressBar) {
            if (progress != 0 || forceChange) mProgressBar.setProgress(progress);
        }
        if (getGSYVideoManager().getBufferedPercentage() > 0) {
            secProgress = getGSYVideoManager().getBufferedPercentage();
        }
        if (secProgress > 94) secProgress = 100;
        setSecondaryProgress(secProgress);
        mTotalTimeTextView.setText(CommonUtil.stringForTime(totalTime));
        if (currentTime > 0)
            mCurrentTimeTextView.setText(CommonUtil.stringForTime(currentTime));

        if (mBottomProgressBar != null) {
            if (progress != 0 || forceChange) mBottomProgressBar.setProgress(progress);
            setSecondaryProgress(secProgress);
        }

        mTvTime.setText(CommonUtil.stringForTime(totalTime - currentTime));
    }


    @Override
    public GSYBaseVideoPlayer startWindowFullscreen(Context context, boolean actionBar, boolean statusBar) {
        GSYBaseVideoPlayer gsyBaseVideoPlayer = super.startWindowFullscreen(context, actionBar, statusBar);
        TXSampleCoverVideo sampleCoverVideo = (TXSampleCoverVideo) gsyBaseVideoPlayer;
        if (mCoverOriginUrl != null) {
            int width = getCurrentVideoWidth();
            int height = getCurrentVideoHeight();
            sampleCoverVideo.loadCoverImage(mCoverOriginUrl, mDefaultRes, width, height);
        } else if (mCoverOriginId != 0) {
            sampleCoverVideo.loadCoverImageBy(mCoverOriginId, mDefaultRes);
        }

        return gsyBaseVideoPlayer;
    }

    @Override
    public GSYBaseVideoPlayer showSmallVideo(Point size, boolean actionBar, boolean statusBar) {
        //下面这里替换成你自己的强制转化
        SampleCoverVideo sampleCoverVideo = (SampleCoverVideo) super.showSmallVideo(size, actionBar, statusBar);
        sampleCoverVideo.mStartButton.setVisibility(GONE);
        sampleCoverVideo.mStartButton = null;
        return sampleCoverVideo;
    }

    @Override
    protected void cloneParams(GSYBaseVideoPlayer from, GSYBaseVideoPlayer to) {
        super.cloneParams(from, to);
        SampleCoverVideo sf = (SampleCoverVideo) from;
        SampleCoverVideo st = (SampleCoverVideo) to;
        st.mShowFullAnimation = sf.mShowFullAnimation;
    }

    /**
     * 退出window层播放全屏效果
     */
    @SuppressWarnings("ResourceType")
    @Override
    protected void clearFullscreenLayout() {
        if (!mFullAnimEnd) {
            return;
        }
        mIfCurrentIsFullscreen = false;
        int delay = 0;
        if (mOrientationUtils != null) {
            delay = mOrientationUtils.backToProtVideo();
            mOrientationUtils.setEnable(false);
            if (mOrientationUtils != null) {
                mOrientationUtils.releaseListener();
                mOrientationUtils = null;
            }
        }

        if (!mShowFullAnimation) {
            delay = 0;
        }

        final ViewGroup vp = (CommonUtil.scanForActivity(getContext())).findViewById(Window.ID_ANDROID_CONTENT);
        final View oldF = vp.findViewById(getFullId());
        if (oldF != null) {
            //此处fix bug#265，推出全屏的时候，虚拟按键问题
            SampleCoverVideo gsyVideoPlayer = (SampleCoverVideo) oldF;
            gsyVideoPlayer.mIfCurrentIsFullscreen = false;
        }

        if (delay == 0) {
            backToNormal();
        } else {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    backToNormal();
                }
            }, delay);
        }

    }


    /******************* 下方两个重载方法，在播放开始前不屏蔽封面，不需要可屏蔽 ********************/
    @Override
    public void onSurfaceUpdated(Surface surface) {
        super.onSurfaceUpdated(surface);
        if (mThumbImageViewLayout != null && mThumbImageViewLayout.getVisibility() == VISIBLE) {
            mThumbImageViewLayout.setVisibility(INVISIBLE);
        }
    }

    @Override
    protected void setViewShowState(View view, int visibility) {
        if (view == mThumbImageViewLayout && visibility != VISIBLE) {
            return;
        }
        super.setViewShowState(view, visibility);
    }

    @Override
    public void onSurfaceAvailable(Surface surface) {
        super.onSurfaceAvailable(surface);
        if (GSYVideoType.getRenderType() != GSYVideoType.TEXTURE) {
            if (mThumbImageViewLayout != null && mThumbImageViewLayout.getVisibility() == VISIBLE) {
                mThumbImageViewLayout.setVisibility(INVISIBLE);
            }
        }
    }

    /******************* 下方重载方法，在播放开始不显示底部进度和按键，不需要可屏蔽 ********************/

    protected boolean byStartedClick;

    @Override
    protected void onClickUiToggle(MotionEvent e) {
        if (mIfCurrentIsFullscreen && mLockCurScreen && mNeedLockFull) {
            setViewShowState(mLockScreen, VISIBLE);
            return;
        }
        byStartedClick = true;
        super.onClickUiToggle(e);
    }

    @Override
    protected void changeUiToNormal() {
        super.changeUiToNormal();
        byStartedClick = false;
    }

    @Override
    protected void changeUiToPreparingShow() {
        super.changeUiToPreparingShow();
        Debuger.printfLog("Sample changeUiToPreparingShow");
        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
    }

    @Override
    protected void changeUiToPlayingBufferingShow() {
        super.changeUiToPlayingBufferingShow();
        Debuger.printfLog("Sample changeUiToPlayingBufferingShow");
        if (!byStartedClick) {
            setViewShowState(mBottomContainer, INVISIBLE);
            setViewShowState(mStartButton, INVISIBLE);
        }
    }

    @Override
    protected void changeUiToPlayingShow() {
        super.changeUiToPlayingShow();
        Debuger.printfLog("Sample changeUiToPlayingShow");
        if (!byStartedClick) {
            setViewShowState(mBottomContainer, INVISIBLE);
            setViewShowState(mStartButton, INVISIBLE);
        }
    }

    @Override
    public void startAfterPrepared() {
        super.startAfterPrepared();
        Debuger.printfLog("Sample startAfterPrepared");
        setViewShowState(mBottomContainer, INVISIBLE);
        setViewShowState(mStartButton, INVISIBLE);
        setViewShowState(mBottomProgressBar, VISIBLE);

        GSYVideoManager.instance().setNeedMute(true);

        changeUI();
    }

    private void changeUI() {
//        GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT);
        GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_FULL);
        changeTextureViewShowType();
        if (mTextureView != null)
            mTextureView.requestLayout();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        byStartedClick = true;
        super.onStartTrackingTouch(seekBar);
    }

    private OnVideoListener listener;

    public void setOnVideoListener(OnVideoListener l) {
        listener = l;
    }

    public interface OnVideoListener {
        void setOnMute(boolean isMute);
    }

    /**
     * 暂停状态
     */
    @Override
    public void onVideoPause() {
        super.onVideoPause();

        mBottomContainer.setVisibility(GONE);
    }

    /**
     * 显示wifi确定框，如需要自定义继承重写即可
     */
    protected void showVedioWifiDialog() {
        if (!YXConfig.isWIFION
                && NetworkUtils.isConnected() && YXConfig.needShowNoWifi && !YXConfig.isOver3Hour) {
            ToastUtils.showShort(mContext.getResources().getString(R.string.tips_not_wifi));

            startPlayLogic();
            YXConfig.needShowNoWifi = false;
            return;
        }

        if (canPlayLogic()) {
            CommonDialog commonDialog = new CommonDialog(mContext, getResources().getString(R.string.tips_not_wifi),
                    getResources().getString(R.string.tips_not_wifi_confirm),
                    getResources().getString(R.string.tips_not_wifi_cancel),
                    true, YXConfig.DURATION_TIME);
            new XPopup.Builder(mContext).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                startPlayLogic();

                mOnDataNetPlayListener.play();
            });
            commonDialog.setOnCancelListener(() -> {
                commonDialog.dismiss();
                mOnDataNetPlayListener.stop();
            });

            this.onVideoPause();
        } else if (!NetworkUtils.isConnected()) {
            ToastUtils.showShort(mContext.getResources().getString(R.string.no_net));
        }
    }

    /**
     * 播放按键点击
     */
    @Override
    protected void clickStartIcon() {
        if (canPlayLogic()) {
            showVedioWifiDialog();

            return;
        } else if (!NetworkUtils.isConnected()) {
            ToastUtils.showShort(mContext.getResources().getString(R.string.no_net));
            return;
        }

        super.clickStartIcon();
    }

    private OnDataNetPlayListener mOnDataNetPlayListener;

    public void setOnDataNetPlayListener(OnDataNetPlayListener listener) {
        mOnDataNetPlayListener = listener;
    }

    /**
     * 非wifi情况下播放提示
     */
    public interface OnDataNetPlayListener {
        void play();

        void stop();

        void detail();
    }

    private boolean canPlayLogic() {
        return null != mOnDataNetPlayListener && !SystemUtil.isWifi(mContext) && YXConfig.needShowNoWifi && NetworkUtils.isConnected() && YXConfig.isOver3Hour;
    }
}
