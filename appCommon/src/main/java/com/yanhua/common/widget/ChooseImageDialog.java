package com.yanhua.common.widget;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.luck.picture.lib.config.PictureConfig;
import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.common.R;
import com.yanhua.core.util.DisplayUtils;

public class ChooseImageDialog extends CenterPopupView {

    private Context mContext;

    public interface OnButtonClickListener {
        void onClick(int type);
    }

    private OnButtonClickListener mListener;

    public ChooseImageDialog(@NonNull Context context,OnButtonClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.layout_choose_image_dialog;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        TextView tvImage = findViewById(R.id.tvImage);
        TextView tvVideo = findViewById(R.id.tvVideo);
        TextView tvCancel = findViewById(R.id.tvCancel);

        tvImage.setOnClickListener(v -> {
            dismiss();

            mListener.onClick(PictureConfig.TYPE_IMAGE);
        });

        tvVideo.setOnClickListener(v -> {
            dismiss();

            mListener.onClick(PictureConfig.TYPE_VIDEO);
        });
        tvCancel.setOnClickListener(v -> dismiss());
    }

    @Override
    protected int getMaxWidth() {
        return (int) (DisplayUtils.getScreenWidth(mContext) * 0.8);
    }
}
