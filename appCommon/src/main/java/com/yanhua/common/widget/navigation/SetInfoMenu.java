package com.yanhua.common.widget.navigation;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yanhua.common.R;

/**
 * 消息顶部menu
 */
public class SetInfoMenu extends LinearLayout {

    private TextView tvTitle;
    private TextView tvSubTitle;
    private View viewDivide;


    public SetInfoMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_set_info, this, true);

        tvTitle = findViewById(R.id.tvTitle);
        tvSubTitle = findViewById(R.id.tvSubTitle);
        viewDivide = findViewById(R.id.viewDivide);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.SetINfoItem);
        if (attributes != null) {
            boolean dividerVisible = attributes.getBoolean(R.styleable.SetINfoItem_dividerItemVisible, false);

            String mainTitle = attributes.getString(R.styleable.SetINfoItem_mainTitle);
//            String subTitle = attributes.getString(R.styleable.SetINfoItem_subTitle);
            String hint = attributes.getString(R.styleable.SetINfoItem_hint);
            tvTitle.setText(mainTitle);
//            tvSubTitle.setText(subTitle);
            tvSubTitle.setHint(hint);
            viewDivide.setVisibility(dividerVisible ? VISIBLE : GONE);

            attributes.recycle();
        }
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void setSubTitle(String subTitle) {
        tvSubTitle.setText(subTitle);
    }
}
