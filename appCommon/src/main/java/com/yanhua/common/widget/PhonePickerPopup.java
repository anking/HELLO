package com.yanhua.common.widget;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.PhonePickerAdapter;
import com.yanhua.core.view.MaxHeightRecyclerView;

import java.util.List;

public class PhonePickerPopup extends BottomPopupView {

    private Context mContext;
    private List<String> phoneNumbers;

    public PhonePickerPopup(@NonNull Context context, List<String> phoneNumbers) {
        super(context);
        this.mContext = context;
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_phone_picker;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_cancel = findViewById(R.id.tv_cancel);
        MaxHeightRecyclerView rv_phone = findViewById(R.id.rv_phone);
        PhonePickerAdapter mAdapter = new PhonePickerAdapter(mContext);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            dismissWith(() -> {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumbers.get(pos)));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            });
        });
        rv_phone.setLayoutManager(manager);
        rv_phone.setAdapter(mAdapter);
        mAdapter.setItems(phoneNumbers);
        tv_cancel.setOnClickListener(view -> dismiss());
    }
}
