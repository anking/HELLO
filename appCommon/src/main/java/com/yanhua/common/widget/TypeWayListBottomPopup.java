package com.yanhua.common.widget;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.TypeWayListAdapter;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.core.view.MaxHeightRecyclerView;

import java.util.List;

/**
 * 通用方式选取
 *
 * @author Administrator
 */
public class TypeWayListBottomPopup extends BottomPopupView {

    private Context mContext;
    private TypeWayListAdapter mAdapter;
    private List<TypeWayModel> mList;


    public interface OnItemClickListener {
        void onItemClick(TypeWayModel bean);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public TypeWayListBottomPopup(@NonNull Context context, List<TypeWayModel> list) {
        super(context);
        mContext = context;
        mList = list;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        MaxHeightRecyclerView rvContent = findViewById(R.id.rv_content);
        findViewById(R.id.rlClose).setOnClickListener(view -> {
            dismiss();
        });

        LinearLayoutManager mManager = new LinearLayoutManager(mContext);
        mAdapter = new TypeWayListAdapter(mContext);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                TypeWayModel bean = mList.get(pos);

                listener.onItemClick(bean);
            }
        });

        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setItems(mList);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_type_list_way;
    }
}
