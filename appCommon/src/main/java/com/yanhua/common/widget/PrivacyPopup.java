package com.yanhua.common.widget;

import android.content.Context;

import androidx.annotation.NonNull;

import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.presenter.CommonAppPresenter;
import com.yanhua.common.presenter.contract.CommonAppContract;
import com.yanhua.core.view.AliIconFontTextView;

import java.util.HashMap;

public class PrivacyPopup extends BaseMvpBottomPopupView<CommonAppPresenter> implements CommonAppContract.IView {

    private int privacyType;
    private boolean mRequest;
    private String mID;

    public PrivacyPopup(@NonNull Context context, int privacyType) {
        super(context);
        this.privacyType = privacyType;
    }

    public PrivacyPopup(@NonNull Context context, int privacyType, boolean request, String id) {
        super(context);
        this.privacyType = privacyType;
        this.mRequest = request;
        this.mID = id;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new CommonAppPresenter();
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_discover_publish_privacy;
    }

    private OnItemSelectListener listener;

    public interface OnItemSelectListener {
        void onSelect(int privacyType);
    }

    public void setOnItemSelectListener(OnItemSelectListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        AliIconFontTextView iv_privacy_public = findViewById(R.id.iv_privacy_public);
        AliIconFontTextView iv_privacy_home = findViewById(R.id.iv_privacy_home);
        AliIconFontTextView iv_privacy_private = findViewById(R.id.iv_privacy_private);

        findViewById(R.id.tvCancel).setOnClickListener(v -> dismiss());
        findViewById(R.id.rl_privacy_public).setOnClickListener(v -> {
            iv_privacy_public.setVisibility(VISIBLE);
            iv_privacy_home.setVisibility(INVISIBLE);
            iv_privacy_private.setVisibility(INVISIBLE);
            privacyType = YXConfig.pivacy.privacy_public;
        });
        findViewById(R.id.rl_privacy_home).setOnClickListener(v -> {
            iv_privacy_public.setVisibility(INVISIBLE);
            iv_privacy_home.setVisibility(VISIBLE);
            iv_privacy_private.setVisibility(INVISIBLE);

            privacyType = YXConfig.pivacy.privacy_home;
        });
        findViewById(R.id.rl_privacy_private).setOnClickListener(v -> {
            iv_privacy_public.setVisibility(INVISIBLE);
            iv_privacy_home.setVisibility(INVISIBLE);
            iv_privacy_private.setVisibility(VISIBLE);

            privacyType = YXConfig.pivacy.privacy_person;
        });
        if (privacyType == 0) {
            iv_privacy_public.setVisibility(VISIBLE);
        } else if (privacyType == 1) {
            iv_privacy_home.setVisibility(VISIBLE);
        } else {
            iv_privacy_private.setVisibility(VISIBLE);
        }

        findViewById(R.id.tvOK).setOnClickListener(v -> {
            if (listener != null) {
                if (mRequest) {
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("privacyType", privacyType);
                    params.put("id", mID);
                    basePresenter.updatePrivacyType(params);
                } else {
                    listener.onSelect(privacyType);
                    dismiss();
                }
            }
        });
    }

    @Override
    public void handleSuccessUpdatePrivacy() {
        listener.onSelect(privacyType);
        dismiss();
    }


}
