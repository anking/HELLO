package com.yanhua.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.common.R;
import com.yanhua.common.adapter.TypeWayAdapter;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.MaxHeightRecyclerView;

import java.util.List;

/**
 * 通用方式选取
 *
 * @author Administrator
 */
public class TypeWayBottomPopup extends BottomPopupView {

    private Context mContext;
    private TypeWayAdapter mAdapter;
    private List<TypeWayModel> mList ;

    private int mPosition = -1;
    private String mTitle;


    public interface OnItemClickListener {
        void onItemClick(TypeWayModel bean);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public TypeWayBottomPopup(@NonNull Context context, String title, List<TypeWayModel> list) {
        super(context);
        mContext = context;
        mTitle = title;
        mList = list;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        AliIconFontTextView iv_close = findViewById(R.id.iv_close);
        MaxHeightRecyclerView rvContent = findViewById(R.id.rv_content);
        TextView tv_title = findViewById(R.id.tv_title);

        tv_title.setText(TextUtils.isEmpty(mTitle) ? "" : mTitle);

        iv_close.setOnClickListener(view -> {
            dismiss();
        });

        LinearLayoutManager mManager = new LinearLayoutManager(mContext);
        mAdapter = new TypeWayAdapter(mContext);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                TypeWayModel bean = mList.get(pos);
                mPosition = pos;

//                listener.onItemClick(bean,mPosition);

                listener.onItemClick(bean);
            }
        });

        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
/*
        int pos = 0;
        for (int i = 0; i < mList.size(); i++) {
            TypeWayModel item = mList.get(i);
            if (item.equals(tt)) {
                pos = i;
                break;
            }
        }
        mAdapter.setSelectedId(pos + "");
        mPosition = pos;
*/

        mAdapter.setItems(mList);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_type_way;
    }
}
