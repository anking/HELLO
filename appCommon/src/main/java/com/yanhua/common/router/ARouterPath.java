package com.yanhua.common.router;


/**
 *
 */
public class ARouterPath {
//    public static final String EXAMPLE_ACTIVITY = "/module/ExampleActivity";

    //----user---
    public static final String LOGIN_ACTIVITY = "/user/LoginActivity";
    public static final String VERIFY_PHONE_ACTIVITY = "/user/VerifyPhoneActivity";

    public static final String COMMON_PROBLEM_ACTIVITY = "/user/CommonProblemActivity";
    public static final String COMMON_PROBLEM_DETAIL_ACTIVITY = "/user/CommonProblemDetailActivity";
    public static final String FEEDBACK_ACTIVITY = "/user/FeedbackActivity";
    public static final String WEB_DETAIL_ACTIVITY = "/user/WebDetailActivity";
    public static final String MODIFY_PSWD_ACTIVITY = "/user/ModifyPswdActivity";
    public static final String SET_ACTIVITY = "/user/SetActivity";
    public static final String SET_PSWD_ACTIVITY = "/user/SetPswdActivity";
    public static final String BIND_PHONE_ACTIVITY = "/user/BindPhoneActivity";
    public static final String DEAL_RESULT_ACTIVITY = "/user/DealResultActivity";
    public static final String SPLASH_ACTIVITY = "/user/SplashActivity";
    public static final String WELCOME_ACTIVITY = "/user/WelcomeActivity";
    public static final String ABOUT_US_ACTIVITY = "/user/AboutUsActivity";
    public static final String SET_PRIVATE_ACTIVITY = "/user/SetPrivateActivity";
    public static final String BLACK_LIST_ACTIVITY = "/user/BlackListActivity";

    public static final String ACCOUT_SAFE_ACTIVITY = "/user/AccoutSafeActivity";
    public static final String LOG_OFF_ACTIVITY = "/user/LogOffActivity";
    public static final String PERSONAL_INFO_ACTIVITY = "/user/PersonalInfoActivity";
    public static final String PERSONAL_INFO_UPDATE_ACTIVITY = "/user/PersonalInfoUpdateActivity";
    public static final String LOG_OFF_FAILED_ACTIVITY = "/user/LogOffFailedActivity";
    public static final String SHARE_FRIENDS_ACTIVITY = "/user/ShareFriendsActivity";


    public static final String MODIFY_PSWD_WAY_ACTIVITY = "/user/ModifyPswdWayActivity";
    public static final String MODIFY_PSWD_BYPSWD_ACTIVITY = "/user/ModifyPswdByPswdActivity";

    public static final String FRIENDS_LIST_ACTIVITY = "/user/FriendsListActivity";

    public static final String SET_PHONE_ACTIVITY = "/user/SetPhoneActivity";
    public static final String RELATION_USER_LIST_ACTIVITY = "/user/RelationUserListActivity";
    public static final String CONTACT_US_ACTIVITY = "/user/ContactUsActivity";
    public static final String CONTACT_CS_ACTIVITY = "/user/ContactCSActivity";
    public static final String AD_ACTIVITY = "/user/ADActivity";
    public static final String CITY_ACTIVITY = "/user/CityActivity";
    public static final String BAR_CITY_ACTIVITY = "/user/BarCityActivity";
    public static final String AMAP_LOCATION_SELECT_ACTIVITY = "/user/AMapLocationSelectActivity";
    public static final String CERTIFICATION_ACTIVITY = "/user/CertificationActivity";
    public static final String CERTIFY_CENTER_ACTIVITY = "/user/CertifyCenterActivity";
    public static final String CERTIFY_RESULT_ACTIVITY = "/user/CertifyResultActivity";

    //----mine---
    public static final String MINE_INVITE_ACTIVITY = "/mine/MyInviteActivity";
    public static final String MINE_COMMENT_ACTIVITY = "/mine/MyCommentListActivity";
    public static final String MY_DYNAMIC_LIST_ACTIVITY = "/mine/MyDynamicListActivity";
    public static final String MY_FAV_LIST_ACTIVITY = "/mine/MyFavListActivity";
    public static final String MY_LIKE_LIST_ACTIVITY = "/mine/MyLikeListActivity";
    public static final String PERSONAL_PAGE_ACTIVITY = "/mine/PersonalPageNewActivity";
    public static final String MY_CIRCLE_LIST_ACTIVITY = "/mine/MyCircleListActivity";
    public static final String MY_BROWSE_LIST_ACTIVITY = "/mine/MyBrowseListActivity";

    //----moment---
    public static final String PUBLISH_CONTENT_ACTIVITY = "/moment/PublishContentActivity";
    public static final String ADDRESS_SEARCH_ACTIVITY = "/moment/AddressSearchActivity";
    public static final String RANKING_LIST_ACTIVITY = "/moment/RankingListActivity";
    public static final String TOPIC_DETAIL_ACTIVITY = "/moment/TopicDetailActivity";
    public static final String CIRCLE_DETAIL_ACTIVITY = "/moment/CircleDetailActivity";

    public static final String ELECT_SOUND_LIST_ACTIVITY = "/moment/ElectSoundListActivity";


    public static final String CIRCLE_INFO_DETAIL_ACTIVITY = "/moment/CircleInfoDetailActivity";
    public static final String CIRCLE_MEMBER_LIST_ACTIVITY = "/moment/CircleMemberListActivity";
    public static final String CIRCLE_APPLY_ACTIVITY = "/moment/CircleApplyActivity";
    public static final String CIRCLE_APPLY_HISTORY_ACTIVITY = "/moment/CircleApplyHistoryActivity";
    public static final String CIRCLE_SUGGEST_LIST_ACTIVITY = "/moment/CircleSuggestListActivity";
    public static final String CIRCLE_MASTER_APPLY_HISTORY_ACTIVITY = "/moment/CircleMasterApplyHistoryActivity";
    public static final String CIRCLE_TRANSFEREN_ACTIVITY = "/moment/CircleTransferenActivity";
    public static final String SHORT_VIDEO_DETAIL_ACTIVITY = "/moment/ShortVideoDetailActivity";
    public static final String CONTENT_DETAIL_ACTIVITY = "/moment/ContentDetailActivity";
    public static final String REPORT_ACTIVITY = "/moment/ReportActivity";
    public static final String LONG_CONTENT_HTML_ACTIVITY = "/moment/LongContentHtmlActivity";
    public static final String LONG_CONTENT_ACTIVITY = "/moment/LongContentActivity";
    public static final String PREVIEW_CONTENT_ACTIVITY = "/moment/PreViewActivity";


    //----message---
    public static final String MSG_FAVLIKE_LIST_ACTIVITY = "/message/MsgFavLikeListActivity";
    public static final String MSG_FAVLIKE_DETAIL_ACTIVITY = "/message/FavLikeDetailActivity";
    public static final String MSG_NEW_FOLLOW_ACTIVITY = "/message/NewFollowActivity";
    public static final String MSG_COMMENT_AT_ACTIVITY = "/message/MsgCommentAtActivity";
    public static final String MSG_INVITE_ACTIVITY = "/message/MsgInviteActivity";
    public static final String MSG_SYSTEM_ACTIVITY = "/message/MsgSystemActivity";
    public static final String MSG_INTERACT_ACTIVITY = "/message/MsgInteractActivity";


    public static final String GROUP_CONTACTS_ACTIVITY = "/message/GroupContactActivity";
    public static final String CREATE_GROUP_ACTIVITY = "/message/CreateGroupActivity";

    public static final String PRIVATE_CHAT_SETTING_ACTIVITY = "/message/ChatSettingActivity";

    public static final String GROUP_HISTORY_MSGLIST_ACTIVITY = "/message/GroupHistoryMsgListActivity";
    public static final String CHAT_HISTORY_MSGLIST_ACTIVITY = "/message/ChatHistoryMsgListActivity";
    public static final String GROUP_MANAGE_ACTIVITY = "/message/GroupManageActivity";
    public static final String GROUP_MANAGE_SET_ACTIVITY = "/message/GroupManageSetActivity";
    public static final String GROUP_INFO_ACTIVITY = "/message/GroupInfoActivity";
    public static final String GROUP_MEMBERS_ACTIVITY = "/message/GroupMembersActivity";
    public static final String GROUP_MODIFY_ACTIVITY = "/message/GroupModifyActivity";
    public static final String GROUP_COMPLAINT_ACTIVITY = "/message/GroupComplaintActivity";
    public static final String GROUP_NOTICE_ACTIVITY = "/message/GroupNoticeActivity";


    //----home---
    public static final String INVITE_LIST_ACTIVITY = "/home/InviteListActivity";
    public static final String PUBLISH_INVITE_ACTIVITY = "/home/PublishInviteActivity";
    public static final String PUBLISH_INVITE_NUM_ACTIVITY = "/home/InvitePeopleNumActivity";
    public static final String INVITE_CANCEL_ACTIVITY = "/home/InviteCancelJoinActivity";
    public static final String INVITE_DETAIL_ACTIVITY = "/home/InviteDetailActivity";
    public static final String SHOW_MAP_LOCATION_ACTIVITY = "/home/ShowMapLocationActivity";
    public static final String INVITE_MEMBER_LIST_ACTIVITY = "/home/InviteMemberListActivity";
    public static final String INVITE_ENROLL_LIST_ACTIVITY = "/home/InviteEnrollListActivity";

    public static final String BEST_NOTE_ACTIVITY = "/home/BestNoteActivity";
    public static final String BREAK_NEWS_ACTIVITY = "/home/BreakNewsActivity";
    public static final String STRATEGY_NEWS_ACTIVITY = "/home/StrategyActivity";
    public static final String ACT_CALENDAR_ACTIVITY = "/home/ActCalendarActivity";
    public static final String PUBLISH_STRATEGY_ACTIVITY = "/home/PublishStrategyActivity";
    public static final String PUBLISH_BREAK_NEWS_ACTIVITY = "/home/PublishBreakNewsActivity";
    public static final String STRATEGY_CLASSIFY_ACTIVITY = "/home/StrategyClassifyActivity";
    public static final String NEWS_DETAIL_ACTIVITY = "/home/NewsDetailActivity";
    public static final String HOME_SEARCH_ACTIVITY = "/home/HomeSearchActivity";
    public static final String HOME_SEARCH_RESULT_ACTIVITY = "/home/HomeSearchResultActivity";

    // ----bar&KTV----
    public static final String BAR_LIST_ACTIVITY = "/home/BarListActivity";
    public static final String BAR_DETAIL_ACTIVITY = "/home/BarDetailActivity";
    public static final String BAR_POST_QA_ACTIVITY = "/home/BarPostQAActivity";
    public static final String BAR_QUESTION_LIST_ACTIVITY = "/home/BarQuestionListActivity";
    public static final String BAR_QUESTION_DETAIL_ACTIVITY = "/home/BarQuestionDetailActivity";
    public static final String BAR_MY_QA_ACTIVITY = "/home/BarMyQAActivity";
    public static final String BAR_ANSWER_DETAIL_ACTIVITY = "/home/BarAnswerDetailActivity";

    //-----app-------
    public static final String MAIN_ACTIVITY = "/app/MainActivity";

    // module_media
    public static final String YX_VIDEO_RECORD = "/media/YXVideoRecordActivity";
    public static final String VIDEO_RECORD = "/media/VideoRecordActivity";
    public static final String PICTURE_PICKER = "/media/PicturePickerActivity";
    public static final String VIDEO_PICKER = "/media/VideoPickerActivity";
    public static final String VIDEO_EDIT = "/media/VideoEditerActivity";
    public static final String VIDEO_BGMUSIC_ACTIVITY = "/media/VideoBgMusicActivity";
    public static final String MUSIC_TYPE_ACTIVITY = "/media/MusicTypeActivity";
    public static final String MUSIC_LIST_TYPE_ACTIVITY = "/media/MusicListTypeActivity";
    public static final String MUSIC_SEARCH_ACTIVITY = "/media/MusicSearchActivity";
    public static final String MUSIC_SEARCH_RESULT_ACTIVITY = "/media/MusicSearchResultActivity";
    public static final String YXPICTURE_PREVIEW_ACTIVITY = "/media/YXPicturePreviewActivity";
}
