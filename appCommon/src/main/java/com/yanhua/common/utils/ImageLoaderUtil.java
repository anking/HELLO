package com.yanhua.common.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * 图片加载框架封装类
 */
public class ImageLoaderUtil {

    public static void loadImgHeadCenterCrop(ImageView v, String url, int placeholder) {
        if (null != url) {
            url = formatUrl(url) + "?x-oss-process=image/resize,m_fixed,h_100,w_100";
        }
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImg(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgCenterCrop(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgCenterCrop(ImageView v, String url, int placeholder) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgCenterCrop(Context context, ImageView v, String url, int placeholder, int size) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .centerCrop()
                        .transform(glideRoundTransform)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgCenterCrop(Context context, ImageView v, String url, int placeholder, int size, GlideRoundTransform.CornerType cornerType) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, cornerType);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .centerCrop()
                        .transform(glideRoundTransform)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgCoverCenterCrop(Context context, ImageView v, String url, int width, int height, int placeholder, GlideRoundTransform.CornerType cornerType) {
        url = formatUrl(url);
        //此处避免有些图有些少许黑色边边，里面的宽故意加大8dp，圆角因此设置4+8--->
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, 4, cornerType);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(placeholder)
//                        .override(width + addWidth, height)
                        .centerCrop()
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadImg(ImageView v, String url, int placeholder) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadImgBlur(ImageView v, String url, int placeholder) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(bitmapTransform(new BlurTransformation(250))//bitmapTransform(new SupportRSBlurTransformation(25, 10))
                        .placeholder(placeholder)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadConnerImg(Context context, ImageView v, String url, int size) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(500, 500)
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadConnerImg(Context context, ImageView v, Uri url, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(500, 500)
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadConnerLiveImg(Context context, ImageView v, String url, int size) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(400, 500)
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadConnerImg(Context context, ImageView v, int resId, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(resId)
                .apply(new RequestOptions()
                        .fitCenter()
                        .transform(glideRoundTransform)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadRoundImg(Context context, ImageView v, String url, int placeholder, int size) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext()).load(url).error(placeholder).transforms(new CenterCrop(), glideRoundTransform).into(v);
    }

    public static void loadRoundImg(Context context, ImageView v, String url, int size) {
        url = formatUrl(url);
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext()).load(url).transforms(glideRoundTransform).into(v);
    }

    public static void loadImg(ImageView v, int resourceId) {
        Glide.with(v.getContext())
                .load(resourceId)
                .apply(new RequestOptions()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadGifImg(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadCircleImg(ImageView v, File file) {
        Glide.with(v.getContext())
                .load(file).apply(bitmapTransform(new CircleCrop()))
                .into(v);
    }

    public static void loadLocalImg(ImageView v, File file) {
        Glide.with(v.getContext())
                .load(file).apply(bitmapTransform(new FitCenter()))
                .into(v);
    }

    public static void loadStartPageAdImg(ImageView v, File file) {
        Glide.with(v.getContext())
                .load(file).apply(bitmapTransform(new CenterCrop()))
                .into(v);
    }

    public static void loadStartPageAdImg(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    /**
     * 加载本地文件
     *
     * @param v
     * @param file
     * @param size
     */
    public static void loadLocalCornerImg(ImageView v, File file, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(v.getContext(), size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(file)
                .apply(bitmapTransform(new CenterCrop())
                        .transform(glideRoundTransform)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(v);
    }

    public static void loadImgBg(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        Drawable drawable = new BitmapDrawable(resource);
                        v.setBackground(drawable);
                    }
                });
    }

    public static void loadCircleImg(ImageView v, Bitmap bitmap) {
        Glide.with(v.getContext())
                .load(bitmap).apply(bitmapTransform(new CircleCrop()))
                .into(v);
    }

    public static void loadBitmap(ImageView v, Bitmap bitmap, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(v.getContext(), size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(bitmap)
                .apply(bitmapTransform(new CircleCrop())
                        .transform(glideRoundTransform)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadCircleImg(ImageView v, Drawable drawable) {
        Glide.with(v.getContext())
                .load(drawable).apply(bitmapTransform(new CircleCrop()))
                .into(v);
    }

    public static void loadImgFillCenter(Context context, ImageView v, String localPath, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(localPath)
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadAdapterImg(ImageView v, String url, final View itemView) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v)
                .getSize((width, height) -> {
                    if (!itemView.isShown()) {
                        itemView.setVisibility(View.VISIBLE);
                    }
                });
    }

    public static void loadImgCenterCrop(Context context, ImageView v, Uri uri, int size) {
        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(context, size, GlideRoundTransform.CornerType.ALL);
        Glide.with(v.getContext())
                .load(uri)
                .apply(new RequestOptions()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(glideRoundTransform))
                .into(v);
    }

    public static void loadImgFitCenter(ImageView v, String url, int placeholder) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .placeholder(placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(v);
    }

    public static void loadOriginImg(ImageView v, String url) {
        url = formatUrl(url);
        Glide.with(v.getContext())
                .load(url)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
//                        .override(Target.SIZE_ORIGINAL))
                .into(v);
    }

    public static String formatUrl(String url) {
        if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
            return url;
        }
        return "http://" + url;
    }

}
