package com.yanhua.common.utils;

import android.content.Context;

import com.xuexiang.xupdate.XUpdate;
import com.xuexiang.xupdate.entity.UpdateEntity;
import com.xuexiang.xupdate.listener.IUpdateParseCallback;
import com.xuexiang.xupdate.proxy.IUpdateParser;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.Api;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.widget.CustomUpdatePrompter;

import org.greenrobot.eventbus.EventBus;

public class UpdateShowDialog {

    public static void showUpdateDialog(Context context, UpgradeInfoModel upgradeInfoModel) {
        XUpdate.newBuild(context)
                .updateParser(new IUpdateParser() {
                    @Override
                    public UpdateEntity parseJson(String json) {
                        return new UpdateEntity()
                                .setHasUpdate(true)
                                .setForce(upgradeInfoModel.getForcedUpdate() == 1)
                                .setIsIgnorable(false)
                                .setVersionCode(upgradeInfoModel.getVersionNo())
                                .setVersionName(upgradeInfoModel.getVersionName())
                                .setUpdateContent(upgradeInfoModel.getUpdateContent())
                                .setIsAutoInstall(true)
                                .setDownloadUrl(upgradeInfoModel.getDownloadUrl());
                    }

                    @Override
                    public void parseJson(String json, IUpdateParseCallback callback) {
                    }

                    @Override
                    public boolean isAsyncParser() {
                        return false;
                    }
                }) //设置自定义的版本更新解析器
                .updateUrl(YXConfig.getBaseUrl() + "/" + Api.user.FETCH_APP_VERSION_INFO + "?type=0")
                .updatePrompter(new CustomUpdatePrompter())
                .supportBackgroundUpdate(true)
                .setOnClosePopupListener(() -> {
                    // 升级的弹框关闭后再去判断权限申请弹框
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.REQUEST_PERMISSIONS));
                })
                .update();
    }
}
