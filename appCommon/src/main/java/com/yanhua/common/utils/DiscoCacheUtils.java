package com.yanhua.common.utils;

import android.content.Context;
import android.text.TextUtils;

import com.blankj.utilcode.util.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.BarConfigModel;
import com.yanhua.common.model.InviteConfigModel;
import com.yanhua.common.model.InviteCreateTypeModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.core.mmkv.MMKVUtil;

import java.lang.reflect.Type;
import java.util.List;

/**
 * 通过获取音圈的其他非用户信息的缓存数据
 */
public class DiscoCacheUtils {
    private static Context mContext;
    private static MMKVUtil mMMKV;
    private static DiscoCacheUtils instance;

    private DiscoCacheUtils() {
    }

    public static void init(Context context) {
        mMMKV = MMKVUtil.getInstance();
        mContext = context;
    }

    public static DiscoCacheUtils getInstance() {
        if (instance == null) {
            synchronized (DiscoCacheUtils.class) {
                if (instance == null) {
                    instance = new DiscoCacheUtils();
                }
            }
        }
        return instance;
    }

    //内存缓存
    public String getCommonConfigType(int type) {
        return mMMKV.getString("commonConfigType" + type);
    }

    public void setCommonConfigType(int type, String commonConfigType) {
        mMMKV.putString("commonConfigType" + type, commonConfigType);

        if (!TextUtils.isEmpty(commonConfigType)) {
            if (YXConfig.TYPE_INVITE == type) {
                Type typeObj = new TypeToken<InviteConfigModel>() {
                }.getType();

                InviteConfigModel invite = GsonUtils.fromJson(commonConfigType, typeObj);
                setInviteConfig(invite);
            } else if (YXConfig.BASE_CONFIG_BAR == type) {
                Type typeObj = new TypeToken<BarConfigModel>() {
                }.getType();
                BarConfigModel bar = GsonUtils.fromJson(commonConfigType, typeObj);
                setBarConfigModel(bar);
            }
        }
    }

    private BarConfigModel barConfigModel;

    public BarConfigModel getBarConfigModel() {
        return barConfigModel;
    }

    public void setBarConfigModel(BarConfigModel barConfigModel) {
        this.barConfigModel = barConfigModel;
    }

    private InviteConfigModel inviteConfig;

    public InviteConfigModel getInviteConfig() {
        return inviteConfig;
    }

    public void setInviteConfig(InviteConfigModel inviteConfig) {
        this.inviteConfig = inviteConfig;
    }

    private List<InviteTypeModel> inviteTypeList;

    public List<InviteTypeModel> getInviteTypeList() {
        return inviteTypeList;
    }

    public void setInviteTypeList(List<InviteTypeModel> inviteTypeList) {
        this.inviteTypeList = inviteTypeList;
    }

    public String getLongitude() {
        String lng = getLng();
        lng = TextUtils.isEmpty(lng) ? YXConfig.longitude : lng;
        return lng;
    }

    public String getLatitude() {
        String lat = getLat();
        lat = TextUtils.isEmpty(lat) ? YXConfig.latitude : lat;
        return lat;
    }

    public String getHomeSearchHistory() {
        return mMMKV.getString("homeSearch");
    }

    public void setHomeSearchHistory(String homeSearch) {
        mMMKV.putString("homeSearch", homeSearch);
    }

    public String getSearchMusicHistory() {
        return mMMKV.getString("musicHistory");
    }

    public void setSearchMusicHistory(String musicHistory) {
        mMMKV.putString("musicHistory", musicHistory);
    }

    public String getMusicListJson() {
        return mMMKV.getString("musicList");
    }

    public void setMusicListJson(String musicList) {
        mMMKV.putString("musicList", musicList);
    }


    public String getMusicTypesJson() {
        return mMMKV.getString("musicTypes");
    }

    public void setMusicTypesJson(String musicTypes) {
        mMMKV.putString("musicTypes", musicTypes);
    }

    public String getPublishTemp() {
        return mMMKV.getString("publishTemp");
    }

    public void setPublishTemp(String publishTemp) {
        mMMKV.putString("publishTemp", publishTemp);
    }
    public String getBreakNewsTemp() {
        return mMMKV.getString("breakNewsTemp");
    }

    public void setBreakNewsTemp(String publishTemp) {
        mMMKV.putString("breakNewsTemp", publishTemp);
    }
    public String getStrategyTemp() {
        return mMMKV.getString("StrategyTemp");
    }

    public void setStrategyTemp(String publishTemp) {
        mMMKV.putString("StrategyTemp", publishTemp);
    }

    public String getTopicHistoryList() {
        return mMMKV.getString("topicHistoryList");
    }

    public void setTopicHistoryList(String topicHistoryList) {
        mMMKV.putString("topicHistoryList", topicHistoryList);
    }

    public String getCircleHistoryList() {
        return mMMKV.getString("circleHistoryList");
    }

    public void setCircleHistoryList(String circleHistoryList) {
        mMMKV.putString("circleHistoryList", circleHistoryList);
    }

    public String getCityHistoryList() {
        return mMMKV.getString("cityHistoryList");
    }

    public void setCityHistoryList(String cityHistoryList) {
        mMMKV.putString("cityHistoryList", cityHistoryList);
    }

    public String getBarCityHistoryList() {
        return mMMKV.getString("barCityHistoryList");
    }

    public void setBarCityHistoryList(String barCityHistoryList) {
        mMMKV.putString("barCityHistoryList", barCityHistoryList);
    }

    public String getStsToken() {
        return mMMKV.getString("stsToken");
    }

    public void setStsToken(String stsToken) {
        mMMKV.putString("stsToken", stsToken);
    }

    public String getFirstOpenTime() {
        return mMMKV.getString("firstOpenTime");
    }

    public void setFirstOpenTime(String firstOpenTime) {
        mMMKV.putString("firstOpenTime", firstOpenTime);
    }

    public String getLng() {
        return mMMKV.getString("lng");
    }

    public void setLng(String lng) {
        mMMKV.putString("lng", lng);
    }

    public String getLat() {
        return mMMKV.getString("lat");
    }

    public void setLat(String lat) {
        mMMKV.putString("lat", lat);
    }

    public String getCurrentCity() {
        return mMMKV.getString("currentCity");
    }

    public void setCurrentCity(String currentCity) {
        mMMKV.putString("currentCity", currentCity);
    }

    public String getCurrentBarCity() {
        return mMMKV.getString("currentBarCity");
    }

    public void setCurrentBarCity(String currentBarCity) {
        mMMKV.putString("currentBarCity", currentBarCity);
    }

    public String getBarLng() {
        return mMMKV.getString("barLng");
    }

    public void setBarLng(String barLng) {
        mMMKV.putString("barLng", barLng);
    }

    public String getBarLat() {
        return mMMKV.getString("barLat");
    }

    public void setBarLat(String barLat) {
        mMMKV.putString("barLat", barLat);
    }

    public String getHomePopAd() {
        return mMMKV.getString("homePopAd");
    }

    public void setHomePopAd(String homePopAd) {
        mMMKV.putString("homePopAd", homePopAd);
    }

    private InviteCreateTypeModel inviteCreateType;

    public InviteCreateTypeModel getInviteCreateType() {
        return inviteCreateType;
    }

    public void setInviteCreateType(InviteCreateTypeModel data) {
        inviteCreateType = data;
    }


    public int getHideSetStatus() {
        return mMMKV.getInt("hideSetStatus",0);
    }

    public void setHideSetStatus(int  hideSetStatus) {
        mMMKV.putInt("hideSetStatus", hideSetStatus);
    }
}
