package com.yanhua.common.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;

import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.chinalwb.are.spans.AreImageSpan;
import com.chinalwb.are.strategies.ImageStrategy;
import com.chinalwb.are.styles.toolitems.styles.ARE_Style_Image;
import com.google.gson.reflect.TypeToken;
import com.yanhua.base.model.HttpResult;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.StsTokenModel;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DiscoImageStrategy implements ImageStrategy {
    ProgressDialog dialog;
    private StsTokenModel stsTokenModel;

    @Override
    public void uploadAndInsertImage(Uri uri, ARE_Style_Image areStyleImage) {
        File file = new File(uri.toString());

        uploadFile(file, areStyleImage);
    }

    private void uploadFile(File file, ARE_Style_Image areStyleImage) {
        Context mContext = areStyleImage.getEditText().getContext();
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);

        if (dialog == null) {
            dialog = ProgressDialog.show(
                    areStyleImage.getEditText().getContext(),
                    "",
                    "上传图片...",
                    true);
        } else {
            dialog.show();
        }

        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();

                if (areStyleImage != null) {
                    areStyleImage.insertImage(httpUrl, AreImageSpan.ImageType.URL);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

}
