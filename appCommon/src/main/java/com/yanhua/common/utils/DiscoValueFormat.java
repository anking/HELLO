package com.yanhua.common.utils;

public class DiscoValueFormat {
    public static boolean isFollow(int followStatus) {
        boolean isFollow = followStatus == 2 || followStatus == 4;

        return isFollow;
    }

    /**
     * 取消关注
     *
     * @param followStatus
     * @return
     */
    public static int unFollowAction(int followStatus) {
        return followStatus - 1;
    }

    /**
     * 关注
     *
     * @param followStatus
     * @return
     */
    public static int followAction(int followStatus) {
        return followStatus + 1;
    }
}
