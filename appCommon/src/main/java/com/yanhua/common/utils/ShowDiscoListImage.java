package com.yanhua.common.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.gridlayout.widget.GridLayout;

import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.OswaldTextView;

import java.util.List;


//ShowDiscoListImage.showGridImageNew
public class ShowDiscoListImage {
    public static void showGridImageNew(Context mContext, GridLayout gridImage, OswaldTextView tvIndex, List<String> imgUrls) {
        //1：<=3 1:1:1
        //2: >3

        ViewGroup.LayoutParams imageLayoutParams = gridImage.getLayoutParams();
        gridImage.removeAllViews();

        int imgSize = imgUrls.size();

        int screenWidth = DisplayUtils.getScreenWidth(mContext);
        //默认初始值--
        //展示1:1，大致为宽的一半。计算方式为screenWidth-左右边距24
        int showWidth = (screenWidth - DisplayUtils.dip2px(mContext, 32));
        int showHeight = 0;


        if (imgSize > 3) {
            showHeight = showWidth / 2;

            int leftShowCount = imgSize - 5;
            if (leftShowCount > 0) {
                tvIndex.setVisibility(View.VISIBLE);
                tvIndex.setText("+" + leftShowCount);
            } else {
                tvIndex.setVisibility(View.GONE);
                tvIndex.setText("0");
            }
        } else {
            showHeight = showWidth / 3;

            tvIndex.setVisibility(View.GONE);
            tvIndex.setText("0");
        }

        imageLayoutParams.width = showWidth;
        imageLayoutParams.height = showHeight;
        gridImage.setLayoutParams(imageLayoutParams);


        //使用Spec定义子控件的位置和比重 设置行列下标， 所占行列  ，比重
        // 设置行列下标， 所占行列  ，比重
        // 对应： layout_row  , layout_rowSpan , layout_rowWeight
        //GridLayout.spec(0, 2, 2f)
        if (imgSize > 3) {
            for (int i = 0; i < imgUrls.size(); i++) {
                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                if (i == 0) {
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 2, 2f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(0, 1, 1.5f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 1) {//(0,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.bottomMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 2) {////(1,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(2, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.bottomMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 3) {////(2,0)
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.topMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }

                if (i == 4 && imgSize > 4) {////(2,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(2, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.topMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                } else {
                    //占位的
                    View functionView = new View(mContext);
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(2, 1, 1f);
                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.topMargin = DisplayUtils.dip2px(mContext, 2);

                    gridImage.addView(functionView, layoutParams);
                }
            }
        } else {
            for (int i = 0; i < imgUrls.size(); i++) {
                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 1f);

                //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                layoutParams.height = 0;
                layoutParams.width = 0;
                //还可以根据位置动态定义子控件直接的边距，下面的意思是

                if (i == 0) {
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                } else if (i == 2) {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                } else {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                }

                gridImage.addView(imageView, layoutParams);
                ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
            }

            //占位的
            int placeHoldlerSize = 3 - imgUrls.size();

            for (int j = 0; j < placeHoldlerSize; j++) {
                int pos = imgSize + j;

                View functionView = new View(mContext);
                GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                GridLayout.Spec columnSpec = GridLayout.spec(pos, 1, 1f);
                //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                layoutParams.height = 0;
                layoutParams.width = 0;
                //还可以根据位置动态定义子控件直接的边距，下面的意思是

                if (pos == 0) {
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                } else if (pos == 2) {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                } else {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 2);
                }


                layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 2);
                gridImage.addView(functionView, layoutParams);
            }
        }
    }


}
