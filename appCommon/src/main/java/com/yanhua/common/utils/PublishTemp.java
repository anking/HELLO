package com.yanhua.common.utils;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.PublishSaveTemp;

public class PublishTemp {
    public static void savePublishSaveTemp(PublishSaveTemp temp) {
        //URi
        String json = new Gson().toJson(temp);

        DiscoCacheUtils.getInstance().setPublishTemp(json);
    }

    public static PublishSaveTemp getPublishSaveTemp(Context context) {
        String json = DiscoCacheUtils.getInstance().getPublishTemp();

        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            PublishSaveTemp temp = new Gson().fromJson(json, PublishSaveTemp.class);
            return temp;
        }
    }

    public static void deletePublishSaveTemp() {
        DiscoCacheUtils.getInstance().setPublishTemp("");
    }

    //-------------------------------------------------------
    public static void saveBreakNewsTemp(PublishNewsForm temp) {
        //URi
        String json = new Gson().toJson(temp);
        DiscoCacheUtils.getInstance().setBreakNewsTemp(json);
    }
    public static PublishNewsForm getBreakNewTemp(Context context) {
        String json = DiscoCacheUtils.getInstance().getBreakNewsTemp();

        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            PublishNewsForm temp = new Gson().fromJson(json, PublishNewsForm.class);
            return temp;
        }
    }

    public static void deleteBreakNewsTemp() {
        DiscoCacheUtils.getInstance().setBreakNewsTemp("");
    }
    ///----------------------------------------------
    //-------------------------------------------------------
    public static void saveStrategyTemp(PublishNewsForm temp) {
        //URi
        String json = new Gson().toJson(temp);
        DiscoCacheUtils.getInstance().setStrategyTemp(json);
    }
    public static PublishNewsForm getStrategyTemp(Context context) {
        String json = DiscoCacheUtils.getInstance().getStrategyTemp();

        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            PublishNewsForm temp = new Gson().fromJson(json, PublishNewsForm.class);
            return temp;
        }
    }

    public static void deleteStrategyTemp() {
        DiscoCacheUtils.getInstance().setStrategyTemp("");
    }
    ///----------------------------------------------
}
