package com.yanhua.common.utils;

import android.app.Activity;
import android.text.TextUtils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.BaseApplication;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.router.ARouterPath;

import cn.jiguang.verifysdk.api.JVerificationInterface;

public class PageJumpUtil {

    public static void pageToPrivacy(String type) {
        if (type.equals(YXConfig.protocol.privacyAgreement)) {
            ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                    .withString("title", "隐私协议条款")
                    .withString("url", YXConfig.getPrivacyAgreement())
                    .navigation();
        } else if (type.equals(YXConfig.protocol.userAgreement)) {
            ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                    .withString("title", "用户协议条款")
                    .withString("url", YXConfig.getUserAgreement())
                    .navigation();
        }
    }

    /**
     * //发布邀约
     *
     * @param inviteAddForm
     */
    public static void pageToPublishInvite(InviteAddForm inviteAddForm, boolean editable) {
        ARouter.getInstance().build(ARouterPath.PUBLISH_INVITE_ACTIVITY)
                .withSerializable("inviteAddForm", inviteAddForm)
                .withBoolean("editable", editable)
                .navigation();
    }

    /**
     * 需要实名认证逻辑判断
     */
    public static void firstRealNameValidThenJump(OnPageJumpListener listener) {
        if (!UserManager.getInstance().isLogin()) {

            boolean verifyEnable = JVerificationInterface.checkVerifyEnable(BaseApplication.getInstance());
            if (verifyEnable && YXConfig.ONEKYE_LOGIN) {
                ARouter.getInstance().build(ARouterPath.VERIFY_PHONE_ACTIVITY).navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.LOGIN_ACTIVITY).navigation();
            }
            return;
        }

        if (UserManager.getInstance().getUserInfo().getRealNameValid() != 1) {
            //实名认证
            ARouter.getInstance().build(ARouterPath.CERTIFICATION_ACTIVITY)
                    .navigation();
        } else {
            if (null != listener) {
                listener.onPageListener();
            }
        }
    }

    /**
     * 需要登录逻辑判断
     */
    public static void firstIsLoginThenJump(OnPageJumpListener listener) {
        if (!UserManager.getInstance().isLogin()) {
            boolean verifyEnable = JVerificationInterface.checkVerifyEnable(BaseApplication.getInstance());
            if (verifyEnable && YXConfig.ONEKYE_LOGIN) {
                ARouter.getInstance().build(ARouterPath.VERIFY_PHONE_ACTIVITY).navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.LOGIN_ACTIVITY).navigation();
            }

            return;
        }

        if (null != listener) {
            listener.onPageListener();
        }
    }

    /**
     * 跳转到爆料活动、攻略详情页面
     * <p>
     * //爆料中包含两种：活动和非活动---根据contentCategoryId区分是否是活动，这个contentCategoryId统一到详情中获取，外部不传入
     */
    public static void jumpNewsDetailPage(String contentId, int type) {
        ARouter.getInstance().build(ARouterPath.NEWS_DETAIL_ACTIVITY)
                .withInt("type", type)
                .withString("contentId", contentId)
                .navigation();
    }

    public interface OnPageJumpListener {
        void onPageListener();
    }

    public static void adJumpContent(AdvertiseModel clickObj, Activity context, int requestCode) {
        adJumpContent(clickObj, context, requestCode, null);
    }

    public static void adJumpContent(AdvertiseModel clickObj, Activity context, int requestCode, OnPageJumpListener listener) {
        if (null == clickObj) {
            return;
        }

        String content = clickObj.getContent();
        int type = clickObj.getType();

        //// 广告类型(1:URL,2:搜索字段,3:话题,4:圈子,5:动态,6:页面,7:视频,8:酒吧,9:爆料/攻略,10:约伴)
        switch (type) {
            case YXConfig.adType.url:
                if (null != listener) {
                    listener.onPageListener();
                }

                ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                        .withString("title", "")
                        .withString("url", content)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.keyword:
                //搜索页面
//                ToastUtil.toastShortMessage("马上来");
                break;
            case YXConfig.adType.topic:
                if (null != listener) {
                    listener.onPageListener();
                }

                ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                        .withString("id", content)
                        .navigation(context, requestCode);

                break;
            case YXConfig.adType.circle:
                if (null != listener) {
                    listener.onPageListener();
                }

                ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                        .withString("id", content)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.dynamic_disco_best:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                        .withSerializable("contentId", content)
                        .withInt("discoBest", 1)//电音集锦
                        .withString("categoryId", "")
                        .withString("city", "")
                        .withInt("module", 0)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.dynamic_moment_content:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                        .withString("id", content)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.dynamic_moment_video:
                if (!TextUtils.isEmpty(content)) {
                    if (null != listener) {
                        listener.onPageListener();
                    }

                    if (content.startsWith("http://") || content.startsWith("https://")) {
                        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                                .withString("title", "")
                                .withString("url", content)
                                .navigation(context, requestCode);
                    } else {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentId", content)
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withInt("module", 0)
                                .navigation(context, requestCode);
                    }
                }

                break;
//            case YXConfig.adType.page:
//                ToastUtil.toastShortMessage("马上来");
//                break;
            case YXConfig.adType.video:
                if (content.startsWith("http://") || content.startsWith("https://")) {
                    if (null != listener) {
                        listener.onPageListener();
                    }
                    ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                            .withString("title", "")
                            .withString("url", content)
                            .navigation(context, requestCode);
                }
                break;
            case YXConfig.adType.bar:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance()
                        .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                        .withString("barId", content)
                        .navigation();
                break;
            case YXConfig.adType.breakNews:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance().build(ARouterPath.NEWS_DETAIL_ACTIVITY)
                        .withInt("type", YXConfig.TYPE_BREAK_NEWS)
                        .withString("contentId", content)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.strategy:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance().build(ARouterPath.NEWS_DETAIL_ACTIVITY)
                        .withInt("type", YXConfig.TYPE_STRATEGY)
                        .withString("contentId", content)
                        .navigation(context, requestCode);
                break;
            case YXConfig.adType.invite:
                if (null != listener) {
                    listener.onPageListener();
                }
                ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                        .withString("id", content)
                        .navigation(context, requestCode);
                break;
        }
    }
}
