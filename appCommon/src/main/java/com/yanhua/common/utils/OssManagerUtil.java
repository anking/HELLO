package com.yanhua.common.utils;

import android.content.Context;

import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.StsTokenModel;

public class OssManagerUtil {
    private static OssManager ossManager;

    private volatile static OssManagerUtil instance;

    public OssManagerUtil() {
    }

    public static OssManagerUtil getInstance() {
        if (instance == null) {
            synchronized (OssManagerUtil.class) {
                if (instance == null) {
                    instance = new OssManagerUtil();
                }
            }
        }
        return instance;
    }

    public void uploadFile(Context context, StsTokenModel stsTokenModel, String objectKey, String filePath, OSSPushListener ossPushListener) {
        OssManager.Builder builder = new OssManager.Builder(context);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());
        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {
            if (ossPushListener != null) {
                ossPushListener.onProgress(currentSize, totalSize);
            }
        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {

            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                if (ossPushListener != null) {
                    ossPushListener.onSuccess(result);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                if (ossPushListener != null) {
                    ossPushListener.onFailure();
                }
            }
        });
    }

}
