package com.yanhua.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DiscoTimeUtils {

    public static long getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
//// long ns = 1000;
//// 获得两个时间的毫秒时间差异
        long time = endDate.getTime() - nowDate.getTime();
//// 计算差多少天
//        long day = time / nd;
//// 计算差多少小时
//        long hour = time % nd / nh;
//// 计算差多少分钟
        long min = time % nd % nh / nm;
//// 计算差多少秒//输出结果
//// long sec = time % nd % nh % nm / ns;
//        return day + “天” + hour + “小时” + min + “分钟”;

        return min;
    }

    public static String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm开局");
        return format.format(date);
    }

    public static String getRequestTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public static String getBreakNewsTime(Date start, Date end) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");

        return format.format(start) + "-" + format.format(end);
    }


    public static String getTimeNews(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        return format.format(date);
    }

    public static String getBreakNewsTopTime(String startTime, String endTime) {//可根据需要自行截取数据显示
        String result = "";
        Date start;
        Date end;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            start = sdf.parse(startTime);
            end = sdf.parse(endTime);

            SimpleDateFormat startformat = new SimpleDateFormat("yyyy年MM月dd日");
            SimpleDateFormat endformat = new SimpleDateFormat("MM月dd日");

            result = startformat.format(start) + "-" + endformat.format(end);
        } catch (Exception e) {

        }

        return result;
    }


    //3.15 17:30至4.15 12:00
    public static String getBreakNewsActTime(String startTime, String endTime) {//可根据需要自行截取数据显示
        String result = "";
        Date start;
        Date end;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            start = sdf.parse(startTime);
            end = sdf.parse(endTime);

            SimpleDateFormat format = new SimpleDateFormat("MM.dd HH:mm");

            result = format.format(start) + "-" + format.format(end);
        } catch (Exception e) {

        }

        return result;
    }
}
