package com.yanhua.common.utils;

import android.app.Application;
import android.content.Context;
import android.widget.EditText;

import com.tencent.qcloud.ugckit.UGCKit;
import com.tencent.ugc.TXUGCBase;
import com.xuexiang.xupdate.XUpdate;
import com.yanhua.common.api.UpdateHttpServiceImpl;
import com.yanhua.common.im.DiscoExtensionConfig;
import com.yanhua.core.BuildConfig;
import com.yanhua.rong.msgprovider.InviteMessage;
import com.yanhua.rong.msgprovider.InviteMessageProvider;
import com.yanhua.rong.msgprovider.MomentContentMessage;
import com.yanhua.rong.msgprovider.MomentContentMessageProvider;
import com.yanhua.rong.msgprovider.MomentVideoMessage;
import com.yanhua.rong.msgprovider.MomentVideoMessageProvider;
import com.yanhua.rong.msgprovider.NBarMessage;
import com.yanhua.rong.msgprovider.NBarMessageProvider;
import com.yanhua.rong.msgprovider.NewsMessage;
import com.yanhua.rong.msgprovider.NewsMessageProvider;
import com.yanhua.rong.msgprovider.TopicCircleMessage;
import com.yanhua.rong.msgprovider.TopicCircleMessageProvider;
import com.yanhua.rong.msgprovider.UserPageMessage;
import com.yanhua.rong.msgprovider.UserPageMessageProvider;

import java.util.ArrayList;

import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jpush.android.api.JPushInterface;
import io.rong.common.RLog;
import io.rong.imkit.RongIM;
import io.rong.imkit.config.RongConfigCenter;
import io.rong.imkit.conversation.extension.RongExtensionManager;
import io.rong.imkit.feature.mention.IExtensionEventWatcher;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.MentionedInfo;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.push.PushEventListener;
import io.rong.push.PushType;
import io.rong.push.RongPushClient;
import io.rong.push.notification.PushNotificationMessage;
import io.rong.push.pushconfig.PushConfig;
import io.rong.recognizer.RecognizeExtensionModule;
import io.rong.sight.SightExtensionModule;

public final class AppPlatformUtil {
    private final static String TAG = AppPlatformUtil.class.getSimpleName();

    private static volatile AppPlatformUtil instance;

    private AppPlatformUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static AppPlatformUtil getInstance() {
        if (instance == null) {
            synchronized (AppPlatformUtil.class) {
                if (instance == null) {
                    instance = new AppPlatformUtil();
                }
            }
        }
        return instance;
    }

    public static void initPlatformSDK(Application context) {
        //判断进程名，保证只有主进程运行
        // 腾讯云短视频生产licence设置
        TXUGCBase.getInstance().setLicence(context, com.yanhua.core.BuildConfig.TXUGC_LICENSE, com.yanhua.core.BuildConfig.TXUGC_APP);
        UGCKit.init(context);

        XUpdate.get()
                .debug(com.yanhua.core.BuildConfig.DEBUG)
                .isWifiOnly(false)                                               //默认设置只在wifi下检查版本更新
                .isGet(true)                                                    //默认设置使用get请求检查版本
                .isAutoMode(false)                                              //默认设置非自动模式，可根据具体使用配置
                .setOnUpdateFailureListener(error -> {

                })
                .supportSilentInstall(true)                                     //设置是否支持静默安装，默认是true
                .setIUpdateHttpService(new UpdateHttpServiceImpl())           //这个必须设置！实现网络请求功能。
                .init(context);

        // 初始化极光推送
        JPushInterface.setDebugMode(com.yanhua.core.BuildConfig.DEBUG);
        JPushInterface.init(context);

        JVerificationInterface.setDebugMode(com.yanhua.core.BuildConfig.DEBUG);
        JVerificationInterface.init(context);

        //初始化融云
        initRongCloud(context);
    }

    /**
     * IM 配置
     */
    private static void initIMConfig() {
        // 将私聊，群组加入消息已读回执
        Conversation.ConversationType[] types =
                new Conversation.ConversationType[]{
                        Conversation.ConversationType.PRIVATE,
                        Conversation.ConversationType.GROUP,
                        Conversation.ConversationType.ENCRYPTED
                };
        RongConfigCenter.featureConfig().enableReadReceipt(types);


        RongConfigCenter.featureConfig().enableDestruct(BuildConfig.DEBUG);
    }

    private static void initRongCloud(Application context) {
//        设置断线重连时是否踢出重连设备。此方法需要在 init 之前调用。
        //设置 enable 为 true：如果重连时发现已有别的移动端设备在线，将不再重连，不影响已正常登录的移动端设备。
        //设置 enable 为 false：如果重连时发现已有别的移动端设备在线，将踢出已在线的移动端设备，使当前设备上线。
//        RongIMClient.getInstance().setReconnectKickEnable(false);//是否踢出正在重连的设备---默认为false

        //融云生产环境appkey
        RongIM.init(context, com.yanhua.core.BuildConfig.RONG_IM_AK, true);
        RongIM.getInstance().enableNewComingMessageIcon(true);
        // 推送渠道生产配置
        initPush();
        initIMConfig();

        // 初始化自定义消息和消息模版
        initMessageAndTemplate();

        // 初始化扩展模块
        initExtensionModules(context);

        RongExtensionManager.getInstance()
                .addExtensionEventWatcher(
                        new IExtensionEventWatcher() {
                            @Override
                            public void onTextChanged(
                                    Context context,
                                    Conversation.ConversationType type,
                                    String targetId,
                                    int cursorPos,
                                    int count,
                                    String text) {
                            }

                            @Override
                            public void onSendToggleClick(Message message) {
                                if (message != null
                                        && message.getContent() != null
                                        && message.getContent().getMentionedInfo() != null
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        != null
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        .size()
                                        > 0
                                        && message.getContent()
                                        .getMentionedInfo()
                                        .getMentionedUserIdList()
                                        .get(0)
                                        .equals(String.valueOf(-1))) {
                                    message.getContent()
                                            .getMentionedInfo()
                                            .setType(MentionedInfo.MentionedType.ALL);
                                }
                            }

                            @Override
                            public void onDeleteClick(
                                    Conversation.ConversationType type,
                                    String targetId,
                                    EditText editText,
                                    int cursorPos) {
                            }

                            @Override
                            public void onDestroy(
                                    Conversation.ConversationType type, String targetId) {
                            }
                        });
    }

    /**
     * 注册消息及消息模版
     */
    private static void initMessageAndTemplate() {
        //以下以注册自定义消息
        ArrayList<Class<? extends MessageContent>> myMessages = new ArrayList<>();
        myMessages.add(MomentContentMessage.class);
        myMessages.add(TopicCircleMessage.class);
        myMessages.add(NBarMessage.class);
        myMessages.add(InviteMessage.class);
        myMessages.add(UserPageMessage.class);
        myMessages.add(MomentVideoMessage.class);
        myMessages.add(NewsMessage.class);

        RongIMClient.registerMessageType(myMessages);

        //自定义展示模板--->注册展示模板--->配置模板属性(默认即可)mConfig.showReadState = true; //修改模板属性
        //@Link https://doc.rongcloud.cn/im/Android/5.X/ui/function/message/customize
        RongConfigCenter.conversationConfig().addMessageProvider(new MomentContentMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new TopicCircleMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new NBarMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new InviteMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new UserPageMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new MomentVideoMessageProvider());
        RongConfigCenter.conversationConfig().addMessageProvider(new NewsMessageProvider());

//        RongConfigCenter.conversationConfig()
//                .replaceMessageProvider(
//                        GroupNotificationMessageItemProvider.class,
//                        new SealGroupNotificationMessageItemProvider());
    }

    /**
     * 初始化扩展模块
     *
     * @param context
     */
    private static void initExtensionModules(Context context) {
//        // 语音输入
        RongExtensionManager.getInstance().registerExtensionModule(new RecognizeExtensionModule());

        // 小视频
        RongExtensionManager.getInstance().registerExtensionModule(new SightExtensionModule());
        // 戳一下
//        RongExtensionManager.getInstance().registerExtensionModule(new PokeExtensionModule());

        // 个人名片
//        RongExtensionManager.getInstance()
//                .registerExtensionModule(createContactCardExtensionModule());

        RongExtensionManager.getInstance().setExtensionConfig(new DiscoExtensionConfig());
    }

    /**
     * 初始化推送
     */
    private static void initPush() {
        /*
         * 配置 融云 IM 消息推送
         * 根据需求配置各个平台的推送
         * 配置推送需要在初始化 融云 SDK 之前
         */
        PushConfig config =
                new PushConfig.Builder()
                        .enableHWPush(
                                true) // 在 AndroidManifest.xml 中搜索 com.huawei.hms.client.appid 进行设置
                        .enableMiPush(
                                BuildConfig.MI_APP_ID,
                                BuildConfig.MI_APP_KEY)
                        .enableMeiZuPush(
                                BuildConfig.MEIZU_APPID,
                                BuildConfig.MEIZU_APPKEY)
                        .enableVivoPush(true) // 在 AndroidManifest.xml 中搜索 com.vivo.push.api_key 和
                        // com.vivo.push.app_id 进行设置
                        .enableOppoPush(
                                BuildConfig.OPPO_AK,
                                BuildConfig.OPPO_SK)
                        .build();
        RongPushClient.setPushConfig(config);
        RongPushClient.setPushEventListener(
                new PushEventListener() {
                    @Override
                    public boolean preNotificationMessageArrived(
                            Context context,
                            PushType pushType,
                            PushNotificationMessage notificationMessage) {
                        RLog.d(TAG, "preNotificationMessageArrived");
                        RongPushClient.recordPushArriveEvent(
                                context, pushType, notificationMessage);
                        return false;
                    }

                    @Override
                    public void afterNotificationMessageArrived(
                            Context context,
                            PushType pushType,
                            PushNotificationMessage notificationMessage) {
                        RLog.d(TAG, "afterNotificationMessageArrived");
                    }

                    @Override
                    public boolean onNotificationMessageClicked(
                            Context context,
                            PushType pushType,
                            PushNotificationMessage notificationMessage) {
                        RLog.d(TAG, "onNotificationMessageClicked");
                        if (!notificationMessage
                                .getSourceType()
                                .equals(PushNotificationMessage.PushSourceType.FROM_ADMIN)) {
                            String targetId = notificationMessage.getTargetId();
                            // 10000 为 Demo Server 加好友的 id，若 targetId 为 10000，则为加好友消息，默认跳转到
                            // NewFriendListActivity
//                            if (targetId != null && targetId.equals("10000")) {
//                                Intent intentMain =
//                                        new Intent(context, NewFriendListActivity.class);
//                                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                Intent intentNewFriend = new Intent(context, MainActivity.class);
//                                intentNewFriend.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                Intent[] intents = new Intent[]{};
//                                intents[0] = intentMain;
//                                intents[1] = intentNewFriend;
//                                context.startActivities(intents);
//                                return true;
//                            } else {
//                                Intent intentMain = new Intent(context, SplashActivity.class);
//                                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                context.startActivity(intentMain);
//                            }
                        }
                        return false;
                    }

                    @Override
                    public void onThirdPartyPushState(
                            PushType pushType, String action, long resultCode) {
                    }
                });
    }
}
