package com.yanhua.common.utils;

import android.content.Context;
import android.text.TextUtils;

import com.yanhua.base.event.MessageEvent;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.UserInfo;
import com.yanhua.core.mmkv.MMKVUtil;

import org.greenrobot.eventbus.EventBus;

import io.rong.imkit.RongIM;

/**
 * 通过此获取用户相关信息
 */
public class UserManager {
    private static Context mContext;
    private static MMKVUtil mMMKV;
    private static UserManager instance;

    private UserManager() {
    }

    public static void init(Context context) {
        mMMKV = MMKVUtil.getInstance();
        mContext = context;
    }

    public static UserManager getInstance() {
        if (instance == null) {
            synchronized (UserManager.class) {
                if (instance == null) {
                    instance = new UserManager();
                }
            }
        }
        return instance;
    }

    //--------------------------------这些信息抛出到此处进行管理，不要直接调用MMKVUtil中的方法-----------
    public boolean isLogin() {
        return mMMKV.isLogin();
    }

    public void setLogin(boolean isLogin) {
        mMMKV.setLogin(isLogin);
    }

    public boolean isFirst() {
        return mMMKV.isFirst();
    }

    public void setFirst(boolean isFirst) {
        mMMKV.setFirst(isFirst);
    }

    public String getLoginToken() {
        return mMMKV.getLoginToken();
    }

    public void setLoginToken(String loginToken) {
        mMMKV.setLoginToken(loginToken);
    }

    public String getUserId() {
        return mMMKV.getUserId();
    }

    public void setUserId(String userId) {
        mMMKV.setUserId(userId);
    }

    public void logout() {
        setLogin(false);
        setLoginToken("");

        setUserId(null);

        setUserInfo(null);
        RongIM.getInstance().disconnect();//断开连接后仍然需要接受到新消息提醒的场景----断开和融云的连接，有新消息时，仍然能够收到通知提醒。
        RongIM.getInstance().logout();//注销账号，切换账号时，推荐使用此方法---彻底注销登录信息，有新消息时不再收到任何通知提醒

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_OUT));
    }

    //------------------------------------------------------------
    public String getStsToken() {
        return mMMKV.getString("ossToken");
    }

    public void setStsToken(String ossToken) {
        mMMKV.putString("ossToken", ossToken);
    }

    //设置用户信息
    private UserInfo userInfo;

    public void setUserInfo(UserInfo info) {
        if (null!=info) {
            userInfo = info;

            setUserId(userInfo.getId());

            EventBus.getDefault().post(new MessageEvent(ConstanceEvent.REFRESH_USER_INFO));
        }
    }

    public void setUserInfo(UserInfo info, boolean refresh) {
        userInfo = info;
        if (refresh) {
            EventBus.getDefault().post(new MessageEvent(ConstanceEvent.REFRESH_USER_INFO));
        }
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public boolean isMyself(String userId) {
        return !TextUtils.isEmpty(userId) && userId.equals(getUserId());
    }
}
