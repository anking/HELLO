package com.yanhua.common.utils;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

public class RecycleViewUtils {

    public static void clearRecycleAnimation(RecyclerView rvContent){
        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvContent.setRecycledViewPool(pool);

        //关闭recyclerview动画
        rvContent.getItemAnimator().setAddDuration(0);
        rvContent.getItemAnimator().setChangeDuration(0);
        rvContent.getItemAnimator().setMoveDuration(0);
        rvContent.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvContent.getItemAnimator()).setSupportsChangeAnimations(false);
    }
}
