package com.yanhua.common.utils;

import com.alibaba.sdk.android.oss.model.PutObjectResult;

public interface OSSPushListener {

    void onProgress(long currentSize, long totalSize);

    void onSuccess(PutObjectResult result);

    void onFailure();

}
