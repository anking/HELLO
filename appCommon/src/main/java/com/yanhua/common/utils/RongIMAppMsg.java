package com.yanhua.common.utils;

import android.content.Context;

import com.blankj.utilcode.util.ToastUtils;

import io.rong.imkit.IMCenter;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;

public class RongIMAppMsg {

    public static void sendCustomMessage(final Context context, Message message, boolean isShowToast, String pushContent) {

        IMCenter.getInstance().sendMessage(message, pushContent, null,
                !isShowToast ? (IRongCallback.ISendMessageCallback) null : new IRongCallback.ISendMessageCallback() {
                    @Override
                    public void onAttached(Message message) {
                    }

                    @Override
                    public void onSuccess(Message message) {
                        ToastUtils.showShort("发送成功");
                    }

                    @Override
                    public void onError(Message message, RongIMClient.ErrorCode errorCode) {
                        ToastUtils.showShort("发送失败");
                    }
                });
    }
}
