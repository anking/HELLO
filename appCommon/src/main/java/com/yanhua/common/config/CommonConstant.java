package com.yanhua.common.config;

public class CommonConstant {

    public static final String SHOE_MAIN_TAB = "show_main_tab";
    public static final String HIDE_MAIN_TAB = "hide_main_tab";


    public static final String TO_MOMENT_PUBLISH = "TO_MOMENT_PUBLISH";

    // 多张图片上传
    public static final String LOGIN_SUCCESS = "login_success";
    public static final String LOGINED_GET_USERINFO = "logined_success";
    public static final String MESSAGE_UNREAD_COUNT = "show_unread";
    public static final String USER_INFO = "USER_INFO";

    public static final String LOGIN_OUT = "login_out";

    public static final String KICKED_OFFLINE_BY_OTHER_CLIENT = "KICKED_OFFLINE_BY_OTHER_CLIENT";

    public static final String CONN_USER_BLOCKED = "CONN_USER_BLOCKED";

    public static final String UPDATE_USER_DETAIL_SUCCESS = "update_user_detail_success";

    public static final String HOME_SEARCH = "home_search";

    public static final String GOODS_CATEGORY_SEARCH = "goods_category_search";

    public static final String YES_VALUE = "YES";

    public static final String NO_VALUE = "NO";

    public static final String FROZEN_BEAN = "frozen_bean";

    public static final String EXCHANGE_BEAN = "exchange_bean";

    public static final String EXCHANGE_POINT = "exchange_point";

    public static final String EXCHANGE_FAVORITE = "exchange_favorite";

    public static final String UPDATE_CART = "update_cart";

    public static final String UPDATE_SALE_SERVICE = "update_sale_service";

    public static final String ADD_APPRAISE_SUCCESS = "add_appraise_success";

    public static final String PUBLISH_CONTENT = "publish_content";
    public static final String MOMENT_HOME_LIST_REFRESH = "MOMENT_HOME_LIST_REFRESH";
    public static final String MOMENT_HOME_LIST_REFRESH_TOPIC = "MOMENT_HOME_LIST_REFRESH_TOPIC";
    public static final String MOMENT_HOME_LIST_REFRESH_CIRCLE = "MOMENT_HOME_LIST_REFRESH_CIRCLE";

    public static final String PAY_SUCCESS = "pay_success";
    public static final String NEARBY_PAY_SUCCESS = "nearby_pay_success";

    public static final String COMMIT_ORDER = "commit_order";

    public static final String HOME_BACK = "home_back";
    public static final String PUBLISH_CONTENT_BEFORE = "publish_content_before";
    public static final String STATION_PUBLISH_CONTENT_BEFORE = "station_publish_content_before";
    public static final String FINISH_PAGE = "finish_page";
    public static final String HOME_CLOSE_VIDEO_WINDOW = "HOME_CLOSE_VIDEO_WINDOW";

    public static final String CURRENT_LOCATION_CITY = "current_location_city";

    //发布
    public static final String ACTION_PUBLISH = "action_publish";

    //关注
    public static final String ACTION_CHAT_FOLLOW = "action_chat_follow";
    //取消关注
    public static final String ACTION_CHAT_UNFOLLOW = "action_chat_unfollow";

    //关注
    public static final String ACTION_FOLLOW = "action_follow";
    //取消关注
    public static final String ACTION_UNFOLLOW = "action_unfollow";

    //收藏
    public static final String ACTION_COLLECT = "action_collect";
    //点赞
    public static final String ACTION_LOVE = "action_love";

    //删除
    public static final String ACTION_DELETE_CONTENT = "action_delete_content";
//
    //刷新已加入
    public static final String ACTION_REFRESH_INVITE_JOINDE = "ACTION_REFRESH_INVITE_JOINDE";
    //待加入
    public static final String ACTION_REFRESH_INVITE_WAITING = "ACTION_REFRESH_INVITE_WAITING";
    public static final String ACTION_HIDE_INVITE_TOP_TIP = "ACTION_HIDE_INVITE_TOP_TIP";



    // 刷新商品订单列表
    public static final String REFRESH_GOODS_ORDER_LIST = "refresh_goods_order_list";

    // 刷新商品订单列表和详情
    public static final String REFRESH_GOODS_ORDER_DETAIL_LIST = "refresh_goods_order_detail_list";

    // 刷新视频详情页面的评论数
    public static final String REFRESH_CONTENT_COMMENTS_COUNT = "refresh_content_comments_count";

    // 充值成功
    public static final String RECHARGE_SUCCESS = "recharge_success";

    // 选择发送商品
    public static final String SELECT_SEND_GOODS = "select_send_goods";

    // 群组相关
    public static final String IM_GROUP_RELATE = "im_group_relate";

    // fenxiang
    public static final String SHARE_APP_LIST = "share_app_list";

    // 删除历史
    public static final String IM_GROUP_HISTORY_DELETE = "IM_GROUP_HISTORY_DELETE";

    // 刷新音乐库收藏列表
    public static final String REFRESH_MUSIC_COLLECTION = "refresh_music_collection";

    // 刷新附近首页
    public static final String REFRESH_NEARBY_HOME = "refresh_nearby_home";

    // 结束排队
    public static final String FINISH_QUEUE = "finish_queue";

    // 结束会话
    public static final String FINISH_SESSION = "finish_session";

    // 显示排队人数
    public static final String SHOW_LINE_UP_NUM = "show_line_up_num";

    // 隐藏排队人数
    public static final String HIDE_LINE_UP_NUM = "hide_line_up_num";

    // 重连IM
    public static final String RECONNECT_IM = "reconnect_im";

    // 滚动到顶部
    public static final String SCROLL_TO_TOP = "scroll_to_top";

    // 绑定消息RID
    public static final String BAND_MSG_RID = "band_msg_rid";

    // 解除绑定消息RID
    public static final String UNBAND_MSG_RID = "unband_msg_rid";

    // 绑定消息RID
    public static final String BAND_MSG_RID_API = "sysadmin/app/jpushRegistration/add";

    // 解除绑定消息RID
    public static final String UNBAND_MSG_RID_API = "sysadmin/app/jpushRegistration/remove";

    // 关闭自动兑换页面
    public static final String CLOSE_AUTO_EXCHANGE = "close_auto_exchange";

    // 重新搜索机票报价
    public static final String AIRPLANE_RESEARCH_PRICE = "airplane_research_price";

    // 申请权限
    public static final String REQUEST_PERMISSIONS = "request_permission";

    // app进入后台
    public static final String COME_BACKGROUND = "come_background";

    // app进入前台
    public static final String COME_FOREGROUND = "come_foreground";
}
