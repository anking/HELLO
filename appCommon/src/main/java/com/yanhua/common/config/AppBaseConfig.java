package com.yanhua.common.config;

public enum AppBaseConfig {

    /**
     * 1:用户封禁原因, 2:举报原因, 3:驳回原因,
     * 4:取消授权原因, 5:约伴停约原因, 6:约伴取消原因,
     * 7:投诉原因, 8:解散原因, 9:交友目的,
     * 10:职业设置, 11:情感状况, 12:酒吧类型, 13:酒吧标签, 14:反馈类型)
     */
    TYPE_USER_BLOCK(1),
    TYPE_REPORT_REASON(2),
    TYPE_REJECT_REASON(3),
    TYPE_CANCEL_AUTHORIZE_REASON(4),
    TYPE_STOP_INVITE_REASON(5),
    TYPE_CANCEL_INVITE_REASON(6),
    TYPE_COMPLAIN_REASON(7),
    TYPE_DISBAND_REASON(8),
    TYPE_INTENTION_MAKE_FRIENDS(9),
    TYPE_PROFESSION(10),
    TYPE_EMOTION(11),
    TYPE_BAR_TYPE(12),
    TYPE_BAR_TAG(13),
    TYPE_FEEDBACK_TYPE(14);

    AppBaseConfig(int i) {
        this.value = i;
    }

    private int value;

    public int getValue() {
        return value;
    }

}
