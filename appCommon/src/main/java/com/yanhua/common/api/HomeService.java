package com.yanhua.common.api;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.InviteCreateTypeModel;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.model.InviteMemberNum;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteSetInfoModel;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishResultModel;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.model.UserModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface HomeService {
    //-------------------------------------------api---------method------------------------------
//    @POST(Api.meetPartner.MEET_PARTNER_INVITE_COLLECT_ADD)
//    Flowable<HttpResult> meetPartnerInviteCollect(@Body RequestBody body);
//
//    @DELETE(Api.meetPartner.MEET_PARTNER_INVITE_COLLECT_DELETE)
//    Flowable<HttpResult> meetPartnerInviteUnCollect(@Path("id") String id);
//
//    @POST(Api.meetPartner.MEET_PARTNER_INVITE_MESSAGE_ADD)
//    Flowable<HttpResult> meetPartnerInviteMessage(@Body RequestBody body);
//    @POST(Api.meetPartner.MEET_PARTNER_INVITE_MESSAGE_PAGE)
//    Flowable<HttpResult> meetPartnerInviteMessageList(@Body RequestBody body);
//
//    @POST(Api.meetPartner.MEET_PARTNER_ADD_FABULOUS)
//    Flowable<HttpResult> meetPartnerAddFabulous(@Body RequestBody body);
//
//    @DELETE(Api.meetPartner.MEET_PARTNER_DELETE_FABULOUS)
//    Flowable<HttpResult> meetPartnerDeleteFabulous(@Path("id") String id);

    @POST(Api.home.HOME_SEARCH)
    Flowable<HttpResult<ListResult<HomeSearchResultModel>>> homeSearch(@Body RequestBody body);

    @POST(Api.home.USER_SEARCH)
    Flowable<HttpResult<ListResult<UserModel>>> userSearch(@Body RequestBody body);

    @GET(Api.moment.MOMENT_TOPIC_HOT_LIST)
    Flowable<HttpResult<ListResult<TopicModel>>> getHotTopicList(@QueryMap HashMap<String, Object> params);

    @GET(Api.moment.MOMENT_CIRCLE_HOT_LIST)
    Flowable<HttpResult<ListResult<CircleModel>>> getHotCircleList(@QueryMap HashMap<String, Object> params);


    //-------start------爆料相关-------------------------------
    @GET(Api.home.NEWS_DETAIL)
    Flowable<HttpResult<NewsDetailModel>> getNewsDetail(@Path("id") String id);

    @GET(Api.home.HOME_BREAK_NEWS_LIST)
    Flowable<HttpResult<ListResult<BreakNewsRecommendModel>>> homePageBreakNewsList(@Query("current") int current, @Query("size") int size);

    /**
     * 最新爆料滚动活动条栏---以及历史
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.BREAK_NEWS_RECENT_ACTIVE)
    Flowable<HttpResult<ListResult<ActivityModel>>> getRecentActiveList(@Query("type") int type, @Query("queryType") int queryType,
                                                                        @Query("current") int current, @Query("size") int size,
                                                                        @Query("city") String city, @Query("contentCategoryId") String contentCategoryId);

    /**
     * 最新爆料 推荐置顶
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.BREAK_NEWS_RECOMMEND)
    Flowable<HttpResult<List<BreakNewsRecommendModel>>> getNewsRecommendList(@Query("type") int type, @Query("queryType") int queryType,
                                                                             @Query("current") int current, @Query("size") int size,
                                                                             @Query("city") String city, @Query("contentCategoryId") String contentCategoryId);

    /**
     * //获取爆料分类和攻略分类
     *
     * @param type 类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.BREAK_NEWS_TYPE_LIST)
    Flowable<HttpResult<List<NewsTypeModel>>> getNewsTypeList(@Path("type") int type, @Query("publish") int publish);//?publish=1

    /**
     * 这个获取城市不需要分页
     *
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     *                          queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @return
     */
    @GET(Api.home.BREAK_NEWS_ACTIVITY_CITY_LIST)
    Flowable<HttpResult<List<String>>> recentActiveCityList(@Query("contentCategoryId") String contentCategoryId, @Query("type") int type,@Query("queryType") int queryType);

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param city              城市
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.BREAK_NEWS_CITY_CONTENT_LIST)
    Flowable<HttpResult<ListResult<BreakNewsListModel>>> cityContentList(@Query("current") int current, @Query("size") int size,
                                                                         @Query("city") String city, @Query("keyword") String keyword,
                                                                         @Query("contentCategoryId") String contentCategoryId, @Query("type") int type);

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.STRATEGY_CONTENT_LIST)
    Flowable<HttpResult<ListResult<StrategyModel>>> strategyList(@Query("current") int current, @Query("size") int size, @Query("keyword") String keyword,
                                                                 @Query("contentCategoryId") String contentCategoryId, @Query("type") int type);

    /**
     * @param attributeType     attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
     * @param contentCategoryId 这个不用管
     * @param type              type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    @GET(Api.home.STRATEGY_HOTSORT_CONTENT_LIST)
    Flowable<HttpResult<List<StrategyHotSortModel>>> hotSortList(@Query("attributeType") int attributeType, @Query("contentCategoryId") String contentCategoryId, @Query("type") int type);

    @GET(Api.home.STRATEGY_HOTSORT_CONTENT_LIST)
    Flowable<HttpResult<List<StrategyHotSortModel>>> hotSortList(@Query("attributeType") int attributeType, @Query("type") int type);

    //发表内容 POST
    @POST(Api.home.NEWS_CONTENT_PUBLISH)
    Flowable<HttpResult<PublishResultModel>> publishNewsDetail(@Body RequestBody body);

    //爆料或攻略流量历史
    @POST(Api.home.NEWS_CONTENT_VIEW_HISTORY)
    Flowable<HttpResult<ListResult<MyNewsListModel>>> newsViewHistoryList(@Body RequestBody body);

    //我的动态爆料或攻略
    @POST(Api.home.MY_DYNAMIC_NEWS)
    Flowable<HttpResult<ListResult<MyNewsListModel>>> newsDynamicList(@Body RequestBody body);

    //我的收藏爆料或攻略
    @POST(Api.home.MY_FAV_NEWS)
    Flowable<HttpResult<ListResult<MyNewsListModel>>> newsCollectList(@Body RequestBody body);
    //--------end-----爆料相关-------------------------------

    @POST(Api.meetPartner.MEET_PARTNER_HISTORY_PAGE)
    Flowable<HttpResult<ListResult<InviteModel>>> meetPartnerHistoryList(@Body RequestBody body);

    @POST(Api.home.DELETE_NEWS_CONTENT)
    Flowable<HttpResult> deleteNewsContent(@Body RequestBody body);

    //约伴类型
    @POST(Api.meetPartner.MEET_PARTNER_TYPE_LIST)
    Flowable<HttpResult<List<InviteTypeModel>>> meetPartnerTypeList(@Body RequestBody body);

    //约伴类型模版
    @POST(Api.meetPartner.MEET_PARTNER_TEMPLATE_LIST)
    Flowable<HttpResult<List<InviteTemplateModel>>> meetPartnerTemplateList(@Body RequestBody body);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_ADD)
    Flowable<HttpResult> meetPartnerInviteAdd(@Body RequestBody body);

    @DELETE(Api.meetPartner.MEET_PARTNER_INVITE_DELETE)
    Flowable<HttpResult> meetPartnerInviteDelete(@Path("id") String id);


    @GET(Api.meetPartner.MEET_PARTNER_INVITE_SET_INFO)
    Flowable<HttpResult<InviteSetInfoModel>> meetPartnerInviteSetInfo(@Query("id") String id);

    @GET(Api.meetPartner.MEET_PARTNER_INVITE_DETAIL)
    Flowable<HttpResult<InviteModel>> meetPartnerInviteDetail(@Path("id") String id);

    @GET(Api.meetPartner.MEET_PARTNER_UPDATE_STATUS)
    Flowable<HttpResult> meetPartnerUpdateStatus(@Path("id") String id);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_PAGE)
    Flowable<HttpResult<ListResult<InviteModel>>> meetPartnerInvitePage(@Body RequestBody body);

    @POST(Api.meetPartner.MEET_PARTNER_MYINVITE_PAGE)
    Flowable<HttpResult<ListResult<InviteModel>>> meetPartnerMyInvitePage(@Body RequestBody body);

    @PUT(Api.meetPartner.MEET_PARTNER_INVITE_INFO_UPDATE)
    Flowable<HttpResult> meetPartnerInviteInfoUpdate(@Body RequestBody body);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_SINGIN)
    Flowable<HttpResult> meetPartnerInviteSignIn(@Body RequestBody body);

    @GET(Api.meetPartner.MEET_PARTNER_INVITE_AGREE_SINGIN)
    Flowable<HttpResult> meetPartnerInviteAgreeSignIn(@Path("id") String id);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_SINGOUT)
    Flowable<HttpResult> meetPartnerInviteSignOut(@Body RequestBody body);

    @GET(Api.meetPartner.MEET_PARTNER_INVITE_NUM)
    Flowable<HttpResult<InviteMemberNum>> meetPartnerInviteNum(@Path("id") String id);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_MEMBER_LIST)
    Flowable<HttpResult<ListResult<InviteMemberModel>>> meetPartnerInviteMembers(@Body RequestBody body);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_REMOVE_SINGED)
    Flowable<HttpResult> meetPartnerInviteRemoveSigned(@Body RequestBody body);

    @POST(Api.meetPartner.MEET_PARTNER_INVITE_CANCEL)
    Flowable<HttpResult> meetPartnerStopReason(@Body RequestBody body);

    @GET(Api.meetPartner.MEET_PARTNER_CREATE_TYPE)
    Flowable<HttpResult<InviteCreateTypeModel>> meetPartnerCreateType();


    @POST(Api.user.UPLOAD_USER_LOCATION_INFO)
    Flowable<HttpResult> uploadUserLocation(@Body RequestBody body);


    /*********************************酒吧模块start****************************/
    @POST(Api.bar.GET_BAR_LIST)
    Flowable<HttpResult<ListResult<BarModel>>> getBarList(@Body RequestBody body);

    @POST(Api.bar.GET_BAR_DETAIL)
    Flowable<HttpResult<BarModel>> getBarDetail(@Path("id") String barId, @Body RequestBody body);

    @POST(Api.bar.DO_LOVE_OR_NOT_BAR)
    Flowable<HttpResult> doLoveOrNotBar(@Path("barId") String barId);

    @POST(Api.bar.GET_BAR_QA_LIST)
    Flowable<HttpResult<ListResult<BarQAModel>>> getBarQAList(@Body RequestBody body);

    @POST(Api.bar.GET_BAR_QA_DETAIL)
    Flowable<HttpResult<BarQAModel>> getBarQADetail(@Body RequestBody body);

    @POST(Api.bar.GET_BAR_ANSWER_DETAIL)
    Flowable<HttpResult<MyQAModel>> getBarAnswerDetail(@Path("askId") String askId);

    @POST(Api.bar.POST_MY_QA)
    Flowable<HttpResult> postMyQA(@Body RequestBody body);

    @POST(Api.bar.UPDATE_MY_QUESTION)
    Flowable<HttpResult> updateMyQuestion(@Body RequestBody body);

    @DELETE(Api.bar.DELETE_MY_QUESTION)
    Flowable<HttpResult> deleteMyQuestion(@Path("askedId") String askedId);

    @DELETE(Api.bar.DELETE_MY_ANSWER)
    Flowable<HttpResult> deleteMyAnswer(@Path("askedId") String askedId);

    @POST(Api.bar.POST_MY_COMMENT)
    Flowable<HttpResult> postMyComment(@Body RequestBody body);

    @POST(Api.bar.GET_MY_BAR_QA_LIST)
    Flowable<HttpResult<ListResult<MyQAModel>>> getMyBarQAList(@Body RequestBody body);

    @POST(Api.bar.GET_COLLECT_BAR_LIST)
    Flowable<HttpResult<ListResult<BarModel>>> getMyColloectBarList(@Body RequestBody body);
    /*********************************酒吧模块end****************************/
}
