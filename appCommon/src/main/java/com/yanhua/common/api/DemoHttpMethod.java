package com.yanhua.common.api;


import com.yanhua.base.config.YXConfig;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;

public class DemoHttpMethod extends BaseRtHttpMethod {

    DemoService api;

    public DemoHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), DemoService.class);
    }

    private static class DemoHttpMethodHolder {
        private static final DemoHttpMethod INSTANCE = new DemoHttpMethod();
    }

    //获取单例
    public static DemoHttpMethod getInstance() {
        return DemoHttpMethodHolder.INSTANCE;
    }
    //-------------------------------------------api---------method------------------------------

}