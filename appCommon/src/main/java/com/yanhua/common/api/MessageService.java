package com.yanhua.common.api;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupResult;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgLastModel;
import com.yanhua.common.model.MsgNewFansModel;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.model.RongUserInfo;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MessageService {
    //获取最近的通知(包含未读数量)-GET
    @GET(Api.message.MESSAGE_SYS_NOTICE_LIST_LAST)
    Flowable<HttpResult<MsgLastModel>> getSysMsgLastItem();

    //系统消息分页请求-POST "current": 0, "size": 0,
    @POST(Api.message.MESSAGE_SYS_NOTICE_LIST_PAGE)
    Flowable<HttpResult<ListResult<MsgSysModel>>> getSysMsgList(@Body RequestBody body);

    @POST(Api.message.GET_BAR_MSG_LIST)
    Flowable<HttpResult<ListResult<BarInterActMsgModel>>> getBarInteractMsgList(@Body RequestBody body);

    //已读-GET
    @GET(Api.message.MESSAGE_SYS_NOTICE_READ_ITEM)
    Flowable<HttpResult> dealReadItem(@Path("id") String id);

    @GET(Api.message.MESSAGE_SYS_NOTICE_OPEN)
    Flowable<HttpResult> getSysMsgOpen(@Path("id") String id);

    @GET(Api.message.MESSAGE_SYS_NOTICE_DETAIL)
    Flowable<HttpResult> getSysMsgDetail(@Path("id") String id);

    //全部已读-GET
    @GET(Api.message.MESSAGE_SYS_NOTICE_READ_ALL)
    Flowable<HttpResult> dealReadAll();

    @POST(Api.message.MESSAGE_NEW_FANS_LIST_PAGE)
    Flowable<HttpResult<ListResult<MsgNewFansModel>>> getNewFansList(@Body RequestBody body);

    //已读-GET
    @GET(Api.message.MESSAGE_NEW_FANS_UNREAD_NUM)
    Flowable<HttpResult<Integer>> dealNewFansUnReadNum();

    //全部已读-GET
    @GET(Api.message.MESSAGE_NEW_FANS_READ_ALL)
    Flowable<HttpResult> dealNewFansReadAll();

    @GET(Api.message.MESSAGE_INTERACT_READ_ALL)
    Flowable<HttpResult> dealInteractReadAll();

    @GET(Api.message.MESSAGE_INTERACT_LIST_LAST)
    Flowable<HttpResult<Integer>> getInterActMsgLastItem();

    @POST(Api.message.MESSAGE_AT_COMMENT_LIST_PAGE)
    Flowable<HttpResult<ListResult<MsgCommentAtModel>>> getCommentAtList(@Body RequestBody body);

    //已读-GET
    @GET(Api.message.MESSAGE_AT_COMMENT_UNREAD_NUM)
    Flowable<HttpResult<Integer>> dealCommentAtUnReadNum();

    //全部已读-GET
    @GET(Api.message.MESSAGE_AT_COMMENT_READ_ALL)
    Flowable<HttpResult> dealCommentAtReadAll();

    @POST(Api.message.MESSAGE_FABULOUS_LIST_PAGE)
    Flowable<HttpResult<ListResult<MsgFabulousModel>>> getFabulousList(@Body RequestBody body);

    //已读-GET
    @GET(Api.message.MESSAGE_FABULOUS_UNREAD_NUM)
    Flowable<HttpResult<Integer>> dealFabulousUnReadNum();

    @GET(Api.message.MESSAGE_INVITE_UNREAD_NUM)
    Flowable<HttpResult<Integer>> dealInviteReadNum();

    //全部已读-GET
    @GET(Api.message.MESSAGE_FABULOUS_READ_ALL)
    Flowable<HttpResult> dealFabulousReadAll();

    @GET(Api.message.MESSAGE_INVITE_SET_READ_ALL)
    Flowable<HttpResult> dealInviteReadAll();

    @GET(Api.message.MESSAGE_INVITE_SET_READ_ID)
    Flowable<HttpResult> dealInviteReadId(@Path("id") String id);


    @POST(Api.message.MESSAGE_FABULOUS_USER_LIST)
    Flowable<HttpResult<ListResult<MsgUserModel>>> getFabulousUserList(@Body RequestBody body);


    @POST(Api.message.MESSAGE_INVITE_LIST)
    Flowable<HttpResult<ListResult<InviteMsgModel>>> getMessageInviteList(@Body RequestBody body);

    @GET(Api.message.GET_RONG_CLOUD_TOKEN)
    Flowable<HttpResult> getRongCloudToken();

    @GET(Api.user.GET_USER_INFO)
    Flowable<HttpResult<RongUserInfo>> getRongIMUserInfo(@Query("userId") String userId);

    @GET(Api.message.GET_USER_GROUP)
    Flowable<HttpResult<List<GroupsBean>>> getUserGroups(@Query("userId") String userId, @Query("likeName") String likeName);


    @POST(Api.message.RONGIM_GROUP_CREATE)
    Flowable<HttpResult<GroupResult>> addGroup(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_ADD_MEMBER)
    Flowable<HttpResult> addMember(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_REMOVE_MEMBER)
    Flowable<HttpResult> deleteMember(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_UPDATE_MANAGER)
    Flowable<HttpResult> updateGroupAdminister(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_CONVERT_MASTER)
    Flowable<HttpResult> convertGroupManagerr(@Body RequestBody body);

    @POST(Api.message.GROUP_ADD_COMPLAINT)
    Flowable<HttpResult> addGroupComplaint(@Body RequestBody body);


    @GET(Api.message.RONGIM_GROUP_MEMBERLIST)
    Flowable<HttpResult<List<DiscoGroupMember>>> queryGroupMembers(@Query("groupId") String groupId);

    @GET(Api.message.RONGIM_GROUP_DETAIL_INFO)
    Flowable<HttpResult<GroupInfo>> getGroupChat(@Query("groupId") String groupId);

    @POST(Api.message.RONGIM_GROUP_SWITCH_INVITATION)
    Flowable<HttpResult> setGroupSwitch(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_UPDATE_INFO)
    Flowable<HttpResult> setGroupInfo(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_EDITE_NOTICE)
    Flowable<HttpResult> setGroupNoticeInfo(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_SIGNOUT)
    Flowable<HttpResult> signOutGroup(@Body RequestBody body);

    @POST(Api.message.RONGIM_GROUP_DISMISS)
    Flowable<HttpResult> updateGroupOrDismiss(@Body RequestBody body);

    @POST(Api.app.ADD_BLACK)
    Flowable<HttpResult> addBlack(@Path("userId") String userId);

    @GET(Api.message.GET_BLACK_LIST)
    Flowable<HttpResult<ListResult<BlackMemberBean>>> getBlackList(@Query("username") String userName, @Query("current") int current, @Query("size") int size);

}
