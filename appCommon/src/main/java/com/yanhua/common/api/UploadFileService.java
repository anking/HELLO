package com.yanhua.common.api;

import com.yanhua.base.model.HttpResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.StsTokenModel;

import io.reactivex.rxjava3.core.Flowable;
import retrofit2.http.GET;

public interface UploadFileService {

    @GET(Api.user.GET_STS_TOKEN)
    Flowable<HttpResult<StsTokenModel>> getStsToken();
}
