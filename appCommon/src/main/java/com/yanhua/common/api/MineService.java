package com.yanhua.common.api;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.MineCommentModel;
import com.yanhua.common.model.MomentListModel;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MineService {
    //
    @POST(Api.mine.GET_MY_COMMENT_LIST)
    Flowable<HttpResult<ListResult<MineCommentModel>>> commentListQuery(@Body RequestBody body);

    @POST(Api.mine.MY_LIKE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getUserLikeList(@Body RequestBody body);
}
