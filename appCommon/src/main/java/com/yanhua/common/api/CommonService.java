package com.yanhua.common.api;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CityObjModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonConfigModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ProtocolVersionModel;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.model.ServerConfigModel;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommonService {
    //-------------------------------------------api---------method------------------------------
    @GET(Api.COMMON_CONFIG_BY_TYPE)
    Flowable<HttpResult<CommonConfigModel>> getConfigByType(@Path("type") int type);

    @POST(Api.ADD_VIEW_COUNT)
    Flowable<HttpResult> addViewCount(@Path("type") int type, @Path("id") String id);

    /**
     * {
     * "current": 0,
     * "extendId": "string", 	string
     * 扩展内容类型过滤
     * 举报原因(1:用户,2:此刻,3:话题,4:圈子,5:评论,6:约伴,7:新闻,8:酒吧问答)
     * 驳回原因(1:约伴,2:新闻,3:圈子)
     * 约伴取消原因(1:报名者, 2:邀约者)
     * "keyword": "string",
     * "size": 0,
     * "state": 0,
     * "type": 0,
     * integer($int32)
     * 配置类型(
     * 1:用户封禁原因, 2:举报原因, 3:驳回原因,
     * 4:取消授权原因, 5:约伴停约原因, 6:约伴取消原因,
     * 7:投诉原因, 8:解散原因, 9:交友目的,
     * 10:职业设置, 11:情感状态, 12:酒吧类型, 13:酒吧标签, 14:反馈类型)
     * "username": "string"
     * }
     *
     * @return
     */
    @POST(Api.COMMON_REASON_LIST)
    Flowable<HttpResult<List<CommonReasonModel>>> getCommonReasonList(@Body RequestBody body);

    @POST(Api.UPDATE_CONTENT_COLLECT)
    Flowable<HttpResult> collectContent(@Path("type") int type, @Path("id") String id);

    @POST(Api.DELETE_LIKE_NULL_CONTENT)
    Flowable<HttpResult> deleteLikeNullContent(@Path("id") String id);

    @DELETE(Api.UPDATE_CONTENT_COLLECT)
    Flowable<HttpResult> cancelCollectContent(@Path("type") int type, @Path("id") String id);

    @GET(Api.COMMENT_LILST_ITEM_TOP)
    Flowable<HttpResult> dealCommentItem2Top(@Path("type") int type, @Path("id") String id);

    @DELETE(Api.COMMENT_LILST_ITEM_DELETE)
    Flowable<HttpResult> deleteCommentItem(@Path("commentId") String commentId);

    @GET(Api.GET_COMMENT_LIST)
    Flowable<HttpResult<ListResult<CommentModel>>> getCommentList(@Query("contentId") String contentId, @Query("current") int current, @Query("childSize") int childSize,
                                                                  @Query("areaName") String areaName, @Query("systemType") int systemType);

    @GET(Api.GET_APP_PROTOCOL_BY_CODE)
    Flowable<HttpResult<ProtocolModel>> getAppProtocol(@Path("code") String code);

    @GET(Api.GET_APP_USER_PHONE)
    Flowable<HttpResult<Boolean>> getAppUserMobile(@Path("mobile") String mobile);

    @POST(Api.LOGOFF_USER)
    Flowable<HttpResult<Boolean>> userLogOff(@Body RequestBody body);


    @GET(Api.APP_CONTACT_US)
    Flowable<HttpResult<List<ServerConfigModel>>> getContactUs();

    @GET(Api.APP_CONTACT_CS)
    Flowable<HttpResult<List<ServerConfigModel>>> getContactCS();

    @GET(Api.QUERY_PROTOCOL_INFO)
    Flowable<HttpResult<List<ProtocolVersionModel>>> queryProtocol();

    @GET(Api.GET_CHILD_COMMENT_LIST)
    Flowable<HttpResult<ListResult<CommentModel>>> getChildCommentList(@Query("commentId") String commentId, @Query("current") int current, @Query("size") int size);

    @POST(Api.UPDATE_COMMENT_FABULOUS)
    Flowable<HttpResult> starComment(@Path("commentId") String commentId);

    @POST(Api.UPDATE_COMMENT_WORSE)
    Flowable<HttpResult> worseComment(@Path("commentId") String commentId);

    @DELETE(Api.UPDATE_COMMENT_FABULOUS)
    Flowable<HttpResult> cancelStarComment(@Path("commentId") String commentId);

    @POST(Api.PUBLISH_COMMENT)
    Flowable<HttpResult<CommentModel>> publishComment(@Body RequestBody body);

    @POST(Api.UPDATE_CONTENT_FABULOUS)
    Flowable<HttpResult> starContent(@Path("type") int type, @Path("id") String id);

    @DELETE(Api.UPDATE_CONTENT_FABULOUS)
    Flowable<HttpResult> cancelStarContent(@Path("type") int type, @Path("id") String id);

    @GET(Api.GET_ALL_CITY_LIST)
    Flowable<HttpResult<List<CityObjModel>>> getCity();

    @GET(Api.GET_ALL_AREA)
    Flowable<HttpResult<List<CityModel>>> getAllAreas();

    @GET(Api.GET_BAR_ALL_AREA)
    Flowable<HttpResult<List<CityModel>>> getBarAllAreas();

    @GET(Api.GET_CITY_OR_AREA)
    Flowable<HttpResult<List<CityModel>>> getCityOrArea();

    @GET(Api.GET_APP_EXPLAIN)
    Flowable<HttpResult<ProtocolModel>> getAppExplain(@Path("code") String code);

    @POST(Api.QUERY_AD_LIST)
    Flowable<HttpResult<List<AdvertiseModel>>> queryAdList(@Body RequestBody body);

    @POST(Api.ADD_MARKETING_CLICK)
    Flowable<HttpResult> addMarketingClick(@Body RequestBody body);


    @POST(Api.PROTOCOL_SUER_SIGN)
    Flowable<HttpResult> userSignProtocol(@Body RequestBody body);


    @GET(Api.GET_REPORT_TYPE_LIST)
    Flowable<HttpResult<List<ReportTypeBean>>> getReportReasonList(@Query("extendId") int extendId);

    @POST(Api.POST_REPORT_CONTENT)
    Flowable<HttpResult> postReportContent(@Body RequestBody body);

    @GET(Api.app.FOLLOW_USER)
    Flowable<HttpResult> followUser(@Path("userId") String userId);

    @GET(Api.app.CANCEL_FOLLOW_USER)
    Flowable<HttpResult> cancelFollowUser(@Path("userId") String userId);
}
