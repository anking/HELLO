package com.yanhua.common.api;


import android.text.TextUtils;

import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentCategoryModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.UserManager;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;

public class MomentHttpMethod extends BaseRtHttpMethod {
    MomentService momentService;

    public MomentHttpMethod() {
        this.momentService = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), MomentService.class);
    }

    private static class MomentHttpMethodHolder {
        private static final MomentHttpMethod INSTANCE = new MomentHttpMethod();
    }

    //获取单例
    public static MomentHttpMethod getInstance() {
        return MomentHttpMethodHolder.INSTANCE;
    }

    // 获取内容详情
    public SimpleSubscriber getContentDetail(SimpleSubscriber<HttpResult<MomentListModel>> subscriber, String id) {
        tocompose(momentService.getContentDetail(id), subscriber);
        return subscriber;
    }

    // 获取电音集锦内容详情
    public SimpleSubscriber getDiscoContentDetail(SimpleSubscriber<HttpResult<MomentListModel>> subscriber, String id) {
        tocompose(momentService.getDiscoContentDetail(id), subscriber);
        return subscriber;
    }

    // 查询会员详情
    public SimpleSubscriber getUserDetail(SimpleSubscriber<HttpResult<UserInfo>> subscriber, String userId) {
        tocompose(momentService.getUserDetail(userId), subscriber);
        return subscriber;
    }

    // 删除内容
    public SimpleSubscriber deleteContent(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(momentService.deleteContent(id), subscriber);
        return subscriber;
    }


    public SimpleSubscriber setFriendRemark(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(momentService.setFriendRemark(convertToJson(params)), subscriber);
        return subscriber;
    }

    //获取内容列表 0的时候不传contentType
    public SimpleSubscriber getContentList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(momentService.getContentList(params), subscriber);
        return subscriber;
    }

    //发布内容
    public SimpleSubscriber publishContent(SimpleSubscriber<HttpResult<ContentModel>> subscriber, RequestBody body) {
        tocompose(momentService.publishContent(body), subscriber);
        return subscriber;
    }

    public SimpleSubscriber applyCircle(SimpleSubscriber<HttpResult<Boolean>> subscriber, RequestBody body) {
        tocompose(momentService.applyCircle(body), subscriber);
        return subscriber;
    }

    public SimpleSubscriber editCircle(SimpleSubscriber<HttpResult<Boolean>> subscriber, RequestBody body, String id) {
        tocompose(momentService.editCircle(body, id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getContentCategory(SimpleSubscriber<HttpResult<List<ContentCategoryModel>>> subscriber) {
        tocompose(momentService.getContentCategory(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber queryCategory(SimpleSubscriber<HttpResult<List<ContentCategoryModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(momentService.queryCategory(params), subscriber);
        return subscriber;
    }

    // 添加或者取消黑名单
    public SimpleSubscriber addBlack(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(momentService.addBlack(userId), subscriber);
        return subscriber;
    }

    // 获取内容用户详情
    public SimpleSubscriber getContentUserDetail(SimpleSubscriber<HttpResult<ContentUserInfoModel>> subscriber, String userId) {
        tocompose(momentService.getContentUserDetail(userId), subscriber);
        return subscriber;
    }


    public SimpleSubscriber setBackGround(SimpleSubscriber<HttpResult> subscriber, String backGroupUrl) {
        tocompose(momentService.setBackGround(backGroupUrl), subscriber);
        return subscriber;
    }

    //获取用户内容列表
    public SimpleSubscriber getUserContentList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, int contentType, String userId, int current) {
        tocompose(momentService.getUserContentList(contentType, userId, current), subscriber);
        return subscriber;
    }

    //获取用户内容列表
    public SimpleSubscriber getUserContentList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String userId, int current) {
        tocompose(momentService.getUserContentList(userId, current), subscriber);
        return subscriber;
    }

    // 关注用户
    public SimpleSubscriber followUser(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(momentService.followUser(userId), subscriber);
        return subscriber;
    }

    // 取消关注用户
    public SimpleSubscriber cancelFollowUser(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(momentService.cancelFollowUser(userId), subscriber);
        return subscriber;
    }

    //获取关注的用户列表
    public SimpleSubscriber getFollowUserList(SimpleSubscriber<HttpResult<ListResult<FriendModel>>> subscriber, String userId, int current, int size, String keyword) {
        tocompose(momentService.getFollowUserList(userId, current, size, keyword), subscriber);
        return subscriber;
    }

    // 获取关注我的列表
    public SimpleSubscriber getNewAttentions(SimpleSubscriber<HttpResult> subscriber, int current, int size) {
        tocompose(momentService.getNewAttentions(current, size), subscriber);
        return subscriber;
    }

    //获取互相关注的用户列表
    public SimpleSubscriber getFriendUserList(SimpleSubscriber<HttpResult<ListResult<FriendModel>>> subscriber, String userId, String nickName, int current, int size) {
        tocompose(momentService.getFriendUserList(userId, nickName, current, size), subscriber);
        return subscriber;
    }

    //获取用户的粉丝列表
    public SimpleSubscriber getUserFansList(SimpleSubscriber<HttpResult<ListResult<FriendModel>>> subscriber, String userId, int current, int size, String keyword) {
        tocompose(momentService.getUserFansList(userId, current, size, keyword), subscriber);
        return subscriber;
    }

    // 喜欢内容列表
    public SimpleSubscriber getFavoriteContent(SimpleSubscriber<HttpResult<ListResult<ContentModel>>> subscriber, int current) {
        tocompose(momentService.getFavoriteContent(current), subscriber);
        return subscriber;
    }


    public SimpleSubscriber updatePrivacyTyoe(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(momentService.updatePrivacyType(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getTopicList(SimpleSubscriber<HttpResult<ListResult<TopicModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getTopicList(title, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getCircleList(title, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleJoinedList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getCircleJoinedList(title, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleManageList(SimpleSubscriber<HttpResult<ListResult<CircleManageModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getCircleManageList(title, current, size), subscriber);
        return subscriber;
    }


    public SimpleSubscriber getRecommendTopicList(SimpleSubscriber<HttpResult<List<TopicModel>>> subscriber) {
        tocompose(momentService.getRecommendTopicList(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getHotTopicList(SimpleSubscriber<HttpResult<ListResult<TopicModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getHotTopicList(title, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getHotCircleList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, String title, int current, int size) {
        tocompose(momentService.getHotCircleList(title, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMomentPageCityList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, int type, int current,
                                                  int size, String distance, String latitude, String longitude) {

        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        String cityName = TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress;

        tocompose(momentService.getMomentPageCityList(title, type, current, size, distance, latitude, longitude,cityName,1), subscriber);
        return subscriber;
    }
    public SimpleSubscriber getMomentPageVideoList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, int type, int current, int size) {
        tocompose(momentService.getMomentPageVideoList(title, type, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMomentPageList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, int type, int current, int size) {

        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        String cityName = TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress;

        tocompose(momentService.getMomentPageList(title, type, current, size,cityName,1), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getDiscoBestList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String keyword, String city, int current, int size) {
        tocompose(momentService.getDiscoBestList(keyword,city, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMomentUserList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, String userId, int current, int size) {
        tocompose(momentService.getMomentUserList(title, userId, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMyFavMomentList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, String userId, int current, int size) {
        tocompose(momentService.getMyFavMomentList(title, userId, current, size), subscriber);
        return subscriber;
    }

    /**
     * See {@link MineHttpMethod } for most of the details on how the zooming
     *
     * @param subscriber
     * @param title
     * @param userId
     * @param current
     * @param size
     * @return
     */
    @Deprecated
    public SimpleSubscriber getUserLikeList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, String userId, int current, int size) {
        tocompose(momentService.getUserLikeList(title, userId, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCirclePageList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, String circleId, int type, int current, int size) {
        tocompose(momentService.getCirclePageList(title, circleId, type, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber momentCircleJoin(SimpleSubscriber<HttpResult<Boolean>> subscriber, String id) {
        tocompose(momentService.momentCircleJoin(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber momentCircleQuit(SimpleSubscriber<HttpResult<Boolean>> subscriber, String id) {
        tocompose(momentService.momentCircleQuit(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber masterTransferCircle(SimpleSubscriber<HttpResult<Boolean>> subscriber, String cid, String uid) {
        tocompose(momentService.masterTransferCircle(cid, uid), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getTopicPageList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String title, String topicId, int type, int current, int size) {
        tocompose(momentService.getTopicPageList(title, topicId, type, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getTopicDetail(SimpleSubscriber<HttpResult<TopicModel>> subscriber, String topicId) {
        tocompose(momentService.getTopicDetail(topicId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleDetail(SimpleSubscriber<HttpResult<CircleModel>> subscriber, String circleId) {
        tocompose(momentService.getCircleDetail(circleId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleApplyDetail(SimpleSubscriber<HttpResult<CircleModel>> subscriber, String circleId) {
        tocompose(momentService.getCircleApplyDetail(circleId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleStatus(SimpleSubscriber<HttpResult<MomentCircleStatus>> subscriber, String circleId) {
        tocompose(momentService.getCircleStatus(circleId), subscriber);
        return subscriber;
    }


    public SimpleSubscriber getCircleMemberList(SimpleSubscriber<HttpResult<ListResult<MomentCircleMemberModel>>> subscriber, String title, String circleId, int current, int size) {
        tocompose(momentService.getCircleMemberList(title, circleId, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCircleApplyList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, int current, int size) {
        tocompose(momentService.getCircleApplyList(current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMasterCircleApplyList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, int current, int size) {
        tocompose(momentService.getMasterCircleApplyList(current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber circleMasterApply(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(momentService.circleMasterApply(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBestNoteCityList(SimpleSubscriber<HttpResult<ValueContentModel>> subscriber) {
        tocompose(momentService.getBestNoteCityList(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getDiscoBestCityList(SimpleSubscriber<HttpResult<ValueContentModel>> subscriber) {
        tocompose(momentService.getDiscoBestCityList(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBestNoteList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, String city, int current, int size) {
        tocompose(momentService.getBestNoteList(city, current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getDiscoCollectList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, int current, int size) {
        tocompose(momentService.getDiscoCollectList(current, size), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMomentBroweList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, int current, int size, String keyword) {
        tocompose(momentService.getMomentBroweList(current, size, UserManager.getInstance().getUserId(), keyword), subscriber);
        return subscriber;
    }

    public SimpleSubscriber addCircleViewCount(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(momentService.addCircleViewCount(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber addTopicViewCount(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(momentService.addTopicViewCount(id), subscriber);
        return subscriber;
    }
}