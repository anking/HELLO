package com.yanhua.common.api;

import android.text.TextUtils;

import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CityObjModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonConfigModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ProtocolVersionModel;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.common.utils.DiscoCacheUtils;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;

public class CommonHttpMethod extends BaseRtHttpMethod {

    CommonService api;

    public CommonHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), CommonService.class);
    }

    private static class HttpMethodHolder {
        private static final CommonHttpMethod INSTANCE = new CommonHttpMethod();
    }

    //获取单例
    public static CommonHttpMethod getInstance() {
        return HttpMethodHolder.INSTANCE;
    }
    //-------------------------------------------api---------method------------------------------

    public SimpleSubscriber getConfigByType(SimpleSubscriber<HttpResult<CommonConfigModel>> subscriber, int type) {
        tocompose(api.getConfigByType(type), subscriber);
        return subscriber;
    }

    // 添加浏览量
    public SimpleSubscriber addViewCount(SimpleSubscriber<HttpResult> subscriber, int type, String id) {
        tocompose(api.addViewCount(type, id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCommonReasonList(SimpleSubscriber<HttpResult<List<CommonReasonModel>>> subscriber, HashMap params) {
        tocompose(api.getCommonReasonList(convertToJson(params)), subscriber);//
        return subscriber;
    }

    // 收藏内容
    public SimpleSubscriber collectContent(SimpleSubscriber<HttpResult> subscriber, int type, String id) {
        tocompose(api.collectContent(type, id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber deleteLikeNullContent(SimpleSubscriber<HttpResult> subscriber,  String id) {
        tocompose(api.deleteLikeNullContent( id), subscriber);
        return subscriber;
    }

    // 点赞内容
    public SimpleSubscriber starContent(SimpleSubscriber<HttpResult> subscriber, int type, String id) {
        tocompose(api.starContent(type, id), subscriber);
        return subscriber;
    }

    //获取评论列表
    public SimpleSubscriber getCommentList(SimpleSubscriber<HttpResult<ListResult<CommentModel>>> subscriber, String contentId, int current, int childSize) {
        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        String cityName = TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress;
        tocompose(api.getCommentList(contentId, current, childSize, cityName, 1), subscriber);
        return subscriber;
    }

    //获取子评论列表
    public SimpleSubscriber getChildCommentList(SimpleSubscriber<HttpResult<ListResult<CommentModel>>> subscriber, String commentId, int current, int size) {
        tocompose(api.getChildCommentList(commentId, current, size), subscriber);
        return subscriber;
    }

    // 点赞评论
    public SimpleSubscriber starComment(SimpleSubscriber<HttpResult> subscriber, String commentId) {
        tocompose(api.starComment(commentId), subscriber);
        return subscriber;
    }

    // 踩赞
    public SimpleSubscriber worseComment(SimpleSubscriber<HttpResult> subscriber, String commentId) {
        tocompose(api.worseComment(commentId), subscriber);
        return subscriber;
    }

    // 取消点赞评论
    public SimpleSubscriber cancelStarComment(SimpleSubscriber<HttpResult> subscriber, int type, String commentId) {
        tocompose(api.cancelStarComment(commentId), subscriber);
        return subscriber;
    }

    // 删除评论
    public SimpleSubscriber deleteCommentItem(SimpleSubscriber<HttpResult> subscriber, String commentId) {
        tocompose(api.deleteCommentItem(commentId), subscriber);
        return subscriber;
    }

    // 置顶评论
    public SimpleSubscriber dealCommentItem2Top(SimpleSubscriber<HttpResult> subscriber, int type, String commentId) {
        tocompose(api.dealCommentItem2Top(type, commentId), subscriber);
        return subscriber;
    }

    //发布评论
    public SimpleSubscriber publishComment(SimpleSubscriber<HttpResult<CommentModel>> subscriber, RequestBody body) {
        tocompose(api.publishComment(body), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCity(SimpleSubscriber<HttpResult<List<CityObjModel>>> subscriber) {
        tocompose(api.getCity(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getAllAreas(SimpleSubscriber<HttpResult<List<CityModel>>> subscriber) {
        tocompose(api.getAllAreas(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarAllAreas(SimpleSubscriber<HttpResult<List<CityModel>>> subscriber) {
        tocompose(api.getBarAllAreas(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getCityOrArea(SimpleSubscriber<HttpResult<List<CityModel>>> subscriber) {
        tocompose(api.getCityOrArea(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getAppExplain(SimpleSubscriber<HttpResult<ProtocolModel>> subscriber, String code) {
        tocompose(api.getAppExplain(code), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getAppProtocol(SimpleSubscriber<HttpResult<ProtocolModel>> subscriber, String code) {
        tocompose(api.getAppProtocol(code), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getAppUserMobile(SimpleSubscriber<HttpResult<Boolean>> subscriber, String phone) {
        tocompose(api.getAppUserMobile(phone), subscriber);
        return subscriber;
    }


    public SimpleSubscriber getContactUs(SimpleSubscriber<HttpResult<List<ServerConfigModel>>> subscriber) {
        tocompose(api.getContactUs(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getContactCS(SimpleSubscriber<HttpResult<List<ServerConfigModel>>> subscriber) {
        tocompose(api.getContactCS(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber queryProtocol(SimpleSubscriber<HttpResult<List<ProtocolVersionModel>>> subscriber) {
        tocompose(api.queryProtocol(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber queryAdList(SimpleSubscriber<HttpResult<List<AdvertiseModel>>> subscriber,
                                        HashMap<String, Object> params) {
        tocompose(api.queryAdList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber addMarketingClick(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.addMarketingClick(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber userLogOff(SimpleSubscriber<HttpResult<Boolean>> subscriber, HashMap<String, Object> params) {
        tocompose(api.userLogOff(convertToJson(params)), subscriber);
        return subscriber;
    }


    public SimpleSubscriber userSignProtocol(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.userSignProtocol(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getReportReasonList(SimpleSubscriber<HttpResult<List<ReportTypeBean>>> subscriber, int extendId) {
        tocompose(api.getReportReasonList(extendId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber reportContent(SimpleSubscriber<HttpResult> subscriber, RequestBody body) {
        tocompose(api.postReportContent(body), subscriber);
        return subscriber;
    }

    // 关注用户
    public SimpleSubscriber followUser(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(api.followUser(userId), subscriber);
        return subscriber;
    }

    // 取消关注用户
    public SimpleSubscriber cancelFollowUser(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(api.cancelFollowUser(userId), subscriber);
        return subscriber;
    }
}