package com.yanhua.common.api;


import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.BarInterActMsgModel;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.model.DiscoGroupMember;
import com.yanhua.common.model.GroupInfo;
import com.yanhua.common.model.GroupResult;
import com.yanhua.common.model.GroupsBean;
import com.yanhua.common.model.InviteMsgModel;
import com.yanhua.common.model.MsgCommentAtModel;
import com.yanhua.common.model.MsgFabulousModel;
import com.yanhua.common.model.MsgLastModel;
import com.yanhua.common.model.MsgNewFansModel;
import com.yanhua.common.model.MsgSysModel;
import com.yanhua.common.model.MsgUserModel;
import com.yanhua.common.model.RongUserInfo;

import java.util.HashMap;
import java.util.List;

public class MessageHttpMethod extends BaseRtHttpMethod {

    MessageService api;

    public MessageHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), MessageService.class);
    }

    private static class MessageHttpMethodHolder {
        private static final MessageHttpMethod INSTANCE = new MessageHttpMethod();
    }

    //获取单例
    public static MessageHttpMethod getInstance() {
        return MessageHttpMethodHolder.INSTANCE;
    }

    //-------------------------------------------api---------method------------------------------
    //获取最近的通知(包含未读数量)-GET
    public SimpleSubscriber getSysMsgLastItem(SimpleSubscriber<HttpResult<MsgLastModel>> subscriber) {
        tocompose(api.getSysMsgLastItem(), subscriber);
        return subscriber;
    }
    public SimpleSubscriber getInterActMsgLastItem(SimpleSubscriber<HttpResult<Integer>> subscriber) {
        tocompose(api.getInterActMsgLastItem(), subscriber);
        return subscriber;
    }

    //系统消息分页请求-POST "current": 0, "size": 0,
    public SimpleSubscriber getSysMsgList(SimpleSubscriber<HttpResult<ListResult<MsgSysModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getSysMsgList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarInteractMsgList(SimpleSubscriber<HttpResult<ListResult<BarInterActMsgModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getBarInteractMsgList(convertToJson(params)), subscriber);
        return subscriber;
    }


    public SimpleSubscriber getSysMsgOpen(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(api.getSysMsgOpen(id), subscriber);
        return subscriber;
    }

    //已读-GET
    public SimpleSubscriber dealReadItem(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(api.dealReadItem(id), subscriber);
        return subscriber;
    }

    //全部已读-GET
    public SimpleSubscriber dealReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealReadAll(), subscriber);
        return subscriber;
    }
    public SimpleSubscriber dealInteractReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealInteractReadAll(), subscriber);
        return subscriber;
    }

    //新增粉丝分页请求-POST "current": 0, "size": 0,
    public SimpleSubscriber getNewFansList(SimpleSubscriber<HttpResult<ListResult<MsgNewFansModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getNewFansList(convertToJson(params)), subscriber);
        return subscriber;
    }

    //未读数量GET
    public SimpleSubscriber dealNewFansUnReadNum(SimpleSubscriber<HttpResult<Integer>> subscriber) {
        tocompose(api.dealNewFansUnReadNum(), subscriber);
        return subscriber;
    }

    //全部已读-GET
    public SimpleSubscriber dealNewFansReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealNewFansReadAll(), subscriber);
        return subscriber;
    }

    //新增AT 评论分页请求-POST "current": 0, "size": 0,
    public SimpleSubscriber getCommentAtList(SimpleSubscriber<HttpResult<ListResult<MsgCommentAtModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getCommentAtList(convertToJson(params)), subscriber);
        return subscriber;
    }

    //未读数量GET
    public SimpleSubscriber dealCommentAtUnReadNum(SimpleSubscriber<HttpResult<Integer>> subscriber) {
        tocompose(api.dealCommentAtUnReadNum(), subscriber);
        return subscriber;
    }

    //全部已读-GET
    public SimpleSubscriber dealCommentAtReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealCommentAtReadAll(), subscriber);
        return subscriber;
    }

    //新增点赞评论分页请求-POST "current": 0, "size": 0,
    public SimpleSubscriber getFabulousList(SimpleSubscriber<HttpResult<ListResult<MsgFabulousModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getFabulousList(convertToJson(params)), subscriber);
        return subscriber;
    }

    //邀约未读数量GET
    public SimpleSubscriber dealInviteReadNum(SimpleSubscriber<HttpResult<Integer>> subscriber) {
        tocompose(api.dealInviteReadNum(), subscriber);
        return subscriber;
    }

    //邀约全部已读-GET
    public SimpleSubscriber dealInviteReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealInviteReadAll(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber dealInviteReadId(SimpleSubscriber<HttpResult> subscriber,String id) {
        tocompose(api.dealInviteReadId(id), subscriber);
        return subscriber;
    }

    //邀约
    public SimpleSubscriber getMessageInviteList(SimpleSubscriber<HttpResult<ListResult<InviteMsgModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getMessageInviteList(convertToJson(params)), subscriber);
        return subscriber;
    }


    //未读数量GET
    public SimpleSubscriber dealFabulousReadNum(SimpleSubscriber<HttpResult<Integer>> subscriber) {
        tocompose(api.dealFabulousUnReadNum(), subscriber);
        return subscriber;
    }

    //全部已读-GET
    public SimpleSubscriber dealFabulousReadAll(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.dealFabulousReadAll(), subscriber);
        return subscriber;
    }
    public SimpleSubscriber getFabulousUserList(SimpleSubscriber<HttpResult<ListResult<MsgUserModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getFabulousUserList(convertToJson(params)), subscriber);
        return subscriber;
    }

    // 获取用户融云token
    public SimpleSubscriber getRongCloudToken(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.getRongCloudToken(), subscriber);
        return subscriber;
    }


    public SimpleSubscriber getRongIMUserInfo(SimpleSubscriber<HttpResult<RongUserInfo>> subscriber, String uid) {
        tocompose(api.getRongIMUserInfo(uid), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getUserGroups(SimpleSubscriber<HttpResult<List<GroupsBean>>> subscriber, String userId, String likeName) {
        tocompose(api.getUserGroups(userId,likeName), subscriber);
        return subscriber;
    }


    //-------------------------群组-------------------------------
    public SimpleSubscriber queryGroupMembers(SimpleSubscriber<HttpResult<List<DiscoGroupMember>>> subscriber, String groupId) {
        tocompose(api.queryGroupMembers(groupId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getGroupChat(SimpleSubscriber<HttpResult<GroupInfo>> subscriber, String groupId) {
        tocompose(api.getGroupChat(groupId), subscriber);
        return subscriber;
    }
    public SimpleSubscriber setGroupInfo(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.setGroupInfo(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }
    public SimpleSubscriber setGroupSwitch(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.setGroupSwitch(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber setGroupNoticeInfo(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.setGroupNoticeInfo(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber signOutGroup(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.signOutGroup(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    /**
     * 群聊/创建群组
     */
    public SimpleSubscriber addGroup(SimpleSubscriber<HttpResult<GroupResult>> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.addGroup(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber addMember(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.addMember(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber deleteMember(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.deleteMember(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber updateGroupAdminister(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.updateGroupAdminister(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }


    public SimpleSubscriber convertGroupManagerr(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.convertGroupManagerr(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber addGroupComplaint(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.addGroupComplaint(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber updateGroupOrDismiss(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.updateGroupOrDismiss(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    // 添加或者取消黑名单
    public SimpleSubscriber addBlack(SimpleSubscriber<HttpResult> subscriber, String userId) {
        tocompose(api.addBlack(userId), subscriber);
        return subscriber;
    }
    // 获取黑名单列表
    public SimpleSubscriber getBlackList(SimpleSubscriber<HttpResult<ListResult<BlackMemberBean>>> subscriber, String userName, int current, int size) {
        tocompose(api.getBlackList(userName, current,size), subscriber);
        return subscriber;
    }

    //-------------------------------------------end----------------------------------------------------

}