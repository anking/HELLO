package com.yanhua.common.api;


import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.StsTokenModel;

public class UploadFileHttpMethod extends BaseRtHttpMethod {

    UploadFileService uploadFileService;

    public UploadFileHttpMethod() {
        this.uploadFileService = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), UploadFileService.class, 120);
    }

    private static class UploadFileHttpMethodHolder {
        private static final UploadFileHttpMethod INSTANCE = new UploadFileHttpMethod();
    }

    //获取单例
    public static UploadFileHttpMethod getInstance() {
        return UploadFileHttpMethodHolder.INSTANCE;
    }

    public SimpleSubscriber getStsToken(SimpleSubscriber<HttpResult<StsTokenModel>> subscriber) {
        tocompose(uploadFileService.getStsToken(), subscriber);
        return subscriber;
    }

}