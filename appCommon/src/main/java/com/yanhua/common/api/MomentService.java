package com.yanhua.common.api;


import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentCategoryModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.model.ValueContentModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface MomentService {
    @GET(Api.moment.BEST_NOTE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getBestNoteList(@Query("city") String city, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.BEST_DISCO_CITY_LIST)
    Flowable<HttpResult<ValueContentModel>> getDiscoBestCityList();

    @GET(Api.moment.BEST_DISCO_COLLECT_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getDiscoCollectList(@Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_BROWE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMomentBroweList(@Query("current") int current, @Query("size") int size, @Query("userId") String userId, @Query("keyWord") String keyWord);

    @GET(Api.moment.BEST_NOTE_CITY_LIST)
    Flowable<HttpResult<ValueContentModel>> getBestNoteCityList();

    @GET(Api.moment.GET_CONTENT_DETAIL)
    Flowable<HttpResult<MomentListModel>> getContentDetail(@Path("id") String id);

    @GET(Api.moment.GET_DISCO_CONTENT_DETAIL)
    Flowable<HttpResult<MomentListModel>> getDiscoContentDetail(@Path("id") String id);

    @POST(Api.moment.MOMENT_CIRCLE_TRANSFER_MASTER)
    Flowable<HttpResult<Boolean>> masterTransferCircle(@Path("circleId") String circleId, @Path("userId") String userId);

    @POST(Api.moment.MOMENT_CIRCLE_JOIN)
    Flowable<HttpResult<Boolean>> momentCircleJoin(@Path("circleId") String circleId);

    @DELETE(Api.moment.MOMENT_CIRCLE_QUIT)
    Flowable<HttpResult<Boolean>> momentCircleQuit(@Path("circleId") String circleId);

    @DELETE(Api.moment.DELETE_CONTENT)
    Flowable<HttpResult> deleteContent(@Path("id") String id);

    @PUT(Api.app.PERSON_INFO_SET_FRIENDREMARK)
    Flowable<HttpResult> setFriendRemark(@Body RequestBody body);

    @POST(Api.moment.CIRCLE_MASTER_APPLY)
    Flowable<HttpResult> circleMasterApply(@Path("circleId") String circleId);

    @GET(Api.moment.GET_CONTENT_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getContentList(@QueryMap HashMap<String, Object> params);


    @GET(Api.moment.GET_ADDRESS_CATEGORY_LIST)
    Flowable<HttpResult<List<ContentCategoryModel>>> queryCategory(@QueryMap HashMap<String, Object> params);

    ///app/circle/apply
    @POST(Api.moment.CIRCLE_APPLY)
    Flowable<HttpResult<Boolean>> applyCircle(@Body RequestBody body);

    @PUT(Api.moment.MOMENT_CIRCLE_DETAIL)
    Flowable<HttpResult<Boolean>> editCircle(@Body RequestBody body, @Path("id") String id);

    @POST(Api.moment.PUBLISH_CONTENT)
    Flowable<HttpResult<ContentModel>> publishContent(@Body RequestBody body);

    @GET(Api.app.GET_CONTENT_CATEGORY_LIST)
    Flowable<HttpResult<List<ContentCategoryModel>>> getContentCategory();

    @POST(Api.app.ADD_BLACK)
    Flowable<HttpResult> addBlack(@Path("userId") String userId);

    @GET(Api.app.GET_USER_DETAIL)
    Flowable<HttpResult<ContentUserInfoModel>> getContentUserDetail(@Path("userId") String userId);

    @GET(Api.app.USER_INFO_DETAIL)
    Flowable<HttpResult<UserInfo>> getUserDetail(@Path("id") String userId);

    @PUT(Api.app.PERSON_INFO_SET_BACKGROUND)
    Flowable<HttpResult> setBackGround(@Query("blackgroundImg") String blackgroundImg);

    @GET(Api.app.GET_USER_CONTENT_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getUserContentList(@Query("contentType") int contentType, @Query("userId") String userId, @Query("current") int current);

    @GET(Api.app.GET_USER_CONTENT_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getUserContentList(@Query("userId") String userId, @Query("current") int current);

    @GET(Api.app.FOLLOW_USER)
    Flowable<HttpResult> followUser(@Path("userId") String userId);

    @GET(Api.app.CANCEL_FOLLOW_USER)
    Flowable<HttpResult> cancelFollowUser(@Path("userId") String userId);

    @GET(Api.app.GET_FOLLOW_USER_LIST)
    Flowable<HttpResult<ListResult<FriendModel>>> getFollowUserList(@Query("userId") String userId, @Query("current") int current, @Query("size") int size, @Query("keyword") String keyword);

    @GET(Api.app.MSG_CARE)
    Flowable<HttpResult> getNewAttentions(@Query("current") int current, @Query("size") int size);

    @GET(Api.app.GET_USER_FRIEND)
    Flowable<HttpResult<ListResult<FriendModel>>> getFriendUserList(@Query("userId") String userId, @Query("nickName") String nickName, @Query("current") int current, @Query("size") int size);

    @GET(Api.app.GET_FANS_USER_LIST)
    Flowable<HttpResult<ListResult<FriendModel>>> getUserFansList(@Query("userId") String userId, @Query("current") int current, @Query("size") int size, @Query("keyword") String keyword);

    @GET(Api.moment.MOMENT_TOPIC_LIST)
    Flowable<HttpResult<ListResult<TopicModel>>> getTopicList(@Query("title") String title, @Query("current") int current, @Query("size") int size);


    @GET(Api.moment.MOMENT_CIRCLE_JOINED_LIST)
    Flowable<HttpResult<ListResult<CircleModel>>> getCircleJoinedList(@Query("title") String title, @Query("current") int current, @Query("size") int size);


    @GET(Api.moment.MOMENT_CIRCLE_MANAGE_LIST)
    Flowable<HttpResult<ListResult<CircleManageModel>>> getCircleManageList(@Query("title") String title, @Query("current") int current, @Query("size") int size);


    @GET(Api.moment.MOMENT_CIRCLE_LIST)
    Flowable<HttpResult<ListResult<CircleModel>>> getCircleList(@Query("title") String title, @Query("current") int current, @Query("size") int size);


    @GET(Api.moment.MOMENT_TOPIC_HOT_LIST)
    Flowable<HttpResult<ListResult<TopicModel>>> getHotTopicList(@Query("title") String title, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_TOPIC_RECOMMEND_LIST)
    Flowable<HttpResult<List<TopicModel>>> getRecommendTopicList();

    @GET(Api.moment.MOMENT_CIRCLE_HOT_LIST)
    Flowable<HttpResult<ListResult<CircleModel>>> getHotCircleList(@Query("title") String title, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_HOME_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMomentPageVideoList(@Query("title") String title, @Query("type") int type, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_HOME_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMomentPageList(@Query("title") String title, @Query("type") int type, @Query("current") int current, @Query("size") int size
            , @Query("areaName") String areaName, @Query("systemType") int systemType);

    @GET(Api.moment.MOMENT_HOME_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMomentPageCityList(@Query("title") String title, @Query("type") int type, @Query("current") int current, @Query("size") int size,
                                                                            @Query("distance") String distance, @Query("latitude") String latitude, @Query("longitude") String longitude
            , @Query("areaName") String areaName, @Query("systemType") int systemType);

    @GET(Api.moment.DISCO_BEST_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getDiscoBestList(@Query("keyword") String keyword,@Query("city") String city, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_USER_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMomentUserList(@Query("keyword") String keyword, @Query("userId") String userId, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_USER_COLLECT_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getMyFavMomentList(@Query("title") String title, @Query("userId") String userId, @Query("current") int current, @Query("size") int size);

    @Deprecated
    @GET(Api.moment.MOMENT_USER_LIKE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getUserLikeList(@Query("title") String title, @Query("userId") String userId, @Query("current") int current, @Query("size") int size);


    @GET(Api.moment.MOMENT_CIRCLE_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getCirclePageList(@Query("title") String title, @Query("circleId") String circleId, @Query("type") int type, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_CIRCLE_MEMBER_LIST)
    Flowable<HttpResult<ListResult<MomentCircleMemberModel>>> getCircleMemberList(@Query("title") String title, @Query("circleId") String circleId, @Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_CIRCLE_APPLY_HISTORY)
    Flowable<HttpResult<ListResult<CircleModel>>> getCircleApplyList(@Query("current") int current, @Query("size") int size);


    @GET(Api.moment.CIRCLE_MASTER_APPLY_LIST)
    Flowable<HttpResult<ListResult<CircleModel>>> getMasterCircleApplyList(@Query("current") int current, @Query("size") int size);

    @GET(Api.moment.MOMENT_TOPIC_PAGE_LIST)
    Flowable<HttpResult<ListResult<MomentListModel>>> getTopicPageList(@Query("title") String title, @Query("topicId") String topicId, @Query("type") int type, @Query("current") int current, @Query("size") int size);

    @POST(Api.app.GET_FAVORITE_CONTENT)
    Flowable<HttpResult<ListResult<ContentModel>>> getFavoriteContent(@Query("current") int current);

    @POST(Api.moment.UPDATE_PRIVACY_TYPE)
    Flowable<HttpResult> updatePrivacyType(@Body RequestBody body);

    @GET(Api.moment.MOMENT_CIRCLE_DETAIL)
    Flowable<HttpResult<CircleModel>> getCircleDetail(@Path("id") String circleId);

    @GET(Api.moment.MOMENT_CIRCLE_APPLY_EDIT_DETAIL)
    Flowable<HttpResult<CircleModel>> getCircleApplyDetail(@Path("id") String circleId);

    @GET(Api.moment.MOMENT_CIRCLE_STATUS)
    Flowable<HttpResult<MomentCircleStatus>> getCircleStatus(@Path("circleId") String circleId);

    @GET(Api.moment.MOMENT_TOPIC_DETAIL)
    Flowable<HttpResult<TopicModel>> getTopicDetail(@Path("id") String topicId);

    @POST(Api.moment.ADD_CIRCLE_VIEWCOUNT)
    Flowable<HttpResult> addCircleViewCount(@Path("id") String id);

    @POST(Api.moment.ADD_TOPIC_VIEWCOUNT)
    Flowable<HttpResult> addTopicViewCount(@Path("id") String id);
}
