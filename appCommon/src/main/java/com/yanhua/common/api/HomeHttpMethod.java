package com.yanhua.common.api;


import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.InviteCreateTypeModel;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.model.InviteMemberNum;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteSetInfoModel;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishResultModel;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.model.UserModel;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;

public class HomeHttpMethod extends BaseRtHttpMethod {

    HomeService api;

    public HomeHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), HomeService.class);
    }

    private static class HomeHttpMethodHolder {
        private static final HomeHttpMethod INSTANCE = new HomeHttpMethod();
    }

    //获取单例
    public static HomeHttpMethod getInstance() {
        return HomeHttpMethodHolder.INSTANCE;
    }

    public SimpleSubscriber homeSearch(SimpleSubscriber<HttpResult<ListResult<HomeSearchResultModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.homeSearch(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber userSearch(SimpleSubscriber<HttpResult<ListResult<UserModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.userSearch(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getHotTopicList(SimpleSubscriber<HttpResult<ListResult<TopicModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getHotTopicList(params), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getHotCircleList(SimpleSubscriber<HttpResult<ListResult<CircleModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getHotCircleList(params), subscriber);
        return subscriber;
    }

    public SimpleSubscriber deleteNewsContent(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.deleteNewsContent(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 约伴浏览记录
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerHistoryList(SimpleSubscriber<HttpResult<ListResult<InviteModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.meetPartnerHistoryList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerTypeList(SimpleSubscriber<HttpResult<List<InviteTypeModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.meetPartnerTypeList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerTemplateList(SimpleSubscriber<HttpResult<List<InviteTemplateModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.meetPartnerTemplateList(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 发起邀约
     *
     * @param subscriber
     * @param body
     * @return
     */
    public SimpleSubscriber meetPartnerInviteAdd(SimpleSubscriber<HttpResult> subscriber, RequestBody body) {
        tocompose(api.meetPartnerInviteAdd(body), subscriber);
        return subscriber;
    }

    /**
     * 删除邀约
     *
     * @param subscriber
     * @param id
     * @return
     */
    public SimpleSubscriber meetPartnerInviteDelete(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(api.meetPartnerInviteDelete(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerInviteSetInfo(SimpleSubscriber<HttpResult<InviteSetInfoModel>> subscriber, String id) {
        tocompose(api.meetPartnerInviteSetInfo(id), subscriber);
        return subscriber;
    }


    /**
     * 邀约详情
     *
     * @param subscriber
     * @param id
     * @return
     */
    public SimpleSubscriber meetPartnerInviteDetail(SimpleSubscriber<HttpResult<InviteModel>> subscriber, String id) {
        tocompose(api.meetPartnerInviteDetail(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerUpdateStatus(SimpleSubscriber<HttpResult> subscriber, String id) {
        tocompose(api.meetPartnerUpdateStatus(id), subscriber);
        return subscriber;
    }

    /**
     * 查询邀约 分页（首页进入的列表）
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerInvitePage(SimpleSubscriber<HttpResult<ListResult<InviteModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.meetPartnerInvitePage(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 查询我的邀约 分页（我的进入的列表）
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerMyInvitePage(SimpleSubscriber<HttpResult<ListResult<InviteModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.meetPartnerMyInvitePage(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 修改邀约
     *
     * @param subscriber
     * @return
     */
    public SimpleSubscriber meetPartnerInviteInfoUpdate(SimpleSubscriber<HttpResult> subscriber, RequestBody body) {
        tocompose(api.meetPartnerInviteInfoUpdate(body), subscriber);
        return subscriber;
    }

    /**
     * 申请加入邀约
     *
     * @param subscriber
     */
    public SimpleSubscriber meetPartnerInviteSignIn(SimpleSubscriber<HttpResult> subscriber,
                                                    HashMap<String, Object> params) {
        tocompose(api.meetPartnerInviteSignIn(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 同意申请加入
     *
     * @param subscriber
     * @param id
     * @return
     */
    public SimpleSubscriber meetPartnerInviteAgreeSignIn(SimpleSubscriber<HttpResult> subscriber,
                                                         String id) {
        tocompose(api.meetPartnerInviteAgreeSignIn(id), subscriber);
        return subscriber;
    }

    /**
     * 取消报名
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerInviteSignOut(SimpleSubscriber<HttpResult> subscriber,
                                                     HashMap<String, Object> params) {
        tocompose(api.meetPartnerInviteSignOut(convertToJson(params)), subscriber);
        return subscriber;
    }

    /**
     * 移除已通过的报名
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerInviteRemoveSigned(SimpleSubscriber<HttpResult> subscriber,
                                                          HashMap<String, Object> params) {
        tocompose(api.meetPartnerInviteRemoveSigned(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerStopReason(SimpleSubscriber<HttpResult> subscriber,
                                                  HashMap<String, Object> params) {
        tocompose(api.meetPartnerStopReason(convertToJson(params)), subscriber);
        return subscriber;
    }


    /**
     * 获取已加入和未加入的人数
     *
     * @param subscriber
     * @param id
     * @return
     */
    public SimpleSubscriber meetPartnerInviteNum(SimpleSubscriber<HttpResult<InviteMemberNum>> subscriber,
                                                 String id) {
        tocompose(api.meetPartnerInviteNum(id), subscriber);
        return subscriber;
    }

    /**
     * 获取已加入和未加入的人
     *
     * @param subscriber
     * @param params
     * @return
     */
    public SimpleSubscriber meetPartnerInviteMembers(SimpleSubscriber<HttpResult<ListResult<InviteMemberModel>>> subscriber,
                                                     HashMap<String, Object> params) {
        tocompose(api.meetPartnerInviteMembers(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber meetPartnerCreateType(SimpleSubscriber<HttpResult<InviteCreateTypeModel>> subscriber) {
        tocompose(api.meetPartnerCreateType(), subscriber);
        return subscriber;
    }

    //-------start------爆料相关-------------------------------
    public SimpleSubscriber getNewsDetail(SimpleSubscriber<HttpResult<NewsDetailModel>> subscriber, String id) {
        tocompose(api.getNewsDetail(id), subscriber);
        return subscriber;
    }

    public SimpleSubscriber homePageBreakNewsList(SimpleSubscriber<HttpResult<ListResult<BreakNewsRecommendModel>>> subscriber, int current, int size) {
        tocompose(api.homePageBreakNewsList(current, size), subscriber);
        return subscriber;
    }

    /**
     * 最新爆料滚动活动条栏---以及历史
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber getRecentActiveList(SimpleSubscriber<HttpResult<ListResult<ActivityModel>>> subscriber, int type, int queryType,
                                                int current, int size,
                                                String city, String contentCategoryId) {
        tocompose(api.getRecentActiveList(type, queryType,
                current, size,
                city, contentCategoryId), subscriber);
        return subscriber;
    }

    /**
     * 最新爆料 推荐置顶
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber getNewsRecommendList(SimpleSubscriber<HttpResult<List<BreakNewsRecommendModel>>> subscriber, int type, int queryType,
                                                 int current, int size,
                                                 String city, String contentCategoryId) {
        tocompose(api.getNewsRecommendList(type, queryType,
                current, size,
                city, contentCategoryId), subscriber);
        return subscriber;
    }

    /**
     * //获取爆料分类和攻略分类
     *
     * @param type 类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber getNewsTypeList(SimpleSubscriber<HttpResult<List<NewsTypeModel>>> subscriber, int type, int publish) {
        tocompose(api.getNewsTypeList(type, publish), subscriber);
        return subscriber;
    }

    /**
     * 这个获取城市不需要分页
     *
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber recentActiveCityList(SimpleSubscriber<HttpResult<List<String>>> subscriber, String contentCategoryId, int type, int queryType) {
        tocompose(api.recentActiveCityList(contentCategoryId, type, queryType), subscriber);
        return subscriber;
    }

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param city              城市
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber cityContentList(SimpleSubscriber<HttpResult<ListResult<BreakNewsListModel>>> subscriber,
                                            int current, int size, String city, String keyword, String contentCategoryId, int type) {
        tocompose(api.cityContentList(current, size, city, keyword, contentCategoryId, type), subscriber);
        return subscriber;
    }

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber strategyList(SimpleSubscriber<HttpResult<ListResult<StrategyModel>>> subscriber,
                                         int current, int size, String keyword, String contentCategoryId, int type) {
        tocompose(api.strategyList(current, size, keyword, contentCategoryId, type), subscriber);
        return subscriber;
    }

    /**
     * @param attributeType     attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
     * @param contentCategoryId 这个不用管
     * @param type              type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public SimpleSubscriber hotSortList(SimpleSubscriber<HttpResult<List<StrategyHotSortModel>>> subscriber,
                                        int attributeType, String contentCategoryId, int type) {
//        tocompose(api.hotSortList(attributeType, contentCategoryId, type), subscriber);
        tocompose(api.hotSortList(attributeType, type), subscriber);
        return subscriber;
    }

    public SimpleSubscriber publishNewsDetail(SimpleSubscriber<HttpResult<PublishResultModel>> subscriber, RequestBody body) {
        tocompose(api.publishNewsDetail(body), subscriber);
        return subscriber;
    }


    //爆料或攻略流量历史
    public SimpleSubscriber newsViewHistoryList(SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>> subscriber,
                                                HashMap<String, Object> params) {
        tocompose(api.newsViewHistoryList(convertToJson(params)), subscriber);
        return subscriber;
    }

    //我的动态爆料或攻略
    public SimpleSubscriber newsDynamicList(SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>> subscriber,
                                            HashMap<String, Object> params) {
        tocompose(api.newsDynamicList(convertToJson(params)), subscriber);
        return subscriber;
    }

    //我的收藏爆料或攻略
    public SimpleSubscriber newsCollectList(SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>> subscriber,
                                            HashMap<String, Object> params) {
        tocompose(api.newsCollectList(convertToJson(params)), subscriber);
        return subscriber;
    }


    //--------end-----爆料相关-------------------------------
    public SimpleSubscriber uploadUserLocation(SimpleSubscriber<HttpResult> subscriber,
                                               HashMap<String, Object> params) {
        tocompose(api.uploadUserLocation(convertToJson(params)), subscriber);
        return subscriber;
    }


    /****************************酒吧模块start*************************/
    public SimpleSubscriber getBarList(SimpleSubscriber<HttpResult<ListResult<BarModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getBarList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarDetail(SimpleSubscriber<HttpResult<BarModel>> subscriber, String barId, HashMap<String, Object> params) {
        tocompose(api.getBarDetail(barId, convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber doLoveOrNotBar(SimpleSubscriber<HttpResult> subscriber, String barId) {
        tocompose(api.doLoveOrNotBar(barId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarQAList(SimpleSubscriber<HttpResult<ListResult<BarQAModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getBarQAList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarQADetail(SimpleSubscriber<HttpResult<BarQAModel>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getBarQADetail(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getBarAnswerDetail(SimpleSubscriber<HttpResult<MyQAModel>> subscriber, String askId) {
        tocompose(api.getBarAnswerDetail(askId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber postMyQA(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.postMyQA(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber updateMyQuestion(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.updateMyQuestion(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber deleteMyQuestion(SimpleSubscriber<HttpResult> subscriber, String askedId) {
        tocompose(api.deleteMyQuestion(askedId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber deleteMyAnswer(SimpleSubscriber<HttpResult> subscriber, String askedId) {
        tocompose(api.deleteMyAnswer(askedId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMyBarQAList(SimpleSubscriber<HttpResult<ListResult<MyQAModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getMyBarQAList(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber postMyComment(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.postMyComment(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getMyCollectBarList(SimpleSubscriber<HttpResult<ListResult<BarModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getMyColloectBarList(convertToJson(params)), subscriber);
        return subscriber;
    }

    /****************************酒吧模块end*************************/
}