package com.yanhua.common.api;


import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.MineCommentModel;
import com.yanhua.common.model.MomentListModel;

import java.util.HashMap;

public class MineHttpMethod extends BaseRtHttpMethod {

    MineService api;

    public MineHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(YXConfig.getBaseUrl(), MineService.class);
    }

    private static class MineHttpMethodHolder {
        private static final MineHttpMethod INSTANCE = new MineHttpMethod();
    }

    //获取单例
    public static MineHttpMethod getInstance() {
        return MineHttpMethodHolder.INSTANCE;
    }

    public SimpleSubscriber commentListQuery(SimpleSubscriber<HttpResult<ListResult<MineCommentModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.commentListQuery(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getUserLikeList(SimpleSubscriber<HttpResult<ListResult<MomentListModel>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getUserLikeList(convertToJson(params)), subscriber);
        return subscriber;
    }
}