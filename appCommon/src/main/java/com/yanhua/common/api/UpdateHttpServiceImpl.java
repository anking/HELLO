package com.yanhua.common.api;

import androidx.annotation.NonNull;

import com.xuexiang.xupdate.proxy.IUpdateHttpService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * @author Administrator
 */
public class UpdateHttpServiceImpl implements IUpdateHttpService {

    @Override
    public void asyncGet(@NonNull String url, @NonNull Map<String, Object> params, @NonNull Callback callBack) {
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()//默认就是GET请求，可以不写
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callBack.onError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                callBack.onSuccess(response.toString());
            }
        });
    }

    @Override
    public void asyncPost(@NonNull String url, @NonNull Map<String, Object> params, @NonNull Callback callBack) {

    }

    @Override
    public void download(@NonNull String url, @NonNull String path, @NonNull String fileName, @NonNull DownloadCallback callback) {
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .tag(url)
                .get()
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onError(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody responseBody = response.body();
                long totalLength = responseBody.contentLength();//下载文件的总长度
                float alreadyDownLength = 0;
                InputStream inp = responseBody.byteStream();
                File folder = new File(path);
                folder.mkdirs();
                File file = new File(folder, fileName);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                callback.onStart();
                try {
                    byte[] bytes = new byte[1024];
                    int len = 0;
                    while ((len = inp.read(bytes)) != -1) {
                        alreadyDownLength += len;
                        fileOutputStream.write(bytes, 0, len);
                        callback.onProgress(alreadyDownLength / totalLength, totalLength);
                    }
                    if (alreadyDownLength >= totalLength) {
                        callback.onSuccess(file);
                    }
                } catch (Exception e) {
                    callback.onError(e);
                } finally {
                    fileOutputStream.close();
                    inp.close();
                }
            }
        });
    }

    @Override
    public void cancelDownload(@NonNull String url) {

    }
}
