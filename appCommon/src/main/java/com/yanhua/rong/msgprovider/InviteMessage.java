package com.yanhua.rong.msgprovider;

import android.os.Parcel;

import com.yanhua.base.config.YXConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.rlog.RLog;
import io.rong.imlib.DestructionTag;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(
        value = YXConfig.imRongMsg.invite,
        flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED
)
@DestructionTag(destructionFlag = DestructionTag.DestructionType.FLAG_COUNT_DOWN_WHEN_CLICK)
public class InviteMessage extends MessageContent implements Serializable {
    private static final String TAG = "InviteMessage";

    private String id;//邀约ID
    private String coverUrl;//封面
    private String title;// 标题
    private String address;// 地址
    private int coverType;// 封面类型 1：图片 2：视频
    private int type;//所属类型--- 1

    private String authorHeadImg;// 发布者头像
    private String authorNickname;// 发布者昵称==

    public InviteMessage() {
    }

    protected InviteMessage(Parcel in) {
        setExtra(ParcelUtils.readFromParcel(in));

        setId(ParcelUtils.readFromParcel(in));
        setCoverUrl(ParcelUtils.readFromParcel(in));
        setTitle(ParcelUtils.readFromParcel(in));
        setAddress(ParcelUtils.readFromParcel(in));
        setCoverType(ParcelUtils.readIntFromParcel(in));
        setType(ParcelUtils.readIntFromParcel(in));
        setAuthorHeadImg(ParcelUtils.readFromParcel(in));
        setAuthorNickname(ParcelUtils.readFromParcel(in));
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // 对消息属性进行序列化，将类的数据写入外部提供的 Parcel 中
        ParcelUtils.writeToParcel(dest, getExtra());

        //按顺序一一对应
        ParcelUtils.writeToParcel(dest, getId());
        ParcelUtils.writeToParcel(dest, getCoverUrl());
        ParcelUtils.writeToParcel(dest, getTitle());
        ParcelUtils.writeToParcel(dest, getAddress());
        ParcelUtils.writeToParcel(dest, getCoverType());
        ParcelUtils.writeToParcel(dest, getType());
        ParcelUtils.writeToParcel(dest, getAuthorHeadImg());
        ParcelUtils.writeToParcel(dest, getAuthorNickname());
    }

    public static final Creator<InviteMessage> CREATOR = new Creator<InviteMessage>() {
        @Override
        public InviteMessage createFromParcel(Parcel in) {
            return new InviteMessage(in);
        }

        @Override
        public InviteMessage[] newArray(int size) {
            return new InviteMessage[size];
        }
    };

    public InviteMessage(byte[] data) {
        if (data == null) {
            RLog.e(TAG, "data is null ");
            return;
        }
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        if (jsonStr == null) {
            RLog.e(TAG, "jsonStr is null ");
            return;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (jsonObj.has("user")) {
                setUserInfo(parseJsonToUserInfo(jsonObj.getJSONObject("user")));
            }
            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (jsonObj.has("mentionedInfo")) {
                setMentionedInfo(parseJsonToMentionInfo(jsonObj.getJSONObject("mentionedInfo")));
            }

            // 自定义消息, 定义的字段
            if (jsonObj.has("id")) {
                setId(jsonObj.optString("id"));
            }

            if (jsonObj.has("coverUrl")) {
                setCoverUrl(jsonObj.optString("coverUrl"));
            }
            if (jsonObj.has("title")) {
                setTitle(jsonObj.optString("title"));
            }
            if (jsonObj.has("address")) {
                setAddress(jsonObj.optString("address"));
            }
            if (jsonObj.has("coverType")) {
                setCoverType(jsonObj.optInt("coverType"));
            }

            if (jsonObj.has("type")) {
                setType(jsonObj.optInt("type"));
            }
            if (jsonObj.has("authorHeadImg")) {
                setAuthorHeadImg(jsonObj.optString("authorHeadImg"));
            }
            if (jsonObj.has("authorNickname")) {
                setAuthorNickname(jsonObj.optString("authorNickname"));
            }

        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (getJSONUserInfo() != null) {
                jsonObj.putOpt("user", getJSONUserInfo());
            }

            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (getJsonMentionInfo() != null) {
                jsonObj.putOpt("mentionedInfo", getJsonMentionInfo());
            }

            // 自定义消息, 定义的字段.
            jsonObj.put("id", getId());

            jsonObj.put("coverUrl", getCoverUrl());
            jsonObj.put("title", getTitle());
            jsonObj.put("address", getAddress());
            jsonObj.put("coverType", getCoverType());
            jsonObj.put("type", getType());
            jsonObj.put("authorHeadImg", getAuthorHeadImg());
            jsonObj.put("authorNickname", getAuthorNickname());
        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCoverType() {
        return coverType;
    }

    public void setCoverType(int coverType) {
        this.coverType = coverType;
    }

    public String getAuthorHeadImg() {
        return authorHeadImg;
    }

    public void setAuthorHeadImg(String authorHeadImg) {
        this.authorHeadImg = authorHeadImg;
    }

    public String getAuthorNickname() {
        return authorNickname;
    }

    public void setAuthorNickname(String authorNickname) {
        this.authorNickname = authorNickname;
    }
}
