package com.yanhua.rong.msgprovider;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.yanhua.common.R;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.GlideRoundTransform;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;

import java.util.List;

import io.rong.imkit.conversation.messgelist.provider.BaseMessageItemProvider;
import io.rong.imkit.model.UiMessage;
import io.rong.imkit.widget.adapter.IViewProviderListener;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;

public class UserPageMessageProvider extends BaseMessageItemProvider<UserPageMessage> {

    public UserPageMessageProvider() {
        mConfig.showReadState = true;
        mConfig.showContentBubble = false;
    }

    /**
     * 创建 ViewHolder
     *
     * @param parent   父 ViewGroup
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    protected ViewHolder onCreateMessageContentViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_userpage_message_item, parent, false);

        return new ViewHolder(view.getContext(), view);
    }

    private Integer minSize = null;
    private Integer maxSize = null;
    private static int THUMB_COMPRESSED_SIZE = 240;
    private static int THUMB_COMPRESSED_MIN_SIZE = 100;

    /**
     * 设置消息视图里各 view 的值
     *
     * @param holder       ViewHolder
     * @param parentHolder 父布局的 ViewHolder
     * @param message      此展示模板对应的消息
     * @param uiMessage    {@link UiMessage}
     * @param position     消息位置
     * @param list         列表
     * @param listener     ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     */
    @Override
    protected void bindMessageContentViewHolder(final ViewHolder holder, ViewHolder parentHolder, UserPageMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        //设置item的容器的宽高
        final LinearLayout itemContainer = holder.getView(R.id.itemContainer);
        int sw = DisplayUtils.getScreenWidth(holder.getContext());

        ViewGroup.LayoutParams containerLayoutParams = itemContainer.getLayoutParams();
        containerLayoutParams.width = sw / 2;
        itemContainer.setLayoutParams(containerLayoutParams);

        ImageView ivBg = holder.getView(R.id.ivBg);
        CircleImageView ivHead = holder.getView(R.id.ivHead);
        TextView tvNickname = holder.getView(R.id.tvNickname);
        TextView tvPostNum = holder.getView(R.id.tvPostNum);
        TextView tvFansNum = holder.getView(R.id.tvFansNum);

        tvNickname.setText(message.getNickName());

        tvPostNum.setText("动态(" + YHStringUtils.quantityFormat(message.getPostNum()) + ")");
        tvFansNum.setText("粉丝(" + YHStringUtils.quantityFormat(message.getFansNum()) + ")");

        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(holder.getContext(),
                8, uiMessage.getMessage().getMessageDirection() == Message.MessageDirection.SEND ?
                GlideRoundTransform.CornerType.TOP_LEFT : GlideRoundTransform.CornerType.TOP_RIGHT);
        if (TextUtils.isEmpty(message.getBgImg())) {
            Glide.with(holder.getContext())
                    .load(R.drawable.place_holder)
                    .apply(new RequestOptions()
                            .fitCenter()
                            .transform(glideRoundTransform)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(ivBg);
        } else {
            Glide.with(holder.getContext())
                    .load(message.getBgImg())
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transform(glideRoundTransform))
                    .placeholder(R.drawable.place_holder)
                    .into(ivBg);
        }
        if (TextUtils.isEmpty(message.getHeadImg())) {
            ImageLoaderUtil.loadImg(ivHead, R.drawable.place_holder);
        } else {
            ImageLoaderUtil.loadImg(ivHead, message.getHeadImg());
        }
    }

    /**
     * @param holder    ViewHolder
     * @param message   自定义消息
     * @param uiMessage {@link UiMessage}
     * @param position  位置
     * @param list      列表数据
     * @param listener  ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     * @return 点击事件是否被消费
     */
    @Override
    protected boolean onItemClick(ViewHolder holder, UserPageMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                .withString("userId", message.getUserId())
                .navigation();
        return true;
    }

    /**
     * 根据消息内容，判断是否为本模板需要展示的消息类型
     *
     * @param messageContent 消息内容
     * @return 本模板是否处理。
     */
    @Override
    protected boolean isMessageViewType(MessageContent messageContent) {
        return messageContent instanceof UserPageMessage;
    }

    /**
     * 在会话列表页某条会话最后一条消息为该类型消息时，会话里需要展示的内容。
     * 比如: 图片消息在会话里需要展示为"图片"，那返回对应的字符串资源即可。
     *
     * @param context 上下文
     * @param message 消息内容
     * @return 会话里需要展示的字符串资源
     */
    @Override
    public Spannable getSummarySpannable(Context context, UserPageMessage message) {
        return new SpannableString("[个人主页]");
    }
}
