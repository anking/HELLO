package com.yanhua.rong.msgprovider;

import android.os.Parcel;

import com.yanhua.base.config.YXConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.rlog.RLog;
import io.rong.imlib.DestructionTag;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(
        value = YXConfig.imRongMsg.momentVideo,
        flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED
)
@DestructionTag(destructionFlag = DestructionTag.DestructionType.FLAG_COUNT_DOWN_WHEN_CLICK)
public class MomentVideoMessage extends MessageContent implements Serializable {
    private static final String TAG = "MomentContentMessage";

    private String id;//内容ID

    private String coverUrl;//封面
    private String width;
    private String height;
    private String content;// 帖子标题
    private String authorHeadImg;// 发布者头像
    private String authorNickname;// 发布者昵称==
    private int type;//所属类型

    public MomentVideoMessage() {
    }

    protected MomentVideoMessage(Parcel in) {
        setExtra(ParcelUtils.readFromParcel(in));

        setId(ParcelUtils.readFromParcel(in));
        setCoverUrl(ParcelUtils.readFromParcel(in));
        setWidth(ParcelUtils.readFromParcel(in));
        setHeight(ParcelUtils.readFromParcel(in));
        setContent(ParcelUtils.readFromParcel(in));
        setAuthorHeadImg(ParcelUtils.readFromParcel(in));
        setAuthorNickname(ParcelUtils.readFromParcel(in));
        setType(ParcelUtils.readIntFromParcel(in));
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // 对消息属性进行序列化，将类的数据写入外部提供的 Parcel 中
        ParcelUtils.writeToParcel(dest, getExtra());

        //按顺序一一对应
        ParcelUtils.writeToParcel(dest, getId());
        ParcelUtils.writeToParcel(dest, getCoverUrl());
        ParcelUtils.writeToParcel(dest, getWidth());
        ParcelUtils.writeToParcel(dest, getHeight());
        ParcelUtils.writeToParcel(dest, getContent());
        ParcelUtils.writeToParcel(dest, getAuthorHeadImg());
        ParcelUtils.writeToParcel(dest, getAuthorNickname());
        ParcelUtils.writeToParcel(dest, getType());
    }

    public static final Creator<MomentVideoMessage> CREATOR = new Creator<MomentVideoMessage>() {
        @Override
        public MomentVideoMessage createFromParcel(Parcel in) {
            return new MomentVideoMessage(in);
        }

        @Override
        public MomentVideoMessage[] newArray(int size) {
            return new MomentVideoMessage[size];
        }
    };

    public MomentVideoMessage(byte[] data) {
        if (data == null) {
            RLog.e(TAG, "data is null ");
            return;
        }
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        if (jsonStr == null) {
            RLog.e(TAG, "jsonStr is null ");
            return;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (jsonObj.has("user")) {
                setUserInfo(parseJsonToUserInfo(jsonObj.getJSONObject("user")));
            }
            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (jsonObj.has("mentionedInfo")) {
                setMentionedInfo(parseJsonToMentionInfo(jsonObj.getJSONObject("mentionedInfo")));
            }


            // 自定义消息, 定义的字段
            if (jsonObj.has("id")) {
                setId(jsonObj.optString("id"));
            }


            if (jsonObj.has("coverUrl")) {
                setCoverUrl(jsonObj.optString("coverUrl"));
            }

            if (jsonObj.has("width")) {
                setWidth(jsonObj.optString("width"));
            }


            if (jsonObj.has("height")) {
                setHeight(jsonObj.optString("height"));
            }
            if (jsonObj.has("content")) {
                setContent(jsonObj.optString("content"));
            }
            if (jsonObj.has("authorHeadImg")) {
                setAuthorHeadImg(jsonObj.optString("authorHeadImg"));
            }
            if (jsonObj.has("authorNickname")) {
                setAuthorNickname(jsonObj.optString("authorNickname"));
            }

            if (jsonObj.has("type")) {
                setType(jsonObj.optInt("type"));
            }
        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (getJSONUserInfo() != null) {
                jsonObj.putOpt("user", getJSONUserInfo());
            }

            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (getJsonMentionInfo() != null) {
                jsonObj.putOpt("mentionedInfo", getJsonMentionInfo());
            }

            // 自定义消息, 定义的字段.
            jsonObj.put("id", getId());

            jsonObj.put("coverUrl", getCoverUrl());
            jsonObj.put("width", getWidth());
            jsonObj.put("height", getHeight());

            jsonObj.put("content", getContent());
            jsonObj.put("authorHeadImg", getAuthorHeadImg());
            jsonObj.put("authorNickname", getAuthorNickname());
            jsonObj.put("type", getType());
        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthorHeadImg() {
        return authorHeadImg;
    }

    public void setAuthorHeadImg(String authorHeadImg) {
        this.authorHeadImg = authorHeadImg;
    }

    public String getAuthorNickname() {
        return authorNickname;
    }

    public void setAuthorNickname(String authorNickname) {
        this.authorNickname = authorNickname;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
