package com.yanhua.rong.msgprovider;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.gridlayout.widget.GridLayout;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.CircleImageView;

import java.util.List;

import io.rong.imkit.conversation.messgelist.provider.BaseMessageItemProvider;
import io.rong.imkit.model.UiMessage;
import io.rong.imkit.widget.adapter.IViewProviderListener;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imlib.model.MessageContent;

public class MomentContentMessageProvider extends BaseMessageItemProvider<MomentContentMessage> {

    public MomentContentMessageProvider() {
        mConfig.showReadState = true;
        mConfig.showContentBubble = false;
    }

    /**
     * 创建 ViewHolder
     *
     * @param parent   父 ViewGroup
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    protected ViewHolder onCreateMessageContentViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_moment_message_item, parent, false);

        return new ViewHolder(view.getContext(), view);
    }

    private void showGridImage(Context mContext, GridLayout gridImage, TextView tvIndex, List<String> imgUrls, int screenWidth) {
        ViewGroup.LayoutParams imageLayoutParams = gridImage.getLayoutParams();
        gridImage.removeAllViews();

        int imgSize = imgUrls.size();

        //默认初始值--
        //展示1:1，大致为宽的一半。计算方式为screenWidth-左右边距24
        int showWidth = (screenWidth - DisplayUtils.dip2px(mContext, 24));
        int showHeight = showWidth / 2;

        if (imgSize >= 3) {
            int leftShowCount = imgSize - 3;
            if (leftShowCount > 0) {
                tvIndex.setVisibility(View.VISIBLE);
                tvIndex.setText(leftShowCount + "+");
            } else {
                tvIndex.setVisibility(View.GONE);
                tvIndex.setText("0");
            }
            ////展示2:1，大致为宽的一半。计算方式为screenWidth-左右边距24,第一张图比较大
            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showWidth / 3 * 2;
        } else {
            tvIndex.setVisibility(View.GONE);
            tvIndex.setText("0");

            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showHeight;
        }

        gridImage.setLayoutParams(imageLayoutParams);

        //根据UI的逻辑，一张和两张展示都只有一行
        //1--（0,0），（0,1）
        //2--（0,0），（0,1）
        //3--（0,0），（0,1），（1,1）

        //使用Spec定义子控件的位置和比重 设置行列下标， 所占行列  ，比重
        // 设置行列下标， 所占行列  ，比重
        // 对应： layout_row  , layout_rowSpan , layout_rowWeight
        //GridLayout.spec(0, 2, 2f)
        if (imgSize >= 3) {
            for (int i = 0; i < imgUrls.size(); i++) {
                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                if (i == 0) {
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 2, 2f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 2f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);

                    gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 1) {//(0,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                    layoutParams.bottomMargin = DisplayUtils.dip2px(mContext, 1);

                    gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 2) {////(1,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                    layoutParams.topMargin = DisplayUtils.dip2px(mContext, 1);

                    gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
            }
        } else if (imgSize == 2) {
            for (int i = 0; i < imgUrls.size(); i++) {

                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 1f);

                //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                layoutParams.height = 0;
                layoutParams.width = 0;
                //还可以根据位置动态定义子控件直接的边距，下面的意思是

                if (i == 0)
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);
                if (i == 1) {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                }

                gridImage.addView(imageView, layoutParams);
                ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
            }
        } else {
            ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            GridLayout.Spec imgRowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec imgColumnSpec = GridLayout.spec(0, 1, 1f);

            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams imglayoutParams = new GridLayout.LayoutParams(imgRowSpec, imgColumnSpec);
            imglayoutParams.height = 0;
            imglayoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是
            imglayoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);
            gridImage.addView(imageView, imglayoutParams);
            ImageLoaderUtil.loadImg(imageView, imgUrls.get(0));

            //占位的
            View functionView = new View(mContext);
            GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);
            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            layoutParams.height = 0;
            layoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是

            layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
            gridImage.addView(functionView, layoutParams);
        }
    }

    /**
     * 设置消息视图里各 view 的值
     *
     * @param holder       ViewHolder
     * @param parentHolder 父布局的 ViewHolder
     * @param message      此展示模板对应的消息
     * @param uiMessage    {@link UiMessage}
     * @param position     消息位置
     * @param list         列表
     * @param listener     ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     */
    @Override
    protected void bindMessageContentViewHolder(final ViewHolder holder, ViewHolder parentHolder, MomentContentMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        //设置item的容器的宽高
        final LinearLayout itemContainer = holder.getView(R.id.itemContainer);
        int sw = DisplayUtils.getScreenWidth(holder.getContext());

        ViewGroup.LayoutParams containerLayoutParams = itemContainer.getLayoutParams();
        containerLayoutParams.width = sw * 2 / 3;
        itemContainer.setLayoutParams(containerLayoutParams);


        //图片
        final FrameLayout flImgContainer = holder.getView(R.id.flImgContainer);
        final GridLayout gridImage = holder.getView(R.id.gridImage);
        final TextView tvIndex = holder.getView(R.id.tvIndex);

        List imgs = message.getImgs();
        if (null != imgs && imgs.size() > 0) {
            flImgContainer.setVisibility(View.VISIBLE);

            showGridImage(holder.getContext(), gridImage, tvIndex, imgs, sw * 2 / 3);
        } else {
            flImgContainer.setVisibility(View.GONE);
        }


        //文字
        TextView richTextView = holder.getView(R.id.richText);

        String content = message.getContent();
        //文本内容展示
        if (!TextUtils.isEmpty(content)) {
            //这里先不处理话题跳转问题
            richTextView.setText(content);

            richTextView.setVisibility(View.VISIBLE);
        } else {
            richTextView.setVisibility(View.GONE);
        }

        //发布者信息
        CircleImageView ivUserHead = holder.getView(R.id.ivUserHead);
        String userPhoto = message.getAuthorHeadImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userPhoto);
        } else {
            ivUserHead.setImageResource(R.drawable.place_holder);
        }

        TextView tvUserName = holder.getView(R.id.tvUserName);
        String authorNickname = message.getAuthorNickname();
        tvUserName.setText(TextUtils.isEmpty(authorNickname) ? "" : authorNickname);
    }

    /**
     * @param holder    ViewHolder
     * @param message   自定义消息
     * @param uiMessage {@link UiMessage}
     * @param position  位置
     * @param list      列表数据
     * @param listener  ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     * @return 点击事件是否被消费
     */
    @Override
    protected boolean onItemClick(ViewHolder holder, MomentContentMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                .withString("id", message.getId())
                .navigation();

        return true;
    }

    /**
     * 根据消息内容，判断是否为本模板需要展示的消息类型
     *
     * @param messageContent 消息内容
     * @return 本模板是否处理。
     */
    @Override
    protected boolean isMessageViewType(MessageContent messageContent) {
        return messageContent instanceof MomentContentMessage;
    }

    /**
     * 在会话列表页某条会话最后一条消息为该类型消息时，会话里需要展示的内容。
     * 比如: 图片消息在会话里需要展示为"图片"，那返回对应的字符串资源即可。
     *
     * @param context 上下文
     * @param message 消息内容
     * @return 会话里需要展示的字符串资源
     */
    @Override
    public Spannable getSummarySpannable(Context context, MomentContentMessage message) {
        int type = message.getType();
        return new SpannableString(YXConfig.getTypeName(type));
    }
}
