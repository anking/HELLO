package com.yanhua.rong.msgprovider;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.view.CircleImageView;

import java.util.List;

import io.rong.imkit.conversation.messgelist.provider.BaseMessageItemProvider;
import io.rong.imkit.model.UiMessage;
import io.rong.imkit.widget.adapter.IViewProviderListener;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imlib.model.MessageContent;

public class NewsMessageProvider extends BaseMessageItemProvider<NewsMessage> {

    public NewsMessageProvider() {
        mConfig.showReadState = true;
        mConfig.showContentBubble = false;
    }

    /**
     * 创建 ViewHolder
     *
     * @param parent   父 ViewGroup
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    protected ViewHolder onCreateMessageContentViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_news_message_item, parent, false);

        return new ViewHolder(view.getContext(), view);
    }

    /**
     * 设置消息视图里各 view 的值
     *
     * @param holder       ViewHolder
     * @param parentHolder 父布局的 ViewHolder
     * @param message      此展示模板对应的消息
     * @param uiMessage    {@link UiMessage}
     * @param position     消息位置
     * @param list         列表
     * @param listener     ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     */
    @Override
    protected void bindMessageContentViewHolder(final ViewHolder holder, ViewHolder parentHolder, NewsMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        //设置item的容器的宽高
        final LinearLayout itemContainer = holder.getView(R.id.itemContainer);
        int sw = DisplayUtils.getScreenWidth(holder.getContext());

        ViewGroup.LayoutParams containerLayoutParams = itemContainer.getLayoutParams();
        containerLayoutParams.width = sw * 2 / 3;
        itemContainer.setLayoutParams(containerLayoutParams);

        TextView tvTitle = holder.getView(R.id.richText);
        ImageView ivCover = holder.getView(R.id.ivCover);

        String title = message.getTitle();
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
        }


        String coverUrl = message.getCoverUrl();
        ImageView ivCoverType = holder.getView(R.id.ivCoverType);

        if (!TextUtils.isEmpty(coverUrl)) {
            boolean isVideo = FileUtils.isVideo(coverUrl);

            if (!isVideo) {
                ivCoverType.setVisibility(View.GONE);
                ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl);
            } else {
                ivCoverType.setVisibility(View.VISIBLE);

                RequestOptions options = new RequestOptions();

                options.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL)
                        .frame(1000000)
                        .centerCrop();

                Glide.with(holder.getContext())
                        .setDefaultRequestOptions(options)
                        .asBitmap()
                        .load(coverUrl)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                ivCover.setImageBitmap(resource);
                            }
                        });
            }

        } else {
            ivCover.setImageResource(R.drawable.place_holder);
        }

        //文字
        TextView richTextView = holder.getView(R.id.tvDesc);
        String content = message.getContent();
        //文本内容展示
        if (!TextUtils.isEmpty(content)) {
            richTextView.setText(content);

            richTextView.setVisibility(View.VISIBLE);
        } else {
            richTextView.setVisibility(View.GONE);
        }

        //发布者信息
        CircleImageView ivUserHead = holder.getView(R.id.ivUserHead);
        String userPhoto = message.getAuthorHeadImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userPhoto);
        } else {
            ivUserHead.setImageResource(R.drawable.place_holder);
        }

        TextView tvUserName = holder.getView(R.id.tvUserName);
        String authorNickname = message.getAuthorNickname();
        tvUserName.setText(TextUtils.isEmpty(authorNickname) ? "" : authorNickname);
    }

    /**
     * @param holder    ViewHolder
     * @param message   自定义消息
     * @param uiMessage {@link UiMessage}
     * @param position  位置
     * @param list      列表数据
     * @param listener  ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     * @return 点击事件是否被消费
     */
    @Override
    protected boolean onItemClick(ViewHolder holder, NewsMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        PageJumpUtil.jumpNewsDetailPage(message.getId(),message.getType());

        return true;
    }

    /**
     * 根据消息内容，判断是否为本模板需要展示的消息类型
     *
     * @param messageContent 消息内容
     * @return 本模板是否处理。
     */
    @Override
    protected boolean isMessageViewType(MessageContent messageContent) {
        return messageContent instanceof NewsMessage;
    }

    /**
     * 在会话列表页某条会话最后一条消息为该类型消息时，会话里需要展示的内容。
     * 比如: 图片消息在会话里需要展示为"图片"，那返回对应的字符串资源即可。
     *
     * @param context 上下文
     * @param message 消息内容
     * @return 会话里需要展示的字符串资源
     */
    @Override
    public Spannable getSummarySpannable(Context context, NewsMessage message) {
        int type = message.getType();
        return new SpannableString(YXConfig.getTypeName(type));
    }
}
