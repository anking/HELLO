package com.yanhua.rong.msgprovider;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.CircleImageView;

import java.util.List;

import io.rong.imkit.conversation.messgelist.provider.BaseMessageItemProvider;
import io.rong.imkit.model.UiMessage;
import io.rong.imkit.widget.adapter.IViewProviderListener;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imlib.model.MessageContent;

public class MomentVideoMessageProvider extends BaseMessageItemProvider<MomentVideoMessage> {

    public MomentVideoMessageProvider() {
        mConfig.showReadState = true;
        mConfig.showContentBubble = false;
    }

    /**
     * 创建 ViewHolder
     *
     * @param parent   父 ViewGroup
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    protected ViewHolder onCreateMessageContentViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_moment_video_message_item, parent, false);

        return new ViewHolder(view.getContext(), view);
    }

    private Integer minSize = null;
    private Integer maxSize = null;
    private static int THUMB_COMPRESSED_SIZE = 240;
    private static int THUMB_COMPRESSED_MIN_SIZE = 100;

    /**
     * 设置消息视图里各 view 的值
     *
     * @param holder       ViewHolder
     * @param parentHolder 父布局的 ViewHolder
     * @param message      此展示模板对应的消息
     * @param uiMessage    {@link UiMessage}
     * @param position     消息位置
     * @param list         列表
     * @param listener     ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     */
    @Override
    protected void bindMessageContentViewHolder(final ViewHolder holder, ViewHolder parentHolder, MomentVideoMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        //设置item的容器的宽高
        final LinearLayout itemContainer = holder.getView(R.id.itemContainer);
        int sw = DisplayUtils.getScreenWidth(holder.getContext());

        ViewGroup.LayoutParams containerLayoutParams = itemContainer.getLayoutParams();
        containerLayoutParams.width = sw * 2 / 3;
        itemContainer.setLayoutParams(containerLayoutParams);

        //图片
        final ImageView ivCover = holder.getView(R.id.ivCover);

        int realWidth = sw * 2 / 3 - DisplayUtils.dip2px(holder.getContext(), 24);
        ViewGroup.LayoutParams imageLayoutParams = ivCover.getLayoutParams();

        String coverUrl = message.getCoverUrl();
        if (!TextUtils.isEmpty(coverUrl)) {

            //后台返回null
            int width = Integer.parseInt(TextUtils.isEmpty(message.getWidth()) ? "0" : message.getWidth());
            int height = Integer.parseInt(TextUtils.isEmpty(message.getHeight()) ? "0" : message.getHeight());

            if (width != 0 && height != 0) {
                float rate = DisplayUtils.getImageRate(width, height);//4:3 1:1 3:4
                float showHeight = realWidth / rate;

                imageLayoutParams.width = realWidth;
                imageLayoutParams.height = (int) showHeight;
            } else {
                imageLayoutParams.width = realWidth;
                imageLayoutParams.height = realWidth;
            }

            ivCover.setLayoutParams(imageLayoutParams);

            ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl + "?x-oss-process=image/resize,p_50", R.drawable.place_holder);
        } else {
            imageLayoutParams.width = realWidth;
            imageLayoutParams.height = realWidth;

            ivCover.setLayoutParams(imageLayoutParams);
            ImageLoaderUtil.loadImgCenterCrop(ivCover, coverUrl, R.drawable.place_holder);
        }


        //文字
        TextView richTextView = holder.getView(R.id.richText);

        String content = message.getContent();
        //文本内容展示
        if (!TextUtils.isEmpty(content)) {
            //这里先不处理话题跳转问题
            richTextView.setText(content);

            richTextView.setVisibility(View.VISIBLE);
        } else {
            richTextView.setVisibility(View.GONE);
        }

        //发布者信息
        CircleImageView ivUserHead = holder.getView(R.id.ivUserHead);
        String userPhoto = message.getAuthorHeadImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userPhoto);
        } else {
            ivUserHead.setImageResource(R.drawable.place_holder);
        }

        TextView tvUserName = holder.getView(R.id.tvUserName);
        String authorNickname = message.getAuthorNickname();
        tvUserName.setText(TextUtils.isEmpty(authorNickname) ? "" : authorNickname);
    }

    /**
     * @param holder    ViewHolder
     * @param message   自定义消息
     * @param uiMessage {@link UiMessage}
     * @param position  位置
     * @param list      列表数据
     * @param listener  ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     * @return 点击事件是否被消费
     */
    @Override
    protected boolean onItemClick(ViewHolder holder, MomentVideoMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        int type = message.getType();

        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                .withSerializable("contentModel", null)
                .withSerializable("contentId", message.getId())
                .withString("categoryId", "")
                .withInt("discoBest", type == YXConfig.TYPE_BEST ? 1 : 0)//电音集锦
                .withString("city", "")
                .withInt("module", 0)
                .navigation();
        return true;
    }

    /**
     * 根据消息内容，判断是否为本模板需要展示的消息类型
     *
     * @param messageContent 消息内容
     * @return 本模板是否处理。
     */
    @Override
    protected boolean isMessageViewType(MessageContent messageContent) {
        return messageContent instanceof MomentVideoMessage;
    }

    /**
     * 在会话列表页某条会话最后一条消息为该类型消息时，会话里需要展示的内容。
     * 比如: 图片消息在会话里需要展示为"图片"，那返回对应的字符串资源即可。
     *
     * @param context 上下文
     * @param message 消息内容
     * @return 会话里需要展示的字符串资源
     */
    @Override
    public Spannable getSummarySpannable(Context context, MomentVideoMessage message) {
        int type = message.getType();
        return new SpannableString(YXConfig.getTypeName(type));
    }
}
