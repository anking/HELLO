package com.yanhua.rong.msgprovider;

import android.os.Parcel;
import android.os.Parcelable;

import com.yanhua.base.config.YXConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.DestructionTag;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(
        value = YXConfig.imRongMsg.userPage,
        flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED
)
@DestructionTag(destructionFlag = DestructionTag.DestructionType.FLAG_COUNT_DOWN_WHEN_CLICK)
public class UserPageMessage extends MessageContent implements Parcelable, Serializable {

    private static final String TAG = UserPageMessage.class.getSimpleName();

    private String userId;// 用户id
    private String bgImg;// 主页背景图
    private String headImg;// 头像
    private String nickName;// 昵称
    private int postNum;// 发布的帖子数
    private int fansNum;// 粉丝数

    public UserPageMessage() {
    }

    protected UserPageMessage(Parcel in) {
        setUserId(ParcelUtils.readFromParcel(in));
        setBgImg(ParcelUtils.readFromParcel(in));
        setHeadImg(ParcelUtils.readFromParcel(in));
        setNickName(ParcelUtils.readFromParcel(in));
        setPostNum(ParcelUtils.readIntFromParcel(in));
        setFansNum(ParcelUtils.readIntFromParcel(in));
    }

    public static final Creator<UserPageMessage> CREATOR = new Creator<UserPageMessage>() {
        @Override
        public UserPageMessage createFromParcel(Parcel in) {
            return new UserPageMessage(in);
        }

        @Override
        public UserPageMessage[] newArray(int size) {
            return new UserPageMessage[size];
        }
    };

    public UserPageMessage(byte[] data) {
        if (data == null) {
            RLog.e(TAG, "data is null ");
            return;
        }
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        if (jsonStr == null) {
            RLog.e(TAG, "jsonStr is null ");
            return;
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (jsonObj.has("user")) {
                setUserInfo(parseJsonToUserInfo(jsonObj.getJSONObject("user")));
            }
            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (jsonObj.has("mentionedInfo")) {
                setMentionedInfo(parseJsonToMentionInfo(jsonObj.getJSONObject("mentionedInfo")));
            }
            // ...
            // 自定义消息, 定义的字段
            // ...
            if (jsonObj.has("userId")) {
                setUserId(jsonObj.optString("userId"));
            }
            if (jsonObj.has("bgImg")) {
                setBgImg(jsonObj.optString("bgImg"));
            }
            if (jsonObj.has("headImg")) {
                setHeadImg(jsonObj.optString("headImg"));
            }
            if (jsonObj.has("nickName")) {
                setNickName(jsonObj.optString("nickName"));
            }
            if (jsonObj.has("postNum")) {
                setPostNum(jsonObj.optInt("postNum"));
            }
            if (jsonObj.has("fansNum")) {
                setFansNum(jsonObj.optInt("fansNum"));
            }
        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            // 消息携带用户信息时, 自定义消息需添加下面代码
            if (getJSONUserInfo() != null) {
                jsonObj.putOpt("user", getJSONUserInfo());
            }
            // 用于群组聊天, 消息携带 @ 人信息时, 自定义消息需添加下面代码
            if (getJsonMentionInfo() != null) {
                jsonObj.putOpt("mentionedInfo", getJsonMentionInfo());
            }
            // ...
            // 自定义消息, 定义的字段.
            // ...
            jsonObj.put("userId", getUserId());
            jsonObj.put("bgImg", getBgImg());
            jsonObj.put("headImg", getHeadImg());
            jsonObj.put("nickName", getNickName());
            jsonObj.put("postNum", getPostNum());
            jsonObj.put("fansNum", getFansNum());
        } catch (JSONException e) {
            RLog.e(TAG, "JSONException " + e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e(TAG, "UnsupportedEncodingException ", e);
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getUserId());
        ParcelUtils.writeToParcel(dest, getBgImg());
        ParcelUtils.writeToParcel(dest, getHeadImg());
        ParcelUtils.writeToParcel(dest, getNickName());
        ParcelUtils.writeToParcel(dest, getPostNum());
        ParcelUtils.writeToParcel(dest, getFansNum());
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBgImg() {
        return bgImg;
    }

    public void setBgImg(String bgImg) {
        this.bgImg = bgImg;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getPostNum() {
        return postNum;
    }

    public void setPostNum(int postNum) {
        this.postNum = postNum;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }
}
