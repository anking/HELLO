package com.yanhua.rong.msgprovider;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.R;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.CircleImageView;

import java.util.List;

import io.rong.imkit.conversation.messgelist.provider.BaseMessageItemProvider;
import io.rong.imkit.model.UiMessage;
import io.rong.imkit.picture.tools.ScreenUtils;
import io.rong.imkit.widget.adapter.IViewProviderListener;
import io.rong.imkit.widget.adapter.ViewHolder;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;

public class InviteMessageProvider extends BaseMessageItemProvider<InviteMessage> {

    public InviteMessageProvider() {
        mConfig.showReadState = true;
        mConfig.showContentBubble = false;
    }

    /**
     * 创建 ViewHolder
     *
     * @param parent   父 ViewGroup
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    protected ViewHolder onCreateMessageContentViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rc_invite_message_item, parent, false);

        return new ViewHolder(view.getContext(), view);
    }

    private Integer minSize = null;
    private Integer maxSize = null;
    private static int THUMB_COMPRESSED_SIZE = 240;
    private static int THUMB_COMPRESSED_MIN_SIZE = 100;

    /**
     * 设置消息视图里各 view 的值
     *
     * @param holder       ViewHolder
     * @param parentHolder 父布局的 ViewHolder
     * @param message      此展示模板对应的消息
     * @param uiMessage    {@link UiMessage}
     * @param position     消息位置
     * @param list         列表
     * @param listener     ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     */
    @Override
    protected void bindMessageContentViewHolder(final ViewHolder holder, ViewHolder parentHolder, InviteMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        //设置item的容器的宽高
        final LinearLayout itemContainer = holder.getView(R.id.itemContainer);
        int sw = DisplayUtils.getScreenWidth(holder.getContext());

        ViewGroup.LayoutParams containerLayoutParams = itemContainer.getLayoutParams();
        containerLayoutParams.width = sw * 2 / 3;
        itemContainer.setLayoutParams(containerLayoutParams);

        final RelativeLayout rlCover = holder.getView(R.id.rlCover);

        final ImageView ivCover = holder.getView(R.id.ivCover);
        final TextView tvTitle = holder.getView(R.id.tvTitle);
        final TextView tvAddress = holder.getView(R.id.tvAddress);

        String coverUrl = message.getCoverUrl();
        if (!TextUtils.isEmpty(coverUrl)) {
//            RequestOptions options = RequestOptions.bitmapTransform(new RoundedCorners(ScreenUtils.dip2px(IMCenter.getInstance().getContext(), 6))).override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            Glide.with(ivCover).load(coverUrl)
                    .error(uiMessage.getMessage().getMessageDirection() == Message.MessageDirection.SEND ? R.drawable.rc_send_thumb_image_broken : R.drawable.rc_received_thumb_image_broken)
//                    .apply(options)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            ViewGroup.LayoutParams params = rlCover.getLayoutParams();
                            params.height = ScreenUtils.dip2px(rlCover.getContext(), 100);
                            params.width = ScreenUtils.dip2px(rlCover.getContext(), 100);
                            rlCover.setLayoutParams(params);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable drawable, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            int width = drawable.getIntrinsicWidth();
                            int height = drawable.getIntrinsicHeight();
                            if (minSize == null) {
                                minSize = THUMB_COMPRESSED_MIN_SIZE;
                            }
                            if (maxSize == null) {
                                maxSize = THUMB_COMPRESSED_SIZE;
                            }
                            int finalWidth;
                            int finalHeight;
                            if (width < minSize || height < minSize) {
                                if (width < height) {
                                    finalWidth = minSize;
                                    finalHeight = Math.min((int) (minSize * 1f / width * height), maxSize);
                                } else {
                                    finalHeight = minSize;
                                    finalWidth = Math.min((int) (minSize * 1f / height * width), maxSize);
                                }
                            } else if (width < maxSize && height < maxSize) {
                                finalWidth = width;
                                finalHeight = height;
                            } else {
                                if (width > height) {
                                    if (width * 1f / height <= maxSize * 1.0f / minSize) {
                                        finalWidth = maxSize;
                                        finalHeight = (int) (maxSize * 1f / width * height);
                                    } else {
                                        finalWidth = maxSize;
                                        finalHeight = minSize;
                                    }
                                } else {
                                    if (height * 1f / width <= maxSize * 1.0f / minSize) {
                                        finalHeight = maxSize;
                                        finalWidth = (int) (maxSize * 1f / height * width);
                                    } else {
                                        finalHeight = maxSize;
                                        finalWidth = minSize;
                                    }
                                }
                            }
                            int showWidth = sw * 2 / 3 - DisplayUtils.dip2px(holder.getContext(), 24);
                            int showheight = finalHeight * showWidth / finalWidth;

                            ViewGroup.LayoutParams params = rlCover.getLayoutParams();
                            params.height = showheight;
                            params.width = showWidth;
                            rlCover.setLayoutParams(params);

                            return false;
                        }
                    })
                    .into(ivCover);
        } else {
            ViewGroup.LayoutParams params = rlCover.getLayoutParams();
            params.height = ScreenUtils.dip2px(rlCover.getContext(), 100);
            params.width = ScreenUtils.dip2px(rlCover.getContext(), 100);
            rlCover.setLayoutParams(params);
            ivCover.setImageResource(uiMessage.getMessage().getMessageDirection() == Message.MessageDirection.SEND ? R.drawable.rc_send_thumb_image_broken : R.drawable.rc_received_thumb_image_broken);
        }

        String title = message.getTitle();
        tvTitle.setText(title);

        String address = message.getAddress();
        tvAddress.setText(address);

        final ImageView ivCoverType = holder.getView(R.id.ivCoverType);
        ivCoverType.setVisibility((message.getCoverType() == 2) ? View.VISIBLE : View.GONE);

        //发布者信息
        CircleImageView ivUserHead = holder.getView(R.id.ivUserHead);
        String userPhoto = message.getAuthorHeadImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userPhoto);
        } else {
            ivUserHead.setImageResource(R.drawable.place_holder);
        }

        TextView tvUserName = holder.getView(R.id.tvUserName);
        String authorNickname = message.getAuthorNickname();
        tvUserName.setText(TextUtils.isEmpty(authorNickname) ? "" : authorNickname);
    }

    /**
     * @param holder    ViewHolder
     * @param message   自定义消息
     * @param uiMessage {@link UiMessage}
     * @param position  位置
     * @param list      列表数据
     * @param listener  ViewModel 的点击事件监听器。如果某个子 view 的点击事件需要 ViewModel 处理，可通过此监听器回调。
     * @return 点击事件是否被消费
     */
    @Override
    protected boolean onItemClick(ViewHolder holder, InviteMessage message, UiMessage uiMessage, int position, List<UiMessage> list, IViewProviderListener<UiMessage> listener) {
        ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                .withSerializable("id", message.getId())
                .navigation();
        return true;
    }

    /**
     * 根据消息内容，判断是否为本模板需要展示的消息类型
     *
     * @param messageContent 消息内容
     * @return 本模板是否处理。
     */
    @Override
    protected boolean isMessageViewType(MessageContent messageContent) {
        return messageContent instanceof InviteMessage;
    }

    /**
     * 在会话列表页某条会话最后一条消息为该类型消息时，会话里需要展示的内容。
     * 比如: 图片消息在会话里需要展示为"图片"，那返回对应的字符串资源即可。
     *
     * @param context 上下文
     * @param message 消息内容
     * @return 会话里需要展示的字符串资源
     */
    @Override
    public Spannable getSummarySpannable(Context context, InviteMessage message) {
        int type = message.getType();
        return new SpannableString(YXConfig.getTypeName(type));
    }
}
