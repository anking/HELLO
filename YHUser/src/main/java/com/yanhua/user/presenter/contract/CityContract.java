package com.yanhua.user.presenter.contract;

import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CityObjModel;

import java.util.List;

public interface CityContract {

    interface IView extends BaseView {

        default void handleSuccessHotcity(List<CityModel> list) {
        }

        default void handleSuccessAllAreas(List<CityObjModel> list) {
        }

        default void handleSuccessAllBarAreas(List<CityModel> list) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void getCity();

        void getBarCity();
    }

}
