package com.yanhua.user.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ProtocolVersionModel;

import java.util.List;

public interface ADContract {

    interface IView extends BaseView {
        /**
         * 获取广告位成功
         *
         * @param advertiseList
         */
        default void handleAdvertiseSuccess(List<AdvertiseModel> advertiseList) {
        }

        default void handleAppProtocol(String code, ProtocolModel data) {
        }

        default void handleProtocolVersion(List<ProtocolVersionModel> data) {
        }
        default void handleProtocolVersionFail(String msg) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

    }
}
