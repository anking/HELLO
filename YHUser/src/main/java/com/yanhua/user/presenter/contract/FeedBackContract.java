package com.yanhua.user.presenter.contract;

import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.FeedbackForm;
import com.yanhua.common.model.StsTokenModel;

import java.util.HashMap;
import java.util.List;

public interface FeedBackContract {

    interface IView extends BaseView {
        default void handleFeedbackTypeSuccess(List<CommonReasonModel> list) {
        }

        default void handleFeedbackSuccess() {
        }

        default void handleStsToken(StsTokenModel model) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void getFeedbackTypeList(HashMap<String, Object> params);

        void feedback(FeedbackForm form);

        void getStsToken();
    }
}
