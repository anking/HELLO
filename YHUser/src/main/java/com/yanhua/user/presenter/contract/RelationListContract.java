package com.yanhua.user.presenter.contract;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;

import java.util.List;

public interface RelationListContract {
    interface IView extends BaseView {

        default void updateFollowUserSuccess() {
        }

        default void handleUserListSuccess(List<FriendModel> list) {
        }

    }

    interface Presenter extends BasePresenter<IView> {
        void getUserFriend(String userId, String nickName ,int current, int size);

        void getNewAttentions(int current, int size);

        void getFollowUserList(String userId, int current, int size, String keyword);

        void getUserFansList(String userId, int current, int size, String keyword);

        void cancelFollowUser(String userId);

        void followUser(String userId);
    }
}
