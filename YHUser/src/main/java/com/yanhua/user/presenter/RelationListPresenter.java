package com.yanhua.user.presenter;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.user.presenter.contract.RelationListContract;

public class RelationListPresenter extends MvpPresenter<RelationListContract.IView> implements RelationListContract.Presenter {


    @Override
    public void getNewAttentions(int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getNewAttentions(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }

            }
        }, current, size));
    }

    @Override
    public void getUserFriend(String userId,String nickName ,int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getFriendUserList(new SimpleSubscriber<HttpResult<ListResult<FriendModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<FriendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<FriendModel> listResult = result.getData();
                        baseView.handleUserListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId,nickName, current, size));
    }

    @Override
    public void getFollowUserList(String userId, int current, int size, String keyword) {
        addSubscribe(MomentHttpMethod.getInstance().getFollowUserList(new SimpleSubscriber<HttpResult<ListResult<FriendModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<FriendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<FriendModel> listResult = result.getData();
                        baseView.handleUserListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId, current, size, keyword));
    }

    @Override
    public void getUserFansList(String userId, int current, int size, String keyword) {
        addSubscribe(MomentHttpMethod.getInstance().getUserFansList(new SimpleSubscriber<HttpResult<ListResult<FriendModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<FriendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<FriendModel> listResult = result.getData();
                        baseView.handleUserListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId, current, size, keyword));
    }


    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }
}
