package com.yanhua.user.presenter;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.presenter.contract.PersonalPageContract;

import java.util.HashMap;

public class PersonalPagePresenter extends MvpPresenter<PersonalPageContract.IView> implements PersonalPageContract.Presenter {

    @Override
    public void setBackGround(String blackgroundImg) {
        addSubscribe(MomentHttpMethod.getInstance().setBackGround(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSetBackGroundSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, blackgroundImg));
    }

    @Override
    public void getContentUserDetail(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().getContentUserDetail(new SimpleSubscriber<HttpResult<ContentUserInfoModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ContentUserInfoModel> result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ContentUserInfoModel model = result.getData();
                        baseView.handleContentUserDetailSuccess(model);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void getUserContent(int contentType, String userId, int current) {
        addSubscribe(MomentHttpMethod.getInstance().getUserContentList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleUserContentList(listResult.getRecords());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, contentType, userId, current));
    }


    @Override
    public void getUserContent(String userId, int current) {
        addSubscribe(MomentHttpMethod.getInstance().getUserContentList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleUserContentList(listResult.getRecords());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId, current));
    }

    @Override
    public void setFriendRemark(String followid, String backName) {
        HashMap params = new HashMap();
        params.put("followeeId", followid);
        params.put("friendRemark", backName);
        addSubscribe(MomentHttpMethod.getInstance().setFriendRemark(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSetFriendRemarkSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void deleteContent(String id) {
        addSubscribe(MomentHttpMethod.getInstance().deleteContent(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteContentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, id));
    }


    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void addOrCancelBlack(String userId) {
        addSubscribe(MessageHttpMethod.getInstance().addBlack(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateBlackUserSuccess();
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }, userId));
    }


//
//    @Override
//    public void updateStarContent(String id,int type) {
//        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
//            @Override
//            public void onNext(HttpResult result) {
//                super.onNext(result);
//                if (result != null) {
//                    if (result.getCode().equals(HttpCode.SUCCESS)) {
//                        baseView.updateStarContentSuccess();
//                    } else {
//                        baseView.handleErrorMsg(result.getMesg());
//                    }
//                }
//            }
//        }, type, id));
//    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void getPersonalInfo(String userId) {
        addSubscribe(UserHttpMethod.getInstance().getPersonalInfo(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    UserInfo userInfo = bean.getData();
                    if (userInfo != null) {
                        baseView.handleUserDetail(userInfo);
                    }
                } else {
                    baseView.handleErrorMsg(bean.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void updateUserDetail(String userId, HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().putPersonalInfo(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    baseView.updateUserDetailSuccess();
                } else {
                    baseView.handleErrorMsg(bean.getMesg());
                }
            }
        }, userId, params));
    }

}
