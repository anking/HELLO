package com.yanhua.user.presenter;

import android.text.TextUtils;

import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ProtocolVersionModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.user.presenter.contract.ADContract;

import java.util.HashMap;
import java.util.List;

public class ADPresenter extends MvpPresenter<ADContract.IView> implements ADContract.Presenter {

    public void getAppProtocol(String code) {
        addSubscribe(CommonHttpMethod.getInstance().getAppProtocol(new SimpleSubscriber<HttpResult<ProtocolModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ProtocolModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleAppProtocol(code, httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, code));
    }

    public void getAdvertise(String code) {
        HashMap<String, Object> params = new HashMap<>();

//        "subModule":0,
        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        String cityName = TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress;

        params.put("areaName", cityName);
        params.put("code", code);
        params.put("systemType", 1);

        addSubscribe(CommonHttpMethod.getInstance().queryAdList(new SimpleSubscriber<HttpResult<List<AdvertiseModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<AdvertiseModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleAdvertiseSuccess(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    public void userSignProtocol(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().userSignProtocol(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handActionSuccess(0);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void queryProtocol() {
        addSubscribe(CommonHttpMethod.getInstance().queryProtocol(new SimpleSubscriber<HttpResult<List<ProtocolVersionModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<ProtocolVersionModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleProtocolVersion(result.getData());
                    } else {
                        baseView.handleProtocolVersionFail(result.getMesg());
                    }
                }
            }
        }));
    }

}
