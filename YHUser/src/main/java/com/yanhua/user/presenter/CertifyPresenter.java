package com.yanhua.user.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.model.FaceResultModel;
import com.yanhua.user.model.IDCradInfo;
import com.yanhua.user.presenter.contract.CertifyContract;

import java.util.HashMap;

public class CertifyPresenter extends MvpPresenter<CertifyContract.IView> implements CertifyContract.Presenter {
    @Override
    public void idCardVerification(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().idCardVerification(new SimpleSubscriber<HttpResult<String>>(baseView, false) {
            @Override
            public void onNext(HttpResult<String> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessVerification((String) result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void checkIdCardInformation(String imageUrl) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("imageUrl", imageUrl);
        addSubscribe(UserHttpMethod.getInstance().checkIdCardInformation(new SimpleSubscriber<HttpResult<IDCradInfo>>(baseView, true) {
            @Override
            public void onNext(HttpResult<IDCradInfo> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessCheckIdCardInfo(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UserHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }


    @Override
    public void updateUserDetail(String userId, String realName, String idCard, int realNameValid) {
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("realName", realName);
        paramsMap.put("idCard", idCard);
        paramsMap.put("realNameValid", realNameValid);
        addSubscribe(UserHttpMethod.getInstance().putPersonalInfo(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.updateUserDetailSuccess();
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, userId, paramsMap));
    }

    @Override
    public void getFaceId(String idcradNo, String name, String userId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("idNo", idcradNo);
        params.put("name", name);
        params.put("userId", userId);
        addSubscribe(UserHttpMethod.getInstance().getFaceId(new SimpleSubscriber<HttpResult<FaceResultModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<FaceResultModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if ("0".equals(result.getData().getCode())) {
                        baseView.handleSuccessFaceId(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getData().getMsg());
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

}
