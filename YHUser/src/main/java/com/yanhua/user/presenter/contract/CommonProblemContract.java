package com.yanhua.user.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommonProblemBean;

import java.util.HashMap;
import java.util.List;

public interface CommonProblemContract {

    interface IView extends BaseView {

        void handleCommonProblemSuccess(ListResult<CommonProblemBean> list);
    }

    interface Presenter extends BasePresenter<IView> {

        void getCommonProblem(HashMap<String, Object> params);
    }

}
