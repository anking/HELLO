package com.yanhua.user.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;

import java.util.HashMap;
import java.util.List;

public interface PersonalPageContract {

    interface IView extends BaseView {
        default void handleDeleteContentSuccess() {
        }

        default void handleSetFriendRemarkSuccess() {
        }

        default void handleSetBackGroundSuccess() {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void handleUserContentList(List<MomentListModel> list) {
        }

        default void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        }

        default void handleErrorMsg(String msg) {
        }

        default void updateFollowUserSuccess() {
        }

        default void updateBlackUserSuccess() {
        }

        default void updateStarContentSuccess() {
        }

        default void handleUserDetail(UserInfo userInfo) {
        }

        default void updateUserDetailSuccess() {
        }

    }

    interface Presenter extends BasePresenter<IView> {
        void getUserContent(String userId, int current);

        void setBackGround(String blackgroundImg);

        void setFriendRemark(String followid, String backName);

        void deleteContent(String id);

        void getContentUserDetail(String userId);

        void getUserContent(int contentType, String userId, int current);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void addOrCancelBlack(String userId);

        void getStsToken();

        /**
         * 获取用户个人信息
         *
         * @param userId
         */
        void getPersonalInfo(String userId);


        /**
         * 修改用户信息
         *
         * @param userId
         * @param params
         */
        void updateUserDetail(String userId, HashMap<String, Object> params);
    }
}
