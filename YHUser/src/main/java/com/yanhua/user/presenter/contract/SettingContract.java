package com.yanhua.user.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.user.model.PrivacySettingModel;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.model.UserInfo;

import java.util.HashMap;
import java.util.List;

public interface SettingContract {

    interface IView extends BaseView {
        default void handleUserDetail(UserInfo userInfo) {

        }

        default void handleSuccessAppVersionInfo(UpgradeInfoModel upgradeInfoModel) {

        }

        default void handleSuccessGetPrivacySetting(PrivacySettingModel privacySettingModel) {

        }

        default void handleSuccessUpdatePrivacySetting() {

        }

        default void handleErrorMessage(String errorMsg) {

        }

        default void handleAppProtocol(String code,ProtocolModel data){}

        default void handleServerConfigList(List<ServerConfigModel> data){}

        default void handleBlackListSuccess(List<BlackMemberBean> records){}

        default void updateBlackUserSuccess(){}
    }

    interface Presenter extends BasePresenter<IView> {
        void fetchAppVersionInfo(int type);

        void getUserDetail(String userId);

        void getPrivacySetting();

        void updatePrivacySetting(HashMap<String, Object> params);
    }

}
