package com.yanhua.user.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.FeedbackForm;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.presenter.contract.FeedBackContract;

import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FeedBackPresenter extends MvpPresenter<FeedBackContract.IView> implements FeedBackContract.Presenter {

    @Override
    public void getFeedbackTypeList(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleFeedbackTypeSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void feedback(FeedbackForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        // 生成RequestBody
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(UserHttpMethod.getInstance().feedback(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleFeedbackSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, body));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }
}
