package com.yanhua.user.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CityObjModel;
import com.yanhua.user.presenter.contract.CityContract;

import java.util.List;


public class CityPresenter extends MvpPresenter<CityContract.IView> implements CityContract.Presenter {

    @Override
    public void getCity() {
        addSubscribe(CommonHttpMethod.getInstance().getCity(new SimpleSubscriber<HttpResult<List<CityObjModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<List<CityObjModel>> bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.handleSuccessAllAreas(bean.getData());
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }));
    }

    @Override
    public void getBarCity() {
        addSubscribe(CommonHttpMethod.getInstance().getBarAllAreas(new SimpleSubscriber<HttpResult<List<CityModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<List<CityModel>> bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessAllBarAreas(bean.getData());
                } else {
                    baseView.handleErrorMessage(bean.getMesg());
                }
            }
        }));
    }
}
