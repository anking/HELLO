package com.yanhua.user.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.user.model.FaceResultModel;
import com.yanhua.user.model.IDCradInfo;

import java.util.HashMap;

public interface CertifyContract {

    interface IView extends BaseView {
        default void updateUserDetailSuccess(){}

        default void handleSuccessFaceId(FaceResultModel faceModel){}

        default void handleStsToken(StsTokenModel data){}
        default void handleSuccessVerification(String data) {

        }

        default void handleSuccessCheckIdCardInfo(IDCradInfo info) {

        }

    }

    interface Presenter extends BasePresenter<IView> {

        /**
         * 修改用户信息
         *
         * @param userId
         * @param realName
         * @param IDCard
         * @param realNameValid
         */
        void updateUserDetail(String userId, String realName, String IDCard, int realNameValid);

        void getFaceId(String idcradNo, String name, String userId);

        void idCardVerification(HashMap<String, Object> params);

        void checkIdCardInformation(String imageUrl);

        void getStsToken();
    }
}
