package com.yanhua.user.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.presenter.contract.CommonProblemContract;

import java.util.HashMap;

public class CommonProblemPresenter extends MvpPresenter<CommonProblemContract.IView> implements CommonProblemContract.Presenter {

    @Override
    public void getCommonProblem(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().getProblemList(new SimpleSubscriber<HttpResult<ListResult<CommonProblemBean>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<CommonProblemBean>> result) {
                super.onNext(result);
                String code = result.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    baseView.handleCommonProblemSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }
}

