package com.yanhua.user.presenter;

import android.text.TextUtils;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.contract.LoginContract;

import java.util.HashMap;

/**
 * 登录注册、绑定
 */
public class LoginPresenter extends MvpPresenter<LoginContract.IView> implements LoginContract.Presenter {

    public void userLogOff(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().userLogOff(new SimpleSubscriber<HttpResult<Boolean>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Boolean> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleUserLogOffSuccess(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }


    public void checkMobilePhone(String phone) {
        addSubscribe(CommonHttpMethod.getInstance().getAppUserMobile(new SimpleSubscriber<HttpResult<Boolean>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Boolean> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                    baseView.handleBooleanResult(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, phone));
    }


    public void getAppProtocol(String code) {
        addSubscribe(CommonHttpMethod.getInstance().getAppProtocol(new SimpleSubscriber<HttpResult<ProtocolModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ProtocolModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleAppProtocol(code, httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, code));
    }

    public void userSignProtocol(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().userSignProtocol(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handActionSuccess(0);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    @Override
    public void login(String username, final String password, final String grant_type, String phone) {

        HashMap<String, Object> params = new HashMap<>();

//        params.put("type","APP");
//        params.put("username",username);
//        params.put("password",password);
//        params.put("grant_type",grant_type);
//        params.put("scope","read");
//        params.put("client_id","test_client");
//        params.put("client_secret","test_secret");

        //悦鑫
        params.put("phone", TextUtils.isEmpty(phone) ? "" : phone);
        params.put("type", "APP");
        params.put("username", username);
        params.put("password", password);
        params.put("grant_type", grant_type);
        params.put("scope", "read");
        params.put("client_id", "app_client");
        params.put("client_secret", "test_secret");

        addSubscribe(UserHttpMethod.getInstance().login(new SimpleSubscriber<HttpResult<LoginResult>>(baseView, true) {
            @Override
            public void onNext(HttpResult<LoginResult> bean) {
                super.onNext(bean);
                if (bean != null) {
                    if (bean.getCode().equals(HttpCode.SUCCESS)) {
                        LoginResult loginResult = bean.getData();
                        baseView.handleLoginResult(loginResult);
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, params));
    }

    @Override
    public void getUserDetail(String userId) {
        addSubscribe(UserHttpMethod.getInstance().getUserDetail(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    baseView.handleUserDetail(bean.getData());
                } else {
                    baseView.handleErrorMessage(bean.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void wechatAuthLogin(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().weixinRegister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {

                    baseView.handleSuccessWechatRegister(result.getData());

                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }


    @Override
    public void weiboAuthLogin(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().weiboRegister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessWeiboRegister(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void qqAuthLogin(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().qqRegister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessQQRegister(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void bindPhone(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().bindPhone(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessBindPhone();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void existPhone(String phone) {
        addSubscribe(UserHttpMethod.getInstance().existPhone(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Double) {
                        double data = (double) result.getData();
                        baseView.handleSuccessExistPhone((int) data);
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, phone));
    }

    @Override
    public void getSmsCode(String phone, String type) {
        addSubscribe(UserHttpMethod.getInstance().getSmsCode(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        boolean data = (boolean) bean.getData();
                        if (data) {
                            baseView.handleSendSuccess();
                        } else {
                            baseView.handleErrorMessage("验证码发送失败");
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, phone, type));
    }

    @Override
    public void validateCode(String phone, String code, String type) {
        addSubscribe(UserHttpMethod.getInstance().validateCode(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        boolean data = (boolean) bean.getData();
                        if (data) {
                            // 验证码正确
                            baseView.handleValidateSuccess(type);
                        } else {
                            // 验证码错误
                            baseView.handleErrorMessage("验证码错误");
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, phone, code, type));
    }

    @Override
    public void reset(String phone, String code, String newpwd, String confirmpwd) {
        HashMap<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("updatePassword", newpwd);
//        paramsMap.put("username", phone);
        paramsMap.put("mobile", phone);
        addSubscribe(UserHttpMethod.getInstance().modifyUserPassword(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.handlePswdResetResult();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, null, paramsMap));
    }

    public void modifyPswdByPswd(String phone, String oriPassword, String newpwd) {
        HashMap<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("updatePassword", newpwd);
        paramsMap.put("password", oriPassword);
        paramsMap.put("mobile", phone);
        addSubscribe(UserHttpMethod.getInstance().modifyUserPassword(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.handlePswdResetResult();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, null, paramsMap));
    }


    public void modifyPhoneNum(String oriPhone, String updatePhone) {
        HashMap<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("mobile", oriPhone);
        paramsMap.put("updateMobile", updatePhone);
        addSubscribe(UserHttpMethod.getInstance().modifyUserPassword(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.handleModifyPhoneSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, null, paramsMap));
    }

    public void setPassword(String phone, String password) {
        HashMap<String, Object> paramsMap = new HashMap<>(1);
        paramsMap.put("password", password);
        paramsMap.put("mobile", phone);
        addSubscribe(UserHttpMethod.getInstance().modifyUserPassword(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    String code = result.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        baseView.handlePswdResetResult();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, null, paramsMap));
    }


    public void weiboUntie() {
        addSubscribe(UserHttpMethod.getInstance().weiboUntie(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleWeiboUntieSuccess();
                        } else {
                            baseView.handleErrorMessage("解绑失败");
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    public void qqUntie() {
        addSubscribe(UserHttpMethod.getInstance().qqUntie(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleQQUntieSuccess();
                        } else {
                            baseView.handleErrorMessage("解绑失败");
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    public void weixinUntie() {
        addSubscribe(UserHttpMethod.getInstance().weixinUntie(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleWechatUntieSuccess();
                        } else {
                            baseView.handleErrorMessage("解绑失败");
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }
}
