package com.yanhua.user.presenter.contract;


import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.model.LoginResult;

import java.util.HashMap;

public interface LoginContract {

    interface IView extends BaseView {//统一一个方法也可以，方便阅读？

        default boolean isProtocolChecked() {//处理是否点击
            return false;
        }


        default void handleLoginResult(LoginResult loginResult) {
        }

        default void handleUserDetail(UserInfo userInfo) {
        }

        default void handleSendSuccess() {
        }

        default void handleValidateSuccess(String type) {
        }

        default void handleSuccessWechatRegister(Object wechatAuth) {
        }

        default void handleSuccessWeiboRegister(Object weiboAuth) {

        }

        default void handleSuccessQQRegister(Object qqAuth) {
        }

        default void handleSuccessBindPhone() {
        }


        default void handleSuccessExistPhone(int rs) {
        }

        default void handlePswdResetResult() {
        }

        default void handleAppProtocol(String code , ProtocolModel data ){}

        default void handleModifyPhoneSuccess(){}

        default void handleWeiboUntieSuccess(){}
        default void handleQQUntieSuccess(){}
        default void handleWechatUntieSuccess(){}

        default void handleBooleanResult(Boolean data){}

        default void handleUserLogOffSuccess(boolean data){}
    }

    interface Presenter extends BasePresenter<IView> {
        void weiboAuthLogin(HashMap<String, Object> params);

        void login(String account, String password, String type,String phone);

        void getUserDetail(String userId);

        void wechatAuthLogin(HashMap<String, Object> params);

        void qqAuthLogin(HashMap<String, Object> params);

        void bindPhone(HashMap<String, Object> params);

        void existPhone(String phone);

        void getSmsCode(String phone, String type);

        void validateCode(String phone, String code, String type);

        void reset(String phone, String code, String newpwd, String confirmpwd);
    }
}
