package com.yanhua.user.presenter.contract;

import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;

import java.util.HashMap;
import java.util.List;

public interface PersonalSettingContract {

    interface IView extends BaseView {
        default void handleUserDetail(UserInfo userInfo) {
        }

        default void handleErrorMessage(String errorMsg) {
        }

        default void updateUserDetailSuccess() {
        }

        default void handleSuccessWeixinUntie() {
        }

        default void handleSuccessQQUntie() {
        }

        default void handleSuccessWeixinRegister() {
        }

        default void handleSuccessQQRegister() {
        }

        default void handleSuccessBindPhone() {
        }

        default void handleSetBackGroundSuccess() {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void handCommonReason(List<CommonReasonModel> data) {
        }

        default void handCommonReason(List<CommonReasonModel> data, int type) {
        }

        default void handleCitySuccess(List<CityModel> list) {
        }

        default void handleUserAloneTags(List<PersonTagModel> list, int type) {
        }

        default void hanldeSetUserAloneTagsSuccess() {
        }

        default void handleUploadRuleSuccess(ProtocolModel model) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void setBackGround(String blackgroundImg);

        /**
         * 获取用户个人信息
         *
         * @param userId
         */
        void getPersonalInfo(String userId);


        /**
         * 修改用户信息
         *
         * @param userId
         * @param params
         */
        void updateUserDetail(String userId, HashMap<String, Object> params);

        /**
         * qq解绑
         */
        void qqUntie();

        /**
         * 微信解绑
         */
        void weixinUntie();

        /**
         * 微信注册
         *
         * @param params
         */
        void weixinRegister(HashMap<String, Object> params);

        /**
         * QQ注册
         *
         * @param params
         */
        void qqRegister(HashMap<String, Object> params);

        /**
         * 绑定手机
         *
         * @param params
         */
        void bindPhone(HashMap<String, Object> params);

        void getUserAloneTags(int type);

        void getStsToken();

        void getCommonReasonList(HashMap<String, Object> params);

        void getCommonReasonList(HashMap<String, Object> params, int type);

        void getAllCity();

        void setUserAloneTags(HashMap<String, Object> params);

        void getUploadRule(String code);
    }

}
