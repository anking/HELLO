package com.yanhua.user.presenter;


import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MessageHttpMethod;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.model.PrivacySettingModel;
import com.yanhua.user.presenter.contract.SettingContract;

import java.util.HashMap;
import java.util.List;

public class SettingPresenter extends MvpPresenter<SettingContract.IView> implements SettingContract.Presenter {

    public void addOrCancelBlack(String userId) {
        addSubscribe(MessageHttpMethod.getInstance().addBlack(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateBlackUserSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, userId));
    }


    public void getBlackList(String userName, int current, int size) {
        addSubscribe(MessageHttpMethod.getInstance().getBlackList(new SimpleSubscriber<HttpResult<ListResult<BlackMemberBean>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BlackMemberBean>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<BlackMemberBean> listResult = result.getData();
                        baseView.handleBlackListSuccess(listResult.getRecords());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userName, current, size));
    }

    public void getAppProtocol(String code) {
        addSubscribe(CommonHttpMethod.getInstance().getAppProtocol(new SimpleSubscriber<HttpResult<ProtocolModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ProtocolModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleAppProtocol(code,httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, code));
    }

    public void getContactUs() {
        addSubscribe(CommonHttpMethod.getInstance().getContactUs(new SimpleSubscriber<HttpResult<List<ServerConfigModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<ServerConfigModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleServerConfigList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }));
    }

    public void getContactCS() {
        addSubscribe(CommonHttpMethod.getInstance().getContactCS(new SimpleSubscriber<HttpResult<List<ServerConfigModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<ServerConfigModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleServerConfigList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }));
    }

    @Override
    public void fetchAppVersionInfo(int type) {
        addSubscribe(UserHttpMethod.getInstance().fetchAppVersionInfo(new SimpleSubscriber<HttpResult<UpgradeInfoModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<UpgradeInfoModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessAppVersionInfo(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, 0));
    }

    @Override
    public void getUserDetail(String userId) {
        addSubscribe(UserHttpMethod.getInstance().getUserDetail(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        UserInfo userInfo = bean.getData();
                        if (userInfo != null) {
                            baseView.handleUserDetail(userInfo);
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void getPrivacySetting() {
        addSubscribe(UserHttpMethod.getInstance().getPrivacySetting(new SimpleSubscriber<HttpResult<PrivacySettingModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<PrivacySettingModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessGetPrivacySetting(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }));
    }

    @Override
    public void updatePrivacySetting(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().updatePrivacySetting(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessUpdatePrivacySetting();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }


    public void updateChat2MeRight(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().updateChat2MeRight(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessUpdatePrivacySetting();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }
}
