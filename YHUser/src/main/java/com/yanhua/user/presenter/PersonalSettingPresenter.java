package com.yanhua.user.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.api.UserHttpMethod;
import com.yanhua.user.presenter.contract.PersonalSettingContract;

import java.util.HashMap;
import java.util.List;

public class PersonalSettingPresenter extends MvpPresenter<PersonalSettingContract.IView> implements PersonalSettingContract.Presenter {

    @Override
    public void setBackGround(String blackgroundImg) {
        addSubscribe(UserHttpMethod.getInstance().setBackGround(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSetBackGroundSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, blackgroundImg));
    }

    @Override
    public void getPersonalInfo(String userId) {
        addSubscribe(UserHttpMethod.getInstance().getPersonalInfo(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    UserInfo userInfo = bean.getData();
                    if (userInfo != null) {
                        baseView.handleUserDetail(userInfo);
                    }
                } else {
                    baseView.handleErrorMessage(bean.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void updateUserDetail(String userId, HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().putPersonalInfo(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult bean) {
                super.onNext(bean);
                String code = bean.getCode();
                if (code.equals(HttpCode.SUCCESS)) {
                    baseView.updateUserDetailSuccess();
                } else {
                    baseView.handleErrorMessage(bean.getMesg());
                }
            }
        }, userId, params));
    }

    @Override
    public void qqUntie() {
        addSubscribe(UserHttpMethod.getInstance().qqUntie(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleSuccessQQUntie();
                        } else {
                            baseView.handleErrorMessage("解绑失败");
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void weixinUntie() {
        addSubscribe(UserHttpMethod.getInstance().weixinUntie(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    if (result.getData() instanceof Boolean) {
                        boolean data = (boolean) result.getData();
                        if (data) {
                            baseView.handleSuccessWeixinUntie();
                        } else {
                            baseView.handleErrorMessage("解绑失败");
                        }
                    }
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void weixinRegister(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().weixinRegister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessWeixinRegister();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void qqRegister(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().qqRegister(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessQQRegister();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void bindPhone(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().bindPhone(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleSuccessBindPhone();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getUserAloneTags(int type) {
        addSubscribe(UserHttpMethod.getInstance().getUserAloneTags(new SimpleSubscriber<HttpResult<List<PersonTagModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<List<PersonTagModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleUserAloneTags(httpResult.getData(), type);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, type));
    }

    @Override
    public void setUserAloneTags(HashMap<String, Object> params) {
        addSubscribe(UserHttpMethod.getInstance().setUserAloneTags(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.hanldeSetUserAloneTagsSuccess();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UserHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    public void getCommonReasonList(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handCommonReason(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void getCommonReasonList(HashMap<String, Object> params, int type) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handCommonReason(httpResult.getData(), type);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getAllCity() {
        addSubscribe(CommonHttpMethod.getInstance().getAllAreas(new SimpleSubscriber<HttpResult<List<CityModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CityModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleCitySuccess(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }));
    }

    @Override
    public void getUploadRule(String code) {
        addSubscribe(CommonHttpMethod.getInstance().getAppExplain(new SimpleSubscriber<HttpResult<ProtocolModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ProtocolModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleUploadRuleSuccess(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, code));
    }
}
