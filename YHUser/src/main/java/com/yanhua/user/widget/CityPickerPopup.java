package com.yanhua.user.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.contrarywind.view.WheelView;
import com.google.gson.Gson;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.lxj.xpopupext.bean.JsonBean;
import com.yanhua.common.model.CityModel;
import com.yanhua.user.R;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CityPickerPopup extends BottomPopupView {

    private String title;
    private List<String> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private CityPickerListener cityPickerListener;
    private WheelOptions wheelOptions;
    public int dividerColor = 0xFFd5d5d5; //分割线的颜色
    public float lineSpace = 2.4f; // 条目间距倍数 默认2
    public int textColorOut = 0xFFB4B8BC; //分割线以外的文字颜色
    public int textColorCenter = 0xFF36384A; //分割线之间的文字颜色

    public CityPickerPopup(@NonNull Context context, String title) {
        super(context);
        this.title = title;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_city_picker;
    }

    TextView btnCancel, btnConfirm, tvTitle;

    @Override
    protected void onCreate() {
        super.onCreate();
        tvTitle = findViewById(R.id.tvTitle);
        btnCancel = findViewById(R.id.btnCancel);
        btnConfirm = findViewById(R.id.btnConfirm);
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
        btnCancel.setOnClickListener(v -> dismiss());
        btnConfirm.setTextColor(XPopup.getPrimaryColor());
        btnConfirm.setOnClickListener(v -> {
            if (cityPickerListener != null) {
                int[] optionsCurrentItems = wheelOptions.getCurrentItems();
                int options1 = optionsCurrentItems[0];
                int options2 = optionsCurrentItems[1];
                cityPickerListener.onCityConfirm(options1Items.get(options1), options2Items.get(options1).get(options2));
            }
            dismiss();
        });

        wheelOptions = new WheelOptions(findViewById(R.id.citypicker), false);
        wheelOptions.setTextContentSize(20);
        wheelOptions.setItemsVisible(5);
        wheelOptions.setAlphaGradient(true);
        wheelOptions.setCyclic(false);

        wheelOptions.setDividerColor(popupInfo.isDarkTheme ? Color.parseColor("#444444") : dividerColor);
        wheelOptions.setDividerType(WheelView.DividerType.FILL);
        wheelOptions.setLineSpacingMultiplier(lineSpace);
        wheelOptions.setTextColorOut(textColorOut);
        wheelOptions.setTextColorCenter(popupInfo.isDarkTheme ? Color.parseColor("#CCCCCC") : textColorCenter);
        wheelOptions.isCenterLabel(false);

        if (!options1Items.isEmpty() && !options2Items.isEmpty()) {
            //有数据直接显示
            if (wheelOptions != null) {
                wheelOptions.setPicker(options1Items, options2Items);
                wheelOptions.setCurrentItems(0, 0);
            }
        } else {
            initJsonData();
        }
    }

    @Override
    protected void applyDarkTheme() {
        super.applyDarkTheme();
        btnCancel.setTextColor(Color.parseColor("#999999"));
        btnConfirm.setTextColor(Color.parseColor("#ffffff"));
        getPopupImplView().setBackground(XPopupUtils.createDrawable(getResources().getColor(R.color._xpopup_dark_color),
                popupInfo.borderRadius, popupInfo.borderRadius, 0, 0));
    }

    @Override
    protected void applyLightTheme() {
        super.applyLightTheme();
        btnCancel.setTextColor(Color.parseColor("#666666"));
        btnConfirm.setTextColor(Color.parseColor("#222222"));
        getPopupImplView().setBackground(XPopupUtils.createDrawable(getResources().getColor(R.color._xpopup_light_color),
                popupInfo.borderRadius, popupInfo.borderRadius, 0, 0));
    }

    public CityPickerPopup setCityPickerListener(CityPickerListener listener) {
        this.cityPickerListener = listener;
        return this;
    }

    private void initJsonData() {
        for (CityModel c : data) {
            options1Items.add(c.getAreaName());
            List<CityModel> subCity = c.getSubCity();
            ArrayList<String> cityList = new ArrayList<>();
            for (CityModel cc : subCity) {
                cityList.add(cc.getAreaName());
            }
            options2Items.add(cityList);
        }
        wheelOptions.setPicker(options1Items, options2Items);
        if (!TextUtils.isEmpty(defaultValue)) {
            String[] arr = defaultValue.split(",");
            if (arr != null && arr.length > 1) {
                int i = options1Items.indexOf(arr[0]);
                int j = options2Items.get(i).indexOf(arr[1]);
                wheelOptions.setCurrentItems(i, j);
            } else {
                wheelOptions.setCurrentItems(0, 0);
            }
        } else {
            wheelOptions.setCurrentItems(0, 0);
        }
    }

    private List<CityModel> data;

    public CityPickerPopup setPickerData(List<CityModel> list) {
        this.data = list;
        return this;
    }

    private String defaultValue;

    public CityPickerPopup setDefaultValue(String value) {
        this.defaultValue = value;
        return this;
    }
}
