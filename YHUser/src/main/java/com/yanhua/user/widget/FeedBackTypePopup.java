package com.yanhua.user.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.core.BottomPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.R2;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;

import java.util.List;

import butterknife.BindView;

public class FeedBackTypePopup extends BottomPopupView {

    private Context context;
    private CommonReasonModel selectFeedbackType;
    private List<CommonReasonModel> list;

    public FeedBackTypePopup(@NonNull Context context, CommonReasonModel selectFeedbackType, List<CommonReasonModel> list) {
        super(context);
        this.context = context;
        this.selectFeedbackType = selectFeedbackType;
        this.list = list;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_feedback_type;
    }

    public interface OnItemClickListener {
        void onItemClick(CommonReasonModel bean);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.tvCancel).setOnClickListener(v -> dismiss());
        RecyclerView rvType = findViewById(R.id.rv_feedback_type);
        rvType.setLayoutManager(new LinearLayoutManager(context));
        FeedbackTypeAdapter adapter = new FeedbackTypeAdapter(context);
        rvType.setAdapter(adapter);
        adapter.setItems(list);
        adapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                listener.onItemClick(list.get(pos));
                dismiss();
            }
        });
    }


    class FeedbackTypeAdapter extends BaseRecyclerAdapter<CommonReasonModel, ViewHolder> {

        public FeedbackTypeAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new ViewHolder(inflater.inflate(R.layout.item_report, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommonReasonModel item) {
            String feedbackType = item.getContent();
            holder.tvTitle.setText(!TextUtils.isEmpty(feedbackType) ? feedbackType : "");
            if (selectFeedbackType != null && selectFeedbackType.getId().equals(item.getId())) {
                holder.iconNext.setVisibility(VISIBLE);
                holder.iconNext.setText("\ue732");
                holder.iconNext.setTextColor(ContextCompat.getColor(context, R.color.theme));
            } else {
                holder.iconNext.setVisibility(GONE);
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_title)
        TextView tvTitle;
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
