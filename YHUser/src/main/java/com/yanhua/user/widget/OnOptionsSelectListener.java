package com.yanhua.user.widget;

public interface OnOptionsSelectListener {

    void onOptionsSelect(int options1, int options2);

}