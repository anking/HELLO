package com.yanhua.user.widget;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.user.R;

public class SetRemarkPopup extends CenterPopupView {

    public SetRemarkPopup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_set_remark;
    }

    private OnSetRemarkListener listener;

    public interface OnSetRemarkListener {
        void setRemark(String remark);
    }

    public void setOnSetRemarkListener(OnSetRemarkListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        AutoClearEditText editText = findViewById(R.id.ed_content);
        findViewById(R.id.btn_cancel).setOnClickListener(v -> dismiss());
        findViewById(R.id.btn_sure).setOnClickListener(v -> {
            String content = editText.getText().toString().trim();
            if (listener != null && !TextUtils.isEmpty(content)) {
                listener.setRemark(content);
                dismiss();
            }
        });
    }

}
