package com.yanhua.user.widget;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.user.R;

public class PrivateProtocolPopup extends CenterPopupView {

    public final static int AGREE = 1;
    public final static int DISAGREE = 2;
    public final static int USER = 3;
    public final static int PRIVASTE = 4;

    private Context mContext;

    public PrivateProtocolPopup(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_private_protocol;
    }

    private OnOperateClickListener operateClickListener;

    public interface OnOperateClickListener {
        void clickButton(int type);// 1同意  2不同意
    }

    public void setOnOperateClickListener(OnOperateClickListener operateClickListener) {
        this.operateClickListener = operateClickListener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        TextView tv_private_protocol = findViewById(R.id.tv_private_protocol);
        TextView tv_disagree = findViewById(R.id.tv_disagree);
        TextView tv_agree = findViewById(R.id.tv_agree);

        tv_disagree.setOnClickListener(v -> {
            // 直接退出app, 关闭
            if (operateClickListener != null) {
                operateClickListener.clickButton(DISAGREE);
            }
        });

        tv_agree.setOnClickListener(v -> {
            if (operateClickListener != null) {
                dismiss();
                operateClickListener.clickButton(AGREE);
            }
        });

        String textContent = "您可以阅读完整版用户协议和隐私政策";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                operateClickListener.clickButton(USER);
            }
        }, 8, 12, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                operateClickListener.clickButton(PRIVASTE);
            }
        }, 13, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_private_protocol.setText(spannableString);
        tv_private_protocol.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected int getMaxHeight() {
        return (int) (DisplayUtils.getScreenHeight(mContext) * 0.5);
    }
}
