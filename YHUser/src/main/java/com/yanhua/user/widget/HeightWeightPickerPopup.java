package com.yanhua.user.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.lxj.xpopupext.adapter.ArrayWheelAdapter;
import com.yanhua.user.R;

import java.util.ArrayList;
import java.util.List;

public class HeightWeightPickerPopup extends BottomPopupView {

    private String title;
    private int itemsVisibleCount = 5;
    private int itemTextSize = 20;
    public int dividerColor = 0xFFd5d5d5; //分割线的颜色
    public float lineSpace = 2.4f; // 条目间距倍数 默认2
    public int textColorOut = 0xFFB4B8BC; //分割线以外的文字颜色
    public int textColorCenter = 0xFF36384A; //分割线之间的文字颜色
    private WheelView wheelView;

    public HeightWeightPickerPopup(@NonNull Context context, String title) {
        super(context);
        this.title = title;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_height_weight_picker;
    }

    TextView btnCancel, btnConfirm, tvTitle;

    @Override
    protected void onCreate() {
        super.onCreate();
        tvTitle = findViewById(R.id.tvTitle);
        btnCancel = findViewById(R.id.btnCancel);
        btnConfirm = findViewById(R.id.btnConfirm);
        wheelView = findViewById(R.id.commonWheel);
        btnCancel.setOnClickListener(v -> dismiss());
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        }
        btnConfirm.setOnClickListener(v -> {
            int index = wheelView.getCurrentItem();
            if (pickerListener != null)
                pickerListener.onItemSelected(index);
            dismiss();
        });
        initWheelData();
    }

    private void initWheelData() {
        wheelView.setItemsVisibleCount(itemsVisibleCount);
        wheelView.setAlphaGradient(true);
        wheelView.setTextSize(itemTextSize);
        wheelView.setCyclic(false);
        wheelView.setDividerColor(popupInfo.isDarkTheme ? Color.parseColor("#444444") : dividerColor);
        wheelView.setDividerType(WheelView.DividerType.FILL);
        wheelView.setLineSpacingMultiplier(lineSpace);
        wheelView.setTextColorOut(textColorOut);
        wheelView.setTextColorCenter(popupInfo.isDarkTheme ? Color.parseColor("#CCCCCC") : textColorCenter);
        wheelView.isCenterLabel(true);
        wheelView.setTypeface(Typeface.DEFAULT);
        wheelView.setLabel(label);
        wheelView.setCurrentItem(currentItem);
        wheelView.setAdapter(new ArrayWheelAdapter<>(list));
    }

    @Override
    protected void applyDarkTheme() {
        super.applyDarkTheme();
        btnCancel.setTextColor(Color.parseColor("#999999"));
        btnConfirm.setTextColor(Color.parseColor("#ffffff"));
        getPopupImplView().setBackground(XPopupUtils.createDrawable(getResources().getColor(R.color._xpopup_dark_color),
                popupInfo.borderRadius, popupInfo.borderRadius, 0, 0));
    }

    @Override
    protected void applyLightTheme() {
        super.applyLightTheme();
        btnCancel.setTextColor(Color.parseColor("#666666"));
        btnConfirm.setTextColor(Color.parseColor("#222222"));
        getPopupImplView().setBackground(XPopupUtils.createDrawable(getResources().getColor(R.color._xpopup_light_color),
                popupInfo.borderRadius, popupInfo.borderRadius, 0, 0));
    }

    private PickerListener pickerListener;

    public HeightWeightPickerPopup setPickerListener(PickerListener pickerListener) {
        this.pickerListener = pickerListener;
        return this;
    }

    public HeightWeightPickerPopup setItemTextSize(int textSize) {
        this.itemTextSize = textSize;
        return this;
    }

    public HeightWeightPickerPopup setItemsVisibleCount(int itemsVisibleCount) {
        this.itemsVisibleCount = itemsVisibleCount;
        return this;
    }

    public HeightWeightPickerPopup setLineSpace(float lineSpace) {
        this.lineSpace = lineSpace;
        return this;
    }

    List<String> list = new ArrayList<>();

    /**
     * 设置选项数据
     */
    public HeightWeightPickerPopup setPickerData(List<String> list) {
        this.list = list;
        return this;
    }

    int currentItem = 0;

    /**
     * 设置默认选中
     */
    public HeightWeightPickerPopup setCurrentItem(int currentItem) {
        this.currentItem = currentItem;
        return this;
    }

    String label = "";

    public HeightWeightPickerPopup setLabel(String label) {
        this.label = label;
        return this;
    }

    public interface PickerListener {
        void onItemSelected(int index);
    }

}