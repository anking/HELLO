package com.yanhua.user.widget;

import android.view.View;

public interface CityPickerListener {

    void onCityConfirm(String province, String city);

}