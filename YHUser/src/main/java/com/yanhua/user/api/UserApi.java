package com.yanhua.user.api;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.common.Api;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.model.FaceResultModel;
import com.yanhua.user.model.IDCradInfo;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.model.PrivacySettingModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface UserApi {

    @POST(Api.user.USER_LOGIN)
    Flowable<HttpResult<LoginResult>> doLogin(@QueryMap HashMap<String, Object> params);

    @GET(Api.app.USER_INFO_DETAIL)
    Flowable<HttpResult<UserInfo>> getUserDetail(@Path("id") String userId);


    @POST(Api.user.WEIBO_REGISTER)
    Flowable<HttpResult> weiboAuthLogin(@Body RequestBody body);

    @POST(Api.user.WEIXIN_REGISTER)
    Flowable<HttpResult> wechatAuthLogin(@Body RequestBody body);

    @POST(Api.user.QQ_REGISTER)
    Flowable<HttpResult> qqAuthLogin(@Body RequestBody body);

    @POST(Api.user.BIND_PHONE)
    Flowable<HttpResult> bindPhone(@Body RequestBody body);

    @GET(Api.user.EXIST_PHONE)
    Flowable<HttpResult> existPhone(@Path("phone") String phone);

    @GET(Api.user.GET_SMS_CODE)
    Flowable<HttpResult> getSmsCode(@Query("phone") String phone, @Query("type") String type);

    @GET(Api.user.VALIDATE_CODE)
    Flowable<HttpResult> validateCode(@Query("phone") String phone, @Query("code") String code, @Query("type") String type);

    @PUT(Api.user.MODIFY_USER_PASSWORD)
    Flowable<HttpResult> modifyUserPassword(@Query("id") String id, @Body RequestBody body);

    @GET(Api.user.GET_PRIVACY_SETTING)
    Flowable<HttpResult<PrivacySettingModel>> getPrivacySetting();

    @POST(Api.user.UPDATE_CHAT2ME_RIGHT)
    Flowable<HttpResult> updateChat2MeRight(@Body RequestBody body);

    @POST(Api.user.UPDATE_PRIVACY_SETTING)
    Flowable<HttpResult> updatePrivacySetting(@Body RequestBody body);

    @GET(Api.user.FETCH_APP_VERSION_INFO)
    Flowable<HttpResult<UpgradeInfoModel>> fetchAppVersionInfo(@Query("type") int type);

    @POST(Api.user.VISIT_RRCORD)
    Flowable<HttpResult> visitRecord(@Body RequestBody body);

    @GET(Api.app.USER_INFO_DETAIL)
    Flowable<HttpResult<UserInfo>> getPersonalInfo(@Path("id") String userId);

    @PUT(Api.user.WEIBO_UNTIE)
    Flowable<HttpResult> weiboUntie();

    @PUT(Api.user.QQ_UNTIE)
    Flowable<HttpResult> qqUntie();

    @PUT(Api.user.WEIXIN_UNTIE)
    Flowable<HttpResult> weixinUntie();

    @PUT(Api.app.USER_INFO_DETAIL)
    Flowable<HttpResult> putPersonalInfo(@Path("id") String userId, @Body RequestBody body);

    @PUT(Api.user.PERSON_INFO_SET_BACKGROUND)
    Flowable<HttpResult> setBackGround(@Query("blackgroundImg") String blackgroundImg);

    @GET(Api.user.GET_STS_TOKEN)
    Flowable<HttpResult<StsTokenModel>> getStsToken();

    @POST(Api.user.GET_FACE_ID)
    Flowable<HttpResult<FaceResultModel>> getFaceId(@Body RequestBody body);

    @POST(Api.user.IDCARD_VERIFICATION)
    Flowable<HttpResult<String>> idCardVerification(@Body RequestBody body);

    @POST(Api.user.CHECK_IDCARD_INFORMATION)
    Flowable<HttpResult<IDCradInfo>> checkIdCardInformation(@Body RequestBody body);

    @GET(Api.user.GET_USER_ALONE_TAG)
    Flowable<HttpResult<List<PersonTagModel>>> getUserAloneTags(@Path("type") int type);

    @POST(Api.user.SET_USER_ALONE_TAG)
    Flowable<HttpResult> setUserAloneTags(@Body RequestBody body);

    @POST(Api.app.FEED_BACK)
    Flowable<HttpResult> feedback(@Body RequestBody body);

    @POST(Api.app.GET_PROBLEM_LIST)
    Flowable<HttpResult<ListResult<CommonProblemBean>>> getProblemList(@Body RequestBody body);
}