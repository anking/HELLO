package com.yanhua.user.api;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.net.ApiServiceFactory;
import com.yanhua.base.net.BaseRtHttpMethod;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.user.model.FaceResultModel;
import com.yanhua.user.model.IDCradInfo;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.model.PrivacySettingModel;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;

public class UserHttpMethod extends BaseRtHttpMethod {

    UserApi api;

    public UserHttpMethod() {
        this.api = ApiServiceFactory.createAPIService(UserApi.class);
    }

    //获取单例
    public static UserHttpMethod getInstance() {
        return HttpMethodHolder.INSTANCE;
    }

    private static class HttpMethodHolder {
        private static final UserHttpMethod INSTANCE = new UserHttpMethod();
    }

    //--------------------------------------------------------------------------------------------
    // 登录
    public SimpleSubscriber login(SimpleSubscriber<HttpResult<LoginResult>> subscriber, HashMap<String, Object> params) {
        tocompose(api.doLogin(params), subscriber);
        return subscriber;
    }

    // 查询会员详情
    public SimpleSubscriber getUserDetail(SimpleSubscriber<HttpResult<UserInfo>> subscriber, String userId) {
        tocompose(api.getUserDetail(userId), subscriber);
        return subscriber;
    }

    // 更新会员详情
//    public SimpleSubscriber updateUserInfo(SimpleSubscriber<HttpResult> subscriber, String userId, HashMap<String, Object> params) {
//        tocompose(api.updateUserInfo(userId, convertToJson(params)), subscriber);
//        return subscriber;
//    }


    // 微博授权注册
    public SimpleSubscriber weiboRegister(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.weiboAuthLogin(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    // 微信授权注册
    public SimpleSubscriber weixinRegister(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.wechatAuthLogin(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    // QQ授权注册
    public SimpleSubscriber qqRegister(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.qqAuthLogin(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    // 绑定手机号
    public SimpleSubscriber bindPhone(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> paramsMap) {
        tocompose(api.bindPhone(convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    // 判断手机号是否存在
    public SimpleSubscriber existPhone(SimpleSubscriber<HttpResult> subscriber, String phone) {
        tocompose(api.existPhone(phone), subscriber);
        return subscriber;
    }


    // 获取验证码
    public SimpleSubscriber getSmsCode(SimpleSubscriber<HttpResult> subscriber, String phone, String type) {
        tocompose(api.getSmsCode(phone, type), subscriber);
        return subscriber;
    }

    // 验证码校验
    public SimpleSubscriber validateCode(SimpleSubscriber<HttpResult> subscriber, String phone, String code, String type) {
        tocompose(api.validateCode(phone, code, type), subscriber);
        return subscriber;
    }

    // 修改用户密码
    public SimpleSubscriber modifyUserPassword(SimpleSubscriber<HttpResult> subscriber, String id, HashMap<String, Object> paramsMap) {
        tocompose(api.modifyUserPassword(id, convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    //---------------------------设置部分-----------------
    public SimpleSubscriber fetchAppVersionInfo(SimpleSubscriber<HttpResult<UpgradeInfoModel>> subscriber, int type) {
        tocompose(api.fetchAppVersionInfo(type), subscriber);
        return subscriber;
    }
    public SimpleSubscriber visitRecord(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.visitRecord(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getPrivacySetting(SimpleSubscriber<HttpResult<PrivacySettingModel>> subscriber) {
        tocompose(api.getPrivacySetting(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber updateChat2MeRight(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.updateChat2MeRight(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber updatePrivacySetting(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.updatePrivacySetting(convertToJson(params)), subscriber);
        return subscriber;
    }

    // 查询会员个人信息
    public SimpleSubscriber getPersonalInfo(SimpleSubscriber<HttpResult<UserInfo>> subscriber, String userId) {
        tocompose(api.getPersonalInfo(userId), subscriber);
        return subscriber;
    }

    public SimpleSubscriber weixinUntie(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.weixinUntie(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber weiboUntie(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.weiboUntie(), subscriber);
        return subscriber;
    }

    public SimpleSubscriber qqUntie(SimpleSubscriber<HttpResult> subscriber) {
        tocompose(api.qqUntie(), subscriber);
        return subscriber;
    }

    // 修改会员个人信息
    public SimpleSubscriber putPersonalInfo(SimpleSubscriber<HttpResult> subscriber, String userId, HashMap<String, Object> paramsMap) {
        tocompose(api.putPersonalInfo(userId, convertToJson(paramsMap)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber setBackGround(SimpleSubscriber<HttpResult> subscriber, String backGroupUrl) {
        tocompose(api.setBackGround(backGroupUrl), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getStsToken(SimpleSubscriber<HttpResult<StsTokenModel>> subscriber) {
        tocompose(api.getStsToken(), subscriber);
        return subscriber;
    }

    //身份认证
    public SimpleSubscriber getFaceId(SimpleSubscriber<HttpResult<FaceResultModel>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getFaceId(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber idCardVerification(SimpleSubscriber<HttpResult<String>> subscriber, HashMap<String, Object> params) {
        tocompose(api.idCardVerification(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber checkIdCardInformation(SimpleSubscriber<HttpResult<IDCradInfo>> subscriber, HashMap<String, Object> params) {
        tocompose(api.checkIdCardInformation(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getUserAloneTags(SimpleSubscriber<HttpResult<List<PersonTagModel>>> subscriber, int type) {
        tocompose(api.getUserAloneTags(type), subscriber);
        return subscriber;
    }

    public SimpleSubscriber setUserAloneTags(SimpleSubscriber<HttpResult> subscriber, HashMap<String, Object> params) {
        tocompose(api.setUserAloneTags(convertToJson(params)), subscriber);
        return subscriber;
    }

    public SimpleSubscriber feedback(SimpleSubscriber<HttpResult> subscriber, RequestBody body) {
        tocompose(api.feedback(body), subscriber);
        return subscriber;
    }

    public SimpleSubscriber getProblemList(SimpleSubscriber<HttpResult<ListResult<CommonProblemBean>>> subscriber, HashMap<String, Object> params) {
        tocompose(api.getProblemList(convertToJson(params)), subscriber);
        return subscriber;
    }
}