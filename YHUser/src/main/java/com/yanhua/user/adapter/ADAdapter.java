package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;


/**
 * @author Administrator
 */
public class ADAdapter extends BannerAdapter<AdvertiseModel, ADAdapter.WelcomeHolder> {

    private Context mContext;

    public ADAdapter(List<AdvertiseModel> datas, Context context) {
        super(datas);

        mContext = context;
    }

    @Override
    public WelcomeHolder onCreateHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad, parent, false);
        return new WelcomeHolder(itemView);
    }

    @Override
    public void onBindView(WelcomeHolder holder, AdvertiseModel model, int position, int size) {
        String imgUrl = model.getImageUrl();
        String videoCoverUrl = model.getVideoCoverUrl();
        String showUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

        if (showUrl.endsWith("gif") || showUrl.endsWith("GIF")) {//如果是gif圖的話就走這裡
            Glide.with(mContext).asGif().load(showUrl).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(holder.ivAD);
        } else {
            ImageLoaderUtil.loadStartPageAdImg(holder.ivAD, showUrl);
        }
    }

    static class WelcomeHolder extends RecyclerView.ViewHolder {

        private ImageView ivAD;

        public WelcomeHolder(@NonNull View itemView) {
            super(itemView);
            ivAD = itemView.findViewById(R.id.iv_ad);
        }
    }
}
