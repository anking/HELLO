package com.yanhua.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

/**
 * 用户列表
 *
 * @author Administrator
 */
public class FriendItemAdapter extends BaseRecyclerAdapter<FriendModel, FriendItemAdapter.ViewHolder> {

    private Context mContext;
    private boolean isAddressBook;


    public FriendItemAdapter(Context context, boolean addressBook) {
        super(context);
        mContext = context;
        isAddressBook = addressBook;
    }

    private OnItemCellClickListener listener;

    public interface OnItemCellClickListener {
        /**
         * @param pos
         * @param cellType 0为点击用户相关 1为关注相关
         */
        void onItemCellClick(int pos, int cellType);
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_friend, parent, false);
        return new ViewHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, @NonNull final FriendModel model) {
        int position = getPosition(holder);

        String userPhoto = model.getUserPhoto();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.iv_header, userPhoto, R.drawable.place_holder);
        } else {
            holder.iv_header.setImageResource(R.drawable.place_holder);
        }

        holder.tv_name.setText(!TextUtils.isEmpty(nickName) ? nickName : "");

        holder.itemContent.setOnClickListener(view -> {
            if (listener != null) {
                //点击头像处理
                listener.onItemCellClick(position, 0);
            }
        });

        if (isAddressBook) {
            String str = "";
            //得到当前字母
            String currentLetter = model.getFirstLetter();
            if (position == 0) {
                str = currentLetter;
            } else {
                //得到上一个字母
                String preLetter = mDataList.get(position - 1).getFirstLetter();
                //如果和上一个字母的首字母不同则显示字母栏
                if (!preLetter.equalsIgnoreCase(currentLetter)) {
                    str = currentLetter;
                }
            }

            //根据str是否为空决定字母栏是否显示
            if (TextUtils.isEmpty(str)) {
                holder.tvIndex.setVisibility(View.GONE);
            } else {
                holder.tvIndex.setVisibility(View.VISIBLE);
                holder.tvIndex.setText(str.toUpperCase());//转大写
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_header)
        CircleImageView iv_header;
        @BindView(R2.id.tv_name)
        TextView tv_name;

        @BindView(R2.id.tvIndex)
        TextView tvIndex;

        @BindView(R2.id.ll_item)
        RelativeLayout itemContent;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}

