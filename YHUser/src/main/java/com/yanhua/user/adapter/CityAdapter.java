package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.common.model.CityModel;

import butterknife.BindView;

public class CityAdapter extends BaseRecyclerAdapter<CityModel, CityAdapter.ViewHolder> {

    public CityAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_city, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CityModel item) {
        holder.tvCity.setText(item.getAreaName());
    }

    public int getSortLettersFirstPosition(String letters) {
        if (mDataList == null || mDataList.isEmpty()) {
            return -1;
        }
        int position = -1;
        for (int index = 0; index < mDataList.size(); index++) {
            if (mDataList.get(index).getFirst().equals(letters)) {
                position = index + 1;
                break;
            }
        }
        return position;
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tv_city)
        TextView tvCity;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
