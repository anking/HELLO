package com.yanhua.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

/**
 * 用户列表
 *
 * @author Administrator
 */
public class UserItemAdapter extends BaseRecyclerAdapter<FriendModel, UserItemAdapter.ViewHolder> {

    private Context mContext;
    private int mType;

    public UserItemAdapter(Context context) {
        super(context);
        mContext = context;
    }

    public UserItemAdapter(Context context, int type) {
        super(context);
        mContext = context;
        mType = type;
    }

    private OnItemCellClickListener listener;

    public interface OnItemCellClickListener {
        /**
         * @param pos
         * @param cellType 0为点击用户相关 1为关注相关
         */
        void onItemCellClick(int pos, int cellType);
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_user, parent, false);
        return new ViewHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, @NonNull final FriendModel model) {
        int position = getPosition(holder);

        String userPhoto = model.getUserPhoto();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());
        String inviteCode ="test";

        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.iv_header, userPhoto, R.drawable.place_holder);
        } else {
            holder.iv_header.setImageResource(R.drawable.place_holder);
        }

        if (TextUtils.isEmpty(inviteCode)) {
            holder.tvSign.setVisibility(View.GONE);
        } else {
            holder.tvSign.setVisibility(View.VISIBLE);
            holder.tvSign.setText(String.format("签名:%s", inviteCode));
        }

        holder.tv_name.setText(!TextUtils.isEmpty(nickName) ? nickName : "");

        holder.iv_header.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 0);
            }
        });
        holder.ll_item.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 0);
            }
        });

//       关注状态 1 未关注 2 已关注 3 被关注 4互相关注
        int followStatus = model.getFollowStatus();

        if (mType == 2) {
            followStatus = 4;
        }

        holder.btn_follow.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 1);
            }
        });

        String userID = UserManager.getInstance().getUserId();
        if (!TextUtils.isEmpty(userID) && userID.equals(model.getUserId())) {
            //
            holder.btn_follow.setVisibility(View.GONE);
        } else {
            holder.btn_follow.setVisibility(View.VISIBLE);
        }
        switch (followStatus) {
            case 1:
                holder.btn_follow.setStatus(false,"关注");
                break;
            case 2:

                holder.btn_follow.setStatus(true,"已关注");
                break;
            case 3:
                holder.btn_follow.setStatus(false,"回粉");
                break;
            case 4:
                holder.btn_follow.setStatus(true,"互相关注");
                break;
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_header)
        CircleImageView iv_header;
        @BindView(R2.id.tv_name)
        TextView tv_name;
        @BindView(R2.id.tvSign)
        TextView tvSign;
        @BindView(R2.id.ll_item)
        RelativeLayout ll_item;
        @BindView(R2.id.btn_follow)
        StatusView btn_follow;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}

