package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

public class ContactUsAdapter extends BaseRecyclerAdapter<ServerConfigModel, ContactUsAdapter.ViewHolder> {

    private Context mContext;

    public ContactUsAdapter(Context context) {
        super(context);
        mContext = context;
    }

    private OnItemCellClickListener listener;

    public interface OnItemCellClickListener {
        /**
         * @param pos
         * @param cellType 0为点击用户相关 1为关注相关
         */
        void onItemCellClick(int pos, int cellType);
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_contact_us, parent, false);
        return new ViewHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, @NonNull final ServerConfigModel model) {
        int position = getPosition(holder);

        holder.viewDivide.setVisibility(position == 0 ? View.GONE : View.VISIBLE);

        holder.tvKey.setText(YHStringUtils.value(model.getKey()));
        holder.tvValue.setText(YHStringUtils.value(model.getValue()));

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.viewDivide)
        View viewDivide;
        @BindView(R2.id.tvKey)
        TextView tvKey;

        @BindView(R2.id.tvValue)
        TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}

