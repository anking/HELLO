package com.yanhua.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

public class CommonProblemAdapter extends BaseRecyclerAdapter<CommonProblemBean, CommonProblemAdapter.CommonProblemViewHolder> {

    public CommonProblemAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected CommonProblemViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new CommonProblemViewHolder(inflater.inflate(R.layout.item_common_problem, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull CommonProblemViewHolder holder, @NonNull CommonProblemBean item) {
        String title = item.getTitle();
        holder.tvTitle.setText(!TextUtils.isEmpty(title) ? title : "");
    }

    static class CommonProblemViewHolder extends BaseViewHolder {
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;
        @BindView(R2.id.tv_title)
        TextView tvTitle;

        public CommonProblemViewHolder(View itemView) {
            super(itemView);
        }
    }
}