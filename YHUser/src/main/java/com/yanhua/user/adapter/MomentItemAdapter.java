package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MomentModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

public class MomentItemAdapter extends BaseRecyclerAdapter<MomentModel, MomentItemAdapter.ViewHolder> {

    public MomentItemAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_moment_image, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentModel item) {
        holder.ivClose.setVisibility(View.GONE);
        ImageLoaderUtil.loadImg(holder.ivIcon, item.getContentUrl());
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_icon)
        ShapeableImageView ivIcon;
        @BindView(R2.id.iv_close)
        AliIconFontTextView ivClose;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
