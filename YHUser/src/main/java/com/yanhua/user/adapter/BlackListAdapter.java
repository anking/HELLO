package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

/**
 * 通讯录列表
 *
 * @author Administrator
 */
public class BlackListAdapter extends BaseRecyclerAdapter<BlackMemberBean, BlackListAdapter.BlackListHolder> {

    private Context mContext;

    public BlackListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @NonNull
    @Override
    protected BlackListHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new BlackListHolder(inflater.inflate(R.layout.item_black_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull BlackListHolder holder, @NonNull BlackMemberBean item) {
        int pos = getPosition(holder);
        if (pos == 0) {
            holder.viewDivide.setVisibility(View.GONE);
        } else {
            holder.viewDivide.setVisibility(View.VISIBLE);
        }

        ImageLoaderUtil.loadImgHeadCenterCrop(holder.civAvatar, item.getUserPhoto(), R.drawable.place_holder);
        holder.tvName.setText(YHStringUtils.pickName(item.getNickName(), ""));
        holder.tvInvideCode.setText("悦鑫码：" + item.getInviteCode());

        int black = item.getIsBlock();
        if (black == 1) {
            holder.btnBlackAction.setSelected(false);

            holder.btnBlackAction.setText("解除拉黑");
        } else {
            holder.btnBlackAction.setSelected(true);
            holder.btnBlackAction.setText("拉黑");
        }

        int position = holder.getAdapterPosition();
        holder.btnBlackAction.setOnClickListener(v -> {
            listener.onBlackListAction(position);
        });
    }

    static class BlackListHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.tv_invide_code)
        TextView tvInvideCode;
        @BindView(R2.id.btn_black_action)
        Button btnBlackAction;

        @BindView(R2.id.viewDivide)
        View viewDivide;

        public BlackListHolder(View itemView) {
            super(itemView);
        }
    }

    private OnBlackListActionListener listener;

    public void setOnBlackListActionListener(OnBlackListActionListener listener) {
        this.listener = listener;
    }

    public interface OnBlackListActionListener {
        void onBlackListAction(int pos);
    }
}
