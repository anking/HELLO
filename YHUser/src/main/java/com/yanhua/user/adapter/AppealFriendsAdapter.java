package com.yanhua.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

public class AppealFriendsAdapter extends BaseRecyclerAdapter<CommonReasonModel, AppealFriendsAdapter.ViewHolder> {

    private Context context;

    public AppealFriendsAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_appeal_friends, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommonReasonModel item) {
        holder.tvText.setText(item.getContent());
        if (item.isSelected()) {
            holder.tvText.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvText.setBackgroundResource(R.drawable.shape_bg_8d46fb_r12);
        } else {
            holder.tvText.setTextColor(ContextCompat.getColor(context, R.color.mainContent));
            holder.tvText.setBackgroundResource(R.drawable.shape_bg_f6f8fc_r12);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_text)
        TextView tvText;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
