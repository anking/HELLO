package com.yanhua.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

public class ContactCSAdapter extends BaseRecyclerAdapter<ServerConfigModel, ContactCSAdapter.ViewHolder> {

    private Context mContext;
    private OnItemCellClickListener listener;

    public interface OnItemCellClickListener {
        void onItemCellClick(int pos, ServerConfigModel model);
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    public ContactCSAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_contact_cs, parent, false);
        return new ViewHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, @NonNull final ServerConfigModel model) {
        int position = getPosition(holder);

        holder.tvKey.setText(YHStringUtils.value(model.getKey()));
        holder.tvValue.setText(YHStringUtils.value(model.getValue()));
        holder.tvMome.setText(TextUtils.isEmpty(model.getMemo())?"":"（"+model.getMemo()+"）");
        holder.tvType.setText(YHStringUtils.value(model.getType() == 1 ? "拨打" : "复制"));

        ImageLoaderUtil.loadImgCenterCrop(holder.ivIcon, model.getIcon(), R.drawable.place_holder);

        holder.tvType.setOnClickListener(v -> {
            if (null != listener) {
                listener.onItemCellClick(position, model);
            }
        });
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvKey)
        TextView tvKey;

        @BindView(R2.id.tvValue)
        TextView tvValue;
        @BindView(R2.id.tvMome)
        TextView tvMome;
        @BindView(R2.id.tvType)
        TextView tvType;
        @BindView(R2.id.ivIcon)
        ImageView ivIcon;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}

