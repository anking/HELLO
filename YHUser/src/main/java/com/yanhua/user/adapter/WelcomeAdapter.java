package com.yanhua.user.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yanhua.user.R;
import com.yanhua.user.model.WelcomeModel;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;


/**
 * 引导图页面
 *
 * @author Administrator
 */
public class WelcomeAdapter extends BannerAdapter<WelcomeModel, WelcomeAdapter.WelcomeHolder> {

    public interface OnGoClickListener {
        void onGoClick();
    }

    private OnGoClickListener listener;

    public void setOnGoClickListener(OnGoClickListener l) {
        listener = l;
    }

    public WelcomeAdapter(List<WelcomeModel> datas) {
        super(datas);
    }

    @Override
    public WelcomeHolder onCreateHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_welcome, parent, false);
        return new WelcomeHolder(itemView);
    }

    @Override
    public void onBindView(WelcomeHolder holder, WelcomeModel model, int position, int size) {
        holder.ivWelcome.setImageResource(model.getImageResource());
        if (position == mDatas.size() - 1) {
            holder.btnGo.setVisibility(View.VISIBLE);
            holder.btnGo.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onGoClick();
                }
            });
        } else {
            holder.btnGo.setVisibility(View.INVISIBLE);
        }
    }


    static class WelcomeHolder extends RecyclerView.ViewHolder {

        private ImageView ivWelcome;
        private Button btnGo;

        public WelcomeHolder(@NonNull View itemView) {
            super(itemView);
            ivWelcome = itemView.findViewById(R.id.iv_welcome);
            btnGo = itemView.findViewById(R.id.btn_go);
        }
    }
}
