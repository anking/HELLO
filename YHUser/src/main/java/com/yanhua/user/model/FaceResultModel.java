package com.yanhua.user.model;

import java.io.Serializable;

public class FaceResultModel implements Serializable {

    /**
     * msg : 请求成功
     * orderNo : 20210119106435931880558592
     * code : 0
     * sign : FE846CEDB1DA95EAE8961F3BBBB76DF72E808721
     * faceId : 412f87b046810ddf0e079a4306c48746
     * bizSeqNo : 21011920001184413709432461161641
     * nonce : 5p1rGB1AayAd5eObPDkJ6QKqridGr7rag8y97JLQmB7GP6kdNYgWx3dR8r3GMV4K
     */

    private String msg;
    private String orderNo;
    private String code;
    private String sign;
    private String faceId;
    private String bizSeqNo;
    private String nonce;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }

    public String getBizSeqNo() {
        return bizSeqNo;
    }

    public void setBizSeqNo(String bizSeqNo) {
        this.bizSeqNo = bizSeqNo;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
