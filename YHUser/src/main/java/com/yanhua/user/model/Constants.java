package com.yanhua.user.model;


public class Constants {

    public static final Boolean DEBUG_ENABLE = false;
    public static final int ACTION_LOGIN_SUCCESS = 5;
    public static String verifyUrl = "https://api.verification.jpush.cn/v1/web/loginTokenVerify";

    public static String consistUrl = "https://demo.verification.jpush.cn/portal-validate-example/v2/info/consistency/android";
    //-----------------------------------------认证sdk错误码-----------------------------------------

    public static final int VERIFY_CONSISTENT = 9000;//手机号验证一致
    public static final int CODE_LOGIN_SUCCESS = 6000;
    public static final int CODE_LOGIN_FAILED = 6001;
    public static final int CODE_LOGIN_CANCELD = 6002;

    //-----------
    public static final int HTTP_TIME_OUT=15*1000;

    //前后两次的间隔,默认30秒
    public static  long INTERVAL_TIME = 1000*30;

    //以下是本地错误码
    public static final int NET_ERROR_CODE=2998;//网络错误
    public static final int NET_TIMEOUT_CODE=3001;//网络超时
    public static final int NET_UNKNOW_HOST = 3003;//域名无效
    public static final int NET_MALTFORMED_ERROR = 3004;//Malformed异常
}
