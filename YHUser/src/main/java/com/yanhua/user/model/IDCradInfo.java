package com.yanhua.user.model;

import java.io.Serializable;

public class IDCradInfo implements Serializable {

    private String name;
    private String idNum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }
}
