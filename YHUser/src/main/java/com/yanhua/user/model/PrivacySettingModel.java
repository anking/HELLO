package com.yanhua.user.model;

import java.io.Serializable;

public class PrivacySettingModel implements Serializable {

    private String id;
    private int allowComment;// 只允许关注的人评论我 0 关闭 1开启
    private int cityExhibition;// 同城展示 0 关闭 1开启
    private int hideFollowersFanList;// 隐藏我的关注和粉丝列表 0 关闭 1开启
    private int publicLikesList;// 不公开点赞列表 0 关闭 1开启

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAllowComment() {
        return allowComment;
    }

    public void setAllowComment(int allowComment) {
        this.allowComment = allowComment;
    }

    public int getCityExhibition() {
        return cityExhibition;
    }

    public void setCityExhibition(int cityExhibition) {
        this.cityExhibition = cityExhibition;
    }

    public int getHideFollowersFanList() {
        return hideFollowersFanList;
    }

    public void setHideFollowersFanList(int hideFollowersFanList) {
        this.hideFollowersFanList = hideFollowersFanList;
    }

    public int getPublicLikesList() {
        return publicLikesList;
    }

    public void setPublicLikesList(int publicLikesList) {
        this.publicLikesList = publicLikesList;
    }
}
