package com.yanhua.user.listener;

import com.yanhua.user.model.AMapLocationInfo;

public interface IMyLocationChangedListener {

    void onMyLocationChanged(AMapLocationInfo locInfo);
}
