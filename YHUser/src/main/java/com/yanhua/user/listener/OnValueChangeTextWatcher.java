package com.yanhua.user.listener;

import android.text.Editable;
import android.text.TextWatcher;

public class OnValueChangeTextWatcher implements TextWatcher {
    private OnValueChangeListener mListener;

    public OnValueChangeTextWatcher setOnValueChangeListener(OnValueChangeListener mListener) {
        this.mListener = mListener;
        return this;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (null != mListener) {
            mListener.onChangeValue(s);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    public interface OnValueChangeListener {
        void onChangeValue(CharSequence s);
    }
}
