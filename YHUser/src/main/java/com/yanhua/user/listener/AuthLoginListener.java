package com.yanhua.user.listener;


import android.text.TextUtils;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;

import java.util.HashMap;

import cn.jiguang.share.android.api.AuthListener;
import cn.jiguang.share.android.api.Platform;
import cn.jiguang.share.android.model.AccessTokenInfo;
import cn.jiguang.share.android.model.BaseResponseInfo;
import cn.jiguang.share.android.model.UserInfo;
import cn.jiguang.share.qqmodel.QQ;
import cn.jiguang.share.wechat.Wechat;
import cn.jiguang.share.weibo.SinaWeibo;

public class AuthLoginListener implements AuthListener {
    @Override
    public void onComplete(Platform platform, int action, BaseResponseInfo resData) {
        String toastMsg = null;
        switch (action) {
            case Platform.ACTION_AUTHORIZING:
                if (resData instanceof AccessTokenInfo) {        //授权信息
                    String token = ((AccessTokenInfo) resData).getToken();//token
                    long expiration = ((AccessTokenInfo) resData).getExpiresIn();//token有效时间，时间戳
                    String refresh_token = ((AccessTokenInfo) resData).getRefeshToken();//refresh_token
                    String openid = ((AccessTokenInfo) resData).getOpenid();//openid
                    //授权原始数据，开发者可自行处理
                    String originData = resData.getOriginData();
                    toastMsg = "授权成功:" + resData.toString();
                    LogUtils.d("openid:" + openid + ",token:" + token + ",expiration:" + expiration + ",refresh_token:" + refresh_token);
                    LogUtils.d("originData:" + originData);
                }
                break;
            case Platform.ACTION_REMOVE_AUTHORIZING:
//                toastMsg = "删除授权成功";
                toastMsg = "";
                break;
            case Platform.ACTION_USER_INFO:
                String platformName = platform.getName();

                if (resData instanceof UserInfo) {      //第三方个人信息
                    String openid = ((UserInfo) resData).getOpenid();  //openid
                    String name = ((UserInfo) resData).getName();  //昵称
                    String imageUrl = ((UserInfo) resData).getImageUrl();  //头像url
                    int gender = ((UserInfo) resData).getGender();//性别, 1表示男性；2表示女性
                    //个人信息原始数据，开发者可自行处理
//                    String originData = resData.getOriginData();

                    HashMap<String, Object> params = new HashMap<>();

                    if (Wechat.Name.equals(platformName)) {
//                        params.put("country", data.opt("country"));
//                        params.put("province", data.opt("province"));
//                        params.put("city", data.opt("city"));
                        params.put("unionid", openid);
                        params.put("nickname", name);
                        params.put("headimgurl", imageUrl);
                        params.put("gender", gender);//性别：gender
                    } else if (QQ.Name.equals(platformName)) {
                        params.put("openid", openid);
                        params.put("nickname", name);
                        params.put("headimgurl", imageUrl);
                        params.put("gender", gender);//性别：gender

                    } else if (SinaWeibo.Name.equals(platformName)) {
                        params.put("weiboOpenid", openid);//用户id
                        params.put("nickname", name);
                        params.put("headimgurl", imageUrl);
                        params.put("gender", gender);//性别：gender

//                        params.put("weiboOpenid", data.opt("uid"));//用户id
//                        params.put("accessToken", data.opt("accessToken"));//                accessToken （6.2以前用access_token）
//                        params.put("refreshtoken", data.opt("refreshtoken"));//refreshtoken: （6.2以前用refresh_token）
//                        params.put("expiration", data.opt("expiration"));//过期时间：expiration （6.2以前用expires_in）
//                        params.put("nickname", data.opt("name"));//用户名：name（6.2以前用screen_name）
//                        params.put("location", data.opt("location"));//位置：location
//                        params.put("iconurl", data.opt("iconurl"));//头像：iconurl（6.2以前用profile_image_url）
//                        params.put("gender", data.opt("gender"));//性别：gender
//                        params.put("followers_count", data.opt("followers_count"));//关注数：followers_count
//                        params.put("friends_count", data.opt("friends_count"));//好友数：friends_count
                    }
/*
                    String originData = resData.getOriginData();

                    JSONObject data = null;
                    try {
                        data = new JSONObject(originData);

                        if (Wechat.Name.equals(platformName)) {
                            params.put("country", data.opt("country"));
                            params.put("province", data.opt("province"));
                            params.put("city", data.opt("city"));
                            params.put("unionid", data.opt("unionid"));
                            params.put("openid", data.opt("openid"));
                            params.put("nickname", data.opt("name"));
                            params.put("headimgurl", data.opt("iconurl"));
                        } else if (QQ.Name.equals(platformName)) {
                            params.put("openid", data.opt("openid"));
                            params.put("nickname", data.opt("name"));
                            params.put("headimgurl", data.opt("iconurl"));
                        } else if (SinaWeibo.Name.equals(platformName)) {
                            params.put("weiboOpenid", data.opt("uid"));//用户id
                            params.put("accessToken", data.opt("accessToken"));//                accessToken （6.2以前用access_token）
                            params.put("refreshtoken", data.opt("refreshtoken"));//refreshtoken: （6.2以前用refresh_token）
                            params.put("expiration", data.opt("expiration"));//过期时间：expiration （6.2以前用expires_in）
                            params.put("nickname", data.opt("name"));//用户名：name（6.2以前用screen_name）
                            params.put("location", data.opt("location"));//位置：location
                            params.put("iconurl", data.opt("iconurl"));//头像：iconurl（6.2以前用profile_image_url）
                            params.put("gender", data.opt("gender"));//性别：gender
                            params.put("followers_count", data.opt("followers_count"));//关注数：followers_count
                            params.put("friends_count", data.opt("friends_count"));//好友数：friends_count
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/

                    listener.callAuthBack(platform, params);
                }

                break;
        }

        if (!TextUtils.isEmpty(toastMsg)) {
            ToastUtils.showShort(toastMsg);
        }
    }

    @Override
    public void onError(Platform platform, int action, int errorCode, Throwable error) {
        LogUtils.d("onError:" + platform + ",action:" + action + ",error:" + error);
        String toastMsg = null;
        switch (action) {
            case Platform.ACTION_AUTHORIZING:
                toastMsg = "授权失败";
                break;
            case Platform.ACTION_REMOVE_AUTHORIZING:
                toastMsg = "删除授权失败";
                break;
            case Platform.ACTION_USER_INFO:
                toastMsg = "获取个人信息失败";
                break;
        }

        if (!TextUtils.isEmpty(toastMsg)) {
            ToastUtils.showShort(toastMsg);
        }
    }

    @Override
    public void onCancel(Platform platform, int action) {
        LogUtils.d("onCancel:" + platform + ",action:" + action);
        String toastMsg = null;
        switch (action) {
            case Platform.ACTION_AUTHORIZING:
                toastMsg = "取消授权";
                break;
            case Platform.ACTION_REMOVE_AUTHORIZING:
                break;
            case Platform.ACTION_USER_INFO:
                toastMsg = "取消获取个人信息";
                break;
        }

        if (!TextUtils.isEmpty(toastMsg)) {
            ToastUtils.showShort(toastMsg);
        }
    }

    private OnAuthCallBack listener;

    public void setOnAuthCallBack(OnAuthCallBack l) {
        listener = l;
    }

    public interface OnAuthCallBack {
        void callAuthBack(Platform platform, HashMap data);
    }
}