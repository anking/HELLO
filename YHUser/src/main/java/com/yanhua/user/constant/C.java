package com.yanhua.user.constant;

public class C {

    public static final String TYPE_FRAGMENT = "typeFragment";

    public static final String INFO_NICK_NAME = "INFO_NICK_NAME";//昵称
    public static final String INFO_HOBBY = "INFO_HOBBY";//兴趣爱好
    public static final String INFO_APPEAL_FRIENDS = "INFO_APPEAL_FRIENDS";//交友诉求
    public static final String INFO_SIGN = "INFO_SIGN";//个性签名
    public static final String INFO_SIGN_TAG = "INFO_SIGN_TAG";//个性标签
    public static final String INFO_SEX = "INFO_SEX";//性别

}
