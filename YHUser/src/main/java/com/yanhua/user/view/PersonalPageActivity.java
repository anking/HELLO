package com.yanhua.user.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.dfqin.grantor.PermissionListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.MomentModel;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.CalendarUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.rong.msgprovider.UserPageMessage;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.MomentItemAdapter;
import com.yanhua.user.constant.C;
import com.yanhua.user.presenter.PersonalPagePresenter;
import com.yanhua.user.presenter.contract.PersonalPageContract;
import com.yanhua.user.widget.SetRemarkPopup;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.listener.OnPageChangeListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.TextMessage;

/**
 * 社交个人主页
 *
 * @author Administrator
 */
public class PersonalPageActivity extends BaseMvpActivity<PersonalPagePresenter> implements PersonalPageContract.IView {

    @BindView(R2.id.fl_backgroud)
    FrameLayout flBackgroud;

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.ivUserTopHead)
    CircleImageView ivUserTopHead;

    @BindView(R2.id.tvTitle)
    TextView tvTitle;

    @BindView(R2.id.ivUserHead)
    CircleImageView ivUserHead;

    @BindView(R2.id.tflTag)
    TagFlowLayout tflTag;

    @BindView(R2.id.tvUserName)
    TextView tvUserName;

    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;

    @BindView(R2.id.tvEditInfo)
    TextView tvEditInfo;

    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;

    @BindView(R2.id.llVerifybyName)
    LinearLayout llVerifybyName;
    @BindView(R2.id.tvVerifyByName)
    TextView tvVerifyByName;
    @BindView(R2.id.tvVerfiyArrow)
    AliIconFontTextView tvVerfiyArrow;

    @BindView(R2.id.ivInfoSex)
    ImageView ivInfoSex;

    @BindView(R2.id.ivSex)
    ImageView ivSex;

    @BindView(R2.id.tvIdentityId)
    TextView tvIdentityId;

    @BindView(R2.id.rlNoPhoto)
    RelativeLayout rlNoPhoto;
    @BindView(R2.id.rlPhoto)
    RelativeLayout rlPhoto;
    @BindView(R2.id.bannerPhotoWall)
    Banner bannerPhotoWall;
    @BindView(R2.id.tvIndex)
    OswaldTextView tvIndex;

    @BindView(R2.id.tvEditSignature)
    TextView tvEditSignature;
    @BindView(R2.id.tvPersonSign)
    TextView tvPersonSign;
    @BindView(R2.id.llAddHobby)
    LinearLayout llAddHobby;
    @BindView(R2.id.tvEditHobby)
    TextView tvEditHobby;
    @BindView(R2.id.tflHobby)
    TagFlowLayout tflHobby;
    @BindView(R2.id.llAddMakeFriend)
    LinearLayout llAddMakeFriend;
    @BindView(R2.id.tvEditMakeFriend)
    TextView tvEditMakeFriend;
    @BindView(R2.id.llMakeFriends)
    LinearLayout llMakeFriends;
    @BindView(R2.id.tvMakeFriends1)
    TextView tvMakeFriends1;
    @BindView(R2.id.tvMakeFriends2)
    TextView tvMakeFriends2;
    @BindView(R2.id.tvMakeFriends3)
    TextView tvMakeFriends3;
    @BindView(R2.id.tvJoinDays)
    TextView tvJoinDays;
    @BindView(R2.id.llAddBaseInfo)
    LinearLayout llAddBaseInfo;
    @BindView(R2.id.tvEditBaseInfo)
    TextView tvEditBaseInfo;
    @BindView(R2.id.tvProfession)
    TextView tvProfession;
    @BindView(R2.id.tvConstellation)
    TextView tvConstellation;
    @BindView(R2.id.tvAge)
    TextView tvAge;
    @BindView(R2.id.tvAddress)
    TextView tvAddress;
    @BindView(R2.id.tvEmotion)
    TextView tvEmotion;
    @BindView(R2.id.tvWeight)
    TextView tvWeight;
    @BindView(R2.id.tvHeight)
    TextView tvHeight;
    @BindView(R2.id.llBaseInfo)
    LinearLayout llBaseInfo;
    @BindView(R2.id.rvMoment)
    RecyclerView rvMoment;
    @BindView(R2.id.tvDynamic)
    TextView tvDynamic;
    @BindView(R2.id.flUpload)
    FrameLayout flUpload;
    @BindView(R2.id.btFollow)
    StatusView btFollow;
    @BindView(R2.id.tvEditName)
    AliIconFontTextView tvEditName;
    @BindView(R2.id.llMoment)
    LinearLayout llMoment;
    @BindView(R2.id.btChat)
    LinearLayout btChat;

    @Autowired
    String userId;

    private boolean isBlock;

    private UserInfo userInfo;
    private int updateIndex = 0;
    private List<String> imgList;
    private List<String> imgPath;
    private StsTokenModel stsTokenModel;

    private static final int UPLOAD_FILE_FAIL = 1000;
    private static final int UPLOAD_FILE_SUCCESS = 1001;

    private MyHandler mHandler = new MyHandler();

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    if (updateIndex < imgList.size()) {
                        uploadFile(imgList.get(updateIndex));
                    } else {
                        hideLoading();
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("imagesUrl", YHStringUtils.join(imgPath.toArray(new String[0]), ","));
                        basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    }
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }


    protected void creatPresent() {
        basePresenter = new PersonalPagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_personal_page;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        imgList = new ArrayList<>();
        imgPath = new ArrayList<>();

        int width = DisplayUtils.getScreenWidth(mContext);
        ViewGroup.LayoutParams imageLayoutParams = flBackgroud.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = width;
        flBackgroud.setLayoutParams(imageLayoutParams);

        CoordinatorLayout.LayoutParams mAppBarLayoutLayoutParams = (CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams();
        mAppBarLayoutLayoutParams.width = width;
        mAppBarLayoutLayoutParams.height = width;
        mAppBarLayout.setLayoutParams(mAppBarLayoutLayoutParams);

        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;

            float rangle = scroll2top / scrollRangle;

            float alpha = 1 - rangle;
            if (alpha > 0 && alpha < 0.5) {
                toolbar.setAlpha(1 - alpha);
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_black);
                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvEditInfo.setBackgroundResource(R.drawable.shape_half_r_black);

                rlTitle.setVisibility(View.GONE);
            } else if (alpha < 1 && alpha >= 0.5) {
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.white);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);
                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.theme));
                tvEditInfo.setBackgroundResource(R.drawable.shape_bg_binded);

                rlTitle.setVisibility(View.VISIBLE);
            } else if (alpha == 1) {//避免滑动过快还显示半透明状态
                mAppBarLayout.postDelayed(() -> {
                    toolbar.setAlpha(1 - rangle);
                    toolbar.setBackgroundResource(R.color.white);

                    tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                    iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);
                    tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.theme));
                    tvEditInfo.setBackgroundResource(R.drawable.shape_bg_binded);

                    rlTitle.setVisibility(View.VISIBLE);
                }, 100);
            } else if (alpha == 0) {//避免滑动过快还显示半透明状态
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvEditInfo.setBackgroundResource(R.drawable.shape_half_r_black);
                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.white));

                rlTitle.setVisibility(View.GONE);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    @Override
    public void handleDeleteContentSuccess() {
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        basePresenter.getStsToken();

        basePresenter.getPersonalInfo(userId);
    }


    private void initUI(UserInfo userInfo) {
        //是否可以看见私聊按钮---状态为0可显示
        int hideSetStatus = userInfo.getHideSetStatus();

        ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userInfo.getImg(), R.drawable.place_holder);
        ImageLoaderUtil.loadImgCenterCrop(ivUserTopHead, userInfo.getImg(), R.drawable.place_holder);
        if (!TextUtils.isEmpty(userInfo.getMemoName())) {
            tvTitle.setText(userInfo.getMemoName());
            tvUserName.setText(userInfo.getMemoName());
        } else {
            tvTitle.setText(userInfo.getNickName());
            tvUserName.setText(userInfo.getNickName());
        }
        tvIdentityId.setText(String.format("ID %s", userInfo.getIdentityId()));
        if (UserManager.getInstance().isMyself(userInfo.getId())) {
            UserManager.getInstance().setUserInfo(userInfo, false);
            UserManager.getInstance().setUserInfo(userInfo, false);
            tvDynamic.setText("我的动态");
            flUpload.setVisibility(View.VISIBLE);
        } else {
            tvDynamic.setText("个人动态");
            flUpload.setVisibility(View.GONE);
        }

        if (userInfo.getUserAloneTagVOList() != null && !userInfo.getUserAloneTagVOList().isEmpty()) {
            tflTag.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserAloneTagVOList()) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tfl_tag, parent, false);
                    //获取标签
                    TextView tvTag = view.findViewById(R.id.tvTag);
                    tvTag.setText(model.getTagName());
                    return view;
                }
            });
            tflTag.setOnTagClickListener((view, position, parent) -> {
                ToastUtils.showShort(userInfo.getUserAloneTagVOList().get(position).getTagName());
                return true;
            });
        }
        if (!UserManager.getInstance().isMyself(userId)) {
            rlNoPhoto.setVisibility(View.GONE);
            rlPhoto.setVisibility(View.VISIBLE);
        } else if (TextUtils.isEmpty(userInfo.getImagesUrl())) {
            rlNoPhoto.setVisibility(View.VISIBLE);
            rlPhoto.setVisibility(View.GONE);
        } else {
            rlNoPhoto.setVisibility(View.GONE);
            rlPhoto.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(userInfo.getImagesUrl())) {
            String imagesUrl = userInfo.getImagesUrl();
            String[] arr = imagesUrl.split(",");
            List<String> imgs = Arrays.asList(arr);
            tvIndex.setVisibility(imgs.size() > 1 ? View.VISIBLE : View.GONE);
            tvIndex.setText("1/" + imgs.size());
            bannerPhotoWall.setAdapter(new BannerImageAdapter<String>(imgs) {
                @Override
                public void onBindView(BannerImageHolder holder, String data, int position, int size) {
                    Glide.with(mContext)
                            .load(data)
                            .thumbnail(Glide.with(mContext).load(data + YXConfig.IMAGE_RESIZE))
                            .apply(new RequestOptions()
                                    .fitCenter()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL))
                            .placeholder(R.mipmap.bg_bluer)
                            .into(holder.imageView);
                    holder.imageView.setOnClickListener(view -> {
                        //当你点击图片的时候执行以下代码：
                        // 多图片场景（你有多张图片需要浏览）
                        List<Object> urlList = new ArrayList<>();
                        for (String url : imgs) {
                            urlList.add(url);
                        }
                        //srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
                        new XPopup.Builder(mContext).asImageViewer(holder.imageView, position, urlList, (popupView, position1) -> {
                            // 作用是当Pager切换了图片，需要更新源View
//                                popupView.updateSrcView((ImageView) bannerGoods.getChildAt(position));
                        }, new SmartGlideImageLoader()).show();
                    });
                }
            });
            bannerPhotoWall.addOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    tvIndex.setText((position + 1) + "/" + imgs.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
        if (userInfo.getGender() == -1) {
            ivInfoSex.setVisibility(View.GONE);
            ivSex.setVisibility(View.GONE);
        } else {
            ivInfoSex.setVisibility(View.VISIBLE);
            ivSex.setVisibility(View.VISIBLE);
            ivInfoSex.setImageResource(userInfo.getGender() == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
            ivSex.setImageResource(userInfo.getGender() == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
        }
        if (!TextUtils.isEmpty(userInfo.getPersonalSignature())) {
            tvPersonSign.setText(userInfo.getPersonalSignature());
        }
        if (userInfo.getUserHobbyTagVOList() != null && !userInfo.getUserHobbyTagVOList().isEmpty()) {
            llAddHobby.setVisibility(View.GONE);
            tvEditHobby.setVisibility(View.VISIBLE);
            tflHobby.setVisibility(View.VISIBLE);
            tflHobby.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserHobbyTagVOList()) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    TextView tvTag = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag_sign, parent, false);
                    tvTag.setText(model.getTagName());
                    return tvTag;
                }
            });
        } else {
            llAddHobby.setVisibility(View.VISIBLE);
            tvEditHobby.setVisibility(View.GONE);
            tflHobby.setVisibility(View.GONE);
        }

        String makeFriendType = userInfo.getMakeFriendType();
        if (TextUtils.isEmpty(makeFriendType)) {
            llAddMakeFriend.setVisibility(View.VISIBLE);
            tvEditMakeFriend.setVisibility(View.GONE);
            llMakeFriends.setVisibility(View.GONE);
        } else {
            llAddMakeFriend.setVisibility(View.GONE);
            tvEditMakeFriend.setVisibility(View.VISIBLE);
            llMakeFriends.setVisibility(View.VISIBLE);
            String[] arr = makeFriendType.split(",");
            if (arr.length == 3) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.VISIBLE);
                tvMakeFriends3.setVisibility(View.VISIBLE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
                tvMakeFriends2.setText(arr[1].split("`~`")[1]);
                tvMakeFriends3.setText(arr[2].split("`~`")[1]);
            } else if (arr.length == 2) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.VISIBLE);
                tvMakeFriends3.setVisibility(View.GONE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
                tvMakeFriends2.setText(arr[1].split("`~`")[1]);
            } else if (arr.length == 1) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.GONE);
                tvMakeFriends3.setVisibility(View.GONE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
            }
        }
        tvJoinDays.setText(String.format("加入音圈%d天", userInfo.getJoinDays()));

        if (!TextUtils.isEmpty(userInfo.getHometown()) || !TextUtils.isEmpty(userInfo.getAddress()) || userInfo.getHeight() != 0 || userInfo.getWeight() != 0 ||
                !TextUtils.isEmpty(userInfo.getBirthday()) || !TextUtils.isEmpty(userInfo.getEmotionTypeId()) || !TextUtils.isEmpty(userInfo.getOccupationTypeId())) {
            llAddBaseInfo.setVisibility(View.GONE);
            tvEditBaseInfo.setVisibility(View.VISIBLE);
            llBaseInfo.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(userInfo.getOccupationTypeName())) {
                tvProfession.setText(userInfo.getOccupationTypeName());
            } else {
                tvProfession.setText("-");
            }

            if (!TextUtils.isEmpty(userInfo.getAddress())) {
                tvAddress.setText(userInfo.getAddress().replace(",", ""));
            } else {
                tvAddress.setText("-");
            }

            if (!TextUtils.isEmpty(userInfo.getEmotionTypeName())) {
                tvEmotion.setText(userInfo.getEmotionTypeName());
            } else {
                tvEmotion.setText("-");
            }

            if (userInfo.getHeight() != 0) {
                tvHeight.setText(String.format("%dcm", userInfo.getHeight()));
            }

            if (userInfo.getWeight() != 0) {
                tvWeight.setText(String.format("%dkg", userInfo.getWeight()));
            }

            String birthday = userInfo.getBirthday();
            if (userInfo.getConstellationState() == 1) {
                if (!TextUtils.isEmpty(birthday)) {
                    tvAge.setVisibility(View.VISIBLE);
                    tvConstellation.setVisibility(View.VISIBLE);
                    tvConstellation.setText(CalendarUtils.getConstellation(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd")));
                    tvAge.setText(String.format("%d岁", CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"), new Date())));
                } else {
                    tvAge.setVisibility(View.GONE);
                    tvConstellation.setVisibility(View.GONE);
                }
            } else {
                if (!TextUtils.isEmpty(birthday)) {
                    tvAge.setVisibility(View.VISIBLE);
                    tvAge.setText(String.format("%d岁", CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"), new Date())));
                } else {
                    tvAge.setVisibility(View.GONE);
                }
                tvConstellation.setText("保密");
            }
        } else {
            llAddBaseInfo.setVisibility(View.VISIBLE);
            tvEditBaseInfo.setVisibility(View.GONE);
            llBaseInfo.setVisibility(View.GONE);
        }
        List<MomentModel> momentContentList = userInfo.getMomentContentList();
        if (momentContentList != null && !momentContentList.isEmpty()) {
            rvMoment.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            MomentItemAdapter momentItemAdapter = new MomentItemAdapter(this);
            rvMoment.setAdapter(momentItemAdapter);
            momentItemAdapter.setItems(momentContentList);
        } else if (UserManager.getInstance().isMyself(userId)) {
            llMoment.setVisibility(View.VISIBLE);
        } else {
            llMoment.setVisibility(View.GONE);
        }

        int realNameValid = userInfo.getRealNameValid();
        if (realNameValid == 1) {
            // 区分自己和其他用户
            llVerifybyName.setSelected(true);
            if (UserManager.getInstance().isMyself(userInfo.getId())) {
                tvVerifyByName.setText("已实名");
                tvVerifyByName.setTextColor(ContextCompat.getColor(this, R.color.white));
                tvVerfiyArrow.setVisibility(View.VISIBLE);
                tvVerfiyArrow.setTextColor(ContextCompat.getColor(this, R.color.white));
                llVerifybyName.setOnClickListener(v -> ARouter.getInstance()
                        .build(ARouterPath.CERTIFY_RESULT_ACTIVITY)
                        .withInt("result", realNameValid)
                        .navigation());
            } else {
                tvVerifyByName.setText("已实名");
                tvVerifyByName.setTextColor(ContextCompat.getColor(this, R.color.white));
                tvVerfiyArrow.setVisibility(View.GONE);
            }
        } else {
            llVerifybyName.setSelected(false);
            if (UserManager.getInstance().isMyself(userInfo.getId())) {
                tvVerifyByName.setText("实名认证");
                tvVerifyByName.setTextColor(ContextCompat.getColor(this, R.color.subWord));
                tvVerfiyArrow.setVisibility(View.VISIBLE);
                tvVerfiyArrow.setTextColor(ContextCompat.getColor(this, R.color.theme));
                llVerifybyName.setOnClickListener(v -> ARouter.getInstance()
                        .build(ARouterPath.CERTIFY_CENTER_ACTIVITY)
                        .withInt("result", realNameValid)
                        .navigation());
            } else {
                tvVerifyByName.setText("未实名");
                tvVerifyByName.setTextColor(ContextCompat.getColor(this, R.color.subWord));
                tvVerfiyArrow.setVisibility(View.GONE);
            }
        }

        // 结尾统一处理布局的隐藏显示
        if (UserManager.getInstance().isMyself(userId)) {//判断是自己
            btFollow.setVisibility(View.GONE);
            tvEditInfo.setVisibility(View.VISIBLE);
            tvEditName.setVisibility(View.GONE);

            btChat.setVisibility(View.GONE);
        } else {
            tvEditInfo.setVisibility(View.GONE);
            tvEditBaseInfo.setVisibility(View.GONE);
            tvEditMakeFriend.setVisibility(View.GONE);
            llAddBaseInfo.setVisibility(View.GONE);
            llAddMakeFriend.setVisibility(View.GONE);
            tvEditSignature.setVisibility(View.GONE);
            tvEditHobby.setVisibility(View.GONE);
            llAddHobby.setVisibility(View.GONE);
            btFollow.setVisibility(View.VISIBLE);
            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
            int relationState = userInfo.getRelationState();
            boolean isFollow = DiscoValueFormat.isFollow(relationState);
            btFollow.setStatus(isFollow);


            if (isFollow) {//粉丝
                tvEditName.setVisibility(View.VISIBLE);
                btChat.setVisibility(View.VISIBLE);
            } else {
                tvEditName.setVisibility(View.GONE);
                if (hideSetStatus == 1) {
                    btChat.setVisibility(View.GONE);
                } else if (hideSetStatus == 0) {
                    btChat.setVisibility(View.VISIBLE);
                }
            }

            //查询用户是否在黑名单中
            RongIMClient.getInstance().getBlacklistStatus(userId, new RongIMClient.ResultCallback<RongIMClient.BlacklistStatus>() {
                @Override
                public void onSuccess(RongIMClient.BlacklistStatus blacklistStatus) {
                    boolean isINBlackList = blacklistStatus == RongIMClient.BlacklistStatus.IN_BLACK_LIST;

                    isBlock = isINBlackList;
                }

                @Override
                public void onError(RongIMClient.ErrorCode errorCode) {

                }
            });
        }
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        UserPageMessage messageContent = new UserPageMessage();

        messageContent.setUserId(userInfo.getId());
        messageContent.setHeadImg(userInfo.getImg());
        messageContent.setNickName(userInfo.getNickName());
        messageContent.setPostNum(userInfo.getDynamicCount());
        messageContent.setFansNum(userInfo.getFans());

        if (!TextUtils.isEmpty(userInfo.getImagesUrl())) {
            //TODO NEXT 用户个人主页的视频缩略图或第一张图片
            String imagesUrl = userInfo.getImagesUrl();
            String[] arr = imagesUrl.split(",");
            List<String> imgs = Arrays.asList(arr);
            if (null != imgs && imgs.size() > 0) {
                messageContent.setBgImg(imgs.get(0));
            }
        }

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            flBackgroud.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType type = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    io.rong.imlib.model.Message message = io.rong.imlib.model.Message.obtain(targetId,
                            Conversation.ConversationType.PRIVATE,
                            messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, "[个人主页]");
                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        io.rong.imlib.model.Message textMsg = io.rong.imlib.model.Message.obtain(targetId, type, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }

    private static final int CHOOSE_PHOTO = 0x001;

    private void chooseImage() {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(8)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(1, 1)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(false)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(false)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(CHOOSE_PHOTO);
    }

    private void checkStoragePermission() {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImage();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    private void chat2ThisPerson() {
        if (userId.equals(mMMKV.getUserId())) {
            ToastUtils.showShort("不能给自己发私信");
            return;
        }

        RouteUtils.routeToConversationActivity(this, Conversation.ConversationType.PRIVATE, userId, null);
    }

    @OnClick({R2.id.rlNoPhoto, R2.id.tvEditInfo, R2.id.tvEditSignature, R2.id.btChat,
            R2.id.tvEditHobby, R2.id.llAddHobby, R2.id.llCheckAll,
            R2.id.tvEditMakeFriend, R2.id.llAddMakeFriend, R2.id.iconMore,
            R2.id.llAddBaseInfo, R2.id.tvEditBaseInfo, R2.id.btFollow, R2.id.tvEditName})
    public void onViewClicked(View view) {
        int id = view.getId();
        if (id == R.id.rlNoPhoto) {
            checkStoragePermission();
        } else if (id == R.id.tvEditInfo) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_ACTIVITY)
                    .navigation();
        } else if (id == R.id.btChat) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                chat2ThisPerson();
            });
        } else if (id == R.id.iconMore) {
            //展示更多
            String nickName = userInfo.getNickName();
            String headPic = userInfo.getImg();

            MoreActionBoard.sharePersonPageDetail(mActivity, userId, nickName, headPic, isBlock, (position, tp) -> {
                switch (position) {
                    case ShareBoardPopup.BOARD_SET_REPORT:
                        PageJumpUtil.firstIsLoginThenJump(() -> toReport());
                        break;

                    case ShareBoardPopup.BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == userInfo) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, userInfo, YXConfig.TYPE_USER_PAGE, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                    case ShareBoardPopup.BOARD_SET_BLACK:
                        if (isBlock) {
                            RongIMClient.getInstance().removeFromBlacklist(userId, new RongIMClient.OperationCallback() {
                                @Override
                                public void onSuccess() {
                                    isBlock = false;

                                    basePresenter.addOrCancelBlack(userId);
                                }

                                @Override
                                public void onError(RongIMClient.ErrorCode errorCode) {

                                }
                            });
                        } else {
                            CommonDialog commonDialog = new CommonDialog(mContext, "拉黑后，对方将无法搜索到你，也不能再给你发私信", "", "确定", "取消",
                                    mContext.getResources().getColor(R.color.color_F42603),
                                    mContext.getResources().getColor(R.color.color_EEEEEE));
                            new XPopup.Builder(mContext).asCustom(commonDialog).show();
                            commonDialog.setOnConfirmListener(() -> {
                                commonDialog.dismiss();

                                RongIMClient.getInstance().addToBlacklist(userId, new RongIMClient.OperationCallback() {
                                    @Override
                                    public void onSuccess() {
                                        isBlock = true;

                                        basePresenter.addOrCancelBlack(userId);
                                    }

                                    @Override
                                    public void onError(RongIMClient.ErrorCode errorCode) {

                                    }
                                });
                            });

                            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
                        }
                        break;
                }
            });
        } else if (id == R.id.tvEditSignature) {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_SIGN)
                    .withString("value", UserManager.getInstance().getUserInfo().getPersonalSignature())
                    .navigation();
        } else if (id == R.id.tvEditHobby) {
            // 编辑
            List<PersonTagModel> userHobbyTagVOList = UserManager.getInstance().getUserInfo().getUserHobbyTagVOList();
            ArrayList<String> list = new ArrayList<>();
            for (PersonTagModel p : userHobbyTagVOList) {
                list.add(p.getId());
            }
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_HOBBY)
                    .withStringArrayList("ids", list)
                    .navigation();
        } else if (id == R.id.llAddHobby) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_HOBBY).navigation();
        } else if (id == R.id.tvEditMakeFriend) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_APPEAL_FRIENDS).navigation();
        } else if (id == R.id.llAddMakeFriend) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_APPEAL_FRIENDS).navigation();
        } else if (id == R.id.llAddBaseInfo) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_ACTIVITY).navigation();
        } else if (id == R.id.tvEditBaseInfo) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_ACTIVITY).navigation();
        } else if (id == R.id.llCheckAll) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.MY_DYNAMIC_LIST_ACTIVITY)
                        .withString("userId", userId)
                        .navigation();
            });
        } else if (id == R.id.btFollow) {
            if (userInfo.getRelationState() != 2 && userInfo.getRelationState() != 4) {
                // 关注
                basePresenter.followUser(userId);
            } else {
                // 取关
                basePresenter.cancelFollowUser(userId);
            }
        } else if (id == R.id.tvEditName) {
            SetRemarkPopup setRemarkPopup = new SetRemarkPopup(this);
            setRemarkPopup.setOnSetRemarkListener(remark -> basePresenter.setFriendRemark(userId, remark));
            new XPopup.Builder(this).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(setRemarkPopup).show();
        }
    }

    @Override
    public void handleSetFriendRemarkSuccess() {
        ToastUtils.showShort("设置成功");
    }

    /**
     * 举报用户主页
     */
    private void toReport() {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", userId)
                    .withInt("reportType", YXConfig.reportType.person)
                    .navigation();
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case ConstanceEvent.REFRESH_USER_INFO:
                boolean isLogin = UserManager.getInstance().isLogin();
                if (isLogin) {
                    basePresenter.getPersonalInfo(mMMKV.getUserId());
                }
                break;
        }
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        this.userInfo = userInfo;
        initUI(userInfo);
    }

    @Override
    public void handleErrorMsg(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void updateFollowUserSuccess() {
        basePresenter.getPersonalInfo(userId);
    }

    @Override
    public void updateBlackUserSuccess() {

    }

    @Override
    public void updateStarContentSuccess() {

    }

    @Override
    public void handleContentUserDetailSuccess(ContentUserInfoModel model) {

    }

    @Override
    public void handleSetBackGroundSuccess() {
//        basePresenter.getContentUserDetail(userId);
    }

    @Override
    public void handleUserContentList(List<MomentListModel> list) {

    }

    @Override
    public void updateUserDetailSuccess() {
        basePresenter.getPersonalInfo(mMMKV.getUserId());
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);
        UserManager.getInstance().setStsToken(json);
    }

    @SuppressLint("AutoDispose")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // QQ授权回调需要配置这里
        if (requestCode == CHOOSE_PHOTO && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            if (!result.isEmpty()) {
                updateIndex = 0;
                basePresenter.addSubscribe(
                        Flowable.just(result)
                                .observeOn(Schedulers.io())
                                .map(list -> {
                                    imgList.clear();
                                    for (LocalMedia item : list) {
                                        imgList.add(item.getRealPath());
                                    }
                                    return imgList;
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnError(throwable -> {
                                })
                                .subscribe(list -> {
                                    showLoading();
                                    uploadFile(list.get(updateIndex));
                                })
                );
            }
        }
    }

    private void uploadFile(String path) {
        String fileName = new File(path).getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, path, new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                imgPath.add(httpUrl);
                updateIndex++;
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

}
