package com.yanhua.user.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.king.zxing.util.CodeUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ShareImageUrlBody;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.utils.SharesUtils;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 分享邀请码页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.SHARE_FRIENDS_ACTIVITY)
public class ShareFriendsActivity extends BaseMvpActivity {

    @BindView(R2.id.ivRealInviteCode)
    ImageView ivRealInviteCode;

    @BindView(R2.id.rl_real_share_content)
    RelativeLayout shareRealContentBitmap;

    private Bitmap qrcodeBitmap;
    private boolean saveOnce;//避免一些傻逼狂点

    public static final int RC_READ_PHOTO = 0X02;

    @Override
    protected void creatPresent() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_share_friends;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle("分享好友");
        String content = YXConfig.getShareDownloadUrl();
        try {
//            Drawable drawable = ContextCompat.getDrawable(mContext, R.mipmap.logo);
//            BitmapDrawable bd = (BitmapDrawable) drawable;
//            Bitmap logo = bd.getBitmap();
//            Bitmap codeBitmap = CodeUtils.createQRCode(content, DisplayUtils.dip2px(mContext, 300), logo);
//            ivRealInviteCode.setImageBitmap(codeBitmap);

            Bitmap codeBitmap = CodeUtils.createQRCode(content, DisplayUtils.dip2px(mContext, 300));
            ivRealInviteCode.setImageBitmap(codeBitmap);
            new Handler().postDelayed(() -> {
                //操作内容
                qrcodeBitmap = getBitmapFromView(shareRealContentBitmap);
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = null;
        if (view == null) {
            return bitmap;
        }
        view.setDrawingCacheEnabled(true);

        view.buildDrawingCache();

        bitmap = view.getDrawingCache();

        return bitmap;
    }

    @OnClick({R2.id.llShareApp, R2.id.ll_qq_friends, R2.id.ll_qq_zone, R2.id.ll_wechat_friends, R2.id.ll_wechat_line, R2.id.ll_copy_url, R2.id.ll_sina})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.ll_copy_url) {
            ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
            String content = YXConfig.getShareDownloadUrl();
            ClipData clipData = ClipData.newPlainText(null, content);
            clipboard.setPrimaryClip(clipData);
            ToastUtils.showShort("已复制");
        } else if (id == R.id.ll_wechat_friends) {
            ShareImageUrlBody shareBody = new ShareImageUrlBody();
            shareBody.setPosition(ShareBoardPopup.BOARD_SHARE_WECHAT_FRIEND);
            shareBody.setWebUrl(YXConfig.getShareDownloadUrl());
            shareBody.setTitle("");
            shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

            shareBody.setShareBitmap(qrcodeBitmap);
//
            SharesUtils.shareImage(mActivity, shareBody);
        } else if (id == R.id.ll_wechat_line) {
            ShareImageUrlBody shareBody = new ShareImageUrlBody();
            shareBody.setPosition(ShareBoardPopup.BOARD_SHARE_WECHAT_MOMENT);
            shareBody.setWebUrl(YXConfig.getShareDownloadUrl());
            shareBody.setTitle("");
            shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

            shareBody.setShareBitmap(qrcodeBitmap);
//
            SharesUtils.shareImage(mActivity, shareBody);
        } else if (id == R.id.ll_qq_friends) {
            ShareImageUrlBody shareBody = new ShareImageUrlBody();
            shareBody.setPosition(ShareBoardPopup.BOARD_SHARE_QQ_FRIEND);
            shareBody.setWebUrl(YXConfig.getShareDownloadUrl());
            shareBody.setTitle("");

            shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

            shareBody.setShareBitmap(qrcodeBitmap);
//
            SharesUtils.shareImage(mActivity, shareBody);
        } else if (id == R.id.ll_qq_zone) {
            ShareImageUrlBody shareBody = new ShareImageUrlBody();
            shareBody.setPosition(ShareBoardPopup.BOARD_SHARE_QQ_ZONE);
            shareBody.setWebUrl(YXConfig.getShareDownloadUrl());
            shareBody.setTitle("");

            shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

            shareBody.setShareBitmap(qrcodeBitmap);
//
            SharesUtils.shareImage(mActivity, shareBody);
        } else if (id == R.id.ll_sina) {
            ShareImageUrlBody shareBody = new ShareImageUrlBody();
            shareBody.setPosition(ShareBoardPopup.BOARD_SHARE_SINA);
            shareBody.setWebUrl(YXConfig.getShareDownloadUrl());
            shareBody.setTitle("");

            shareBody.setDesc("您的好友正在与您分享精彩的内容，赶快来…");

            shareBody.setShareBitmap(qrcodeBitmap);
//
            SharesUtils.shareImage(mActivity, shareBody);
        }
    }

}
