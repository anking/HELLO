package com.yanhua.user.view;

import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.PasswordInputFilter;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import butterknife.BindView;

/**
 * 设置密码
 */
@Route(path = ARouterPath.SET_PSWD_ACTIVITY)
public class SetPswdActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @BindView(R2.id.tv_commit)
    TextView tvCommit;

    @BindView(R2.id.et_password)
    EditText etPassword;
    @BindView(R2.id.et_confirmPassword)
    EditText etConfirmPassword;

    @BindView(R2.id.tv_eye1)
    AliIconFontTextView tvEye;
    @BindView(R2.id.tv_eye2)
    AliIconFontTextView tvConfirmEye;

    @Autowired
    boolean fromSet;

    private boolean isEyePswdClose, isConfirmEyePswdClose;

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("");

        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("暂不设置");
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitleContainBg(R.color.transparent, -1);

        //设置密码不可以输入其他中文
        PasswordInputFilter.setPasswordInputFilter(etPassword);
        PasswordInputFilter.setPasswordInputFilter(etConfirmPassword);

        applyDebouncingClickListener(true, tvRight, tvEye, tvConfirmEye, tvCommit);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_right) {//
            if (fromSet) {
                onBackPressed();
            } else {
                ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                        .navigation();
            }
        } else if (id == R.id.tv_eye1) {
            isEyePswdClose = !isEyePswdClose;

            setEyeVisiable(isEyePswdClose, etPassword, tvEye);
        } else if (id == R.id.tv_eye2) {
            isConfirmEyePswdClose = !isConfirmEyePswdClose;

            setEyeVisiable(isConfirmEyePswdClose, etConfirmPassword, tvConfirmEye);
        } else if (id == R.id.tv_commit) {//

            String password = etPassword.getText().toString().trim();
            String confirmPassword = etConfirmPassword.getText().toString().trim();

            if (TextUtils.isEmpty(password)) {
                ToastUtils.showShort(R.string.please_input_pwd);
                return;
            }
            if (TextUtils.isEmpty(confirmPassword)) {
                ToastUtils.showShort(R.string.please_input_confirm_pwd);
                return;
            }
            if (!password.equals(confirmPassword)) {
                ToastUtils.showShort(R.string.different_pwd);
                return;
            }

            if (!YHStringUtils.validPassword(confirmPassword)) {
                ToastUtils.showShort(R.string.valid_pswd_error);
                return;
            }

            basePresenter.setPassword(UserManager.getInstance().getUserInfo().getMobile(), password);
        }
    }

    /**
     * 设置眼睛visible
     *
     * @param isClose
     * @param editText
     * @param eye
     */
    private void setEyeVisiable(boolean isClose, EditText editText, TextView eye) {
        if (isClose) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        eye.setText(isClose ? "\ue765" : "\ue751");

        String content = editText.getText().toString();
        editText.setSelection(content.length());
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return false;
    }

    @Override
    public int getStatusColor() {
        return R.color.transparent;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_set_pswd;
    }

    @Override
    public void handlePswdResetResult() {
        ToastUtils.showShort(R.string.set_successful);

        UserManager.getInstance().getUserInfo().setLoginPasswordStatus(true);

        onBackPressed();
    }
}