package com.yanhua.user.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.CountDownTimerUtils;
import com.yanhua.core.util.RegUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 绑定手机
 */
@Route(path = ARouterPath.LOG_OFF_ACTIVITY)
public class LogOffActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_phoneCode)
    TextView tvGetCode;

    @BindView(R2.id.etPhoneCode)
    EditText etPhoneCode;
    @BindView(R2.id.etPhone)
    EditText etPhone;

    @BindView(R2.id.tvLogOff)
    TextView tvLogOff;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("注销账号");

        String phone = UserManager.getInstance().getUserInfo().getMobile();
        etPhone.setText(TextUtils.isEmpty(phone) ? "" : phone);
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @OnClick(R2.id.tv_phoneCode)
    public void clickGetCode() {
        hideSoftKeyboard();
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort(R.string.please_input_phone);
        } else if (!RegUtils.isMobilePhone(phone)) {
            ToastUtils.showShort(R.string.error_format_phone);
        } else {
            basePresenter.getSmsCode(phone, YXConfig.smsWay.logoff);
        }
    }

    @OnClick(R2.id.tvLogOff)
    public void clickLogOff() {
        hideSoftKeyboard();
        String phoneCode = etPhoneCode.getText().toString().trim();//验证码
        String phone = etPhone.getText().toString().trim();//手机号

        basePresenter.validateCode(phone, phoneCode, YXConfig.smsWay.logoff);
    }

    @Override
    public void handleValidateSuccess(String type) {

        String phone = etPhone.getText().toString().trim();
        String code = etPhoneCode.getText().toString().trim();

        HashMap<String, Object> params = new HashMap<>();
        params.put("code", code);
        params.put("mobile", phone);

        basePresenter.userLogOff(params);
    }

    @Override
    public void handleUserLogOffSuccess(boolean isSuccess) {
        //如果成功，返回，注销成功--->返回到首页
        //失败跳到说明原因页面
        if (isSuccess) {
            ARouter.getInstance().build(ARouterPath.LOGIN_ACTIVITY).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).navigation();
        } else {
            ARouter.getInstance().build(ARouterPath.LOG_OFF_FAILED_ACTIVITY).navigation();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_log_off;
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhoneCode.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPhoneCode.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        tvLogOff.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    //-------------------数据接口返回逻辑------------------------
    @Override
    public void handleSuccessExistPhone(int data) {

    }

    @Override
    public void handleSendSuccess() {
        ToastUtils.showShort(R.string.send_code_successful);
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }
}