package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/*
实名认证
 */
@Route(path = ARouterPath.CERTIFY_CENTER_ACTIVITY)
public class CertifyCenterActivity extends BaseMvpActivity {

    @BindView(R2.id.btRealNameCertify)
    TextView btRealNameCertify;

    @Override
    protected void creatPresent() {

    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, btRealNameCertify);
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.btRealNameCertify) {
            ARouter.getInstance().build(ARouterPath.CERTIFICATION_ACTIVITY).navigation();
        }
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_certify_center;
    }
}