package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ScrollViewPager;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.fragment.AlreadyAtFragment;
import com.yanhua.user.fragment.BeAtFragment;
import com.yanhua.user.fragment.FriendsFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ta的关注和ta的粉丝页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.RELATION_USER_LIST_ACTIVITY)
public class RelationUserListActivity extends BaseMvpActivity {

    @BindView(R2.id.viewPager)
    ScrollViewPager viewPager;
    @BindView(R2.id.sgTabLayout)
    SegmentTabLayout sgTabLayout;

    //-----------------------------------搜素------------------------------------------------------
    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;
    @BindView(R2.id.tv_search_cancle)
    TextView tvSearchCancle;

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.titleContain)
    RelativeLayout rlTitle;

    private String keyword;

    @OnClick(R2.id.tv_search_cancle)
    public void hideSearch() {
        rlTitle.setVisibility(View.VISIBLE);
        rlSearch.setVisibility(View.GONE);
        etSearch.setText("");

        viewPager.setScroll(true);

        getListData("");
    }

    private void getListData(String keyword) {
        int pos = sgTabLayout.getCurrentTab();
        Fragment fragment = mFragments.get(pos);
        if (fragment instanceof AlreadyAtFragment) {
            ((AlreadyAtFragment) fragment).setKeyword(keyword);
            ((AlreadyAtFragment) fragment).getUserList(1);
        } else if (fragment instanceof BeAtFragment) {
            ((BeAtFragment) fragment).setKeyword(keyword);
            ((BeAtFragment) fragment).getUserList(1);
        } else if (fragment instanceof FriendsFragment) {
            ((FriendsFragment) fragment).getUserList(1);
        }

    }

    @OnClick(R2.id.tvSearch)
    public void showSearch() {
        rlSearch.setVisibility(View.VISIBLE);
        rlTitle.setVisibility(View.GONE);
        viewPager.setScroll(false);
        etSearch.setFocusable(true);
        etSearch.setFocusableInTouchMode(true);
        etSearch.requestFocus();
    }

    private void setInputListener() {
        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                keyword = etSearch.getText().toString().trim();
                getListData(keyword);
                return true;
            }
            return false;
        });
    }
    //-----------------------------------搜素------------------------------------------------------

    @Autowired(desc = "0是已关注 1是被关注 2是互相关注")
    int type;

    @Autowired
    String userId;

    private ArrayList<Fragment> mFragments;
    private ArrayList<String> mTitles;

    @Override
    protected void creatPresent() {
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_relation_user_list;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        Bundle alreadybundle = new Bundle();
        alreadybundle.putInt("type", 0);
        alreadybundle.putString("userId", userId);

        AlreadyAtFragment alreadyAtFragment = new AlreadyAtFragment();
        alreadyAtFragment.setArguments(alreadybundle);


        Bundle bebundle = new Bundle();
        bebundle.putInt("type", 1);
        bebundle.putString("userId", userId);
        BeAtFragment beFragment = new BeAtFragment();
        beFragment.setArguments(bebundle);

        Bundle friendbundle = new Bundle();
        friendbundle.putInt("type", 2);
        friendbundle.putString("userId", userId);

        FriendsFragment friendsFragment = new FriendsFragment();
        friendsFragment.setArguments(friendbundle);


        mFragments = new ArrayList<>();
        mTitles = new ArrayList<>();
        if (UserManager.getInstance().isMyself(userId)) {
            mTitles.add("关注");
            mTitles.add("粉丝");
            //            mFragments.add(friendsFragment);
        } else {
            mTitles.add("Ta关注");
            mTitles.add("关注Ta");

        }
        mFragments.add(alreadyAtFragment);
        mFragments.add(beFragment);

        initSegTab(type);

        setInputListener();
    }

    private void initSegTab(int type) {
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        sgTabLayout.setTabData(mTitles.toArray(new String[mTitles.size()]));
        sgTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sgTabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setCurrentItem(type);
        viewPager.setOffscreenPageLimit(3);

        viewPager.setScroll(true);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
