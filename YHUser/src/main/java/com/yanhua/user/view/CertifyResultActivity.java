package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

/*
 *实名认证结果
 */
@Route(path = ARouterPath.CERTIFY_RESULT_ACTIVITY)
public class CertifyResultActivity extends BaseMvpActivity {
    @Autowired
    int result;//1 成功

    @BindView(R2.id.bgContaner)
    FrameLayout bgContaner;
    @BindView(R2.id.llSuccess)
    LinearLayout llSuccess;
    @BindView(R2.id.tvRealName)
    TextView tvRealName;
    @BindView(R2.id.tvRealIDNum)
    TextView tvRealIDNum;

    @BindView(R2.id.rlFailed)
    RelativeLayout rlFailed;

    @BindView(R2.id.tvFailed)
    TextView tvFailed;
    @BindView(R2.id.tvReCertify)
    TextView tvReCertify;


    @Override
    public int bindLayout() {
        return R.layout.activity_certify_result;
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle(result == 1 ? "认证中心" : "");

        llSuccess.setVisibility(result == 1 ? View.VISIBLE : View.GONE);
        rlFailed.setVisibility(result != 1 ? View.VISIBLE : View.GONE);

//        bgContaner.setBackgroundColor(mContext.getResources().getColor(result == 1 ? R.color.color_F7F7F7 : R.color.white));//根据状态设置背景颜色

        applyDebouncingClickListener(true, tvFailed, tvReCertify);
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();

        if (id == R.id.tvFailed) {
            onBackPressed();
        } else if (id == R.id.tvReCertify) {
            ARouter.getInstance().build(ARouterPath.CERTIFICATION_ACTIVITY).navigation();
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        if (result == 1) {
            String idNum = UserManager.getInstance().getUserInfo().getIdCard();
            String realName = UserManager.getInstance().getUserInfo().getRealName();

            tvRealName.setText(YHStringUtils.realNameDataMasking(realName));
            tvRealIDNum.setText(YHStringUtils.idCardDataMasking(idNum));
        }
    }
}