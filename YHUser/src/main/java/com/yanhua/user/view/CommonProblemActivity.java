package com.yanhua.user.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.CommonProblemAdapter;
import com.yanhua.user.presenter.CommonProblemPresenter;
import com.yanhua.user.presenter.contract.CommonProblemContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 常见问题页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.COMMON_PROBLEM_ACTIVITY)
public class CommonProblemActivity extends BaseMvpActivity<CommonProblemPresenter> implements CommonProblemContract.IView {

    @BindView(R2.id.rv_problem)
    RecyclerView rvProblem;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyLayout;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    private List<CommonProblemBean> mList = new ArrayList<>();

    private int current = 1;
    private int size = 20;
    private int total;
    private String title;
    private CommonProblemAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new CommonProblemPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_common_problem;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle("常见问题");
        mAdapter = new CommonProblemAdapter(mContext);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            CommonProblemBean commonProblemBean = mList.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.COMMON_PROBLEM_DETAIL_ACTIVITY)
                    .withSerializable("problem", commonProblemBean)
                    .navigation();
        });
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        rvProblem.setLayoutManager(manager);
        rvProblem.setAdapter(mAdapter);
        refreshLayout.setOnRefreshListener(rl -> {
            current = 1;
            getCommonProblem();
        });
        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mList.size() < total) {
                current++;
                getCommonProblem();
            } else {
                rl.finishLoadMoreWithNoMoreData();
            }
        });
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                title = etSearch.getText().toString().trim();
                current = 1;
                getCommonProblem();
                return true;
            }
            return false;
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        refreshLayout.autoRefresh(100);
    }

    private void getCommonProblem() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        if (!TextUtils.isEmpty(title)) {
            params.put("title", title);
        }
        basePresenter.getCommonProblem(params);
    }

    @Override
    public void handleCommonProblemSuccess(ListResult<CommonProblemBean> list) {
        List<CommonProblemBean> records = list.getRecords();
        total = list.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mList.clear();
            if (records != null && !records.isEmpty()) {
                emptyLayout.setVisibility(View.GONE);
                mList = records;
            } else {
                emptyLayout.setVisibility(View.VISIBLE);
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                emptyLayout.setErrorMessage("暂无常见问题");
            }
        } else {
            refreshLayout.finishLoadMore();
            if (records != null && !records.isEmpty()) {
                mList.addAll(records);
            }
        }
        mAdapter.setItems(mList);
    }
}
