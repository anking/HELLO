package com.yanhua.user.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UpgradeInfoModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UpdateShowDialog;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.BuildConfig;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.OSUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.presenter.SettingPresenter;
import com.yanhua.user.presenter.contract.SettingContract;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 关于我们
 *
 * @author Administrator
 */
@Route(path = ARouterPath.ABOUT_US_ACTIVITY)
public class AboutUsActivity extends BaseMvpActivity<SettingPresenter> implements SettingContract.IView {

    @BindView(R2.id.tvVersion)
    TextView tvVersion;
    @BindView(R2.id.tvAppName)
    TextView tvAppName;

    @BindView(R2.id.ll_server_url)
    LinearLayout llServerUrl;
    @BindView(R2.id.rg)
    RadioGroup rg;
    @BindView(R2.id.rbDevelop)
    RadioButton rbDevelop;
    @BindView(R2.id.rbTest)
    RadioButton rbTest;
    @BindView(R2.id.rbUat)
    RadioButton rbUat;
    @BindView(R2.id.edServer)
    EditText edServer;
    @BindView(R2.id.viewRed)
    View viewRed;

    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;

    @Override
    protected void creatPresent() {
        basePresenter = new SettingPresenter();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_about_us;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("关于我们");
        tvAppName.setText(AppUtils.getAppName() + AppUtils.getAppVersionName());
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        basePresenter.fetchAppVersionInfo(0);
    }

    private void setServerUrl() {
        String serverUrl = mMMKV.getServerUrl();
        if (TextUtils.isEmpty(serverUrl)) {
            serverUrl = BuildConfig.BASE_URL;
        }

        //uat---BASE_URL="http://8.134.13.164:8443"
        if ("http://8.134.13.164:8443".equals(serverUrl)) {
            rbUat.setChecked(true);
        } else if ("http://8.134.53.118:9443".equals(serverUrl)) {//test---BASE_URL="http://8.134.53.118:9443"
            rbTest.setChecked(true);
        } else if ("http://121.40.204.59:9443".equals(serverUrl)) {//dev---BASE_URL="http://121.40.204.59:9443"
            rbDevelop.setChecked(true);
        } else {
            edServer.setText(serverUrl);
        }
    }

    private int showServerConfigCount;

    @OnClick({R2.id.rlAppVersion, R2.id.rlUserProtocol, R2.id.rlPrivateProtocol, R2.id.rlContactUs, R2.id.btOK, R2.id.tv_logo})
    public void clickView(View view) {
        int id = view.getId();

        if (id == R.id.rlUserProtocol) {
            if (FastClickUtil.isFastClick(500)) {
                if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                    PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                } else {
                    if (null == userAgreement) {
                        basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                    } else {
                        toProtocolPage(userAgreement);
                    }
                }
            }
        } else if (id == R.id.rlAppVersion) {//
            if (FastClickUtil.isFastClick(500)) {
                onClickVersion();
            }
        } else if (id == R.id.rlPrivateProtocol) {//
            if (FastClickUtil.isFastClick(500)) {
                if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                    PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                } else {
                    if (null == privacyAgreement) {
                        basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                    } else {
                        toProtocolPage(privacyAgreement);
                    }
                }
            }
        } else if (id == R.id.rlContactUs) {//
            ARouter.getInstance().build(ARouterPath.CONTACT_US_ACTIVITY).navigation();
        } else if (id == R.id.tv_logo) {//
            showServerConfigCount++;
            if (showServerConfigCount > YXConfig.MAX_SHOW_CONFIG) {
                if (mContext.getPackageName().equals(YXConfig.APP_PKG_ID)) {//此处只针对非生产可操作
                    llServerUrl.setVisibility(View.VISIBLE);

                    setServerUrl();
                } else {
                    llServerUrl.setVisibility(View.GONE);
                }
                showServerConfigCount = 0;
            }
        } else if (id == R.id.btOK) {//此功能只是为了方便开发测试》》》生产会屏蔽
            String serverUrl = null;

            String inputServer = edServer.getText().toString().trim();
            if (TextUtils.isEmpty(inputServer)) {
                int rbID = rg.getCheckedRadioButtonId();

                if (rbID == R.id.rbDevelop) {
                    serverUrl = "http://121.40.204.59:9443";
                    Toast.makeText(mContext, "develop重启后生效", Toast.LENGTH_SHORT).show();
                } else if (rbID == R.id.rbTest) {
                    serverUrl = "http://8.134.53.118:9443";
                    Toast.makeText(mContext, "Test重启后生效", Toast.LENGTH_SHORT).show();
                } else if (rbID == R.id.rbUat) {
                    serverUrl = "http://8.134.13.164:8443";
                    Toast.makeText(mContext, "uat重启后生效", Toast.LENGTH_SHORT).show();
                }
            } else {
                serverUrl = inputServer;
                Toast.makeText(mContext, "你本地服务重启后生效", Toast.LENGTH_SHORT).show();
            }


            mMMKV.setServerUrl(serverUrl);
            rg.postDelayed(() -> {
                UserManager.getInstance().logout();

                System.exit(0);
            }, 1000);
        }
    }

    private UpgradeInfoModel mUpgradeInfoModel;

    private void onClickVersion() {
        if (mUpgradeInfoModel != null) {
            int versionCode = mUpgradeInfoModel.getVersionNo();
            if (OSUtils.getAppVersion(this) < versionCode) {
                // 本地版本号<服务端配置的版本号，说明有新版本，弹出升级提示框
                UpdateShowDialog.showUpdateDialog(mContext, mUpgradeInfoModel);
            } else {
                ToastUtils.showShort("已是最新版本~");
            }
        }
    }

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    @Override
    public void handleSuccessAppVersionInfo(UpgradeInfoModel upgradeInfoModel) {
        if (upgradeInfoModel != null) {
            mUpgradeInfoModel = upgradeInfoModel;

            int versionCode = upgradeInfoModel.getVersionNo();
            if (OSUtils.getAppVersion(this) < versionCode) {
                viewRed.setVisibility(View.VISIBLE);
            } else {
                viewRed.setVisibility(View.GONE);
            }
            tvVersion.setText(upgradeInfoModel.getVersionName());
        }

        tvVersion.setText(AppUtils.getAppVersionName());
    }

}
