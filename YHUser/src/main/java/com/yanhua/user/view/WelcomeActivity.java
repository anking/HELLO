package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;


import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.user.R;
import com.yanhua.user.adapter.WelcomeAdapter;
import com.yanhua.user.model.WelcomeModel;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.CircleIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * 引导页
 */

@Route(path = ARouterPath.WELCOME_ACTIVITY)
public class WelcomeActivity extends BaseMvpActivity {
    private Banner bannerWelcome;

    @Override
    protected void creatPresent() {

    }
    @Override
    protected boolean isCheckAppStatus() {
        return true;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        bannerWelcome = findViewById(R.id.banner_welcome);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        List<WelcomeModel> mList = new ArrayList<>();
        mList.add(new WelcomeModel(R.mipmap.ic_splash_desc, mContext.getResources().getString(R.string.app_name), mContext.getResources().getString(R.string.app_name)));

        WelcomeAdapter adapter = new WelcomeAdapter(mList);
        adapter.setOnGoClickListener(() -> {
            ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                    .navigation();
            finish();
            mMMKV.setFirst(false);
        });
        bannerWelcome.addBannerLifecycleObserver(this)//添加生命周期观察者
                .setAdapter(adapter)
                .setIndicator(new CircleIndicator(this))
                .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.theme))
                .setIndicatorWidth(DisplayUtils.dip2px(mContext, 5), DisplayUtils.dip2px(mContext, 6))
                .setIndicatorMargins(new IndicatorConfig.Margins(DisplayUtils.dip2px(mContext, 8), 0, DisplayUtils.dip2px(mContext, 4), DisplayUtils.dip2px(mContext, 32)));
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
