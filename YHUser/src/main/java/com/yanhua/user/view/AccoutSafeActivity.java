package com.yanhua.user.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.dialog.CommonDialogContent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.BindStatusView;
import com.yanhua.core.util.RegUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.AuthLoginListener;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.qqmodel.QQ;
import cn.jiguang.share.wechat.Wechat;
import cn.jiguang.share.weibo.SinaWeibo;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.ACCOUT_SAFE_ACTIVITY)
public class AccoutSafeActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.llBindWechat)
    BindStatusView llBindWechat;
    @BindView(R2.id.llBindQQ)
    BindStatusView llBindQQ;
    @BindView(R2.id.llBindWeibo)
    BindStatusView llBindWeibo;

    @BindView(R2.id.tvWechat)
    TextView tvWechat;
    @BindView(R2.id.tvQQ)
    TextView tvQQ;
    @BindView(R2.id.tvWeibo)
    TextView tvWeibo;

    @BindView(R2.id.tvPhone)
    TextView tvPhone;
    @BindView(R2.id.tvPhoneStatus)
    TextView tvPhoneStatus;
    @BindView(R2.id.tvPswdStatus)
    TextView tvPswdStatus;

    private HashMap<String, Object> thirdMap;
    private AuthLoginListener listener;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_accout_safe;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("账号与安全");
        String mobile = UserManager.getInstance().getUserInfo().getMobile();
        if (TextUtils.isEmpty(mobile)) {
            tvPhoneStatus.setText("去绑定");
            tvPhone.setVisibility(View.GONE);
        } else {
            tvPhoneStatus.setText("已绑定");
            tvPhone.setText(String.format("+86 %s", RegUtils.hideMiddleMobile(mobile)));
        }

        UserInfo userInfo = UserManager.getInstance().getUserInfo();

        setBindStatus(userInfo);
    }

    private void setBindStatus(UserInfo userInfo) {
        boolean isSetLoginPswd = userInfo.isLoginPasswordStatus();
        String mobile = UserManager.getInstance().getUserInfo().getMobile();
        tvPswdStatus.setVisibility(isSetLoginPswd ? View.GONE : View.VISIBLE);

        if (!TextUtils.isEmpty(userInfo.getUnionid())) {
            // 绑定了微信
            llBindWechat.setBindStatus(true);
            tvWechat.setVisibility(View.VISIBLE);
            tvWechat.setText(RegUtils.hideMiddleMobile(mobile) + " （" + userInfo.getWeixinNickname() + "）");
        } else {
            llBindWechat.setBindStatus(false);
            tvWechat.setText("");
            tvWechat.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(userInfo.getOpenid())) {
            // 绑定了QQ
            llBindQQ.setBindStatus(true);
            tvQQ.setVisibility(View.VISIBLE);
            tvQQ.setText(RegUtils.hideMiddleMobile(mobile) + " （" + userInfo.getQqNickname() + "）");
        } else {
            llBindQQ.setBindStatus(false);
            tvQQ.setText("");
            tvQQ.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(userInfo.getWeiboOpenid())) {
            // 绑定了微博
            llBindWeibo.setBindStatus(true);
            tvWeibo.setVisibility(View.VISIBLE);
            tvWeibo.setText(RegUtils.hideMiddleMobile(mobile) + " （" + userInfo.getWeiboNickname() + "）");
        } else {
            llBindWeibo.setBindStatus(false);
            tvWeibo.setText("");
            tvWeibo.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick({R2.id.rlPhone, R2.id.rlPswd, R2.id.tvLogOff, R2.id.llBindWechat, R2.id.llBindQQ, R2.id.llBindWeibo,})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.rlPhone) {
            ARouter.getInstance().build(ARouterPath.SET_PHONE_ACTIVITY).withBoolean("bindStatus", true).navigation();
        } else if (id == R.id.rlPswd) {
            //先判断密码是否有设置
            boolean isSetPwsd = UserManager.getInstance().getUserInfo().isLoginPasswordStatus();
            if (isSetPwsd) {
                ARouter.getInstance().build(ARouterPath.MODIFY_PSWD_WAY_ACTIVITY).navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.SET_PSWD_ACTIVITY).withBoolean("fromSet", true).navigation();
            }
        } else if (id == R.id.llBindWechat) {
            boolean bindWechatStatus = llBindWechat.getBindStatus();
            if (bindWechatStatus) {
                CommonDialogContent dialogContent = new CommonDialogContent().init(mContext,
                        "解绑",
                        "是否要解绑微信账号？",
                        new Pair<>("确定", v -> {
                            unBind(YXConfig.thirdPlatform.wechat);
                        }),
                        new Pair<>("取消", v -> {
                        }));
                dialogContent.show();
            } else {
                //绑定
                if (isWeixinAvilible(this)) {
                    doAuthLogin(Wechat.Name);
                } else {
                    ToastUtils.showShort(getString(R.string.wechat_unavailable));
                }
            }
        } else if (id == R.id.llBindQQ) {
            boolean bindQQStatus = llBindQQ.getBindStatus();
            if (bindQQStatus) {
                CommonDialogContent dialogContent = new CommonDialogContent().init(mContext,
                        "解绑",
                        "是否要解绑QQ账号？",
                        new Pair<>("确定", v -> {
                            unBind(YXConfig.thirdPlatform.qq);
                        }),
                        new Pair<>("取消", v -> {
                        }));
                dialogContent.show();
            } else {
                //绑定
                if (isQQClientAvailable(this)) {
                    doAuthLogin(QQ.Name);
                } else {
                    ToastUtils.showShort(getString(R.string.qq_unavailable));
                }
            }
        } else if (id == R.id.llBindWeibo) {
            boolean bindWeiboStatus = llBindWeibo.getBindStatus();

            if (bindWeiboStatus) {
                CommonDialogContent dialogContent = new CommonDialogContent().init(mContext,
                        "解绑",
                        "是否要解绑微博账号？",
                        new Pair<>("确定", v -> {
                            unBind(YXConfig.thirdPlatform.weibo);
                        }),
                        new Pair<>("取消", v -> {
                        }));
                dialogContent.show();
            } else {
                //绑定
                if (isSinaClientAvailable(this)) {
                    doAuthLogin(SinaWeibo.Name);
                } else {
                    ToastUtils.showShort(getString(R.string.weibo_unavailable));
                }
            }
        } else if (id == R.id.tvLogOff) {
            String phone = UserManager.getInstance().getUserInfo().getMobile();
            phone = YHStringUtils.phoneDataMasking(phone);

            //注销---此处的手机号是需要加*
            CommonDialogContent dialogContent = new CommonDialogContent().init(mContext,
                    "将" + phone + "所绑定的账号注销",
                    "注销账号后所有相关信息将被清空无法找回！？",
                    new Pair<>("确定", v -> {
                        //跳转到
                        ARouter.getInstance().build(ARouterPath.LOG_OFF_ACTIVITY).navigation();
                    }),
                    new Pair<>("取消", v -> {
                    }));
            dialogContent.show();
        }
    }

    private void unBind(int platform) {
        if (platform == YXConfig.thirdPlatform.wechat) {
            basePresenter.weixinUntie();
        } else if (platform == YXConfig.thirdPlatform.qq) {
            basePresenter.qqUntie();
        } else if (platform == YXConfig.thirdPlatform.weibo) {
            basePresenter.weiboUntie();
        }
    }

    //QQ.Name, Wechat.Name, SinaWeibo.Name,
    private void doAuthLogin(String platformName) {
        JShareInterface.getUserInfo(platformName, listener);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        listener = new AuthLoginListener();
        listener.setOnAuthCallBack((platform, params) -> {
//            QQ.Name, Wechat.Name, SinaWeibo.Name,
            String platformName = platform.getName();

            if (Wechat.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.wechatAuthLogin(params);
            } else if (QQ.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.qqAuthLogin(params);
            } else if (SinaWeibo.Name.equals(platformName)) {
                thirdMap = params;

                String mobile = null;
                String nickname = (String) params.get("nickname");
                String weiboOpenid = (String) params.get("weiboOpenid");

                HashMap<String, Object> requestParams = new HashMap<>();
                requestParams.put("mobile", mobile);
                requestParams.put("nickname", nickname);
                requestParams.put("weiboOpenid", weiboOpenid);
                basePresenter.weiboAuthLogin(requestParams);
            }
        });
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
    }

    @Override
    public void handleSuccessWeiboRegister(Object result) {
        thirdMap.put("mobile", UserManager.getInstance().getUserInfo().getMobile());
        basePresenter.bindPhone(thirdMap);
    }

    @Override
    public void handleSuccessWechatRegister(Object result) {
        thirdMap.put("mobile", UserManager.getInstance().getUserInfo().getMobile());
        basePresenter.bindPhone(thirdMap);
    }

    @Override
    public void handleSuccessQQRegister(Object result) {
        thirdMap.put("mobile", UserManager.getInstance().getUserInfo().getMobile());
        basePresenter.bindPhone(thirdMap);
    }


    @Override
    public void handleSuccessBindPhone() {
        basePresenter.getUserDetail(UserManager.getInstance().getUserId());
    }

    @Override
    public void handleWechatUntieSuccess() {
        JShareInterface.removeAuthorize(Wechat.Name, listener);

        basePresenter.getUserDetail(UserManager.getInstance().getUserId());
    }

    @Override
    public void handleQQUntieSuccess() {
        JShareInterface.removeAuthorize(QQ.Name, listener);

        basePresenter.getUserDetail(UserManager.getInstance().getUserId());
    }

    @Override
    public void handleWeiboUntieSuccess() {
        JShareInterface.removeAuthorize(SinaWeibo.Name, listener);

        basePresenter.getUserDetail(UserManager.getInstance().getUserId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo, false);
        if (null != userInfo) {
            setBindStatus(userInfo);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
