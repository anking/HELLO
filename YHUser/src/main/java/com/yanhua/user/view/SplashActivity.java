package com.yanhua.user.view;


import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps.MapsInitializer;
import com.amap.api.services.core.ServiceSettings;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionsUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.BaseApplication;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.utils.AppStatusConstant;
import com.yanhua.base.utils.AppStatusManager;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.ProtocolVersionModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.AppPlatformUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.widget.expandabletext.model.UUIDUtils;
import com.yanhua.user.R;
import com.yanhua.user.presenter.ADPresenter;
import com.yanhua.user.presenter.contract.ADContract;
import com.yanhua.user.widget.PrivateProtocolPopup;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Route(path = ARouterPath.SPLASH_ACTIVITY)
public class SplashActivity extends BaseMvpActivity<ADPresenter> implements ADContract.IView {
    private boolean hasPermission = false;
    private final static String ALBUM_PATH
            = Environment.getExternalStorageState() + "/disco/ad/";//getExternalStorageDirectory

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new ADPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        String uuid = mMMKV.getUuid();
        if (TextUtils.isEmpty(uuid)) {
            String uid = UUIDUtils.getUuid();
            mMMKV.setUuid(uid);
        }
    }

    @Override
    protected boolean isCheckAppStatus() {
        return true;
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        basePresenter.queryProtocol();
    }

    @Override
    public void handleProtocolVersion(List<ProtocolVersionModel> data) {
        // 判断有没有显示过用户协议和隐私政策
        showPrivatePopup(data);
    }


    private void setAppStatus() {
        //app状态改为正常
        AppStatusManager.getInstance().setAppStatus(AppStatusConstant.STATUS_NORMAL);
        finish();
    }

    @Override
    public void handleProtocolVersionFail(String msg) {
        // 判断有没有显示过用户协议和隐私政策
        boolean isShownPrivateProtocol = mMMKV.isShowPrivateProtocol();
        if (isShownPrivateProtocol) {
            checkPermission();
        }
    }

    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    private void showPrivatePopup(List<ProtocolVersionModel> list) {
        if (null == list) {
            checkPermission();
            return;
        }
        int userVersionNumCache = mMMKV.getShowUserProtocolVersion();
        int privacyVersionNumCache = mMMKV.getShowPrivacyProtocolVersion();

//        String userIdCache = mMMKV.getShowUserProtocolId();
//        String privacyIdCache = mMMKV.getShowPrivacyProtocolId();

        ProtocolVersionModel use = null, privacy = null;
        for (ProtocolVersionModel item : list) {
            String code = item.getCode();

            if (code.equals(YXConfig.protocol.userAgreement)) {
                use = item;
            } else if (code.equals(YXConfig.protocol.privacyAgreement)) {
                privacy = item;
            }
        }

        int userVersionNum = use.getVersion();
        int privacyVersionNum = privacy.getVersion();
        String userId = use.getId();
        String privacyId = privacy.getId();

        if (userVersionNumCache < userVersionNum || privacyVersionNumCache < privacyVersionNum) {
            PrivateProtocolPopup privateProtocolPopup = new PrivateProtocolPopup(mContext);
            privateProtocolPopup.setOnOperateClickListener(type -> {
                switch (type) {
                    case PrivateProtocolPopup.AGREE:
                        mMMKV.setShowPrivateProtocol(true);
                        mMMKV.setShowUserProtocolId(userId);
                        mMMKV.setShowUserProtocolVersion(userVersionNum);

                        mMMKV.setShowPrivacyProtocolId(privacyId);
                        mMMKV.setShowPrivacyProtocolVersion(privacyVersionNum);

                        //签署协议
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("deviceId", mMMKV.getUuid());
                        params.put("protocolIds", userId + "," + privacyId);
                        if (UserManager.getInstance().isLogin()) {
                            params.put("userId", UserManager.getInstance().getUserId());
                        }

                        basePresenter.userSignProtocol(params);

                        AppPlatformUtil.initPlatformSDK(BaseApplication.getInstance());

                        break;
                    case PrivateProtocolPopup.DISAGREE:
                        setAppStatus();
                        break;

                    case PrivateProtocolPopup.USER://用户协议
                        if (FastClickUtil.isFastClick(500)) {
                            if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                                PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                            } else {
                                if (null == userAgreement) {
                                    basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                                } else {
                                    toProtocolPage(userAgreement);
                                }
                            }
                        }
                        break;

                    case PrivateProtocolPopup.PRIVASTE://隐私协议
                        if (FastClickUtil.isFastClick(500)) {
                            if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                                PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                            } else {
                                if (null == privacyAgreement) {
                                    basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                                } else {
                                    toProtocolPage(privacyAgreement);
                                }
                            }
                        }
                        break;
                }
            });
            new XPopup.Builder(mContext)
                    .dismissOnTouchOutside(false)
                    .dismissOnBackPressed(false)
                    .asCustom(privateProtocolPopup)
                    .show();
        } else {
            checkPermission();
        }
    }

    @Override
    public void handActionSuccess(int type) {
        checkPermission();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void hideLoading() {

    }

    private void checkPermission() {
        String[] permissionsO = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        hasPermission = PermissionsUtil.hasPermission(this, permissionsO);

        // 表示用户同意定位功能隐私协议
        AMapLocationClient.updatePrivacyShow(this, true, true);
        AMapLocationClient.updatePrivacyAgree(this, true);

        // 表示用户同意地图功能隐私协议
        MapsInitializer.updatePrivacyShow(this, true, true);
        MapsInitializer.updatePrivacyAgree(this, true);

        // 表示用户同意搜索功能隐私协议
        ServiceSettings.updatePrivacyShow(this, true, true);
        ServiceSettings.updatePrivacyAgree(this, true);

        loadAd();
    }

    /**
     * 加载广告
     */
    private void loadAd() {
//         获取启动广告
//         有网络从网络获取，无网络时从缓存加载
        boolean netWorkAvailable = NetworkUtils.isConnected();
        if (netWorkAvailable) {
            basePresenter.getAdvertise(YXConfig.AD.s_adv);
        } else {
            if (hasPermission) {
                // 从缓存获取
                loadAdFromCache();
            } else {
                jumpPage();
            }
        }
    }

    @Override
    public void handSystemError(String action, String mesg) {
        if (action.equals(HttpCode.NET_ERROR)) {
            ToastUtils.showShort("系统升级中~");
        }
        //放此避免服务器此接口异常，无法使用app
        loadAdFromCache();
    }

    @Override
    public void handleErrorMessage(String errorMsg) {
        ToastUtils.showShort(errorMsg);
        //放此避免服务器此接口异常，无法使用app
        loadAdFromCache();
    }

    @Override
    public void handleAdvertiseSuccess(List<AdvertiseModel> advertiseList) {
        if (advertiseList != null && !advertiseList.isEmpty()) {
            // 跳转到启动广告展示页面
            ARouter.getInstance().build(ARouterPath.AD_ACTIVITY)
                    .withSerializable("advertiseModelList", (Serializable) advertiseList)
                    .withTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
                    .navigation();
            // 有访问权限就下载缓存
            if (hasPermission) {
                // 保存广告json
                String adJson = new Gson().toJson(advertiseList);
                mMMKV.setAdJson(adJson);
            }

            setAppStatus();
        } else {
            jumpPage();
        }
    }

    private void loadAdFromCache() {
        String adjson = mMMKV.getAdJson();
        if (TextUtils.isEmpty(adjson)) {
            jumpPage();
            return;
        }

        Type listType = new TypeToken<ArrayList<AdvertiseModel>>() {
        }.getType();
        ArrayList<AdvertiseModel> advertiseModelList = GsonUtils.fromJson(adjson, listType);

        if (null != advertiseModelList && advertiseModelList.size() > 0) {
            ARouter.getInstance().build(ARouterPath.AD_ACTIVITY)
                    .withSerializable("advertiseModelList", advertiseModelList)
                    .withTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
                    .navigation();

            setAppStatus();
        } else {
            jumpPage();
        }
    }

    private void jumpPage() {
        if (mMMKV.isLogin()) {
            ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                    .navigation();
        } else {
            if (mMMKV.isFirst()) {
                ARouter.getInstance().build(ARouterPath.WELCOME_ACTIVITY)
                        .navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                        .navigation();
            }
        }

        setAppStatus();
    }
}