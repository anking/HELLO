package com.yanhua.user.view;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.Gson;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.fragment.LoginOneKeyFragment;
import com.yanhua.user.fragment.LoginPasswordFragment;
import com.yanhua.user.fragment.LoginPhoneCodeFragment;
import com.yanhua.user.listener.AuthLoginListener;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.qqmodel.QQ;
import cn.jiguang.share.wechat.Wechat;
import cn.jiguang.share.weibo.SinaWeibo;

@Route(path = ARouterPath.LOGIN_ACTIVITY)
public class LoginActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {
    @BindView(R2.id.iv_login_wechat)
    ImageView ivLoginWechat;

    @BindView(R2.id.iv_login_qq)
    ImageView ivLoginQQ;
    @BindView(R2.id.iv_login_weibo)
    ImageView ivLoginWeibo;

    @BindView(R2.id.rootView)
    RelativeLayout rootView;

    @BindView(R2.id.ll_login_bottom)
    LinearLayout llLoginBottom;

    @Autowired
    int loginType;

    public static final int ONE_KEY_LOGIN = 1;
    public static final int PSWD_LOGIN = 2;
    public static final int PHONE_CODE_LOGIN = 3;

    public static final int VERIFY_CONSISTENT = 9000;//手机号验证一致
    public static final int FETCH_TOKEN_SUCCESS = 2000;//获取token成功
    public static final int CODE_LOGIN_SUCCESS = 6000;
    public static final int CODE_LOGIN_FAILED = 6001;
    public static final int CODE_LOGIN_CANCELD = 6002;
    public static final int ACTION_LOGIN_SUCCESS = 5;
    public static final int ACTION_LOGIN_FAILED = 6;
    public static final int ACTION_NATIVE_VERIFY_SUCCESS = 7;
    public static final int ACTION_VERIFY_FAILED = 8;

    private HashMap<String, Object> thirdMap;
    private AuthLoginListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        listener = new AuthLoginListener();
        listener.setOnAuthCallBack((platform, params) -> {
//            QQ.Name, Wechat.Name, SinaWeibo.Name,
            String platformName = platform.getName();

            if (Wechat.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.wechatAuthLogin(params);
            } else if (QQ.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.qqAuthLogin(params);
            } else if (SinaWeibo.Name.equals(platformName)) {
                thirdMap = params;

                String mobile = null;
                String nickname = (String) params.get("nickname");
                String weiboOpenid = (String) params.get("weiboOpenid");

                HashMap<String, Object> requestParams = new HashMap<>();
                requestParams.put("mobile", mobile);
                requestParams.put("nickname", nickname);
                requestParams.put("weiboOpenid", weiboOpenid);
                basePresenter.weiboAuthLogin(requestParams);
            }
        });
//        checkPhonePermission();

        setPageInit();
    }

    private void checkPhonePermission() {
        String[] permissionsO = new String[]{
                Manifest.permission.READ_PHONE_STATE};

        requestCheckPermission(permissionsO, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                setPageInit();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_phone_permission_tip);
                setPageInit();
            }
        });
    }

    private void setPageInit() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        switch (loginType) {
            case PHONE_CODE_LOGIN:
                LoginPhoneCodeFragment fragment = new LoginPhoneCodeFragment();

                transaction.replace(R.id.container, fragment);
                transaction.commit();
                break;

            case PSWD_LOGIN:
            default:
                //如果手机不支持一键登录环境
                LoginPasswordFragment fragmentPswd = new LoginPasswordFragment();

                transaction.replace(R.id.container, fragmentPswd);
                transaction.commit();
                break;
        }



        /*boolean verifyEnable = JVerificationInterface.checkVerifyEnable(mContext);
        if (!verifyEnable) {
            FragmentManager manager = getSupportFragmentManager();
            LoginPasswordFragment fragment = new LoginPasswordFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else {
            JVerificationInterface.clearPreLoginCache();
            //autoFinish 可以设置是否在点击登录的时候自动结束授权界面
            JVerificationInterface.loginAuth(this, true, new VerifyListener() {
                @Override
                public void onResult(final int code, final String content, final String operator) {
                    LogUtils.d("[" + code + "]message=" + content + ", operator=" + operator);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (code == CODE_LOGIN_SUCCESS) {
                                toSuccessActivity(ACTION_LOGIN_SUCCESS, content);
                            } else if (code != CODE_LOGIN_CANCELD) {
                            }
                        }
                    });
                }
            }, new AuthPageEventListener() {
                @Override
                public void onEvent(int cmd, String msg) {
                }
            });
        }*/
    }


    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo);
        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);

        ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY).navigation();
        finish();
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        //底部增加间距
        applyDebouncingClickListener(true, ivLoginWechat, ivLoginQQ, ivLoginWeibo);
    }

    public void onDebouncingClick(View v) {
        int id = v.getId();
        thirdPlatformLogin(id);
    }

    private void thirdPlatformLogin(int id) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        boolean isProtocolChecked = false;
        if (fragment instanceof LoginOneKeyFragment) {
            isProtocolChecked = ((LoginOneKeyFragment) fragment).isProtocolChecked();
        } else if (fragment instanceof LoginPhoneCodeFragment) {
            isProtocolChecked = ((LoginPhoneCodeFragment) fragment).isProtocolChecked();
        } else if (fragment instanceof LoginPasswordFragment) {
            isProtocolChecked = ((LoginPasswordFragment) fragment).isProtocolChecked();
        }

        if (!isProtocolChecked) {
            ToastUtils.showShort("请先阅读并同意相关协议");
            return;
        }

        if (id == R.id.iv_login_qq) {//
            if (isQQClientAvailable(this)) {
                doAuthLogin(QQ.Name);
            } else {
                ToastUtils.showShort(getString(R.string.qq_unavailable));
            }
        } else if (id == R.id.iv_login_wechat) {//
            if (isWeixinAvilible(this)) {
                doAuthLogin(Wechat.Name);
            } else {
                ToastUtils.showShort(getString(R.string.wechat_unavailable));
            }
        } else if (id == R.id.iv_login_weibo) {//
            if (isSinaClientAvailable(this)) {
                doAuthLogin(SinaWeibo.Name);
            } else {
                ToastUtils.showShort(getString(R.string.weibo_unavailable));
            }
        }
    }

    //QQ.Name, Wechat.Name, SinaWeibo.Name,
    private void doAuthLogin(String platformName) {
        JShareInterface.getUserInfo(platformName, listener);
    }

    /**
     * 设置背景
     *
     * @param type
     */
    public void setRootViewBackgroud(int type) {
        switch (type) {
            case ONE_KEY_LOGIN:
                rootView.setBackgroundResource(R.mipmap.bg_user);
                break;
            case PSWD_LOGIN:
            case PHONE_CODE_LOGIN:
                rootView.setBackgroundColor(getResources().getColor(R.color.bg_login));
                break;
            default:
                rootView.setBackgroundColor(getResources().getColor(R.color.bg_login));
                break;
        }
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public void handleSuccessWeiboRegister(Object result) {
        if (null == result) {
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 3)// QQ传1 微博 3
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);
                JSONObject jsonObject = new JSONObject(json);
                String weiboOpenid = jsonObject.optString("weiboOpenid");
                basePresenter.login(weiboOpenid, "", "weiboApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSuccessWechatRegister(Object result) {
        if (null == result) {
            // 未绑定手机号
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 1)// 微信传1
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);

                JSONObject jsonObject = new JSONObject(json);
                String unionid = jsonObject.optString("unionid");
                basePresenter.login(unionid, "", "weixinMobileApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSuccessQQRegister(Object result) {
        if (null == result) {
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 2)// QQ传1
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);
                JSONObject jsonObject = new JSONObject(json);
                String openid = jsonObject.optString("openid");
                basePresenter.login(openid, "", "qqApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        String eventName = event.getEventName();
        if (EventName.LOGIN_PAGE_CLOSE.equals(eventName)) {
            finish();
        }
    }

}