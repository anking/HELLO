package com.yanhua.user.view;

import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.dialog.CommonDialogContent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.CacheDataManager;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 设置密码
 */
@Route(path = ARouterPath.SET_ACTIVITY)
public class SetActivity extends BaseMvpActivity {

    @BindView(R2.id.tvCacheData)
    TextView tvCacheData;

    @BindView(R2.id.tvLogout)
    TextView tvLogout;

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("设置");
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initCacheView();

        tvLogout.setVisibility(UserManager.getInstance().isLogin() ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_set;
    }

    @OnClick({R2.id.tvLogout, R2.id.rlAccoutSafe, R2.id.rlPrivate, R2.id.rlClearCache, R2.id.rlAboutUs})
    public void onViewClick(View v) {
        int id = v.getId();
        if (id == R.id.tvLogout) {
            CommonDialogContent dialogContent = new CommonDialogContent().init(mContext,
                    "退出登录",
                    "是否要退出登录？",
                    new Pair<>("确定", vv -> {
                        UserManager.getInstance().logout();
                        onBackPressed();
                    }),
                    new Pair<>("取消", vv -> {
                    }));
            dialogContent.show();
        } else if (id == R.id.rlAccoutSafe) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.ACCOUT_SAFE_ACTIVITY).navigation();
            });
        } else if (id == R.id.rlPrivate) {
            ARouter.getInstance().build(ARouterPath.SET_PRIVATE_ACTIVITY).navigation();
        } else if (id == R.id.rlClearCache) {
            CacheDataManager.clearAllCache(mContext);
            initCacheView();
            ToastUtils.showShort("清理缓存成功");
        } else if (id == R.id.rlAboutUs) {
            ARouter.getInstance().build(ARouterPath.ABOUT_US_ACTIVITY).navigation();
        }
    }

    private void initCacheView() {
        try {
            String cacheSize = CacheDataManager.getTotalCacheSize(mContext);
            tvCacheData.setText(cacheSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}