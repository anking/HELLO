package com.yanhua.user.view;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.encrypt.RSACoderUtil;
import com.yanhua.user.R;
import com.yanhua.user.listener.AuthLoginListener;
import com.yanhua.user.model.Constants;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.jiguang.share.android.api.JShareInterface;
import cn.jiguang.share.qqmodel.QQ;
import cn.jiguang.share.wechat.Wechat;
import cn.jiguang.share.weibo.SinaWeibo;
import cn.jiguang.verifysdk.api.AuthPageEventListener;
import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.JVerifyUIConfig;
import cn.jiguang.verifysdk.api.PrivacyBean;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.yanhua.user.model.Constants.CODE_LOGIN_CANCELD;
import static com.yanhua.user.model.Constants.CODE_LOGIN_SUCCESS;

@Route(path = ARouterPath.VERIFY_PHONE_ACTIVITY)
public class VerifyPhoneActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {
    private static final String TAG = "VerifyActivity";
    private boolean useCustomerPrivacy = false;//协议不能自己自定义
    TextView tvPro;
    ITCheckBox checkboxPro;
    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;
    private ProtocolModel oneKeyLoginProtocol;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        loginAuth();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_tran;
    }

    private void getPhoneNum(final String token) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    String content = YXConfig.JPUSH_APPKEY + ":" + YXConfig.JPUSH_MSK;

                    content = Base64.encodeToString(content.getBytes("UTF-8"), Base64.NO_WRAP);
                    OkHttpClient client = new OkHttpClient();
                    JSONObject bodyJson = new JSONObject();
                    bodyJson.put("loginToken", token);
                    String body = bodyJson.toString();
                    RequestBody requestBody = RequestBody.create(YXConfig.JSON, body);

                    Request request = new Request.Builder().url(Constants.verifyUrl).post(requestBody)
                            .addHeader("Authorization", "Basic " + content).
                                    build();

                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    JSONObject responseJson = new JSONObject(responseData);
                    String phone = responseJson.optString("phone");

                    String decryptPhone = RSACoderUtil.decryptRSA(phone, YXConfig.prikey);
                    //登录
                    basePresenter.login("", "", "yijianApplications", decryptPhone);
                } catch (Throwable e) {
                }
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void loginAuth() {
        int result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            result = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (result != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "[2016],msg = 当前缺少权限", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(this);
        if (!verifyEnable) {
            Toast.makeText(this, "[2016],msg = 当前网络环境不支持认证", Toast.LENGTH_SHORT).show();
            return;
        }

        showLoading();
        JVerificationInterface.clearPreLoginCache();
        setUIConfig();
        //autoFinish 可以设置是否在点击登录的时候自动结束授权界面
        JVerificationInterface.loginAuth(this, false, (code, content, operator) -> {
            hideLoading();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (code == CODE_LOGIN_SUCCESS) {
                        getPhoneNum(content);
                    } else if (code != CODE_LOGIN_CANCELD) {
                    }
                }
            });
        }, new AuthPageEventListener() {
            @Override
            public void onEvent(int cmd, String msg) {
//                1	login activity closed.	授权页关闭事件
//                2	login activity started.	授权页打开事件
//                3	carrier privacy clicked.	运营商协议点击事件
//                4	privacy 1 clicked.	自定义协议1点击事件
//                5	privacy 2 clicked.	自定义协议2点击事件
//                6	checkbox checked.	协议栏checkbox变为选中事件
//                7	checkbox unchecked.	协议栏checkbox变为未选中事件
//                8	login button clicked.	一键登录按钮（可用状态下）点击事件
                switch (cmd) {
                    case 1:
                        EventBus.getDefault().post(new MessageEvent(EventName.LOGIN_PAGE_CLOSE));
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        isProtocolChecked = true;
                        break;
                    case 7:
                        isProtocolChecked = false;
                        break;
                    case 8:
                        break;
                }
            }
        });
    }

    private void setUIConfig() {
        JVerifyUIConfig portrait = getFullScreenPortraitConfig();
        //支持授权页设置横竖屏两套config，在授权页中触发横竖屏切换时，sdk自动选择对应的config加载。
        JVerificationInterface.setCustomUIWithConfig(portrait);
    }

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            } else if (YXConfig.protocol.oneKeyLoginProtocol.equals(code)) {
                oneKeyLoginProtocol = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    //设置一键登录的协议描述
    public void setOneKeyPro() {
        //注册/登录即表示同意《中国移动认证服务条款》、音圈25 Disco服务条款34 和隐私条款";
        String textContent = "注册/登录即表示同意《中国移动认证服务条款》、音圈Disco服务条款和隐私条款";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (null == oneKeyLoginProtocol) {
                        basePresenter.getAppProtocol(YXConfig.protocol.oneKeyLoginProtocol);
                    } else {
                        toProtocolPage(oneKeyLoginProtocol);
                    }
                }
            }
        }, 10, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                    } else {
                        if (null == userAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                        } else {
                            toProtocolPage(userAgreement);
                        }
                    }
                }
            }
        }, 25, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                    } else {
                        if (null == privacyAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                        } else {
                            toProtocolPage(privacyAgreement);
                        }
                    }
                }
            }
        }, 35, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private JVerifyUIConfig getFullScreenPortraitConfig() {
        JVerifyUIConfig.Builder uiConfigBuilder = new JVerifyUIConfig.Builder();
        uiConfigBuilder.setAuthBGImgPath("bg_verification");
        uiConfigBuilder.setStatusBarTransparent(true);
        uiConfigBuilder.setStatusBarDarkMode(true);

        //导航栏
        uiConfigBuilder.setNavText("");
        uiConfigBuilder.setNavTransparent(true);
        uiConfigBuilder.setNavReturnImgPath("icon_back_verification");

        //LOGO
        uiConfigBuilder.setLogoOffsetY(60);
        uiConfigBuilder.setLogoImgPath("ic_jverification_logo");

        //运营商
        uiConfigBuilder.setSloganTextColor(0xFFB4B8BC);
        uiConfigBuilder.setSloganTextSize(12);
        uiConfigBuilder.setSloganOffsetY(192);
        uiConfigBuilder.setSloganHidden(false);

        //号码显示
        uiConfigBuilder.setNumFieldOffsetY(218);
        uiConfigBuilder.setNumberColor(0xFF36384A);//#36384A
        uiConfigBuilder.setNumberSize(22);//
        uiConfigBuilder.setNumberTextBold(true);//

        //登录按钮
        uiConfigBuilder.setLogBtnImgPath("selector_button");
        uiConfigBuilder.setLogBtnTextColor(0xFFFFFFFF);
        uiConfigBuilder.setLogBtnText("本机号码一键登录");
        uiConfigBuilder.setLogBtnTextSize(16);
        uiConfigBuilder.setLogBtnOffsetY(264);
        uiConfigBuilder.setLogBtnWidth(px2dip(mContext, DisplayUtils.getScreenW(mActivity)) - 40);
        uiConfigBuilder.setLogBtnHeight(56);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        if (!useCustomerPrivacy) {
            uiConfigBuilder.setPrivacyState(false);
            uiConfigBuilder.setCheckedImgPath("checkbox_checked64");
            uiConfigBuilder.setUncheckedImgPath("checkbox_unchecked64");
            uiConfigBuilder.setPrivacyCheckboxHidden(false);
            uiConfigBuilder.setPrivacyOffsetX(20);
            uiConfigBuilder.setPrivacyCheckboxSize(16);
            uiConfigBuilder.setAppPrivacyColor(0xFF36384A, 0xFF783AD8);
            uiConfigBuilder.setPrivacyTopOffsetY(370);

            //最后如果不OK可以自定义这一块
            //android:text="注册/登录即表示同意《中国移动认证服务条款》、音圈Disco服务条款和隐私条款"
            uiConfigBuilder.setPrivacyText("注册/登录即表示同意《", "");
            PrivacyBean p1 = new PrivacyBean("服务条款", YXConfig.getUserAgreement(), "》并授权音圈获取本机号码、");
            PrivacyBean p2 = new PrivacyBean("隐私条款", YXConfig.getPrivacyAgreement(), "和");
            List<PrivacyBean> list = new ArrayList<>();
            list.add(p1);
            list.add(p2);
            uiConfigBuilder.setPrivacyNameAndUrlBeanList(list);
            uiConfigBuilder.setPrivacyTextSize(12);

            //协议详情展示
            uiConfigBuilder.setPrivacyNavColor(0xFFFFFFFF);
            uiConfigBuilder.setPrivacyNavTitleTextColor(0xFF36384A);
            uiConfigBuilder.setPrivacyNavTitleTextSize(18);//设置协议展示web页面导航栏标题文字大小
            RelativeLayout.LayoutParams layoutParamBackIcon = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParamBackIcon.setMargins(dp2Pix(this, 12.0f), 0, 0, 0);
            layoutParamBackIcon.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            layoutParamBackIcon.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

            ImageView backIcon = new ImageView(this);
            backIcon.setImageResource(R.drawable.back_black);
            backIcon.setLayoutParams(layoutParamBackIcon);
            uiConfigBuilder.setPrivacyNavReturnBtn(backIcon);
            uiConfigBuilder.setPrivacyStatusBarColorWithNav(false);
            uiConfigBuilder.setPrivacyStatusBarDarkMode(true);
            uiConfigBuilder.setPrivacyStatusBarTransparent(false);
        } else {
            View viewPrivacy = inflater.inflate(R.layout.verification_priv, null);

            checkboxPro = viewPrivacy.findViewById(R.id.checkbox_pro);
            tvPro = viewPrivacy.findViewById(R.id.tv_pro);

            checkboxPro.setOnClickListener(v -> {
                boolean checked = checkboxPro.isChecked();
                checkboxPro.setChecked(!checked);
            });

            RelativeLayout.LayoutParams layoutParamPSWDLogin = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParamPSWDLogin.setMargins(0, dp2Pix(this, 370.f), 0, 0);
            layoutParamPSWDLogin.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            viewPrivacy.setLayoutParams(layoutParamPSWDLogin);
            uiConfigBuilder.addCustomView(viewPrivacy, false, null);
            setOneKeyPro();
        }
        // 其他登录
        View view = inflater.inflate(R.layout.verification_login_ways, null);
        TextView tvPhoneLogin = view.findViewById(R.id.tv_login_phoneCode);
        TextView tvPswdLogin = view.findViewById(R.id.tv_login_password);

        tvPhoneLogin.setOnClickListener(v -> {
            JVerificationInterface.dismissLoginAuthActivity(true, (code, desc) -> {
            });

            ARouter.getInstance().build(ARouterPath.LOGIN_ACTIVITY).withInt("loginType", 3).navigation();
        });

        tvPswdLogin.setOnClickListener(v -> {
            JVerificationInterface.dismissLoginAuthActivity(true, (code, desc) -> {
            });
            ARouter.getInstance().build(ARouterPath.LOGIN_ACTIVITY).withInt("loginType", 2).navigation();
        });

        RelativeLayout.LayoutParams layoutParamPrivacy = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParamPrivacy.setMargins(0, dp2Pix(this, 330.0f), 0, 0);
        layoutParamPrivacy.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParamPrivacy);
        uiConfigBuilder.addCustomView(view, false, null);

        // 微信qq新浪登录
        View viewPlatform = inflater.inflate(R.layout.verification_platform_login, null);
        viewPlatform.findViewById(R.id.iv_login_wechat).setOnClickListener(v -> thirdPlatformLogin(Wechat.Name));
        viewPlatform.findViewById(R.id.iv_login_qq).setOnClickListener(v -> thirdPlatformLogin(QQ.Name));
        viewPlatform.findViewById(R.id.iv_login_weibo).setOnClickListener(v -> thirdPlatformLogin(SinaWeibo.Name));

        RelativeLayout.LayoutParams layoutLoginGroupParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutLoginGroupParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        viewPlatform.setLayoutParams(layoutLoginGroupParam);
        uiConfigBuilder.addCustomView(viewPlatform, false, null);

        return uiConfigBuilder.build();
    }

    private int dp2Pix(Context context, float dp) {
        try {
            float density = context.getResources().getDisplayMetrics().density;
            return (int) (dp * density + 0.5F);
        } catch (Exception e) {
            return (int) dp;
        }
    }

    private int px2dip(Context context, int pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }


    private HashMap<String, Object> thirdMap;
    private AuthLoginListener listener;

    @Override
    public void doBusiness() {
        super.doBusiness();

        listener = new AuthLoginListener();
        listener.setOnAuthCallBack((platform, params) -> {
//            QQ.Name, Wechat.Name, SinaWeibo.Name,
            String platformName = platform.getName();

            if (Wechat.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.wechatAuthLogin(params);
            } else if (QQ.Name.equals(platformName)) {
                thirdMap = params;
                basePresenter.qqAuthLogin(params);
            } else if (SinaWeibo.Name.equals(platformName)) {
                thirdMap = params;

                String mobile = null;
                String nickname = (String) params.get("nickname");
                String weiboOpenid = (String) params.get("weiboOpenid");

                HashMap<String, Object> requestParams = new HashMap<>();
                requestParams.put("mobile", mobile);
                requestParams.put("nickname", nickname);
                requestParams.put("weiboOpenid", weiboOpenid);
                basePresenter.weiboAuthLogin(requestParams);
            }
        });
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {

        JVerificationInterface.dismissLoginAuthActivity(true, (code, desc) -> {
        });

        UserManager.getInstance().setUserInfo(userInfo);
        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);

        ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY).navigation();
        finish();
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    boolean isProtocolChecked = false;

    /**
     * QQ.Name Wechat.Name SinaWeibo.Name
     *
     * @param name
     */
    private void thirdPlatformLogin(String name) {
        isProtocolChecked = useCustomerPrivacy ? checkboxPro.isChecked() : isProtocolChecked;

        if (!isProtocolChecked) {
            ToastUtils.showShort("请先阅读并同意相关协议");
            return;
        }

        if (QQ.Name.equals(name)) {//
            if (isQQClientAvailable(this)) {
                doAuthLogin(QQ.Name);
            } else {
                ToastUtils.showShort(getString(R.string.qq_unavailable));
            }
        } else if (Wechat.Name.equals(name)) {//
            if (isWeixinAvilible(this)) {
                doAuthLogin(Wechat.Name);
            } else {
                ToastUtils.showShort(getString(R.string.wechat_unavailable));
            }
        } else if (SinaWeibo.Name.equals(name)) {//
            if (isSinaClientAvailable(this)) {
                doAuthLogin(SinaWeibo.Name);
            } else {
                ToastUtils.showShort(getString(R.string.weibo_unavailable));
            }
        }
    }

    //QQ.Name, Wechat.Name, SinaWeibo.Name,
    private void doAuthLogin(String platformName) {
        JShareInterface.getUserInfo(platformName, listener);
    }


    @Override
    public void handleSuccessWeiboRegister(Object result) {
        if (null == result) {
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 3)// QQ传1 微博 3
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);
                JSONObject jsonObject = new JSONObject(json);
                String weiboOpenid = jsonObject.optString("weiboOpenid");
                basePresenter.login(weiboOpenid, "", "weiboApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSuccessWechatRegister(Object result) {
        if (null == result) {
            // 未绑定手机号
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 1)// 微信传1
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);

                JSONObject jsonObject = new JSONObject(json);
                String unionid = jsonObject.optString("unionid");
                basePresenter.login(unionid, "", "weixinMobileApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSuccessQQRegister(Object result) {
        if (null == result) {
            // 未绑定手机号，跳转绑定页面
            ARouter.getInstance()
                    .build(ARouterPath.BIND_PHONE_ACTIVITY)
                    .withSerializable("params", thirdMap)
                    .withInt("type", 2)// QQ传1
                    .navigation(mActivity);
        } else {
            // 绑定过手机号，做登录操作
            try {
                String json = new Gson().toJson(result);
                JSONObject jsonObject = new JSONObject(json);
                String openid = jsonObject.optString("openid");
                basePresenter.login(openid, "", "qqApplications", null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        String eventName = event.getEventName();
        if (EventName.LOGIN_PAGE_CLOSE.equals(eventName)) {
            finish();
        }
    }
}
