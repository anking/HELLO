package com.yanhua.user.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.common.model.DealResult;

import java.io.Serializable;

import butterknife.BindView;

import static androidx.annotation.Dimension.SP;

/**
 * 绑定手机
 *
 */
@Route(path = ARouterPath.DEAL_RESULT_ACTIVITY)
public class DealResultActivity extends BaseMvpActivity {

    @BindView(R2.id.tv_go_page)
    TextView tvGoPage;
    @BindView(R2.id.iv_result)
    AliIconFontTextView ivResult;
    @BindView(R2.id.tv_result)
    TextView tvResult;
    @BindView(R2.id.tv_result_desc)
    TextView tvResultDesc;
    @BindView(R2.id.tvSubJump)
    TextView tvSubJump;


    @Autowired(name = "dealResult")
    DealResult dealResult;

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("提交成功");

        //从参数中获取这些值并且动态设置
        ivResult.setText(dealResult.getIvResultText());
        ivResult.setTextSize(SP,dealResult.getIvResultSize());
        ivResult.setTextColor(mContext.getResources().getColor(dealResult.getIvResultColor()));

        tvResult.setText(dealResult.getTvResultText());
        tvResultDesc.setText(dealResult.getTvResultDescText());

        tvGoPage.setText(dealResult.getTvGoPageText());
        tvSubJump.setText(dealResult.getSubText());
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvGoPage,tvSubJump);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_go_page) {//
            if (TextUtils.isEmpty(dealResult.getTvGoPage())){
                onBackPressed();
                return;
            }
            ARouter.getInstance()
                    .build(dealResult.getTvGoPage())
                    .withSerializable("pageParams", (Serializable) dealResult.getGoToObj())
                    .navigation();
        }else if(id == R.id.tvSubJump){
            if (TextUtils.isEmpty(dealResult.getSubPage())){

                return;
            }
            ARouter.getInstance()
                    .build(dealResult.getSubPage())
                    .withSerializable("pageParams", (Serializable) dealResult.getSubObj())
                    .navigation();
        }
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean getIsPadding() {
        return true;
    }
    @Override
    public int getStatusColor() {
        return R.color.white;
    }
    @Override
    public int bindLayout() {
        return R.layout.activity_deal_result;
    }
}