package com.yanhua.user.view;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.maps.model.animation.AlphaAnimation;
import com.amap.api.maps.model.animation.AnimationSet;
import com.amap.api.maps.model.animation.ScaleAnimation;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.blankj.utilcode.util.NetworkUtils;
import com.yanhua.base.activity.CommonActivity;
import com.yanhua.common.config.LocationConst;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.StatusBarUtil;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.user.R;
import com.yanhua.user.listener.IMyLocationChangedListener;
import com.yanhua.user.location.LocationManager;
import com.yanhua.user.model.AMapLocationInfo;

import java.util.ArrayList;
import java.util.List;

@Route(path = ARouterPath.AMAP_LOCATION_SELECT_ACTIVITY)
public class AMapLocationSelectActivity extends CommonActivity implements
        LocationSource,
        AMap.OnCameraChangeListener,
        GeocodeSearch.OnGeocodeSearchListener,
        IMyLocationChangedListener,
        View.OnClickListener {

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 100;
    private static final int PAGE_COUNT = 20;
    private int currentPage = 1;
    private int mTouchSlop;//系统值

    private BitmapDescriptor mBitmapDescriptor;
    private MapView mAMapView;
    private AMap mAMap;
    private LinearLayout llList;
    private LinearLayout llSearch;
    private LinearLayout llsearch;
    private AutoClearEditText etSearch;
    private Handler mHandler;
    private Marker mMarker;
    private GeocodeSearch mGeocodeSearch;
    private OnLocationChangedListener mLocationChangedListener;

    private double mMyLat;
    private double mMyLng;
    private String mMyPoi;
    private String mMyCity;
    private String mMyAddress;

    private double mLatResult;
    private double mLngResult;
    private String mPoiResult;
    private boolean mUpdateNearby;

    private ListView listViewNearby;
    private NearbyListAdapter nearbyListAdapter;
    private ProgressBar listLoadingView;
    private float Y;
    private float downY;
    private float lastY;
    private int flag = 0;
    private String cityCode = "";
    private TextView send;
    private String filterString;
    private ListView searchListView;
    private TextView searchNoResult;
    private SearchListAdapter searchListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_location_select);

        StatusBarUtil.setImmersiveStatusBar(this, true);

        mAMapView = findViewById(R.id.rc_ext_amap);
        initNearbyView();
        mHandler = new Handler();
        llList = findViewById(R.id.ll_list);
        llSearch = findViewById(R.id.ll_search);
        llsearch = findViewById(R.id.llsearch);
        etSearch = findViewById(R.id.et_search);
        ImageView myLocationView = findViewById(R.id.rc_ext_my_location);
        myLocationView.setOnClickListener(this);
        send = findViewById(R.id.tv_right);
        send.setOnClickListener(this);

        findViewById(R.id.tv_left).setOnClickListener(this);

        llSearch.setOnClickListener(v -> {
            // 修改布局高度
            ViewGroup.LayoutParams layoutParams = llList.getLayoutParams();
            layoutParams.height = DisplayUtils.dip2px(AMapLocationSelectActivity.this, 580);
            llList.setLayoutParams(layoutParams);
            // 显示和隐藏响应的搜索框
            llSearch.setVisibility(View.GONE);
            llsearch.setVisibility(View.VISIBLE);
            // 隐藏列表
            listViewNearby.setVisibility(View.GONE);
            // 弹出键盘
            getWindow().getDecorView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputManager != null) {
                        etSearch.requestFocus();
                        inputManager.showSoftInput(etSearch, 0);
                    }
                }
            }, 100);
        });
        findViewById(R.id.tv_cancel).setOnClickListener(v -> {
            // 修改布局高度
            ViewGroup.LayoutParams layoutParams = llList.getLayoutParams();
            layoutParams.height = DisplayUtils.dip2px(AMapLocationSelectActivity.this, 402);
            llList.setLayoutParams(layoutParams);
            // 显示和隐藏响应的搜索框
            llSearch.setVisibility(View.VISIBLE);
            llsearch.setVisibility(View.GONE);
            // 显示原来的列表数据
            listViewNearby.setVisibility(View.VISIBLE);
            // 收起键盘
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            // 隐藏搜索结果列表
            searchListView.setVisibility(View.GONE);
            searchNoResult.setVisibility(View.GONE);
        });
        searchListView = findViewById(R.id.rc_filtered_location_list);
        searchNoResult = findViewById(R.id.rc_tv_search_no_results);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterString = s.toString();
                searchLocationByKeyword(filterString);
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // 收起键盘
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    // 列表高度
                    resetViewHeight();
                    return true;
                }
                return false;
            }
        });
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                SearchLocationInfo searchLocationInfo = (SearchLocationInfo) searchListAdapter.getItem(position);
//                resetViewHeight();
//                updateToPosition(searchLocationInfo.getCity(), searchLocationInfo.getAddress(),
//                        searchLocationInfo.getLongitude(), searchLocationInfo.getLatitude(), searchLocationInfo.getPoi(), false);
                for (int i = 0; i < searchListAdapter.getCount(); i++) {
                    SearchLocationInfo searchLocationInfo = (SearchLocationInfo) searchListAdapter.getItem(position);
                    if (i == position) {
                        searchLocationInfo.setChecked(true);
                        updateToPosition(searchLocationInfo.getCity(), searchLocationInfo.getAddress(),
                                searchLocationInfo.getLongitude(), searchLocationInfo.getLatitude(), searchLocationInfo.getPoi(), false);
                    } else {
                        searchLocationInfo.setChecked(false);
                    }
                }
                searchListAdapter.notifyDataSetChanged();

                resetViewHeight();
            }
        });
        boolean netWorkAvailable = NetworkUtils.isConnected();
        if (netWorkAvailable) {
            send.setEnabled(true);
            send.setTextColor(getResources().getColor(R.color.white));
        } else {
            send.setEnabled(false);
            send.setTextColor(getResources().getColor(R.color.gray6));
        }
        mAMapView.onCreate(savedInstanceState);
        initMap();
    }

    private void searchLocationByKeyword(final String keyword) {
        PoiSearch.Query query = new PoiSearch.Query(keyword, LocationConst.CATEGORY, cityCode);
        query.setExtensions(PoiSearch.EXTENSIONS_ALL);
//        query.setDistanceSort(true);
        query.setCityLimit(false);
        query.setPageSize(PAGE_COUNT);// 设置每页最多返回多少条poiitem
        currentPage = 1;
        query.setPageNum(currentPage);// 设置查第一页
        PoiSearch poiSearch = null;
        try {
            poiSearch = new PoiSearch(this, query);
        } catch (AMapException e) {
            e.printStackTrace();
        }
        double longitude = mLngResult, latitude = mLatResult;
        if (latitude != 0.0 && longitude != 0.0) {
            // 设置周边搜索的中心点以及区域
//            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude,
//                    longitude), LocationConst.DISTANCE));
        }
        poiSearch.setOnPoiSearchListener(new PoiSearch.OnPoiSearchListener() {
            @Override
            public void onPoiSearched(PoiResult poiResult, int i) {
                if (poiResult != null && poiResult.getPois().size() > 0) {
                    List<PoiItem> poiItemList = poiResult.getPois();
                    SearchLocationInfo searchLocationInfo;
                    List<SearchLocationInfo> searchLocationInfoList = new ArrayList<>();
                    for (int j = 0; j < poiItemList.size(); j++) {
                        PoiItem poiItem = poiItemList.get(j);
                        searchLocationInfo = new SearchLocationInfo(poiItem.getTitle(), poiItem.getSnippet());
                        searchLocationInfo.setLongitude(poiItem.getLatLonPoint().getLongitude());
                        searchLocationInfo.setLatitude(poiItem.getLatLonPoint().getLatitude());
                        searchLocationInfo.setPoi(poiItem.getTitle());
                        searchLocationInfo.setCity(poiItem.getCityName());

                        searchLocationInfoList.add(searchLocationInfo);
                    }
                    searchNoResult.setVisibility(View.GONE);
                    searchListView.setVisibility(View.VISIBLE);
                    searchListAdapter = new SearchListAdapter(AMapLocationSelectActivity.this, searchLocationInfoList, keyword);
                    searchListView.setAdapter(searchListAdapter);

                    PoiItem poiItem = poiItemList.get(0);

                    updateCheckedMapView(poiItem.getCityName(), poiItem.getSnippet(), poiItem.getLatLonPoint().getLongitude(), poiItem.getLatLonPoint().getLatitude(), poiItem.getTitle());
                } else {
                    if (filterString.equals("")) {
                        searchNoResult.setVisibility(View.GONE);
                    } else {
                        searchNoResult.setVisibility(View.VISIBLE);
                        searchNoResult.setText("暂无结果");
                    }
                    searchListView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPoiItemSearched(PoiItem poiItem, int i) {

            }
        });// 设置数据返回的监听器
        poiSearch.searchPOIAsyn();// 开始搜索
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissions[0].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    initMap();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initMap() {
        mAMap = mAMapView.getMap();
        mAMap.setLocationSource(AMapLocationSelectActivity.this);
        mAMap.setMyLocationEnabled(true);
        mAMap.getUiSettings().setGestureScaleByMapCenter(true);
        mAMap.getUiSettings().setZoomControlsEnabled(false);
        mAMap.getUiSettings().setMyLocationButtonEnabled(false);
        mAMap.setMapType(AMap.MAP_TYPE_NORMAL);

        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource(R.drawable.rc_ext_my_locator));// 设置小蓝点的图标
        myLocationStyle.strokeWidth(0);
        myLocationStyle.strokeColor(R.color.theme);
        myLocationStyle.radiusFillColor(Color.TRANSPARENT);
        mAMap.setMyLocationStyle(myLocationStyle);

        try {
            mGeocodeSearch = new GeocodeSearch(AMapLocationSelectActivity.this);
        } catch (AMapException e) {
            e.printStackTrace();
        }
        mGeocodeSearch.setOnGeocodeSearchListener(AMapLocationSelectActivity.this);
        LocationManager.getInstance(this).setMyLocationChangedListener(this);
        mAMap.setOnCameraChangeListener(AMapLocationSelectActivity.this);
    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
        if (regeocodeResult != null) {
            RegeocodeAddress regeocodeAddress = regeocodeResult.getRegeocodeAddress();
            mLatResult = regeocodeResult.getRegeocodeQuery().getPoint().getLatitude();
            mLngResult = regeocodeResult.getRegeocodeQuery().getPoint().getLongitude();

            mCity = regeocodeResult.getRegeocodeAddress().getCity();

            String formatAddress = regeocodeResult.getRegeocodeAddress().getFormatAddress();
            mPoiResult = formatAddress.replace(regeocodeAddress.getProvince(), "").replace(regeocodeAddress.getCity(), "").replace(regeocodeAddress.getDistrict(), "");
            mAddress = mPoiResult;

            try {
                updateNearByView(mPoiResult);
            } catch (AMapException e) {
                e.printStackTrace();
            }
            send.setEnabled(true);
            send.setTextColor(getResources().getColor(R.color.white));
        } else {
            send.setEnabled(false);
            send.setTextColor(getResources().getColor(R.color.gray6));
            Toast.makeText(AMapLocationSelectActivity.this, getString(R.string.rc_location_fail), Toast.LENGTH_SHORT).show();
        }
    }

    private String getMapUrl(double latitude, double longitude) {
        return "http://restapi.amap.com/v3/staticmap?location=" + longitude + "," + latitude +
                "&zoom=16&scale=2&size=408*240&markers=mid,,A:" + longitude + ","
                + latitude + "&key=" + "e09af6a2b26c02086e9216bd07c960ae";
    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mLocationChangedListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (Build.VERSION.SDK_INT < 11) {
            mMarker.setPosition(cameraPosition.target);
        }
    }

    @Override
    public void onCameraChangeFinish(CameraPosition cameraPosition) {
        // 用户手动选择位置列表某个位置后，不需要重新查询附近位置
        if (!mUpdateNearby) {
            mUpdateNearby = true;
            return;
        }
        LatLonPoint point = new LatLonPoint(cameraPosition.target.latitude, cameraPosition.target.longitude);
        RegeocodeQuery query = new RegeocodeQuery(point, 50, GeocodeSearch.AMAP);
        mGeocodeSearch.getFromLocationAsyn(query);
    }

    private void addLocatedMarker(LatLng latLng, String poi) {
        mBitmapDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_location);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).icon(mBitmapDescriptor);
        if (mMarker != null) {
            mMarker.remove();
        }
        mMarker = mAMap.addMarker(markerOptions);

        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.8f, 1f);
        alphaAnimation.setDuration(2000);
        //设置不断重复
        alphaAnimation.setRepeatCount(3);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0.5f, 1f, 0.5f);
        scaleAnimation.setDuration(2000);
        //设置不断重复
        scaleAnimation.setRepeatCount(3);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);

        animationSet.setInterpolator(new LinearInterpolator());

        mMarker.setAnimation(animationSet);
        mMarker.startAnimation();

        mMarker.setPositionByPixels(mAMapView.getWidth() / 2, mAMapView.getHeight() / 2);
    }

    ValueAnimator animator;

    @TargetApi(11)
    private void animMarker() {
        if (Build.VERSION.SDK_INT > 11) {
            if (animator != null) {
                animator.start();
                return;
            }

            animator = ValueAnimator.ofFloat(mAMapView.getHeight() / 2, mAMapView.getHeight() / 2 - 30);
            animator.setInterpolator(new DecelerateInterpolator());
            animator.setDuration(150);
            animator.setRepeatCount(1);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    Float value = (Float) animation.getAnimatedValue();
                    mMarker.setPositionByPixels(mAMapView.getWidth() / 2, Math.round(value));
                }
            });
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMarker.setIcon(mBitmapDescriptor);
                }
            });
            animator.start();
        }
    }

    @Override
    protected void onDestroy() {
        mAMapView.onDestroy();
        LocationManager.getInstance(this).setMyLocationChangedListener(null);
        super.onDestroy();
    }

    @Override
    public void onMyLocationChanged(final AMapLocationInfo locationInfo) {
        if (mLocationChangedListener != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (locationInfo != null) {
                        mMyLat = mLatResult = locationInfo.getLat();
                        mMyLng = mLngResult = locationInfo.getLng();
                        mMyPoi = mPoiResult = locationInfo.getStreet() + locationInfo.getPoiname();
                        mMyCity = mCity = locationInfo.getCity();

                        //TODO
                        mMyAddress = mAddress = locationInfo.getDesc();

                        cityCode = locationInfo.getCitycode();

                        try {
                            updateNearByView(mMyPoi);
                        } catch (AMapException e) {
                            e.printStackTrace();
                        }
                        Location location = new Location("AMap");
                        location.setLatitude(locationInfo.getLat());
                        location.setLongitude(locationInfo.getLng());
                        location.setTime(locationInfo.getTime());
                        location.setAccuracy(locationInfo.getAccuracy());
                        mLocationChangedListener.onLocationChanged(location);
                        LatLng mLatLang = new LatLng(mLatResult, mLngResult);
                        addLocatedMarker(mLatLang, mPoiResult);
                        mAMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLang, 17f));
                    } else {
                        Toast.makeText(AMapLocationSelectActivity.this, getString(R.string.rc_location_fail), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private class NearbyListAdapter extends BaseAdapter {
        List<MapNearbyInfo> nearbyInfoList;
        Context context;

        public NearbyListAdapter(Context context, List<MapNearbyInfo> nearbyInfoList) {
            this.context = context;
            this.nearbyInfoList = nearbyInfoList;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NearbyViewHolder nearbyViewHolder;
            final MapNearbyInfo mapNearbyInfo = nearbyInfoList.get(position);
            if (convertView == null) {
                nearbyViewHolder = new NearbyViewHolder();
                convertView = View.inflate(context, R.layout.rc_map_nearby_info_item, null);
                nearbyViewHolder.itemView = convertView.findViewById(R.id.item_view);
                nearbyViewHolder.tvNearbyName = convertView.findViewById(R.id.rc_nearby_name);
                nearbyViewHolder.tvNearbyAddress = convertView.findViewById(R.id.rc_nearby_address);
//                nearbyViewHolder.ivNearbyChecked = convertView.findViewById(R.id.rc_nearby_checked);
                convertView.setTag(nearbyViewHolder);
            } else {
                nearbyViewHolder = (NearbyViewHolder) convertView.getTag();
            }
            if (position == 0) {
                nearbyViewHolder.tvNearbyAddress.setVisibility(View.GONE);
                nearbyViewHolder.tvNearbyName.setText(mapNearbyInfo.getName());
            } else {
                nearbyViewHolder.tvNearbyAddress.setVisibility(View.VISIBLE);
                nearbyViewHolder.tvNearbyName.setText(mapNearbyInfo.getName());
                nearbyViewHolder.tvNearbyAddress.setText(mapNearbyInfo.getAddress());
            }
            if (mapNearbyInfo.getChecked()) {
                nearbyViewHolder.itemView.setBackgroundResource(R.drawable.shape_bg_item_location);
                nearbyViewHolder.tvNearbyAddress.setTextColor(Color.parseColor("#f42603"));
                nearbyViewHolder.tvNearbyName.setTextColor(Color.parseColor("#f42603"));
//                nearbyViewHolder.ivNearbyChecked.setVisibility(View.VISIBLE);
            } else {
                nearbyViewHolder.itemView.setBackgroundColor(Color.WHITE);
                nearbyViewHolder.tvNearbyAddress.setTextColor(Color.parseColor("#666666"));
                nearbyViewHolder.tvNearbyName.setTextColor(Color.parseColor("#333333"));
//                nearbyViewHolder.ivNearbyChecked.setVisibility(View.GONE);
            }
            return convertView;
        }

        @Override
        public Object getItem(int position) {
            if (nearbyInfoList != null && nearbyInfoList.size() > 0) {
                return nearbyInfoList.get(position);
            } else {
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return nearbyInfoList.size();
        }

        public void addItems(List<MapNearbyInfo> nearbyInfoList) {
            if (this.nearbyInfoList != null) {
                this.nearbyInfoList.addAll(nearbyInfoList);
            }
        }

        class NearbyViewHolder {
            RelativeLayout itemView;
            TextView tvNearbyName;
            TextView tvNearbyAddress;
//            ImageView ivNearbyChecked;
        }
    }

    private class MapNearbyInfo {
        String name;
        String address;
        double latitude;
        double longitude;
        String poi;
        String city;
        boolean checked;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public MapNearbyInfo() {
        }

        public MapNearbyInfo(String name, String address) {
            this.name = name;
            this.address = address;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return address;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getPoi() {
            return poi;
        }

        public void setPoi(String poi) {
            this.poi = poi;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public boolean getChecked() {
            return checked;
        }
    }

    private void initNearbyView() {
        listViewNearby = findViewById(R.id.rc_list_nearby);
        listLoadingView = findViewById(R.id.rc_ext_loading);
        listViewNearby.setVisibility(View.GONE);
        listLoadingView.setVisibility(View.VISIBLE);
        mTouchSlop = ViewConfiguration.get(this).getScaledTouchSlop();
        listViewNearby.setOnScrollListener(onScrollListener);
        listViewNearby.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < nearbyListAdapter.getCount(); i++) {
                    MapNearbyInfo mapNearbyInfo = (MapNearbyInfo) nearbyListAdapter.getItem(i);
                    if (i == position) {
                        mapNearbyInfo.setChecked(true);
                        mLngResult = mapNearbyInfo.getLongitude();
                        mLatResult = mapNearbyInfo.getLatitude();
                        mPoiResult = mapNearbyInfo.getPoi();
                        mCity = mapNearbyInfo.getCity();
                        mAddress = mapNearbyInfo.getAddress();

                        updateCheckedMapView(mCity, mAddress, mLngResult, mLatResult, mPoiResult);
                    } else {
                        mapNearbyInfo.setChecked(false);
                    }
                }
                nearbyListAdapter.notifyDataSetChanged();
            }
        });
        listViewNearby.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Y = event.getRawY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downY = Y;
                        lastY = Y;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (Math.abs(Y - downY) > mTouchSlop) {
                            if (Y - downY >= 0 && Y - lastY >= 0) {
                                lastY = Y;
                                return handleScrollState(ScrollDirection.SCROLL_DOWN);
                            } else {
                                lastY = Y;
                                return handleScrollState(ScrollDirection.SCROLL_UP);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        // 触摸抬起时的操作
                        Y = 0;
                        if (flag == 1) {
                            flag = 0;
                            return true;
                        }
                        lastY = 0;
                        break;
                }
                return false;
            }
        });
    }

    private enum ScrollDirection {
        SCROLL_UP,
        SCROLL_DOWN
    }

    private boolean handleScrollState(ScrollDirection scrollDirection) {
        int minHeight = (int) getResources().getDimension(R.dimen.rc_ext_location_nearby_list_min_height);
        int maxHeight = (int) getResources().getDimension(R.dimen.rc_ext_location_nearby_list_max_height);
        if (scrollDirection == ScrollDirection.SCROLL_DOWN) {
            return updateListViewHeight(maxHeight, minHeight);
        } else {
            return updateListViewHeight(minHeight, maxHeight);
        }
    }

    private boolean updateListViewHeight(int oldHeight, int newHeight) {
        int height = listViewNearby.getHeight();
        int top;
        if (listViewNearby == null || listViewNearby.getChildAt(0) == null) {
            return false;
        }
        top = listViewNearby.getChildAt(0).getTop();
        if (top == 0 && Math.abs(Y - downY) > mTouchSlop && flag == 0 && height == oldHeight) {
            flag = 1;
        }
        if (flag == 1) {
            ViewGroup.LayoutParams layoutParams = listViewNearby.getLayoutParams();
            layoutParams.height = newHeight;
            listViewNearby.setLayoutParams(layoutParams);
            LatLng latLng = new LatLng(mLatResult, mLngResult);
            if (mMarker != null) {
                mMarker.setPosition(latLng);
            }
            /*FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) (mLocationTip.getLayoutParams());
            int marginTop;
            int marginLeft;
            int marginRight;
            if (oldHeight < newHeight) {
                //设置 title 位置
                marginTop = RongUtils.dip2px(3);
                marginLeft = RongUtils.dip2px(20);
                marginRight = RongUtils.dip2px(20);
                params.setMargins(marginLeft, marginTop, marginRight, 0);
                mLocationTip.setLayoutParams(params);
            } else {
                marginTop = RongUtils.dip2px(20);
                marginLeft = RongUtils.dip2px(20);
                marginRight = RongUtils.dip2px(20);
                params.setMargins(marginLeft, marginTop, marginRight, 0);
                mLocationTip.setLayoutParams(params);
            }*/
            //阻止 listViewNearby scroll 事件
            return true;
        }
        return false;
    }

    private void updateNearByView(final String poi) throws AMapException {
        PoiSearch.Query query = new PoiSearch.Query("", LocationConst.CATEGORY, cityCode);
        query.setPageSize(PAGE_COUNT);// 设置每页最多返回多少条poiitem
        query.setExtensions(PoiSearch.EXTENSIONS_ALL);
        query.setCityLimit(false);
        //query.setDistanceSort(true);
        currentPage = 1;
        query.setPageNum(currentPage);// 设置查第一页
        PoiSearch poiSearch = new PoiSearch(this, query);

        double longitude = mLngResult, latitude = mLatResult;
        if (latitude != 0.0 && longitude != 0.0) {
            // 设置周边搜索的中心点以及区域
//            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude,
//                    longitude), LocationConst.DISTANCE));
            poiSearch.setOnPoiSearchListener(new PoiSearch.OnPoiSearchListener() {
                @Override
                public void onPoiSearched(PoiResult poiResult, int i) {
                    if (poiResult != null && poiResult.getPois().size() > 0) {
                        List<MapNearbyInfo> mapNearbyInfos = new ArrayList<>();
                        List<PoiItem> poiItemList = poiResult.getPois();
                        //设置列表第一项
                        MapNearbyInfo nearbyInfo;
                        nearbyInfo = new MapNearbyInfo();
                        nearbyInfo.setName(poi);
                        nearbyInfo.setChecked(true);
                        nearbyInfo.setLongitude(mLngResult);
                        nearbyInfo.setLatitude(mLatResult);
                        nearbyInfo.setCity(mCity);
                        nearbyInfo.setPoi(poi);
                        nearbyInfo.setAddress(mAddress);

                        mapNearbyInfos.add(nearbyInfo);

                        //根据搜索结果设置列表其余项
                        for (int j = 0; j < poiItemList.size(); j++) {
                            PoiItem poiItem = poiItemList.get(j);
                            nearbyInfo = new MapNearbyInfo(poiItem.getTitle(), poiItem.getSnippet());
                            nearbyInfo.setLongitude(poiItem.getLatLonPoint().getLongitude());
                            nearbyInfo.setLatitude(poiItem.getLatLonPoint().getLatitude());
                            nearbyInfo.setPoi(poiItem.getTitle());
                            nearbyInfo.setCity(poiItem.getCityName());
                            mapNearbyInfos.add(nearbyInfo);
                        }

                        //设置附近位置列表 adapter
                        nearbyListAdapter = new NearbyListAdapter(AMapLocationSelectActivity.this, mapNearbyInfos);
                        listViewNearby.setAdapter(nearbyListAdapter);
                        listViewNearby.setVisibility(View.VISIBLE);
                        listLoadingView.setVisibility(View.GONE);

                        updateCheckedMapView(mCity, mAddress, mLngResult, mLatResult, poi);
                    }
                }

                @Override
                public void onPoiItemSearched(PoiItem poiItem, int i) {

                }
            });// 设置数据返回的监听器
            poiSearch.searchPOIAsyn();// 开始搜索
        } else {
            listViewNearby.setVisibility(View.VISIBLE);
            listLoadingView.setVisibility(View.GONE);
            Toast.makeText(AMapLocationSelectActivity.this, getResources().getString(R.string.rc_location_fail),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void loadNextPageNearByView() {
        PoiSearch.Query query = new PoiSearch.Query("", LocationConst.CATEGORY, cityCode);
        query.setPageSize(PAGE_COUNT);// 设置每页最多返回多少条poiitem
        query.setExtensions(PoiSearch.EXTENSIONS_ALL);
//        query.setDistanceSort(true);
        query.setCityLimit(false);

        query.setPageNum(++currentPage);// 设置查第一页
        PoiSearch poiSearch = null;
        try {
            poiSearch = new PoiSearch(this, query);
        } catch (AMapException e) {
            e.printStackTrace();
        }
        double longitude = mLngResult, latitude = mLatResult;
        if (latitude != 0.0 && longitude != 0.0) {
            // 设置周边搜索的中心点以及区域
//            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude,
//                    longitude), LocationConst.DISTANCE));
            poiSearch.setOnPoiSearchListener(new PoiSearch.OnPoiSearchListener() {
                @Override
                public void onPoiSearched(PoiResult poiResult, int i) {
                    if (poiResult != null && poiResult.getPois().size() > 0) {
                        List<MapNearbyInfo> mapNearbyInfos = new ArrayList<>();
                        List<PoiItem> poiItemList = poiResult.getPois();
                        MapNearbyInfo nearbyInfo;
                        for (int j = 0; j < poiItemList.size(); j++) {
                            PoiItem poiItem = poiItemList.get(j);
                            nearbyInfo = new MapNearbyInfo(poiItem.getTitle(), poiItem.getSnippet());
                            nearbyInfo.setLongitude(poiItem.getLatLonPoint().getLongitude());
                            nearbyInfo.setLatitude(poiItem.getLatLonPoint().getLatitude());
                            nearbyInfo.setPoi(poiItem.getTitle());
                            nearbyInfo.setCity(poiItem.getCityName());

                            mapNearbyInfos.add(nearbyInfo);
                        }
                        nearbyListAdapter.addItems(mapNearbyInfos);
                        nearbyListAdapter.notifyDataSetChanged();

                        PoiItem poiItem = poiItemList.get(0);


                        updateCheckedMapView(poiItem.getCityName(), poiItem.getSnippet(), poiItem.getLatLonPoint().getLongitude(), poiItem.getLatLonPoint().getLatitude(), poiItem.getTitle());
                    }
                }

                @Override
                public void onPoiItemSearched(PoiItem poiItem, int i) {

                }
            });// 设置数据返回的监听器
            poiSearch.searchPOIAsyn();// 开始搜索
        } else {
            Toast.makeText(AMapLocationSelectActivity.this, getResources().getString(R.string.rc_location_fail),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rc_ext_my_location) {
            handleMyLocation();
        } else if (v.getId() == R.id.tv_right) {
            handleOkButton();
        } else if (v.getId() == R.id.tv_left) {
            onBackPressed();
        }
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem != 0 && (firstVisibleItem + visibleItemCount) == totalItemCount) {
                View lastVisibleItemView = listViewNearby.getChildAt(listViewNearby.getChildCount() - 1);
                if (lastVisibleItemView != null && lastVisibleItemView.getBottom() == listViewNearby.getHeight()) {
                    loadNextPageNearByView();
                }
            }
        }
    };

    private void handleMyLocation() {
        if (mMyPoi != null) {
            mAMap.setOnCameraChangeListener(null);
            CameraUpdate update = CameraUpdateFactory.changeLatLng(new LatLng(mMyLat, mMyLng));
            mAMap.animateCamera(update, new AMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mAMap.setOnCameraChangeListener(AMapLocationSelectActivity.this);
                }

                @Override
                public void onCancel() {

                }
            });
            mLatResult = mMyLat;
            mLngResult = mMyLng;
            mPoiResult = mMyPoi;
            mCity = mMyCity;
            mAddress = mMyAddress;

            LatLng latLng = new LatLng(mLatResult, mLngResult);
            try {
                updateNearByView(mMyPoi);
            } catch (AMapException e) {
                e.printStackTrace();
            }
            if (mMarker != null) {
                mMarker.setPosition(latLng);
            }
        }
    }

    private void handleOkButton() {
        if (mLatResult == 0.0d && mLngResult == 0.0d && TextUtils.isEmpty(mPoiResult)) {
            Toast.makeText(AMapLocationSelectActivity.this, getString(R.string.rc_location_temp_failed), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("thumb", getMapUrl(mLatResult, mLngResult));
        intent.putExtra("lat", String.valueOf(mLatResult));
        intent.putExtra("lng", String.valueOf(mLngResult));
        intent.putExtra("poi", mPoiResult);
        intent.putExtra("city", mCity);
        intent.putExtra("address", mAddress);

        setResult(RESULT_OK, intent);
        finish();
    }

    private void resetViewHeight() {
        //重置附近位置列表高度
        if (llList != null) {
            ViewGroup.LayoutParams layoutParams = llList.getLayoutParams();
            layoutParams.height = DisplayUtils.dip2px(AMapLocationSelectActivity.this, 402);
            llList.setLayoutParams(layoutParams);
        }
    }

    private String mCity;
    private String mAddress;

    private void updateToPosition(String city, String address, double longitude, double latitude, String poi, boolean updateNearby) {
        if (poi != null) {
            mLatResult = latitude;
            mLngResult = longitude;
            mPoiResult = poi;
            mCity = city;
            mAddress = address;

            mUpdateNearby = updateNearby;
            if (updateNearby) {
                try {
                    updateNearByView(mPoiResult);
                } catch (AMapException e) {
                    e.printStackTrace();
                }
            }
            final LatLng latLng = new LatLng(mLatResult, mLngResult);
            CameraUpdate update = CameraUpdateFactory.changeLatLng(latLng);
            mAMap.setOnCameraChangeListener(null);
            mAMap.animateCamera(update, new AMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mAMap.setOnCameraChangeListener(AMapLocationSelectActivity.this);
                    if (mMarker != null) {
                        mMarker.setPosition(latLng);
                    }
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private void updateCheckedMapView(String city, String address, double longitude, double latitude, String poi) {
        updateToPosition(city, address, longitude, latitude, poi, false);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mAMapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mAMapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAMapView.onSaveInstanceState(outState);
    }

    private class SearchListAdapter extends BaseAdapter {
        private List<SearchLocationInfo> searchList = new ArrayList<>();
        private Context context;
        private String keyword;

        public SearchListAdapter(Context context, List<SearchLocationInfo> searchList, String keyword) {
            this.context = context;
            this.searchList = searchList;
            this.keyword = keyword;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SearchListAdapter.SearchViewHolder searchViewHolder;
            final SearchLocationInfo searchLocationInfo = searchList.get(position);
            if (convertView == null) {
                searchViewHolder = new SearchListAdapter.SearchViewHolder();
                convertView = View.inflate(context, R.layout.rc_location_search_item, null);
                searchViewHolder.tvName = convertView.findViewById(R.id.rc_search_name);
                searchViewHolder.tvAddress = convertView.findViewById(R.id.rc_search_address);
                searchViewHolder.rlContainer = convertView.findViewById(R.id.rlContainer);

                convertView.setTag(searchViewHolder);
            } else {
                searchViewHolder = (SearchListAdapter.SearchViewHolder) convertView.getTag();
            }
            setLocationTitle(searchViewHolder.tvName, searchLocationInfo.getName(), keyword);
            setLocationAddress(searchViewHolder.tvAddress, searchLocationInfo.getAddress(), keyword);

            if (searchLocationInfo.getChecked()) {
                searchViewHolder.rlContainer.setBackgroundResource(R.drawable.shape_bg_item_location);
                searchViewHolder.tvAddress.setTextColor(Color.parseColor("#f42603"));
                searchViewHolder.tvName.setTextColor(Color.parseColor("#f42603"));
            } else {
                searchViewHolder.rlContainer.setBackgroundColor(Color.WHITE);
                searchViewHolder.tvAddress.setTextColor(Color.parseColor("#666666"));
                searchViewHolder.tvName.setTextColor(Color.parseColor("#333333"));
            }

            return convertView;
        }

        @Override
        public Object getItem(int position) {
            if (searchList != null && searchList.size() > 0) {
                return searchList.get(position);
            } else {
                return null;
            }
        }

        public void addItems(List<SearchLocationInfo> searchList) {
            if (this.searchList != null) {
                this.searchList.addAll(searchList);
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getCount() {
            if (TextUtils.isEmpty(filterString)) {
                return 0;
            }
            return searchList.size();
        }

        class SearchViewHolder {
            TextView tvName;
            TextView tvAddress;

            RelativeLayout rlContainer;
        }
    }

    private void setLocationTitle(TextView titleView, String locationTitle, String keyword) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        SpannableStringBuilder colorFilterStr = new SpannableStringBuilder(locationTitle);
        int subPosition = (locationTitle.toLowerCase()).indexOf(keyword.toLowerCase());
        if (subPosition >= 0) {
            colorFilterStr.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.rc_map_search_highlight_color)), subPosition,
                    subPosition + keyword.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStringBuilder.append(colorFilterStr);
            titleView.setText(spannableStringBuilder);
        } else {
            titleView.setText(locationTitle);
        }
    }

    private void setLocationAddress(TextView addressView, String locationAddress, String keyword) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        SpannableStringBuilder colorFilterStr = new SpannableStringBuilder(locationAddress);
        int subPosition = (locationAddress.toLowerCase()).indexOf(keyword.toLowerCase());
        if (subPosition >= 0) {
            colorFilterStr.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.rc_map_search_highlight_color)), subPosition,
                    subPosition + keyword.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStringBuilder.append(colorFilterStr);
            addressView.setText(spannableStringBuilder);
            addressView.setText(spannableStringBuilder);
        } else {
            addressView.setText(locationAddress);
        }
    }

    private class SearchLocationInfo {
        String name;
        String address;
        double latitude;
        double longitude;
        String poi;
        String city;

        boolean checked;

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public boolean getChecked() {
            return checked;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public SearchLocationInfo(String name, String address) {
            this.name = name;
            this.address = address;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return address;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getPoi() {
            return poi;
        }

        public void setPoi(String poi) {
            this.poi = poi;
        }
    }

}
