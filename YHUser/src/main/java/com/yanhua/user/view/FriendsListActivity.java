package com.yanhua.user.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.CollectionUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.PinyinUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.core.view.WaveSideBarView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.FriendItemAdapter;
import com.yanhua.user.presenter.RelationListPresenter;
import com.yanhua.user.presenter.contract.RelationListContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.model.Conversation;

/**
 * @author Administrator
 * @好友页面
 */
@Route(path = ARouterPath.FRIENDS_LIST_ACTIVITY)
public class FriendsListActivity extends BaseMvpActivity<RelationListPresenter> implements RelationListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.wave_side_bar)
    WaveSideBarView sideBar;
    @BindView(R2.id.et_search)
    EditText etSearch;
    @BindView(R2.id.otherContanner)
    LinearLayout otherContanner;
    @BindView(R2.id.llMygroups)
    LinearLayout llMygroups;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;

    @Autowired
    boolean isSelect;
    @Autowired
    String userId;

    private List<FriendModel> allListData = new ArrayList<>();
    private String keyWord;

    private FriendItemAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new RelationListPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_friends_list;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle(isSelect ? "好友" : "通讯录");

        if (isSelect) {
            setTitle("好友");

            llMygroups.setVisibility(View.GONE);
        } else {
            setTitle("通讯录");

            llMygroups.setVisibility(View.VISIBLE);
        }

        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new FriendItemAdapter(mContext, !isSelect);

        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            FriendModel item = mAdapter.getmDataList().get(pos);
            if (isSelect) {
                //@
                Intent intent = new Intent();
                intent.putExtra("selectAtUser", item);
                setResult(RESULT_OK, intent);
                onBackPressed();
            } else {
                //私聊
                RouteUtils.routeToConversationActivity(mContext, Conversation.ConversationType.PRIVATE, item.getUserId(), null);
            }
        });

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            getUserList(1);
        });

        refreshLayout.setEnableLoadMore(false);

        sideBar.setOnTouchLetterChangeListener(letter -> {
            List<FriendModel> data = mAdapter.getmDataList();
            for (int i = 0; i < data.size(); i++) {
                FriendModel friend = data.get(i);
                String firstLetter = friend.getFirstLetter();
                if ("#".equals(letter) && "#".equals(firstLetter)) {
                    rvContent.scrollToPosition(i + 1);
                    break;
                } else if (firstLetter.equalsIgnoreCase(letter)) {
                    rvContent.scrollToPosition(i + 1);
                    break;
                }
            }
        });

        //搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                if (keyWord.length() > 0) {
                    String inputContent = etSearch.getText().toString().trim();
                    List<FriendModel> user_temp = new ArrayList<FriendModel>();
                    for (FriendModel user : mAdapter.getmDataList()) {
                        String uesrname = YHStringUtils.pickName(user.getNickName(), user.getFriendRemark());

                        String nameTemp = uesrname.toLowerCase();
                        String inputContentTemp = inputContent.toLowerCase();
                        if (nameTemp.contains(inputContentTemp)) {
                            user_temp.add(user);
                        }
                        mAdapter.setItems(user_temp);
                    }

//                    showSearchView();
                } else {
                    mAdapter.setItems(allListData);
//                    hideSearchView();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        sideBar.setVisibility(View.GONE);
        refreshLayout.autoRefresh(100);
    }

    public void getUserList(int cur) {
        current = cur;
        keyWord = "";
        etSearch.setText(keyWord);

        basePresenter.getUserFriend(userId, "", current, 10000);//一次性请求全部好友
    }

    private void showSearchView() {
        emptyLayout.setVisibility(View.VISIBLE);
        emptyLayout.setErrorType(EmptyLayout.SEARCH_INPUT_STATUS);
    }

    public void hideSearchView() {
        emptyLayout.setVisibility(View.GONE);
        emptyLayout.setErrorType(EmptyLayout.HIDE_LAYOUT);
    }

    @OnClick({R2.id.llMygroups})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.llMygroups) {
            ARouter.getInstance().build(ARouterPath.GROUP_CONTACTS_ACTIVITY).navigation();//我的群组
        }
    }

    @Override
    public void handleUserListSuccess(List<FriendModel> friends) {
        refreshLayout.finishRefresh();
        if (CollectionUtils.isNotEmpty(friends)) {
            allListData.clear();
            rvContent.setVisibility(View.VISIBLE);

            List<FriendModel> friendList = new ArrayList<>();
            //过滤只有选中的
            for (FriendModel friend : friends) {
                String name = YHStringUtils.pickName(friend.getNickName(), friend.getFriendRemark());
                String pinyin = PinyinUtils.formatPinyin(name);
                friend.setNamePinYin(pinyin);
                String firstLetter = pinyin.substring(0, 1).toUpperCase();
                if (!firstLetter.matches("[A-Z]")) { // 如果不在A-Z中则默认为“#”
                    firstLetter = "#";
                }
                friend.setFirstLetter(firstLetter);
                if (null != friend.getUserId() && !friend.getUserId().equals(UserManager.getInstance().getUserId())) {
                    friendList.add(friend);
                }
            }

            //整理排序
            Collections.sort(friends);
            allListData.addAll(friends);
            mAdapter.setItems(allListData);
            sideBar.setData(allListData, FriendModel::getFirstLetter, 0);

            sideBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    List<String> letters = sideBar.getLetters();
                    if (null != letters && letters.size() > YXConfig.SHOW_SIDEBAR_MIN_NUM) {
                        sideBar.setVisibility(View.VISIBLE);
                    } else {
                        sideBar.setVisibility(View.GONE);
                    }
                }
            }, 300);
        }
    }
}
