package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.OnClick;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MODIFY_PSWD_WAY_ACTIVITY)
public class ModifyPswdWayActivity extends BaseMvpActivity {

    @Override
    protected void creatPresent() {

    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_modify_pswd_way;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("修改密码");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick({R2.id.rlPhone, R2.id.rlPswd})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.rlPhone) {
            ARouter.getInstance().build(ARouterPath.MODIFY_PSWD_ACTIVITY)
//                    .withString("phone",phone)
                    .navigation();
        } else if (id == R.id.rlPswd) {

            ARouter.getInstance().build(ARouterPath.MODIFY_PSWD_BYPSWD_ACTIVITY)
//                    .withString("phone",phone)
                    .navigation();
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
    }
}
