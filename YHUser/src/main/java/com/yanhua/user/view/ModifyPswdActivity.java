package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.fragment.ModifyPswd1Fragment;

import butterknife.BindView;

/**
 * 网页说明页面（隐私、规则等）
 */
@Route(path = ARouterPath.MODIFY_PSWD_ACTIVITY)
public class ModifyPswdActivity extends BaseMvpActivity {

    @BindView(R2.id.tv_header)
    TextView tvTitle;

    @Autowired(name = "phone")
    String phone;

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        tvTitle.setText("修改密码");
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ModifyPswd1Fragment fragment = new ModifyPswd1Fragment();

        Bundle data = new Bundle();
        data.putString("phone", phone);
        fragment.setArguments(data);//将未注册的手机号带过注册、登录页

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public int bindLayout() {
        return R.layout.activity_modify_pswd;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }
    @Override
    public boolean getIsPadding() {
        return true;
    }
}