package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.CollectionUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.BlackMemberBean;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.BlackListAdapter;
import com.yanhua.user.presenter.SettingPresenter;
import com.yanhua.user.presenter.contract.SettingContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.imlib.RongIMClient;

/**
 * 黑名单
 */
@Route(path = ARouterPath.BLACK_LIST_ACTIVITY)
public class BlackListActivity extends BaseMvpActivity<SettingPresenter> implements SettingContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.empty_view)
    EmptyLayout emptyLayout;

    private BlackListAdapter mAdapter;
    private int current = 1;
    private int size = 20;
    private int limitSize = 5;

    private BlackMemberBean mModel;
    private int currentPos;

    private CommonDialog commonDialog;
    private List<BlackMemberBean> blackList;

    @Override
    protected void creatPresent() {
        basePresenter = new SettingPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_black_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("黑名单");
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new BlackListAdapter(mContext);
        blackList = new ArrayList<>();

        rvContent.setAdapter(mAdapter);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvContent.setRecycledViewPool(pool);

        //关闭recyclerview动画
        rvContent.getItemAnimator().setAddDuration(0);
        rvContent.getItemAnimator().setChangeDuration(0);
        rvContent.getItemAnimator().setMoveDuration(0);
        rvContent.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvContent.getItemAnimator()).setSupportsChangeAnimations(false);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<BlackMemberBean> itemModels = mAdapter.getmDataList();
            if (pos > itemModels.size() - 1) return;
            BlackMemberBean model = mAdapter.getmDataList().get(pos);
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", model.getBlockUserId())
                    .navigation();
        });

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getBlackList();
        });


        refreshLayout.setEnableLoadMore(true);
        refreshLayout.autoRefresh();

        mAdapter.setOnBlackListActionListener(pos -> {
            currentPos = pos;
            mModel = mAdapter.getItemObject(pos);
            int isBlock = mModel.getIsBlock();
            String userId = mModel.getBlockUserId();

            if (isBlock == 1) {
                //取消拉黑不显示提示
                RongIMClient.getInstance().removeFromBlacklist(userId, new RongIMClient.OperationCallback() {
                    @Override
                    public void onSuccess() {
                        basePresenter.addOrCancelBlack(userId);
                    }

                    @Override
                    public void onError(RongIMClient.ErrorCode errorCode) {

                    }
                });
            } else {
                if (null == commonDialog) {
                    commonDialog = new CommonDialog(mContext, "拉黑后，对方将无法搜索到你，也不能再给你发私信", "",
                            "确定", "取消");
                }
                new XPopup.Builder(mContext).asCustom(commonDialog).show();
                commonDialog.setOnConfirmListener(() -> {
                    commonDialog.dismiss();

                    RongIMClient.getInstance().addToBlacklist(userId, new RongIMClient.OperationCallback() {
                        @Override
                        public void onSuccess() {
                            basePresenter.addOrCancelBlack(userId);
                        }

                        @Override
                        public void onError(RongIMClient.ErrorCode errorCode) {

                        }
                    });
                });

                commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
            }
        });

        rvContent.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager l = (LinearLayoutManager) recyclerView.getLayoutManager();
                int adapterNowPos = l.findLastVisibleItemPosition();

                int eventSize = current * size - limitSize;

                if (adapterNowPos == eventSize) {
                    current++;
                    getBlackList();

                    refreshLayout.finishLoadMore(1000, true, true);
                }
            }
        });
    }

    @Override
    public void updateBlackUserSuccess() {
        if (null != mModel) {
            int block = mModel.getIsBlock();
            mModel.setIsBlock(block == 1 ? 0 : 1);
            mAdapter.notifyItemChanged(currentPos);
        }
    }

    private void getBlackList() {
        basePresenter.getBlackList(UserManager.getInstance().getUserInfo().getUsername(), current, size);
    }

    @Override
    public void handleBlackListSuccess(List<BlackMemberBean> friends) {
        refreshLayout.finishRefresh();

        if (current == 1) {
            if (CollectionUtils.isNotEmpty(friends)) {
                blackList.clear();
                blackList.addAll(friends);
                emptyLayout.setVisibility(View.GONE);
            } else {
                refreshLayout.setVisibility(View.GONE);
                emptyLayout.setVisibility(View.VISIBLE);
                emptyLayout.setErrorType(EmptyLayout.NODATA);

//                emptyLayout.setErrorImag(R.mipmap.empty_data);
                emptyLayout.setErrorMessage("这里没有相关记录哦");

            }
        } else {
            blackList.addAll(friends);
        }

        mAdapter.setItems(blackList);
    }
}
