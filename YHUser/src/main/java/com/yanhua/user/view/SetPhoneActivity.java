package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.user.R;
import com.yanhua.user.fragment.ChangePhoneFragment;
import com.yanhua.user.fragment.SetNewPhoneFragment;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

/**
 * 绑定手机
 */
@Route(path = ARouterPath.SET_PHONE_ACTIVITY)
public class SetPhoneActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @Autowired(name = "bindStatus", desc = "true false")
    boolean bindStatus;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle(bindStatus ? "更换手机号" : "绑定手机号");
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_set_phone;
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        if (bindStatus) {//已经绑定了手机号，那么进行更换手机号
            ChangePhoneFragment fragment = new ChangePhoneFragment();
            Bundle bundle = new Bundle();
            bundle.putString("phone", UserManager.getInstance().getUserInfo().getMobile());
            fragment.setArguments(bundle);
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else {//没有绑定手机号，绑定手机号
            SetNewPhoneFragment fragment = new SetNewPhoneFragment();

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
    }
}