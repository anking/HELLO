package com.yanhua.user.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 网页说明页面（隐私、规则等）
 */
@Route(path = ARouterPath.WEB_DETAIL_ACTIVITY)
public class WebDetailActivity extends BaseMvpActivity {

    @BindView(R2.id.wv)
    WebView webView;
    @Autowired
    String id;
    @Autowired
    String code;
    @Autowired
    String title;
    @Autowired
    String url;
    @Autowired
    String content;

    @OnClick({
            R2.id.tv_left
    })
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.tv_left) {
            onBackPressed();
        }
    }

    @Override
    protected boolean isCheckAppStatus() {
        return true;
    }


    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle(YHStringUtils.value(title));

        WebSettings settings = webView.getSettings();
        // 设置WebView支持JavaScript
        settings.setJavaScriptEnabled(true);
        //不支持屏幕缩放
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        //不显示webview缩放按钮
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);
        settings.setUseWideViewPort(false); // 将图片调整到适合webView的大小
        settings.setLoadWithOverviewMode(false); // 缩放至屏幕的大小
        settings.setBuiltInZoomControls(false); // 设置内置的缩放控件，若为false则该WebView不可缩放
        settings.setDisplayZoomControls(true); // 隐藏原生的缩放控件
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); // 缓存模式
        settings.setAllowFileAccess(true); // 设置可以访问文件
        settings.setJavaScriptCanOpenWindowsAutomatically(false); // 支持通过JS打开新窗口
        settings.setLoadsImagesAutomatically(true); // 支持自动加载图片
        settings.setDefaultTextEncodingName("utf-8"); // 设置编码格式

        webView.setWebViewClient(new XYWebviewClient());

        //https://ggfw.gdhrss.gov.cn/gdggfw-service/html/ysxy/ysxy.html
        //https://maka.im/appweb/mori/private_policy
        //http://www.mplanet.cn/wap/components/privacy.htm

        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
        } else {
            content = content.replace("<img", "<img style='width:100%;height:auto;margin:0;padding:0;'");
            webView.loadDataWithBaseURL(null, content, "text/html", "utf-8", null);
        }
    }

    private class XYWebviewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showLoading();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideLoading();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);//此处能拦截超链接的url,即拦截href请求的内容.

            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webView.clearCache(true);
        webView.destroy();
    }


    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_web_detail;
    }
}