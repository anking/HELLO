package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import butterknife.OnClick;

/**
 * 绑定手机
 */
@Route(path = ARouterPath.LOG_OFF_FAILED_ACTIVITY)
public class LogOffFailedActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("");
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_log_off_failed;
    }

    @OnClick(R2.id.tvKnow)
    public void onClickKnow() {
        onBackPressed();
    }
}