package com.yanhua.user.view;

import android.Manifest;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.github.dfqin.grantor.PermissionListener;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.core.util.PhoneUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.ContactCSAdapter;
import com.yanhua.user.presenter.SettingPresenter;
import com.yanhua.user.presenter.contract.SettingContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 联系客服
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CONTACT_CS_ACTIVITY)
public class ContactCSActivity extends BaseMvpActivity<SettingPresenter> implements SettingContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;

    @BindView(R2.id.rvContent)
    RecyclerView rvContent;

    private ContactCSAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new SettingPresenter();
    }

    /**
     * 设置状态栏的背景颜色
     *
     * @return
     */
    public int getStatusColor() {
        return R.color.theme;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_contact_cs;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("联系客服");

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContactCSAdapter(mContext);
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemCellClickListener((pos, model) -> {
            int type = model.getType();
            String content = model.getValue();

            if (TextUtils.isEmpty(content)) {//为空拒绝执行下面代码
                return;
            }

            if (type == 1) {
                checkPermission(content);
            } else {
                YHStringUtils.copyContent(mActivity, content);
            }
        });
    }


    private void checkPermission(String phone) {

        String[] permissionsO = new String[]{
                Manifest.permission.CALL_PHONE
        };

        requestCheckPermission(permissionsO, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                PhoneUtil.callPhones(mActivity, phone);
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
            }
        });
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        dataList = new ArrayList<>();

        basePresenter.getContactCS();
    }

    List<ServerConfigModel> dataList;

    @Override
    public void handleServerConfigList(List<ServerConfigModel> data) {
        if (null != data) {
            dataList.addAll(data);
        } else {
            //String icon, String key, String memo, int type, String value
            ServerConfigModel config1 = new ServerConfigModel("", "客服热线", "（工作时间：周一至周日 9:00-18:00）", 1, "400-9913-699");
            ServerConfigModel config2 = new ServerConfigModel("", "客服QQ", "（工作时间：周一至周日 9:00-18:00）", 2, "1931878770");

            dataList.add(config1);
            dataList.add(config2);
        }

        mAdapter.setItems(dataList);
    }
}
