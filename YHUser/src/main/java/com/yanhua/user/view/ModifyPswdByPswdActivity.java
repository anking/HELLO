package com.yanhua.user.view;

import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.PasswordInputFilter;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.MODIFY_PSWD_BYPSWD_ACTIVITY)
public class ModifyPswdByPswdActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_commit)
    TextView tvCommit;
    @BindView(R2.id.etOriginalPswd)
    EditText etOriginalPswd;
    @BindView(R2.id.etNewPswd)
    EditText etNewPswd;
    @BindView(R2.id.etNewSurePswd)
    EditText etNewSurePswd;

    @BindView(R2.id.tvOriginalPswdEye)
    AliIconFontTextView tvOriginalPswd;
    @BindView(R2.id.tvNewPswdEye)
    AliIconFontTextView tvNewPswd;
    @BindView(R2.id.tvNewSurePswdEye)
    AliIconFontTextView tvNewSurePswd;

    private boolean isOriginalPswdClose, isNewPswdClose, isNewSurePswdClose;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_modify_pswd_bypswd;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("修改密码");

        //设置密码不可以输入其他中文
        PasswordInputFilter.setPasswordInputFilter(etOriginalPswd);
        PasswordInputFilter.setPasswordInputFilter(etNewPswd);
        PasswordInputFilter.setPasswordInputFilter(etNewSurePswd);

        applyDebouncingClickListener(true, tvCommit, tvOriginalPswd, tvNewPswd, tvNewSurePswd);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_commit) {//
            String originalPassword = etOriginalPswd.getText().toString().trim();

            String password = etNewPswd.getText().toString().trim();
            String confirmPassword = etNewSurePswd.getText().toString().trim();

            if (TextUtils.isEmpty(originalPassword)) {
                ToastUtils.showShort("输入原密码");
                return;
            }

            if (TextUtils.isEmpty(password)) {
                ToastUtils.showShort("输入新的密码");
                return;
            }
            if (TextUtils.isEmpty(confirmPassword)) {
                ToastUtils.showShort("再次输入新密码");
                return;
            }

            if (!password.equals(confirmPassword)) {
                ToastUtils.showShort(R.string.different_pwd);
                return;
            }

            if (!YHStringUtils.validPassword(confirmPassword)) {
                ToastUtils.showShort(R.string.valid_pswd_error);
                return;
            }

            basePresenter.modifyPswdByPswd(UserManager.getInstance().getUserInfo().getMobile(), originalPassword, confirmPassword);
        } else if (id == R.id.tvOriginalPswdEye) {
            isOriginalPswdClose = !isOriginalPswdClose;

            setEyeVisiable(isOriginalPswdClose, etOriginalPswd, tvOriginalPswd);
        } else if (id == R.id.tvNewPswdEye) {
            isNewPswdClose = !isNewPswdClose;

            setEyeVisiable(isNewPswdClose, etNewPswd, tvNewPswd);
        } else if (id == R.id.tvNewSurePswdEye) {
            isNewSurePswdClose = !isNewSurePswdClose;

            setEyeVisiable(isNewSurePswdClose, etNewSurePswd, tvNewSurePswd);
        }
    }

    /**
     * 设置眼睛visible
     *
     * @param isClose
     * @param editText
     * @param eye
     */
    private void setEyeVisiable(boolean isClose, EditText editText, TextView eye) {
        if (isClose) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        eye.setText(isClose ? "\ue765" : "\ue751");

        String content = editText.getText().toString();
        editText.setSelection(content.length());
    }


    @Override
    public void doBusiness() {
        super.doBusiness();

        etOriginalPswd.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etNewPswd.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etNewSurePswd.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String passwordOriginal = etOriginalPswd.getText().toString().trim();
        String password = etNewPswd.getText().toString().trim();
        String passwordConfirm = etNewSurePswd.getText().toString().trim();

        tvCommit.setEnabled(!TextUtils.isEmpty(passwordOriginal) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(passwordConfirm));
    }

    @Override
    public void handlePswdResetResult() {
        EventBus.getDefault().post(new MessageEvent(EventName.LOGIN_PAGE_CLOSE));
        ToastUtils.showShort(R.string.reset_successful);
        new Handler().postDelayed(() -> {
            onReLogin();
            onBackPressed();
        },1000);
    }
}
