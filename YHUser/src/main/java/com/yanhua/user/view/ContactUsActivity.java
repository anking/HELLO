package com.yanhua.user.view;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.AppUtils;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.ServerConfigModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.ContactUsAdapter;
import com.yanhua.user.presenter.SettingPresenter;
import com.yanhua.user.presenter.contract.SettingContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 联系我们===有些字段通过接口获取
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CONTACT_US_ACTIVITY)
public class ContactUsActivity extends BaseMvpActivity<SettingPresenter> implements SettingContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.tvAppName)
    TextView tvAppName;

    @BindView(R2.id.rvContent)
    RecyclerView rvContent;

    private ContactUsAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new SettingPresenter();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_contact_us;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("联系我们");
        tvAppName.setText(AppUtils.getAppName() + AppUtils.getAppVersionName());

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContactUsAdapter(mContext);
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        dataList = new ArrayList<>();

        basePresenter.getContactUs();
    }

    List<ServerConfigModel> dataList;

    @Override
    public void handleServerConfigList(List<ServerConfigModel> data) {
        if (null != data) {
            dataList.addAll(data);
        } else {
            ServerConfigModel config1 = new ServerConfigModel("工作时间", "周一至周五 9:00-18:30");
            ServerConfigModel config2 = new ServerConfigModel("客服电话", "400-9913-699");
            ServerConfigModel config3 = new ServerConfigModel("公司邮箱", "1931878770@qq.com");

            dataList.add(config1);
            dataList.add(config2);
            dataList.add(config3);
        }

        mAdapter.setItems(dataList);
    }
}
