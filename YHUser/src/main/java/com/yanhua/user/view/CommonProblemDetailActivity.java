package com.yanhua.user.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CommonProblemBean;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.user.R;
import com.yanhua.user.R2;

import butterknife.BindView;

@Route(path = ARouterPath.COMMON_PROBLEM_DETAIL_ACTIVITY)
public class CommonProblemDetailActivity extends BaseMvpActivity {

    @BindView(R2.id.tvTitle)
    TextView tvTitle;
    @BindView(R2.id.webview)
    WebView webview;

    @Autowired
    CommonProblemBean problem;

    @Override
    protected void creatPresent() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_common_problem_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle(problem.getTitle());
        tvTitle.setText(problem.getTitle());
        String content = problem.getContent();
        if (!TextUtils.isEmpty(content)) {
            content = content.replace("<img", "<img style='width:100%;height:auto;margin:0;padding:0;'");
            webview.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null);
        }
    }
}