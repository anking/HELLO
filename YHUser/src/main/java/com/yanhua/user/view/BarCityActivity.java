package com.yanhua.user.view;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.util.PinyinUtils;
import com.yanhua.core.util.SystemUtil;
import com.yanhua.core.view.CitySideLetterBar;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.LetterDecoration;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.CityAdapter;
import com.yanhua.user.presenter.CityPresenter;
import com.yanhua.user.presenter.contract.CityContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.BAR_CITY_ACTIVITY)
public class BarCityActivity extends BaseMvpActivity<CityPresenter> implements CityContract.IView, AMapLocationListener {

    @BindView(R2.id.et_search)
    EditText etSearch;
    @BindView(R2.id.tv_current_city)
    TextView tvCurrentCity;
    @BindView(R2.id.tv_location_city)
    TextView tvLocationCity;
    @BindView(R2.id.rv_all_city)
    RecyclerView rvAllCity;
    @BindView(R2.id.tv_letter_overlay)
    TextView tvLetterOverlay;
    @BindView(R2.id.slb_menu)
    CitySideLetterBar slbMenu;
    @BindView(R2.id.rv_search_city)
    RecyclerView rvSearchCity;

    LinearLayout llHotCity;
    LinearLayout llSearchHistory;
    TagFlowLayout tflHot;
    TagFlowLayout tflHis;

    private List<CityModel> cityList;
    private List<CityModel> hotList;
    private List<CityModel> historyList;
    private CityAdapter mAdapter;
    private CityAdapter mSearchAdapter;
    //
    @Autowired
    String city;
    @Autowired
    String location;
    @Autowired
    public String plongitude;
    @Autowired
    public String platitude;
    CityModel allCity;

    @Override
    protected void creatPresent() {
        basePresenter = new CityPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_city;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        tvCurrentCity.setText(city);
        tvLocationCity.setText(location);
        cityList = new ArrayList<>();
        filterCityList = new ArrayList<>();

        mAdapter = new CityAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvAllCity.setLayoutManager(mManager);
        rvAllCity.setAdapter(mAdapter);

        mSearchAdapter = new CityAdapter(mContext);
        rvSearchCity.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rvSearchCity.setAdapter(mSearchAdapter);
        slbMenu.setOverlay(tvLetterOverlay);
        slbMenu.setOnLetterChangedListener(letter -> {
            int position = mAdapter.getSortLettersFirstPosition(letter);
            if (position != -1) {
                LinearLayoutManager manager = (LinearLayoutManager) rvAllCity.getLayoutManager();
                manager.scrollToPositionWithOffset(position, 0);
            }
        });

        View header = LayoutInflater.from(this).inflate(R.layout.layout_city_header, rvAllCity, false);
        llHotCity = header.findViewById(R.id.ll_hot_city);
        llSearchHistory = header.findViewById(R.id.ll_search_history);
        tflHot = header.findViewById(R.id.tfl_hot);
        tflHis = header.findViewById(R.id.tfl_his);
        mAdapter.addHeaderView(header);

        header.findViewById(R.id.ib_delete).setOnClickListener(v -> {
            // 此处是为了默认加载全国
            DiscoCacheUtils.getInstance().setBarCityHistoryList("");
            historyList.clear();
            tflHis.getAdapter().refreshData(historyList);
        });


        mAdapter.setOnItemClickListener((itemView, pos) -> {
            hideSoftKeyboard();

            CityModel cityModel = cityList.get(pos - mAdapter.getHeaderCount());
            selectCity(cityModel);
        });
        mSearchAdapter.setOnItemClickListener((itemView, pos) -> {
            hideSoftKeyboard();
            CityModel cityModel = filterCityList.get(pos);
            // 此时点击item要记录下当前model的id存储
            selectCity(cityModel);
        });
        final LetterDecoration decoration = new LetterDecoration() {
            @Override
            public String getHeaderName(int pos) {
                if (pos == 0) {
                    return "最近/热门";
                }
                return cityList.get(pos - 1).getFirst();
            }
        };

        rvAllCity.addItemDecoration(decoration);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String keyword = s.toString();
                if (!TextUtils.isEmpty(keyword)) {
                    filterList(keyword);
                } else {
                    rvAllCity.setVisibility(View.VISIBLE);
                    rvSearchCity.setVisibility(View.GONE);
                    slbMenu.setVisibility(View.VISIBLE);

                    mAdapter.setItems(cityList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        hotList = new ArrayList<>();
        tflHot.setAdapter(new TagAdapter<CityModel>(hotList) {
            @Override
            public View getView(FlowLayout parent, int position, CityModel s) {
                TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag, parent, false);
                tv.setText(s.getAreaName());
                return tv;
            }
        });
        tflHot.setOnTagClickListener((view, position, parent) -> {
            hideSoftKeyboard();
            CityModel cityModel = hotList.get(position);
            CityModel a = new CityModel();
            a.setLat(cityModel.getLat());
            a.setLng(cityModel.getLng());
            a.setAreaName(cityModel.getAreaName());
            a.setId(cityModel.getId());
            MessageEvent event = new MessageEvent(EventName.SELECT_BAR_CITY, a);
            EventBus.getDefault().post(event);
            finish();
            return true;
        });

        historyList = new ArrayList<>();
        tflHis.setAdapter(new TagAdapter<CityModel>(historyList) {
            @Override
            public View getView(FlowLayout parent, int position, CityModel s) {
                TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag, parent, false);
                tv.setText(s.getAreaName());
                return tv;
            }
        });

        tflHis.setOnTagClickListener((view, position, parent) -> {
            hideSoftKeyboard();
            CityModel cityModel = historyList.get(position);
            MessageEvent event = new MessageEvent(EventName.SELECT_BAR_CITY, cityModel);
            EventBus.getDefault().post(event);
            finish();
            return true;
        });
    }

    /**
     * 选择城市后
     *
     * @param cityModel
     */
    private void selectCity(CityModel cityModel) {
        String barCitySearchHistory = DiscoCacheUtils.getInstance().getBarCityHistoryList();

        if (!TextUtils.isEmpty(barCitySearchHistory)) {
            String[] arr = barCitySearchHistory.split(",");
            if (!Arrays.asList(arr).contains(cityModel.getAreaName())) {
                barCitySearchHistory += cityModel.getAreaName() + ",";
                DiscoCacheUtils.getInstance().setBarCityHistoryList(barCitySearchHistory);
            }
        } else {
            barCitySearchHistory = cityModel.getAreaName() + ",";
            DiscoCacheUtils.getInstance().setBarCityHistoryList(barCitySearchHistory);
        }
        MessageEvent event = new MessageEvent(EventName.SELECT_BAR_CITY, cityModel);
        EventBus.getDefault().post(event);
        finish();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        allCity = new CityModel();
        allCity.setAreaName("全国");

        //选择全国时候，经纬度为经纬度
        allCity.setLat("");
        allCity.setLng("");
        allCity.setPinyin("quanguo");

        historyList.add(allCity);

        basePresenter.getBarCity();
//        basePresenter.getNearbyHotcity(3);

        try {
            mlocationClient = new AMapLocationClient(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置定位监听
        mlocationClient.setLocationListener(this);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(2000);
        //设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用onDestroy()方法
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
        //启动定位
        checkLocationPermission();
    }

    public void checkLocationPermission() {
        requestCheckPermission(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                mlocationClient.startLocation();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_location_permission_tip);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//
        if (SystemUtil.isLocationEnabled(mContext)) {
            Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_common_location);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            tvLocationCity.setText(location);
            tvLocationCity.setCompoundDrawables(drawable, null, null, null);
        } else {
            Drawable drawable = mContext.getResources().getDrawable(R.mipmap.ic_common_no_navigation);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            tvLocationCity.setText("开启定位");
            tvLocationCity.setCompoundDrawables(drawable, null, null, null);
        }
    }

    @OnClick({R2.id.tv_right, R2.id.tv_location_city})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.tv_right) {
            hideSoftKeyboard();
            String keyword = etSearch.getText().toString().trim();
            if (!TextUtils.isEmpty(keyword)) {
                filterList(keyword);
            }
        } else if (id == R.id.tv_location_city) {
            if (SystemUtil.isLocationEnabled(mContext)) {
                if (!tvLocationCity.getText().toString().equals("开启定位")) {
                    if (null != locationCityModel) {
                        MessageEvent event = new MessageEvent(EventName.SELECT_BAR_CITY, locationCityModel);
                        EventBus.getDefault().post(event);
                        finish();
                    } else {
                        CityModel pModel = new CityModel();
                        pModel.setLng(plongitude);
                        pModel.setLat(platitude);
                        pModel.setAreaName(location);
                        pModel.setPinyin(PinyinUtils.formatPinyin(location));
                        MessageEvent event = new MessageEvent(EventName.SELECT_BAR_CITY, pModel);
                        EventBus.getDefault().post(event);
                        finish();
                    }
                }
            } else {
                showLoacationDialog();
            }
        }
    }

    /**
     * 打开GPS对话框
     */
    private void showLoacationDialog() {
        CommonDialog commonDialog = new CommonDialog(mContext, "为了提高定位的准确度，更好的为您服务，请打开定位服务", "请打开定位服务", "设置", "取消");
        new XPopup.Builder(mContext).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(() -> {
            commonDialog.dismiss();
            //跳转到手机打开GPS页面
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            //设置完成后返回原来的界面
            startActivityForResult(intent, 1001);
        });
        commonDialog.setOnCancelListener(() -> {
            commonDialog.dismiss();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            mlocationClient.stopLocation();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mlocationClient) {
            mlocationClient.onDestroy();
        }
    }

    List<CityModel> filterCityList = null;

    private void filterList(String keyword) {
        filterCityList.clear();

        for (CityModel city : cityList) {
            String pinyin = city.getPinyin();
            pinyin = pinyin.toUpperCase();
            String upkey = keyword.toUpperCase();

            if (city.getAreaName().contains(keyword) || pinyin.startsWith(upkey)) {//根据拼音
                filterCityList.add(city);
            }
        }

        if (filterCityList.size() > 0) {
            rvAllCity.setVisibility(View.GONE);
            rvSearchCity.setVisibility(View.VISIBLE);
            slbMenu.setVisibility(View.GONE);

            mSearchAdapter.setItems(filterCityList);
        } else {
            rvAllCity.setVisibility(View.VISIBLE);
            rvSearchCity.setVisibility(View.GONE);
            slbMenu.setVisibility(View.VISIBLE);

            mAdapter.setItems(cityList);
        }
    }

    @Override
    public void handleSuccessHotcity(List<CityModel> list) {
        if (list != null && !list.isEmpty()) {
            llHotCity.setVisibility(View.VISIBLE);
            hotList.clear();
            hotList.addAll(list);
            tflHot.getAdapter().notifyDataChanged();
        } else {
            llHotCity.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleSuccessAllBarAreas(List<CityModel> list) {
        if (list != null) {
            for (CityModel model : list) {
                cityList.addAll(model.getSubCity());
            }
            Collections.sort(cityList, (o1, o2) -> {
                return o1.getFirst().compareTo(o2.getFirst());
            });
            mAdapter.setItems(cityList);
            String barCitySearchHistory = DiscoCacheUtils.getInstance().getBarCityHistoryList();
            if (!TextUtils.isEmpty(barCitySearchHistory)) {
                llSearchHistory.setVisibility(View.VISIBLE);
                String[] arr = barCitySearchHistory.split(",");
                for (String name : arr) {
                    for (CityModel model : cityList) {
                        if (model.getAreaName().equals(name)) {
                            historyList.add(model);
                            break;
                        }
                    }
                }
            }
        }
        tflHis.getAdapter().refreshData(historyList);
    }

    private CityModel locationCityModel;
    // 声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    // 声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                mlocationClient.stopLocation();
                // 未选择过城市，此时获取到定位直接展示
                setCurrentLoacation(amapLocation);
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                mlocationClient.stopLocation();
                if (amapLocation.getErrorCode() == 12) {
                    // 缺少定位权限
                    ToastUtils.showShort(R.string.please_grant_location_permission);
                } else if (amapLocation.getErrorCode() == 6) {
                    // 定位失败
                    ToastUtils.showShort(R.string.fail_to_get_location);
                    tvLocationCity.setText(location);
                }
            }
        }
    }

    private void setCurrentLoacation(AMapLocation amapLocation) {
        if (!TextUtils.isEmpty(amapLocation.getCity())) {
            location = amapLocation.getCity();

            if (null == locationCityModel) {
                locationCityModel = new CityModel();
            }

            locationCityModel.setAreaName(location);
            String pinyin = PinyinUtils.formatPinyin(location);
            locationCityModel.setPinyin(pinyin);
            locationCityModel.setLat(String.valueOf(amapLocation.getLatitude()));
            locationCityModel.setLng(String.valueOf(amapLocation.getLongitude()));
        }
        tvLocationCity.setText(location);
    }
}