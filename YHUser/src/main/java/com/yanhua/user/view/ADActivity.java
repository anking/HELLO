package com.yanhua.user.view;


import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.widget.TextCountDownTimer;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.ADAdapter;
import com.yanhua.user.presenter.ADPresenter;
import com.yanhua.user.presenter.contract.ADContract;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.CircleIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.AD_ACTIVITY)
public class ADActivity extends BaseMvpActivity<ADPresenter> implements ADContract.IView {
    private static final int FROM_AD = 1024;

    @Autowired
    ArrayList advertiseModelList;
    @BindView(R2.id.bannerAD)
    Banner bannerAD;

    @BindView(R2.id.tv_timer)
    TextView tvTimer;

    private TextCountDownTimer countDownTimer;
    @Override
    protected boolean isCheckAppStatus() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new ADPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_ad;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        List<AdvertiseModel> mList = advertiseModelList;
        ADAdapter adapter = new ADAdapter(mList, mContext);

        bannerAD.addBannerLifecycleObserver(this)//添加生命周期观察者
                .setAdapter(adapter)
                .setIndicator(new CircleIndicator(this));

        if (null != advertiseModelList && advertiseModelList.size() > 1) {
            bannerAD.setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                    .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.theme))
                    .setIndicatorWidth(DisplayUtils.dip2px(mContext, 5), DisplayUtils.dip2px(mContext, 6))
                    .setIndicatorMargins(new IndicatorConfig.Margins(DisplayUtils.dip2px(mContext, 8), 0, DisplayUtils.dip2px(mContext, 4), DisplayUtils.dip2px(mContext, 32)));
        }

        bannerAD.setOnBannerListener((data, position) -> {
            AdvertiseModel clickItem = (AdvertiseModel) data;
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);

            PageJumpUtil.adJumpContent(clickItem, mActivity, FROM_AD, () -> {
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
            });
        });
        // 处理倒计时
        countDownTimer = new TextCountDownTimer(tvTimer, 6000, 1000);
        countDownTimer.setOnTimerFinishListener(() -> toMain());
        countDownTimer.start();
    }

    @OnClick(R2.id.tv_timer)
    public void toMain() {
        if (mMMKV.isLogin()) {
            ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                    .navigation();
        } else {
            if (mMMKV.isFirst()) {
                ARouter.getInstance().build(ARouterPath.WELCOME_ACTIVITY)
                        .navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                        .navigation();
            }
        }

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FROM_AD) {
            toMain();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}