package com.yanhua.user.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.AppBaseConfig;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.navigation.SetInfoMenu;
import com.yanhua.core.util.CalendarUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.ItemView;
import com.yanhua.core.widget.swipemenu.MenuAdapter;
import com.yanhua.core.widget.swipemenu.SwipeMenuRecyclerView;
import com.yanhua.core.widget.swipemenu.touch.OnItemMoveListener;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.constant.C;
import com.yanhua.user.presenter.PersonalSettingPresenter;
import com.yanhua.user.presenter.contract.PersonalSettingContract;
import com.yanhua.user.widget.BirthdayPickerPopup;
import com.yanhua.user.widget.CityPickerPopup;
import com.yanhua.user.widget.HeightWeightPickerPopup;
import com.yanhua.user.widget.ProfilePickerPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * 个人信息页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.PERSONAL_INFO_ACTIVITY)
public class PersonalInfoActivity extends BaseMvpActivity<PersonalSettingPresenter> implements
        PersonalSettingContract.IView, ItemView.OnDeleteListener, OnItemMoveListener {

    private static final int CHOOSE_HEAD = 0x001;
    private static final int CHOOSE_IMGS = 0x002;
    private static final int UPLOAD_FILE_FAIL = 1000;
    private static final int UPLOAD_FILE_SUCCESS = 1001;

    @BindView(R2.id.civ_avatar)
    CircleImageView civAvatar;
    @BindView(R2.id.itemNickName)
    SetInfoMenu itemNickName;
    @BindView(R2.id.itemSex)
    SetInfoMenu itemSex;
    @BindView(R2.id.itemAge)
    SetInfoMenu itemAge;
    @BindView(R2.id.itemSign)
    SetInfoMenu itemSign;
    @BindView(R2.id.itemEmotion)
    SetInfoMenu itemEmotion;
    @BindView(R2.id.itemProfession)
    SetInfoMenu itemProfession;
    @BindView(R2.id.itemAppealOfFriends)
    SetInfoMenu itemAppealOfFriends;
    @BindView(R2.id.tvConstellation)
    TextView tvConstellation;
    @BindView(R2.id.llConstellation)
    RelativeLayout llConstellation;
    @BindView(R2.id.itemHeight)
    SetInfoMenu itemHeight;
    @BindView(R2.id.itemWeight)
    SetInfoMenu itemWeight;
    @BindView(R2.id.itemCurrentAddress)
    SetInfoMenu itemCurrentAddress;
    @BindView(R2.id.itemCountry)
    SetInfoMenu itemCountry;
    @BindView(R2.id.llSign)
    LinearLayout llSign;
    @BindView(R2.id.tvSign)
    TextView tvSign;
    @BindView(R2.id.tflPersonTag)
    TagFlowLayout tflPersonTag;
    @BindView(R2.id.llHobby)
    LinearLayout llHobby;
    @BindView(R2.id.tvHobby)
    TextView tvHobby;
    @BindView(R2.id.tflHobby)
    TagFlowLayout tflHobby;
    @BindView(R2.id.rlv_selector)
    SwipeMenuRecyclerView mSwipeMenuRecyclerView;
    @BindView(R2.id.flUpload)
    FrameLayout flUpload;

    private StsTokenModel stsTokenModel;
    private String uploadPath;
    private List<CommonReasonModel> professionConfig;
    private List<CommonReasonModel> emotionConfig;
    private List<CityModel> cityList;
    private MenuAdapter mMenuAdapter;
    private ArrayList<String> imgList;
    private ArrayList<String> imgUrls;
    private int updateIndex = 0;

    private MyHandler mHandler = new MyHandler();

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    if (updateIndex < imgList.size()) {
                        uploadFile(imgList.get(updateIndex), CHOOSE_IMGS);
                    } else {
                        hideLoading();
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("imagesUrl", YHStringUtils.join(imgUrls.toArray(new String[0]), ","));
                        basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    }
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new PersonalSettingPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_personal_setting;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setTitle("个人资料");

        imgList = new ArrayList<>();
        imgUrls = new ArrayList<>();
        mSwipeMenuRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mMenuAdapter = new MenuAdapter(mContext, imgUrls);
        mMenuAdapter.setOnItemDeleteListener(this);

        mSwipeMenuRecyclerView.setAdapter(mMenuAdapter);
        mSwipeMenuRecyclerView.setLongPressDragEnabled(true);
        mSwipeMenuRecyclerView.setItemViewSwipeEnabled(false);
        mSwipeMenuRecyclerView.setOnItemMoveListener(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        UserInfo userInfo = UserManager.getInstance().getUserInfo();
        basePresenter.getPersonalInfo(userInfo.getId());
        basePresenter.getStsToken();

        HashMap<String, Object> params = new HashMap<>();
        params.put("current", 1);
        params.put("size", 100);

        // 职业
        int type_p = AppBaseConfig.TYPE_PROFESSION.getValue();
        params.put("type", type_p);
        basePresenter.getCommonReasonList(params, type_p);

        // 情感状况
        int type_e = AppBaseConfig.TYPE_EMOTION.getValue();
        params.put("type", type_e);
        basePresenter.getCommonReasonList(params, type_e);

        // 获取省级和市级
        basePresenter.getAllCity();
    }

    @OnClick({R2.id.ll_avatar})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.ll_avatar) {
            checkStoragePermission(0);
        }
    }

    @OnClick({R2.id.itemNickName, R2.id.itemSex, R2.id.itemAge, R2.id.itemHeight, R2.id.itemWeight,
            R2.id.itemCurrentAddress, R2.id.itemCountry, R2.id.tvSign, R2.id.llSign, R2.id.tvHobby,
            R2.id.llHobby, R2.id.itemSign, R2.id.itemEmotion, R2.id.itemProfession,
            R2.id.itemAppealOfFriends, R2.id.llUploadRule, R2.id.flUpload})
    public void onClickView(View view) {//点击保存
        int id = view.getId();
        if (id == R.id.itemNickName) {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_NICK_NAME)
                    .withString("value", UserManager.getInstance().getUserInfo().getNickName())
                    .navigation();
        } else if (id == R.id.itemSign) {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_SIGN)
                    .withString("value", UserManager.getInstance().getUserInfo().getPersonalSignature())
                    .navigation();
        } else if (id == R.id.itemAppealOfFriends) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_APPEAL_FRIENDS).navigation();
        } else if (id == R.id.tvSign) {
            // 编辑
            List<PersonTagModel> userAloneTagVOList = UserManager.getInstance().getUserInfo().getUserAloneTagVOList();
            ArrayList<String> list = new ArrayList<>();
            for (PersonTagModel p : userAloneTagVOList) {
                list.add(p.getId());
            }
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_SIGN_TAG)
                    .withStringArrayList("ids", list)
                    .navigation();
        } else if (id == R.id.llSign) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_SIGN_TAG).navigation();
        } else if (id == R.id.tvHobby) {
            // 编辑
            List<PersonTagModel> userHobbyTagVOList = UserManager.getInstance().getUserInfo().getUserHobbyTagVOList();
            ArrayList<String> list = new ArrayList<>();
            for (PersonTagModel p : userHobbyTagVOList) {
                list.add(p.getId());
            }
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_HOBBY)
                    .withStringArrayList("ids", list)
                    .navigation();
        } else if (id == R.id.llHobby) {
            ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY).withString(C.TYPE_FRAGMENT, C.INFO_HOBBY).navigation();
        } else if (id == R.id.itemEmotion) {
            if (emotionConfig == null || emotionConfig.isEmpty()) return;
            int currentItem = 0;
            for (int i = 0; i < emotionConfig.size(); i++) {
                if (emotionConfig.get(i).getId().equals(UserManager.getInstance().getUserInfo().getEmotionTypeId())) {
                    currentItem = i;
                }
            }
            ProfilePickerPopup popup = new ProfilePickerPopup(this, "情感状况");
            popup.setPickerData(emotionConfig).setCurrentItem(currentItem);
            popup.setPickerListener(index -> {
                String emotionTypeId = emotionConfig.get(index).getId();
                String emotionTypeName = emotionConfig.get(index).getContent();
                itemEmotion.setSubTitle(emotionTypeName);
                HashMap<String, Object> params = new HashMap<>();
                params.put("emotionTypeId", emotionTypeId);
                params.put("emotionTypeName", emotionTypeName);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemProfession) {
            if (professionConfig == null || professionConfig.isEmpty()) return;
            int currentItem = 0;
            for (int i = 0; i < professionConfig.size(); i++) {
                if (professionConfig.get(i).getId().equals(UserManager.getInstance().getUserInfo().getOccupationTypeId())) {
                    currentItem = i;
                }
            }
            ProfilePickerPopup popup = new ProfilePickerPopup(this, "职业");
            popup.setPickerData(professionConfig).setCurrentItem(currentItem);
            popup.setPickerListener(index -> {
                String occupationTypeId = professionConfig.get(index).getId();
                String occupationTypeName = professionConfig.get(index).getContent();
                itemProfession.setSubTitle(occupationTypeName);
                HashMap<String, Object> params = new HashMap<>();
                params.put("occupationTypeId", occupationTypeId);
                params.put("occupationTypeName", occupationTypeName);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemSex) {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
                    .withString(C.TYPE_FRAGMENT, C.INFO_SEX)
                    .withString("value", String.valueOf(UserManager.getInstance().getUserInfo().getGender()))
                    .navigation();
        } else if (id == R.id.itemAge) {
            String birthday = UserManager.getInstance().getUserInfo().getBirthday();
            int constellationState = UserManager.getInstance().getUserInfo().getConstellationState();
            BirthdayPickerPopup popup = new BirthdayPickerPopup(this, "年龄", birthday, constellationState);
            if (!TextUtils.isEmpty(birthday)) {
                Calendar date = Calendar.getInstance();
                date.setTime(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"));
                popup.setDefaultDate(date);
            }
            popup.setTimePickerListener((date, constellationState1) -> {
                HashMap<String, Object> params = new HashMap<>();
                params.put("constellationState", constellationState1);
                params.put("birthday", CalendarUtils.dateFormat(date, "yyyy-MM-dd"));
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemHeight) {
            List<String> heights = new ArrayList<>();
            int currentItem = 0;
            for (int i = 50; i <= 230; i++) {
                heights.add(String.valueOf(i));
            }
            /**
             * 身高、体重：未设置时关联性别选择
             *
             * 身高范围：50-230cm
             * 女/性别保密时：未设置时默认选中160cm
             * 男：未设置时默认选中170cm
             * 体重范围：25-130kg
             * 女/性别保密：未设置时默认选中50kg
             * 男：未设置时默认选中65kg
             */
            if (UserManager.getInstance().getUserInfo().getHeight() == 0) {
                if (UserManager.getInstance().getUserInfo().getGender() == -1 || UserManager.getInstance().getUserInfo().getGender() == 2) {
                    currentItem = heights.indexOf("160");
                } else if (UserManager.getInstance().getUserInfo().getGender() == 1) {
                    currentItem = heights.indexOf("170");
                }
            } else {
                currentItem = heights.indexOf(String.valueOf(UserManager.getInstance().getUserInfo().getHeight()));
            }
            if (currentItem < 0)
                currentItem = 0;
            HeightWeightPickerPopup popup = new HeightWeightPickerPopup(this, "身高");
            popup.setPickerData(heights).setLabel("cm").setCurrentItem(currentItem);
            popup.setPickerListener(index -> {
                String s = heights.get(index);
                itemHeight.setSubTitle(s + "cm");
                HashMap<String, Object> params = new HashMap<>();
                params.put("height", s);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemWeight) {
            List<String> weights = new ArrayList<>();
            int currentItem = 0;
            for (int i = 25; i <= 130; i++) {
                weights.add(String.valueOf(i));
            }
            if (UserManager.getInstance().getUserInfo().getWeight() == 0) {
                if (UserManager.getInstance().getUserInfo().getGender() == -1 || UserManager.getInstance().getUserInfo().getGender() == 2) {
                    currentItem = weights.indexOf("50");
                } else if (UserManager.getInstance().getUserInfo().getGender() == 1) {
                    currentItem = weights.indexOf("65");
                }
            } else {
                currentItem = weights.indexOf(String.valueOf(UserManager.getInstance().getUserInfo().getWeight()));
            }
            if (currentItem < 0)
                currentItem = 0;
            HeightWeightPickerPopup popup = new HeightWeightPickerPopup(this, "体重");
            popup.setPickerData(weights).setLabel("kg").setCurrentItem(currentItem);
            popup.setPickerListener(index -> {
                String s = weights.get(index);
                itemWeight.setSubTitle(s + "kg");
                HashMap<String, Object> params = new HashMap<>();
                params.put("weight", s);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemCurrentAddress) {
            if (cityList == null || cityList.isEmpty()) return;
            CityPickerPopup popup = new CityPickerPopup(this, "所在地");
            popup.setPickerData(cityList).setDefaultValue(UserManager.getInstance().getUserInfo().getAddress());
            popup.setCityPickerListener((province, city) -> {
                itemCurrentAddress.setSubTitle(province + city);
                HashMap<String, Object> params = new HashMap<>();
                params.put("address", province + "," + city);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.itemCountry) {
            if (cityList == null || cityList.isEmpty()) return;
            CityPickerPopup popup = new CityPickerPopup(this, "故乡");
            popup.setPickerData(cityList).setDefaultValue(UserManager.getInstance().getUserInfo().getHometown());
            popup.setCityPickerListener((province, city) -> {
                itemCountry.setSubTitle(province + city);
                HashMap<String, Object> params = new HashMap<>();
                params.put("hometown", province + "," + city);
                basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
            });
            new XPopup.Builder(this).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.llUploadRule) {
            basePresenter.getUploadRule("userPicture");
        } else if (id == R.id.flUpload) {
            checkStoragePermission(1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case ConstanceEvent.REFRESH_USER_INFO:
                boolean isLogin = UserManager.getInstance().isLogin();
                if (isLogin) {
                    basePresenter.getPersonalInfo(mMMKV.getUserId());
                }
                break;
        }
    }

    @Override
    public void handleUploadRuleSuccess(ProtocolModel model) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", model.getTitle())
                .withString("content", model.getContent())
                .navigation();
    }

    @Override
    public void handCommonReason(List<CommonReasonModel> data, int type) {
        if (type == AppBaseConfig.TYPE_PROFESSION.getValue()) {
            professionConfig = data;
        } else if (type == AppBaseConfig.TYPE_EMOTION.getValue()) {
            emotionConfig = data;
        }
    }

    @Override
    public void handleCitySuccess(List<CityModel> list) {
        this.cityList = list;
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        if (TextUtils.isEmpty(userInfo.getImg())) {
            civAvatar.setImageResource(R.drawable.place_holder);
        } else {
            ImageLoaderUtil.loadImgCenterCrop(civAvatar, userInfo.getImg(), R.drawable.place_holder);
        }
        itemNickName.setSubTitle(userInfo.getNickName());
        switch (userInfo.getGender()) {
            case -1:
                itemSex.setSubTitle("保密");
                break;
            case 1:
                itemSex.setSubTitle("男");
                break;
            case 2:
                itemSex.setSubTitle("女");
                break;
        }
        String makeFriendType = userInfo.getMakeFriendType();
        if (!TextUtils.isEmpty(makeFriendType)) {
            String[] arr = makeFriendType.split(",");
            List<String> strlist = new ArrayList<>();
            for (String s : arr) {
                String[] split = s.split("`~`");
                if (split.length > 1) {
                    strlist.add(split[1]);
                }
            }
            itemAppealOfFriends.setSubTitle(YHStringUtils.join(strlist.toArray(new String[0]), "/"));
        }
        if (!TextUtils.isEmpty(userInfo.getPersonalSignature())) {
            itemSign.setSubTitle(userInfo.getPersonalSignature());
        }
        if (!TextUtils.isEmpty(userInfo.getEmotionTypeName())) {
            itemEmotion.setSubTitle(userInfo.getEmotionTypeName());
        }
        if (!TextUtils.isEmpty(userInfo.getOccupationTypeName())) {
            itemProfession.setSubTitle(userInfo.getOccupationTypeName());
        }
        if (!TextUtils.isEmpty(userInfo.getBirthday())) {
            int age = CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(userInfo.getBirthday(), "yyyy-MM-dd"), new Date());
            itemAge.setSubTitle(String.valueOf(age));
        }
        if (userInfo.getConstellationState() == 0) {
            tvConstellation.setText("保密");
        } else if (userInfo.getConstellationState() == 1) {
            tvConstellation.setText(CalendarUtils.getConstellation(CalendarUtils.parseDateStr(userInfo.getBirthday(), "yyyy-MM-dd")));
        }
        if (userInfo.getHeight() > 0) {
            itemHeight.setSubTitle(String.format("%dcm", userInfo.getHeight()));
        }
        if (userInfo.getWeight() > 0) {
            itemWeight.setSubTitle(String.format("%dkg", userInfo.getWeight()));
        }
        if (!TextUtils.isEmpty(userInfo.getAddress())) {
            itemCurrentAddress.setSubTitle(userInfo.getAddress().replace(",", ""));
        }
        if (!TextUtils.isEmpty(userInfo.getHometown())) {
            itemCountry.setSubTitle(userInfo.getHometown().replace(",", ""));
        }
        if (userInfo.getUserAloneTagVOList() != null && !userInfo.getUserAloneTagVOList().isEmpty()) {
            llSign.setVisibility(View.GONE);
            tvSign.setVisibility(View.VISIBLE);
            tflPersonTag.setVisibility(View.VISIBLE);
            tflPersonTag.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserAloneTagVOList()) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    TextView tvTag = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag_sign, parent, false);
                    tvTag.setText(model.getTagName());
                    return tvTag;
                }
            });
        } else {
            llSign.setVisibility(View.VISIBLE);
            tvSign.setVisibility(View.GONE);
            tflPersonTag.setVisibility(View.GONE);
        }
        if (userInfo.getUserHobbyTagVOList() != null && !userInfo.getUserHobbyTagVOList().isEmpty()) {
            llHobby.setVisibility(View.GONE);
            tvHobby.setVisibility(View.VISIBLE);
            tflHobby.setVisibility(View.VISIBLE);
            tflHobby.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserHobbyTagVOList()) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    TextView tvTag = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag_sign, parent, false);
                    tvTag.setText(model.getTagName());
                    return tvTag;
                }
            });
        } else {
            llHobby.setVisibility(View.VISIBLE);
            tvHobby.setVisibility(View.GONE);
            tflHobby.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(userInfo.getImagesUrl())) {
            String[] imageUrls = userInfo.getImagesUrl().split(",");
            if (imageUrls != null) {
                if (imageUrls.length == 8) {
                    flUpload.setVisibility(View.GONE);
                } else if (imageUrls.length < 8) {
                    flUpload.setVisibility(View.VISIBLE);
                }
                imgUrls.clear();
                for (String s : imageUrls) {
                    imgUrls.add(s);
                }
                mMenuAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void handleErrorMessage(String errorMsg) {
        ToastUtils.showShort(errorMsg, Toast.LENGTH_LONG);
    }

    private void checkStoragePermission(int type) {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImage(type);
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    private void chooseImage(int type) {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(type == 0 ? 1 : 8)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(type == 0 ? PictureConfig.SINGLE : PictureConfig.MULTIPLE)// 多选 or 单选
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(1, 1)
//                .cropImageWideHigh(400, 400)//
//                .filterMinFileSize(200)
                .isSingleDirectReturn(true)
                .showCropGrid(false)
                .rotateEnabled(false)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(type == 0)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(type == 0 ? CHOOSE_HEAD : CHOOSE_IMGS);
    }

    @SuppressLint("AutoDispose")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // QQ授权回调需要配置这里
        if (requestCode == CHOOSE_HEAD && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();
            //如果裁剪路径是空的，那么使用真是路径
            uploadPath = TextUtils.isEmpty(cutPath) ? realPath : cutPath;
            ImageLoaderUtil.loadLocalImg(civAvatar, new File(uploadPath));
            uploadFile(uploadPath, CHOOSE_HEAD);
        } else if (requestCode == CHOOSE_IMGS && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            if (!result.isEmpty()) {
                updateIndex = 0;
                basePresenter.addSubscribe(
                        Flowable.just(result)
                                .observeOn(Schedulers.io())
                                .map(list -> {
                                    imgList.clear();
                                    for (LocalMedia item : list) {
                                        imgList.add(item.getRealPath());
                                    }
                                    return imgList;
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnError(throwable -> {
                                })
                                .subscribe(list -> {
                                    showLoading();
                                    uploadFile(list.get(updateIndex), CHOOSE_IMGS);
                                })
                );
            }
        }
    }

    @Override
    public void updateUserDetailSuccess() {
        EventBus.getDefault().post(new MessageEvent(ConstanceEvent.REFRESH_USER_INFO));
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);
        UserManager.getInstance().setStsToken(json);
    }

    private void uploadFile(String path, int i) {
        String fileName = new File(path).getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, path, new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                if (i == CHOOSE_HEAD) {
                    UserManager.getInstance().getUserInfo().setImg(httpUrl);
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("img", httpUrl);
                    basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                } else {
                    imgUrls.add(httpUrl);
                    updateIndex++;
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_FILE_SUCCESS;
                    mHandler.sendMessage(msg);
                }
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        if (position < 0) {  // 异常情况处理，getAdapterPosition可能返回为 -1
            return;
        }
    }

    @Override
    public void onDelete(int position) {
        if (position < 0) {  // 异常情况处理，getAdapterPosition可能返回为 -1
            return;
        }
        flUpload.setVisibility(View.VISIBLE);
        mMenuAdapter.removeIndex(position);
        HashMap<String, Object> params = new HashMap<>();
        params.put("imagesUrl", imgUrls.isEmpty() ? "" : YHStringUtils.join(imgUrls.toArray(new String[0]), ","));
        basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(imgUrls, fromPosition, toPosition);
        mMenuAdapter.notifyItemMoved(fromPosition, toPosition);
        HashMap<String, Object> params = new HashMap<>();
        params.put("imagesUrl", YHStringUtils.join(imgUrls.toArray(new String[0]), ","));
        basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {

    }

}
