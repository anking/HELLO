package com.yanhua.user.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.tencent.cloud.huiyansdkface.facelight.api.WbCloudFaceContant;
import com.tencent.cloud.huiyansdkface.facelight.api.WbCloudFaceVerifySdk;
import com.tencent.cloud.huiyansdkface.facelight.api.listeners.WbCloudFaceVerifyLoginListener;
import com.tencent.cloud.huiyansdkface.facelight.api.result.WbFaceError;
import com.tencent.cloud.huiyansdkface.facelight.process.FaceVerifyStatus;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.FaceUserInfoModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.BuildConfig;
import com.yanhua.user.R;
import com.yanhua.user.fragment.RealNameCertifyFragment;
import com.yanhua.user.presenter.CertifyPresenter;
import com.yanhua.user.presenter.contract.CertifyContract;

import java.util.List;

/*
实名认证
 */
@Route(path = ARouterPath.CERTIFICATION_ACTIVITY)
public class CertificationActivity extends BaseMvpActivity<CertifyPresenter> implements CertifyContract.IView {

    private boolean isShowSuccess;
    private boolean isShowFail;
    private boolean isRecordVideo;
    private boolean isEnableCloseEyes;
    private boolean isPlayVoice;
    private String color;

    //此处为demo模拟，请输入标识唯一用户的userId
    private String userId;
    //此处为demo使用，由合作方提供包名申请，统一下发
    private String compareType;
    //【防重复点击保护】通过标志位和loading显示控制用户点击后无法再次点击按钮
    private boolean isFaceVerifyInService;

    private void setInitDat() {

        //默认选择黑色模式
        color = WbCloudFaceContant.BLACK;
        //默认展示成功/失败页面
        isShowSuccess = false;
        isShowFail = false;
        //默认录制视频
        isRecordVideo = true;
        //默认播放提示语
        isPlayVoice = true;
        //默认不检测闭眼
        isEnableCloseEyes = false;
        //设置选择的比对类型  默认为公安网纹图片对比
        //公安网纹图片比对 WbCloudFaceContant.ID_CRAD
        //自带比对源比对  WbCloudFaceContant.SRC_IMG
        //仅活体检测  WbCloudFaceContant.NONE
        //默认公安网纹图片对比
        compareType = WbCloudFaceContant.ID_CARD;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        setInitDat();
        userId = UserManager.getInstance().getUserId();
    }

    String name = null;
    String idNum = null;

    @Override
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        if (event.getEventName().equals("UPDATE_USER_INFO")) {
            FaceUserInfoModel faceUser = (FaceUserInfoModel) event.getSerializable();

            name = faceUser.getName();
            idNum = faceUser.getId();

            showLoading();
            openCloudFaceService(FaceVerifyStatus.Mode.GRADE, BuildConfig.FACE_APPID, faceUser.getOrderNo(), faceUser.getSign(), faceUser.getFaceId(), faceUser.getNonce());
        }
    }

    //拉起刷脸sdk
    public void openCloudFaceService(FaceVerifyStatus.Mode mode, String appId, String order, String sign, String faceId, String nonce) {
        Bundle data = new Bundle();
        WbCloudFaceVerifySdk.InputData inputData = new WbCloudFaceVerifySdk.InputData(
                faceId,
                order,
                appId,
                "1.0.0",
                nonce,
                userId,
                sign,
                mode,
                BuildConfig.FACE_LICENSE);

        data.putSerializable(WbCloudFaceContant.INPUT_DATA, inputData);
        data.putString(WbCloudFaceContant.LANGUAGE, WbCloudFaceContant.LANGUAGE_ZH_CN);

        //是否展示刷脸成功页面，默认展示
        data.putBoolean(WbCloudFaceContant.SHOW_SUCCESS_PAGE, isShowSuccess);
        //是否展示刷脸失败页面，默认展示
        data.putBoolean(WbCloudFaceContant.SHOW_FAIL_PAGE, isShowFail);
        //颜色设置,sdk内置黑色和白色两种模式，默认白色
        //如果客户想定制自己的皮肤，可以传入WbCloudFaceContant.CUSTOM模式,此时可以配置ui里各种元素的色值
        //定制详情参考app/res/colors.xml文件里各个参数
        //颜色设置
        data.putString(WbCloudFaceContant.COLOR_MODE, color);
        //是否需要录制上传视频 默认需要
        data.putBoolean(WbCloudFaceContant.VIDEO_UPLOAD, isRecordVideo);

        //是否播放提示音，默认播放
        data.putBoolean(WbCloudFaceContant.PLAY_VOICE, isPlayVoice);
        //识别阶段合作方定制提示语,可不传，此处为demo演示
//        data.putString(WbCloudFaceContant.CUSTOMER_TIPS_LIVE, "仅供体验使用 请勿用于投产!");
        //上传阶段合作方定制提示语,可不传，此处为demo演示
//        data.putString(WbCloudFaceContant.CUSTOMER_TIPS_UPLOAD, "仅供体验使用 请勿用于投产!");
        //合作方长定制提示语，可不传，此处为demo演示
        //如果需要展示长提示语，需要邮件申请
//        data.putString(WbCloudFaceContant.CUSTOMER_LONG_TIP, "本demo提供的appId仅用于体验，实际生产请使用控制台给您分配的appId！");
        //设置选择的比对类型  默认为公安网纹图片对比
        //公安网纹图片比对 WbCloudFaceContant.ID_CRAD
        //仅活体检测  WbCloudFaceContant.NONE
        //默认公安网纹图片比对
        data.putString(WbCloudFaceContant.COMPARE_TYPE, compareType);
        data.putBoolean(WbCloudFaceContant.IS_ENABLE_LOG, BuildConfig.DEBUG);

        isFaceVerifyInService = true;

        //【特别注意】请使用activity context拉起sdk
        //【特别注意】请在主线程拉起sdk！
        WbCloudFaceVerifySdk.getInstance().initSdk(mActivity, data, new WbCloudFaceVerifyLoginListener() {
            @Override
            public void onLoginSuccess() {
                hideLoading();
                //拉起刷脸页面
                WbCloudFaceVerifySdk.getInstance().startWbFaceVerifySdk(mContext, result -> {
                    isFaceVerifyInService = false;

                    //得到刷脸结果
                    if (result != null) {
                        if (result.isSuccess()) {
                            if (!isShowSuccess) {
                                ToastUtils.showShort("刷脸成功！");
                            }
                            refreshUserInfo();
                        } else {
                            WbFaceError error = result.getError();
                            if (error != null) {
                                if (error.getDomain().equals(WbFaceError.WBFaceErrorDomainCompareServer)) {
                                }
                                if (!isShowSuccess) {
                                    ToastUtils.showShort("刷脸失败！" + error.getDesc());
                                }
                            }
                        }
                    }

                    //刷脸结束后，及时释放资源
                    WbCloudFaceVerifySdk.getInstance().release();
                });
            }

            @Override
            public void onLoginFailed(WbFaceError error) {
                isFaceVerifyInService = false;

                hideLoading();
                //登录失败
                if (error != null) {
                    if (error.getDomain().equals(WbFaceError.WBFaceErrorDomainParams)) {
                        ToastUtils.showShort("传入参数有误！" + error.getDesc());
                    } else {
                        ToastUtils.showShort("登录刷脸sdk失败！" + error.getDesc());
                    }
                }

                //刷脸结束后，及时释放资源
                WbCloudFaceVerifySdk.getInstance().release();
            }
        });
    }

    private void refreshUserInfo() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                basePresenter.updateUserDetail(userId, name, idNum, 1);
            }
        }, 500);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new CertifyPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_certification;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("认证中心");

        RealNameCertifyFragment fragment = new RealNameCertifyFragment();
//
//        Bundle data = new Bundle();
//        data.putSerializable("addInviteForm", inviteAddForm);
//        fragment.setArguments(data);

        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0)//这里是取出我们返回栈存在Fragment的个数
            finish();
        else
            getSupportFragmentManager().popBackStack();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getSupportFragmentManager().getFragments();
        if (getSupportFragmentManager().getFragments().size() > 0) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (Fragment mFragment : fragments) {
                mFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void updateUserDetailSuccess() {
        UserManager.getInstance().getUserInfo().setIdCard(idNum);
        UserManager.getInstance().getUserInfo().setRealName(name);
        UserManager.getInstance().getUserInfo().setRealNameValid(1);

        // 跳转到认证成功页面
        ARouter.getInstance()
                .build(ARouterPath.CERTIFY_RESULT_ACTIVITY)
                .withInt("result", 1)
                .navigation(mActivity);

        CertificationActivity.this.finish();
    }
}