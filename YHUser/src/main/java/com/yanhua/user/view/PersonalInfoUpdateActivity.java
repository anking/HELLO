package com.yanhua.user.view;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.AppBaseConfig;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.AppealFriendsAdapter;
import com.yanhua.user.constant.C;
import com.yanhua.user.presenter.PersonalSettingPresenter;
import com.yanhua.user.presenter.contract.PersonalSettingContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 个人信息页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.PERSONAL_INFO_UPDATE_ACTIVITY)
public class PersonalInfoUpdateActivity extends BaseMvpActivity<PersonalSettingPresenter> implements PersonalSettingContract.IView {

    @BindView(R2.id.tvChoose)
    TextView tvChoose;
    @BindView(R2.id.tv_right)
    TextView tvRight;
    @BindView(R2.id.llNickName)
    LinearLayout llNickName;
    @BindView(R2.id.llSex)
    LinearLayout llSex;
    @BindView(R2.id.llSign)
    LinearLayout llSign;
    @BindView(R2.id.llAppealFriends)
    LinearLayout llAppealFriends;
    @BindView(R2.id.llHobby)
    LinearLayout llHobby;
    @BindView(R2.id.tflHobby)
    TagFlowLayout tflHobby;
    @BindView(R2.id.rvAppealFriends)
    RecyclerView rvAppealFriends;
    @BindView(R2.id.etNickName)
    AutoClearEditText etNickName;
    @BindView(R2.id.llGirl)
    LinearLayout llGirl;
    @BindView(R2.id.llBoy)
    LinearLayout llBoy;
    @BindView(R2.id.etSign)
    EditText etSign;
    @BindView(R2.id.tvSignTotal)
    TextView tvSignTotal;
    @BindView(R2.id.llPersonTag)
    LinearLayout llPersonTag;
    @BindView(R2.id.tflPersonTag)
    TagFlowLayout tflPersonTag;

    @Autowired(name = "typeFragment")
    String typeFragment;
    @Autowired
    String value;
    @Autowired
    ArrayList<String> ids;

    private List<CommonReasonModel> friendsList;
    private int selectedFriendCount;
    private List<String> selectedPersonTagIds;

    @Override
    protected void creatPresent() {
        basePresenter = new PersonalSettingPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_personal_info_update;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        switch (typeFragment) {
            case C.INFO_NICK_NAME:
                setTitle("昵称");
                etNickName.setOnInputTextChangeListener(content -> tvRight.setEnabled(!TextUtils.isEmpty(content)));
                llNickName.setVisibility(View.VISIBLE);
                etNickName.setText(value);
                break;
            case C.INFO_HOBBY:
                setTitle("兴趣爱好");
                tvChoose.setVisibility(View.VISIBLE);
                String indexContent = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + "0" + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setText(Html.fromHtml(indexContent));
                tvRight.setEnabled(false);
                selectedPersonTagIds = new ArrayList<>();
                basePresenter.getUserAloneTags(2);
                break;
            case C.INFO_APPEAL_FRIENDS:
                setTitle("来音圈的目的");
                tvRight.setEnabled(false);
                llAppealFriends.setVisibility(View.VISIBLE);
                // 获取配置
                HashMap<String, Object> params = new HashMap<>();
                params.put("current", 1);
                params.put("size", 100);
                params.put("type", AppBaseConfig.TYPE_INTENTION_MAKE_FRIENDS.getValue());
                basePresenter.getCommonReasonList(params);
                break;
            case C.INFO_SIGN:
                setTitle("");
                llSign.setVisibility(View.VISIBLE);
                tvRight.setEnabled(false);
                OnValueChangeTextWatcher textWatcher = new OnValueChangeTextWatcher();
                textWatcher.setOnValueChangeListener(s -> {
                    String text = s.toString();
                    if (!TextUtils.isEmpty(text)) {
                        tvRight.setEnabled(true);
                        tvSignTotal.setText(String.format("%d/50", text.length()));
                    } else {
                        tvRight.setEnabled(false);
                        tvSignTotal.setText("0/50");
                    }
                });
                etSign.addTextChangedListener(textWatcher);
                if (!TextUtils.isEmpty(value)) {
                    etSign.setText(value);
                }
                break;
            case C.INFO_SEX:
                setTitle("性别");
                llSex.setVisibility(View.VISIBLE);
                tvRight.setEnabled(false);
                if (!TextUtils.isEmpty(value)) {
                    int gender = Integer.parseInt(value);
                    if (gender == 1) {
                        llBoy.setSelected(true);
                        tvRight.setEnabled(true);
                    } else if (gender == 2) {
                        llGirl.setSelected(true);
                        tvRight.setEnabled(true);
                    }
                }
                break;
            case C.INFO_SIGN_TAG:
                setTitle("个人标签");
                String text = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + "0" + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setVisibility(View.VISIBLE);
                tvChoose.setText(Html.fromHtml(text));
                tvRight.setEnabled(false);
                selectedPersonTagIds = new ArrayList<>();
                basePresenter.getUserAloneTags(1);
                break;
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

    }

    @Override
    public void handleErrorMessage(String errorMsg) {
        ToastUtils.showShort(errorMsg, Toast.LENGTH_LONG);
    }

    @Override
    public void handCommonReason(List<CommonReasonModel> data) {
        switch (typeFragment) {
            case C.INFO_APPEAL_FRIENDS:
                rvAppealFriends.setLayoutManager(new LinearLayoutManager(this));
                AppealFriendsAdapter adapter = new AppealFriendsAdapter(this);
                rvAppealFriends.setAdapter(adapter);
                adapter.setOnItemClickListener((itemView, pos) -> {
                    boolean selected = data.get(pos).isSelected();
                    if (selected) {
                        selectedFriendCount--;
                    } else {
                        if (selectedFriendCount >= 3) {
                            ToastUtils.showShort("最多选择三个哦", Toast.LENGTH_SHORT);
                            return;
                        }
                        selectedFriendCount++;
                    }
                    data.get(pos).setSelected(!selected);
                    adapter.notifyItemChanged(pos);
                    tvRight.setEnabled(selectedFriendCount > 0);
                });
                String makeFriendType = UserManager.getInstance().getUserInfo().getMakeFriendType();
                if (!TextUtils.isEmpty(makeFriendType)) {
                    String[] arr = makeFriendType.split(",");
                    List<String> list = new ArrayList<>();
                    for (String s : arr) {
                        String[] split = s.split("`~`");
                        list.add(split[0]);
                    }
                    for (CommonReasonModel c : data) {
                        if (list.contains(c.getId())) {
                            c.setSelected(true);
                            selectedFriendCount++;
                        }
                    }
                    tvRight.setEnabled(selectedFriendCount > 0);
                }
                adapter.setItems(data);
                friendsList = data;
                break;
        }
    }

    @Override
    public void handleUserAloneTags(List<PersonTagModel> list, int type) {
        if (type == 1) {
            // 个人标签
            llPersonTag.setVisibility(View.VISIBLE);
            TagAdapter signAdapter = new TagAdapter<PersonTagModel>(list) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_hobby, parent, false);
                    //获取标签
                    TextView tvTag = view.findViewById(R.id.tvTagName);
                    tvTag.setText(model.getTagName());
                    return view;
                }
            };
            tflPersonTag.setAdapter(signAdapter);
            tflPersonTag.setOnTagClickListener((view, position, parent) -> {
                selectedPersonTagIds.clear();
                Set<Integer> selectedPos = tflPersonTag.getSelectedList();
                if (!selectedPos.isEmpty()) {
                    for (Integer pos : selectedPos) {
                        selectedPersonTagIds.add(list.get(pos).getId());
                    }
                }
                tvRight.setEnabled(selectedPersonTagIds.size() > 0);
                String text = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + selectedPersonTagIds.size() + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setText(Html.fromHtml(text));
                return true;
            });
            // 回显数据
            if (ids != null && !ids.isEmpty()) {
                Set<Integer> set = new HashSet<>();
                for (int i = 0; i < list.size(); i++) {
                    PersonTagModel p = list.get(i);
                    if (ids.contains(p.getId())) {
                        set.add(i);
                    }
                }
                signAdapter.setSelectedList(set);
                String text = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + ids.size() + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setText(Html.fromHtml(text));
            }
            Set<Integer> set = tflPersonTag.getSelectedList();
            tvRight.setEnabled(!set.isEmpty());
        } else if (type == 2) {
            // 兴趣爱好
            llHobby.setVisibility(View.VISIBLE);
            TagAdapter hobbyAdapter = new TagAdapter<PersonTagModel>(list) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_hobby, parent, false);
                    //获取标签
                    TextView tvTag = view.findViewById(R.id.tvTagName);
                    tvTag.setText(model.getTagName());
                    return view;
                }
            };
            tflHobby.setAdapter(hobbyAdapter);
            tflHobby.setOnTagClickListener((view, position, parent) -> {
                selectedPersonTagIds.clear();
                Set<Integer> selectedPos = tflHobby.getSelectedList();
                if (!selectedPos.isEmpty()) {
                    for (Integer pos : selectedPos) {
                        selectedPersonTagIds.add(list.get(pos).getId());
                    }
                }
                tvRight.setEnabled(selectedPersonTagIds.size() > 0);
                String text = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + selectedPersonTagIds.size() + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setText(Html.fromHtml(text));
                return true;
            });
            // 回显数据
            if (ids != null && !ids.isEmpty()) {
                Set<Integer> set = new HashSet<>();
                for (int i = 0; i < list.size(); i++) {
                    PersonTagModel p = list.get(i);
                    if (ids.contains(p.getId())) {
                        set.add(i);
                    }
                }
                hobbyAdapter.setSelectedList(set);
                String text = "<font color=\"#B4B8BC\" >已选</font>"
                        + "<font color=\"#36384A\" >" + ids.size() + "</font>"
                        + "<font color=\"#B4B8BC\" >/9</font>";
                tvChoose.setText(Html.fromHtml(text));
            }
            Set<Integer> set = tflHobby.getSelectedList();
            tvRight.setEnabled(!set.isEmpty());
        }
    }

    @OnClick({R2.id.tv_right, R2.id.llGirl, R2.id.llBoy})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.llGirl) {
            llGirl.setSelected(true);
            llBoy.setSelected(false);
            tvRight.setEnabled(true);
        } else if (id == R.id.llBoy) {
            llGirl.setSelected(false);
            llBoy.setSelected(true);
            tvRight.setEnabled(true);
        } else if (id == R.id.tv_right) {
            hideSoftKeyboard();
            HashMap<String, Object> params = new HashMap<>();
            switch (typeFragment) {
                case C.INFO_NICK_NAME:
                    params.put("nickName", etNickName.getText().toString().trim());
                    basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    break;
                case C.INFO_SEX:
                    int gender = -1;
                    if (llBoy.isSelected()) {
                        gender = 1;
                    } else if (llGirl.isSelected()) {
                        gender = 2;
                    }
                    params.put("gender", gender);
                    basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    break;
                case C.INFO_SIGN:
                    params.put("personalSignature", etSign.getText().toString().trim());
                    basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    break;
                case C.INFO_APPEAL_FRIENDS:
                    if (selectedFriendCount > 0) {
                        List<String> list = new ArrayList<>();
                        for (CommonReasonModel c : friendsList) {
                            if (c.isSelected()) {
                                list.add(c.getId() + "`~`" + c.getContent());
                            }
                        }
                        String json = YHStringUtils.join(list.toArray(new String[0]), ",");
                        params.put("makeFriendType", json);
                        basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    }
                    break;
                case C.INFO_SIGN_TAG:
                    if (selectedPersonTagIds.size() > 0) {
                        params.put("tagIds", selectedPersonTagIds);
                        params.put("type", 1);
                        basePresenter.setUserAloneTags(params);
                    }
                    break;
                case C.INFO_HOBBY:
                    if (selectedPersonTagIds.size() > 0) {
                        params.put("tagIds", selectedPersonTagIds);
                        params.put("type", 2);
                        basePresenter.setUserAloneTags(params);
                    }
                    break;
            }
        }
    }

    @Override
    public void hanldeSetUserAloneTagsSuccess() {
        EventBus.getDefault().post(new MessageEvent(ConstanceEvent.REFRESH_USER_INFO));
        finish();
    }

    @Override
    public void updateUserDetailSuccess() {
        EventBus.getDefault().post(new MessageEvent(ConstanceEvent.REFRESH_USER_INFO));
        finish();
    }
}
