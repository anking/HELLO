package com.yanhua.user.view;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.DealResult;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.CountDownTimerUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.RegUtils;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 绑定手机
 */
@Route(path = ARouterPath.BIND_PHONE_ACTIVITY)
public class BindPhoneActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @BindView(R2.id.tv_left)
    TextView tvBack;
    @BindView(R2.id.tv_bind_phone)
    TextView tvBindPhone;

    @BindView(R2.id.tv_pro)
    TextView tvPro;

    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;

    @BindView(R2.id.tv_phoneCode)
    TextView tvGetCode;

    @BindView(R2.id.etPhoneCode)
    EditText etPhoneCode;
    @BindView(R2.id.etPhone)
    EditText etPhone;

    @Autowired(name = "params", desc = "第三方授权的信息参数")
    HashMap<String, Object> thirdMap;

    @Autowired(name = "type", desc = "1微信 2QQ 3Sina")
    int type;

    private String platform = "";
    private String bindPlatform = "";
    private String platformMobile = "";
    private String platformApp = "";
    private String weiboOpenid = "";

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("绑定手机号");

        switch (type) {
            case 1:
                platform = "微信";
                bindPlatform = YXConfig.smsWay.bindingWx;
                platformMobile = (String) thirdMap.get("unionid");
                platformApp = "weixinMobileApplications";
                break;
            case 2:
                platform = "QQ";
                bindPlatform = YXConfig.smsWay.bindingQQ;
                platformMobile = (String) thirdMap.get("openid");
                platformApp = "qqApplications";
                break;
            case 3:// 绑定微博的逻辑
                platform = "微博";
                bindPlatform = YXConfig.smsWay.bindingWeibo;
                platformMobile = (String) thirdMap.get("weiboOpenid");
                platformApp = "weiboApplications";
                weiboOpenid = platformMobile;
                break;
        }
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        setOneKeyPro();
        applyDebouncingClickListener(true, tvBindPhone, tvBack, checkboxPro);
    }

    //设置一键登录的协议描述
    public void setOneKeyPro() {
        String textContent = "注册/登录即表示同意音圈Disco服务条款和隐私条款";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                    } else {
                        if (null == userAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                        } else {
                            toProtocolPage(userAgreement);
                        }
                    }
                }
            }
        }, 10, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                    } else {
                        if (null == privacyAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                        } else {
                            toProtocolPage(privacyAgreement);
                        }
                    }
                }
            }
        }, 22, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    @OnClick(R2.id.tv_phoneCode)
    public void clickGetCode() {
        hideSoftKeyboard();
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort(R.string.please_input_phone);
        } else if (!RegUtils.isMobilePhone(phone)) {
            ToastUtils.showShort(R.string.error_format_phone);
        } else {
////            // 判断手机号是否存在
//            basePresenter.existPhone(phone);
            basePresenter.getSmsCode(phone, bindPlatform);
        }
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_bind_phone) {//
            hideSoftKeyboard();

            String phoneCode = etPhoneCode.getText().toString().trim();//验证码
            String phone = etPhone.getText().toString().trim();//手机号

            boolean isSelect = checkboxPro.isChecked();
            if (isSelect) {
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showShort(R.string.please_input_phone);
                    return;
                }
                if (!RegUtils.isMobilePhone(phone)) {
                    ToastUtils.showShort(R.string.error_format_phone);
                    return;
                }
                if (TextUtils.isEmpty(phoneCode)) {
                    ToastUtils.showShort(R.string.please_input_code);
                    return;
                }

                // 先校验验证码
                basePresenter.validateCode(phone, phoneCode, bindPlatform);
            } else {
                ToastUtils.showShort("请先阅读并同意相关协议");
            }
        } else if (id == R.id.checkbox_pro) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        } else if (id == R.id.tv_left) {
            onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bind_phone;
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhoneCode.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPhoneCode.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        tvBindPhone.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    //-------------------数据接口返回逻辑------------------------
    @Override
    public void handleSuccessExistPhone(int data) {
        if (data > 0) {
            // 手机号已注册过
            AlertPopup alertPopup = new AlertPopup(mContext, "手机号码已注册，是否绑定" + platform);
            new XPopup.Builder(mContext).asCustom(alertPopup).show();
            alertPopup.setOnConfirmListener(() -> {
                alertPopup.dismiss();
            });
            alertPopup.setOnCancelListener(() -> {
                alertPopup.dismiss();
                finish();
            });
        } else {
            // 未注册过
            String phone = etPhone.getText().toString().trim();

            basePresenter.getSmsCode(phone, bindPlatform);
        }
    }

    @Override
    public void handleSendSuccess() {
        ToastUtils.showShort(R.string.send_code_successful);
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    @Override
    public void handleValidateSuccess(String type) {
        String phone = etPhone.getText().toString().trim();
        String code = etPhoneCode.getText().toString().trim();

        thirdMap.put("code", code);
//        thirdMap.put("invitationCode", "");
        thirdMap.put("mobile", phone);

        basePresenter.bindPhone(thirdMap);
    }

    @Override
    public void handleSuccessBindPhone() {
        String platformBindID = platformMobile;

        String type = platformApp;
        basePresenter.login(platformBindID, "", type,null);
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);


        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(EventName.LOGIN_PAGE_CLOSE));//Close LoginActivity

        DealResult dealResult = new DealResult();
        dealResult.setGoToObj(null);
        dealResult.setTvGoPage(ARouterPath.MAIN_ACTIVITY);
        dealResult.setIvResultColor(R.color.theme);
        dealResult.setIvResultSize(56);
        dealResult.setIvResultText("\ue76a");
        dealResult.setTvResultText("绑定成功");
        dealResult.setTvResultDescText("手机号码绑定成功，音圈Disco欢迎您");
        dealResult.setTvGoPageText("去首页");

        ARouter.getInstance()
                .build(ARouterPath.DEAL_RESULT_ACTIVITY)
                .withSerializable("dealResult", dealResult)
                .navigation();
        finish();
    }
}