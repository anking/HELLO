package com.yanhua.user.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.dialog.CommonDialogContent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.widget.TypeWayBottomPopup;
import com.yanhua.core.util.PushNotificationRight;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.presenter.SettingPresenter;
import com.yanhua.user.presenter.contract.SettingContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.RongIMClient;


/**
 * 联系我们===有些字段通过接口获取
 *
 * @author Administrator
 */
@Route(path = ARouterPath.SET_PRIVATE_ACTIVITY)
public class SetPrivateActivity extends BaseMvpActivity<SettingPresenter> implements SettingContract.IView {

    @BindView(R2.id.switchNotification)
    Switch switchNotification;

    @BindView(R2.id.tvBlackListNum)
    TextView tvBlackListNum;

    @BindView(R2.id.tvWho)
    TextView tvWho;


    @BindView(R2.id.rlSetChat)
    RelativeLayout rlSetChat;

    private List<TypeWayModel> mChatRightList;
    private TypeWayModel mSelect;

    @Override
    protected void creatPresent() {
        basePresenter = new SettingPresenter();
    }

    @Override
    public boolean getIsPadding() {
        return true;
    }

    @Override
    public int getStatusColor() {
        return R.color.white;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_set_private;
    }


    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle("隐私设置");
    }


    @Override
    public void handleUserDetail(UserInfo userInfo) {
        if (null != userInfo) {
            //获取用户信息
//            int blackNum = userInfo.getBlockUserNums();
//            if (blackNum > 0) {
//                tvBlackListNum.setText(blackNum + "人");
//            } else {
//                tvBlackListNum.setText("");
//            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean isOpen = PushNotificationRight.isNotificationEnabled(mContext);
        switchNotification.setChecked(isOpen);

//        basePresenter.getUserDetail(mMMKV.getUserId());//跟下面的handleUserDetail方法配套
        RongIMClient.getInstance().getBlacklist(new RongIMClient.GetBlacklistCallback() {
            @Override
            public void onSuccess(String[] strings) {
                if (null != strings) {
                    //获取用户信息
                    int blackNum = strings.length;
                    if (blackNum > 0) {
                        tvBlackListNum.setText(blackNum + "人");
                    } else {
                        tvBlackListNum.setText("");
                    }
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {

            }
        });
    }

    @OnClick({R2.id.switchNotification, R2.id.rlBlackList, R2.id.rlSetChat})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.switchNotification) {
            goToSet(this);
        } else if (id == R.id.rlBlackList) {
            ARouter.getInstance().build(ARouterPath.BLACK_LIST_ACTIVITY).navigation();
        } else if (id == R.id.rlSetChat) {
            TypeWayBottomPopup popup = new TypeWayBottomPopup(mContext, "伙伴类型", mChatRightList);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();
                String itemName = bean.getName();
                String itemId = bean.getId();
                mSelect = bean;

                DiscoCacheUtils.getInstance().setHideSetStatus(Integer.valueOf(itemId));
                tvWho.setText(mSelect.getName());

                HashMap<String, Object> params = new HashMap<>();
                params.put("currUserId", mMMKV.getUserId());
                params.put("hideSet", itemId);

                basePresenter.updateChat2MeRight(params);
            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        }
    }

    private void goToSet(Context context) {
        // 返回值为true时，通知栏打开，false未打开
        boolean isClose = !PushNotificationRight.isNotificationEnabled(context);
        CommonDialogContent dialogContent = new CommonDialogContent().init(context,
                "消息通知",
                isClose ? "是否开启通知与状态栏权限？" : "确定关闭消息通知权限？关闭后消息将无法提示",
                new Pair<>(isClose ? "开启" : "关闭", v ->
                        PushNotificationRight.getAppDetailSettingIntent(context)
                ), new Pair<>("取消", v -> {
                }));

        dialogContent.show();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mChatRightList = new ArrayList<>();

        TypeWayModel all = new TypeWayModel("0", "全部");
        TypeWayModel fans = new TypeWayModel("1", "仅粉丝");

        mChatRightList.add(all);
        mChatRightList.add(fans);

        //坑呀----后台没有这个接口回显
        int hideSetStatus = DiscoCacheUtils.getInstance().getHideSetStatus();
        mSelect = mChatRightList.get(hideSetStatus);
        tvWho.setText(mSelect.getName());

    }

    @Override
    public void handleSuccessUpdatePrivacySetting() {

    }
}
