package com.yanhua.user.location;

import android.content.Context;
import android.text.TextUtils;

import com.amap.api.netlocation.AMapNetworkLocationClient;
import com.yanhua.user.listener.IMyLocationChangedListener;
import com.yanhua.user.model.AMapLocationInfo;


public class LocationManager {

    private IMyLocationChangedListener mMyLocationChangedListener;
    private Context mContext;
    private AMapLocationParser mLocationParser;
    private AMapNetworkLocationClient mLocationClient;
    private AMapLocationInfo mMyLastLatLng;

    private LoopThread mLoopThread;

    private static  LocationManager INSTANCE;

    private LocationManager(Context context) {
        mContext = context;
        mLocationParser = new AMapLocationParser();
    }

    public static LocationManager getInstance(Context context) {
        if (null==INSTANCE){
            INSTANCE = new LocationManager(context);
        }

        return INSTANCE;
    }

    public void setMyLocationChangedListener(IMyLocationChangedListener listener) {
        mMyLocationChangedListener = listener;
        if (mMyLocationChangedListener != null) {
            updateMyLocation();
            if (mMyLastLatLng != null) {
                listener.onMyLocationChanged(mMyLastLatLng);
            }
        }
    }

    public void updateMyLocation() {
        MyLocationThread thread = new MyLocationThread();
        thread.start();
    }

    private class MyLocationThread extends Thread {
        @Override
        public void run() {
            if (mLocationClient == null) {
                mLocationClient = new AMapNetworkLocationClient(mContext);
                mLocationClient.setApiKey("8bc77f14ab831d62baaf2ed17fb798a4");
            }
            String locStr = mLocationClient.getNetworkLocation();
            AMapLocationInfo locInfo = mLocationParser.parserApsJsonResp(locStr);
            sendOnMyLocationChanged(locInfo);
            mMyLastLatLng = locInfo;
        }
    }

    private void sendOnMyLocationChanged(AMapLocationInfo locInfo) {
        if (mContext != null && mMyLocationChangedListener != null && locInfo != null) {
            mMyLocationChangedListener.onMyLocationChanged(locInfo);
        }
    }


    public void updateMyLocationInLoop(int sec) {
        if (mLoopThread != null) {
            mLoopThread.stopLooping();
        }
        mLoopThread = new LoopThread(sec);
        mLoopThread.start();
    }

    public void stopMyLocationInLoop() {
        if (mLoopThread != null) {
            mLoopThread.stopLooping();
            mLoopThread = null;
        }
    }

    private class LoopThread extends Thread {
        private boolean mLooping;
        private int mMilSec;

        public LoopThread(int sec) {
            mMilSec = sec * 1000;
        }

        public void stopLooping() {
            mLooping = false;
        }

        @Override
        public void run() {
            mLooping = true;
            while (mLooping) {
                if (mLocationClient == null) {
                    mLocationClient = new AMapNetworkLocationClient(mContext);
                    mLocationClient.setApiKey("8bc77f14ab831d62baaf2ed17fb798a4");
                }
                String locStr = mLocationClient.getNetworkLocation();
                if (!TextUtils.isEmpty(locStr)) {
                    AMapLocationInfo locInfo = mLocationParser.parserApsJsonResp(locStr);
                    mMyLastLatLng = locInfo;
                }
                try {
                    sleep(mMilSec);
                } catch (InterruptedException e) {
                    // Restore interrupted state...
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

}
