package com.yanhua.user.fragment;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.CountDownTimerUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.RegUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;
import com.yanhua.user.view.LoginActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jiguang.verifysdk.api.JVerificationInterface;

public class LoginPhoneCodeFragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_phoneCode)
    TextView tvGetCode;
    @BindView(R2.id.tv_login_onekey)
    TextView loginOneKey;

    @BindView(R2.id.tv_login_phoneCode)
    TextView loginPhoneCode;

    @BindView(R2.id.tv_login_password)
    TextView loginPassword;

    @BindView(R2.id.tv_pro)
    TextView tvPro;

    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;

    @BindView(R2.id.etPhoneCode)
    EditText etPhoneCode;
    @BindView(R2.id.etPhone)
    EditText etPhone;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_login_phone;
    }


    @Override
    public void handleErrorMessage(String message) {
        ToastUtils.showShort(message);
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ((LoginActivity) getActivity()).setRootViewBackgroud(LoginActivity.PHONE_CODE_LOGIN);//设置不同的背景

        setOneKeyPro();
        applyDebouncingClickListener(true, loginOneKey, loginPhoneCode, loginPassword, checkboxPro);
    }

    //设置一键登录的协议描述
    public void setOneKeyPro() {
        String textContent = "注册/登录即表示同意音圈Disco服务条款和隐私条款";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                    } else {
                        if (null == userAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                        } else {
                            toProtocolPage(userAgreement);
                        }
                    }
                }
            }
        }, 10, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                    } else {
                        if (null == privacyAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                        } else {
                            toProtocolPage(privacyAgreement);
                        }
                    }
                }
            }
        }, 22, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }


    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_login_onekey) {//
//            LoginOneKeyFragment fragment = new LoginOneKeyFragment();
//            FragmentManager manager = this.getActivity().getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.replace(R.id.container, fragment);
//            transaction.commit();

            ARouter.getInstance().build(ARouterPath.VERIFY_PHONE_ACTIVITY).navigation();
        } else if (id == R.id.tv_login_phoneCode) {//验证码登录
            String phoneCode = etPhoneCode.getText().toString().trim();//验证码
            String phone = etPhone.getText().toString().trim();//手机号

            boolean isSelect = checkboxPro.isChecked();
            if (isSelect) {
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showShort(R.string.please_input_phone);
                    return;
                }
                if (!RegUtils.isMobilePhone(phone)) {
                    ToastUtils.showShort(R.string.error_format_phone);
                    return;
                }
                if (TextUtils.isEmpty(phoneCode)) {
                    ToastUtils.showShort(R.string.please_input_code);
                    return;
                }

                basePresenter.validateCode(phone, phoneCode, YXConfig.smsWay.login);
            } else {
                ToastUtils.showShort("请先阅读并同意相关协议");
            }
        } else if (id == R.id.tv_login_password) {//密码
            LoginPasswordFragment fragment = new LoginPasswordFragment();

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else if (id == R.id.checkbox_pro) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        }
    }

    @OnClick(R2.id.tv_phoneCode)
    public void clickGetCode() {//点击获取登录验证码
        hideSoftKeyboard();
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort(R.string.please_input_phone);
        } else if (!RegUtils.isMobilePhone(phone)) {
            ToastUtils.showShort(R.string.error_format_phone);
        } else {
            basePresenter.getSmsCode(phone, YXConfig.smsWay.login);
        }
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        Bundle data = getArguments();
        if (null != data) {
            String phone = data.getString("phone", "");
            etPhone.setText(phone);
        }

        //监听设置
        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhoneCode.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        addListenEdit();
        //监听设置------------------end-------------------

        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(mContext);
        if (!verifyEnable) {
            loginOneKey.setVisibility(View.GONE);
        }
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPhoneCode.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        loginPhoneCode.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    @Override
    public void handleSendSuccess() {
        ToastUtils.showShort(R.string.send_code_successful);

        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    @Override
    public void handleValidateSuccess(String type) {
        if (type.equals(YXConfig.smsWay.login)) {
            // 短信登陆验证成功
            String phone = etPhone.getText().toString().trim();
            String phoneCode = etPhoneCode.getText().toString().trim();
            basePresenter.login(phone, phoneCode, "mobile",null);
        } else {

        }
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo);
        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);

        getActivity().finish();
    }

    @Override
    public boolean isProtocolChecked() {
        boolean checked = checkboxPro.isChecked();
        return checked;
    }
}
