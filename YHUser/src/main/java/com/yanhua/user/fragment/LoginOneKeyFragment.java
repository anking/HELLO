package com.yanhua.user.fragment;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;
import com.yanhua.user.view.LoginActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;

public class LoginOneKeyFragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_login_onekey)
    TextView loginOneKey;

    @BindView(R2.id.tv_login_phoneCode)
    TextView loginPhoneCode;

    @BindView(R2.id.tv_login_password)
    TextView loginPassword;

    @BindView(R2.id.tv_pro)
    TextView tvPro;

    @BindView(R2.id.tv_close)
    TextView tvClose;

    @BindView(R2.id.tvCurrentPhone)
    TextView tvCurrentPhone;

    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;

    private int mAction;
    private String token;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_login_onekey;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ((LoginActivity) getActivity()).setRootViewBackgroud(LoginActivity.ONE_KEY_LOGIN);
        setOneKeyPro();
        applyDebouncingClickListener(true, loginOneKey, loginPhoneCode, loginPassword, tvPro, checkboxPro, tvClose);
    }

    private void avoidHintColor(View view) {
        if (view instanceof TextView)
            ((TextView) view).setHighlightColor(getResources().getColor(android.R.color.transparent));
    }

    //设置一键登录的协议描述
    public void setOneKeyPro() {
        //注册/登录即表示同意《中国移动认证服务条款》、音圈25 Disco服务条款34 和隐私条款";
        String textContent = "注册/登录即表示同意《中国移动认证服务条款》、音圈Disco服务条款和隐私条款";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (null == oneKeyLoginProtocol) {
                        basePresenter.getAppProtocol(YXConfig.protocol.oneKeyLoginProtocol);
                    } else {
                        toProtocolPage(oneKeyLoginProtocol);
                    }
                }
            }
        }, 10, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                    } else {
                        if (null == userAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                        } else {
                            toProtocolPage(userAgreement);
                        }
                    }
                }
            }
        }, 25, 34, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                    } else {
                        if (null == privacyAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                        } else {
                            toProtocolPage(privacyAgreement);
                        }
                    }
                }
            }
        }, 35, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_login_onekey) {//一键登录
            boolean isSelect = checkboxPro.isChecked();
            if (isSelect) {
                ARouter.getInstance().build(ARouterPath.MAIN_ACTIVITY)
                        .navigation();
            } else {
                ToastUtils.showShort("请先阅读并同意相关协议");
            }
        } else if (id == R.id.tv_login_phoneCode) {//验证码登录
            LoginPhoneCodeFragment fragment = new LoginPhoneCodeFragment();

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else if (id == R.id.tv_login_password) {//密码
            LoginPasswordFragment fragment = new LoginPasswordFragment();

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else if (id == R.id.checkbox_pro) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        } else if (id == R.id.tv_close) {
            //关闭
            getActivity().finish();
        }
    }

    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;
    private ProtocolModel oneKeyLoginProtocol;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            } else if (YXConfig.protocol.oneKeyLoginProtocol.equals(code)) {
                oneKeyLoginProtocol = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo);
        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);

        getActivity().finish();
    }

    @Override
    public void handleErrorMessage(String message) {
        if ("账号未注册".equals(message)) {
            LoginPhoneCodeFragment fragment = new LoginPhoneCodeFragment();

            String phone = tvCurrentPhone.getText().toString().trim();

            Bundle data = new Bundle();
            data.putString("phone", phone);
            fragment.setArguments(data);//将未注册的手机号带过注册、登录页

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
    }

    @Override
    public boolean isProtocolChecked() {
        boolean checked = checkboxPro.isChecked();
        return checked;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mAction = bundle.getInt("action", 3);
        token = bundle.getString("token");
    }
}
