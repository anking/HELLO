package com.yanhua.user.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.model.IDCradInfo;
import com.yanhua.user.presenter.CertifyPresenter;
import com.yanhua.user.presenter.contract.CertifyContract;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * 實名認證
 */
public class RealNameCertifyFragment extends BaseMvpFragment<CertifyPresenter> implements CertifyContract.IView {

    @BindView(R2.id.tvNextStep)
    TextView tvNextStep;
    @BindView(R2.id.etRealName)
    EditText etRealName;
    @BindView(R2.id.etRealIDNum)
    EditText etRealIDNum;

    @BindView(R2.id.ivIDFront)
    ImageView ivIDFront;
    @BindView(R2.id.ivIDBack)
    ImageView ivIDBack;

    @BindView(R2.id.llUploadIDBack)
    LinearLayout llUploadIDBack;
    @BindView(R2.id.llUploadIDFront)
    LinearLayout llUploadIDFront;

    private String idCardBack;// 身份证反面照片id
    private String idCardBackUrl;// 身份证反面照片url
    private String idCardFront;// 身份证正面照片id
    private String idCardFrontUrl;// 身份证正面照片url

    @Override
    public int bindLayout() {
        return R.layout.fragment_realname_certify;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new CertifyPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvNextStep);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        basePresenter.getStsToken();
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        //
        etRealName.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etRealIDNum.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String name = etRealName.getText().toString().trim();
        String idNum = etRealIDNum.getText().toString().trim();

        tvNextStep.setEnabled(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(idNum));
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
        if (id == R.id.tvNextStep) {
            //先识别身份，再而人脸识别
//            HashMap<String, Object> params = new HashMap<>();
//            params.put("name", etRealName.getText().toString().trim());
//            params.put("idCard", etRealIDNum.getText().toString().trim());
//            basePresenter.idCardVerification(params);//身份二要素认证

            //跳过身份二要素
            String name = etRealName.getText().toString().trim();
            String idNum = etRealIDNum.getText().toString().trim();

            FaceIdCertifyFragment fragment = new FaceIdCertifyFragment();

            Bundle data = new Bundle();
            data.putString("name", name);
            data.putString("idNum", idNum);
            fragment.setArguments(data);

            FragmentManager manager = this.getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @OnClick({R2.id.llUploadIDBack, R2.id.llUploadIDFront, R2.id.ivIDBack, R2.id.ivIDFront})
    public void onClickView(View view) {//点击保存
        int id = view.getId();
        if (id == R.id.llUploadIDBack || id == R.id.ivIDBack) {
            checkStoragePermission(CHOOSE_ID_CARD_BACKGROUND);
        } else if (id == R.id.llUploadIDFront || id == R.id.ivIDFront) {
            checkStoragePermission(CHOOSE_ID_CARD_FOREGROUND);
        }
    }

    private void checkStoragePermission(int imageRequestCode) {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseIdCardImage(imageRequestCode);
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    private void chooseIdCardImage(int imageRequestCode) {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
//                .withAspectRatio(79, 50)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(false)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(imageRequestCode);
    }

    //上传身份证正反面
    private static final int CHOOSE_ID_CARD_BACKGROUND = 1;
    private static final int CHOOSE_ID_CARD_FOREGROUND = 2;
    private final static int UPLOAD_ID_BG_SUCCESS = 1002;
    private final static int UPLOAD_ID_BG_FAIL = 1003;
    private final static int UPLOAD_ID_FG_SUCCESS = 1004;
    private final static int UPLOAD_ID_FG_FAIL = 1005;
    private StsTokenModel stsTokenModel;
    private MyHandler mHandler = new MyHandler();

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_ID_BG_SUCCESS:
                    llUploadIDBack.setVisibility(View.VISIBLE);
                    ImageLoaderUtil.loadImgCenterCrop(ivIDBack, idCardBackUrl + "?x-oss-process=image/watermark,text_5LuF5L6b6Z-z5ZyI5bmz5Y-w5a6e5ZCN6K6k6K-B5pe25L2_55So77yM5LuW55So5peg5pWI,color_8D46FB,size_40,shadow_50,g_center");
                    break;
                case UPLOAD_ID_FG_SUCCESS:
                    llUploadIDFront.setVisibility(View.VISIBLE);

                    ImageLoaderUtil.loadImgCenterCrop(ivIDFront, idCardFrontUrl + "?x-oss-process=image/watermark,text_5LuF5L6b6Z-z5ZyI5bmz5Y-w5a6e5ZCN6K6k6K-B5pe25L2_55So77yM5LuW55So5peg5pWI,color_8D46FB,size_40,shadow_50,g_center");

                    basePresenter.checkIdCardInformation(idCardFrontUrl);
                    break;
                case UPLOAD_ID_FG_FAIL:
                case UPLOAD_ID_BG_FAIL:
                    ToastUtils.showShort(R.string.upload_pic_fail);
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_ID_CARD_BACKGROUND && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();
            String path = null;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }
            File bgFile = new File(path);
            long mb = bgFile.length() / 1024 / 1024;
            if (mb >= 10) {
                ToastUtils.showShort("身份证图片过大，请重新上传");
                return;
            }
            if (stsTokenModel == null) {
                stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
            }
            String fileName = bgFile.getName();
            int index = fileName.lastIndexOf(".");
            String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
            OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, path, new OSSPushListener() {
                @Override
                public void onProgress(long currentSize, long totalSize) {
                    // 这里不用关注进度
                }

                @Override
                public void onSuccess(PutObjectResult result) {
                    String resultJson = result.getServerCallbackReturnBody();
                    Type type = new TypeToken<HttpResult<FileResult>>() {
                    }.getType();
                    HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                    FileResult fileResult = httpResult.getData();
                    idCardBackUrl = fileResult.getHttpUrl();
                    idCardBack = fileResult.getId();
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_ID_BG_SUCCESS;
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onFailure() {
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_ID_BG_FAIL;
                    mHandler.sendMessage(msg);
                }
            });
        } else if (requestCode == CHOOSE_ID_CARD_FOREGROUND && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();
            String path = null;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }
            File fgFile = new File(path);
            long mb = fgFile.length() / 1024 / 1024;
            if (mb >= 10) {
                ToastUtils.showShort("身份证图片过大，请重新上传");
                return;
            }
            if (stsTokenModel == null) {
                stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
            }
            String fileName = fgFile.getName();
            int index = fileName.lastIndexOf(".");
            String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
            OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, path, new OSSPushListener() {
                @Override
                public void onProgress(long currentSize, long totalSize) {
                    // 这里不用关注进度
                }

                @Override
                public void onSuccess(PutObjectResult result) {
                    String resultJson = result.getServerCallbackReturnBody();
                    Type type = new TypeToken<HttpResult<FileResult>>() {
                    }.getType();
                    HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                    FileResult fileResult = httpResult.getData();
                    idCardFrontUrl = fileResult.getHttpUrl();
                    idCardFront = fileResult.getId();
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_ID_FG_SUCCESS;
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onFailure() {
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_ID_FG_FAIL;
                    mHandler.sendMessage(msg);
                }
            });
        }
    }

    @Override
    public void handleSuccessVerification(String jsonData) {
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            String Result = jsonObject.getString("Result");
            if (!"0".equals(Result)) {
                // 身份证姓名验证不通过
                String Description = jsonObject.getString("Description");
                ToastUtils.showShort(Description);
            } else {
                String name = etRealName.getText().toString().trim();
                String idNum = etRealIDNum.getText().toString().trim();

                FaceIdCertifyFragment fragment = new FaceIdCertifyFragment();

                Bundle data = new Bundle();
                data.putString("name", name);
                data.putString("idNum", idNum);
                fragment.setArguments(data);

                FragmentManager manager = this.getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleSuccessCheckIdCardInfo(IDCradInfo info) {
        if (info != null) {
            etRealName.setText(info.getName());
            etRealIDNum.setText(info.getIdNum());
        }
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }


}