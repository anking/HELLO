package com.yanhua.user.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.FriendModel;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.adapter.UserItemAdapter;
import com.yanhua.user.presenter.RelationListPresenter;
import com.yanhua.user.presenter.contract.RelationListContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 已关注列表
 */
public class AlreadyAtFragment extends BaseMvpFragment<RelationListPresenter> implements RelationListContract.IView {

    @BindView(R2.id.rv_content)
    RecyclerView rvContent;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    private int current = 1;
    private List<FriendModel> mList = new ArrayList<>();
    private UserItemAdapter mAdapter;

    int type;
    String userId;
    private String keyword;

    @Override
    public int bindLayout() {
        return R.layout.fragment_relation_user;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new RelationListPresenter();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        if (bundle != null) {
            type = bundle.getInt("type");
            userId = bundle.getString("userId");
        } else {
            type = 0;
            userId = "";
        }

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            refreshLayout.finishRefresh(1000);
            getUserList();
        });

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mList.isEmpty() || mList.size() % 20 != 0) {
                refreshLayout.finishLoadMore(1000, true, true);
                return;
            }
            current = current + 1;
            refreshLayout.finishLoadMore(1000);
            getUserList();
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new UserItemAdapter(mContext, type);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.shape_divide_line1_d8d8d8));

        rvContent.addItemDecoration(itemDecoration);
        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            FriendModel model = mList.get(pos);
            String userId = model.getUserId();
            if (cellType == 0) {
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userId)
                        .navigation();
            } else if (cellType == 1) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    if (UserManager.getInstance().isMyself(userId)) {
                        ToastUtils.showShort("不能关注自己");
                        return;
                    }

                    boolean isFollow = DiscoValueFormat.isFollow(model.getFollowStatus());
                    if (isFollow) {
                        AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                        new XPopup.Builder(mContext).asCustom(alertPopup).show();
                        alertPopup.setOnConfirmListener(() -> {
                            alertPopup.dismiss();
                            basePresenter.cancelFollowUser(userId);
                        });
                        alertPopup.setOnCancelListener(alertPopup::dismiss);
                    } else {
                        basePresenter.followUser(userId);
                    }
                });

            }
        });

        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        getUserList();
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    private void getUserList() {
        basePresenter.getFollowUserList(userId, current, 20, keyword);
    }

    public void getUserList(int cur) {
        current = cur;
        basePresenter.getFollowUserList(userId, current, 20, keyword);
    }

    @Override
    public void handleUserListSuccess(List<FriendModel> list) {
        if (current == 1) {
            mList.clear();
            if (list != null && !list.isEmpty()) {
                mList = list;
            }
        } else {
            if (list != null && !list.isEmpty()) {
                mList.addAll(list);
            }
        }
        mAdapter.setItems(mList);
    }

    @Override
    public void updateFollowUserSuccess() {

        current = 1;

        getUserList();
    }

}