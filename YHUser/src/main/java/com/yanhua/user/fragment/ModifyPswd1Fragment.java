package com.yanhua.user.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.CountDownTimerUtils;
import com.yanhua.core.util.RegUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 修改密码第1步
 */
public class ModifyPswd1Fragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_next_step)
    TextView tvNextStep;
    @BindView(R2.id.tv_phoneCode)
    TextView tvGetCode;

    @BindView(R2.id.etPhoneCode)
    EditText etPhoneCode;
    @BindView(R2.id.etPhone)
    EditText etPhone;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_modify_pswd1;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle data) {
        super.initData(data);

        if (null != data) {
            String phone = data.getString("phone", "");
            etPhone.setText(phone);
        }
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvNextStep);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_next_step) {//一键登录tv_next_step
            hideSoftKeyboard();

            String phoneCode = etPhoneCode.getText().toString().trim();//验证码
            String phone = etPhone.getText().toString().trim();//手机号

            if (TextUtils.isEmpty(phone)) {
                ToastUtils.showShort(R.string.please_input_phone);
                return;
            }
            if (!RegUtils.isMobilePhone(phone)) {
                ToastUtils.showShort(R.string.error_format_phone);
                return;
            }
            if (TextUtils.isEmpty(phoneCode)) {
                ToastUtils.showShort(R.string.please_input_code);
                return;
            }

            // 先校验验证码
            basePresenter.validateCode(phone, phoneCode, YXConfig.smsWay.modify);
        }
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhoneCode.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPhoneCode.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        tvNextStep.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    @OnClick(R2.id.tv_phoneCode)
    public void clickGetCode() {
        hideSoftKeyboard();
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort(R.string.please_input_phone);
        } else if (!RegUtils.isMobilePhone(phone)) {
            ToastUtils.showShort(R.string.error_format_phone);
        } else {
            if (UserManager.getInstance().isLogin()) {
                basePresenter.checkMobilePhone(phone);
            } else {
                basePresenter.getSmsCode(phone, YXConfig.smsWay.modify);
            }
        }
    }

    @Override
    public void handleBooleanResult(Boolean data) {
        if (data) {
            String phone = etPhone.getText().toString().trim();
            basePresenter.getSmsCode(phone, YXConfig.smsWay.modify);
        } else {
            ToastUtils.showShort("请使用当前账号绑定的手机号！");
        }
    }

    @Override
    public void handleSendSuccess() {
        ToastUtils.showShort(R.string.send_code_successful);
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    @Override
    public void handleValidateSuccess(String type) {
        String phone = etPhone.getText().toString().trim();
        String code = etPhoneCode.getText().toString().trim();

        ModifyPswd2Fragment fragment = new ModifyPswd2Fragment();

        Bundle data = new Bundle();
        data.putString("phone", phone);
        data.putString("code", code);
        fragment.setArguments(data);//

        FragmentManager manager = this.getActivity().getSupportFragmentManager();

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
