package com.yanhua.user.fragment;

import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.PasswordInputFilter;
import com.yanhua.core.util.RegUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.model.LoginResult;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;
import com.yanhua.user.view.LoginActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import cn.jiguang.verifysdk.api.JVerificationInterface;

public class LoginPasswordFragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_login_onekey)
    TextView loginOneKey;
    @BindView(R2.id.tv_login_phoneCode)
    TextView loginPhoneCode;
    @BindView(R2.id.tv_login_password)
    TextView loginPassword;
    @BindView(R2.id.tv_forget_password)
    TextView forgetPassword;
    @BindView(R2.id.tv_pro)
    TextView tvPro;
    @BindView(R2.id.et_phone)
    EditText etPhone;
    @BindView(R2.id.et_password)
    EditText etPassword;
    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;

    @BindView(R2.id.tv_eye)
    AliIconFontTextView tvEye;
    private boolean isEyePswdClose;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_login_password;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ((LoginActivity) getActivity()).setRootViewBackgroud(LoginActivity.PSWD_LOGIN);

        PasswordInputFilter.setPasswordInputFilter(etPassword);

        setOneKeyPro();
        applyDebouncingClickListener(true, loginOneKey, loginPhoneCode, loginPassword, forgetPassword, tvEye, checkboxPro);
    }

    //设置一键登录的协议描述
    public void setOneKeyPro() {
        String textContent = "注册/登录即表示同意音圈Disco服务条款和隐私条款";
        SpannableString spannableString = new SpannableString(textContent);
        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.userAgreement);
                    } else {
                        if (null == userAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.userAgreement);
                        } else {
                            toProtocolPage(userAgreement);
                        }
                    }
                }
            }
        }, 10, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
                if (FastClickUtil.isFastClick(500)) {
                    if (YXConfig.USE_WEB_SHOW_PRIVACY) {
                        PageJumpUtil.pageToPrivacy(YXConfig.protocol.privacyAgreement);
                    } else {
                        if (null == privacyAgreement) {
                            basePresenter.getAppProtocol(YXConfig.protocol.privacyAgreement);
                        } else {
                            toProtocolPage(privacyAgreement);
                        }
                    }
                }
            }
        }, 22, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private ProtocolModel userAgreement;
    private ProtocolModel privacyAgreement;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.userAgreement.equals(code)) {
                userAgreement = data;
            } else if (YXConfig.protocol.privacyAgreement.equals(code)) {
                privacyAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_login_onekey) {//
//            LoginOneKeyFragment fragment = new LoginOneKeyFragment();
//
//            FragmentManager manager = this.getActivity().getSupportFragmentManager();
//
//            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.replace(R.id.container, fragment);
//            transaction.commit();
            ARouter.getInstance().build(ARouterPath.VERIFY_PHONE_ACTIVITY).navigation();
        } else if (id == R.id.tv_login_phoneCode) {//验证码登录
            LoginPhoneCodeFragment fragment = new LoginPhoneCodeFragment();

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else if (id == R.id.tv_login_password) {//密码
            String password = etPassword.getText().toString().trim();
            String phone = etPhone.getText().toString().trim();

            boolean isSelect = checkboxPro.isChecked();
            if (isSelect) {
                if (TextUtils.isEmpty(phone)) {
                    ToastUtils.showShort(R.string.please_input_phone);
                    return;
                }
                if (!RegUtils.isMobilePhone(phone)) {
                    ToastUtils.showShort(R.string.error_format_phone);
                    return;
                }
                if (TextUtils.isEmpty(password) || password.length() < YXConfig.PASSWORD_MIN_BIT) {//密码最小位数
                    ToastUtils.showShort(R.string.error_format_pwd);
                    return;
                }

                basePresenter.login(phone, password, "password",null);
            } else {
                ToastUtils.showShort("请先阅读并同意相关协议");
            }
        } else if (id == R.id.tv_forget_password) {//修改密码---忘记密码
            String phone = etPhone.getText().toString().trim();
            ARouter.getInstance().build(ARouterPath.MODIFY_PSWD_ACTIVITY)
                    .withString("phone", phone)
                    .navigation();
        } else if (id == R.id.tv_eye) {
            isEyePswdClose = !isEyePswdClose;
            if (isEyePswdClose) {
                etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
            } else {
                etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            tvEye.setText(isEyePswdClose ? "\ue765" : "\ue751");

            String content = etPassword.getText().toString();
            etPassword.setSelection(content.length());
        } else if (id == R.id.checkbox_pro) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        }
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        //
        etPassword.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        addListenEdit();

        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(mContext);
        if (!verifyEnable) {
            loginOneKey.setVisibility(View.GONE);
        }
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPassword.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        loginPassword.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    @Override
    public void handleErrorMessage(String message) {
        if ("账号未注册".equals(message)) {
            LoginPhoneCodeFragment fragment = new LoginPhoneCodeFragment();

            String phone = etPhone.getText().toString().trim();

            Bundle data = new Bundle();
            data.putString("phone", phone);
            fragment.setArguments(data);//将未注册的手机号带过注册、登录页

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
    }

    @Override
    public void handleLoginResult(LoginResult loginResult) {
        mMMKV.setLogin(true);
        mMMKV.setUserId(loginResult.getId());
        mMMKV.setLoginToken(loginResult.getAccess_token());
        mMMKV.setRefreshToken(loginResult.getRefresh_token());

        // 获取用户详情
        basePresenter.getUserDetail(loginResult.getId());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo);
        ToastUtils.showShort(R.string.login_successful);

        EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGIN_SUCCESS, userInfo.getId()));

        //签署协议
        String privacyUserId = mMMKV.getShowUserProtocolId();
        String privacyId = mMMKV.getShowPrivacyProtocolId();

        HashMap<String, Object> params = new HashMap<>();
        params.put("deviceId", mMMKV.getUuid());
        params.put("protocolIds", privacyUserId + "," + privacyId);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", userInfo.getId());
        }
        basePresenter.userSignProtocol(params);

        getActivity().finish();
    }

    @Override
    public boolean isProtocolChecked() {
        boolean checked = checkboxPro.isChecked();
        return checked;
    }
}
