package com.yanhua.user.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.core.util.CountDownTimerUtils;
import com.yanhua.core.util.RegUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 修改密码第1步
 */
public class SetNewPhoneFragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_commit)
    TextView tvCommit;
    @BindView(R2.id.tv_phoneCode)
    TextView tvGetCode;

    @BindView(R2.id.etPhoneCode)
    EditText etPhoneCode;
    @BindView(R2.id.etPhone)
    EditText etPhone;

    String oriPhone;
    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_set_phone;
    }



    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle data) {
        super.initData(data);

        if (null != data) {
            oriPhone = data.getString("phone", "");
//            etPhone.setText(oriPhone);
        }
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvCommit);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_commit) {//一键登录tv_next_step
            hideSoftKeyboard();

            String phoneCode = etPhoneCode.getText().toString().trim();//验证码
            String phone = etPhone.getText().toString().trim();//手机号

            if (TextUtils.isEmpty(phone)) {
                ToastUtils.showShort(R.string.please_input_phone);
                return;
            }
            if (!RegUtils.isMobilePhone(phone)) {
                ToastUtils.showShort(R.string.error_format_phone);
                return;
            }
            if (TextUtils.isEmpty(phoneCode)) {
                ToastUtils.showShort(R.string.please_input_code);
                return;
            }

            // 先校验验证码
            basePresenter.validateCode(phone, phoneCode, YXConfig.smsWay.modify);//绑定新的手机号
        }
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        etPhone.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etPhoneCode.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPhoneCode.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        tvCommit.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(phone));
    }

    @OnClick(R2.id.tv_phoneCode)
    public void clickGetCode() {
        hideSoftKeyboard();
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort(R.string.please_input_phone);
        } else if (!RegUtils.isMobilePhone(phone)) {
            ToastUtils.showShort(R.string.error_format_phone);
        } else {
            basePresenter.getSmsCode(phone, YXConfig.smsWay.modify);
        }
    }

    @Override
    public void handleSendSuccess() {
        ToastUtils.showShort(R.string.send_code_successful);
        CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(tvGetCode, 60000, 1000);
        mCountDownTimerUtils.start();
    }

    @Override
    public void handleValidateSuccess(String type) {
        String phone = etPhone.getText().toString().trim();
        String code = etPhoneCode.getText().toString().trim();

        basePresenter.modifyPhoneNum(oriPhone, phone);
    }

    @Override
    public void handleModifyPhoneSuccess() {
        ToastUtils.showShort("更换手机号成功");

        getActivity().finish();
    }
}
