package com.yanhua.user.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.FaceUserInfoModel;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.model.FaceResultModel;
import com.yanhua.user.presenter.CertifyPresenter;
import com.yanhua.user.presenter.contract.CertifyContract;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * 人脸实名
 */
public class FaceIdCertifyFragment extends BaseMvpFragment<CertifyPresenter> implements CertifyContract.IView {

    @BindView(R2.id.tvNextStep)
    TextView tvNextStep;

    @BindView(R2.id.tvName)
    TextView tvName;

    String name;
    String idNum;

    @Override
    public int bindLayout() {
        return R.layout.fragment_faceid_certify;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new CertifyPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvNextStep);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        if (null != bundle) {
            name = bundle.getString("name");
            idNum = bundle.getString("idNum");
        }

        tvName.setText("确认" + YHStringUtils.realNameDataMasking(name) + "本人操作");
    }

    private void checkOnId() {
        if (!TextUtils.isEmpty(name)) {
            if (!TextUtils.isEmpty(idNum)) {
                if (idNum.contains("x")) {
                    idNum = idNum.replace('x', 'X');
                }
                basePresenter.getFaceId(idNum, name, UserManager.getInstance().getUserId());
            } else {
                ToastUtils.showShort("证件号不能为空");
            }
        } else {
            ToastUtils.showShort("姓名不能为空");
        }
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
        if (id == R.id.tvNextStep) {//
            checkOnId();//
        }
    }

    @Override
    public void handleSuccessFaceId(FaceResultModel faceModel) {
        FaceUserInfoModel faceUserInfoModel = new FaceUserInfoModel(name, idNum);

        faceUserInfoModel.setOrderNo(faceModel.getOrderNo());
        faceUserInfoModel.setSign(faceModel.getSign());
        faceUserInfoModel.setFaceId(faceModel.getFaceId());
        faceUserInfoModel.setNonce(faceModel.getNonce());

        EventBus.getDefault().post(new MessageEvent("UPDATE_USER_INFO", faceUserInfoModel));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}