package com.yanhua.user.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.core.util.PasswordInputFilter;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.user.R;
import com.yanhua.user.R2;
import com.yanhua.user.listener.OnValueChangeTextWatcher;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

/**
 * 修改密码第2步
 */
public class ModifyPswd2Fragment extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.tv_commit)
    TextView tvCommit;
    @BindView(R2.id.et_password)
    EditText etPassword;
    @BindView(R2.id.et_confirmPassword)
    EditText etConfirmPassword;

    @BindView(R2.id.tv_eye1)
    AliIconFontTextView tvEye;
    @BindView(R2.id.tv_eye2)
    AliIconFontTextView tvConfirmEye;

    private boolean isEyePswdClose, isConfirmEyePswdClose;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_modify_pswd2;
    }


    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        //设置密码不可以输入其他中文
        PasswordInputFilter.setPasswordInputFilter(etPassword);
        PasswordInputFilter.setPasswordInputFilter(etConfirmPassword);

        applyDebouncingClickListener(true, tvCommit, tvEye, tvConfirmEye);
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tv_commit) {//
            String phone = getArguments().getString("phone");
            String code = getArguments().getString("code");

            String password = etPassword.getText().toString().trim();
            String confirmPassword = etConfirmPassword.getText().toString().trim();

            if (TextUtils.isEmpty(password)) {
                ToastUtils.showShort(R.string.please_input_pwd);
                return;
            }
            if (TextUtils.isEmpty(confirmPassword)) {
                ToastUtils.showShort(R.string.please_input_confirm_pwd);
                return;
            }
            if (!password.equals(confirmPassword)) {
                ToastUtils.showShort(R.string.different_pwd);
                return;
            }

            if (!YHStringUtils.validPassword(confirmPassword)) {
                ToastUtils.showShort(R.string.valid_pswd_error);
                return;
            }

            basePresenter.reset(phone, code, password, confirmPassword);
        } else if (id == R.id.tv_eye1) {
            isEyePswdClose = !isEyePswdClose;

            setEyeVisiable(isEyePswdClose, etPassword, tvEye);
        } else if (id == R.id.tv_eye2) {
            isConfirmEyePswdClose = !isConfirmEyePswdClose;

            setEyeVisiable(isConfirmEyePswdClose, etConfirmPassword, tvConfirmEye);
        }
    }

    /**
     * 设置眼睛visible
     *
     * @param isClose
     * @param editText
     * @param eye
     */
    private void setEyeVisiable(boolean isClose, EditText editText, TextView eye) {
        if (isClose) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        eye.setText(isClose ? "\ue765" : "\ue751");

        String content = editText.getText().toString();
        editText.setSelection(content.length());
    }


    @Override
    public void doBusiness() {
        super.doBusiness();

        etPassword.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        etConfirmPassword.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        //初始化
        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String password = etPassword.getText().toString().trim();
        String passwordConfirm = etConfirmPassword.getText().toString().trim();

        tvCommit.setEnabled(!TextUtils.isEmpty(password) && !TextUtils.isEmpty(passwordConfirm));
    }

    @Override
    public void handlePswdResetResult() {
        EventBus.getDefault().post(new MessageEvent(EventName.LOGIN_PAGE_CLOSE));

        ToastUtils.showShort(R.string.reset_successful);

        new Handler().postDelayed(() -> {
            onReLogin();
            getActivity().onBackPressed();
        }, 1000);
    }
}
