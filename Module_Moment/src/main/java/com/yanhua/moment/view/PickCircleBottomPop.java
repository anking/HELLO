package com.yanhua.moment.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.adapter.PickCircleListAdapter;
import com.yanhua.moment.presenter.PublishContentPresenter;
import com.yanhua.moment.presenter.contract.PublishContentContract;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * 选择圈子
 *
 * @author Administrator
 */
public class PickCircleBottomPop extends BaseMvpBottomPopupView<PublishContentPresenter> implements PublishContentContract.IView {
    private final int PICK_MAX = 3;
    private Context mContext;
    TextView tvCancel;
    TextView tvOK;
    DataObserverRecyclerView rvContent;
    TagFlowLayout tflHistory, tflPicked;
    private LinearLayout llHeaderView;

    SmartRefreshLayout refreshLayout;
    EditText etInput;
    TextView tvPickedNum;
    LinearLayout llPickedHistory, llPicked;

    private PickCircleListAdapter mAdapter;
    private OnSelectResultListener mListener;
    private String keyWord;

    List<CircleModel> pickedList;
    List<CircleModel> historyList;

    @Override
    protected void creatPresent() {
        basePresenter = new PublishContentPresenter();
    }

    public PickCircleBottomPop(@NonNull Context context, List<CircleModel> pickList,OnSelectResultListener listener) {
        super(context);
        mContext = context;
        mListener = listener;

        pickedList = new ArrayList<>();
        if (null!=pickList&&pickList.size()>0) {
            pickedList.addAll(pickList);
        }
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_popup_pick_circle;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

        initData();
    }

    private void initView() {
        rvContent = findViewById(R.id.rvContent);
        tvCancel = findViewById(R.id.tvCancel);
        tvOK = findViewById(R.id.tvOK);
        etInput = findViewById(R.id.etInput);

        refreshLayout = findViewById(R.id.refreshLayout);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);
//        rvContent.setEmptyView();

        tvOK.setOnClickListener(v -> {
            mListener.selectResult(pickedList);
            dismiss();
        });
        tvCancel.setOnClickListener(v -> dismiss());

        etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                keyWord = s.toString().trim();

                if (!TextUtils.isEmpty(keyWord)) {
                    basePresenter.getCircleJoinedList(keyWord, 1, 20);//搜索
                } else {
                    current = 1;
                    basePresenter.getCircleJoinedList(keyWord, current, size);//搜索
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                keyWord = "";

                etInput.setText(keyWord);
                basePresenter.getCircleJoinedList(keyWord, current, size);
            }
        });


        mAdapter = new PickCircleListAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            CircleModel item = mAdapter.getItemObject(pos - 1);

            updatePickData(item);
        });
    }

    /**
     * @param item
     */
    private void updatePickData(CircleModel item) {
        int listSize = pickedList.size();

        if (!pickedList.contains(item)) {
            if (listSize < PICK_MAX) {
                pickedList.add(item);//加入
                tflPicked.getAdapter().refreshData(pickedList);
            } else {
                ToastUtils.showShort("可选取圈子数量不可超过" + PICK_MAX+"个");
            }
        } else {
            ToastUtils.showShort("话题已经选取");
        }

        llPicked.setVisibility(pickedList.size() > 0 ? VISIBLE : GONE);
        tvPickedNum.setText("已选择" + pickedList.size() + "/" + PICK_MAX);
    }

    protected void initData() {
        View header = LayoutInflater.from(mContext).inflate(R.layout.head_popup_pick_circle, rvContent, false);
        mAdapter.addHeaderView(header);
        tflHistory = header.findViewById(R.id.tflHistory);
        tflPicked = header.findViewById(R.id.tflPicked);
        tvPickedNum = header.findViewById(R.id.tvPickedNum);
        llPickedHistory = header.findViewById(R.id.llPickedHistory);
        llPicked = header.findViewById(R.id.llPicked);

        llHeaderView = header.findViewById(R.id.llHeaderView);

        basePresenter.getCircleJoinedList("", current, size);


        llPicked.setVisibility(pickedList.size() > 0 ? VISIBLE : GONE);
        tvPickedNum.setText("已选择" + pickedList.size() + "/" + PICK_MAX);


        TagAdapter tagAdapterPicked = new TagAdapter<CircleModel>(pickedList) {
            @Override
            public View getView(FlowLayout parent, int position, CircleModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_topic_picked, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                AliIconFontTextView iconDelete = view.findViewById(R.id.iconDelete);

                tvTagName.setText("◎" + tagItem.getTitle());
                iconDelete.setOnClickListener(v -> {
                    pickedList.remove(position);
                    tflPicked.getAdapter().refreshData(pickedList);

                    llPicked.setVisibility(pickedList.size() > 0 ? VISIBLE : GONE);
                    tvPickedNum.setText("已选择" + pickedList.size() + "/" + PICK_MAX);
                });
                return view;
            }
        };

        //控制显示5个
        tflPicked.setAdapter(tagAdapterPicked);
        tflPicked.setOnTagClickListener((view, position, parent) -> {
//                ToastUtils.showShort(tagList.get(position).getName());
            return true;
        });

        historyList = new ArrayList<>();
        Gson gson = new Gson();
        String json = DiscoCacheUtils.getInstance().getCircleHistoryList();

        Type listType = new TypeToken<List<CircleModel>>() {
        }.getType();
        if (!TextUtils.isEmpty(json)) {
            historyList.addAll(gson.fromJson(json, listType));
        }

        if (historyList.size() > 0) {
            llPickedHistory.setVisibility(VISIBLE);
        } else {
            llPickedHistory.setVisibility(GONE);
        }

        TagAdapter tagAdapterHistory = new TagAdapter<CircleModel>(historyList) {
            @Override
            public View getView(FlowLayout parent, int position, CircleModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_topic_history, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                tvTagName.setText("◎"+tagItem.getTitle());
                return view;
            }
        };

        //控制显示5个
        tflHistory.setAdapter(tagAdapterHistory);
        tflHistory.setOnTagClickListener((view, position, parent) -> {
            CircleModel item = historyList.get(position);
            updatePickData(item);

            return true;
        });
    }

    @Override
    protected int getMaxHeight() {
        return DisplayUtils.getScreenHeight(mContext) * 7 / 8;
    }


    public interface OnSelectResultListener {
        void selectResult(List<CircleModel> pickedList);
    }

    @Override
    public void handleCircleList(ListResult<CircleModel> listResult) {
        int total = listResult.getTotal();

        List<CircleModel> dataList = new ArrayList<>();
        if (!TextUtils.isEmpty(keyWord)) {
            //搜索状态
            if (total == 0) {
                //圈子未搜索到结果不显示，话题需要显示，并可以新增
//                CircleModel newTopic = new CircleModel();
//                newTopic.setTitle(keyWord);
//
//                dataList.add(newTopic);
            } else {
                dataList.addAll(listResult.getRecords());
            }

            mAdapter.setItems(dataList);
        } else {
            dataList.addAll(listResult.getRecords());
            //
            if (current == 1) {
                mAdapter.setItems(dataList);
            } else {
                mAdapter.setMoreData(dataList);
            }

            if (mAdapter.getItemCount() + dataList.size() < total) {
                current++;
            } else {
                refreshLayout.setNoMoreData(true);
            }
        }
    }
}
