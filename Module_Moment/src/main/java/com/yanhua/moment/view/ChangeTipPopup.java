package com.yanhua.moment.view;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.AttachPopupView;
import com.yanhua.moment.R;

public class ChangeTipPopup extends AttachPopupView {
    private ChangeTipPopupListener mListener;
    private String remark;

    public ChangeTipPopup(@NonNull Context context, String remark, ChangeTipPopupListener listener) {
        super(context);
        this.remark = remark;

        mListener = listener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.change_tip_popup;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        final TextView tv = findViewById(R.id.tv);

        tv.setText(this.remark);

        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChangeTipClick();
            }
        });
    }

    public interface ChangeTipPopupListener {
        void onChangeTipClick();
    }
}
