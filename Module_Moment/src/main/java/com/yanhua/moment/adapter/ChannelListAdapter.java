package com.yanhua.moment.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.gridlayout.widget.GridLayout;
import androidx.lifecycle.LifecycleOwner;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.reflect.TypeToken;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.CustomTextViewTouchListener;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.listener.SpanTopicCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.adapter.AdvertiseBannerAdapter;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.TagFlowModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.ShowDiscoListImage;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.SampleCoverVideo;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.RectangleIndicator;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ChannelListAdapter extends BaseRecyclerAdapter<MomentListModel, ChannelListAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mListener;
    private LifecycleOwner mLifecycleOwner;

    public ChannelListAdapter(Context context, LifecycleOwner lifecycleOwner, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mLifecycleOwner = lifecycleOwner;
        mListener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_moment_chanenl, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentListModel item) {
        int resultType = item.getResultType();
        if (resultType == 1) {
            holder.flAd.setVisibility(View.GONE);
            holder.llItem.setVisibility(View.VISIBLE);
            setContentDetail(holder, item);
        } else if (resultType == 2) {
            holder.flAd.setVisibility(View.VISIBLE);
            holder.llItem.setVisibility(View.GONE);

            setAdContent(holder, item);
        } else {
            holder.flAd.setVisibility(View.GONE);
            holder.llItem.setVisibility(View.GONE);
        }
    }

    private void setAdContent(ViewHolder holder, MomentListModel item) {
        int position = getPosition(holder);

        List<AdvertiseModel> advertiseList = item.getAdvertisingList();

        if (null != advertiseList && advertiseList.size() > 0) {
            //取出第一个获取长高
            AdvertiseModel firstItem = advertiseList.get(0);

            String imgUrl = firstItem.getImageUrl();
            String videoCoverUrl = firstItem.getVideoCoverUrl();
            String showUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

            Glide.with(mContext)
                    .asBitmap()
                    .load(showUrl)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picWidth = resource.getWidth();
                            int picHeight = resource.getHeight();

                            int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
                            int height = width * picHeight / picWidth;

                            ViewGroup.LayoutParams layoutParams = holder.flAd.getLayoutParams();
                            layoutParams.width = width;
                            layoutParams.height = height;
                            holder.flAd.setLayoutParams(layoutParams);

                            AdvertiseBannerAdapter adapter = new AdvertiseBannerAdapter(advertiseList, mContext);
                            holder.bannerAd.setAdapter(adapter);

                            if (null != advertiseList && advertiseList.size() > 1) {
                                holder.bannerAd.addBannerLifecycleObserver(mLifecycleOwner)
                                        .setLoopTime(3000)
                                        .isAutoLoop(false)
                                        .setIndicator(new RectangleIndicator(mContext))
                                        .setIndicatorRadius(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                                        .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.sub_first))
                                        .setIndicatorHeight(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorWidth(DisplayUtils.dip2px(mContext, 6), DisplayUtils.dip2px(mContext, 12))
                                        .setIndicatorGravity(IndicatorConfig.Direction.CENTER)
                                        .setIndicatorSpace(DisplayUtils.dip2px(mContext, 3))
                                        .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 9)));
                            }

                            holder.bannerAd.setOnBannerListener((data, adPos) -> {
                                mListener.onHandleClick(OnHandleClickListener.CLICK_AD, item, position, adPos);
                            });
                        }
                    });
        }
    }

    private void setContentDetail(@NotNull ViewHolder holder, @NotNull MomentListModel item) {
        int position = getPosition(holder);
        //基础信息
        String nickName = YHStringUtils.pickName(item.getNickName(), item.getFriendRemark());
        holder.tvUserName.setText(nickName);

        String publishTime = item.getCreatedTime();
        holder.tvPublishTime.setText(YXTimeUtils.getFriendlyTimeAtContent(publishTime, true));//将后台返回数据进行加工处理

        //操作按钮
        int fabulousCount = item.getFabulousCount();
        boolean isFabulous = item.isFabulous();
        holder.tvLikeNum.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");

        holder.iconLike.setText(isFabulous ? "\ue772" : "\ue71b");
        holder.iconLike.setSelected(isFabulous);

        int favCount = item.getCollectCount();
        boolean isFav = item.isCollect();
        holder.tvFavNum.setText(favCount > 0 ? YHStringUtils.quantityFormat(favCount) : "收藏");

        holder.iconFav.setText(isFav ? "\ue760" : "\ue742");
        holder.iconFav.setSelected(isFav);

        int replyCount = item.getReplyCount();
        holder.tvCommentNum.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

        String userPhoto = item.getUserPhoto();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }

        String pUserId = item.getUserId();
        if (UserManager.getInstance().isMyself(pUserId)) {
            holder.llFollow.setVisibility(View.GONE);
        } else {
            holder.llFollow.setVisibility(View.VISIBLE);

            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
            //    private Integer followStatus;
            int followStatus = item.getFollowStatus();

            boolean isFollow = DiscoValueFormat.isFollow(followStatus);
            holder.iconFollowStatus.setVisibility(isFollow ? View.GONE : View.VISIBLE);

            holder.tvFollow.setTextColor(mContext.getResources().getColor(isFollow ? R.color.assistEACBFF : R.color.theme));
            holder.tvFollow.setText(isFollow ? "已关注" : "关注");

            holder.tvFollow.setTextSize(Dimension.SP, (isFollow ? 12 : 16));
            holder.llFollow.setBackgroundResource(isFollow ? R.drawable.shape_half_r_eacbff : R.drawable.shape_half_r_tran);
        }

        holder.llFollow.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.FOLLOW, item, position, -1));
        holder.llFav.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.COLLECT, item, position, -1));
        holder.llComment.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.COMMENT, item, position, -1));
        holder.llLike.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.THUMB_UP, item, position, -1));
        holder.llItem.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position, -1));

        List<TagFlowModel> tagList = new ArrayList<>();
        String issueCity = item.getIssueCity();
        String issueAddress = item.getIssueAddress();
        String issueAddressDetail = item.getIssueAddressDetail();
        String issueLatitude = item.getIssueLatitude();
        String issueLongitude = item.getIssueLongitude();

        if (TextUtils.isEmpty(issueLatitude) && TextUtils.isEmpty(issueLongitude)) {
        } else {
            String address = "";
            if (!TextUtils.isEmpty(issueCity)) {
                address = TextUtils.isEmpty(issueAddress) ? issueCity : issueCity + "." + issueAddress;
            } else if (!TextUtils.isEmpty(issueAddress)) {
                address = issueAddress;
            }

            if (!TextUtils.isEmpty(address)) {
                TagFlowModel addressTag = new TagFlowModel(TagFlowModel.ADDRESS, "0", address, R.mipmap.ic_address);
                addressTag.setIssueLatitude(issueLatitude);
                addressTag.setIssueLongitude(issueLongitude);

                tagList.add(addressTag);
            }
        }

        //获取圈子
        List<CircleModel> circleList = item.getCircleList();
        if (null != circleList && circleList.size() > 0) {
            for (CircleModel circleItem : circleList) {
                TagFlowModel tag = new TagFlowModel(TagFlowModel.CIRCLE, circleItem.getId(), circleItem.getTitle(), R.mipmap.ic_circle);

                if (circleItem.getDeleted() != 1) {
                    tagList.add(tag);
                }
            }
        }

        if (tagList.size() > 0) {
            holder.tflMomentItem.setVisibility(View.VISIBLE);
            //控制显示5个
            holder.tflMomentItem.setAdapter(new TagAdapter<TagFlowModel>(tagList) {
                @Override
                public View getView(FlowLayout parent, int position, TagFlowModel tagItem) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow, parent, false);
                    //获取标签
                    TextView tvTagName = view.findViewById(R.id.tvTagName);
                    ImageView ivTagIcon = view.findViewById(R.id.ivTagIcon);

                    ivTagIcon.setImageResource(tagItem.getDrawable());
                    tvTagName.setText(tagItem.getName());
                    return view;
                }
            });
            holder.tflMomentItem.setOnTagClickListener((view, pos, parent) -> {
                TagFlowModel tagFlowModel = tagList.get(pos);
                if (tagFlowModel.getType() == TagFlowModel.ADDRESS) {

                } else {
                    ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                            .withString("id", tagFlowModel.getId()).navigation();
                }

                return true;
            });
        } else {
            holder.tflMomentItem.setVisibility(View.GONE);
        }

        //获取图片1 视频2 文章3
        int contentType = item.getContentType();

        String urlJson = item.getContentUrl();
        String content = "";
        switch (contentType) {
            case 1:
            case 4:
                holder.gsyVideoPlayer.setVisibility(View.GONE);
                holder.rlVideoItemPlayer.setVisibility(View.GONE);
                holder.flTopTitle.setVisibility(View.GONE);

                content = item.getContent();

                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    List<String> imageUrl = new ArrayList<>();
                    for (FileResult fileResult : fileResults) {
                        imageUrl.add(fileResult.getHttpUrl());
                    }

                    if (!imageUrl.isEmpty()) {
                        holder.flImgContainer.setVisibility(View.VISIBLE);
                        ShowDiscoListImage.showGridImageNew(mContext,holder.gridImage,holder.tvIndex, imageUrl);
                    } else {
                        holder.flImgContainer.setVisibility(View.GONE);
                    }
                }

                break;
            case 2:
                holder.flImgContainer.setVisibility(View.GONE);

                holder.gsyVideoPlayer.setVisibility(View.VISIBLE);
                holder.rlVideoItemPlayer.setVisibility(View.VISIBLE);
                holder.flTopTitle.setVisibility(View.GONE);

                content = item.getContent();
                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    if (!fileResults.isEmpty()) {
                        holder.rlVideoItemPlayer.setVisibility(View.VISIBLE);

                        holder.gsyVideoPlayer.setOnVideoListener(new SampleCoverVideo.OnVideoListener() {
                            @Override
                            public void setOnMute(boolean isMute) {
//                    HomeFollowAdapter.this.notifyDataSetChanged();
                            }
                        });

                        holder.gsyVideoPlayer.setOnDataNetPlayListener(new SampleCoverVideo.OnDataNetPlayListener() {
                            @Override
                            public void play() {
                                YXConfig.needShowNoWifi = false;
                                String currentTime = String.valueOf(System.currentTimeMillis());
                                DiscoCacheUtils.getInstance().setFirstOpenTime(currentTime);
                            }

                            @Override
                            public void stop() {
                            }

                            @Override
                            public void detail() {
                                if (mListener != null) {
                                    mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position, -1);
                                }
                            }
                        });

                        holder.gsyVideoPlayer.setVideoTime(item.getVideoTime());

                        FileResult fileResult = fileResults.get(0);

                        String coverImage = fileResult.getHttpUrl();
                        String url = fileResults.get(1).getHttpUrl();//

                        if (!url.equals(holder.gsyVideoPlayer.getTag())) {
                            int width = Integer.parseInt(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());
                            int height = Integer.parseInt(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());

                            holder.gsyVideoPlayer.loadCoverImage(coverImage, R.drawable.bg_tran_circle_no_stroke, width, height);

                            holder.gsyVideoPlayer.setUpLazy(url, true, null, null, "");
                            //增加title
                            holder.gsyVideoPlayer.getTitleTextView().setVisibility(View.GONE);

                            //设置返回键
                            holder.gsyVideoPlayer.getBackButton().setVisibility(View.GONE);

                            //设置全屏按键功能
                            holder.gsyVideoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    resolveFullBtn(holder.gsyVideoPlayer);
                                }
                            });
                            holder.gsyVideoPlayer.setRotateViewAuto(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setLockLand(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setPlayTag(TAG);
                            holder.gsyVideoPlayer.setReleaseWhenLossAudio(false);
                            holder.gsyVideoPlayer.setAutoFullWithSize(true);
                            holder.gsyVideoPlayer.setShowFullAnimation(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setIsTouchWiget(false);
                            holder.gsyVideoPlayer.setNeedShowWifiTip(false);
                            //循环
                            holder.gsyVideoPlayer.setNeedLockFull(true);
                            holder.gsyVideoPlayer.setPlayPosition(position);

                            ViewGroup.LayoutParams imageLayoutParams = holder.gsyVideoPlayer.getLayoutParams();
                            int sw = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 24);//设置图片的宽


                            if (height > 0 && width > 0) {
                                int showWidth;
                                int showHeight;
                                if (sw > width) {
                                    showWidth = width;
                                    showHeight = height;
                                } else {
                                    showWidth = sw;
                                    showHeight = sw * height / width;
                                }

                                imageLayoutParams.width = showWidth;
                                imageLayoutParams.height = showHeight;
                            } else {
                                imageLayoutParams.width = sw;
                                imageLayoutParams.height = imageLayoutParams.width * 3 / 4;
                            }

                            holder.gsyVideoPlayer.setLayoutParams(imageLayoutParams);
                            holder.gsyVideoPlayer.setTag(url);
                        }

                    } else {
                        holder.rlVideoItemPlayer.setVisibility(View.GONE);
                    }
                } else {
                    holder.rlVideoItemPlayer.setVisibility(View.GONE);
                }
                break;
            case 3:
                holder.flImgContainer.setVisibility(View.GONE);
                holder.gsyVideoPlayer.setVisibility(View.GONE);
                holder.rlVideoItemPlayer.setVisibility(View.GONE);
                holder.flTopTitle.setVisibility(View.VISIBLE);

                //先获取导语，没有则从正文里头取
                content = YHStringUtils.pickLastFirst(item.getIntroduce(), item.getContent());
                content = YHStringUtils.getHtmlContent(content);
                contentType = 3;

                holder.tvTopTitle.setText(item.getContentTitle());

                ViewGroup.LayoutParams imageLayoutParams = holder.flTopTitle.getLayoutParams();
                int sw = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 24);//设置图片的宽

                imageLayoutParams.width = sw;
                imageLayoutParams.height = imageLayoutParams.width * 3 / 5;
                holder.flTopTitle.setLayoutParams(imageLayoutParams);

                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    if (null != fileResults && fileResults.size() > 0) {
                        FileResult fileResult = fileResults.get(0);

                        ImageLoaderUtil.loadImgCenterCrop(holder.ivTopCover, fileResult.getHttpUrl());
                    }
                }
                break;
        }

        //文本内容展示
        if (!TextUtils.isEmpty(content)) {
            List<FriendModel> nameList = item.getUserList();//At的用户

            List<TopicModel> topicList = new ArrayList<>();//存在的话题

            List<TopicModel> topicListItem = item.getTopicList();//话题
            if (null != topicListItem && topicListItem.size() > 0) {
                for (TopicModel topic : topicListItem) {
                    if (topic.getDeleted() != 1) {
                        topicList.add(topic);
                    }
                }
            }

            if (contentType == 3) {
                if (null != topicList && topicList.size() > 0) {
                    String topicContent = "";

                    for (int i = 0; i < topicList.size(); i++) {
                        topicContent = topicContent + "#" + topicList.get(i).getTitle() + "# ";
                    }
                    content = topicContent + content;
                }
            }

            //直接使用RichTextView
            SpanAtUserCallBack spanAtUserCallBack = (view, userModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userModel.getUserId()).navigation();
            };

            SpanTopicCallBack spanTopicCallBack = (view, topicModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                        .withString("id", topicModel.getId()).navigation();
            };

            holder.richTextView.setOnTouchListener(new CustomTextViewTouchListener(
                    v -> {
                        mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position, -1);
                    }
            ));
            holder.richTextView.setSpanAtUserCallBackListener(spanAtUserCallBack);
            holder.richTextView.setSpanTopicCallBackListener(spanTopicCallBack);
            //所有配置完成后才设置text
            content = YHStringUtils.firstAtAddSpace(content);
            holder.richTextView.setRichText(content, null != nameList ? nameList : new ArrayList<>(), topicList);

            holder.richTextView.setVisibility(View.VISIBLE);
        } else {
            holder.richTextView.setVisibility(View.GONE);
        }
    }


    private void showGridImage(ViewHolder holder, List<String> imgUrls) {
        ViewGroup.LayoutParams imageLayoutParams = holder.gridImage.getLayoutParams();
        holder.gridImage.removeAllViews();

        int imgSize = imgUrls.size();

        int screenWidth = DisplayUtils.getScreenWidth(mContext);
        //默认初始值--
        //展示1:1，大致为宽的一半。计算方式为screenWidth-左右边距24
        int showWidth = (screenWidth - DisplayUtils.dip2px(mContext, 14));
        int showHeight = showWidth / 2;

        if (imgSize >= 3) {
            int leftShowCount = imgSize - 3;
            if (leftShowCount > 0) {
                holder.tvIndex.setVisibility(View.VISIBLE);
                holder.tvIndex.setText(leftShowCount + "+");
            } else {
                holder.tvIndex.setVisibility(View.GONE);
                holder.tvIndex.setText("0");
            }
            ////展示2:1，大致为宽的一半。计算方式为screenWidth-左右边距24,第一张图比较大
            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showWidth / 3 * 2;
        } else {
            holder.tvIndex.setVisibility(View.GONE);
            holder.tvIndex.setText("0");

            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showHeight;
        }

        holder.gridImage.setLayoutParams(imageLayoutParams);

        //根据UI的逻辑，一张和两张展示都只有一行
        //1--（0,0），（0,1）
        //2--（0,0），（0,1）
        //3--（0,0），（0,1），（1,1）

        //使用Spec定义子控件的位置和比重 设置行列下标， 所占行列  ，比重
        // 设置行列下标， 所占行列  ，比重
        // 对应： layout_row  , layout_rowSpan , layout_rowWeight
        //GridLayout.spec(0, 2, 2f)
        if (imgSize >= 3) {
            for (int i = 0; i < imgUrls.size(); i++) {
                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                if (i == 0) {
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 2, 2f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 2f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);

                    holder.gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 1) {//(0,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                    layoutParams.bottomMargin = DisplayUtils.dip2px(mContext, 1);

                    holder.gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 2) {////(1,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                    layoutParams.topMargin = DisplayUtils.dip2px(mContext, 1);

                    holder.gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
            }
        } else if (imgSize == 2) {
            for (int i = 0; i < imgUrls.size(); i++) {

                ImageView imageView = new ImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 1f);

                //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                layoutParams.height = 0;
                layoutParams.width = 0;
                //还可以根据位置动态定义子控件直接的边距，下面的意思是

                if (i == 0)
                    layoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);
                if (i == 1) {
                    layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
                }

                holder.gridImage.addView(imageView, layoutParams);
                ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
            }
        } else {
            ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            GridLayout.Spec imgRowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec imgColumnSpec = GridLayout.spec(0, 1, 1f);

            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams imglayoutParams = new GridLayout.LayoutParams(imgRowSpec, imgColumnSpec);
            imglayoutParams.height = 0;
            imglayoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是
            imglayoutParams.rightMargin = DisplayUtils.dip2px(mContext, 1);
            holder.gridImage.addView(imageView, imglayoutParams);
            ImageLoaderUtil.loadImg(imageView, imgUrls.get(0));

            //占位的
            View functionView = new View(mContext);
            GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);
            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            layoutParams.height = 0;
            layoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是

            layoutParams.leftMargin = DisplayUtils.dip2px(mContext, 1);
            holder.gridImage.addView(functionView, layoutParams);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tflMomentItem)
        TagFlowLayout tflMomentItem;
        @BindView(R2.id.gridImage)
        GridLayout gridImage;
        @BindView(R2.id.tvIndex)
        OswaldTextView tvIndex;
        @BindView(R2.id.richText)
        RichTextView richTextView;
        @BindView(R2.id.flImgContainer)
        FrameLayout flImgContainer;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvPublishTime)
        TextView tvPublishTime;
        //关注
        @BindView(R2.id.llFollow)
        LinearLayout llFollow;
        @BindView(R2.id.iconFollowStatus)
        AliIconFontTextView iconFollowStatus;
        @BindView(R2.id.tvFollow)
        TextView tvFollow;
        //收藏
        @BindView(R2.id.llFav)
        LinearLayout llFav;
        @BindView(R2.id.iconFav)
        AliIconFontTextView iconFav;
        @BindView(R2.id.tvFavNum)
        TextView tvFavNum;
        //评论
        @BindView(R2.id.llComment)
        LinearLayout llComment;

        @BindView(R2.id.llItem)
        LinearLayout llItem;

        @BindView(R2.id.iconComment)
        AliIconFontTextView iconComment;
        @BindView(R2.id.tvCommentNum)
        TextView tvCommentNum;
        //点赞
        @BindView(R2.id.llLike)
        LinearLayout llLike;
        @BindView(R2.id.iconLike)
        AliIconFontTextView iconLike;
        @BindView(R2.id.tvLikeNum)
        TextView tvLikeNum;

        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.video_item_player)
        SampleCoverVideo gsyVideoPlayer;
        @BindView(R2.id.rlvideo_item_player)
        RelativeLayout rlVideoItemPlayer;
        @BindView(R2.id.container)
        FrameLayout container;
        @BindView(R2.id.flTopTitle)
        FrameLayout flTopTitle;
        @BindView(R2.id.ivTopCover)
        ImageView ivTopCover;
        @BindView(R2.id.tvTopTitle)
        TextView tvTopTitle;

        @BindView(R2.id.flAd)
        FrameLayout flAd;
        @BindView(R2.id.bannerAd)
        Banner bannerAd;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnHandleClickListener {
        int FOLLOW = 1;//关注
        int COLLECT = 2;//收藏
        int COMMENT = 3;//评论
        int THUMB_UP = 4;//点赞
        int CLICK_AD = 5;//点击广告
        int TO_DETAIL = 0;//详情

        void onHandleClick(int type, MomentListModel item, int pos, int adPos);
    }


    protected OrientationUtils orientationUtils;
    protected boolean isPlay;
    protected boolean isFull;
    public static final String TAG = "ChannelListAdapter";

    /**
     * 全屏幕按键处理
     */
    private void resolveFullBtn(final StandardGSYVideoPlayer standardGSYVideoPlayer) {
        if (getListNeedAutoLand() && orientationUtils != null) {
            resolveFull();
        }
        standardGSYVideoPlayer.startWindowFullscreen(context, false, true);
    }

    public void clearCache() {
    }

    public boolean isFull() {
        return isFull;
    }

    /**************************支持全屏重力全屏的部分**************************/

    /**
     * 列表时是否需要支持重力旋转
     *
     * @return 返回true为支持列表重力全屏
     */
    public boolean getListNeedAutoLand() {
        return true;
    }

    private void initOrientationUtils(StandardGSYVideoPlayer standardGSYVideoPlayer, boolean full) {
        orientationUtils = new OrientationUtils((Activity) context, standardGSYVideoPlayer);
        //是否需要跟随系统旋转设置
        //orientationUtils.setRotateWithSystem(false);
        orientationUtils.setEnable(false);
        orientationUtils.setIsLand((full) ? 1 : 0);
    }

    private void resolveFull() {
        if (getListNeedAutoLand() && orientationUtils != null) {
            //直接横屏
            orientationUtils.resolveByClick();
        }
    }

    private void onQuitFullscreen() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
    }

    public void onAutoComplete() {
        if (orientationUtils != null) {
            orientationUtils.setEnable(false);
            orientationUtils.releaseListener();
            orientationUtils = null;
        }
        isPlay = false;
    }

    public void onPrepared() {
        if (orientationUtils == null) {
            return;
        }
        //开始播放了才能旋转和全屏
        orientationUtils.setEnable(true);
    }

    public OrientationUtils getOrientationUtils() {
        return orientationUtils;
    }

    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
    }

    public void onDestroy() {
        if (orientationUtils != null) {
            orientationUtils.releaseListener();
            orientationUtils = null;
        }
    }

}
