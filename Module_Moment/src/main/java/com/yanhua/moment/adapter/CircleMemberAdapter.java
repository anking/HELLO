package com.yanhua.moment.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;

public class CircleMemberAdapter extends BaseRecyclerAdapter<MomentCircleMemberModel, CircleMemberAdapter.ViewHolder> {
    public CircleMemberAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_member, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentCircleMemberModel item) {
        int position = getPosition(holder);

        holder.tvName.setText(item.getNickName());

        String address = item.getAddress();
        if (TextUtils.isEmpty(address)) {
            holder.llAddress.setVisibility(View.GONE);
        } else {
            holder.llAddress.setVisibility(View.VISIBLE);

            holder.tvAddress.setText(item.getAddress());
        }

        ImageLoaderUtil.loadImg(holder.ivUserHead, item.getImg());

        //用户性别,(-1:保密, 1:男, 2:女)
        int sex = item.getGenderCode();
        if (sex == -1) {
            holder.ivInfoSex.setVisibility(View.GONE);
        } else {
            holder.ivInfoSex.setVisibility(View.VISIBLE);
            holder.ivInfoSex.setImageResource(sex == 2 ? R.mipmap.ic_female : R.mipmap.ic_male);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvName)
        TextView tvName;

        @BindView(R2.id.tvAddress)
        TextView tvAddress;

        @BindView(R2.id.llAddress)
        LinearLayout llAddress;

        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;


        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
