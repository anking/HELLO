package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;


public class CircleListJoinAdapter extends BaseRecyclerAdapter<CircleModel, CircleListJoinAdapter.ViewHolder> {
    public CircleListJoinAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_joined, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText(YHStringUtils.formatCircle(item.getTitle()) );
        holder.tvCircleNum.setText("帖子：" + YHStringUtils.quantityFormat(item.getDynamicCount()));

        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover,item.getCoverUrl());



    }


    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
