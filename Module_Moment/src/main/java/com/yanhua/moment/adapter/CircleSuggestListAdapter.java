package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;


public class CircleSuggestListAdapter extends BaseRecyclerAdapter<CircleModel, CircleSuggestListAdapter.ViewHolder> {

    public boolean isHome;

    public CircleSuggestListAdapter(Context context, boolean isHome) {
        super(context);
        this.isHome = isHome;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_suggest, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.contentDivite.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        holder.tvCircieName.setText(YHStringUtils.value(item.getTitle()));
        holder.tvCircleNum.setText("帖子：" + YHStringUtils.quantityFormat(item.getDynamicCount()));
        holder.tvDesc.setText(YHStringUtils.value(item.getCircleDesc()));

        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl()+ YXConfig.IMAGE_RESIZE);

        holder.ll2Detail.setVisibility(isHome ? View.VISIBLE : View.GONE);
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;

        @BindView(R2.id.ll2Detail)
        LinearLayout ll2Detail;

        @BindView(R2.id.contentDivite)
        View contentDivite;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
