package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleModel;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;

//
public class HotCircleListAdapter extends BaseRecyclerAdapter<CircleModel, HotCircleListAdapter.ViewHolder> {
    public HotCircleListAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_ranking_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        if (position < 3) {
            holder.viewDot.setVisibility(View.VISIBLE);
            holder.otvOrder.setTextSize(Dimension.SP, 20);
        } else {
            holder.viewDot.setVisibility(View.GONE);
            holder.otvOrder.setTextSize(Dimension.SP, 16);
        }

        holder.tvTag.setText("◎" + item.getTitle());
        holder.otvOrder.setText(String.valueOf(position + 1));
        holder.tvViewCount.setText(YHStringUtils.quantityFormat(item.getDynamicCount()));
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvTag)
        TextView tvTag;
        @BindView(R2.id.otvOrder)
        OswaldTextView otvOrder;
        @BindView(R2.id.tvViewCount)
        TextView tvViewCount;

        @BindView(R2.id.viewDot)
        View viewDot;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
