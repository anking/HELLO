package com.yanhua.moment.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.reflect.TypeToken;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.adapter.AdvertiseBannerAdapter;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.RectangleIndicator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MomentVideoAdapter extends BaseRecyclerAdapter<MomentListModel, MomentVideoAdapter.ViewHolder> {

    private Context mContext;
    private int mDiscoBest;
    private String mCity;


    public MomentVideoAdapter(Context context, int disco, String city) {
        super(context);
        mContext = context;
        mDiscoBest = disco;
        mCity = city;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_moment_video, parent, false));
    }

    private void setContentDetail(ViewHolder holder, MomentListModel item) {
        int position = getPosition(holder);

        //获取图片1 视频2 文章3
        int contentType = item.getContentType();

        String urlJson = item.getContentUrl();
        String coverImage = null;

        ViewGroup.LayoutParams imageLayoutParams = holder.ivCover.getLayoutParams();
        int realWidth = DisplayUtils.getScreenWidth(context) / 2 - DisplayUtils.dip2px(context, 16);//

        if (!TextUtils.isEmpty(urlJson)) {
            Type listType = new TypeToken<ArrayList<FileResult>>() {
            }.getType();
            ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

            FileResult fileResult = fileResults.get(0);
            coverImage = fileResult.getHttpUrl();

            //后台返回null
            int width = Integer.parseInt(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());
            int height = Integer.parseInt(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());

            if (width != 0 && height != 0) {
                float rate = DisplayUtils.getImageRate(width, height);//4:3 1:1 3:4
                float showHeight = realWidth / rate;

                imageLayoutParams.width = realWidth;
                imageLayoutParams.height = (int) showHeight;
            } else {
                imageLayoutParams.width = realWidth;
                imageLayoutParams.height = realWidth;
            }

            holder.ivCover.setLayoutParams(imageLayoutParams);

            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage + "?x-oss-process=image/resize,p_50", R.drawable.place_holder, 12);
        } else {
            imageLayoutParams.width = realWidth;
            imageLayoutParams.height = realWidth;

            holder.ivCover.setLayoutParams(imageLayoutParams);
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 12);
        }

        holder.ivPlay.setVisibility(contentType == 2 ? View.VISIBLE : View.GONE);

        String content = YHStringUtils.pickLastFirst(item.getIntroduce(), item.getContent());
        if (!TextUtils.isEmpty(content)) {
            holder.tvSuggestTitle.setVisibility(View.VISIBLE);
            holder.tvSuggestTitle.setText(YHStringUtils.getHtmlContent(content));
        } else {
            holder.tvSuggestTitle.setVisibility(View.GONE);
        }

        //话题
        List<TopicModel> topicList = new ArrayList<>();//存在的话题

        List<TopicModel> topicListItem = item.getTopicList();//话题
        if (null != topicListItem && topicListItem.size() > 0) {
            for (TopicModel topic : topicListItem) {
                if (topic.getDeleted() != 1) {
                    topicList.add(topic);
                }
            }
        }

        if (null != topicList && topicList.size() > 0) {
            holder.tvSuggestTopic.setVisibility(View.VISIBLE);
            holder.tvSuggestTopic.setText(YHStringUtils.formatTopic(topicList.get(0).getTitle()));
        } else {
            holder.tvSuggestTopic.setVisibility(View.GONE);
        }

        String userPhoto = item.getUserPhoto();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }

        String nickName = YHStringUtils.pickName(item.getNickName(), item.getFriendRemark());
        holder.tvUserName.setText(nickName);
        //点赞数量
        int fabulousCount = item.getFabulousCount();
        holder.tvLikeNum.setText(fabulousCount > 0 ? YHStringUtils.quantityEnFormat(fabulousCount) : "点赞");

        boolean isFabulous = item.isFabulous();
        holder.iconLike.setText(isFabulous?"\ue772":"\ue71b");
        holder.iconLike.setSelected(isFabulous);

        holder.llItem.setOnClickListener(v -> {
            //跳转到详情
            if (contentType == 2) {
                ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                        .withSerializable("contentModel", item)
                        .withString("categoryId", "")
                        .withInt("discoBest", mDiscoBest)//电音集锦
                        .withString("city", TextUtils.isEmpty(mCity) ? "" : mCity)
                        .withInt("module", 0)
                        .navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                        .withString("id", item.getId())
                        .navigation();
            }
        });

        holder.llLike.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onHandleClick(OnHandleClickListener.THUMB_UP, item, position);
            }
        });
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentListModel item) {
        int resultType = item.getResultType();
        if (resultType == 1) {
            holder.flAd.setVisibility(View.GONE);
            holder.llItem.setVisibility(View.VISIBLE);
            setContentDetail(holder, item);
        } else if (resultType == 2) {
            holder.flAd.setVisibility(View.VISIBLE);
            holder.llItem.setVisibility(View.GONE);

            setAdContent(holder, item);
        } else {
            holder.flAd.setVisibility(View.GONE);
            holder.llItem.setVisibility(View.GONE);
        }
    }

    private void setAdContent(ViewHolder holder, MomentListModel item) {
        List<AdvertiseModel> advertiseList = item.getAdvertisingList();

        if (null != advertiseList && advertiseList.size() > 0) {
            //取出第一个获取长高
            AdvertiseModel firstItem = advertiseList.get(0);

            String imgUrl = firstItem.getImageUrl();
            String videoCoverUrl = firstItem.getVideoCoverUrl();
            String showUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

            Glide.with(mContext)
                    .asBitmap()
                    .load(showUrl)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picWidth = resource.getWidth();
                            int picHeight = resource.getHeight();

                            int realWidth = DisplayUtils.getScreenWidth(context) / 2 - DisplayUtils.dip2px(context, 16);//
                            int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
                            int height = width * picHeight / picWidth;

                            ViewGroup.LayoutParams layoutParams = holder.flAd.getLayoutParams();
                            if (width != 0 && height != 0) {
                                float rate = DisplayUtils.getImageRate(width, height);//4:3 1:1 3:4
                                float showHeight = realWidth / rate;

                                layoutParams.width = realWidth;
                                layoutParams.height = (int) showHeight;
                            } else {
                                layoutParams.width = realWidth;
                                layoutParams.height = realWidth;
                            }

                            holder.flAd.setLayoutParams(layoutParams);

                            AdvertiseBannerAdapter adapter = new AdvertiseBannerAdapter(advertiseList, mContext);
                            holder.bannerAd.setAdapter(adapter);

                            if (null != advertiseList && advertiseList.size() > 1) {
                                holder.bannerAd
                                        .setLoopTime(3000)
                                        .isAutoLoop(false)
                                        .setIndicator(new RectangleIndicator(mContext))
                                        .setIndicatorRadius(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                                        .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.sub_first))
                                        .setIndicatorHeight(DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorWidth(DisplayUtils.dip2px(mContext, 6), DisplayUtils.dip2px(mContext, 12))
                                        .setIndicatorGravity(IndicatorConfig.Direction.CENTER)
                                        .setIndicatorSpace(DisplayUtils.dip2px(mContext, 3))
                                        .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 9)));
                            }

                            holder.bannerAd.setOnBannerListener((data, adPos) -> {
                                AdvertiseModel clickItem = (AdvertiseModel) data;
                                if (null != mAdListener) {
                                    mAdListener.onADClick(clickItem);
                                }
                            });
                        }
                    });
        }
    }

    private OnADClickListener mAdListener;

    public interface OnADClickListener {
        void onADClick(AdvertiseModel clickItem);
    }

    public void setOnADClickListener(OnADClickListener listener) {
        this.mAdListener = listener;
    }


    private OnHandleClickListener mListener;

    public void setOnHandleClickListener(OnHandleClickListener listener) {
        mListener = listener;
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.ivPlay)
        ImageView ivPlay;
        @BindView(R2.id.tvSuggestTitle)
        TextView tvSuggestTitle;
        @BindView(R2.id.tvSuggestTopic)
        TextView tvSuggestTopic;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.llLike)
        LinearLayout llLike;
        @BindView(R2.id.tvLikeNum)
        TextView tvLikeNum;

        @BindView(R2.id.iconLike)
        AliIconFontTextView iconLike;
        @BindView(R2.id.llItem)
        LinearLayout llItem;

        @BindView(R2.id.flAd)
        FrameLayout flAd;
        @BindView(R2.id.bannerAd)
        Banner bannerAd;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
