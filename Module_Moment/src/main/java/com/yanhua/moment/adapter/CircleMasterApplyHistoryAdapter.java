package com.yanhua.moment.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;

public class CircleMasterApplyHistoryAdapter extends BaseRecyclerAdapter<CircleModel, CircleMasterApplyHistoryAdapter.ViewHolder> {
    public CircleMasterApplyHistoryAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_apply_history, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText("◎" + item.getTitle());

        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl());
        if (TextUtils.isEmpty(item.getCreatedTime())) {
            holder.tvCircieApplyTime.setVisibility(View.GONE);
        } else {
            holder.tvCircieApplyTime.setVisibility(View.VISIBLE);
            holder.tvCircieApplyTime.setText(item.getCreatedTime() + "申请");
        }

        //
        int status = item.getAuditStatus();////圈主审核状态;0待审核 1圈主 2驳回//auditStatus
        String content = "审核结果：";
        if (0 == status) {
            content = content + "审核中";
        } else if (1 == status) {
            content = content + "已通过，你已成为新圈主";
        } else if (2 == status) {
            content = content + "圈主申请失败";
        }

        holder.tvCircieApplyStatus.setText(content);
        holder.tvCircieApplyStatus.setVisibility(View.VISIBLE);
        holder.tvCircieApplyResult.setVisibility(View.GONE);
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircieApplyTime)
        TextView tvCircieApplyTime;//-申请
        @BindView(R2.id.tvCircieApplyStatus)
        TextView tvCircieApplyStatus;//审核结果：-
        @BindView(R2.id.tvCircieApplyResult)
        TextView tvCircieApplyResult;//审核结果：-


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
