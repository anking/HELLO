package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;

public class CircleTransferAdapter extends BaseRecyclerAdapter<MomentCircleMemberModel, CircleTransferAdapter.ViewHolder> {
    public CircleTransferAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_transfer, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentCircleMemberModel item) {
        int position = getPosition(holder);

        holder.tvName.setText(item.getNickName());
        boolean isSelect = item.isSelect();
        if (!isSelect) {
            holder.iconSelect.setVisibility(View.GONE);
        } else {
            holder.iconSelect.setVisibility(View.VISIBLE);
        }

        ImageLoaderUtil.loadImg(holder.ivUserHead, item.getImg());

        //用户性别,(-1:保密, 1:男, 2:女)
        int sex = item.getGenderCode();
        if (sex == -1) {
            holder.ivInfoSex.setVisibility(View.GONE);
        } else {
            holder.ivInfoSex.setVisibility(View.VISIBLE);
            holder.ivInfoSex.setImageResource(sex == 2 ? R.mipmap.ic_female : R.mipmap.ic_male);
        }
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;

        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;

        @BindView(R2.id.iconSelect)
        AliIconFontTextView iconSelect;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
