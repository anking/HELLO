package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.common.model.CircleModel;

import butterknife.BindView;


public class PickCircleListAdapter extends BaseRecyclerAdapter<CircleModel, PickCircleListAdapter.ViewHolder> {
    public PickCircleListAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_pick_circle_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvTopicName.setText(YHStringUtils.formatCircle(item.getTitle()) );
        holder.tvTopicNum.setText(YHStringUtils.quantityFormat(item.getDynamicCount()) + "内容");
    }


    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvTopicName)
        TextView tvTopicName;

        @BindView(R2.id.tvTopicNum)
        TextView tvTopicNum;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
