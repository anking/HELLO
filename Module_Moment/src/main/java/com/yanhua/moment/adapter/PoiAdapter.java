package com.yanhua.moment.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.amap.api.services.core.PoiItem;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.moment.R2;
import com.yanhua.moment.R;

import butterknife.BindView;

public class PoiAdapter extends BaseRecyclerAdapter<PoiItem, PoiAdapter.ViewHolder> {


    public PoiAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_poi, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull PoiItem item) {
        String title = item.getTitle();
        String address = item.getSnippet();
        holder.tvTitle.setText(!TextUtils.isEmpty(title) ? title : "");
        holder.tvAddress.setText(!TextUtils.isEmpty(address) ? address : "");
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tv_title)
        TextView tvTitle;
        @BindView(R2.id.tv_address)
        TextView tvAddress;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
