package com.yanhua.moment.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;

public class CircleApplyHistoryAdapter extends BaseRecyclerAdapter<CircleModel, CircleApplyHistoryAdapter.ViewHolder> {
    public CircleApplyHistoryAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_circle_apply_history, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText("◎" + item.getTitle());
        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl());
        holder.tvCircieApplyTime.setText(item.getCreatedTime() + "申请");

        holder.tvCircieApplyStatus.setVisibility(View.GONE);
        holder.tvCircieApplyResult.setVisibility(View.VISIBLE);

        holder.tvCircieApplyResult.setText(item.getStatus());

        //0待审核 1已通过 2未通过
        int statusCode = item.getStatusCode();
        if (statusCode == 0) {
            holder.tvCircieApplyResult.setTextColor(Color.parseColor("#FE9D1E"));
            holder.iconNext.setVisibility(View.GONE);
        } else if (statusCode == 1) {
            holder.tvCircieApplyResult.setTextColor(Color.parseColor("#33C35C"));
            holder.iconNext.setVisibility(View.GONE);
        } else if (statusCode == 2) {
            holder.tvCircieApplyResult.setTextColor(Color.parseColor("#FF4B19"));
            holder.iconNext.setVisibility(View.VISIBLE);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircieApplyTime)
        TextView tvCircieApplyTime;//-申请
        @BindView(R2.id.tvCircieApplyStatus)
        TextView tvCircieApplyStatus;//审核结果：-
        @BindView(R2.id.tvCircieApplyResult)
        TextView tvCircieApplyResult;//审核结果：-
        @BindView(R2.id.llStatusText)
        LinearLayout llStatusText;//
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;//

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
