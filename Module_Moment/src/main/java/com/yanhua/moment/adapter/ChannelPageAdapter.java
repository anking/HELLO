package com.yanhua.moment.adapter;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.yanhua.common.model.Channel;
import com.yanhua.moment.fragment.MomentChanelFragment;
import com.yanhua.moment.fragment.MomentCircleFragment;
import com.yanhua.moment.fragment.MomentVideoFragment;

import java.util.List;

public class ChannelPageAdapter extends FragmentStatePagerAdapter {
    private List<Channel> mMenuList;

    public ChannelPageAdapter(@NonNull FragmentManager fm, List<Channel> list) {
        super(fm);
        this.mMenuList = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Channel model = mMenuList.get(position);
        int type = model.getType();
        String idType = model.getId();

        Fragment fragment = null;

        if (-1 == type) {
            //圈子
            fragment = new MomentCircleFragment();
        } else if (type == 4) {
            //视频
            fragment = new MomentVideoFragment();
        } else {
            fragment = new MomentChanelFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);//此刻首页的类型

        int objType = model.getType();
        String circleId = model.getCircleId();
        String topicId = model.getTipicId();

        bundle.putInt("objType", objType);
        bundle.putString("circleId", circleId);
        bundle.putString("idType", idType);
        bundle.putString("topicId", topicId);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mMenuList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        Channel model = mMenuList.get(position);
        String reportName = model.getReportName();
        return !TextUtils.isEmpty(reportName) ? reportName : "";
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
    }
}
