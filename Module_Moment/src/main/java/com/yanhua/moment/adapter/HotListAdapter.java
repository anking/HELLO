package com.yanhua.moment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.MomentHotListModel;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;



public class HotListAdapter extends BaseRecyclerAdapter<MomentHotListModel, HotListAdapter.ViewHolder> {
    public HotListAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_hot_list, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentHotListModel item) {
        int position = getPosition(holder);

        if (position == YXConfig.MAX_HOT_LIST) {
            holder.llHotItem.setVisibility(View.GONE);
            holder.llHotMore.setVisibility(View.VISIBLE);
        } else {
            holder.llHotItem.setVisibility(View.VISIBLE);
            holder.llHotMore.setVisibility(View.GONE);
        }

        if (null != item) {
            int type = item.getHotListType();
            if (type == MomentHotListModel.TOPIC) {
                holder.tvTag.setText("#" + item.getTitle() + "#");
            } else {
                holder.tvTag.setText("◎" + item.getTitle());
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.llHotItem)
        LinearLayout llHotItem;
        @BindView(R2.id.llHotMore)
        LinearLayout llHotMore;
        @BindView(R2.id.tvTag)
        TextView tvTag;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
