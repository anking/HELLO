package com.yanhua.moment.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.MomentVideoAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 此刻视频数据
 */
public class MomentVideoFragment extends BaseMvpFragment<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private int type;
    private int objType;
    private String circleId;
    private String topicId;
    private String idType;

    //电音集集锦
    private int discoBest;
    private String id;
    private String city;
    private String name;

    private MomentVideoAdapter mAdapter;
    private List<MomentListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_moment_video;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getInt("type");
            objType = bundle.getInt("objType");
            circleId = bundle.getString("circleId");
            topicId = bundle.getString("topicId");
            idType = bundle.getString("idType");

            discoBest = bundle.getInt("discoBest");
            id = bundle.getString("id");
            city = bundle.getString("city");
            name = bundle.getString("name");
        }

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    /**
     * 获取列表数据
     *
     * @param page
     */
    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            if (discoBest == 1) {
                basePresenter.getDiscoBestList("",city, current, size);
            } else {
                basePresenter.getMomentPageList("", type, current, size);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(discoBest == 1);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new MomentVideoAdapter(mContext, discoBest, city);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        mAdapter.setOnADClickListener(clickItem -> {
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);
            PageJumpUtil.adJumpContent(clickItem, (Activity) mContext, -1);
        });
    }


    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
