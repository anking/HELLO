package com.yanhua.moment.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.SlidingTabLayout;
import com.github.dfqin.grantor.PermissionListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.Channel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentHotListModel;
import com.yanhua.common.model.PublishContent;
import com.yanhua.common.model.PublishContentForm;
import com.yanhua.common.model.PublishSaveTemp;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OssManager;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.HotTabView;
import com.yanhua.common.widget.MomentHotListDecoration;
import com.yanhua.core.util.FileUtils;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.ChannelPageAdapter;
import com.yanhua.moment.adapter.HotListAdapter;
import com.yanhua.moment.presenter.PublishContentPresenter;
import com.yanhua.moment.presenter.contract.PublishContentContract;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import top.zibin.luban.Luban;


public class MomentFragment extends BaseMvpFragment<PublishContentPresenter> implements PublishContentContract.IView {

    @BindView(R2.id.rl_publish_upload_progress)
    LinearLayout publishUpload;
    @BindView(R2.id.iv_publish_cover)
    ImageView publishCover;
    @BindView(R2.id.tv_publish_show_progress)
    TextView publishShowProgress;
    @BindView(R2.id.tv_publish_desc)
    TextView publishDesc;
    @BindView(R2.id.tv_publish_retry)
    TextView publishRetry;
    @BindView(R2.id.pb_publish)
    ProgressBar publishPB;

    @BindView(R2.id.toolbar)
    LinearLayout toolbar;
    @BindView(R2.id.ll_to_search)
    LinearLayout llToSearch;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.rl_hot_topic)
    HotTabView hotTabTopic;
    @BindView(R2.id.rl_hot_circle)
    HotTabView hotTabCircle;
    @BindView(R2.id.rcvHotList)
    RecyclerView rcvHotList;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;
    @BindView(R2.id.ll_to_publish)
    LinearLayout llToPublish;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    ////////////////////////////////
    private HotListAdapter hotListAdapter;
    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new PublishContentPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_moment;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        UltimateBarX.addStatusBarTopPadding(toolbar);

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setOnRefreshListener(rl -> {
            refreshData();

            rl.finishRefresh(1000);
        });

        refreshLayout.autoRefresh(500);
        refreshLayout.setEnableLoadMore(false);

        //顶部tab控制
        setTopTabSwitch(true, false);

        GridLayoutManager managerContent = new GridLayoutManager(mContext, 2);
        rcvHotList.addItemDecoration(new MomentHotListDecoration(mContext, 15));

        hotListAdapter = new HotListAdapter(mContext);
        rcvHotList.setLayoutManager(managerContent);
        rcvHotList.setAdapter(hotListAdapter);
        rcvHotList.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        setTopView();

        EventBus.getDefault().register(this);


        hotListAdapter.setOnItemClickListener((itemView, pos) -> {
            List<MomentHotListModel> hotList = hotListAdapter.getmDataList();
            MomentHotListModel item = hotList.get(pos);

            if (null != item) {
                int type = item.getHotListType();
//                ARouter.getInstance().build(ARouterPath.RANKING_LIST_ACTIVITY).withInt("rankingTopic", type).navigation(getContext());
                ARouter.getInstance().build(type == MomentHotListModel.TOPIC ? ARouterPath.TOPIC_DETAIL_ACTIVITY : ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                        .withString("id", item.getId()).navigation(getContext());
            } else {
                boolean isTopic = hotTabTopic.isTabSelected();

                ARouter.getInstance().build(ARouterPath.RANKING_LIST_ACTIVITY).withInt("rankingTopic", isTopic ? MomentHotListModel.TOPIC : MomentHotListModel.CIRCLE).navigation(getContext());
            }
        });
    }

    /**
     * 刷新数据
     */
    private void refreshData() {
        //需要更新当前tab的数据MomentCHANELFragment
        if (null != mAdapter && null != mChannelList && mChannelList.size() > 0) {
            int curPos = sltabChannel.getCurrentTab();

            Channel channel = mChannelList.get(curPos);
            EventBus.getDefault().post(new MessageEvent(CommonConstant.MOMENT_HOME_LIST_REFRESH, String.valueOf(channel.getType())));
        }

        //话题热榜最多获取100
        basePresenter.getHotCircleList("", 1, 10);
        basePresenter.getHotTopicList("", 1, 10);
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);

            if (alp >= 0 && alp < 125) {
                llToSearch.setBackgroundResource(R.drawable.shape_bg_r_white);
                llToSearch.getBackground().setAlpha(255 - alp);
            } else if (alp >= 125 && alp <= 255) {
                llToSearch.setBackgroundResource(R.drawable.shape_bg_r_bgui);
                llToSearch.getBackground().setAlpha(alp);
            }

            toolbar.setBackgroundColor(Color.argb(alp, 255, 255, 255));

        });
    }

    /**
     * 顶部tab控制
     *
     * @param topicSelect
     */
    private void setTopTabSwitch(boolean topicSelect, boolean setData) {
        hotTabTopic.setTabSelected(topicSelect);
        hotTabCircle.setTabSelected(!topicSelect);
        if (setData) {
            if (topicSelect) {
                initHotTopicData();
            } else {
                initHotCircleData();
            }
        }
    }

    @OnClick({R2.id.rl_hot_circle, R2.id.rl_hot_topic, R2.id.ll_to_publish, R2.id.ll_to_search})
    public void onViewClick(View v) {
        int id = v.getId();
        if (id == R.id.rl_hot_topic) {
            if (hotTabTopic.isTabSelected()) {
                return;
            }
            setTopTabSwitch(true, true);
        } else if (id == R.id.rl_hot_circle) {
            if (hotTabCircle.isTabSelected()) {
                return;
            }
            setTopTabSwitch(false, true);
        } else if (id == R.id.ll_to_publish) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                if (UserManager.getInstance().getUserInfo().getStatus() == 2) {
                    ToastUtils.showShort("账号被限制使用，请与平台客服联系");
                    return;
                }
                if (UserManager.getInstance().getUserInfo().getStatus() == 3 || UserManager.getInstance().getUserInfo().getStatus() == 4) {
                    return;
                }
                checkPermission();
            });
        } else if (id == R.id.ll_to_search) {
            ARouter.getInstance().build(ARouterPath.HOME_SEARCH_ACTIVITY).navigation();
        }
    }

    public void checkPermission() {
        String[] permissionsO = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };

        requestCheckPermission(permissionsO, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                doPublishSource();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
            }
        });
    }

    private void doPublishSource() {
        PublishSaveTemp temp = PublishTemp.getPublishSaveTemp(mContext);
        if (null != temp) {
            ArrayList<String> pathList = temp.getPathList();
            String videoPath = temp.getVideoPath();
            String coverPath = temp.getCoverPath();
            int type = temp.getType();
            int videoTime = temp.getVideoTime();
            String videoCoverPath = temp.getVideoCoverPath();

            ArrayList<LocalMedia> dataList = temp.getDataList();

            ARouter.getInstance().build(ARouterPath.PUBLISH_CONTENT_ACTIVITY)
                    .withInt("type", type)
                    .withInt("videoTime", videoTime)
                    .withString("videoPath", videoPath)
                    .withString("coverPath", coverPath)
                    .withString("videoCoverPath", videoCoverPath)
                    .withParcelableArrayList("dataList", dataList)
//                    .withSerializable("describeList", descList)
                    .withSerializable("pathList", pathList).withFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .navigation(mActivity);
        } else {
            PictureSelector.create(this)
                    .openGallery(PictureMimeType.ofAll())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                    .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                    .isWeChatStyle(false)
                    .setPictureStyle(PickImageUtil.getBlackStyle(this.getActivity()))

//                    .setPictureStyle(PickImageUtil.getWhiteStyle(this.getActivity()))
                    .isUseCustomCamera(false)// 是否使用自定义相机
                    .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                    .isWithVideoImage(false)// 图片和视频是否可以同选,只在ofAll模式下有效
                    .isMaxSelectEnabledMask(false)// 选择数到了最大阀值列表是否启用蒙层效果,启动蒙层会比较丑
                    .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                    .isOpenStyleNumComplete(false)
                    .maxSelectNum(9)// 最大图片选择数量
                    .minSelectNum(1)// 最小选择数量
                    .maxVideoSelectNum(9) // 视频最大选择数量
                    .videoMinSecond(3)
//                    .filterMinFileSize(100)
                    .recordVideoMinSecond(3)
                    .imageSpanCount(3)// 每行显示个数
                    .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                    .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                    .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                    .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                    .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                    .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                    .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                    .isPreviewImage(true)// 是否可预览图片
                    .isPreviewVideo(false)// 是否可预览视频
                    .isCamera(false)// 是否显示拍照按钮
                    .isZoomAnim(false)// 图片列表点击 缩放效果 默认true
                    .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                    .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                    .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                    .isEnableCrop(false)// 是否裁剪
                    .isCompress(false)// 是否压缩
                    .synOrAsy(false)//同步true或异步false 压缩 默认同步
                    .isGif(false)// 是否显示gif图片
                    .cutOutQuality(90)// 裁剪输出质量 默认100
                    .minimumCompressSize(100);// 小于多少kb的图片不压缩
            try {
                Class cls = Class.forName("com.yanhua.media.activity.YXVideoRecordActivity");
                Intent intent = new Intent(mContext, cls);

                this.startActivity(intent);

                PictureWindowAnimationStyle windowAnimationStyle = PictureSelectionConfig.windowAnimationStyle;
                this.getActivity().overridePendingTransition(
                        windowAnimationStyle.activityEnterAnimation, R.anim.picture_anim_fade_in);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mChannelList = new ArrayList<>();//(1-关注,2-推荐,3-广场,4-视频,5-同城)
        Channel followMC = new Channel("MOMENT", "关注", "关注", 1);
        Channel suggestMC = new Channel("MOMENT", "推荐", "推荐", 2);
        Channel squareMC = new Channel("MOMENT", "广场", "广场", 3);
        Channel videoMC = new Channel("MOMENT", "视频", "视频", 4);
        Channel circleMC = new Channel("MOMENT", "圈子", "圈子", -1);
        Channel sameCityMC = new Channel("MOMENT", "同城", "同城", 5);

        mChannelList.add(followMC);
        mChannelList.add(suggestMC);
        mChannelList.add(squareMC);
        mChannelList.add(videoMC);
        mChannelList.add(circleMC);
        mChannelList.add(sameCityMC);

        initCategoryView();
    }

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getChildFragmentManager(), mChannelList);
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(1);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    //-------------上传---------------
    private List<File> fileList;
    private List<String> origFilePaths;
    private List<String> describeFile;
    private List<String> contentUrlList;
    private int updateIndex;
    private PublishContentForm publishContentForm;
    private PublishContent publishContent;

    private OssManager ossManager;
    private StsTokenModel stsTokenModel;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.PUBLISH_CONTENT_BEFORE:
                publishContent = (PublishContent) event.getSerializable();
                origFilePaths = publishContent.getFilePahtList();

                describeFile = new ArrayList<>();
                contentUrlList = new ArrayList<>();
                fileResultList = new ArrayList<>();

                if (null == origFilePaths || (null != origFilePaths && (origFilePaths.size() == 0 || (origFilePaths.size() == 1 && null == origFilePaths.get(0))))) {
                    publishContent.setContentType(YXConfig.CONTENT_TYPE_TEXT);
                    publishAction(true);
                } else {
                    int type = publishContent.getContentType();
                    setSmalUploadCover(type);
                    basePresenter.getStsToken();
                }

                break;
        }
    }

    /**
     * 显示上传小封面
     *
     * @param type
     */
    private void setSmalUploadCover(int type) {
        String cover = null;
        if (type == YXConfig.CONTENT_TYPE_VIDEO) {
            String firstPath = origFilePaths.get(0);
            if (FileUtils.isVideo(firstPath) && origFilePaths.size() > 1) {
                Collections.swap(origFilePaths, 0, 1);
            }
            cover = origFilePaths.get(0);
            String coverPath = publishContent.getVideoCoverPath();
            if (!TextUtils.isEmpty(coverPath)) {
                cover = coverPath;
            }
            ImageLoaderUtil.loadLocalCornerImg(publishCover, new File(cover), 6);
        } else if (type == YXConfig.CONTENT_TYPE_PICTURE) {
            cover = origFilePaths.get(0);
            ImageLoaderUtil.loadLocalCornerImg(publishCover, new File(cover), 6);
        }
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);

        publishAction(false);
    }

    @SuppressLint("AutoDispose")
    private void publishAction(boolean isAllText) {
        fileList = new ArrayList<>();
        updateIndex = 0;

        describeFile.clear();
        contentUrlList.clear();

        publishContentForm = new PublishContentForm();

        publishContentForm.setTopicIdList(publishContent.getTopicIdList());//选中话题id集合{}
        publishContentForm.setTopicSaveFormList(publishContent.getTopicSaveFormList());//创建话题数据集合---通过搜索新建的
        publishContentForm.setCircleIdList(publishContent.getCircleIdList());
        publishContentForm.setUserIdList(publishContent.getUserIdList());

        publishContentForm.setContentCategoryId(publishContent.getContentCategoryId());
        publishContentForm.setContent(publishContent.getContent());
        publishContentForm.setContentTitle(publishContent.getContentTitle());
        publishContentForm.setIssueAddress(publishContent.getIssueAddress());
        publishContentForm.setIssueAddressDetail(publishContent.getIssueAddressDetail());
        publishContentForm.setIssueCity(publishContent.getIssueCity());

        publishContentForm.setIssueLatitude(publishContent.getIssueLatitude());
        publishContentForm.setIssueLongitude(publishContent.getIssueLongitude());
        publishContentForm.setPrivacyType(publishContent.getPrivacyType());

        int type = publishContent.getContentType();

        publishContentForm.setContentType(type);
        publishContentForm.setVideoTime(publishContent.getVideoTime());

        if (isAllText) {
            basePresenter.publishContent(publishContentForm);
        } else {
            //1 图片 2视频
            if (type == YXConfig.CONTENT_TYPE_VIDEO) {
                publishUpload.setVisibility(View.VISIBLE);
//            uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                String firstPath = origFilePaths.get(0);
                if (FileUtils.isVideo(firstPath) && origFilePaths.size() > 1) {
                    Collections.swap(origFilePaths, 0, 1);
                }

                String coverPath = publishContent.getVideoCoverPath();
                if (!TextUtils.isEmpty(coverPath)) {
                    File file = new File(coverPath);
                    uploadVideoCover(file.getName(), file.getPath());
                } else {
                    uploadVideo();
                }
            } else if (type == YXConfig.CONTENT_TYPE_PICTURE) {
                publishUpload.setVisibility(View.VISIBLE);
                basePresenter.addSubscribe(Flowable.just(origFilePaths)
                                .observeOn(Schedulers.io())
                                .map(list -> {
                                    List<File> fileList = new ArrayList<>();
                                    for (String filePath : list) {
                                        try {
                                            File file = new File(filePath);
                                            fileList.add(file);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    return fileList;
                                })
                                .map(new Function<List<File>, List<File>>() {
                                    @Override
                                    public List<File> apply(@NonNull List<File> list) throws Exception {
                                        return Luban.with(mContext)
                                                .setTargetDir(getPath())
                                                .ignoreBy(500)
                                                .load(list)
                                                .get();
                                    }
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnError(new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) {
                                    }
                                })
//                    .onErrorResumeNext(Flowable.<List<File>>empty())
                                .subscribe(new Consumer<List<File>>() {
                                    @Override
                                    public void accept(@NonNull List<File> list) {
                                        fileList.clear();
                                        fileList.addAll(list);
                                        uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                                    }
                                })
                );
            }
        }
    }

    @SuppressLint("AutoDispose")
    private void uploadVideo() {
        basePresenter.addSubscribe(Flowable.just(origFilePaths)
                        .observeOn(Schedulers.io())
                        .map(list -> {
                            List<File> fileList = new ArrayList<>();
                            for (String filePath : list) {
                                try {
                                    File file = new File(filePath);
                                    fileList.add(file);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            return fileList;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                            }
                        })
//                .onErrorResumeNext(Flowable.<List<File>>empty())
                        .subscribe(new Consumer<List<File>>() {
                            @Override
                            public void accept(@NonNull List<File> list) {
                                fileList.clear();
                                fileList.addAll(list);
                                uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                            }
                        })
        );
    }


    @OnClick(R2.id.tv_publish_retry)
    public void OnClickRetry() {
        publishUpload.setVisibility(View.GONE);

        publishAction(false);
    }


    private void uploadFile(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        // 判断是图片还是视频
        int index = fileName.lastIndexOf(".");
//        String mediaType = fileName.substring(index + 1);
        boolean isVideo = FileUtils.isVideo(fileName);

        String objectKey = "";
        Date date = new Date();
        try {
            if (isVideo) {
                objectKey = "video/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            } else {
                objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {
            //1 图片 2 视频
            if (publishContentForm.getContentType() == 2 && updateIndex == 1) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_PROGRESS;

                //更新进度
                int progress = (int) (currentSize * 100 / totalSize);
                if (progress == 100) {
                    return;
                }

                msg.obj = progress;
                mHandler.sendMessage(msg);
            }
        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
//                String resultId = fileResult.getId();
//                String httpUrl = fileResult.getHttpUrl();
                fileResult.setHttpUrlType(isVideo ? 2 : 1);

                fileResultList.add(fileResult);
//
//                describeFile.add(resultId);
//                contentUrlList.add(httpUrl);
                updateIndex++;

                if (publishContentForm.getContentType() == 1) {
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_PROGRESS;

                    //更新进度
                    int progress = updateIndex * 100 / origFilePaths.size();
                    msg.obj = progress;
                    mHandler.sendMessage(msg);
                }

                if (updateIndex < origFilePaths.size()) {
                    uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                } else {
                    //
//                    publishContentForm.setFileIds(StringUtils.listToString(describeFile, ","));
                    Gson gson = new Gson();
                    String contentUrl = gson.toJson(fileResultList);

                    publishContentForm.setContentUrl(contentUrl);
                    basePresenter.publishContent(publishContentForm);

                    publishUpload.postDelayed(() -> publishUpload.setVisibility(View.GONE), 100);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FAILED;
                mHandler.sendMessage(msg);
            }
        });
    }

    private void uploadVideoCover(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        int index = fileName.lastIndexOf(".");

        String objectKey = "";
        Date date = new Date();
        objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);

        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {

        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                fileResult.setHttpUrlType(-1);
//                String resultId = fileResult.getId();
//                String httpUrl = fileResult.getHttpUrl();
//                String width = fileResult.getWidth();
//                String height = fileResult.getHeight();
                fileResultList.add(fileResult);
//                String coverUrl = httpUrl + "?x-oss-process=image/resize,h_" + height + ",w_" + width;
//                publishContentForm.setCoverUrl(coverUrl);

                uploadVideo();
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FAILED;
                mHandler.sendMessage(msg);
            }
        });
    }

    @Override
    public void publishContentSuccess(ContentModel model) {
        publishUpload.setVisibility(View.GONE);

        if (model != null) {
            int auditStatus = model.getAuditStatus();
            if (auditStatus == 0) {
                ToastUtils.showShort("发布成功，等待管理员审核");
            } else {
                ToastUtils.showShort("发布成功");
            }
        }
        EventBus.getDefault().post(new MessageEvent(CommonConstant.PUBLISH_CONTENT));
        PublishTemp.deletePublishSaveTemp();//发布成功清除缓存
    }

    @Override
    public void handlePublishErrorMsg(String msg) {
        ToastUtils.showShort(msg);
    }

    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        file.mkdirs();
        return path;
    }

    private List<FileResult> fileResultList;
    private MyHandler mHandler = new MyHandler();
    private final static int UPLOAD_FAILED = 1000;
    private final static int UPLOAD_PROGRESS = 1001;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FAILED:
                    publishDesc.setText("发布内容失败");
                    publishRetry.setVisibility(View.VISIBLE);
                    break;

                case UPLOAD_PROGRESS:
                    int progress = (int) msg.obj;

                    publishShowProgress.setText(progress + "%");
                    publishPB.setProgress(progress);
                    break;
            }
        }
    }

    List<CircleModel> mCircleList;
    List<TopicModel> mTopicList;
    List<MomentHotListModel> listModelList;

    //平台先定义一套
    @Override
    public void handleHotCircleList(ListResult<CircleModel> listResult) {
        List<CircleModel> circleList = listResult.getRecords();
        if (null != circleList && circleList.size() > 0) {
            mCircleList = circleList;

            if (hotTabCircle.isTabSelected()) {
                initHotCircleData();
            }
        } else {
            if (null != listModelList) {
                listModelList.clear();
                hotListAdapter.setItems(listModelList);
            }
        }
    }

    private void initHotCircleData() {
        if (null != mCircleList && mCircleList.size() > 0) {
            listModelList = new ArrayList<>();
            //这里最多只会取出7个

            for (int i = 0; i < mCircleList.size() && i < YXConfig.MAX_HOT_LIST; i++) {
                CircleModel circle = mCircleList.get(i);

                MomentHotListModel hotItem = new MomentHotListModel();
                hotItem.setId(circle.getId());
                hotItem.setTitle(circle.getTitle());
                hotItem.setHotListType(MomentHotListModel.CIRCLE);

                listModelList.add(hotItem);
            }

            //根据业务只显示9条--原型
            int size = listModelList.size();
            if (size == YXConfig.MAX_HOT_LIST) {

                listModelList.add(null);
            }

            hotListAdapter.setItems(listModelList);
        }
    }

    @Override
    public void handleHotTopicList(ListResult<TopicModel> listResult) {
        List<TopicModel> topicList = listResult.getRecords();
        if (null != topicList && topicList.size() > 0) {
            mTopicList = topicList;

            if (hotTabTopic.isTabSelected()) {
                initHotTopicData();
            }
        } else {
            if (null != listModelList) {
                listModelList.clear();
                hotListAdapter.setItems(listModelList);
            }
        }
    }

    private void initHotTopicData() {
        if (null != mTopicList && mTopicList.size() > 0) {
            listModelList = new ArrayList<>();
            //这里最多只会取出7个
            for (int i = 0; i < mTopicList.size() && i < YXConfig.MAX_HOT_LIST; i++) {
                TopicModel topic = mTopicList.get(i);

                MomentHotListModel hotItem = new MomentHotListModel();
                hotItem.setId(topic.getId());
                hotItem.setTitle(topic.getTitle());
                hotItem.setHotListType(MomentHotListModel.TOPIC);

                listModelList.add(hotItem);
            }

            //根据业务只显示7条
            int size = listModelList.size();
            if (size == YXConfig.MAX_HOT_LIST) {
                listModelList.add(null);
            }

            hotListAdapter.setItems(listModelList);
        }
    }

    public void toIndexPage(int pos) {
        sltabChannel.setCurrentTab(pos);
    }
}
