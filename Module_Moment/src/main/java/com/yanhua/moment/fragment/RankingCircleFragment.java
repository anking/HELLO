package com.yanhua.moment.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.MomentHotListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.HotCircleListAdapter;
import com.yanhua.moment.presenter.PublishContentPresenter;
import com.yanhua.moment.presenter.contract.PublishContentContract;

import java.util.List;

import butterknife.BindView;

public class RankingCircleFragment extends BaseMvpFragment<PublishContentPresenter> implements PublishContentContract.IView {

    @BindView(R2.id.rcvHotList)
    RecyclerView rcvHotList;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    ////////////////////////////////
    private HotCircleListAdapter hotListAdapter;

    List<CircleModel> mCircleList;
    List<TopicModel> mTopicList;
    List<MomentHotListModel> listModelList;

    @Override
    protected void creatPresent() {
        basePresenter = new PublishContentPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_ranking_list;
    }



    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        refreshLayout.setOnRefreshListener(rl -> {
            refreshData();

        });

        refreshLayout.autoRefresh(500);

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);

        LinearLayoutManager managerContent = new LinearLayoutManager(mContext);

        hotListAdapter = new HotCircleListAdapter(mContext);
        rcvHotList.setLayoutManager(managerContent);
        rcvHotList.setAdapter(hotListAdapter);
        rcvHotList.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        hotListAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleModel> list = hotListAdapter.getmDataList();
            CircleModel item = list.get(pos);

            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation(getContext());
        });
    }

    /**
     * 刷新数据
     */
    private void refreshData() {
        //需要更新当前tab的数据MomentCHANELFragment

        //话题热榜最多获取100
        basePresenter.getHotCircleList("", 1, 100);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //平台先定义一套
    @Override
    public void handleHotCircleList(ListResult<CircleModel> listResult) {
        List<CircleModel> circleList = listResult.getRecords();
        if (null != circleList && circleList.size() > 0) {
            mCircleList = circleList;

            hotListAdapter.setItems(mCircleList);
        }
        refreshLayout.finishRefresh(1000);
    }
}
