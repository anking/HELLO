package com.yanhua.moment.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.ChannelListAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 此刻列表数据
 */
public class MomentChanelFragment extends BaseMvpFragment<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private int type;
    private int objType;
    private String circleId;
    private String topicId;
    private String idType;

    private ChannelListAdapter mAdapter;
    private List<MomentListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_moment_channel;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getInt("type");
            objType = bundle.getInt("objType");
            circleId = bundle.getString("circleId");
            topicId = bundle.getString("topicId");
            idType = bundle.getString("idType");
        }

        initRecyclerView();

        EventBus.getDefault().register(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            if (!TextUtils.isEmpty(circleId)) {
                //请求圈子
                basePresenter.getCirclePageList("", circleId, objType, current, size);
            } else if (!TextUtils.isEmpty(topicId)) {
                //话题
                basePresenter.getTopicPageList("", topicId, objType, current, size);
            } else {
                if (type == 5) {//同城
                    String lng = DiscoCacheUtils.getInstance().getLongitude();

                    String lat = DiscoCacheUtils.getInstance().getLatitude();

                    basePresenter.getMomentPageCityList("", type, current, size, "", lat, lng);
                } else {
                    basePresenter.getMomentPageList("", type, current, size);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        //"total":39
        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ChannelListAdapter(mContext, this, (type, item, pos, adPos) -> {
            currentPosition = pos;

            String id = item.getId();

            //resultType;//结果类型(1:内容,2:广告)
            int resultType = item.getResultType();

            int contentType = item.getContentType();
            int attrType = 1;//根据tab或者这一项的属性类型归属进行赋值 ,attrType
            switch (type) {
                case ChannelListAdapter.OnHandleClickListener.FOLLOW://关注
                    PageJumpUtil.firstIsLoginThenJump(() -> {
                        //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
                        //    private Integer followStatus;
                        int followStatus = item.getFollowStatus();

                        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
                        String userId = item.getUserId();

                        if (isFollow) {
                            AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                            new XPopup.Builder(mContext).asCustom(alertPopup).show();
                            alertPopup.setOnConfirmListener(() -> {
                                alertPopup.dismiss();
                                basePresenter.cancelFollowUser(userId);
                            });
                            alertPopup.setOnCancelListener(alertPopup::dismiss);
                        } else {
                            basePresenter.followUser(userId);
                        }
                    });
                    break;
                case ChannelListAdapter.OnHandleClickListener.CLICK_AD://广告
                    List<AdvertiseModel> advertiseList = item.getAdvertisingList();

                    AdvertiseModel clickItem = advertiseList.get(adPos);

                    HashMap<String, Object> params = new HashMap<>();
                    params.put("userId", UserManager.getInstance().getUserId());
                    params.put("id", clickItem.getId());

                    basePresenter.addMarketingClick(params);

                    PageJumpUtil.adJumpContent(clickItem, mActivity, -1);
                    break;
                case ChannelListAdapter.OnHandleClickListener.COLLECT://收藏
                    PageJumpUtil.firstIsLoginThenJump(() -> {
                        basePresenter.updateCollectContent(id, attrType);
                    });
                    break;
                case ChannelListAdapter.OnHandleClickListener.COMMENT://评论
                case ChannelListAdapter.OnHandleClickListener.TO_DETAIL://详情
                    if (resultType == 2) {
                        return;
                    }
                    //跳转到详情
                    if (contentType == 2) {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentModel", item)
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withString("fromClassName", className)
                                .withInt("module", 0)
                                .navigation();
                    } else {
                        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                .withString("id", id)
                                .withString("fromClassName", className)
                                .navigation();
                    }

                    break;
                case ChannelListAdapter.OnHandleClickListener.THUMB_UP://点赞
                    PageJumpUtil.firstIsLoginThenJump(() -> {
                        basePresenter.updateStarContent(id, attrType);
                    });
                    break;
            }
        });
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
//
//
//        //避免只有一条也提示
//        if (mContentList.size() != 1 && mContentList.size() == (mCurrentPosition + 1)) {
//            if (NetworkUtils.isConnected()) {
//                current = current + 1;
//                getDataContentList(current);
//            } else {
//                ToastUtils.showShort("网络异常");
//            }
//        }
//        影响性能
//        rvContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                LogUtils.d("------->isSlideToBottom:" + isSlideToBottom(recyclerView));
//                if (isSlideToBottom(recyclerView)) {
//                    getListData(current++);
//                }
//            }
//        });
    }

    protected boolean isSlideToBottom(RecyclerView recyclerView) {
        if (recyclerView == null) return false;
        if (recyclerView.computeVerticalScrollExtent() + recyclerView.computeVerticalScrollOffset() >= recyclerView.computeVerticalScrollRange())
            return true;
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.MOMENT_HOME_LIST_REFRESH_TOPIC:
            case CommonConstant.MOMENT_HOME_LIST_REFRESH_CIRCLE:
            case CommonConstant.MOMENT_HOME_LIST_REFRESH:
                String value = event.getValue();

                //下拉只更新当前选中tab的数据
                if (type == Integer.valueOf(value)) {
                    getListData(1);
                }
                break;

            case CommonConstant.ACTION_FOLLOW:
                String userIdFollow = event.getValue();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (!TextUtils.isEmpty(userIdFollow) && userIdFollow.equals(c.getUserId())) {
                        c.setFollowStatus(DiscoValueFormat.followAction(c.getFollowStatus()));
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_UNFOLLOW:
                String userIdUnFollow = event.getValue();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (!TextUtils.isEmpty(userIdUnFollow) && userIdUnFollow.equals(c.getUserId())) {
                        c.setFollowStatus(DiscoValueFormat.unFollowAction(c.getFollowStatus()));
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.PUBLISH_CONTENT:
                current = 1;
                getListData(1);
                break;
            case CommonConstant.ACTION_COLLECT:
                MomentListModel momentCollect = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentCollect.getId().equals(c.getId())) {
                        c.setCollect(momentCollect.isCollect());
                        c.setCollectCount(momentCollect.getCollectCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_LOVE:
                MomentListModel momentLove = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentLove.getId().equals(c.getId())) {
                        c.setFabulous(momentLove.isFabulous());
                        c.setFabulousCount(momentLove.getFabulousCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;

            case CommonConstant.ACTION_DELETE_CONTENT:
                String valueType = event.getValue();
                if (className.equals(valueType)) {
                    mAdapter.notifyItemRemoved(currentPosition);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore(100, true, false);
        }
        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowUserSuccess() {
        //此处需要更新列表中该人的关注状态
        MomentListModel model = mListData.get(currentPosition);

        //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
        //    private Integer followStatus;
        int followStatus = model.getFollowStatus();

        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
        int changeResult = isFollow ? (followStatus - 1) : (followStatus + 1);

        for (MomentListModel moment : mListData) {
            String userId = moment.getUserId();//广告排除
            if (null != userId && userId.equals(model.getUserId())) {
                moment.setFollowStatus(changeResult);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateCollectContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
