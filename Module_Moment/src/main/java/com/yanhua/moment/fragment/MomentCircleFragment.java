package com.yanhua.moment.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.MomentHotListDecoration;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.CircleListJoinAdapter;
import com.yanhua.moment.adapter.CircleSuggestListAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 此刻圈子数据
 */
public class MomentCircleFragment extends BaseMvpFragment<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.tvApplyCircle)
    TextView tvApplyCircle;
    @BindView(R2.id.rcvJoinList)
    DataObserverRecyclerView rcvJoinList;
    @BindView(R2.id.llLookJoinMore)
    LinearLayout llLookJoinMore;

    @BindView(R2.id.llMyJoinedCircle)
    LinearLayout llMyJoinedCircle;

    @BindView(R2.id.llLookSuggestMore)
    LinearLayout llLookSuggestMore;
    @BindView(R2.id.rcvSuggestList)
    DataObserverRecyclerView rcvSuggestList;

    @BindView(R2.id.emptyJoinView)
    EmptyLayout emptyJoinView;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;
    private String idType;

    private CircleSuggestListAdapter mAdapter;
    private CircleListJoinAdapter mJoinAdapter;
    List<CircleModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_moment_circle;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initRecyclerView();

        EventBus.getDefault().register(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    @OnClick({R2.id.tvApplyCircle, R2.id.llLookSuggestMore, R2.id.llLookJoinMore})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.tvApplyCircle) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                //跳转到申请
                ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY).navigation(getContext());
            });
        } else if (id == R.id.llLookSuggestMore) {
            ARouter.getInstance().build(ARouterPath.CIRCLE_SUGGEST_LIST_ACTIVITY).withBoolean("joined", false).navigation(getContext());
        } else if (id == R.id.llLookJoinMore) {
            ARouter.getInstance().build(ARouterPath.CIRCLE_SUGGEST_LIST_ACTIVITY).withBoolean("joined", true).navigation(getContext());
        }
    }

    public void getListData(int page) {
        if (UserManager.getInstance().isLogin()) {
            llMyJoinedCircle.setVisibility(View.VISIBLE);
        } else {
            llMyJoinedCircle.setVisibility(View.GONE);
        }

        if (null != basePresenter) {
            current = page;
            if (current == 1) {
                if (UserManager.getInstance().isLogin()) {
                    //我的加入第一页的时候可以请求
                    basePresenter.getCircleJoinedList("", current, size);
                }
            }
            basePresenter.getCircleList("", current, size);
        }
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        GridLayoutManager managerContent = new GridLayoutManager(mContext, 2);
        rcvJoinList.addItemDecoration(new MomentHotListDecoration(mContext, 15));

        mJoinAdapter = new CircleListJoinAdapter(mContext);
        rcvJoinList.setLayoutManager(managerContent);
        rcvJoinList.setAdapter(mJoinAdapter);
        rcvJoinList.setEmptyView(emptyJoinView);
        mJoinAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleModel> list = mJoinAdapter.getmDataList();
            CircleModel item = list.get(pos);

            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation(getContext());
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new CircleSuggestListAdapter(mContext, true);
        rcvSuggestList.setLayoutManager(manager);
        rcvSuggestList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleModel> list = mAdapter.getmDataList();
            CircleModel item = list.get(pos);

            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation(getContext());
        });


        //干掉 取出上拉下拉阴影
        rcvJoinList.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rcvSuggestList.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rcvJoinList);
        RecycleViewUtils.clearRecycleAnimation(rcvSuggestList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.MOMENT_HOME_LIST_REFRESH:
                //下拉只更新当前选中tab的数据
                getListData(1);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void handleCircleList(ListResult<CircleModel> listResult) {
        List<CircleModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
            if (total < size / 2) {
                llLookSuggestMore.setVisibility(View.GONE);
            } else {
                llLookSuggestMore.setVisibility(View.VISIBLE);
            }
            refreshLayout.resetNoMoreData();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @Override
    public void handleCircleJoinedList(ListResult<CircleModel> listResult) {
        List<CircleModel> list = listResult.getRecords();
        if (list.size() > 6) {
            mJoinAdapter.setItems(list.subList(0, 6));
            llLookJoinMore.setVisibility(View.VISIBLE);
        } else {
            mJoinAdapter.setItems(list);
            llLookJoinMore.setVisibility(View.GONE);
        }
    }
}
