package com.yanhua.moment.activity;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ImageUtils;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;
import butterknife.OnClick;
import jp.wasabeef.richeditor.RichEditor;

@Route(path = ARouterPath.LONG_CONTENT_HTML_ACTIVITY)
public class LongContentHtmlActivity extends BaseMvpActivity {

    private RichEditor mEditor;
    private WebView mPreview;

    @BindView(R2.id.action_undo)
    ImageButton actionUndo;
    @BindView(R2.id.action_redo)
    ImageButton actionRedo;
    @BindView(R2.id.action_bold)
    ImageButton actionBold;
    @BindView(R2.id.action_italic)
    ImageButton actionItalic;
    @BindView(R2.id.action_align_left)
    ImageButton actionAlignLeft;
    @BindView(R2.id.action_align_center)
    ImageButton actionAlignCenter;
    @BindView(R2.id.action_align_right)
    ImageButton actionAlignRight;
    @BindView(R2.id.action_insert_image)
    ImageButton actionInsertImage;
    @BindView(R2.id.action_insert_link)
    ImageButton actionInsertLink;

    // 图标颜色状态
    private final static ColorStateList sColorStateList;

    static {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_selected}, // 选中
                new int[]{-android.R.attr.state_selected} // 未选中
        };

        int[] colors = new int[]{R.color.theme, R.color.mainWord};
        sColorStateList = new ColorStateList(states, colors);
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_long_content_html;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(400);
        mEditor.setEditorFontSize(16);
        mEditor.setEditorFontColor(R.color.mainContent);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("说点好玩的...");

        ImageUtils.setTintList(actionUndo.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionRedo.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionBold.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionItalic.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionAlignLeft.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionAlignCenter.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionAlignRight.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionInsertImage.getDrawable(), sColorStateList);
        ImageUtils.setTintList(actionInsertLink.getDrawable(), sColorStateList);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //跳转数据
//        mPreview = (WebView) findViewById(R.id.preview);
//        WebSettings settings = mPreview.getSettings();
//        settings.setJavaScriptEnabled(true);
//        mPreview.setWebChromeClient(new WebChromeClient());
//
//        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
//            @Override
//            public void onTextChange(String text) {
//                String html = mEditor.getHtml();
//
//                mPreview.loadData(html, "text/html", "UTF-8");
//            }
//        });
    }

    @OnClick({
            R2.id.action_undo,
            R2.id.action_insert_checkbox,
            R2.id.action_superscript,
            R2.id.action_strikethrough,
            R2.id.action_underline,
            R2.id.action_heading1,
            R2.id.action_heading2,
            R2.id.action_heading3,
            R2.id.action_heading4,
            R2.id.action_heading5,
            R2.id.action_heading6,
            R2.id.action_subscript,
            R2.id.action_italic,
            R2.id.action_bold,
            R2.id.action_redo,
            R2.id.action_txt_color,
            R2.id.action_bg_color,
            R2.id.action_indent,
            R2.id.action_outdent,
            R2.id.action_align_left,
            R2.id.action_align_center,
            R2.id.action_align_right,
            R2.id.action_blockquote,
            R2.id.action_insert_bullets,
            R2.id.action_insert_numbers,
            R2.id.action_insert_image,
            R2.id.action_insert_youtube,
            R2.id.action_insert_audio,
            R2.id.action_insert_video,
            R2.id.action_insert_link

            //Html编辑器上方
    })
    public void onClickView(View view) {
        int id = view.getId();

        if (id == R.id.action_insert_checkbox) {
            mEditor.insertTodo();
        } else if (id == R.id.action_superscript) {
            mEditor.setSuperscript();
        } else if (id == R.id.action_strikethrough) {
            mEditor.setStrikeThrough();
        } else if (id == R.id.action_underline) {
            mEditor.setUnderline();
        } else if (id == R.id.action_heading1) {
            mEditor.setHeading(1);
        } else if (id == R.id.action_heading2) {
            mEditor.setHeading(2);
        } else if (id == R.id.action_heading3) {
            mEditor.setHeading(3);
        } else if (id == R.id.action_heading4) {
            mEditor.setHeading(4);
        } else if (id == R.id.action_heading5) {
            mEditor.setHeading(5);
        } else if (id == R.id.action_heading6) {
            mEditor.setHeading(6);
        } else if (id == R.id.action_subscript) {
            mEditor.setSubscript();
        } else if (id == R.id.action_txt_color) {
            mEditor.setTextColor(false ? Color.BLACK : Color.RED);
        } else if (id == R.id.action_bg_color) {
            mEditor.setTextBackgroundColor(false ? Color.BLACK : Color.RED);
        } else if (id == R.id.action_indent) {
            mEditor.setIndent();
        } else if (id == R.id.action_outdent) {
            mEditor.setOutdent();

        } else if (id == R.id.action_blockquote) {
            mEditor.setBlockquote();
        } else if (id == R.id.action_insert_bullets) {
            mEditor.setBullets();
        } else if (id == R.id.action_insert_numbers) {
            mEditor.setNumbers();

        } else if (id == R.id.action_italic) {
            mEditor.setItalic();
            actionItalic.setSelected(!actionItalic.isSelected());
        } else if (id == R.id.action_bold) {
            mEditor.setBold();
            actionBold.setSelected(!actionBold.isSelected());
        } else if (id == R.id.action_redo) {
            mEditor.redo();
            actionRedo.setSelected(!actionRedo.isSelected());
        } else if (id == R.id.action_undo) {
            mEditor.undo();
            actionUndo.setSelected(!actionUndo.isSelected());
        } else if (id == R.id.action_align_left) {
            mEditor.setAlignLeft();

            actionAlignLeft.setSelected(!actionAlignLeft.isSelected());
        } else if (id == R.id.action_align_center) {
            mEditor.setAlignCenter();

            actionAlignCenter.setSelected(!actionAlignCenter.isSelected());
        } else if (id == R.id.action_align_right) {
            mEditor.setAlignRight();

            actionAlignRight.setSelected(!actionAlignRight.isSelected());
        } else if (id == R.id.action_insert_image) {
            mEditor.insertImage("https://yinquan-public.oss-cn-guangzhou.aliyuncs.com/image/2022/03/1647338106769.jpeg",
                    "dachshund", 320);
        } else if (id == R.id.action_insert_youtube) {
            mEditor.insertYoutubeVideo("https://www.youtube.com/embed/pS5peqApgUA");

        } else if (id == R.id.action_insert_audio) {
            mEditor.insertAudio("https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3");

        } else if (id == R.id.action_insert_video) {
            mEditor.insertVideo("https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/1080/Big_Buck_Bunny_1080_10s_10MB.mp4", 360);
        } else if (id == R.id.action_insert_link) {
            mEditor.insertLink("https://github.com/wasabeef", "wasabeef");
        }
    }

    @Override
    protected void creatPresent() {

    }
}
