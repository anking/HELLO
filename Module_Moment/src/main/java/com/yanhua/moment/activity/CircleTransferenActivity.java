package com.yanhua.moment.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.CircleTransferAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 圈主转让
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_TRANSFEREN_ACTIVITY)
public class CircleTransferenActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    @BindView(R2.id.etInput)
    EditText etInput;

    private CircleTransferAdapter mAdapter;

    @Autowired
    String circleId;
    @Autowired
    CircleModel currentCircle;

    private String keyWord;
    private int total;
    private List<MomentCircleMemberModel> mListData;

    private int selectPos = -1;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_transfer;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("转让圈主");
        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        etInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                keyWord = s.toString().trim();

                if (!TextUtils.isEmpty(keyWord)) {
                } else {
                    current = 1;
                }
                getListData(current);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mAdapter = new CircleTransferAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            if (selectPos == pos) {
                return;
            }

            selectPos = pos;

            List<MomentCircleMemberModel> dataList = mAdapter.getmDataList();

            for (int i = 0; i < dataList.size(); i++) {
                MomentCircleMemberModel circleMember = dataList.get(i);
                if (i == pos) {
                    circleMember.setSelect(true);
                } else {
                    circleMember.setSelect(false);
                }
            }

            mAdapter.notifyDataSetChanged();
        });

        getListData(1);
    }


    private void getListData(int page) {
        current = page;
        basePresenter.getCircleMemberList(keyWord, circleId, current, size);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }


    @Override
    public void handleCircleMemberList(ListResult<MomentCircleMemberModel> listResult) {
        List<MomentCircleMemberModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @OnClick({R2.id.tvTransferSystem, R2.id.tvTransferMember})
    public void onClickView(View view) {
        int viewId = view.getId();
        if (viewId == R.id.tvTransferSystem) {
            //2 直接退出，托管系统
            CommonDialog commonDialog = new CommonDialog(this, "是否确定直接退出圈子？", "确定", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();

                basePresenter.joinAction(circleId, 2);
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());

        } else if (viewId == R.id.tvTransferMember) {
            //转让给别人 确定转让给“ACE-YOUNGKING”为圈主？
            if (selectPos != -1) {
                String masterID = currentCircle.getCircleMaster();
                MomentCircleMemberModel circleMemberModel = mAdapter.getmDataList().get(selectPos);
                String memberId = circleMemberModel.getUserId();
                if (null != masterID && masterID.equals(memberId)) {
                    ToastUtils.showShort("请选择非圈主成员");
                    return;
                }

                CommonDialog commonDialog = new CommonDialog(this, "确定转让给“" + circleMemberModel.getNickName() + "”为圈主？", "确定", "取消");
                new XPopup.Builder(this).asCustom(commonDialog).show();
                commonDialog.setOnConfirmListener(() -> {
                    commonDialog.dismiss();

                    basePresenter.masterTransferCircle(circleId, memberId);
                });
                commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
            }
        }
    }

    @Override
    public void handTransferCircleMaster() {
        //1 加入 2 退出
        ToastUtils.showShort("转让圈子成功");

        this.finish();
    }

    @Override
    public void handActionSuccess(int type) {
        //1 加入 2 退出
        ToastUtils.showShort("退出圈子成功");

        this.finish();
    }
}
