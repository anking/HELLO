package com.yanhua.moment.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.fragment.RankingCircleFragment;
import com.yanhua.moment.fragment.RankingTopicFragment;

import butterknife.BindView;

/**
 * 选择地点页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.RANKING_LIST_ACTIVITY)
public class RankingListActivity extends BaseMvpActivity {
    @BindView(R2.id.tlSegment)
    SegmentTabLayout tlSegment;
    private String[] mTitles = {"热门话题", "热门圈子"};
    @Autowired
    int rankingTopic;//1:热门话题，2：热门圈子

    private boolean showRT;//当前展示话题


    @Override
    public int bindLayout() {
        return R.layout.activity_ranking_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        tlSegment.setTabData(mTitles);
        tlSegment.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                tlSegment.postDelayed(() -> switchAction(position == 0), 200);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        switchAction(rankingTopic == 1);
        tlSegment.setCurrentTab(rankingTopic == 1 ? 0 : 1);
    }


    @Override
    protected void creatPresent() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * 处理切换
     */
    private void switchAction(boolean isRT) {
        showRT = isRT;

        if (showRT) {
            RankingTopicFragment fragment = new RankingTopicFragment();

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.fl_container, fragment);
            transaction.commit();

        } else {
            RankingCircleFragment fragment = new RankingCircleFragment();

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.fl_container, fragment);
            transaction.commit();

        }
    }

}
