package com.yanhua.moment.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chinalwb.are.AREditText;
import com.chinalwb.are.strategies.ImageStrategy;
import com.chinalwb.are.styles.toolbar.ARE_ToolbarDefault;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentCenter;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentLeft;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentRight;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_BackgroundColor;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Bold;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontColor;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontSize;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Hr;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Image;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Italic;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Link;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_ListBullet;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Quote;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Strikethrough;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Underline;
import com.chinalwb.are.styles.toolitems.IARE_ToolItem;
import com.chinalwb.are.styles.toolitems.styles.ARE_Style_Image;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.PublishContentForm;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoImageStrategy;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.widget.PickTopicBottomPop;
import com.yanhua.common.widget.PrivacyPopup;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;
import com.yanhua.moment.view.PickCircleBottomPop;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.LONG_CONTENT_ACTIVITY)
public class LongContentActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {
    private static final int SELECT_ADDRESS = 0x006;
    private static final int CHOOSE_CONTENT_IMAGE = 0x001;
    private static final int CHOOSE_CONTENT_VIDEO = 0x002;
    private static final int CHOOSE_COVER_IMAGE = 0x003;
    private static final int CHOOSE_COVER_VIDEO = 0x004;
    private static final int CHOOSE_TYPE_IMAGE = PictureConfig.TYPE_IMAGE;
    private static final int CHOOSE_TYPE_VIDEO = PictureConfig.TYPE_VIDEO;
    @BindView(R2.id.areToolbar)
    ARE_ToolbarDefault mToolbar;
    @BindView(R2.id.arEditText)
    AREditText mEditText;

    @BindView(R2.id.etTitle)
    EditText etTitle;

    @BindView(R2.id.etGuideTitle)
    EditText etGuideTitle;

    @BindView(R2.id.upload)
    FrameLayout flUpload;//根据封面比例  351：210 5：3

    @BindView(R2.id.ivUpload)
    ImageView ivUpload;//根据封面比例  351：210 5：3
    ARE_ToolItem_Image image;
    @BindView(R2.id.iv_privacy)
    AliIconFontTextView ivPivacy;
    @BindView(R2.id.tv_privacy)
    TextView tvPrivacy;
    @BindView(R2.id.tv_address)
    TextView tvAddress;
    @Autowired
    String content;
    @BindView(R2.id.tflHistory)
    TagFlowLayout tflHistory;
    @BindView(R2.id.tflCircle)
    TagFlowLayout tflCircle;

    @BindView(R2.id.btnPublish)
    TextView btnPublish;

    private int privacyType = 0;

    private StsTokenModel stsTokenModel;
    private List<CircleModel> pickedList;

    private DiscoImageStrategy imageStrategy = new DiscoImageStrategy();

    @Override
    public int bindLayout() {
        return R.layout.activity_long_content;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        //5：3
        int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
        int height = width * 3 / 5;

        ViewGroup.LayoutParams imageLayoutParams = flUpload.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = height;
        flUpload.setLayoutParams(imageLayoutParams);

        initEditToolbar();
    }

    private void initEditToolbar() {
        IARE_ToolItem bold = new ARE_ToolItem_Bold();
        IARE_ToolItem italic = new ARE_ToolItem_Italic();
        IARE_ToolItem underline = new ARE_ToolItem_Underline();
        IARE_ToolItem strikethrough = new ARE_ToolItem_Strikethrough();
        IARE_ToolItem fontSize = new ARE_ToolItem_FontSize();
        IARE_ToolItem fontColor = new ARE_ToolItem_FontColor();
        IARE_ToolItem backgroundColor = new ARE_ToolItem_BackgroundColor();
        IARE_ToolItem quote = new ARE_ToolItem_Quote();
//        IARE_ToolItem listNumber = new ARE_ToolItem_ListNumber();
        IARE_ToolItem listBullet = new ARE_ToolItem_ListBullet();
        IARE_ToolItem hr = new ARE_ToolItem_Hr();
        IARE_ToolItem link = new ARE_ToolItem_Link();
//        IARE_ToolItem subscript = new ARE_ToolItem_Subscript();
//        IARE_ToolItem superscript = new ARE_ToolItem_Superscript();
        IARE_ToolItem left = new ARE_ToolItem_AlignmentLeft();
        IARE_ToolItem center = new ARE_ToolItem_AlignmentCenter();
        IARE_ToolItem right = new ARE_ToolItem_AlignmentRight();
        image = new ARE_ToolItem_Image(() -> {
            //
            checkStoragePermission(false, CHOOSE_CONTENT_IMAGE);
        });
//        IARE_ToolItem video = new ARE_ToolItem_Video();
//        IARE_ToolItem at = new ARE_ToolItem_At();

        mToolbar.addToolbarItem(image);
        mToolbar.addToolbarItem(bold);
        mToolbar.addToolbarItem(italic);
        mToolbar.addToolbarItem(underline);
        mToolbar.addToolbarItem(strikethrough);
        mToolbar.addToolbarItem(fontSize);
        mToolbar.addToolbarItem(fontColor);
        mToolbar.addToolbarItem(backgroundColor);
        mToolbar.addToolbarItem(quote);
//        mToolbar.addToolbarItem(listNumber);
        mToolbar.addToolbarItem(listBullet);
        mToolbar.addToolbarItem(hr);
        mToolbar.addToolbarItem(link);
//        mToolbar.addToolbarItem(subscript);
//        mToolbar.addToolbarItem(superscript);
        mToolbar.addToolbarItem(left);
        mToolbar.addToolbarItem(center);
        mToolbar.addToolbarItem(right);
//        mToolbar.addToolbarItem(video);
//        mToolbar.addToolbarItem(at);

        mEditText = this.findViewById(R.id.arEditText);
        mEditText.setToolbar(mToolbar);
        mEditText.setImageStrategy(imageStrategy);
    }

    private void checkStoragePermission(boolean iscrop, int requestCode) {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImage(iscrop, requestCode);
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }


    private void chooseImage(boolean iscrop, int requestCode) {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(5, 3)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(iscrop)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(requestCode);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        mEditText.setText(YHStringUtils.value(content));

        initTopicCircleView();


        basePresenter.getStsToken();
    }

    private void initTopicCircleView() {
        pickedList = new ArrayList<>();

        TagAdapter tagAdapter = new TagAdapter<CircleModel>(pickedList) {
            @Override
            public View getView(FlowLayout parent, int position, CircleModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_circle, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                AliIconFontTextView iconDelete = view.findViewById(R.id.iconDelete);

                tvTagName.setText(tagItem.getTitle());
                iconDelete.setOnClickListener(v -> {
                    pickedList.remove(position);
                    tflCircle.getAdapter().refreshData(pickedList);
                });
                return view;
            }
        };

        //控制显示5个
        tflCircle.setAdapter(tagAdapter);

        //显示选择的话题
        pickedTopics = new ArrayList<>();
        tagAdapterHistory = new TagAdapter<TopicModel>(pickedTopics) {
            @Override
            public View getView(FlowLayout parent, int position, TopicModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_topic_publish, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                tvTagName.setText("#" + tagItem.getTitle() + "#");

                AliIconFontTextView iconDelete = view.findViewById(R.id.iconDelete);
                iconDelete.setOnClickListener(v -> {
                    pickedTopics.remove(position);
                    tflHistory.getAdapter().refreshData(pickedTopics);
                });
                return view;
            }
        };

        //控制显示5个
        tflHistory.setAdapter(tagAdapterHistory);
    }

    private String mCityName;
    private String issueAddressDetail;
    private double mLatitude;
    private double mLongitude;
    private String mPoiName;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mToolbar.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_CONTENT_IMAGE && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();

            String path;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }

            ARE_Style_Image imageStyle = (ARE_Style_Image) image.getStyle();
            Uri uri = Uri.parse(path);
            ImageStrategy imageStrategy = image.getEditText().getImageStrategy();
            if (imageStrategy != null) {
                imageStrategy.uploadAndInsertImage(uri, imageStyle);
                return;
            }
        } else if (requestCode == CHOOSE_COVER_IMAGE && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();

            String path;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }

            uploadFile(new File(path));
        } else if (requestCode == SELECT_ADDRESS && resultCode == RESULT_OK) {
            if (data != null) {
                PoiItem poiItem = data.getParcelableExtra("poiItem");
                if (poiItem != null) {
                    mPoiName = poiItem.getTitle();
                    issueAddressDetail = poiItem.getSnippet();//详细地址
                    mCityName = poiItem.getCityName();
                    LatLonPoint latLonPoint = poiItem.getLatLonPoint();
                    mLatitude = latLonPoint.getLatitude();
                    mLongitude = latLonPoint.getLongitude();
                }

                String address = !TextUtils.isEmpty(mPoiName) ? mPoiName : "你在哪里";
                tvAddress.setText(address);
            } else {
                mPoiName = null;
                issueAddressDetail = null;
                mCityName = null;
                mLatitude = 0;
                mLongitude = 0;
            }
        }
    }

    private List<FileResult> fileResultList;

    private void uploadFile(File file) {
        fileResultList = new ArrayList<>();
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);

        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                //上传完毕后提交内容
                fileResultList.add(fileResult);

                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    private MyHandler mHandler = new MyHandler();

    private final static int UPLOAD_FILE_FAIL = 1000;
    private final static int UPLOAD_FILE_SUCCESS = 1001;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    String httpUrl = fileResultList.get(0).getHttpUrl();
                    ImageLoaderUtil.loadImgFillCenter(mContext, ivUpload, httpUrl, 0);
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }


    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }


    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    @OnClick({R2.id.btnPublish, R2.id.ll_location, R2.id.llCircle, R2.id.llTopic,
            R2.id.ivUpload, R2.id.ll_privacy})
    public void onClickView(View view) {
        int id = view.getId();
//        if (id == R.id.btnPreview) {
//            String title = etTitle.getText().toString().trim();
//            String guideText = etGuideTitle.getText().toString().trim();
//
//            String html = this.mEditText.getHtml();
//            ARouter.getInstance().build(ARouterPath.PREVIEW_CONTENT_ACTIVITY)
//                    .withString("title", title)
//                    .withString("guideText", guideText)
//                    .withString("html_text", html)
//                    .navigation();
        //发布
//        }
        if (view.getId() == R.id.btnPublish) {
            if (FastClickUtil.isFastClick(2000)) {
                try {
                    toPublish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (view.getId() == R.id.ll_location) {
            checkLoacationStatus();
        } else if (view.getId() == R.id.llCircle) {
            //将已经选取的丢进去
            PickCircleBottomPop popup = new PickCircleBottomPop(mContext, pickedList, new PickCircleBottomPop.OnSelectResultListener() {
                @Override
                public void selectResult(List<CircleModel> list) {
                    if (null != list && list.size() > 0) {
                        pickedList.clear();
                        pickedList.addAll(list);
                        tflCircle.getAdapter().refreshData(pickedList);
                    }
                }
            });
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(false).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(popup).show();
        } else if (view.getId() == R.id.llTopic) {
            pickTopic(false);
        } else if (id == R.id.ivUpload) {
            checkStoragePermission(true, CHOOSE_COVER_IMAGE);
        } else if (id == R.id.ll_privacy) {
            PrivacyPopup privacyPopup = new PrivacyPopup(this, privacyType);
            privacyPopup.setOnItemSelectListener(pType -> {
                privacyType = pType;//privacyType 默认公开(0公开 1仅主页可见 2私密) --- 公开(所有人可见) 主页可见(仅个人主页可见) 私密(仅自己可见)
                if (pType == YXConfig.pivacy.privacy_public) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_public);
                    tvPrivacy.setText("公开");
                } else if (pType == YXConfig.pivacy.privacy_home) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_friend);
                    tvPrivacy.setText("主页");
                } else if (pType == YXConfig.pivacy.privacy_person) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_private);
                    tvPrivacy.setText("私密");
                }
            });
            new XPopup.Builder(this).asCustom(privacyPopup).show();
        }
    }

    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {// 开了定位服务
            ARouter.getInstance().build(ARouterPath.ADDRESS_SEARCH_ACTIVITY).navigation(mActivity, SELECT_ADDRESS);
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }


    private List<TopicModel> pickedTopics;
    private TagAdapter tagAdapterHistory;

    private void pickTopic(boolean isEnter) {
        PickTopicBottomPop popup = new PickTopicBottomPop(mContext, topic -> {
            if (null != topic) {
                pickedTopics.add(topic);

                tagAdapterHistory.refreshData(pickedTopics);

            }
        });
        new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(false).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(popup).show();
    }

    private void toPublish() {
        hideSoftKeyboard();

        String title = etTitle.getText().toString().trim();

        String html = this.mEditText.getHtml();

        if (TextUtils.isEmpty(title)) {
            ToastUtils.showShort("请输入标题");
            return;
        }

        if (TextUtils.isEmpty(html)) {
            ToastUtils.showShort("请输入正文");
            return;
        }

        publishContent(title, html);
    }

    private void publishContent(String title, String content) {
        String guideText = etGuideTitle.getText().toString().trim();

        ArrayList<String> circles = new ArrayList<>();

        ArrayList<String> topics = new ArrayList<>();
        ArrayList<String> newTopics = new ArrayList<>();

        if (null != pickedList && pickedList.size() > 0) {
            for (int i = 0; i < pickedList.size(); i++) {
                circles.add(pickedList.get(i).getId());
            }
        }

        if (null != pickedTopics && pickedTopics.size() > 0) {
            for (int i = 0; i < pickedTopics.size(); i++) {
                TopicModel item = pickedTopics.get(i);

                if (!"-1".equals(item.getId())) {
                    topics.add(item.getId());
                } else {
                    newTopics.add(item.getTitle());
                }
            }
        }

        try {
            PublishContentForm publishContentForm = new PublishContentForm();

            publishContentForm.setTopicIdList(topics);
            publishContentForm.setTopicSaveFormList(newTopics);
            publishContentForm.setCircleIdList(circles);
            publishContentForm.setUserIdList(null);
            publishContentForm.setContentCategoryId(null);
            publishContentForm.setContent(content);
            publishContentForm.setContentTitle(title);
            publishContentForm.setIssueAddress(mPoiName);
            publishContentForm.setIssueAddressDetail(issueAddressDetail);
            publishContentForm.setIssueCity(mCityName);
            publishContentForm.setIssueLatitude(String.valueOf(mLatitude));
            publishContentForm.setIssueLongitude(String.valueOf(mLongitude));
            publishContentForm.setContentType(YXConfig.CONTENT_TYPE_LONG_TEXT);
            publishContentForm.setPrivacyType(privacyType);
            publishContentForm.setIntroduce(guideText);

            if (null != fileResultList && fileResultList.size() > 0) {
                Gson gson = new Gson();
                String contentUrl = gson.toJson(fileResultList);

                publishContentForm.setContentUrl(contentUrl);
            }
            basePresenter.publishContent(publishContentForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        //
        etTitle.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        mEditText.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String title = etTitle.getText().toString().trim();
        String html = this.mEditText.getHtml();

        btnPublish.setEnabled(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(html));
    }

    @Override
    public void publishContentSuccess(ContentModel model) {
        if (model != null) {
            int auditStatus = model.getAuditStatus();
            if (auditStatus == 0) {
                ToastUtils.showShort("发布成功，等待管理员审核");
            } else {
                ToastUtils.showShort("发布成功");
            }
        }

        EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
        EventBus.getDefault().post(new MessageEvent(CommonConstant.PUBLISH_CONTENT));

        onBackPressed();
    }
}
