package com.yanhua.moment.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.CircleApplyHistoryAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_APPLY_HISTORY_ACTIVITY)
public class CircleApplyHistoryActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    private CircleApplyHistoryAdapter mAdapter;

    private int total;
    private List<CircleModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_apply_history;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("申请记录");
        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        mAdapter = new CircleApplyHistoryAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnItemClickListener((itemView, pos) -> {
//            CircleModel circleModel = mAdapter.getmDataList().get(pos);
//
//            String id = circleModel.getCircleId();
//            if (!TextUtils.isEmpty(id)) {
//                //跳到圈子详情
//                ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY).withString("id", id).navigation();
//            }

            CircleModel circleModel = mAdapter.getmDataList().get(pos);

            //状态;0待审核 1已通过 2未通过
            int status = circleModel.getStatusCode();
            if (status != 2) {
                String id = circleModel.getCircleId();
                if (!TextUtils.isEmpty(id)) {
                    //跳到圈子详情
                    ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY).withString("id", id).navigation();
                }
            } else {
                String rejectReason = circleModel.getAuditRemark();

                AlertPopup alertPopup = new AlertPopup(mContext, "驳回原因", rejectReason, "确定", true);
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                });
            }
        });

        refreshLayout.autoRefresh(500);
    }

    private void getListData(int page) {
        current = page;
        basePresenter.getCircleApplyList(current, size);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void handleCircleHistoryList(ListResult<CircleModel> listResult) {

        List<CircleModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}