package com.yanhua.moment.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.SlidingTabLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.common.widget.SelectPartShadowPopupView;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.MomentVideoAdapter;
import com.yanhua.moment.fragment.MomentVideoFragment;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 新版电音集锦
 */
@Route(path = ARouterPath.ELECT_SOUND_LIST_ACTIVITY)
public class ElectSoundListActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshSearch)
    SmartRefreshLayout refreshSearch;
    @BindView(R2.id.rv_result)
    RecyclerView rvResult;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;
    @BindView(R2.id.iconShowMore)
    TextView iconShowMore;

    @BindView(R2.id.stabLayout)
    RelativeLayout stabLayout;

    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;
    @BindView(R2.id.tv_search_cancle)
    TextView tvSearchCancle;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    private List<MomentListModel> mListData;
    private List<SelectConditionModel> mCityList;
    private ChannelPageAdapter mAdapter;
    private ArrayList<Fragment> mFragments;
    private SelectConditionModel selectCondition;
    private SelectPartShadowPopupView conditionPopupView;
    private int total;
    private MomentVideoAdapter adapter;
    @Autowired
    String id;

    @Override
    public int bindLayout() {
        return R.layout.activity_electric_sound_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, iconShowMore);

        refreshSearch.setOnRefreshListener(refreshLayout -> {
            current = 1;
            String keyword = etSearch.getText().toString().trim();
            getData(keyword);
        });

        refreshSearch.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                String keyword = etSearch.getText().toString().trim();
                getData(keyword);
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });

        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                String keyword = etSearch.getText().toString().trim();
                getData(keyword);
                return true;
            }
            return false;
        });
        mListData = new ArrayList<>();

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvResult.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        rvResult.setLayoutManager(managerContent);

        adapter = new MomentVideoAdapter(mContext, 1, "");

        rvResult.setAdapter(adapter);

        adapter.setOnItemClickListener((itemView, pos) -> {
            MomentListModel clickItem = adapter.getItemObject(pos);
            //跳转到详情
            if (clickItem.getContentType() == 2) {
                ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                        .withSerializable("contentModel", clickItem)
                        .withString("categoryId", "")
                        .withInt("discoBest", 1)//电音集锦
                        .withString("city", "")
                        .withInt("module", 0)
                        .navigation();
            } else {
                ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                        .withString("id", clickItem.getId())
                        .navigation();
            }
        });
    }

    private void getData(String keyword) {
        if (null != basePresenter) {
            current = 1;
            basePresenter.getDiscoBestList(keyword, "", current, size);
        }
    }


    @OnClick({R2.id.tvPublish, R2.id.ll_to_search, R2.id.tv_search_cancle})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.tvPublish) {
            //
        } else if (id == R.id.ll_to_search) {
            rlSearch.setVisibility(View.VISIBLE);
            etSearch.setFocusable(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.requestFocus();
        } else if (id == R.id.tv_search_cancle) {
            hideSoftKeyboard();
            rlSearch.setVisibility(View.GONE);
            etSearch.setText("");
            refreshSearch.setVisibility(View.GONE);
        }
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mCityList = new ArrayList<>();

        basePresenter.getDiscoBestCityList();//
    }


    @Override
    public void handElectSoundCityList(ValueContentModel data) {
        if (null != data) {
            SelectConditionModel suggest = new SelectConditionModel();
            suggest.setFieldName("城市");
            suggest.setFieldValue("全部");
            suggest.setId("");

            mCityList.add(suggest);

            List<SelectConditionModel> cityList = data.getValue_count();

            if (null != cityList && cityList.size() > 0) {
                mCityList.addAll(cityList);
            }


            initCategoryView();
        }
    }


    private void initCategoryView() {
        if (mCityList != null && mCityList.size() > 0) {
            mFragments = new ArrayList<>();

            int size = mCityList.size();
            if (size > 4) {
                iconShowMore.setVisibility(View.VISIBLE);
            } else {
                iconShowMore.setVisibility(View.GONE);
            }

            for (int i = 0; i < size; i++) {
                SelectConditionModel model = mCityList.get(i);

                Fragment fragment = new MomentVideoFragment();
                Bundle bundle = new Bundle();
                String name = model.getFieldValue();
                String id = model.getId();

                bundle.putInt("discoBest", 1);
                bundle.putString("id", id);
                bundle.putString("city", "全部".equals(name) ? "" : name);
                bundle.putString("name", name);
                fragment.setArguments(bundle);

                mFragments.add(fragment);
            }

            mAdapter = new ChannelPageAdapter(getSupportFragmentManager());
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {

        public ChannelPageAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mCityList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            SelectConditionModel model = mCityList.get(position);
            String fieldValue = model.getFieldValue();
            return fieldValue;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.iconShowMore) {
            showConditionPopup();
        }
    }

    public void showConditionPopup() {
        if (conditionPopupView == null) {
            if (null == selectCondition) {
                //默认取第一个作为默认值
                selectCondition = mCityList.get(0);
            }

            conditionPopupView = new SelectPartShadowPopupView(this, mCityList, selectCondition, 0, R.drawable.shape_bg_white);
            conditionPopupView.setOnSelectTypeListener((item, pos) -> {
                setConditionText(true, item, pos);
            });
        }

        new XPopup.Builder(mContext).atView(stabLayout).asCustom(conditionPopupView).show();
        iconShowMore.setText("\ue739");
    }


    private void setConditionText(boolean select, SelectConditionModel selectObj, int pos) {
        if (null == selectCondition) {
            selectCondition = mCityList.get(0);
        } else {
            selectCondition = selectObj;
        }

        iconShowMore.setText("\ue750");

        if (select) {
            sltabChannel.setCurrentTab(pos);
        }
    }

    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> data) {
        if (data != null) {
            List<MomentListModel> records = data.getRecords();
            if (records != null && !records.isEmpty()) {
                refreshSearch.setVisibility(View.VISIBLE);
            } else {
            }
            total = data.getTotal();

            if (current == 1) {
                refreshSearch.finishRefresh();
                mListData.clear();
            } else {
                refreshSearch.finishLoadMore();
            }

            if (records != null && !records.isEmpty()) {
                mListData.addAll(records);
            }
            adapter.setItems(mListData);
        }
    }
}
