package com.yanhua.moment.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.SlidingTabLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.imageview.ShapeableImageView;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.Channel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.MomentHotListModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.ChannelPageAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;
import com.yanhua.rong.msgprovider.TopicCircleMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 圈子详情
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_DETAIL_ACTIVITY)
public class CircleDetailActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.toolbar)
    LinearLayout toolbar;
    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;

    @BindView(R2.id.flBg)
    FrameLayout flBg;
    @BindView(R2.id.ivBg)
    ImageView ivBg;
    @BindView(R2.id.rIVCirclePic)
    ShapeableImageView rIVCirclePic;
    @BindView(R2.id.tvCircleName)
    TextView tvCircleName;
    @BindView(R2.id.tvCount)
    TextView tvCount;//"5422条动态 7878次浏览"
    @BindView(R2.id.tvDesc)
    TextView tvDesc;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;
    @BindView(R2.id.tvCircleOwner)
    TextView tvCircleOwner;
    @BindView(R2.id.tvJoinCircle)
    TextView tvJoinCircle;

    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;

    @Autowired
    String id;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

//        //3 2
        int width = DisplayUtils.getScreenWidth(mContext);
        int height = width * 2 / 3 + DisplayUtils.dip2px(mContext, 16);

        ViewGroup.LayoutParams imageLayoutParams = flBg.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = height;
        flBg.setLayoutParams(imageLayoutParams);

        refreshLayout.setOnRefreshListener(rl -> {
            refreshData();
            rl.finishRefresh(1000);
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        setTopView();
    }


    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);

            if (alp >= 0 && alp < 125) {
                rlTitle.setVisibility(View.GONE);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
            } else if (alp >= 125 && alp <= 255) {
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                rlTitle.setVisibility(View.VISIBLE);
            }

            toolbar.setBackgroundColor(Color.argb(alp, 255, 255, 255));
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    private void refreshData() {
        Channel channel = mChannelList.get(sltabChannel.getCurrentTab());
        EventBus.getDefault().post(new MessageEvent(CommonConstant.MOMENT_HOME_LIST_REFRESH_CIRCLE, String.valueOf(channel.getType())));
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        mChannelList = new ArrayList<>();
        Channel hottest = new Channel("CIRCLE", id, false, "精华", "精华", 1);
        Channel latest = new Channel("CIRCLE", id, false, "全部", "全部", 2);

        mChannelList.add(hottest);
        mChannelList.add(latest);

        initCategoryView();

        basePresenter.getCircleDetail(id);//获取圈子详情

        if (UserManager.getInstance().isLogin()) {
            //我的加入第一页的时候可以请求
            basePresenter.getCircleStatus(id);//获取圈子状态
        }

        basePresenter.addCircleViewCount(id);
    }

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager(), mChannelList);
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @OnClick({R2.id.ll2Detail, R2.id.tvJoinCircle, R2.id.iconMore})
    public void onClickView(View view) {
        int viewId = view.getId();
        if (viewId == R.id.ll2Detail) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.CIRCLE_INFO_DETAIL_ACTIVITY).withString("circleId", id).navigation();
            });

        } else if (viewId == R.id.tvJoinCircle) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                //1 加入 2 退出
                basePresenter.joinAction(id, !joninStatus ? 1 : 2);
            });
        } else if (view.getId() == R.id.iconMore) {
            //展示更多
            MoreActionBoard.shareCircleDetail(mActivity, mModle, (position, tp) -> {
                switch (position) {
                    case ShareBoardPopup.BOARD_SET_REPORT:
                        toReport();
                        break;

                    case ShareBoardPopup.BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == mModle) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, mModle, YXConfig.TYPE_CIRCLE, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                }
            });
        }
    }

    /**
     * 举报内容
     *
     * @param
     */
    private void toReport() {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", id)
                    .withInt("reportType", YXConfig.reportType.circle)
                    .navigation();
        });
    }

    @Override
    public void handActionSuccess(int type) {
        //1 加入 2 退出
        switch (type) {
            case 1:
                setJoinStatus(true);

                ToastUtils.showShort("加入圈子成功");
                break;
            case 2:
                setJoinStatus(false);

                ToastUtils.showShort("退出圈子成功");
                break;
        }
    }

    private void setJoinStatus(boolean b) {
        joninStatus = b;
        tvJoinCircle.setText(joninStatus ? "退出" : "加入");//1加入
        tvJoinCircle.setSelected(joninStatus);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    CircleModel mModle;

    @Override
    public void handCircleDetail(CircleModel circle) {
        if (null != circle) {
            mModle = circle;
            showCircleDetail(circle);
        }
    }

    private void showCircleDetail(CircleModel circle) {
        tvCircleName.setText(YHStringUtils.value(circle.getTitle()));
        tvCount.setText(circle.getDynamicCount() + "条动态 " + circle.getViewCount() + "次浏览");
        tvDesc.setText(YHStringUtils.value(circle.getCircleDesc()));
        tvTitle.setText(YHStringUtils.value(circle.getTitle()));

        String circleOwnerID = YHStringUtils.value(circle.getCircleMaster());

        if (TextUtils.isEmpty(circleOwnerID)) {
            tvCircleOwner.setText("暂无圈主");
        } else {
            //请求圈主个人信息
            //获取这个人的详情
            if (UserManager.getInstance().isLogin()) {

                tvJoinCircle.setVisibility(UserManager.getInstance().getUserId().equals(circleOwnerID) ? View.GONE : View.VISIBLE);

                basePresenter.getUserDetail(circleOwnerID);
            }
        }

        ImageLoaderUtil.loadImgCenterCrop(rIVCirclePic, circle.getCoverUrl(), R.mipmap.bg_bluer);
        ImageLoaderUtil.loadImgBlur(ivBg, circle.getCoverUrl(), R.mipmap.bg_bluer);
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        TopicCircleMessage messageContent = new TopicCircleMessage();

        messageContent.setId(mModle.getId());
        messageContent.setCoverUrl(mModle.getCoverUrl());
        messageContent.setTitle("◎" + mModle.getTitle());
        messageContent.setType(MomentHotListModel.CIRCLE);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            rlTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);

                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, "[推荐圈子]");

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }

    private boolean joninStatus;

    @Override
    public void handCircleStatus(MomentCircleStatus circleStatus) {
        setJoinStatus(circleStatus.isJoinCircle());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        //从个人信息中取出的信息并不知道备注名
        String circleOwner = "<font color=\"#ffffff\" >圈主</font>"
                + "<font color=\"#ffffff\" >" + " " + "</font>"
                + "<font color=\"#00EDFF\" >" + userInfo.getNickName() + "</font>";
        tvCircleOwner.setText(Html.fromHtml(circleOwner));
    }
}
