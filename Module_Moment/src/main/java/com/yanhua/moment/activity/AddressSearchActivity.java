package com.yanhua.moment.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.LocationConst;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.PoiAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 选择地点页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.ADDRESS_SEARCH_ACTIVITY)
public class AddressSearchActivity extends BaseMvpActivity implements  AMapLocationListener, PoiSearch.OnPoiSearchListener {
    @BindView(R2.id.et_search)
    EditText etSearch;
    @BindView(R2.id.rv_address)
    RecyclerView rvAddress;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.et_error)
    EmptyLayout emptyLayout;

    private int current = 1;
    private String keyWord;

    //声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;
    private double latitude;
    private double longitude;
    private List<PoiItem> mPoiList = new ArrayList<>();
    private PoiAdapter mAdapter;


    @Override
    public int bindLayout() {
        return R.layout.activity_address_search;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        mAdapter = new PoiAdapter(mContext);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter.setItems(mPoiList);
        rvAddress.setLayoutManager(manager);
        rvAddress.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            PoiItem poiItem = mPoiList.get(pos);

            Intent intent = new Intent();
            intent.putExtra("poiItem", poiItem);
            setResult(RESULT_OK, intent);
            finish();
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                keyWord = charSequence.toString().trim();
                current = 1;
                toQuery();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                return true;
            }
            return false;
        });


        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                current = 1;
                refreshLayout.finishRefresh(1000);
                toQuery();
            }
        });

        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                current = current + 1;
                refreshLayout.finishLoadMore(100);
                toQuery();
            }
        });
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        etSearch.setHint("搜索地点");

        try {
            mlocationClient = new AMapLocationClient(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置定位监听
        mlocationClient.setLocationListener(this);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(2000);
        //设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用onDestroy()方法
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
        //启动定位
        checkLocationPermission();
    }

    private void checkLocationPermission() {
        requestCheckPermission(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                mlocationClient.startLocation();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_location_permission_tip);
            }
        });
    }


    @Override
    protected void creatPresent() {

    }

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                mlocationClient.stopLocation();
                //获取纬度
                latitude = amapLocation.getLatitude();
                //获取经度
                longitude = amapLocation.getLongitude();

                cityCode = amapLocation.getCityCode();

                toQuery();
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                mlocationClient.stopLocation();
                if (amapLocation.getErrorCode() == 12) {
                    // 缺少定位权限
                    ToastUtils.showShort(R.string.please_grant_location_permission);
                } else if (amapLocation.getErrorCode() == 6) {
                    // 定位失败
                    ToastUtils.showShort(R.string.fail_to_get_location);
                }
            }
        }
    }

    @Override
    public void onPoiSearched(PoiResult result, int i) {
        this.hideLoading();
        List<PoiItem> poiList = result.getPois();
        if (current == 1) {
            mPoiList.clear();
            mPoiList = poiList;
            if (mPoiList.isEmpty()) {
                emptyLayout.setVisibility(View.VISIBLE);
                emptyLayout.setErrorType(EmptyLayout.NODATA);
                emptyLayout.setErrorMessage("地点为空");
            }
        } else {
            mPoiList.addAll(poiList);
        }
        mAdapter.setItems(mPoiList);
    }

    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mlocationClient.onDestroy();
    }

    @OnClick({
            R2.id.rl_channel
    })
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.rl_channel) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private String cityCode = "";

    private void toQuery() {
        this.showLoading();

        String searchCityCode = cityCode;
        if (TextUtils.isEmpty(cityCode)) {
            searchCityCode = LocationConst.DEFAULT_CITY_CODE;//北京市
        }
        PoiSearch.Query query = new PoiSearch.Query(!TextUtils.isEmpty(keyWord) ? keyWord : "", "", searchCityCode);
        query.setDistanceSort(true);
        query.setPageNum(current);
        query.setPageSize(20);


        PoiSearch search = null;
        try {
            search = new PoiSearch(this, query);
        } catch (AMapException e) {
            e.printStackTrace();
        }
        //如果没有有输入关键字的话就是周边搜索
        if (TextUtils.isEmpty(keyWord) && latitude != 0) {
            search.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude, longitude), 10000));
        }

        search.setOnPoiSearchListener(this);
        search.searchPOIAsyn();
    }
}
