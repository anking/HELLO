package com.yanhua.moment.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.ImageAdapter;
import com.yanhua.common.model.ApplyCircleForm;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.DealResult;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_APPLY_ACTIVITY)
public class CircleApplyActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;

    @BindView(R2.id.etTitle)
    EditText etTitle;
    @BindView(R2.id.tvTitleNum)
    TextView tvTitleNum;
    @BindView(R2.id.etSubTitle)
    EditText etSubTitle;
    @BindView(R2.id.tvSubTitleNum)
    TextView tvSubTitleNum;

    @BindView(R2.id.etNotice)
    EditText etNotice;
    @BindView(R2.id.tvNoticeNum)
    TextView tvNoticeNum;

    @BindView(R2.id.etDesc)
    EditText etDesc;
    @BindView(R2.id.tvDescNum)
    TextView tvDescNum;

    @BindView(R2.id.gv_content)
    RecyclerView gvContent;

    @BindView(R2.id.tvCommit)
    TextView tvCommit;

    @Autowired
    String circleId;

    @Autowired
    String applyId;

    private int total;
    private ImageAdapter imageAdapter;
    ArrayList<String> pathList = new ArrayList<>();

    private StsTokenModel stsTokenModel;
    private ApplyCircleForm form;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_apply;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("申请圈子");
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("申请记录");

        pathList.add(null);
        imageAdapter = new ImageAdapter(mContext, pos -> {
            imageAdapter.getmDataList().remove(pos);

            pathList.add(null);
            imageAdapter.setItems(pathList);
        });
        imageAdapter.setOnItemClickListener((itemView, index) -> {
            checkStoragePermission();
        });
        imageAdapter.setItems(pathList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gvContent.setLayoutManager(gridLayoutManager);
        gvContent.setAdapter(imageAdapter);
        gvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        basePresenter.getStsToken();

        if (!TextUtils.isEmpty(circleId)) {//如果id不为空，请求圈子详情进行编辑
            basePresenter.getCircleDetail(circleId);

            etTitle.setEnabled(false);
        } else {
            if (!TextUtils.isEmpty(applyId)) {//如果id不为空，请求圈子详情进行编辑
                basePresenter.getCircleApplyDetail(applyId);
            }
        }
    }

    private static final int CHOOSE_LOGO = 0x001;

    private void chooseImage() {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(3, 2)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(true)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(CHOOSE_LOGO);
    }

    private void checkStoragePermission() {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImage();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @OnClick({R2.id.tv_right, R2.id.tvCommit})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.tv_right) {
            //跳转到申请记录
            ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_HISTORY_ACTIVITY).navigation();
        } else if (id == R.id.tvCommit) {
            String title = etTitle.getText().toString().trim();
            String desc = etDesc.getText().toString().trim();

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(desc) && pathList.size() > 0 && pathList.get(0) != null) {
                form = new ApplyCircleForm();
                form.setTitle(title);
                form.setSubtitle(etSubTitle.getText().toString().trim());
                form.setCircleDesc(desc);
                form.setNotice(etNotice.getText().toString().trim());

                String url = pathList.get(0);
                if (!TextUtils.isEmpty(url)) {
                    String urlPath = url.toLowerCase();
                    if (urlPath.startsWith("http://") || urlPath.startsWith("https://")) {
                        form.setCoverUrl(url);

                        if (TextUtils.isEmpty(circleId)) {
                            basePresenter.applyCircle(form);
                        } else {
                            basePresenter.editCircle(form, circleId);
                        }
                    } else {
                        //需要上传
                        uploadFile(new File(pathList.get(0)));
                    }
                }
            }
        }
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    @Override
    public void doBusiness() {
        super.doBusiness();
        //
        etTitle.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> {
            int left = 15 - s.length();
            tvTitleNum.setText(String.valueOf(left));
            addListenEdit();
        }));
        etDesc.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> {
            int left = 500 - s.length();
            tvDescNum.setText(String.valueOf(left));
            addListenEdit();
        }));


        etNotice.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> {
            int left = 200 - s.length();
            tvNoticeNum.setText(String.valueOf(left));
            addListenEdit();
        }));
        etSubTitle.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> {
            int left = 32 - s.length();
            tvSubTitleNum.setText(String.valueOf(left));
            addListenEdit();
        }));

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String title = etTitle.getText().toString().trim();
//        String subTitle = etSubTitle.getText().toString().trim();
//        String notice = etNotice.getText().toString().trim();
        String desc = etDesc.getText().toString().trim();

        tvCommit.setEnabled(!TextUtils.isEmpty(title) /*&& !TextUtils.isEmpty(subTitle)
                && !TextUtils.isEmpty(notice)*/ && !TextUtils.isEmpty(desc)
                && pathList.size() > 0 && pathList.get(0) != null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_LOGO && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();

            String path;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }

            pathList.clear();
            pathList.add(path);

            imageAdapter.setItems(pathList);

            addListenEdit();
        }
    }

    private void uploadFile(File file) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);

        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();

                //上传完毕后提交内容
                form.setCoverUrl(httpUrl);

                if (TextUtils.isEmpty(circleId)) {
                    basePresenter.applyCircle(form);
                } else {
                    basePresenter.editCircle(form, circleId);
                }
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
            }
        });
    }

    @Override
    public void handApplyCircleResult() {
        ToastUtils.showShort("提交成功");

        if (TextUtils.isEmpty(circleId)) {
            //跳到结果页面
            DealResult dealResult = new DealResult();
            dealResult.setGoToObj(null);
            dealResult.setTvGoPage(null);
            dealResult.setIvResultColor(R.color.theme);
            dealResult.setIvResultSize(56);
            dealResult.setIvResultText("\ue76a");
            dealResult.setTvResultText("你已申请圈主");
            dealResult.setTvResultDescText("请耐心等候管理员审核，预计审核时长2-3工作日");
            dealResult.setTvGoPageText("返回圈子");
            dealResult.setSubObj(null);
            dealResult.setSubPage(ARouterPath.CIRCLE_APPLY_HISTORY_ACTIVITY);
            dealResult.setSubText("查看申请记录");

            ARouter.getInstance()
                    .build(ARouterPath.DEAL_RESULT_ACTIVITY)
                    .withSerializable("dealResult", dealResult)
                    .navigation();
        }

        this.finish();
    }


    @Override
    public void handCircleDetail(CircleModel circle) {
        if (null != circle) {
            showCircleDetail(circle);
        }
    }

    /**
     * 编辑状态进入
     *
     * @param circle
     */
    private void showCircleDetail(CircleModel circle) {
        etTitle.setText(YHStringUtils.value(circle.getTitle()));
        etSubTitle.setText(YHStringUtils.value(circle.getSubtitle()));
        etNotice.setText(YHStringUtils.value(circle.getNotice()));
        etDesc.setText(YHStringUtils.value(circle.getCircleDesc()));

        pathList.clear();
        pathList.add(circle.getCoverUrl());
        imageAdapter.setItems(pathList);
    }
}
