package com.yanhua.moment.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.SlidingTabLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.Channel;
import com.yanhua.common.model.MomentHotListModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.ChannelPageAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;
import com.yanhua.rong.msgprovider.TopicCircleMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 话题详情
 *
 * @author Administrator
 */
@Route(path = ARouterPath.TOPIC_DETAIL_ACTIVITY)
public class TopicDetailActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.toolbar)
    LinearLayout toolbar;
    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;


    @BindView(R2.id.tvTopicName)
    TextView tvTopicName;
    @BindView(R2.id.tvCount)
    TextView tvCount;//"5422条动态 7878次浏览"
    @BindView(R2.id.tvDesc)
    TextView tvDesc;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;

    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;

    @Autowired
    String id;

    @Override
    public int bindLayout() {
        return R.layout.activity_topic_detail;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        refreshLayout.setOnRefreshListener(rl -> {
            refreshData();

            rl.finishRefresh(1000);
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        setTopView();
    }

    private void refreshData() {
        Channel channel = mChannelList.get(sltabChannel.getCurrentTab());
        EventBus.getDefault().post(new MessageEvent(CommonConstant.MOMENT_HOME_LIST_REFRESH_TOPIC, String.valueOf(channel.getType())));
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);

            if (alp >= 0 && alp < 125) {
//                rlTitle.setBackgroundResource(R.drawable.shape_bg_r_white);
//                rlTitle.getBackground().setAlpha(255 - alp);
                rlTitle.setVisibility(View.GONE);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
            } else if (alp >= 125 && alp <= 255) {
//                rlTitle.setBackgroundResource(R.drawable.shape_bg_r_bgui);
//                rlTitle.getBackground().setAlpha(alp);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                rlTitle.setVisibility(View.VISIBLE);
            }

            toolbar.setBackgroundColor(Color.argb(alp, 255, 255, 255));
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mChannelList = new ArrayList<>();
        Channel hottest = new Channel("TOPIC", id, true, "最热", "最热", 1);
        Channel latest = new Channel("TOPIC", id, true, "最新", "最新", 2);

        mChannelList.add(hottest);
        mChannelList.add(latest);

        initCategoryView();

        basePresenter.getTopicDetail(id);
        basePresenter.addTopicViewCount(id);
    }

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager(), mChannelList);
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }


    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handTopicDetail(TopicModel topic) {
        if (null != topic) {
            showTopicDetail(topic);
        }
    }


    @OnClick({R2.id.iconMore})
    public void onClickView(View view) {
        int viewId = view.getId();
        if (view.getId() == R.id.iconMore) {
            //展示更多
            MoreActionBoard.shareTopicDetail(mActivity, mTopic, (position, tp) -> {

                switch (position) {
                    case ShareBoardPopup.BOARD_SET_REPORT:
                        toReport();
                        break;

                    case ShareBoardPopup.BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == mTopic) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, mTopic, YXConfig.TYPE_TOPIC, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                }
            });
        }
    }


    /**
     * 举报内容
     *
     * @param
     */
    private void toReport() {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", id)
                    .withInt("reportType", YXConfig.reportType.topic)
                    .navigation();
        });
    }


    TopicModel mTopic;

    private void showTopicDetail(TopicModel topic) {
        mTopic = topic;
        mTopic.setId(id);

        tvTopicName.setText("#" + YHStringUtils.value(topic.getTitle()) + "#");
        tvCount.setText(topic.getDynamicCount() + "条动态 " + topic.getViewCount() + "次浏览");
        tvDesc.setText(YHStringUtils.value(topic.getTopicDesc()));
        tvTitle.setText("#" + YHStringUtils.value(topic.getTitle()) + "#");
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        TopicCircleMessage messageContent = new TopicCircleMessage();

        messageContent.setId(mTopic.getId());
        messageContent.setCoverUrl(mTopic.getTitleUrl());
        messageContent.setTitle("#" + YHStringUtils.value(mTopic.getTitle()) + "#");
        messageContent.setType(MomentHotListModel.TOPIC);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            rlTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType type = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId,
                            Conversation.ConversationType.PRIVATE,
                            messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, "[推荐话题]");
                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, type, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }
}
