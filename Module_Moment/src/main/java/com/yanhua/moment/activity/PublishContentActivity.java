package com.yanhua.moment.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.style.PictureWindowAnimationStyle;
import com.luck.picture.lib.tools.DateUtils;
import com.luck.picture.lib.tools.DoubleUtils;
import com.luck.picture.lib.tools.PictureFileUtils;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.shuyu.gsyvideoplayer.utils.CommonUtil;
import com.shuyu.textutillib.RichEditBuilder;
import com.shuyu.textutillib.RichEditText;
import com.shuyu.textutillib.listener.OnEditTextUtilListener;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yalantis.ucrop.UCrop;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.ImageAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.PublishContent;
import com.yanhua.common.model.PublishContentForm;
import com.yanhua.common.model.PublishSaveTemp;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OssManager;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.PickTopicBottomPop;
import com.yanhua.common.widget.PrivacyPopup;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.presenter.PublishContentPresenter;
import com.yanhua.moment.presenter.contract.PublishContentContract;
import com.yanhua.moment.view.PickCircleBottomPop;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import top.zibin.luban.Luban;

/**
 * 发布内容页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.PUBLISH_CONTENT_ACTIVITY)
public class PublishContentActivity extends BaseMvpActivity<PublishContentPresenter> implements PublishContentContract.IView {
    private static final int SELECT_ADDRESS = 0x001;
    private static final int SELECT_ATUSER = 0x002;

    private int currentPos = 1000;
    private int lastX;

    @BindView(R2.id.tvLeft)
    TextView tvLeft;
    @BindView(R2.id.btn_publish)
    TextView btnPublish;
    @BindView(R2.id.et_content)
    RichEditText etContent;
    @BindView(R2.id.tv_content_length)
    TextView tvContentLength;
    @BindView(R2.id.gv_content)
    RecyclerView gvContent;
    @BindView(R2.id.iv_video_cover)
    ImageView ivVideoCover;
    @BindView(R2.id.rl_video)
    RelativeLayout rlVideo;
    @BindView(R2.id.tv_address)
    TextView tvAddress;
    @BindView(R2.id.ll_location)
    LinearLayout llLocation;

    @BindView(R2.id.ll_privacy)
    LinearLayout llPrivacy;

    @BindView(R2.id.tv_video_time)
    TextView tvVideoTime;

    @BindView(R2.id.iv_privacy)
    AliIconFontTextView ivPivacy;
    @BindView(R2.id.tv_privacy)
    TextView tvPrivacy;

    @BindView(R2.id.tflCircle)
    TagFlowLayout tflCircle;

    @BindView(R2.id.rlChangeTip)
    RelativeLayout rlChangeTip;

    @BindView(R2.id.nscContent)
    NestedScrollView nscContent;

    @Autowired
    ArrayList<String> pathList = new ArrayList<>();
    @Autowired
    String videoPath;
    @Autowired
    String coverPath;
    @Autowired
    String videoCoverPath;
    @Autowired
    int type;
    @Autowired
    int videoTime;
    @Autowired
    ArrayList<LocalMedia> dataList;
    @Autowired
    boolean isContinueAdd;

    private String mCityName;
    private String issueAddressDetail;
    private double mLatitude;
    private double mLongitude;
    private String mPoiName;

    private String contentCategoryId;
    private String title;
    private String content;

    private ArrayList<String> filePaths;
    private int privacyType = 0;
    private final int INPUT_TIP_MAX_NUM = 50;

    private boolean isShowChangeTip = false;
    List<TopicModel> topicModelsEd = new ArrayList<>();
    List<FriendModel> nameListEd = new ArrayList<>();

    @Override
    protected void creatPresent() {
        basePresenter = new PublishContentPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_publish_content;
    }

    private List<CircleModel> pickedList;
    private List<TopicModel> pickedTopics;
    private List<FriendModel> pickedAt;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        pickedAt = new ArrayList<>();
        pickedTopics = new ArrayList<>();
        pickedList = new ArrayList<>();

        TagAdapter tagAdapter = new TagAdapter<CircleModel>(pickedList) {
            @Override
            public View getView(FlowLayout parent, int position, CircleModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_circle, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                AliIconFontTextView iconDelete = view.findViewById(R.id.iconDelete);

                tvTagName.setText(tagItem.getTitle());
                iconDelete.setOnClickListener(v -> {
                    pickedList.remove(position);
                    tflCircle.getAdapter().refreshData(pickedList);
                });
                return view;
            }
        };

        //控制显示5个
        tflCircle.setAdapter(tagAdapter);
        tflCircle.setOnTagClickListener((view, position, parent) -> {
//                ToastUtils.showShort(tagList.get(position).getName());
            return true;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        onListenPublishStatus();

        if (null != pathList) {
            removeLastNull(pathList);
            if (pathList.size() < 9) {
                pathList.add(null);
            }
        }
    }

    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {// 开了定位服务
            ARouter.getInstance().build(ARouterPath.ADDRESS_SEARCH_ACTIVITY).navigation(mActivity, SELECT_ADDRESS);
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }

    @OnClick({R2.id.tvLeft, R2.id.ll_location, R2.id.llCircle, R2.id.llTopic, R2.id.llPicture, R2.id.rl_video, R2.id.tvChooseCover, R2.id.ll_privacy, R2.id.btn_publish,
            R2.id.llAt, R2.id.rlChangeTip})
    public void onViewClicked(View view) {
        hideSoftKeyboard();
        if (view.getId() == R.id.tvLeft) {
            onBackPressed();
        } else if (view.getId() == R.id.ll_location) {
            checkLoacationStatus();
        } else if (view.getId() == R.id.llCircle) {
            //将已经选取的丢进去
            PickCircleBottomPop popup = new PickCircleBottomPop(mContext, pickedList, new PickCircleBottomPop.OnSelectResultListener() {
                @Override
                public void selectResult(List<CircleModel> list) {
                    if (null != list && list.size() > 0) {
                        pickedList.clear();
                        pickedList.addAll(list);
                        tflCircle.getAdapter().refreshData(pickedList);
                    }
                }
            });
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(false).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(popup).show();
        } else if (view.getId() == R.id.tvChooseCover) {
            onChooseCover();
        } else if (view.getId() == R.id.rl_video) {
            ARouter.getInstance().build(ARouterPath.VIDEO_EDIT)
                    .withString("videoPath", videoPath)
                    .navigation();
        } else if (view.getId() == R.id.btn_publish) {
            if (FastClickUtil.isFastClick(2000)) {
                try {
                    toPublish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (view.getId() == R.id.rlChangeTip) {
            isShowChangeTip = false;
            rlChangeTip.setVisibility(View.GONE);

            content = etContent.getText().toString().trim();

            ARouter.getInstance().build(ARouterPath.LONG_CONTENT_ACTIVITY).withString("content", content).navigation();

            onFinishBackPressed(true);
        } else if (view.getId() == R.id.llTopic) {
            pickTopic(false);
        } else if (view.getId() == R.id.llAt) {
            //好友列表
            ARouter.getInstance().build(ARouterPath.FRIENDS_LIST_ACTIVITY)
                    .withString("userId", UserManager.getInstance().getUserId())
                    .withBoolean("isSelect", true)
                    .navigation(mActivity, SELECT_ATUSER);
        } else if (view.getId() == R.id.llPicture) {
            if (YXConfig.CONTENT_TYPE_VIDEO != type) {
                checkStoragePermission();
            } else {
                com.blankj.utilcode.util.ToastUtils.showShort("已选取视频内容");
            }
        } else if (view.getId() == R.id.ll_privacy) {
            PrivacyPopup privacyPopup = new PrivacyPopup(this, privacyType);
            privacyPopup.setOnItemSelectListener(pType -> {
                privacyType = pType;//privacyType 默认公开(0公开 1仅主页可见 2私密) --- 公开(所有人可见) 主页可见(仅个人主页可见) 私密(仅自己可见)
                if (pType == YXConfig.pivacy.privacy_public) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_public);
                    tvPrivacy.setText("公开");
                } else if (pType == YXConfig.pivacy.privacy_home) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_friend);
                    tvPrivacy.setText("主页");
                } else if (pType == YXConfig.pivacy.privacy_person) {
//                    ivPivacy.setImageResource(R.drawable.icon_privacy_private);
                    tvPrivacy.setText("私密");
                }
            });
            new XPopup.Builder(this).asCustom(privacyPopup).show();
        }
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle
                                 savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        filePaths = new ArrayList<>();
        RichEditBuilder richEditBuilder = new RichEditBuilder();
        richEditBuilder.setEditText(etContent)
                .setTopicModels(topicModelsEd)
                .setUserModels(nameListEd)
                .setEditTextAtUtilJumpListener(new OnEditTextUtilListener() {
                    @Override
                    public void notifyAt() {
                        com.blankj.utilcode.util.ToastUtils.showShort("AT");
                    }

                    @Override
                    public void notifyTopic() {
//                        pickTopic(true);
                    }

                    @Override
                    public void onDelete() {
                        //获取原始数据可以通过以下获取
                        List<TopicModel> topicList = etContent.getRealTopicList();
                        List<FriendModel> userList = etContent.getRealUserList();

                        if (topicList.size() > 0) {
                            pickedTopics.clear();
                            pickedTopics.addAll(topicList);
                        } else {
                            pickedTopics.clear();
                        }

                        if (userList.size() > 0) {
                            pickedAt.clear();
                            pickedAt.addAll(userList);
                        } else {
                            pickedAt.clear();
                        }
                    }

                    @Override
                    public void onValueChange(int length) {
                        //int left = INPUT_TIP_MAX_NUM - length;
                        if (length < INPUT_TIP_MAX_NUM) {

                        } else if (length <= 5000 && length >= INPUT_TIP_MAX_NUM) {
                            //显示转化为头条
                            if (!isShowChangeTip) {
                                rlChangeTip.setVisibility(View.VISIBLE);
                                isShowChangeTip = true;
                            }
                        } else {
                            //输入的字符超过5000
                        }

                        tvContentLength.setText(length + "字");

                        onListenPublishStatus();
                    }
                })
                .builder();

        etContent.setMaxLines(Integer.MAX_VALUE);
        etContent.setHorizontallyScrolling(false);
        etContent.setVerticalScrollBarEnabled(true);

        etContent.setOnTouchListener((v, event) -> {
            if (v.getId() == R.id.et_content && canVerticalScroll(etContent)) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });

        PublishSaveTemp temp1 = PublishTemp.getPublishSaveTemp(mContext);

        if (null != temp1) {
            ArrayList<TopicModel> topicModels = temp1.getTopicModelsEd();
            ArrayList<FriendModel> nameList = temp1.getNameListEd();

            if (null != topicModels && topicModels.size() > 0) {
                topicModelsEd.addAll(topicModels);
            }

            if (null != nameList && nameList.size() > 0) {
                nameListEd.addAll(nameList);
            }

            String initContent = temp1.getContent();
            etContent.resolveInsertText(mActivity, initContent, nameListEd, topicModelsEd);
        }

        onListenPublishStatus();

        //打开页面后弹出输入法
        etContent.postDelayed(() -> {
            etContent.requestFocus();
            etContent.setFocusableInTouchMode(true);

            KeyboardUtils.showSoftInput();
        }, 500);

        gvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        if (type == YXConfig.CONTENT_TYPE_VIDEO) {
            gvContent.setVisibility(View.GONE);
            rlVideo.setVisibility(View.VISIBLE);

            showVideoCover();
            showVideoTime();
        } else {
            rlVideo.setVisibility(View.GONE);
            initGridView();
        }

//        nscContent.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                if (Math.abs(scrollY - oldScrollY) > 10) {//滑动距离超过10像素就收起键盘
//
//                    etContent.setFocusable(false);
//                    etContent.setFocusableInTouchMode(false);
//
//                    KeyboardUtils.hideSoftInput(mActivity);
//                }
//            }
//        });
//        etContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                etContent.requestFocus();
//                etContent.setFocusableInTouchMode(true);
//            }
//        });
    }

    private void onListenPublishStatus() {
        boolean enable = false;

        String input = etContent.getText().toString().trim();

        if (null != coverPath && null != videoPath) {
            enable = true;
        } else if (null != pathList && !pathList.isEmpty()) {
            enable = true;
        } else {
            enable = !TextUtils.isEmpty(input);
        }

        btnPublish.setEnabled(enable);
    }

    private void pickTopic(boolean isEnter) {
        PickTopicBottomPop popup = new PickTopicBottomPop(mContext, topic -> {
            if (null != topic) {
                pickedTopics.add(topic);

                if (isEnter) {//输入法
                    etContent.resolveTopicResultByEnter(topic);
                } else {
                    etContent.resolveTopicResult(topic);//
                }
            } else {
                if (isEnter) {//输入模式
                    if (etContent.getSelectionEnd() == 0) {
                        etContent.getText().delete(0, 1);
                    } else {
                        int index = etContent.getText().toString().indexOf("#", etContent.getSelectionEnd() - 1);
                        if (index != -1) {
                            etContent.getText().delete(index, index + 1);
                        }
                    }
                }
            }
        });
        new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(false).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(popup).show();
    }

    private void showVideoCover() {
        String url = null;
        if (!TextUtils.isEmpty(coverPath)) {
            url = coverPath;
        }
        //下面有值覆盖上面的
        if (!TextUtils.isEmpty(videoCoverPath)) {
            url = videoCoverPath;
        }
        ImageLoaderUtil.loadImgFillCenter(this, ivVideoCover, url, 0);
    }

    /**
     * 设置视频时长
     */
    private void showVideoTime() {
        if (videoTime > 0) {
            tvVideoTime.setVisibility(View.VISIBLE);
            tvVideoTime.setText(CommonUtil.stringForTime(videoTime));
        }
    }

    /**
     * EditText竖直方向是否可以滚动
     *
     * @param //editText需要判断的EditText
     * @return true：可以滚动   false：不可以滚动
     */
    private boolean canVerticalScroll(TextView contentEt) {
        //滚动的距离
        int scrollY = contentEt.getScrollY();
        //控件内容的总高度
        int scrollRange = contentEt.getLayout().getHeight();
        //控件实际显示的高度
        int scrollExtent = contentEt.getHeight() - contentEt.getCompoundPaddingTop() - contentEt.getCompoundPaddingBottom();
        //控件内容总高度与实际显示高度的差值
        int scrollDifference = scrollRange - scrollExtent;

        if (scrollDifference == 0) {
            return false;
        }
        return (scrollY > 0) || (scrollY < scrollDifference - 1);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

        if (type == YXConfig.CONTENT_TYPE_VIDEO) {
            gvContent.setVisibility(View.GONE);
            rlVideo.setVisibility(View.VISIBLE);

            showVideoCover();

            showVideoTime();
        } else {
            rlVideo.setVisibility(View.GONE);
            initGridView();
        }
    }

    private void toPublish() {
        hideSoftKeyboard();
        if (TextUtils.isEmpty(title)) {
            title = "";
        }

        content = etContent.getText().toString().trim();

        if (TextUtils.isEmpty(content)) {
            content = "";
        }

        if (null == filePaths) {
            filePaths = new ArrayList<>();
        }
        filePaths.clear();

        if (null != coverPath && null != videoPath) {
            type = YXConfig.CONTENT_TYPE_VIDEO;
            filePaths.add(coverPath);
            filePaths.add(videoPath);
        } else if (null != pathList && !pathList.isEmpty()) {
            ArrayList<String> pathListTemp = new ArrayList<>();
            pathListTemp.addAll(pathList);
            //删除最后一个为null的，避免出现多个加号
            removeLastNull(pathListTemp);
            filePaths.addAll(pathListTemp);
            if (filePaths.isEmpty()) {
                type = YXConfig.CONTENT_TYPE_TEXT;
            } else {
                type = YXConfig.CONTENT_TYPE_PICTURE;
            }
        } else {
            type = YXConfig.CONTENT_TYPE_TEXT;
        }
        publishContent();
    }

    private void publishContent() {
        ArrayList<String> circles = new ArrayList<>();

        ArrayList<String> topics = new ArrayList<>();
        ArrayList<String> newTopics = new ArrayList<>();

        if (null != pickedList && pickedList.size() > 0) {
            for (int i = 0; i < pickedList.size(); i++) {
                circles.add(pickedList.get(i).getId());
            }
        }

        List<TopicModel> topicList = etContent.getRealTopicList();
        if (null != topicList && topicList.size() > 0) {
            for (int i = 0; i < topicList.size(); i++) {
                TopicModel item = topicList.get(i);

                if (!"-1".equals(item.getId())) {
                    topics.add(item.getId());
                } else {
                    newTopics.add(item.getTitle());
                }
            }
        }

        ArrayList<String> userIdList = new ArrayList<>();
        List<FriendModel> userList = etContent.getRealUserList();
        if (null != userList && userList.size() > 0) {
            for (int i = 0; i < userList.size(); i++) {
                FriendModel item = userList.get(i);
                userIdList.add(item.getUserId());
            }
        }


        try {
            PublishContent contentForm = new PublishContent();

            contentForm.setTopicIdList(topics);
            contentForm.setTopicSaveFormList(newTopics);
            contentForm.setCircleIdList(circles);
            contentForm.setUserIdList(userIdList);
            contentForm.setContentCategoryId(contentCategoryId);
            contentForm.setContent(content);
            contentForm.setContentTitle(title);

            contentForm.setIssueAddressDetail(issueAddressDetail);
            contentForm.setIssueAddress(mPoiName);//
            contentForm.setIssueCity(mCityName);
            if (mLatitude > 0 && mLongitude > 0) {
                contentForm.setIssueLatitude(String.valueOf(mLatitude));
                contentForm.setIssueLongitude(String.valueOf(mLongitude));
            }
            // 1图片 2视频 3文章 4文本
            if (type == YXConfig.CONTENT_TYPE_VIDEO) {
                contentForm.setVideoTime(videoTime / 1000);
                contentForm.setVideoCoverPath(videoCoverPath);
            }

            contentForm.setContentType(type);

            contentForm.setFilePahtList(filePaths);
            contentForm.setPrivacyType(privacyType);

            //以前的逻辑
//            EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
//            EventBus.getDefault().post(new MessageEvent(CommonConstant.PUBLISH_CONTENT_BEFORE, contentForm));
//            onFinishBackPressed(true);

            //改成本页面进行上传
            toCommitContent(contentForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkStoragePermission() {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                if (FastClickUtil.isFastClick(FastClickUtil.MIN_DURATION_2SECOND_TIME)) {
                    chooseImage();
                }
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                com.blankj.utilcode.util.ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    private void chooseImage() {
        int dataSize = 0;

        if (null != dataList) {
            dataSize = dataList.size();
        }

        //不可重新选择
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofAll())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .setPictureStyle(PickImageUtil.getBlackStyle(mActivity))

//               .setPictureStyle(PickImageUtil.getWhiteStyle(this.getActivity()))
                .isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(false)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(false)// 选择数到了最大阀值列表是否启用蒙层效果,启动蒙层会比较丑
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .isOpenStyleNumComplete(false)
                .maxSelectNum(9 - dataSize)// 最大图片选择数量
//                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(9) // 视频最大选择数量
                .videoMinSecond(3)
//                .filterMinFileSize(100)
                .recordVideoMinSecond(3)
                .imageSpanCount(3)// 每行显示个数
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .isCamera(false)// 是否显示拍照按钮
//                .selectionData(dataList)// 是否传入已选图片----/要不要继续传入
                .isZoomAnim(false)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(false)// 是否裁剪
                .isCompress(false)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100);// 小于多少kb的图片不压缩
        try {
            Class cls = Class.forName("com.yanhua.media.activity.YXVideoRecordActivity");
            Intent intent = new Intent(mContext, cls);

//            intent.putExtra("isContinueAdd", YXConfig.CONTENT_TYPE_PICTURE == type);
//            if (null != pathList && pathList.size() > 1) {
//                intent.putExtra("isContinueAdd", true);
//            } else if (null != pathList && pathList.size() == 1 && null != pathList.get(0)) {
//                intent.putExtra("isContinueAdd", true);
//            } else {
//                intent.putExtra("isContinueAdd", false);
//            }

            intent.putExtra("isContinueAdd", dataSize > 0 ? true : false);
            intent.putExtra("alreadyChooseSize", dataSize);

            this.startActivity(intent);

            PictureWindowAnimationStyle windowAnimationStyle = PictureSelectionConfig.windowAnimationStyle;
            mActivity.overridePendingTransition(
                    windowAnimationStyle.activityEnterAnimation, R.anim.picture_anim_fade_in);

            toAddSave();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initGridView() {
        ArrayList<String> pathListTemp = new ArrayList<>();
        ArrayList<LocalMedia> dataListTemp = new ArrayList<>();
        // 获取stsToken，只在发布这里重新获取，避免stsToken过期失效导致上传失败
        PublishSaveTemp temp1 = PublishTemp.getPublishSaveTemp(mContext);
        if (null != temp1) {
            etContent.setText(TextUtils.isEmpty(temp1.getContent()) ? "" : temp1.getContent());

            //从缓存中获取的内容
//            int leftContent = 500 - etContent.getText().toString().length();
            int leftContent = etContent.getText().toString().length();
            tvContentLength.setText(leftContent + "字");

            if (isContinueAdd) {
                //累计加上 pathList dataList
                ArrayList<String> pathList = temp1.getPathList();
                ArrayList<LocalMedia> dataList = temp1.getDataList();

                if (null != pathList) {
                    removeNull(pathList);//删除缓存数据中包含null的数据，避免出现多个加号

                    pathListTemp.addAll(pathList);
                }
                if (null != dataList) {
                    dataListTemp.addAll(dataList);
                }
            }
        }

        //先从缓存中取出数据，然后再加上选取的
        if (null != pathList) {
            pathListTemp.addAll(pathList);
        }
        if (null != dataList) {
            dataListTemp.addAll(dataList);
        }

        pathList = pathListTemp;
        dataList = dataListTemp;

        setNineImageView();
    }


    private ImageAdapter imageAdapter;

    private void setNineImageView() {
        if (null == pathList || pathList.size() == 0) {
            return;
        }

        removeLastNull(pathList);
        if (pathList.size() < 9) {
            pathList.add(null);
        }

        imageAdapter = new ImageAdapter(mContext, pos -> {
            imageAdapter.getmDataList().remove(pos);
            imageAdapter.setItems(imageAdapter.getmDataList());

            if (null != dataList && dataList.size() > 0) {
                dataList.remove(pos);
            }
        });

        imageAdapter.setOnItemClickListener((itemView, index) -> {
            String uri = (String) pathList.get(index);
//            String uri = (String) pathList.get(pathList.size() - 1);
            if (uri == null) {
                checkStoragePermission();
            } else {
                startPicturePreviewActivity(index);
            }
        });
        imageAdapter.setItems(pathList);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gvContent.setLayoutManager(gridLayoutManager);
        gvContent.setAdapter(imageAdapter);
    }

    private void removeNull(ArrayList<String> pathList) {
        if (null != pathList) {
            //删除为null的，避免出现多个加号
            for (String item : pathList) {
                if (TextUtils.isEmpty(item)) {
                    pathList.remove(item);
                }
            }
        }
    }

    private void removeLastNull(ArrayList<String> pathList) {
        if (null != pathList && pathList.size() > 0) {
            //删除最后一个为null的，避免出现多个加号
            int lastIndex = pathList.size() - 1;
            String last = pathList.get(lastIndex);

            if (TextUtils.isEmpty(last)) {
                pathList.remove(lastIndex);
            }
        }
    }

    private void startPicturePreviewActivity(int index) {
        List<LocalMedia> result = dataList;

        Class cls = null;
        try {
            cls = Class.forName("com.yanhua.media.activity.YXPicturePreviewActivity");

            ArrayList<String> pathListTemp = new ArrayList<>();

            pathListTemp.addAll(pathList);
            //删除最后一个为null的，避免出现多个加号
            removeLastNull(pathListTemp);

            Intent intent = new Intent(mContext, cls);

            Bundle data = new Bundle();
            data.putInt("type", YXConfig.CONTENT_TYPE_PICTURE);
            data.putStringArrayList("describeList", null);
            data.putStringArrayList("pathList", pathListTemp);
            data.putBoolean("isContinueAdd", false);
            data.putInt("curPos", index);
            data.putParcelableArrayList("listLocalMedia", (ArrayList<? extends Parcelable>) result);

            intent.putExtras(data);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void onFinishBackPressed(boolean noSave) {
        if (noSave) {
            super.onBackPressed();
        }
    }

    @Override
    public void handleErrorMsg(String msg) {
        hideLoading();
        com.blankj.utilcode.util.ToastUtils.showShort(msg);
    }

    private void toAddSave() {
        //删除最后一个为null的，避免出现多个加号
        if (null != pathList) {
            removeLastNull(pathList);
            if (pathList.size() == 0) {
                super.onBackPressed();
                return;
            }
//            super.onBackPressed();
        }
        saveToDraft();
    }

    @Override
    public void onBackPressed() {
        //删除最后一个为null的，避免出现多个加号
        if (null != pathList) {
            removeLastNull(pathList);

            if (pathList.size() == 0) {

                PublishTemp.deletePublishSaveTemp();

                EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));

                super.onBackPressed();
                return;
            }
        }

        CommonDialog commonDialog = new CommonDialog(mContext, "将此次编辑保留草稿？", "保留", "不保留");
        new XPopup.Builder(mContext).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(() -> {
            commonDialog.dismiss();
            saveToDraft();
            super.onBackPressed();

            EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
        });
        commonDialog.setOnCancelListener(() -> {
            commonDialog.dismiss();
            super.onBackPressed();

            PublishTemp.deletePublishSaveTemp();

            EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
        });
    }


    /**
     * 只会保存一个
     */
    private void saveToDraft() {
        if (null != coverPath && null != videoPath) {
            type = YXConfig.CONTENT_TYPE_VIDEO;
        } else if (null != pathList && !pathList.isEmpty()) {
            type = YXConfig.CONTENT_TYPE_PICTURE;
        } else {
            type = YXConfig.CONTENT_TYPE_TEXT;
        }

        PublishSaveTemp temp = new PublishSaveTemp();

        temp.setDataList(dataList);
        temp.setCoverPath(coverPath);
        temp.setVideoTime(videoTime);
        temp.setVideoCoverPath(videoCoverPath);
        temp.setVideoPath(videoPath);
        temp.setType(type);
//            temp.setDescribeList(descList);
        temp.setPathList(pathList);

        temp.setContent(etContent.getText().toString());

        temp.setNameListEd((ArrayList<FriendModel>) etContent.getUserList());
        temp.setTopicModelsEd((ArrayList<TopicModel>) etContent.getTopicList());

        PublishTemp.savePublishSaveTemp(temp);
    }

    /**
     * 选取封面
     */
    public void onChooseCover() {
        if (DoubleUtils.isFastDoubleClick()) {
            return;
        }
        //裁剪页面
        Activity activity = this;
        String originalPath = coverPath;

        if (TextUtils.isEmpty(originalPath)) {
            ToastUtils.showShort(activity.getString(R.string.picture_not_crop_data));
            return;
        }

        boolean isHttp = PictureMimeType.isHasHttp(originalPath);
        File file = new File(PictureFileUtils.getDiskCacheDir(activity.getApplicationContext()), DateUtils.getCreateFileName("IMG_CROP_"));
        Uri uri = isHttp || PictureMimeType.isContent(originalPath) ? Uri.parse(originalPath) : Uri.fromFile(new File(originalPath));

        Class cls = null;
        try {
            Intent mCropIntent;
            Bundle mCropOptionsBundle;

            mCropIntent = new Intent();
            mCropOptionsBundle = new Bundle();
            mCropOptionsBundle.putParcelable(UCrop.EXTRA_INPUT_URI, uri);
            mCropOptionsBundle.putParcelable(UCrop.EXTRA_OUTPUT_URI, Uri.fromFile(file));
            mCropOptionsBundle.putString(UCrop.EXTRA_INPUT_VIDEO_PATH, videoPath);
            mCropOptionsBundle.putString(UCrop.EXTRA_INPUT_VIDEO_COVER, coverPath);
            mCropOptionsBundle.putInt("CURRENT_POS", currentPos);//当前的选取位置
            mCropOptionsBundle.putInt("CURRENT_POS_X", lastX);//当前的选取位置

            cls = Class.forName("com.yanhua.media.activity.YXVideoCropActivity");

            mCropIntent.setClass(activity, cls);

            mCropIntent.putExtras(mCropOptionsBundle);

            activity.startActivityForResult(mCropIntent, UCrop.REQUEST_CROP);
            activity.overridePendingTransition(PictureSelectionConfig.windowAnimationStyle.activityCropEnterAnimation, R.anim.ucrop_anim_fade_in);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_ADDRESS && resultCode == RESULT_OK) {
            if (data != null) {
                PoiItem poiItem = data.getParcelableExtra("poiItem");
                if (poiItem != null) {
                    mPoiName = poiItem.getTitle();
                    issueAddressDetail = poiItem.getSnippet();//详细地址
                    mCityName = poiItem.getCityName();
                    LatLonPoint latLonPoint = poiItem.getLatLonPoint();
                    mLatitude = latLonPoint.getLatitude();
                    mLongitude = latLonPoint.getLongitude();
                }
            } else {
                mPoiName = null;
                issueAddressDetail = null;
                mCityName = null;
                mLatitude = 0;
                mLongitude = 0;
            }

            String address = !TextUtils.isEmpty(mPoiName) ? mPoiName : "你在哪里";
            tvAddress.setText(address);
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            if (data != null) {
                Uri resultUri = UCrop.getOutput(data);
                if (resultUri != null) {
                    String cutPath = resultUri.getPath();

                    data.getExtras();

                    currentPos = data.getIntExtra("CURRENT_POS", 1000);
                    lastX = data.getIntExtra("CURRENT_POS_X", 0);

                    //处理更新封面
                    handRefreshCover(cutPath);
                }
            }
        } else if (requestCode == SELECT_ATUSER && resultCode == RESULT_OK) {
            //pickedTopics.add(topic);
            if (data != null) {
                FriendModel selectAtUser = (FriendModel) data.getSerializableExtra("selectAtUser");
                if (null != selectAtUser) {
                    pickedAt.add(selectAtUser);

                    /*if (isEnter) {//输入法
                        etContent.resolveAtResultByEnterAt(selectAtUser);
                    } else {
                        etContent.resolveAtResult(selectAtUser);//
                    }*/
                    etContent.resolveAtResult(selectAtUser);//
                } else {
                    /*if (isEnter) {//输入模式
                        if (etContent.getSelectionEnd() == 0) {
                            etContent.getText().delete(0, 1);
                        } else {
                            int index = etContent.getText().toString().indexOf("@", etContent.getSelectionEnd() - 1);
                            if (index != -1) {
                                etContent.getText().delete(index, index + 1);
                            }
                        }
                    }*/
                }
            }
        }
    }

    private void handRefreshCover(String cutPath) {
        videoCoverPath = cutPath;

        showVideoCover();
    }

    //-------------上传---------------
    private List<File> fileList;
    private List<String> origFilePaths;
    private List<String> describeFile;
    private List<String> contentUrlList;
    private int updateIndex;
    private PublishContentForm publishContentForm;
    private PublishContent publishContent;

    private OssManager ossManager;
    private StsTokenModel stsTokenModel;


    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        file.mkdirs();
        return path;
    }

    @BindView(R2.id.rl_publish_upload_progress)
    LinearLayout publishUpload;
    @BindView(R2.id.iv_publish_cover)
    ImageView publishCover;
    @BindView(R2.id.tv_publish_show_progress)
    TextView publishShowProgress;
    @BindView(R2.id.tv_publish_desc)
    TextView publishDesc;
    @BindView(R2.id.tv_publish_success)
    TextView publishSuccess;
    @BindView(R2.id.pb_publish)
    ProgressBar publishPB;

    private List<FileResult> fileResultList;
    private MyHandler mHandler = new MyHandler();
    private final static int UPLOAD_FAILED = 1000;
    private final static int UPLOAD_PROGRESS = 1001;
    private final static int UPLOAD_SUCCESS = 1002;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FAILED:
                    publishUpload.setVisibility(View.GONE);

                    ToastUtils.showShort("发布内容失败");
                    break;

                case UPLOAD_PROGRESS:
                    int progress = (int) msg.obj;

                    publishShowProgress.setText(progress + "%");
                    publishPB.setProgress(progress);
                    break;


                case UPLOAD_SUCCESS:
                    int obj = (int) msg.obj;
                    publishSuccess.setVisibility(View.VISIBLE);
                    publishSuccess.setText(obj + "秒后返回");

                    Message msgP = Message.obtain();
                    msgP.what = UPLOAD_SUCCESS;
                    msgP.obj = obj - 1;

                    if (obj == 0) {
                        publishUpload.setVisibility(View.GONE);

                        EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
                        EventBus.getDefault().post(new MessageEvent(CommonConstant.PUBLISH_CONTENT));
                        PublishTemp.deletePublishSaveTemp();//发布成功清除缓存

                        onFinishBackPressed(true);
                        return;
                    }
                    mHandler.sendMessageDelayed(msgP, 1000l);
                    break;
            }
        }
    }


    private void toCommitContent(PublishContent contentForm) {
        publishContent = contentForm;
        origFilePaths = publishContent.getFilePahtList();

        describeFile = new ArrayList<>();
        contentUrlList = new ArrayList<>();
        fileResultList = new ArrayList<>();

        if (null == origFilePaths || (null != origFilePaths && (origFilePaths.size() == 0 || (origFilePaths.size() == 1 && null == origFilePaths.get(0))))) {
            publishContent.setContentType(YXConfig.CONTENT_TYPE_TEXT);
            publishAction(true);
        } else {
            int type = publishContent.getContentType();
            setSmalUploadCover(type);
            basePresenter.getStsToken();
        }
    }

    /**
     * 显示上传小封面
     *
     * @param type
     */
    private void setSmalUploadCover(int type) {
        String cover = null;
        if (type == YXConfig.CONTENT_TYPE_VIDEO) {
            String firstPath = origFilePaths.get(0);
            if (FileUtils.isVideo(firstPath) && origFilePaths.size() > 1) {
                Collections.swap(origFilePaths, 0, 1);
            }
            cover = origFilePaths.get(0);
            String coverPath = publishContent.getVideoCoverPath();
            if (!TextUtils.isEmpty(coverPath)) {
                cover = coverPath;
            }
            ImageLoaderUtil.loadLocalCornerImg(publishCover, new File(cover), 6);
        } else if (type == YXConfig.CONTENT_TYPE_PICTURE) {
            cover = origFilePaths.get(0);
            ImageLoaderUtil.loadLocalCornerImg(publishCover, new File(cover), 6);
        }
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);

        publishAction(false);
    }

    @SuppressLint("AutoDispose")
    private void publishAction(boolean isAllText) {
        fileList = new ArrayList<>();
        updateIndex = 0;

        describeFile.clear();
        contentUrlList.clear();

        publishContentForm = new PublishContentForm();

        publishContentForm.setTopicIdList(publishContent.getTopicIdList());//选中话题id集合{}
        publishContentForm.setTopicSaveFormList(publishContent.getTopicSaveFormList());//创建话题数据集合---通过搜索新建的
        publishContentForm.setCircleIdList(publishContent.getCircleIdList());
        publishContentForm.setUserIdList(publishContent.getUserIdList());

        publishContentForm.setContentCategoryId(publishContent.getContentCategoryId());
        publishContentForm.setContent(publishContent.getContent());
        publishContentForm.setContentTitle(publishContent.getContentTitle());
        publishContentForm.setIssueAddress(publishContent.getIssueAddress());
        publishContentForm.setIssueAddressDetail(publishContent.getIssueAddressDetail());
        publishContentForm.setIssueCity(publishContent.getIssueCity());

        publishContentForm.setIssueLatitude(publishContent.getIssueLatitude());
        publishContentForm.setIssueLongitude(publishContent.getIssueLongitude());
        publishContentForm.setPrivacyType(publishContent.getPrivacyType());

        int type = publishContent.getContentType();

        publishContentForm.setContentType(type);
        publishContentForm.setVideoTime(publishContent.getVideoTime());

        if (isAllText) {
            basePresenter.publishContent(publishContentForm);
        } else {
            //1 图片 2视频
            if (type == YXConfig.CONTENT_TYPE_VIDEO) {
                publishUpload.setVisibility(View.VISIBLE);
//            uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                String firstPath = origFilePaths.get(0);
                if (FileUtils.isVideo(firstPath) && origFilePaths.size() > 1) {
                    Collections.swap(origFilePaths, 0, 1);
                }

                String coverPath = publishContent.getVideoCoverPath();
                if (!TextUtils.isEmpty(coverPath)) {
                    File file = new File(coverPath);
                    uploadVideoCover(file.getName(), file.getPath());
                } else {
                    uploadVideo();
                }
            } else if (type == YXConfig.CONTENT_TYPE_PICTURE) {
                publishUpload.setVisibility(View.VISIBLE);
                basePresenter.addSubscribe(Flowable.just(origFilePaths)
                                .observeOn(Schedulers.io())
                                .map(list -> {
                                    List<File> fileList = new ArrayList<>();
                                    for (String filePath : list) {
                                        try {
                                            File file = new File(filePath);
                                            fileList.add(file);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    return fileList;
                                })
                                .map(new Function<List<File>, List<File>>() {
                                    @Override
                                    public List<File> apply(@NonNull List<File> list) throws Exception {
                                        return Luban.with(mContext)
                                                .setTargetDir(getPath())
                                                .ignoreBy(500)
                                                .load(list)
                                                .get();
                                    }
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnError(new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) {
                                    }
                                })
//                    .onErrorResumeNext(Flowable.<List<File>>empty())
                                .subscribe(new Consumer<List<File>>() {
                                    @Override
                                    public void accept(@NonNull List<File> list) {
                                        fileList.clear();
                                        fileList.addAll(list);
                                        uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                                    }
                                })
                );
            }
        }
    }

    @SuppressLint("AutoDispose")
    private void uploadVideo() {
        basePresenter.addSubscribe(Flowable.just(origFilePaths)
                        .observeOn(Schedulers.io())
                        .map(list -> {
                            List<File> fileList = new ArrayList<>();
                            for (String filePath : list) {
                                try {
                                    File file = new File(filePath);
                                    fileList.add(file);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            return fileList;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                            }
                        })
//                .onErrorResumeNext(Flowable.<List<File>>empty())
                        .subscribe(new Consumer<List<File>>() {
                            @Override
                            public void accept(@NonNull List<File> list) {
                                fileList.clear();
                                fileList.addAll(list);
                                uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                            }
                        })
        );
    }


    private void uploadFile(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        // 判断是图片还是视频
        int index = fileName.lastIndexOf(".");
//        String mediaType = fileName.substring(index + 1);
        boolean isVideo = FileUtils.isVideo(fileName);

        String objectKey = "";
        Date date = new Date();
        try {
            if (isVideo) {
                objectKey = "video/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            } else {
                objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {
            //1 图片 2 视频
            if (publishContentForm.getContentType() == 2 && updateIndex == 1) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_PROGRESS;

                //更新进度
                int progress = (int) (currentSize * 100 / totalSize);
                if (progress == 100) {
                    return;
                }

                msg.obj = progress;
                mHandler.sendMessage(msg);
            }
        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
//                String resultId = fileResult.getId();
//                String httpUrl = fileResult.getHttpUrl();
                fileResult.setHttpUrlType(isVideo ? 2 : 1);

                fileResultList.add(fileResult);
//
//                describeFile.add(resultId);
//                contentUrlList.add(httpUrl);
                updateIndex++;

                if (publishContentForm.getContentType() == 1) {
                    Message msg = Message.obtain();
                    msg.what = UPLOAD_PROGRESS;

                    //更新进度
                    int progress = updateIndex * 100 / origFilePaths.size();
                    msg.obj = progress;
                    mHandler.sendMessage(msg);
                }

                if (updateIndex < origFilePaths.size()) {
                    uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                } else {
                    //
//                    publishContentForm.setFileIds(StringUtils.listToString(describeFile, ","));
                    Gson gson = new Gson();
                    String contentUrl = gson.toJson(fileResultList);

                    publishContentForm.setContentUrl(contentUrl);
                    basePresenter.publishContent(publishContentForm);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FAILED;
                mHandler.sendMessage(msg);
            }
        });
    }

    private void uploadVideoCover(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        int index = fileName.lastIndexOf(".");

        String objectKey = "";
        Date date = new Date();
        objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);

        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {

        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                fileResult.setHttpUrlType(-1);
                fileResultList.add(fileResult);

                uploadVideo();
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FAILED;
                mHandler.sendMessage(msg);
            }
        });
    }


    @Override
    public void publishContentSuccess(ContentModel model) {
        if (model != null) {
            int auditStatus = model.getAuditStatus();
            if (auditStatus == 0) {
                ToastUtils.showShort("发布成功，等待管理员审核");
            } else {
                ToastUtils.showShort("发布成功");
            }
        }

        if (publishUpload.getVisibility() == View.VISIBLE) {//倒计时3秒处理
            Message msg = Message.obtain();
            msg.what = UPLOAD_SUCCESS;
            msg.obj = 3;
            mHandler.sendMessage(msg);
        } else {
            publishUpload.setVisibility(View.GONE);
            EventBus.getDefault().post(new MessageEvent(CommonConstant.FINISH_PAGE));
            EventBus.getDefault().post(new MessageEvent(CommonConstant.PUBLISH_CONTENT));
            PublishTemp.deletePublishSaveTemp();//发布成功清除缓存

            onFinishBackPressed(true);
        }
    }

    @Override
    public void handlePublishErrorMsg(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }
}



