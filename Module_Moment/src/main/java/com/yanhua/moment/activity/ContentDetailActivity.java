package com.yanhua.moment.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chinalwb.are.render.AreTextView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.CustomTextViewTouchListener;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.listener.SpanTopicCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.common.adapter.ContentCommentAdapter;
import com.yanhua.common.adapter.YXDiscoverImageAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.CommentInputTemp;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.TagFlowModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.CommentInputPopup;
import com.yanhua.common.widget.CommentPopup;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.presenter.ContentDetailPresenter;
import com.yanhua.moment.presenter.contract.ContentDetailContract;
import com.yanhua.rong.msgprovider.MomentContentMessage;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.CircleIndicator;
import com.youth.banner.listener.OnPageChangeListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

import static com.yanhua.base.view.ShareBoardPopup.BOARD_SET_DELETE;
import static com.yanhua.base.view.ShareBoardPopup.BOARD_SET_PRIVATE;
import static com.yanhua.base.view.ShareBoardPopup.BOARD_SET_REPORT;
import static com.yanhua.base.view.ShareBoardPopup.BOARD_SHARE_APP;

/**
 * 内容详情页
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CONTENT_DETAIL_ACTIVITY)
public class ContentDetailActivity extends BaseMvpActivity<ContentDetailPresenter> implements ContentDetailContract.IView {

    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;
    @BindView(R2.id.ivSex)
    ImageView ivSex;

    @BindView(R2.id.civ_avatar)
    CircleImageView civAvatar;
    @BindView(R2.id.tv_name)
    TextView tvName;

    @BindView(R2.id.tvUserName)
    TextView tvUserName;
    @BindView(R2.id.civ_user)
    CircleImageView civUser;
    @BindView(R2.id.tv_date)
    TextView tvDate;

    @BindView(R2.id.civFloatUser)
    CircleImageView civFloatUser;

    @BindView(R2.id.tvFloatUserName)
    TextView tvFloatUserName;
    @BindView(R2.id.tvFloatUserSign)
    TextView tvFloatUserSign;
    @BindView(R2.id.btnFloatFollow)
    StatusView btnFloatFollow;

    @BindView(R2.id.tb_base)
    LinearLayout tbBase;
    @BindView(R2.id.tv_content)
    RichTextView tvContent;
    //    AutoClearEditText etCommentFirst;
    @BindView(R2.id.rv_comment)
    RecyclerView rvComment;
    @BindView(R2.id.tv_comment_second)
    TextView tvCommentSecond;
    @BindView(R2.id.iv_collect)
    AliIconFontTextView ivCollect;
    @BindView(R2.id.tv_collect)
    TextView tvCollect;
    @BindView(R2.id.ll_collect)
    LinearLayout llCollect;
    @BindView(R2.id.iv_comment)
    AliIconFontTextView ivComment;
    @BindView(R2.id.tv_comment)
    TextView tvComment;
    @BindView(R2.id.ll_comment)
    LinearLayout llComment;
    @BindView(R2.id.iv_love)
    AliIconFontTextView ivLove;

    @BindView(R2.id.tv_love)
    TextView tvLove;
    @BindView(R2.id.ll_love)
    LinearLayout llLove;
    @BindView(R2.id.tflMomentItem)
    TagFlowLayout tflMomentItem;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.ll_nodata)
    LinearLayout llNodata;

    @BindView(R2.id.tv_total_comment)
    TextView tvTotalComment;

    @BindView(R2.id.ll_content)
    LinearLayout llContent;
    //关注
    @BindView(R2.id.btFollow)
    StatusView btFollow;

    @BindView(R2.id.rlFloat)
    RelativeLayout rlFloat;

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @BindView(R2.id.rlUserInfo)
    RelativeLayout rlUserInfo;
    @BindView(R2.id.llTopContent)
    LinearLayout llTopContent;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;
    @BindView(R2.id.tvTimeDesc)
    TextView tvTimeDesc;
    @BindView(R2.id.tvIntrduce)
    TextView tvIntrduce;
    @BindView(R2.id.areTextView)
    AreTextView areTextView;

    @BindView(R2.id.tv_profile)
    TextView tv_profile;
    @BindView(R2.id.bannerImgs)
    Banner bannerImgs;
    @BindView(R2.id.tv_banner_index)
    TextView tvBannerIndex;
    @BindView(R2.id.flBannerImgs)
    RelativeLayout flBannerImgs;
    @BindView(R2.id.indicator)
    CircleIndicator indicator;

//
//    @BindView(R2.id.rcvImgs)
//    RecyclerView rcvImgs;


    @Autowired
    String id;

    @Autowired
    String fromClassName;

    private boolean isShowFloat;

    private final static int CHILD_SIZE = 1;

    private List<CommentModel> mList = new ArrayList<>();

    //组装后的评论列表
    private List<CommentModel> mCommentList = new ArrayList<>();

    private int current = 1;
    private ContentCommentAdapter mAdapter;
    private int currentPosition;

    private MomentListModel mModel;

    private boolean isReply = false;

    private int index = 0;
    private Timer timer;
    private boolean isFirstLoad;//控制第一次加载和下拉的控制

    private int contentType;

    //    "contentType": 3,
    @Override
    protected void creatPresent() {
        basePresenter = new ContentDetailPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_content_detail;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContentCommentAdapter(mContext, "");

        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(rl -> {
            current = 1;
            isFirstLoad = true;
            refreshLayout.finishRefresh(1000);
            basePresenter.getContentDetail(true, id);
            basePresenter.getCommentList(id, current, 1);
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mList.size() == 0 || mList.size() % 10 != 0) {
                refreshLayout.finishLoadMore(100, true, true);
                return;
            }
            current = current + 1;
            refreshLayout.finishLoadMore(100);
//            basePresenter.getContentDetail(false, id);
            basePresenter.getCommentList(id, current, 1);
        });

        rvComment.setLayoutManager(manager);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvComment.setRecycledViewPool(pool);

        //关闭recyclerview动画
        rvComment.getItemAnimator().setAddDuration(0);
        rvComment.getItemAnimator().setChangeDuration(0);
        rvComment.getItemAnimator().setMoveDuration(0);
        rvComment.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvComment.getItemAnimator()).setSupportsChangeAnimations(false);

        rvComment.setAdapter(mAdapter);

        rvComment.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnADClickListener(clickItem -> {
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);
            PageJumpUtil.adJumpContent(clickItem, (Activity) mContext, -1);
        });
        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);

            //resultType;//结果类型(1:内容,2:广告)
            int resultType = model.getResultType();
            if (resultType == 2) {
                return;
            }

            if (cellType == 0) {
                String uId = model.getUserId();
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", uId)
                        .navigation();
            } else if (cellType == 1) {

                PageJumpUtil.firstIsLoginThenJump(() -> {
                    String commentId = model.getId();
//                    basePresenter.updateStarComment(commentId, YXConfig.TYPE_MOMENT);
                    basePresenter.updateStarComment(commentId);
                });
            } else if (cellType == 2) {
                int groupPosition = model.getGroupPosition();
                String parentCommentId = mList.get(groupPosition).getId();
                List<CommentModel> childCommentList = mList.get(groupPosition).getChildrens();
                int size = childCommentList.size() + 5;
                basePresenter.getChildCommentList(parentCommentId, 1, size);
            }
        });

        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            //resultType;//结果类型(1:内容,2:广告)
            int resultType = model.getResultType();
            if (resultType == 2) {
                return;
            }

            boolean myself = UserManager.getInstance().isMyself(model.getUserId());

            boolean isMyContent = UserManager.getInstance().isMyself(mModel.getUserId());
            CommentBoardPopup popup = new CommentBoardPopup(mActivity, isMyContent, true, true, !myself, myself);
            popup.setOnActionItemListener(actionType -> {
                switch (actionType) {
                    case CommentBoardPopup.ACTION_TOP:
                        toTop(pos);
                        break;
                    case CommentBoardPopup.ACTION_REPLY:
                        //回复
                        replyComment(model);
                        break;
                    case CommentBoardPopup.ACTION_COPY:
                        String content = model.getCommentDesc();

                        YHStringUtils.copyContent(mActivity, content);
                        break;
                    case CommentBoardPopup.ACTION_REPORT:
                        toReport(pos);
                        break;
                    case CommentBoardPopup.ACTION_DELETE:
                        toDelete(pos);
                        break;
                }

            });

            new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
        });

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            //resultType;//结果类型(1:内容,2:广告)
            int resultType = model.getResultType();
            if (resultType == 2) {
                return;
            }
            //回复
            replyComment(model);
        });

//        setTopView();
    }

    /**
     * 置顶评论
     *
     * @param pos
     */
    private void toTop(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();

        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.dealCommentItem2Top(contentId, YXConfig.TYPE_MOMENT, pos);
        });
    }

    @Override
    public void handleDealItemTopSuccess(int pos) {
        if (mAdapter != null) {
            List list = mAdapter.getmDataList();
            Collections.swap(list, pos, 0);
            mAdapter.notifyItemMoved(pos, 0);
        }
    }

    /**
     * 删除评论
     *
     * @param pos
     */
    private void toDelete(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();


        PageJumpUtil.firstIsLoginThenJump(() -> {

            if (!UserManager.getInstance().isMyself(currentUserId)) {
                ToastUtils.showShort("只能删除自己的评论");
                return;
            }

            basePresenter.deleteCommentContent(contentId, pos);
        });
    }

    @Override
    public void handleDeleteCommentSuccess(int pos) {
        if (mAdapter != null) {
            mCommentList.remove(pos);
            mAdapter.notifyItemRemoved(pos);
            mAdapter.notifyItemRangeChanged(pos, mCommentList.size() - pos);

            int replyCount = mModel.getReplyCount() - 1;
            mModel.setReplyCount(replyCount);

            tvTotalComment.setText("评论 " + replyCount);
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

            ToastUtils.showShort("评论删除成功");
        }
    }

    /**
     * 回复动作
     *
     * @param model
     */
    private void replyComment(CommentModel model) {
        String commentId = model.getId();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        int childType = model.getChildType();

        showInputPopup(commentId, nickName, childType);
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);

            if (alp >= 0 && alp < 18) {
                rlTitle.setVisibility(View.GONE);
            } else if (alp >= 18 && alp <= 255) {
                rlTitle.setVisibility(View.VISIBLE);

                String pUserId = mModel.getUserId();
                int followStatus = mModel.getFollowStatus();
                boolean isFollow = DiscoValueFormat.isFollow(followStatus);
                if (!isShowFloat && !UserManager.getInstance().isMyself(pUserId) && UserManager.getInstance().isLogin() && !isFollow) {
                    rlFloat.setVisibility(View.VISIBLE);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
        }
    }

    //    private ImageAdapter imageAdapter;
    private ArrayList<String> pathList;

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        CommentInputTemp.setNick("");
        CommentInputTemp.setContent("");
        // 评论列表
        isFirstLoad = true;
        basePresenter.getContentDetail(true, id);
        basePresenter.getCommentList(id, current, 1);


        pathList = new ArrayList<>();

/*
        imageAdapter = new ImageAdapter(mContext, true, pos -> {
            imageAdapter.getmDataList().remove(pos);
            imageAdapter.setItems(imageAdapter.getmDataList());
        });
        imageAdapter.setOnItemClickListener((itemView, index) -> {
            ImageView imageView = itemView.findViewById(R.id.iv_pic);

            List<Object> urlList = new ArrayList<>();
            for (String url : pathList) {
                urlList.add(url);
            }
            new XPopup.Builder(mContext).asImageViewer(imageView, index, urlList, (popupView, position1) -> {
            }, new SmartGlideImageLoader()).show();

        });
*/

//        rcvImgs.setOverScrollMode(View.OVER_SCROLL_NEVER);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
//        rcvImgs.setLayoutManager(gridLayoutManager);
//        rcvImgs.setAdapter(imageAdapter);
    }


    /**
     * 关注状态
     */
    private void showFollow() {
        String pUserId = mModel.getUserId();

        if (UserManager.getInstance().isMyself(pUserId)) {
            btFollow.setVisibility(View.GONE);

            btnFloatFollow.setVisibility(View.GONE);
        } else {
            btFollow.setVisibility(View.VISIBLE);
            btnFloatFollow.setVisibility(View.VISIBLE);
            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
            int followStatus = mModel.getFollowStatus();

            boolean isFollow = DiscoValueFormat.isFollow(followStatus);
            btFollow.setStatus(isFollow);
            btnFloatFollow.setStatus(isFollow);
        }
    }

    /**
     * 展示地址和圈子
     */
    private void showAddressAndCircle() {
        String issueCity = mModel.getIssueCity();
        String issueAddress = mModel.getIssueAddress();
        String issueLatitude = mModel.getIssueLatitude();
        String issueLongitude = mModel.getIssueLongitude();

        List<TagFlowModel> tagList = new ArrayList<>();

        if (TextUtils.isEmpty(issueLatitude) && TextUtils.isEmpty(issueLongitude)) {
        } else {
            String address = "";
            if (!TextUtils.isEmpty(issueCity)) {
                address = TextUtils.isEmpty(issueAddress) ? issueCity : issueCity + "." + issueAddress;
            } else if (!TextUtils.isEmpty(issueAddress)) {
                address = issueAddress;
            }

            if (!TextUtils.isEmpty(address)) {
                TagFlowModel addressTag = new TagFlowModel(TagFlowModel.ADDRESS, "0", address, R.mipmap.ic_address);
                addressTag.setIssueLatitude(issueLatitude);
                addressTag.setIssueLongitude(issueLongitude);

                tagList.add(addressTag);

            }
        }

        if (contentType == 3) {
            List<TopicModel> topicList = mModel.getTopicList();//话题
            if (null != topicList && topicList.size() > 0) {
                for (TopicModel topic : topicList) {
                    if (topic.getDeleted() != 1) {
                        TagFlowModel tag = new TagFlowModel(TagFlowModel.TOPIC, topic.getId(), topic.getTitle(), R.mipmap.ic_topic);

                        tagList.add(tag);
                    }
                }
            }
        }

        //获取圈子
        List<CircleModel> circleList = mModel.getCircleList();
        if (null != circleList && circleList.size() > 0) {
            for (CircleModel circleItem : circleList) {

                if (circleItem.getDeleted() != 1) {
                    TagFlowModel tag = new TagFlowModel(TagFlowModel.CIRCLE, circleItem.getId(), circleItem.getTitle(), R.mipmap.ic_circle);

                    tagList.add(tag);
                }
            }
        }

        if (tagList.size() > 0) {
            tflMomentItem.setVisibility(View.VISIBLE);
            //控制显示5个
            tflMomentItem.setAdapter(new TagAdapter<TagFlowModel>(tagList) {
                @Override
                public View getView(FlowLayout parent, int position, TagFlowModel tagItem) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow, parent, false);
                    //获取标签
                    TextView tvTagName = view.findViewById(R.id.tvTagName);
                    ImageView ivTagIcon = view.findViewById(R.id.ivTagIcon);

                    ivTagIcon.setImageResource(tagItem.getDrawable());
                    tvTagName.setText(tagItem.getName());
                    return view;
                }
            });
            tflMomentItem.setOnTagClickListener((view, pos, parent) -> {
                TagFlowModel tagFlowModel = tagList.get(pos);
                if (tagFlowModel.getType() == TagFlowModel.ADDRESS) {

                } else if (tagFlowModel.getType() == TagFlowModel.CIRCLE) {
                    ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                            .withString("id", tagFlowModel.getId()).navigation();
                } else if (tagFlowModel.getType() == TagFlowModel.TOPIC) {
                    ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                            .withString("id", tagFlowModel.getId()).navigation();
                }

                return true;
            });
        } else {
            tflMomentItem.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 内容详情
    }

    @Override
    public void handleContentErrorMsg(boolean isFirst, String err) {
        ToastUtils.showShort(err);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        switch (event.getEventName()) {
            case CommonConstant.REFRESH_CONTENT_COMMENTS_COUNT:
                current = 1;
                basePresenter.getCommentList(id, current, 1);
                break;
        }
    }

    /**
     * 显示评论弹窗
     *
     * @param contentId
     * @param userId
     */
    private void toComment(String contentId, String userId) {

        PageJumpUtil.firstIsLoginThenJump(() -> {
            CommentPopup popup = new CommentPopup(mActivity, contentId, userId, YXConfig.TYPE_MOMENT);
            new XPopup.Builder(mContext).moveUpToKeyboard(false).enableDrag(false).atView(llComment).asCustom(popup).show();
        });
    }

    @OnClick({R2.id.iconMore, R2.id.tv_comment_second,
            R2.id.ll_collect, R2.id.ll_love, R2.id.civ_avatar, R2.id.civ_user, R2.id.iconDelete,
            R2.id.tv_name, R2.id.btFollow, R2.id.ll_comment})
    public void onViewClicked(View view) {
        if (mModel == null) {
            return;
        }
        hideSoftKeyboard();
        if (view.getId() == R.id.iconMore) {
            //展示更多
            MoreActionBoard.shareMyDynamic(mActivity, mModel, (position, tp) -> {
                switch (position) {
                    case BOARD_SET_PRIVATE:
                        mModel.setPrivacyType(tp);
                        break;
                    case BOARD_SET_REPORT:
                        toReport(-1);
                        break;
                    case BOARD_SET_DELETE:
                        deleteContent();
                        break;
                    case BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == mModel) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, mModel, YXConfig.TYPE_MOMENT, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                }
            });
        } else if (view.getId() == R.id.tv_comment_second) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ////-----评论
                showInputPopup("", "", 0);
            });
        } else if (view.getId() == R.id.iconDelete) {
            isShowFloat = true;
            rlFloat.setVisibility(View.GONE);
        } else if (view.getId() == R.id.ll_comment) {
            //保持跟视频评论点击一致
            String contentId = mModel.getId();
            String userId = mModel.getUserId();
            toComment(contentId, userId);
        } else if (view.getId() == R.id.ll_collect) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toCollect();
            });
        } else if (view.getId() == R.id.ll_love) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toStar();
            });
        } else if (view.getId() == R.id.btFollow) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toFollow();
            });
        } else if (view.getId() == R.id.civ_avatar || view.getId() == R.id.tv_name) {
            String userId = mModel.getUserId();
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", userId)
                    .navigation();
        } else if (view.getId() == R.id.civ_user) {
            String userId = mModel.getUserId();
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", userId)
                    .navigation();
        }
    }

    private void toFollow() {
        int followStatus = mModel.getFollowStatus();
        boolean isFollow = DiscoValueFormat.isFollow(followStatus);

        String userId = mModel.getUserId();
        String uId = UserManager.getInstance().getUserId();
        if (uId.equals(userId)) {
            ToastUtils.showShort("不能关注自己");
            return;
        }
        if (isFollow) {
            AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
            new XPopup.Builder(mContext).asCustom(alertPopup).show();
            alertPopup.setOnConfirmListener(() -> {
                alertPopup.dismiss();
                basePresenter.cancelFollowUser(userId);
            });
            alertPopup.setOnCancelListener(alertPopup::dismiss);
        } else {
            basePresenter.followUser(userId);
        }
    }

    /**
     * 收藏或者取消收藏
     */
    private void toCollect() {
        basePresenter.updateCollectContent(id, YXConfig.TYPE_MOMENT);
    }

    /**
     * 点赞或者取消点赞
     */
    private void toStar() {
        basePresenter.updateStarContent(id, YXConfig.TYPE_MOMENT);
    }

    /**
     * 举报内容或者评论
     *
     * @param pos -1 内容
     */
    private void toReport(int pos) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            int reportType = 0;
            String contentId = "";
            String currentUserId = null;
            if (pos >= 0) {
                reportType = YXConfig.reportType.comment;
                CommentModel model = mCommentList.get(pos);
                contentId = model.getId();
                currentUserId = model.getUserId();
            } else {
                reportType = YXConfig.reportType.moment;
                contentId = mModel.getId();
                currentUserId = mModel.getUserId();
            }

            if (TextUtils.isEmpty(currentUserId) || currentUserId.equals(UserManager.getInstance().getUserId())) {
                ToastUtils.showShort(reportType == 1 ? "不能举报自己的内容" : "不能举报自己的评论");
                return;
            }

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", contentId)
                    .withInt("reportType", reportType)
                    .navigation();
        });
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        //TODO NEXT 1、跳转动态详情（话题可直接进入话题详情）先不做处理
        //TODO NEXT 2、发送者和接受者背景的显示圆角（头像处为直角）
        MomentContentMessage messageContent = new MomentContentMessage();
        messageContent.setId(mModel.getId());

        List<String> imgs = new ArrayList<>();

        contentType = mModel.getContentType();

        String content = "";
        if (contentType == 3) {
            content = YHStringUtils.pickLastFirst(mModel.getIntroduce(), mModel.getContent());
            content = YHStringUtils.getHtmlContent(content);
        } else {
            content = mModel.getContent();

            String urlJson = mModel.getContentUrl();
            if (!TextUtils.isEmpty(urlJson)) {
                Type listType = new TypeToken<ArrayList<FileResult>>() {
                }.getType();
                ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                for (FileResult fileResult : fileResults) {
                    imgs.add(fileResult.getHttpUrl());
                }
            }
        }

        messageContent.setImgs(imgs);
        messageContent.setContent(content);
        messageContent.setAuthorHeadImg(mModel.getUserPhoto());
        messageContent.setAuthorNickname(mModel.getNickName());
        messageContent.setType(YXConfig.TYPE_MOMENT);


        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            tvTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);

                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType type = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, type, messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(YXConfig.TYPE_MOMENT));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, type, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }

    @Override
    public void handleContentDetailSuccess(boolean isFirst, MomentListModel model) {
//        if (model != null && !model.isView() && model.getPrivacyType() != 0) {
//            ToastUtils.showShort("内容已经删除");
//            onBackPressed();
//            return;
//        }
        llContent.setVisibility(View.VISIBLE);
        if (model != null) {
            mModel = model;

            mAdapter.setPublishUserId(mModel.getUserId());

            String contentUrl = model.getContentUrl();
            String createdTime = model.getCreatedTime();
            String userPhoto = model.getUserPhoto();
            int fabulousCount = model.getFabulousCount();
            int collectCount = model.getCollectCount();
            String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());
            int replyCount = model.getReplyCount();
            String userId = model.getUserId();

            contentType = mModel.getContentType();
            if (contentType == 3) {
//                rcvImgs.setVisibility(View.GONE);
                flBannerImgs.setVisibility(View.GONE);

                rlUserInfo.setVisibility(View.GONE);
                llTopContent.setVisibility(View.VISIBLE);
                tvContent.setVisibility(View.GONE);

                tvTitle.setText(YHStringUtils.value(mModel.getContentTitle()));

                //阅读 46万+
                String timeDesc = YXTimeUtils.getFriendlyTimeAtContent(createdTime) + "\u3000阅读\u3000" + YHStringUtils.quantityFormat(mModel.getPageViewCount());
                tvTimeDesc.setText(timeDesc);//

                if (TextUtils.isEmpty(mModel.getIntroduce())) {
                    tvIntrduce.setVisibility(View.GONE);
                    tv_profile.setVisibility(View.GONE);
                } else {
                    tvIntrduce.setVisibility(View.VISIBLE);
                    tv_profile.setVisibility(View.VISIBLE);
                    tvIntrduce.setText("导语：" + YHStringUtils.value(mModel.getIntroduce()));

                    tv_profile.setVisibility(View.VISIBLE);
                    tv_profile.setText(YHStringUtils.value(mModel.getIntroduce()));
                }

                areTextView.fromHtml(YHStringUtils.value(mModel.getContent()));
            } else {
//                rcvImgs.setVisibility(View.VISIBLE);

                flBannerImgs.setVisibility(View.VISIBLE);

//                rlUserInfo.setVisibility(View.VISIBLE);//内容上面的个人信息展示被废弃了
                llTopContent.setVisibility(View.GONE);
                tvContent.setVisibility(View.VISIBLE);
                showContentText();

                //图片展示
                if (!TextUtils.isEmpty(contentUrl)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(contentUrl, listType);
                    pathList.clear();

                    for (FileResult fileResult : fileResults) {
                        String imgUrl = fileResult.getHttpUrl();
                        //预加载
                        Glide.with(mContext)
                                .load(imgUrl)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .preload();

                        pathList.add(imgUrl);
                    }

                    if (!pathList.isEmpty()) {
//                        rcvImgs.setVisibility(View.VISIBLE);
//                        imageAdapter.setItems(pathList);

                        int imgSize = pathList.size();
                        flBannerImgs.setVisibility(View.VISIBLE);

                        tvBannerIndex.setVisibility(imgSize > 1 ? View.VISIBLE : View.GONE);
                        tvBannerIndex.setText("1/" + imgSize);

                        bannerImgs.setVisibility(View.VISIBLE);
                        String imgUrl = pathList.get(0);

                        Glide.with(mContext).asBitmap().load(ImageLoaderUtil.formatUrl(imgUrl)).into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                                int width = bitmap.getWidth();
                                int height = bitmap.getHeight();

                                float showWidth = DisplayUtils.getScreenWidth(mContext);

                                //此处后续需要根据图片的原始比例作为基础，然后取最接近的一个比例
                                float rate = DisplayUtils.getImageRate(width, height);
                                float showHeight = showWidth / rate;

                                bannerImgs.setLayoutParams(new RelativeLayout.LayoutParams((int) showWidth, (int) showHeight));
                                bannerImgs.isAutoLoop(false);

//                                bannerImgs.setIndicator(indicator, false);
//                                bannerImgs.setIndicatorSelectedWidth(BannerUtils.dp2px(15));

                                bannerImgs.setAdapter(new YXDiscoverImageAdapter(pathList, mContext))
                                        .setIndicator(indicator, false)
                                        .setIndicatorNormalColor(ContextCompat.getColor(mContext, size > 1 ? R.color.color_cbcbcb : R.color.transparent))
                                        .setIndicatorSelectedColor(ContextCompat.getColor(mContext, size > 1 ? R.color.theme : R.color.transparent))
                                        .setIndicatorWidth(DisplayUtils.dip2px(mContext, 4), DisplayUtils.dip2px(mContext, 6))
                                        .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 8)));
                            }
                        });

                        bannerImgs.addOnPageChangeListener(new OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                tvBannerIndex.setText((position + 1) + "/" + imgSize);
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                    } else {
//                        rcvImgs.setVisibility(View.GONE);

                        flBannerImgs.setVisibility(View.GONE);
                    }
                } else {
//                    rcvImgs.setVisibility(View.GONE);
                    flBannerImgs.setVisibility(View.GONE);
                }
            }

            showFollow();

            tvName.setText(!TextUtils.isEmpty(nickName) ? nickName : "");
            tvUserName.setText(!TextUtils.isEmpty(nickName) ? nickName : "");
            tvFloatUserName.setText(!TextUtils.isEmpty(nickName) ? nickName : "");
//            tvFloatUserSign.setText("-");

            tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime));

            tvCollect.setText(collectCount > 0 ? YHStringUtils.quantityFormat(collectCount) : "收藏");
            tvLove.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");

            tvTotalComment.setText("评论 " + replyCount);
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

            showNoDataView(replyCount);

            ivCollect.setSelected(model.isCollect());
            ivCollect.setText(model.isCollect() ? "\ue760" : "\ue742");

            ivLove.setSelected(model.isFabulous());
            ivLove.setText(model.isFabulous() ? "\ue772" : "\ue71b");

            showAddressAndCircle();

            if (!TextUtils.isEmpty(userPhoto)) {
                ImageLoaderUtil.loadImgHeadCenterCrop(civUser, userPhoto, R.drawable.place_holder);
                ImageLoaderUtil.loadImgHeadCenterCrop(civAvatar, userPhoto, R.drawable.place_holder);
                ImageLoaderUtil.loadImgHeadCenterCrop(civFloatUser, userPhoto, R.drawable.place_holder);
            } else {
                civAvatar.setImageResource(R.drawable.place_holder);
                civUser.setImageResource(R.drawable.place_holder);
                civFloatUser.setImageResource(R.drawable.place_holder);
            }
        }

        //获取这个人的详情
        if (UserManager.getInstance().isLogin()) {
            basePresenter.getUserDetail(mModel.getUserId());
        }
        //增加图片浏览量
        basePresenter.addViewCount(YXConfig.TYPE_MOMENT, id);
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        String introduction = userInfo.getIntroduction();
        if (TextUtils.isEmpty(introduction)) {
            tvFloatUserSign.setVisibility(View.GONE);
        } else {
            tvFloatUserSign.setVisibility(View.VISIBLE);
            tvFloatUserSign.setText(introduction);
        }
    }

    private void showContentText() {
        //文本内容展示
        String content = mModel.getContent();
        if (!TextUtils.isEmpty(content)) {
            List<FriendModel> nameList = mModel.getUserList();

            List<TopicModel> topicList = new ArrayList<>();//存在的话题

            List<TopicModel> topicListItem = mModel.getTopicList();//话题
            if (null != topicListItem && topicListItem.size() > 0) {
                for (TopicModel topic : topicListItem) {
                    if (topic.getDeleted() != 1) {
                        topicList.add(topic);
                    }
                }
            }

            //直接使用RichTextView
            SpanAtUserCallBack spanAtUserCallBack = new SpanAtUserCallBack() {
                @Override
                public void onClick(View view, FriendModel userModel) {
                    if (view instanceof TextView) {
                        ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                    }
                    ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", userModel.getUserId()).navigation();
                }
            };

            SpanTopicCallBack spanTopicCallBack = (view, topicModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                        .withString("id", topicModel.getId()).navigation();
            };

            tvContent.setOnTouchListener(new CustomTextViewTouchListener(
                    v -> {

                    }
            ));

            content = YHStringUtils.firstAtAddSpace(content);
            tvContent.setSpanAtUserCallBackListener(spanAtUserCallBack);
            tvContent.setSpanTopicCallBackListener(spanTopicCallBack);
            //所有配置完成后才设置text
            tvContent.setRichText(content, null != nameList ? nameList : new ArrayList<>(), topicList);

            tvContent.setVisibility(View.VISIBLE);
        } else {
            tvContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleErrorMsg(String msg) {

        ToastUtils.showShort(msg);
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int totalSum) {

        if (current == 1) {
            mList.clear();
        }
        if (list != null && !list.isEmpty()) {
            mList.addAll(list);
        }
//不要取评论列表中的总数
//        tvTotalComment.setText("评论 " + totalSum);
//        tvComment.setText(totalSum > 0 ? YHStringUtils.quantityFormat(totalSum) : "评论");
        showNoDataView(totalSum);

        mCommentList = toBuildCommentList();

        mAdapter.setItems(mCommentList);

//        if (null != mCommentList && mCommentList.size() > 0 && current == 1 && !isFirstLoad) {
//            rvComment.postDelayed(() -> {
//                int totalNumTop = tvTotalComment.getBottom();//获取评论数的顶部//+ ScreenUtil.dip2px(mContext, 56)
//
//                totalNumTop = totalNumTop + rvComment.getChildAt(0).getBottom();
//
//                rvComment.scrollToPosition(0);//第一个
//            }, 200);
//        }

        isFirstLoad = false;
    }

    private void showNoDataView(int sum) {
        if (sum > 0) {
            llNodata.setVisibility(View.GONE);
            rvComment.setVisibility(View.VISIBLE);
        } else {
            llNodata.setVisibility(View.VISIBLE);
            rvComment.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleChildCommentList(ListResult<CommentModel> listResult) {
        if (listResult != null) {
            int total = listResult.getTotal();
            CommentModel model = mCommentList.get(currentPosition);
            int groupPosition = model.getGroupPosition();
            mList.get(groupPosition).setChildCount(total);
            List<CommentModel> list = listResult.getRecords();
            if (list != null && !list.isEmpty()) {
                mList.get(groupPosition).setChildrens(list);
            }
            mCommentList = toBuildCommentList();
            mAdapter.setItems(mCommentList);
        }
    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");

            int replyCount = mModel.getReplyCount() + 1;
            mModel.setReplyCount(replyCount);

            tvTotalComment.setText("评论 " + replyCount);
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

            showNoDataView(replyCount);

            if (!isReply) {
                current = 1;
                basePresenter.getCommentList(id, current, 1);
            } else {
                CommentModel preModel = mCommentList.get(currentPosition);
                int groupPosition = preModel.getGroupPosition();
                int type = preModel.getType();
                if (type == 0) {
                    model.setType(1);
                } else {
                    preModel.setType(1);
                    model.setType(type);

                    //设置回复
                    model.setReplyStatus(1);
                    String preNick = YHStringUtils.pickName(preModel.getNickName(), preModel.getFriendRemark());
                    model.setReplyName(preNick);
                }
                model.setGroupPosition(groupPosition);

                UserInfo userInfo = UserManager.getInstance().getUserInfo();

                String nickName = userInfo.getNickName();
                String userPhoto = userInfo.getImg();
                String userId = userInfo.getId();
                if (TextUtils.isEmpty(model.getUserPhoto())) {
                    model.setUserPhoto(userPhoto);
                }
                if (TextUtils.isEmpty(model.getNickName())) {
                    model.setNickName(nickName);
                }
                if (TextUtils.isEmpty(model.getUserId())) {
                    model.setUserId(userId);
                }

                mCommentList.add(currentPosition + 1, model);
                mAdapter.setItems(mCommentList);
            }
        }
    }

    @Override
    public void updateCollectContentSuccess() {
        int collectCount = mModel.getCollectCount();
        mModel.setCollect(!mModel.isCollect());
        mModel.setCollectCount(mModel.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));
        ivCollect.setSelected(mModel.isCollect());
        ivCollect.setText(mModel.isCollect() ? "\ue760" : "\ue742");

        tvCollect.setText(YHStringUtils.quantityFormat(mModel.getCollectCount()));

        EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_COLLECT, mModel));
    }

    @Override
    public void updateStarContentSuccess() {
        int fabulousCount = mModel.getFabulousCount();
        boolean isFabulous = mModel.isFabulous();
        mModel.setFabulousCount(!isFabulous ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mModel.setFabulous(!isFabulous);

        ivLove.setSelected(!isFabulous);
        ivLove.setText(!isFabulous ? "\ue772" : "\ue71b");

        tvLove.setText(mModel.getFabulousCount() > 0 ? YHStringUtils.quantityFormat(mModel.getFabulousCount()) : "点赞");

        EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_LOVE, mModel));
    }

    @Override
    public void updateStarCommentSuccess() {
        CommentModel model = mCommentList.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setIsFabulous(!model.isIsFabulous());

        model.setFabulousCount(model.isIsFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowUserSuccess() {
        int followStatus = mModel.getFollowStatus();
        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
        if (isFollow) {
            mModel.setFollowStatus(DiscoValueFormat.unFollowAction(followStatus));
        } else {
            mModel.setFollowStatus(DiscoValueFormat.followAction(followStatus));
        }

        showFollow();

        if (isFollow) {
            // 取消关注
            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_UNFOLLOW, mModel.getUserId()));
        } else {
            // 关注
            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_FOLLOW, mModel.getUserId()));
        }
    }

    private void showInputPopup(String commentId, String nickName, int childType) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!TextUtils.isEmpty(commentId)) {
                isReply = true;
            } else {
                isReply = false;
            }

            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);
            popup.setOnSendCommentListener((comment, ct) -> {//发送评论
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(id);
                commentForm.setParentCommentId(commentId);

                if (ct != 0) {
                    commentForm.setReplyType(2);
                } else {
                    commentForm.setReplyType(1);
                }

                commentForm.setUserId(UserManager.getInstance().getUserId());
                basePresenter.publishComment(commentForm, YXConfig.TYPE_MOMENT);
            }, childType);
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    private List<CommentModel> toBuildCommentList() {
        List<CommentModel> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            CommentModel model = mList.get(i);
            int resultType = model.getResultType();
            if (resultType == 1) {
                //这是父评论
                model.setType(0);
                model.setGroupPosition(i);
                model.setParentChildCount(0);

                list.add(model);
                List<CommentModel> childrenList = model.getChildrens();
                int childCount = model.getChildCount();
                int count = 0;
                /*for (CommentModel childrenComment : childrenList) {
                    count = count + 1;
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());
                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }*/

                int childSize = childrenList.size();
                for (int j = 0; j < childSize; j++) {
                    count = count + 1;
                    CommentModel childrenComment = childrenList.get(j);
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());

                    if (j == 0) {
                        if (childSize == 1) {
                            childrenComment.setChildType(3);
                        } else {
                            childrenComment.setChildType(1);
                        }
                    } else if (j == childSize - 1) {
                        childrenComment.setChildType(2);
                    } else {
                        childrenComment.setChildType(0);
                    }

                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }

            } else if (resultType == 2) {//广告
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                //点击非输入框区域，清空输入框焦点
                hideSoftKeyboard();
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v instanceof EditText) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private void deleteContent() {
        basePresenter.deleteContent(mModel.getId());
    }

    @Override
    public void handleDeleteContentSuccess() {
        onBackPressed();//删除自己的内容，返回上一级页面

        EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_DELETE_CONTENT, fromClassName));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
