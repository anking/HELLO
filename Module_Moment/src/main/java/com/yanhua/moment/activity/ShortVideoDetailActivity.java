package com.yanhua.moment.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.CustomTextViewTouchListener;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.listener.SpanTopicCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.adapter.RecyclerItemBaseHolder;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.CommentInputTemp;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.TagFlowModel;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.CommentInputPopup;
import com.yanhua.common.widget.CommentPopup;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectPartShadowPopupView;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.common.widget.TXSampleCoverVideo;
import com.yanhua.common.widget.VideoFollowStatusView;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.StatusBarUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.ChannelPageAdapter;
import com.yanhua.moment.presenter.ContentDetailPresenter;
import com.yanhua.moment.presenter.contract.ContentDetailContract;
import com.yanhua.rong.msgprovider.MomentVideoMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * 社交短视频详情页
 *
 * @author Administrator
 */
@Route(path = ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
public class ShortVideoDetailActivity extends BaseMvpActivity<ContentDetailPresenter> implements ContentDetailContract.IView {

    @Autowired
    String fromClassName;
    @BindView(R2.id.llAddress)
    LinearLayout llAddress;
    @BindView(R2.id.tvAddress)
    TextView tvAddress;

    @BindView(R2.id.vvp_detail)
    ViewPager2 vvpDetail;
    private List<MomentListModel> mContentList;
    private int mCurrentPosition = 0;
    private int current = 1;
    private int size = 20;
    private String freshTime;
    /**
     * SDK播放器以及配置
     */
    private VideoViewPagerAdapter mPagerAdapter;

    @Autowired
    MomentListModel contentModel;

    @Autowired(name = "contentId", desc = "内容id")
    String contentId;

    @Autowired
    int module;
    @Autowired
    double latitude;
    @Autowired
    double longitude;
    @Autowired
    String city;
    @Autowired
    String categoryId;
    @Autowired
    int discoBest;

    private AliIconFontTextView mIvCollect;
    private AliIconFontTextView mIvLove;
    private TextView mTvCollect;
    private TextView mTvLove;
    private TextView mTvComment;
    private VideoFollowStatusView mBtnFollow;

    @Override
    protected void creatPresent() {
        basePresenter = new ContentDetailPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_short_video_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        GSYVideoManager.instance().setNeedMute(false);

        current = 1;
        mCurrentPosition = 0;
        mContentList = new ArrayList<>();

        mPagerAdapter = new VideoViewPagerAdapter(this, mContentList);

        vvpDetail.setOrientation(ViewPager2.ORIENTATION_VERTICAL);
        vvpDetail.setAdapter(mPagerAdapter);

        //切换
        vvpDetail.setPageTransformer((page, position) -> {
            ViewGroup viewGroup = (ViewGroup) page;

            mIvCollect = viewGroup.findViewById(R.id.iv_collect);
            mIvLove = viewGroup.findViewById(R.id.iv_love);
            mTvCollect = viewGroup.findViewById(R.id.tv_collect);
            mTvLove = viewGroup.findViewById(R.id.tv_love);
            mTvComment = viewGroup.findViewById(R.id.tv_comment);
            mBtnFollow = viewGroup.findViewById(R.id.btn_follow);
        });

        CommentInputTemp.setNick("");
        CommentInputTemp.setContent("");

        vvpDetail.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if (changeData) {
                    mPagerAdapter.notifyDataSetChanged();
                    changeData = false;
                }

                mCurrentPosition = position;

                //大于0说明有播放
                int playPosition = GSYVideoManager.instance().getPlayPosition();
                if (playPosition >= 0) {
                    //对应的播放列表TAG
                    if (GSYVideoManager.instance().getPlayTag().equals(RecyclerItemNormalHolder.TAG)
                            && (position != playPosition)) {
                        vvpDetail.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                playPosition(mCurrentPosition);
                            }
                        }, 0);
                    }
                }

                //避免只有一条也提示
                if (mContentList.size() != 1 && mContentList.size() == (mCurrentPosition + 1)) {
                    if (NetworkUtils.isConnected()) {
                        current = current + 1;
                        getDataContentList(current);
                    } else {
                        ToastUtils.showShort("网络异常");
                    }
                }
            }
        });

        StatusBarUtil.setImmersiveStatusBar(this, false);//设置为白色字体
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        if (discoBest == 1) {
            String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
            tvAddress.setText(TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress);
//            llAddress.setVisibility(View.VISIBLE);

            if (TextUtils.isEmpty(contentId)) {
//                basePresenter.getDiscoBestCityList();//
                if (contentModel != null) {
                    basePresenter.getContentUserDetail(contentModel.getUserId());
                    basePresenter.getDiscoContentDetail(contentModel.getId());
                }
            } else {
                basePresenter.getDiscoContentDetail(contentId);
            }
        } else {
//            llAddress.setVisibility(View.GONE);
            if (contentModel != null) {
                basePresenter.getContentUserDetail(contentModel.getUserId());
                basePresenter.getContentDetail(true, contentModel.getId());
            } else {
                // 通过接口获取详情
                basePresenter.getContentDetail(true, contentId);
            }
        }
    }

    @Override
    public void handleContentErrorMsg(boolean isFirst, String err) {
        ToastUtils.showShort(err);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        brokenEvent = false;

        GSYVideoManager.onPause();

        GSYVideoManager.instance().setNeedMute(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.instance().setNeedMute(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        GSYVideoManager.instance().setNeedMute(true);
        GSYVideoManager.releaseAllVideos();
    }

    private void playPosition(int position) {
        MomentListModel model = mContentList.get(mCurrentPosition);
        if (model != null) {
            // 增加访问量
            if (null != basePresenter) {
                basePresenter.addViewCount(discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT, model.getId());
            }
        }

        RecyclerView.ViewHolder viewHolder = ((RecyclerView) vvpDetail.getChildAt(0)).findViewHolderForAdapterPosition(position);
        if (viewHolder != null) {
            RecyclerItemNormalHolder recyclerItemNormalHolder = (RecyclerItemNormalHolder) viewHolder;
            recyclerItemNormalHolder.getPlayer().startPlayLogic();
        }
    }

    private void getDataContentList(int currentpage) {
        current = currentpage;
        if (discoBest == 1) {
//            String city = tvAddress.getText().toString().trim();
//            //电音集锦
            basePresenter.getDiscoBestList("", city, current, size);
        } else {
            basePresenter.getContentList("", 4, current, size);
        }
    }

    @Override
    public void handleDeleteContentSuccess() {
        onBackPressed();//删除自己的内容，返回上一级页面

        EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_DELETE_CONTENT, fromClassName));
    }


    @OnClick({R2.id.tv_left, R2.id.iconMore, R2.id.llAddress})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.tv_left) {
            onBackPressed();
        } else if (view.getId() == R.id.iconMore) {
            toMoreShare();
        } else if (view.getId() == R.id.llAddress) {
            //展示更多城市
            showConditionPopup();
        }
    }

    private void toMoreShare() {
        //展示更多
        MomentListModel clickItem = mContentList.get(mCurrentPosition);
        clickItem.setDiscoBest(discoBest);

        MoreActionBoard.shareMyDynamic(mActivity, clickItem, discoBest, (position, tp) -> {
            switch (position) {
                case ShareBoardPopup.BOARD_SET_PRIVATE:
                    contentModel.setPrivacyType(tp);
                    break;
                case ShareBoardPopup.BOARD_SET_REPORT:
                    toReport();
                    break;
                case ShareBoardPopup.BOARD_SET_DELETE:
                    deleteContent();
                    break;
                case ShareBoardPopup.BOARD_SHARE_APP:
                    //.isLightStatusBar(true)不会改变状态栏的颜色
                    if (null == contentModel) return;
                    SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, contentModel, discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                    new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                            .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                            .autoFocusEditText(false)
                            .moveUpToKeyboard(false).asCustom(sharePopup).show();
                    break;
            }
        });
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        MomentVideoMessage messageContent = new MomentVideoMessage();
        messageContent.setId(contentModel.getId());

        //封面
        String urlJson = contentModel.getContentUrl();
        String coverImage = null;
        if (!TextUtils.isEmpty(urlJson)) {
            Type listType = new TypeToken<ArrayList<FileResult>>() {
            }.getType();
            ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

            FileResult fileResult = fileResults.get(0);
            coverImage = fileResult.getHttpUrl();

            messageContent.setHeight(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());
            messageContent.setWidth(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());

            messageContent.setCoverUrl(coverImage);
        }

        String content = YHStringUtils.pickLastFirst(contentModel.getIntroduce(), contentModel.getContent());
        content = YHStringUtils.getHtmlContent(content);//去掉html标签

        if (!TextUtils.isEmpty(content)) {
            messageContent.setContent(content);
        } else {
            messageContent.setContent(null);
        }

        messageContent.setAuthorHeadImg(contentModel.getUserPhoto());
        messageContent.setAuthorNickname(contentModel.getNickName());

        int type = discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT;
        messageContent.setType(type);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            vvpDetail.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(type));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }

    private void deleteContent() {
        basePresenter.deleteContent(contentModel.getId());
    }

    /**
     * 举报内容
     *
     * @param
     */
    private void toReport() {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            MomentListModel model = mContentList.get(mCurrentPosition);
            String contentId = model.getId();
            String currentUserId = model.getUserId();

            if (TextUtils.isEmpty(currentUserId) || currentUserId.equals(UserManager.getInstance().getUserId())) {
                ToastUtils.showShort("不能举报自己的内容");
                return;
            }

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", contentId)
                    .withInt("reportType", discoBest == 1 ? YXConfig.reportType.disco_best : YXConfig.reportType.moment)
                    .navigation();
        });
    }


    /**
     * 显示评论弹窗
     *
     * @param contentId
     * @param userId
     */
    private void toComment(String contentId, String userId) {

        PageJumpUtil.firstIsLoginThenJump(() -> {

            CommentPopup popup = new CommentPopup(mActivity, contentId, userId, discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT);
            new XPopup.Builder(mContext).moveUpToKeyboard(false).enableDrag(false).asCustom(popup).show();
        });
    }

    /**
     * 显示输入评论
     *
     * @param contentId
     * @param commentId
     * @param nickName
     */
    private void showInputPopup(String contentId, String commentId, String nickName) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);
            popup.setOnSendCommentListener((comment, ct) -> {
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(contentId);
                commentForm.setParentCommentId("");
                commentForm.setReplyType(1);
                commentForm.setUserId(UserManager.getInstance().getUserId());
                basePresenter.publishComment(commentForm, discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT);
            }, 0);
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    /**
     * 关注或者取消关注用户
     */
    private void toFollow() {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            MomentListModel model = mContentList.get(mCurrentPosition);
            String userId = model.getUserId();
            String uId = UserManager.getInstance().getUserId();
            if (uId.equals(userId)) {
                ToastUtils.showShort("不能关注自己");
                return;
            }
            boolean isFollow = DiscoValueFormat.isFollow(model.getFollowStatus());

            if (isFollow) {
                AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    basePresenter.cancelFollowUser(userId);
                });
                alertPopup.setOnCancelListener(alertPopup::dismiss);
            } else {
                basePresenter.followUser(userId);
            }
        });
    }

    /**
     * 收藏或者取消收藏
     */
    private void toCollect(String contentId) {

        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.updateCollectContent(contentId, discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT);
        });
    }

    /**
     * 点赞或者取消点赞
     */
    private void toStar(String contentId) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.updateStarContent(contentId, discoBest == 1 ? YXConfig.TYPE_BEST : YXConfig.TYPE_MOMENT);
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        switch (event.getEventName()) {
            case CommonConstant.REFRESH_CONTENT_COMMENTS_COUNT:
                contentModel = mContentList.get(mCurrentPosition);

                int replyCount = contentModel.getReplyCount() + 1;
                contentModel.setReplyCount(replyCount);
                mTvComment.setText(String.valueOf(replyCount));
                EventBus.getDefault().post(contentModel);
                break;
            case CommonConstant.ACTION_FOLLOW: {
                if (brokenEvent) {
                    return;
                }

                MomentListModel model = mContentList.get(mCurrentPosition);

                model.setFollowStatus(DiscoValueFormat.followAction(model.getFollowStatus()));

                mBtnFollow.setSelected(true);
                mBtnFollow.setStatus(true);

                changeRelationShipStatus(model);

                break;
            }
            case CommonConstant.ACTION_UNFOLLOW: {
                if (brokenEvent) {
                    return;
                }
                MomentListModel model = mContentList.get(mCurrentPosition);
                model.setFollowStatus(DiscoValueFormat.unFollowAction(model.getFollowStatus()));

                mBtnFollow.setSelected(false);
                mBtnFollow.setStatus(false);

                changeRelationShipStatus(model);

                break;
            }
        }
    }

    @Override
    public void handleDiscoContentDetail(MomentListModel model) {
        if (!mContentList.contains(model)) {
            if (null != contentModel) {
                mContentList.add(contentModel);
            } else {
                mContentList.add(model);
            }
        }

        getDataContentList(1);

        //延迟播放
        vvpDetail.postDelayed(new Runnable() {
            @Override
            public void run() {
                playPosition(0);
            }
        }, 1000);

        //单个元素
        mPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void handleContentDetailSuccess(boolean isFirst, MomentListModel model) {
        if (!mContentList.contains(model)) {
            if (null != contentModel) {
                mContentList.add(contentModel);
            } else {
                mContentList.add(model);
            }
        }

        if (isFirst) {
            getDataContentList(1);

            //延迟播放
            vvpDetail.postDelayed(new Runnable() {
                @Override
                public void run() {
                    playPosition(0);
                }
            }, 1000);
        }

        //单个元素
        mPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void handleErrorMsg(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            ToastUtils.showShort(msg);
        }
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int sum) {

    }

    @Override
    public void handleChildCommentList(ListResult<CommentModel> listResult) {

    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");
            contentModel = mContentList.get(mCurrentPosition);

            int replyCount = contentModel.getReplyCount() + 1;
            contentModel.setReplyCount(replyCount);
//            String comment = mTvComment.getText().toString();
//            comment = comment.equals("评论") ? "0" : replyCount;

            mTvComment.setText(String.valueOf(replyCount));
            EventBus.getDefault().post(contentModel);
        }
    }

    @Override
    public void updateCollectContentSuccess() {
        MomentListModel model = mContentList.get(mCurrentPosition);
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));
        mTvCollect.setText(model.getCollectCount() > 0 ? YHStringUtils.quantityFormat(model.getCollectCount()) : "收藏");
        mIvCollect.setSelected(model.isCollect());

        EventBus.getDefault().post(model);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mContentList.get(mCurrentPosition);
        int fabulousCount = model.getFabulousCount();
        boolean isFabulous = model.isFabulous();
        model.setFabulous(!isFabulous);
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mTvLove.setText(model.getFabulousCount() > 0 ? YHStringUtils.quantityFormat(model.getFabulousCount()) : "点赞");

        mIvLove.setSelected(model.isFabulous());

        if (!isFabulous) {
            AnimatorSet animSet = new AnimatorSet();

            ObjectAnimator animatorX = ObjectAnimator.ofFloat(mIvLove, "scaleX", 0.6f, 0.7f, 0.8f, 0.9f, 1f, 1.1f, 1.2f, 1.1f, 1.f);
            ObjectAnimator animatorY = ObjectAnimator.ofFloat(mIvLove, "scaleY", 0.6f, 0.7f, 0.8f, 0.9f, 1f, 1.1f, 1.2f, 1.1f, 1.f);
            animSet.play(animatorX).with(animatorY);
            animSet.setDuration(500l);
            animSet.start();
        }

        EventBus.getDefault().post(model);
    }

    @Override
    public void updateStarCommentSuccess() {

    }

    //控制本页面的事件将状态值改变
    private boolean brokenEvent;

    @Override
    public void updateFollowUserSuccess() {
        brokenEvent = false;

        MomentListModel model = mContentList.get(mCurrentPosition);
        int status = model.getFollowStatus();

        //请求接口以前的状态
        boolean isFollow = DiscoValueFormat.isFollow(status);//2 || followStatus == 4;
        if (isFollow) {
            model.setFollowStatus(DiscoValueFormat.unFollowAction(status));// 1 3
        } else {
            model.setFollowStatus(DiscoValueFormat.followAction(status));// 2 4
        }

        mBtnFollow.setSelected(!isFollow);
        mBtnFollow.setStatus(!isFollow);

        if (isFollow) {
            brokenEvent = true;

            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_UNFOLLOW, model.getUserId()));
        } else {
            brokenEvent = true;

            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_FOLLOW, model.getUserId()));
        }

        changeRelationShipStatus(model);
    }


    private boolean changeData;

    private void changeRelationShipStatus(MomentListModel model) {
        //将同一个用户的关系设置
        if (mContentList != null && !mContentList.isEmpty()) {
            Iterator<MomentListModel> iterator = mContentList.iterator();
            while (iterator.hasNext()) {
                MomentListModel modelTemp = iterator.next();
                if (modelTemp.getUserId().equals(model.getUserId())) {
                    modelTemp.setFollowStatus(model.getFollowStatus());
                }
            }

            changeData = true;
        }
    }

    @Override
    public void handleDiscoBestListSuccess(List<MomentListModel> list) {
        if (list != null && !list.isEmpty()) {
            mContentList.addAll(list);
            mPagerAdapter.setListData(mContentList);
            vvpDetail.setCurrentItem(mCurrentPosition);

            if (current == 1) {
                //延迟播放
                vvpDetail.postDelayed(() -> playPosition(0), 1000);
            }
        } else {
            if (mContentList.size() > 1) {//只有一个的话
                ToastUtils.showShort("已经没有更多视频了");
            }
        }
    }


    @Override
    public void handleContentListSuccess(List<MomentListModel> list) {
        if (list != null && !list.isEmpty()) {
            Iterator<MomentListModel> iterator = list.iterator();

            while (iterator.hasNext()) {
                MomentListModel model = iterator.next();
                String contentItemId = model.getId();

                if (!TextUtils.isEmpty(contentItemId) && (null != contentId && contentItemId.equals(contentId) || null != contentModel && contentItemId.equals(contentModel.getId()))) {
                    iterator.remove();
                }
            }

            mContentList.addAll(list);
            mPagerAdapter.setListData(mContentList);
            vvpDetail.setCurrentItem(mCurrentPosition);
        } else {
            if (mContentList.size() > 1) {//只有一个的话
                ToastUtils.showShort("已经没有更多视频了");
            }
        }
    }

    private void toPersonalPage(String userId) {
        ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                .withString("userId", userId)
                .navigation();
    }

    @Override
    public void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        if (model != null) {
            contentModel.setFollowStatus(model.getFollowStatus());
            //获取第一个显示的关系
            mPagerAdapter.notifyDataSetChanged();
        }
    }

    //------------------------
    class VideoViewPagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private final static String TAG = "RecyclerBaseAdapter";

        private List<MomentListModel> itemDataList = null;
        private Context context = null;

        public VideoViewPagerAdapter(Context context, List<MomentListModel> itemDataList) {
            this.itemDataList = itemDataList;
            this.context = context;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.item_short_video, parent, false);
            final RecyclerView.ViewHolder holder = new RecyclerItemNormalHolder(context, v);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
            RecyclerItemNormalHolder recyclerItemViewHolder = (RecyclerItemNormalHolder) holder;
            recyclerItemViewHolder.setRecyclerBaseAdapter(this);
            recyclerItemViewHolder.onBind(position, itemDataList.get(position));
        }

        @Override
        public int getItemCount() {
            return itemDataList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        public void setListData(List<MomentListModel> data) {
            itemDataList = data;
            notifyDataSetChanged();
        }
    }

    //===========================================================
    class RecyclerItemNormalHolder extends RecyclerItemBaseHolder {
        public final static String TAG = "RecyclerView2List";
        protected Context context;
        TXSampleCoverVideo gsyVideoPlayer;

        // 头像
        CircleImageView ivAvatar;
        // 姓名
        TextView tvName;
        LinearLayout llToComment;
        RichTextView tvContent;

        AliIconFontTextView ivCollect;
        AliIconFontTextView ivLove;
        TextView tvCollect;
        TextView tvLove;
        TextView tvComment;
        VideoFollowStatusView btnFollow;

        LinearLayout llMore;

        LinearLayout llComment;
        LinearLayout llCollect;
        LinearLayout llLove;
        ImageView ivShowFull;
        ImageView ivShowFullClose;
        LinearLayout ll_info;
        FrameLayout fl_info;

        LinearLayout llBottomInfo;

        TextView tvDate;
        TextView tvClose;
        NestedScrollView nscContent;
        TagFlowLayout tflMomentItem;

        RelativeLayout llBottom;

        public RecyclerItemNormalHolder(Context context, View view) {
            super(view);
            this.context = context;
            gsyVideoPlayer = view.findViewById(R.id.video_item_player);

            // 头像
            ivAvatar = view.findViewById(R.id.civ_user);
            // 姓名
            tvName = view.findViewById(R.id.tv_user);
            llToComment = view.findViewById(R.id.llToComment);
            tvContent = view.findViewById(R.id.tv_content);

            ivCollect = view.findViewById(R.id.iv_collect);
            ivLove = view.findViewById(R.id.iv_love);
            tvCollect = view.findViewById(R.id.tv_collect);
            tvLove = view.findViewById(R.id.tv_love);
            tvComment = view.findViewById(R.id.tv_comment);

            btnFollow = view.findViewById(R.id.btn_follow);

            llMore = view.findViewById(R.id.llMore);
            llComment = view.findViewById(R.id.ll_comment);
            llCollect = view.findViewById(R.id.ll_collect);
            llLove = view.findViewById(R.id.ll_love);
            ivShowFull = view.findViewById(R.id.iv_show_full);
            ivShowFullClose = view.findViewById(R.id.iv_show_full_close);
            ll_info = view.findViewById(R.id.ll_info);
            fl_info = view.findViewById(R.id.fl_info);

            llBottomInfo = view.findViewById(R.id.ll_bottom_info);
            tflMomentItem = view.findViewById(R.id.tflMomentItem);
            nscContent = view.findViewById(R.id.nsc_content);

            tvDate = view.findViewById(R.id.tv_date);
            tvClose = view.findViewById(R.id.tv_close);

            llBottom = view.findViewById(R.id.ll_bottom);
        }

        public void onBind(final int position, MomentListModel model) {
            String contentUrl = model.getContentUrl();

            Type listType = new TypeToken<ArrayList<FileResult>>() {
            }.getType();
            ArrayList<FileResult> fileResults = GsonUtils.fromJson(contentUrl, listType);
            FileResult fileResult = fileResults.get(0);

            String coverImage = fileResult.getHttpUrl();
            String url = fileResults.get(1).getHttpUrl();//

            int width = Integer.parseInt(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());
            int height = Integer.parseInt(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());

            gsyVideoPlayer.loadCoverImage(coverImage, R.drawable.bg_tran_circle_no_stroke, width, height);
            gsyVideoPlayer.setUpLazy(url, true, null, null, "");
            //增加title
            gsyVideoPlayer.getTitleTextView().setVisibility(View.GONE);
            //设置返回键
            gsyVideoPlayer.getBackButton().setVisibility(View.GONE);
            //设置全屏
            gsyVideoPlayer.getFullscreenButton().setVisibility(View.GONE);
            gsyVideoPlayer.setRotateViewAuto(true);
            gsyVideoPlayer.setLockLand(true);
            gsyVideoPlayer.setIsTouchWiget(true);
            gsyVideoPlayer.setIsTouchWigetFull(true);
            gsyVideoPlayer.setPlayTag(TAG);
            gsyVideoPlayer.setPlayPosition(position);
            gsyVideoPlayer.setReleaseWhenLossAudio(false);
            gsyVideoPlayer.setAutoFullWithSize(true);
            gsyVideoPlayer.setLooping(true);
            gsyVideoPlayer.setShowFullAnimation(true);
            gsyVideoPlayer.setIsTouchWiget(false);
            gsyVideoPlayer.setNeedShowWifiTip(false);//是否展示WiFi提示
            //循环
            gsyVideoPlayer.setNeedLockFull(true);

            //自己的内容隐藏关注按钮
            String currentUserId = model.getUserId();
            if (TextUtils.isEmpty(currentUserId) || currentUserId.equals(UserManager.getInstance().getUserId())) {
                btnFollow.setVisibility(View.GONE);
            } else {
                btnFollow.setVisibility(discoBest == 1 ? View.GONE : View.VISIBLE);
            }

            String title = model.getContentTitle();
            String content = model.getContent();
            String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());
            String userPhoto = model.getUserPhoto();
            int fabulousCount = model.getFabulousCount();
            int collectCount = model.getCollectCount();
            int replyCount = model.getReplyCount();

            String contentId = model.getId();
            String userId = model.getUserId();

            tvCollect.setText(collectCount > 0 ? YHStringUtils.quantityFormat(collectCount) : "收藏");
            tvLove.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

            ivCollect.setSelected(model.isCollect());
            ivLove.setSelected(model.isFabulous());
            tvName.setOnClickListener(v -> {
                if (discoBest == 1) {
                    return;
                }

                toPersonalPage(userId);
            });

            nscContent.setHorizontalFadingEdgeEnabled(false);

            if (!TextUtils.isEmpty(content)) {
                showContentText(model);
            } else {
                tvContent.setVisibility(View.GONE);
                tvContent.setText("");
            }

            showAddressAndCircle(model);

            boolean isFollow = DiscoValueFormat.isFollow(model.getFollowStatus());
            btnFollow.setSelected(isFollow);
            btnFollow.setStatus(isFollow);
            btnFollow.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    toFollow();
                }
            });

            llCollect.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    toCollect(contentId);
                }
            });
            llLove.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    toStar(contentId);
                }
            });
            llComment.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    toComment(contentId, userId);
                }
            });

            llMore.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    toMoreShare();
                }
            });

            llToComment.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(1000)) {
                    showInputPopup(contentId, "", "");
                }
            });

            tvName.setText(!TextUtils.isEmpty(nickName) ? nickName : "");

            String createdTime = model.getCreatedTime();
            tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理

            if (!TextUtils.isEmpty(userPhoto)) {
                ImageLoaderUtil.loadImgHeadCenterCrop(ivAvatar, userPhoto, R.drawable.place_holder);
            } else {
                ivAvatar.setImageResource(R.drawable.place_holder);
            }


            //事件处理
            gsyVideoPlayer.setVideoAllCallBack(new GSYSampleCallBack() {
                //打开一个视频的步骤
//                onClickStartThumb===
//                onComplete===
//                onStartPrepared===
//                onPrepared===
                //开始加载，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                //点击了开始按键播放，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                //点击了空白弹出seekbar，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                @Override
                public void onClickSeekbar(String url, Object... objects) {
                }

                //点击了播放中的空白区域，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                @Override
                public void onClickBlank(String url, Object... objects) {
                }

                //点击了播放状态下的开始按键--->停止，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                @Override
                public void onClickStop(String url, Object... objects) {
                    //息屏
                    mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }

                //点击了暂停状态下的开始按键--->播放，objects[0]是title，object[1]是当前所处播放器（全屏或非全屏）
                @Override
                public void onClickResume(String url, Object... objects) {

                    //保持常亮
                    mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });

            gsyVideoPlayer.setOnDataNetPlayListener(new TXSampleCoverVideo.OnDataNetPlayListener() {
                @Override
                public void onTouchDoubleUp(MotionEvent e) {
                    if (FastClickUtil.isFastClick(1000)) {
                        boolean isFabulous = model.isFabulous();
                        if (!isFabulous) {
                            toStar(contentId);
                        }
                    }
                }

                @Override
                public void play() {
                    YXConfig.needShowNoWifi = false;
                    String currentTime = String.valueOf(System.currentTimeMillis());
                    DiscoCacheUtils.getInstance().setFirstOpenTime(currentTime);
                }

                @Override
                public void stop() {
                    onBackPressed();
                }

                @Override
                public void hideInfo(boolean isHideInfo) {
                    if (isHideInfo) {
                        ll_info.setVisibility(View.GONE);

                        ivShowFullClose.setVisibility(View.GONE);
                        ivShowFull.setVisibility(View.GONE);
                    } else {
                        ll_info.setVisibility(View.VISIBLE);

                        ivShowFullClose.setVisibility(View.GONE);
                        ivShowFull.setVisibility(View.VISIBLE);
                    }
                }
            });

            ivShowFull.setOnClickListener(view -> {
                ivShowFullClose.setVisibility(View.VISIBLE);
                ivShowFull.setVisibility(View.GONE);
                ll_info.setVisibility(View.GONE);
                fl_info.setVisibility(View.GONE);
            });

            ivShowFullClose.setOnClickListener(view -> {
                ivShowFullClose.setVisibility(View.GONE);
                ivShowFull.setVisibility(View.VISIBLE);
                ll_info.setVisibility(View.VISIBLE);
                fl_info.setVisibility(View.VISIBLE);
            });

            FrameLayout.LayoutParams contentLayoutParams = (FrameLayout.LayoutParams) nscContent.getLayoutParams();

            fl_info.setBackgroundResource(R.drawable.bg_gradient_bluer);//bg_gradient_bluer-->bg_bottom
            contentLayoutParams.rightMargin = DisplayUtils.dip2px(mContext, 42);
            contentLayoutParams.bottomMargin = 0;
            nscContent.setLayoutParams(contentLayoutParams);
            llBottomInfo.setVisibility(View.GONE);

            ll_info.setOnClickListener(v -> {
                if (llBottomInfo.getVisibility() == View.GONE) {
                    closeOrOpenDetailView(contentLayoutParams, R.drawable.bg_deep_gradient_bluer, 0, View.VISIBLE);
                } else {
                    closeOrOpenDetailView(contentLayoutParams, R.drawable.bg_gradient_bluer, 42, View.GONE);//bg_gradient_bluer-->bg_bottom
                }
            });
//
            tvContent.setOnClickListener(v -> {
                closeOrOpenDetailView(contentLayoutParams, R.drawable.bg_deep_gradient_bluer, 0, View.VISIBLE);
            });

            fl_info.setOnClickListener(v -> {
                closeOrOpenDetailView(contentLayoutParams, R.drawable.bg_gradient_bluer, 42, View.GONE);//bg_gradient_bluer-->bg_bottom
            });

            tvClose.setOnClickListener(v -> {//s收起
                closeOrOpenDetailView(contentLayoutParams, R.drawable.bg_gradient_bluer, 42, View.GONE);//bg_gradient_bluer-->bg_bottom
            });
        }

        /**
         * 展示地址和圈子
         */
        private void showAddressAndCircle(MomentListModel mModel) {
            String issueCity = mModel.getIssueCity();
            String issueAddress = mModel.getIssueAddress();
            String issueLatitude = mModel.getIssueLatitude();
            String issueLongitude = mModel.getIssueLongitude();

            List<TagFlowModel> tagList = new ArrayList<>();

            if (TextUtils.isEmpty(issueLatitude) && TextUtils.isEmpty(issueLongitude)) {
            } else {
                String address = "";
                if (!TextUtils.isEmpty(issueCity)) {
                    address = TextUtils.isEmpty(issueAddress) ? issueCity : issueCity + "." + issueAddress;
                } else if (!TextUtils.isEmpty(issueAddress)) {
                    address = issueAddress;
                }

                if (!TextUtils.isEmpty(address)) {
                    TagFlowModel addressTag = new TagFlowModel(TagFlowModel.ADDRESS, "0", address, R.mipmap.ic_address);
                    addressTag.setIssueLatitude(issueLatitude);
                    addressTag.setIssueLongitude(issueLongitude);

                    tagList.add(addressTag);
                }
            }

            //获取圈子
            List<CircleModel> circleList = mModel.getCircleList();
            if (null != circleList && circleList.size() > 0) {
                for (CircleModel circleItem : circleList) {
                    TagFlowModel tag = new TagFlowModel(TagFlowModel.CIRCLE, circleItem.getId(), circleItem.getTitle(), R.mipmap.ic_circle);

                    if (circleItem.getDeleted() != 1) {
                        tagList.add(tag);
                    }
                }
            }

            if (tagList.size() > 0) {
                tflMomentItem.setVisibility(View.VISIBLE);
                //控制显示5个
                tflMomentItem.setAdapter(new TagAdapter<TagFlowModel>(tagList) {
                    @Override
                    public View getView(FlowLayout parent, int position, TagFlowModel tagItem) {
                        //加载tag布局
                        View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow, parent, false);
                        //获取标签
                        TextView tvTagName = view.findViewById(R.id.tvTagName);
                        ImageView ivTagIcon = view.findViewById(R.id.ivTagIcon);

                        ivTagIcon.setImageResource(tagItem.getDrawable());
                        tvTagName.setText(tagItem.getName());
                        return view;
                    }
                });
                tflMomentItem.setOnTagClickListener((view, pos, parent) -> {
                    TagFlowModel tagFlowModel = tagList.get(pos);
                    if (tagFlowModel.getType() == TagFlowModel.ADDRESS) {

                    } else {
                        ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                                .withString("id", tagFlowModel.getId()).navigation();
                    }

                    return true;
                });
            } else {
                tflMomentItem.setVisibility(View.GONE);
            }
        }

        private void showContentText(MomentListModel model) {
            //文本内容展示
            String content = model.getContent();

            content = YHStringUtils.getHtmlContent(content);

            if (!TextUtils.isEmpty(content)) {
                List<FriendModel> nameList = model.getUserList();
                List<TopicModel> topicList = model.getTopicList();

                //直接使用RichTextView
                SpanAtUserCallBack spanAtUserCallBack = new SpanAtUserCallBack() {
                    @Override
                    public void onClick(View view, FriendModel userModel) {
                        if (view instanceof TextView) {
                            ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                        }
                        ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                                .withString("userId", userModel.getUserId()).navigation();
                    }
                };

                SpanTopicCallBack spanTopicCallBack = (view, topicModel) -> {
                    if (view instanceof TextView) {
                        ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                    }
                    ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                            .withString("id", topicModel.getId()).navigation();
                };

                tvContent.setOnTouchListener(new CustomTextViewTouchListener(
                        v -> {
                        }
                ));
                tvContent.setSpanAtUserCallBackListener(spanAtUserCallBack);
                tvContent.setSpanTopicCallBackListener(spanTopicCallBack);
                //所有配置完成后才设置text
                content = YHStringUtils.firstAtAddSpace(content);
                tvContent.setRichText(content, null != nameList ? nameList : new ArrayList<>(), topicList);

                tvContent.setVisibility(View.VISIBLE);
            } else {
                tvContent.setVisibility(View.GONE);
            }
        }

        /**
         * 控制详情展示信息
         *
         * @param contentLayoutParams
         * @param
         * @param p
         * @param i
         * @param gone
         */
        private void closeOrOpenDetailView(FrameLayout.LayoutParams contentLayoutParams, int p,
                                           int i, int gone) {
            fl_info.setBackgroundResource(p);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) fl_info.getLayoutParams();

            layoutParams.height = gone == View.GONE ? WRAP_CONTENT : MATCH_PARENT;
            fl_info.setLayoutParams(layoutParams);

            contentLayoutParams.rightMargin = DisplayUtils.dip2px(mContext, i);
            contentLayoutParams.bottomMargin = gone == View.GONE ? 0 : DisplayUtils.dip2px(mContext, 64);
            nscContent.setLayoutParams(contentLayoutParams);
            llBottomInfo.setVisibility(gone);
        }

        public TXSampleCoverVideo getPlayer() {
            return gsyVideoPlayer;
        }
    }


    private List<SelectConditionModel> mChannelList;
    private ChannelPageAdapter mAdapter;
    private SelectConditionModel selectCondition;
    private SelectPartShadowPopupView conditionPopupView;

    @Override
    public void handBestNoteCityList(ValueContentModel data) {
        if (null != data) {
            mChannelList = new ArrayList<>();

            List<SelectConditionModel> cityList = data.getValue_count();
            String city = tvAddress.getText().toString().trim();

            String requestCity = "";

            if (null != cityList && cityList.size() > 0) {
                for (int i = 0; i < cityList.size(); i++) {
                    SelectConditionModel cityItem = cityList.get(i);
                    String cityName = cityItem.getFieldValue();

                    if (city.equals(cityName) || cityName.startsWith(city)) {
                        //判断这个城市有没有内容
                        requestCity = cityName;
                        break;
                    }
                }

                if (TextUtils.isEmpty(requestCity)) {
                    requestCity = cityList.get(0).getFieldValue();
                }
            }

            tvAddress.setText(requestCity);
            getDataContentList(1);

            mChannelList.addAll(cityList);
        }
    }

    public void showConditionPopup() {
        if (conditionPopupView == null) {
            if (null == selectCondition) {
                //默认取第一个作为默认值
                selectCondition = mChannelList.get(0);
            }

            conditionPopupView = new SelectPartShadowPopupView(this, mChannelList, selectCondition, 0, R.drawable.shape_half_black);
            conditionPopupView.setOnSelectTypeListener((item, pos) -> {
                if (null != selectCondition && selectCondition.getFieldValue().equals(item.getFieldValue())) {
                    return;
                }

                setConditionText(true, item, pos);
            });
        }
        new XPopup.Builder(this).atView(llAddress).asCustom(conditionPopupView).show();
    }

    private void setConditionText(boolean select, SelectConditionModel selectObj, int pos) {
        if (null == selectCondition) {
            selectCondition = mChannelList.get(0);
        } else {
            selectCondition = selectObj;
        }

        if (select) {
            if (null != mChannelList) {
                mContentList.clear();
                GSYVideoManager.instance().stop();
                mPagerAdapter.notifyDataSetChanged();
            }

            tvAddress.setText(selectCondition.getFieldValue());
            getDataContentList(1);
        }
    }
}
