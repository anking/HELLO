package com.yanhua.moment.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.imageview.ShapeableImageView;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.DealResult;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.CircleMemberAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 圈子信息详情
 *
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_INFO_DETAIL_ACTIVITY)
public class CircleInfoDetailActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    @BindView(R2.id.rIVCirclePic)
    ShapeableImageView rIVCirclePic;
    @BindView(R2.id.tvCircleName)
    TextView tvCircleName;
    @BindView(R2.id.tvSubTitle)
    TextView tvSubTitle;//
    @BindView(R2.id.tvCircleBrief)
    TextView tvCircleBrief;
    @BindView(R2.id.tvCircleTip)
    TextView tvCircleTip;

    @BindView(R2.id.tvCircleOwner)
    TextView tvCircleOwner;
    @BindView(R2.id.tvJoinCircle)
    TextView tvJoinCircle;

    @BindView(R2.id.llOwner)
    LinearLayout llOwner;

    @BindView(R2.id.tvOwnerName)
    TextView tvOwnerName;
    @BindView(R2.id.rIVOwnerHeader)
    CircleImageView rIVOwnerHeader;
    @BindView(R2.id.rIVOwnerSeleHeader)
    CircleImageView rIVOwnerSeleHeader;

    @BindView(R2.id.tvMemberNum)
    TextView tvMemberNum;


    @BindView(R2.id.tv_right)
    TextView tvRight;


    private CircleMemberAdapter mAdapter;

    @Autowired
    String circleId;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_info_detail;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        setTitle("圈子资料");

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                getData();
            }
        });


        mAdapter = new CircleMemberAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnItemClickListener((itemView, pos) -> ARouter.getInstance().build(ARouterPath.CIRCLE_MEMBER_LIST_ACTIVITY).withString("circleId", circleId).navigation());

        refreshLayout.autoRefresh(500);
    }

    private void getData() {
        basePresenter.getCircleDetail(circleId);//获取圈子详情
        basePresenter.getCircleStatus(circleId);//获取圈子状态
        basePresenter.getCircleMemberList("", circleId, 1, 20);

        refreshLayout.finishRefresh(1000);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @OnClick({R2.id.tvJoinCircle, R2.id.tv_right, R2.id.llCircleAction})
    public void onClickView(View view) {
        int viewId = view.getId();
        if (viewId == R.id.tvJoinCircle) {

            PageJumpUtil.firstIsLoginThenJump(() -> {

                //1 加入 2 退出
                basePresenter.joinAction(circleId, !joninStatus ? 1 : 2);
            });
        } else if (viewId == R.id.tv_right) {
            ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY).withString("circleId", circleId).navigation();
        } else if (viewId == R.id.llCircleAction) {
            if (!joninStatus) {
                ToastUtils.showShort("请先加入圈子");
                return;
            }

            if (null != currentCircle) {
                String circleOwnerID = YHStringUtils.value(currentCircle.getCircleMaster());
                if (TextUtils.isEmpty(circleOwnerID)) {
                    //去申请为圈主
                    int status = currentCircle.getAuditStatus();//圈主审核状态;0待审核 1已有圈主 2暂无圈主
                    int masterApplyStatus = currentCircle.getMasterApplyStatus();// "圈主申请状态:0未申请 1已申请"

                    if (0 == status || 2 == status) {
                        if (1 == masterApplyStatus) {
                            ToastUtils.showShort("您的申请待审核");
                        } else {
                            CommonDialog commonDialog = new CommonDialog(this, "是否确定申请为圈子“" + currentCircle.getTitle() + "”的圈主？", "确定", "取消");
                            new XPopup.Builder(this).asCustom(commonDialog).show();
                            commonDialog.setOnConfirmListener(() -> {
                                commonDialog.dismiss();

                                basePresenter.circleMasterApply(currentCircle.getId());//getId()
                            });
                            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
                        }
                    } else {
                        //
                    }
                } else {
                    //判断圈主是否是自己
                    if (UserManager.getInstance().getUserId().equals(circleOwnerID)) {
                        //转让圈主
                        ARouter.getInstance()
                                .build(ARouterPath.CIRCLE_TRANSFEREN_ACTIVITY)
                                .withString("circleId", circleId)
                                .withSerializable("currentCircle", currentCircle)

                                .navigation();
                    } else {
                        //别人的圈子，别动
                    }
                }
            }
        }
    }

    @Override
    public void handApplyCircleMaster() {
        ToastUtils.showShort("提交申请成功");
        //跳到结果页面
        DealResult dealResult = new DealResult();
        dealResult.setGoToObj(null);
        dealResult.setTvGoPage(null);
        dealResult.setIvResultColor(R.color.theme);
        dealResult.setIvResultSize(56);
        dealResult.setIvResultText("\ue76a");
        dealResult.setTvResultText("你已申请圈主");
        dealResult.setTvResultDescText("请耐心等候管理员审核，预计审核时长2-3工作日");
        dealResult.setTvGoPageText("返回圈子");
        dealResult.setSubObj(currentCircle.getCircleId());
        dealResult.setSubPage(ARouterPath.CIRCLE_MASTER_APPLY_HISTORY_ACTIVITY);
        dealResult.setSubText("查看申请记录");

        ARouter.getInstance()
                .build(ARouterPath.DEAL_RESULT_ACTIVITY)
                .withSerializable("dealResult", dealResult)
                .navigation();

        this.finish();
    }

    @Override
    public void handActionSuccess(int type) {
        //1 加入 2 退出
        switch (type) {
            case 1:
                setJoinStatus(true);

                ToastUtils.showShort("加入圈子成功");
                break;
            case 2:
                setJoinStatus(false);

                ToastUtils.showShort("退出圈子成功");
                break;
        }
    }

    private void setJoinStatus(boolean b) {
        joninStatus = b;
        tvJoinCircle.setText(joninStatus ? "退出" : "加入");//1加入
        tvJoinCircle.setSelected(joninStatus);
    }

    @Override
    public void handCircleDetail(CircleModel circle) {
        if (null != circle) {
            showCircleDetail(circle);
        }
    }

    private CircleModel currentCircle;

    private void showCircleDetail(CircleModel circle) {
        tvCircleName.setText(YHStringUtils.value(circle.getTitle()));
        tvSubTitle.setText(YHStringUtils.value(circle.getSubtitle()));
        tvCircleBrief.setText(YHStringUtils.value(circle.getCircleDesc()));
        tvCircleTip.setText(YHStringUtils.value(circle.getNotice()));//公告

        currentCircle = circle;

        String circleOwnerID = YHStringUtils.value(circle.getCircleMaster());
        if (TextUtils.isEmpty(circleOwnerID)) {
            tvCircleOwner.setText("暂无圈主");

            //暂无圈主的情况存在：审核中、
            tvOwnerName.setText("去申请为圈主");
        } else {
            //判断圈主是否是自己
            if (UserManager.getInstance().getUserId().equals(circleOwnerID)) {
                tvJoinCircle.setVisibility(View.GONE);

                rIVOwnerSeleHeader.setVisibility(View.VISIBLE);
                ImageLoaderUtil.loadImg(rIVOwnerSeleHeader, UserManager.getInstance().getUserInfo().getImg());
                tvCircleOwner.setText("圈主(自己)");
                tvOwnerName.setText("转让圈主");

                //圈主审核状态;0待审核 1已有圈主 2暂无圈主
                int status = circle.getAuditStatus();

                if (status == 1) {
                    //通过
                    tvRight.setText("编辑");
                    tvRight.setVisibility(View.VISIBLE);
                }
            } else {
                tvCircleOwner.setText("圈主");

                if (UserManager.getInstance().isLogin()) {
                    basePresenter.getUserDetail(circleOwnerID);
                }
                tvOwnerName.setText(circleOwnerID);
            }
        }

        ImageLoaderUtil.loadImgCenterCrop(rIVCirclePic, circle.getCoverUrl(), R.mipmap.bg_bluer);
    }

    private boolean joninStatus;

    @Override
    public void handCircleStatus(MomentCircleStatus circleStatus) {
        setJoinStatus(circleStatus.isJoinCircle());
    }

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        //从个人信息中取出的信息并不知道备注名
        tvOwnerName.setText(userInfo.getNickName());
        rIVOwnerHeader.setVisibility(View.VISIBLE);
        ImageLoaderUtil.loadImg(rIVOwnerHeader, userInfo.getImg());

    }

    @Override
    public void handleCircleMemberList(ListResult<MomentCircleMemberModel> listResult) {
        tvMemberNum.setText("｜(" + listResult.getTotal() + "人)");

        List<MomentCircleMemberModel> listData = listResult.getRecords();

        mAdapter.setItems(listData);
    }
}
