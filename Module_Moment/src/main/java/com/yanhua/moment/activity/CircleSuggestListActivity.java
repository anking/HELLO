package com.yanhua.moment.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;
import com.yanhua.moment.adapter.CircleSuggestListAdapter;
import com.yanhua.moment.presenter.MomentListPresenter;
import com.yanhua.moment.presenter.contract.MomentListContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Administrator
 */
@Route(path = ARouterPath.CIRCLE_SUGGEST_LIST_ACTIVITY)
public class CircleSuggestListActivity extends BaseMvpActivity<MomentListPresenter> implements MomentListContract.IView {

    @BindView(R2.id.btnRight)
    TextView btnRight;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    @Autowired
    boolean joined;

    private CircleSuggestListAdapter mAdapter;
    private int total;
    private List<CircleModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.activity_circle_suggest_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle(joined ? "我加入的" : "推荐圈子");
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setText("申请圈子");

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new CircleSuggestListAdapter(mContext, false);
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

//        rvContent.addItemDecoration(new DividerItemDecoration(mContext, LinearLayout.VERTICAL));
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleModel> list = mAdapter.getmDataList();
            CircleModel item = list.get(pos);

            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation();
        });


        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);
    }

    private void getListData(int page) {
        current = page;

        if (joined) {
            basePresenter.getCircleJoinedList("", current, size);
        } else {
            basePresenter.getCircleList("", current, size);
        }
    }
    @OnClick({R2.id.btnRight})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.btnRight) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                //跳转到申请
                ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY).navigation();
            });
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MomentListPresenter();
    }

    @Override
    public void handleCircleList(ListResult<CircleModel> listResult) {
        List<CircleModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.btnRight) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                //跳转到申请
                ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY).navigation();
            });
        }
    }


    @Override
    public void handleCircleJoinedList(ListResult<CircleModel> listResult) {
        List<CircleModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}