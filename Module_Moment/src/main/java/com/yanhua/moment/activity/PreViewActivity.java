package com.yanhua.moment.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.chinalwb.are.render.AreTextView;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.PREVIEW_CONTENT_ACTIVITY)
public class PreViewActivity extends BaseMvpActivity {
    @BindView(R2.id.areTextView)
    AreTextView areTextView;
    @BindView(R2.id.tvGuideText)
    TextView tvGuideText;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;

    public static final String HTML_TEXT = "html_text";

    @Autowired
    String html_text;

    @Autowired
    String title;
    @Autowired
    String guideText;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        areTextView.fromHtml(html_text);
        tvGuideText.setText(guideText);
        tvTitle.setText(title);
    }

    @Override
    protected void creatPresent() {

    }

    @OnClick({R2.id.btn_publish, R2.id.tvLeft})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.tvLeft) {
            onBackPressed();
        } else if (id == R.id.btn_publish) {
            //如果编辑页面没有finish则先finish，---》发布跳到首页中进行

        }
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_preview;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }
}
