package com.yanhua.moment.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.common.reflect.TypeToken;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.GridImageAdapter;
import com.yanhua.common.imagepick.FullyGridLayoutManager;
import com.yanhua.common.model.DealResult;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.ReportForm;
import com.yanhua.common.model.ReportTypeBean;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.presenter.ReportPresenter;
import com.yanhua.common.presenter.contract.ReportContract;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.widget.ReportPopup;
import com.yanhua.moment.R;
import com.yanhua.moment.R2;

import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * 举报页面
 *
 * @author Administrator
 */
@Route(path = ARouterPath.REPORT_ACTIVITY)
public class ReportActivity extends BaseMvpActivity<ReportPresenter> implements ReportContract.IView {

    @BindView(R2.id.tv_title)
    TextView tvTitle;
    @BindView(R2.id.tv_total_content)
    TextView tvTotalContent;
    @BindView(R2.id.et_content)
    EditText etContent;
    @BindView(R2.id.btn_commit)
    TextView btnCommit;

    @BindView(R2.id.recycler)
    RecyclerView mRecyclerView;

    private GridImageAdapter mAdapter;
    private int maxSelectNum = 9;

    @Autowired
    int reportType;
    @Autowired
    String businessID;

    String reportDesc;
    String reportReasonName;
    String reportReasonId;
    private List<File> fileList;
    private int updateIndex;

    private StsTokenModel stsTokenModel;

    private MyHandler mHandler = new MyHandler();

    private final static int UPLOAD_FILE_FAIL = 1000;
    private final static int UPLOAD_FILE_SUCCESS = 1001;

    private List<FileResult> reportUrl;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    if (updateIndex < fileList.size()) {
                        uploadFile(fileList.get(updateIndex));
                    } else {
                        hideLoading();
                        toReport();
                    }
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new ReportPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_report;
    }

    @OnClick({R2.id.btn_commit, R2.id.rlReportType})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.btn_commit) {
            toCommit();
        } else if (view.getId() == R.id.rlReportType) {
            ReportPopup popup = new ReportPopup(mContext, reportType);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();
                reportReasonName = bean.getContent();
                reportReasonId = bean.getId();

                if (!TextUtils.isEmpty(reportReasonName)) {
                    tvTitle.setText(reportReasonName);
                }
            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        }
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        ARouter.getInstance().inject(this);

        setTitle("举报");

        etContent.setMaxLines(Integer.MAX_VALUE);
        etContent.setHorizontallyScrolling(false);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                reportDesc = charSequence.toString().trim();
                int length = charSequence.toString().length();
                tvTotalContent.setText(String.format("%d/200", length));

                if (reportDesc.isEmpty()) {
                    btnCommit.setEnabled(false);
                } else {
                    btnCommit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
        mAdapter = new GridImageAdapter(this, onAddPicClickListener);

        mAdapter.setSelectMax(maxSelectNum);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        reportUrl = new ArrayList<>();

        basePresenter.getStsToken();
    }

    private final static String TAG = "PictureSelectorTag";
    private final GridImageAdapter.onAddPicClickListener onAddPicClickListener = () -> checkStoragePermission();


    /**
     * 返回结果回调
     */
    private static class ImagePickResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;

        public ImagePickResultCallback(GridImageAdapter adapter) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }

    @SuppressLint("AutoDispose")
    private void toCommit() {
        if (TextUtils.isEmpty(reportReasonId)) {
            ToastUtils.showShort("请先选择举报原因");
            return;
        }

        List<LocalMedia> listData = mAdapter.getData();

        fileList = new ArrayList<>();
        updateIndex = 0;

        reportUrl.clear();

        //选取的图片路径
        if (!listData.isEmpty()) {
            basePresenter.addSubscribe(Flowable.just(listData)
                    .observeOn(Schedulers.io())
                    .map(list -> {
                        for (LocalMedia item : list) {
                            try {
                                if (item != null) {
                                    File file = new File(item.getRealPath());
                                    fileList.add(file);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return fileList;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) {
                        }
                    })
                    .subscribe(list -> {
                        showLoading();
                        uploadFile(list.get(updateIndex));
                    })
            );
        } else {
            toReport();
        }
    }

    private void uploadFile(File file) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                reportUrl.add(fileResult);

                updateIndex++;
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    private void toReport() {
        ReportForm form = new ReportForm();

        form.setReportDesc(reportDesc);
        form.setReportType(reportType);
        form.setReportReasonId(reportReasonId);//举报原因id

        form.setBusinessId(businessID);
        form.setReportUrl(reportUrl);

        //根据类型不通调用不同接口
        basePresenter.reportContent(form);
    }

    private void checkStoragePermission() {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                // 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                PickImageUtil.chooseImage(mActivity, PictureMimeType.ofImage(), true, maxSelectNum, mAdapter.getData(), new ImagePickResultCallback(mAdapter));
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    @Override
    public void getReportReasonListSuccess(List<ReportTypeBean> list) {

    }

    @Override
    public void reportSuccess() {
        finish();

        DealResult dealResult = new DealResult();
        dealResult.setGoToObj(null);
        dealResult.setTvGoPage(null);
        dealResult.setIvResultColor(R.color.theme);
        dealResult.setIvResultSize(56);
        dealResult.setIvResultText("\ue76a");
        dealResult.setTvResultText("提交成功");
        dealResult.setTvResultDescText("你的举报将在24小时内被处理，处理结果将在第一时间在通知消息中反馈");
        dealResult.setTvGoPageText("返回");

        ARouter.getInstance().build(ARouterPath.DEAL_RESULT_ACTIVITY)
                .withSerializable("dealResult", dealResult)
                .navigation();
    }
}
