package com.yanhua.moment.presenter.contract;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ApplyCircleForm;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.model.ValueContentModel;

public interface MomentListContract {
    interface IView extends BaseView {

        default void publishContentSuccess(ContentModel model) {
        }

        default void handlePublishErrorMsg(String msg) {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void handleUserDetail(UserInfo userInfo) {
        }

        default void handleMomentHomeList(ListResult<MomentListModel> listResult) {

        }

        default void updateCollectContentSuccess() {
        }

        default void handleCircleList(ListResult<CircleModel> listResult) {
        }

        default void handleCircleMemberList(ListResult<MomentCircleMemberModel> listResult) {
        }

        default void handleCircleJoinedList(ListResult<CircleModel> listResult) {
        }

        default void updateStarContentSuccess() {
        }

        default void updateFollowUserSuccess() {
        }

        default void handTopicDetail(TopicModel topic) {
        }

        default void handCircleDetail(CircleModel circle) {
        }

        default void handCircleStatus(MomentCircleStatus circleStatus) {
        }

        default void handleCircleHistoryList(ListResult<CircleModel> listResult) {
        }

        default void handApplyCircleResult() {
        }

        default void handApplyCircleMaster() {
        }

        default void handTransferCircleMaster() {
        }

        default void handElectSoundCityList(ValueContentModel data) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void updateCollectContent(String id, int type);

        void updateStarContent(String id, int type);

        void masterTransferCircle(String cid, String uid);

        void getMasterCircleApplyList(int current, int size);

        void circleMasterApply(String id);

        void editCircle(ApplyCircleForm form, String id);

        void getCircleApplyList(int current, int size);

        void applyCircle(ApplyCircleForm form);

        void getStsToken();

        void getCircleMemberList(String title, String circleId, int current, int size);

        void getUserDetail(String userId);

        void getCircleStatus(String id);

        void momentCircleQuit(String id, int type);

        void momentCircleJoin(String id, int type);

        void getMomentPageCityList(String title, int type, int current, int size, String distance, String latitude, String longitude);

        void getCircleDetail(String id);

        void getTopicDetail(String id);

        void getMomentPageList(String title, int type, int current, int size);

        void getCirclePageList(String title, String circleId, int type, int current, int size);

        void getTopicPageList(String title, String topicId, int type, int current, int size);


        void cancelFollowUser(String userId);

        void followUser(String userId);

        void getCircleJoinedList(String title, int current, int size);

        void getCircleList(String title, int current, int size);
    }
}
