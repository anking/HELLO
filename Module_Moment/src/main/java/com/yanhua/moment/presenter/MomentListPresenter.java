package com.yanhua.moment.presenter;

import com.google.gson.Gson;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.ApplyCircleForm;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.MomentCircleMemberModel;
import com.yanhua.common.model.MomentCircleStatus;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishContentForm;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.moment.presenter.contract.MomentListContract;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MomentListPresenter extends MvpPresenter<MomentListContract.IView> implements MomentListContract.Presenter {

    public void getDiscoBestCityList() {
        addSubscribe(MomentHttpMethod.getInstance().getDiscoBestCityList(new SimpleSubscriber<HttpResult<ValueContentModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ValueContentModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handElectSoundCityList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }


    public void publishContent(PublishContentForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(MomentHttpMethod.getInstance().publishContent(new SimpleSubscriber<HttpResult<ContentModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ContentModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.publishContentSuccess(result.getData());
                    } else {
                        baseView.handlePublishErrorMsg(result.getMesg());
                    }
                }
            }
        }, body));
    }

    @Override
    public void circleMasterApply(String id) {
        addSubscribe(MomentHttpMethod.getInstance().circleMasterApply(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handApplyCircleMaster();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void applyCircle(ApplyCircleForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(MomentHttpMethod.getInstance().applyCircle(new SimpleSubscriber<HttpResult<Boolean>>(baseView, true) {
            @Override
            public void onNext(HttpResult<Boolean> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handApplyCircleResult();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, body));
    }


    @Override
    public void editCircle(ApplyCircleForm form, String id) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(MomentHttpMethod.getInstance().editCircle(new SimpleSubscriber<HttpResult<Boolean>>(baseView, true) {
            @Override
            public void onNext(HttpResult<Boolean> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handApplyCircleResult();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, body, id));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void getUserDetail(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().getUserDetail(new SimpleSubscriber<HttpResult<UserInfo>>(baseView, false) {
            @Override
            public void onNext(HttpResult<UserInfo> bean) {
                super.onNext(bean);
                if (bean != null) {
                    String code = bean.getCode();
                    if (code.equals(HttpCode.SUCCESS)) {
                        UserInfo userInfo = bean.getData();
                        if (userInfo != null) {
                            baseView.handleUserDetail(userInfo);
                        }
                    } else {
                        baseView.handleErrorMessage(bean.getMesg());
                    }
                }
            }
        }, userId));
    }

    public void joinAction(String id, int type) {
        if (type == 1) {
            momentCircleJoin(id, type);
        } else {
            momentCircleQuit(id, type);
        }
    }

    @Override
    public void momentCircleJoin(String id, int type) {
        addSubscribe(MomentHttpMethod.getInstance().momentCircleJoin(new SimpleSubscriber<HttpResult<Boolean>>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        Boolean success = (Boolean) result.getData();
                        baseView.handActionSuccess(type);//1 加入 2 退出
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void momentCircleQuit(String id, int type) {
        addSubscribe(MomentHttpMethod.getInstance().momentCircleQuit(new SimpleSubscriber<HttpResult<Boolean>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Boolean> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        Boolean success = result.getData();
                        baseView.handActionSuccess(type);//1 加入 2 退出
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void masterTransferCircle(String cid, String uid) {
        addSubscribe(MomentHttpMethod.getInstance().masterTransferCircle(new SimpleSubscriber<HttpResult<Boolean>>(baseView, false) {
            @Override
            public void onNext(HttpResult<Boolean> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        Boolean success = result.getData();
                        baseView.handTransferCircleMaster();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, cid, uid));
    }

    @Override
    public void getMasterCircleApplyList(int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getMasterCircleApplyList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleHistoryList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size));
    }

    @Override
    public void getCircleApplyList(int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleApplyList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleHistoryList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size));
    }

    @Override
    public void getCircleMemberList(String title, String circleId, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleMemberList(new SimpleSubscriber<HttpResult<ListResult<MomentCircleMemberModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentCircleMemberModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentCircleMemberModel> listResult = result.getData();
                        baseView.handleCircleMemberList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, circleId, current, size));
    }


    @Override
    public void getCircleList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }


    @Override
    public void getCircleJoinedList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleJoinedList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleJoinedList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }

    @Override
    public void getMomentPageCityList(String title, int type, int current, int size, String distance, String latitude, String longitude) {
        addSubscribe(MomentHttpMethod.getInstance().getMomentPageCityList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, type, current, size, distance, latitude, longitude));
    }

    public void getDiscoBestList(String keyword, String city, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getDiscoBestList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        },keyword, city, current, size));
    }


    @Override
    public void getMomentPageList(String title, int type, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getMomentPageList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, type, current, size));
    }

    @Override
    public void getCirclePageList(String title, String circleId, int type, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCirclePageList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, circleId, type, current, size));
    }

    @Override
    public void getTopicPageList(String title, String topicId, int type, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getTopicPageList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, topicId, type, current, size));
    }

    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }


    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void getTopicDetail(String id) {
        addSubscribe(MomentHttpMethod.getInstance().getTopicDetail(new SimpleSubscriber<HttpResult<TopicModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<TopicModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        TopicModel topic = result.getData();

                        baseView.handTopicDetail(topic);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void getCircleDetail(String id) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleDetail(new SimpleSubscriber<HttpResult<CircleModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<CircleModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        CircleModel circle = result.getData();

                        baseView.handCircleDetail(circle);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }
    public void getCircleApplyDetail(String id) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleApplyDetail(new SimpleSubscriber<HttpResult<CircleModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<CircleModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        CircleModel circle = result.getData();

                        baseView.handCircleDetail(circle);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }
    @Override
    public void getCircleStatus(String id) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleStatus(new SimpleSubscriber<HttpResult<MomentCircleStatus>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MomentCircleStatus> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        MomentCircleStatus circleStatus = result.getData();
                        baseView.handCircleStatus(circleStatus);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void addCircleViewCount(String id) {
        addSubscribe(MomentHttpMethod.getInstance().addCircleViewCount(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void addTopicViewCount(String id) {
        addSubscribe(MomentHttpMethod.getInstance().addTopicViewCount(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }
}
