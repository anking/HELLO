package com.yanhua.moment.presenter.contract;


import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.PublishContentForm;

public interface PublishContentContract {

    interface IView extends BaseView {

        default void publishContentSuccess(ContentModel model) {
        }

        default void handlePublishErrorMsg(String msg) {
        }

        default void handleErrorMsg(String msg) {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void handleTopicList(ListResult<TopicModel> listResult) {
        }

        default void handleCircleList(ListResult<CircleModel> listResult) {
        }
        default void handleHotTopicList(ListResult<TopicModel> listResult) {
        }

        default void handleHotCircleList(ListResult<CircleModel> listResult) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void getHotCircleList(String title, int current, int size);

        void getHotTopicList(String title, int current, int size);
        void publishContent(PublishContentForm form);

        void getStsToken();

        void getCircleJoinedList(String title, int current, int size);

        void getTopicList(String title, int current, int size);
    }
}
