package com.yanhua.moment.presenter.contract;


import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.model.ValueContentModel;

import java.util.HashMap;
import java.util.List;

public interface ContentDetailContract {
    interface IView extends BaseView {

        default void handleDeleteContentSuccess() {
        }

        default void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        }

        default void handleUserDetail(UserInfo userInfo ) {
        }
        void handleContentDetailSuccess(boolean isFirst, MomentListModel model);

        default void handleSuccessContentDetail(MomentListModel model, int i) {

        }

        default void handleContentErrorMsg(boolean isFirst, String msg) {
        }

        void handleErrorMsg(String msg);

        default void handleCommentList(List<CommentModel> list, int totalSum) {
        }

        void handleChildCommentList(ListResult<CommentModel> listResult);

        void handlePublishCommentSuccess(CommentModel model);

        void updateCollectContentSuccess();

        void updateStarContentSuccess();

        void updateStarCommentSuccess();

        void updateFollowUserSuccess();

        default void handleContentListSuccess(List<MomentListModel> list) {
        }

        default  void handleDiscoBestListSuccess(List<MomentListModel> records){}

        default void handBestNoteCityList(ValueContentModel data){}

        default void handleDiscoContentDetail(MomentListModel model){}

        default void handleDeleteCommentSuccess(int pos){}

        default void handleDealItemTopSuccess(int pos){}
    }

    interface Presenter extends BasePresenter<IView> {
        void getCommentList(String contentId, int current, int childSize);

        void publishComment(PublishCommentForm form, int type);

        void updateCollectContent(String id, int type);

        void updateStarContent(String id, int type);

        void updateStarComment(String id);

        void getChildCommentList(String commentId, int current, int size);

        void getContentList(String title, int type, int current, int size);

        void getUserDetail(String userId);

        void deleteContent(String id);

        void getContentUserDetail(String userId);

        void getContentDetail(boolean isFirst, String id);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void getContentList(HashMap<String, Object> params);

        void getUserContent(int contentType, String userId, int current);

        void addViewCount(int type,String id);
    }
}
