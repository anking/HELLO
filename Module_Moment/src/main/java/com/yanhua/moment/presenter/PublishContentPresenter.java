package com.yanhua.moment.presenter;

import com.google.gson.Gson;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.ContentModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.PublishContentForm;
import com.yanhua.moment.presenter.contract.PublishContentContract;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PublishContentPresenter extends MvpPresenter<PublishContentContract.IView> implements PublishContentContract.Presenter {

    @Override
    public void publishContent(PublishContentForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(MomentHttpMethod.getInstance().publishContent(new SimpleSubscriber<HttpResult<ContentModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ContentModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.publishContentSuccess(result.getData());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, body));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMsg(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void getTopicList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getTopicList(new SimpleSubscriber<HttpResult<ListResult<TopicModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<TopicModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<TopicModel> listResult = result.getData();
                        baseView.handleTopicList(listResult);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }


    @Override
    public void getCircleJoinedList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleJoinedList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleList(listResult);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }


    @Override
    public void getHotTopicList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getHotTopicList(new SimpleSubscriber<HttpResult<ListResult<TopicModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<TopicModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<TopicModel> listResult = result.getData();
                        baseView.handleHotTopicList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }


    @Override
    public void getHotCircleList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getHotCircleList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleHotCircleList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }

}
