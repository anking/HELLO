package com.yanhua.mine.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;

public class MyQuestionAdapter extends BaseRecyclerAdapter<MyQAModel, MyQuestionAdapter.ViewHolder> {

    private Context context;

    public MyQuestionAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_my_question, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MyQAModel item) {
        holder.tvBarName.setText(item.getBarName());
        holder.tvQuestionName.setText(item.getAskedDesc());
        if (TextUtils.isEmpty(item.getParentAskedDesc())) {
            holder.tvAnswer.setText("暂无回答");
            holder.llAnswerNum.setVisibility(View.GONE);
        } else {
            holder.llAnswerNum.setVisibility(View.VISIBLE);
            holder.tvAnswerNum.setText(String.format("全部%d个回答", item.getAskedNum()));
            String content = "<font color=\"#B4B8BC\" >" + item.getAccountName() + "：</font>"
                    + "<font color=\"#36404A\" >" + item.getParentAskedDesc() + "</font>";
            holder.tvAnswer.setText(Html.fromHtml(content));
        }
        int askedAudit = item.getAskedAudit();
        switch (askedAudit) {
            case -1:
                holder.tvStatus.setVisibility(View.VISIBLE);
                holder.tvStatus.setText("已驳回");
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.color_ff4b19));
                break;
            case 0:
                holder.tvStatus.setVisibility(View.VISIBLE);
                holder.tvStatus.setText("待审核");
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.color_80868c));
                break;
            case 1:
                holder.tvStatus.setVisibility(View.GONE);
                break;
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvBarName)
        TextView tvBarName;
        @BindView(R2.id.tvQuestionName)
        TextView tvQuestionName;
        @BindView(R2.id.tvAnswer)
        TextView tvAnswer;
        @BindView(R2.id.llAnswerNum)
        LinearLayout llAnswerNum;
        @BindView(R2.id.tvAnswerNum)
        TextView tvAnswerNum;
        @BindView(R2.id.tvStatus)
        TextView tvStatus;
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
