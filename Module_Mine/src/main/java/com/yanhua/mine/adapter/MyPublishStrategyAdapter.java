package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyPublishStrategyAdapter extends BaseRecyclerAdapter<MyNewsListModel, MyPublishStrategyAdapter.ViewHolder> {

    private Context mContext;
    private boolean isMyPublish;
    private OnHandleClickListener mListener;

    public MyPublishStrategyAdapter(Context context, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    public MyPublishStrategyAdapter(Context context, boolean isMy, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
        isMyPublish = isMy;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_strategy_game, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MyNewsListModel item) {
        int position = getPosition(holder);

        ViewGroup.LayoutParams imageLayoutParams = holder.ivCover.getLayoutParams();
        int realWidth = DisplayUtils.getScreenWidth(context) / 2 - DisplayUtils.dip2px(context, 16);//

        String coverUrl = "";
        if (!TextUtils.isEmpty(item.getVedioCoverUrl())) {
            coverUrl = item.getVedioCoverUrl();
            holder.ivVideoPlay.setVisibility(View.VISIBLE);
        } else {
            coverUrl = item.getCoverUrl();
            holder.ivVideoPlay.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(coverUrl)) {
            //后台返回null
            String finalCoverUrl = coverUrl;
            Glide.with(mContext)
                    .asBitmap()
                    .load(coverUrl)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int width = resource.getHeight();
                            int height = resource.getWidth();

                            if (width != 0 && height != 0) {
                                float rate = DisplayUtils.getImageRate(width, height);//4:3 1:1 3:4
                                float showHeight = realWidth / rate;

                                imageLayoutParams.width = realWidth;
                                imageLayoutParams.height = (int) showHeight;
                            } else {
                                imageLayoutParams.width = realWidth;
                                imageLayoutParams.height = realWidth;
                            }

                            holder.ivCover.setLayoutParams(imageLayoutParams);

                            ImageLoaderUtil.loadImgCenterCrop(context, holder.ivCover, finalCoverUrl, R.drawable.place_holder, 12);
                        }
                    });
        } else {
            imageLayoutParams.width = realWidth;
            imageLayoutParams.height = realWidth;

            holder.ivCover.setLayoutParams(imageLayoutParams);
            ImageLoaderUtil.loadImgCenterCrop(context, holder.ivCover, coverUrl, R.drawable.place_holder, 12);
        }

        String keys = item.getKeyword();
        if (TextUtils.isEmpty(keys)) {
            holder.tflKeys.setVisibility(View.GONE);
        } else {
            holder.tflKeys.setVisibility(View.VISIBLE);

            String[] keyArr = keys.split(",");
            List<String> mKeys = new ArrayList<>();
            for (String key : keyArr) {
                mKeys.add(key);
            }

            holder.tflKeys.setAdapter(new TagAdapter<String>(mKeys) {
                @Override
                public View getView(FlowLayout parent, int position, String s) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_strategy_key, parent, false);
                    //获取标签
                    TextView tvTag = view.findViewById(R.id.tvTagName);
                    tvTag.setText(s);
                    return view;
                }
            });
        }

        holder.tvSuggestTitle.setText(YHStringUtils.value(item.getContentTitle()));//

        String publishTime = item.getPublishTime();
        if (TextUtils.isEmpty(publishTime)) {
            holder.tvTime.setVisibility(View.GONE);//

        } else {
            holder.tvTime.setVisibility(View.VISIBLE);//

            holder.tvTime.setText(YXTimeUtils.getFriendlyTimeAtContent(publishTime));//
        }
        holder.tvUserName.setText(YHStringUtils.value(item.getNickName()));

        ImageLoaderUtil.loadImgHeadCenterCrop(holder.ivUserHead, YHStringUtils.value(item.getImg()), R.drawable.place_holder);

        if (isMyPublish) {
            String auditStatusName = item.getAuditStatusName();
            if (TextUtils.isEmpty(auditStatusName)) {
                holder.llAdult.setVisibility(View.GONE);
            } else {
                holder.llAdult.setVisibility(View.VISIBLE);
                holder.tvAdultName.setText(auditStatusName);
            }

            int auditStatus = item.getAuditStatus();
            if (auditStatus!=0&&auditStatus!=1) {
                holder.llAdult.setOnClickListener(v -> {
                    mListener.onHandleClick(OnHandleClickListener.REJECT, item, position);
                });
            }else {
                holder.llAdult.setOnClickListener(null);
            }

        } else {
            holder.llAdult.setVisibility(View.GONE);
        }

        holder.llItem.setOnClickListener(v -> {
            mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position);
        });
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.tvSuggestTitle)
        TextView tvSuggestTitle;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tflKeys)
        TagFlowLayout tflKeys;

        @BindView(R2.id.llItem)
        LinearLayout llItem;

        @BindView(R2.id.ivVideoPlay)
        ImageView ivVideoPlay;

        @BindView(R2.id.llAdult)
        LinearLayout llAdult;
        @BindView(R2.id.tvAdultName)
        TextView tvAdultName;
        @BindView(R2.id.tvTime)
        TextView tvTime;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
