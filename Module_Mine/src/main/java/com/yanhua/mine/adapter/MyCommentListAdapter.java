package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.shuyu.textutillib.RichTextBuilder;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MineCommentModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyCommentListAdapter extends BaseRecyclerAdapter<MineCommentModel, MyCommentListAdapter.ViewHolder> {

    private Context mContext;

    public MyCommentListAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_comment, parent, false));

        //具体的@点击效果参考此刻列表项中的
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MineCommentModel item) {
        int position = getPosition(holder);

        String userPhoto = UserManager.getInstance().getUserInfo().getImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImgCenterCrop(holder.civAvatar, userPhoto, R.drawable.place_holder);
        } else {
            holder.civAvatar.setImageResource(R.drawable.place_holder);
        }
        holder.tvName.setText(YHStringUtils.value(item.getNickName()));

        int replyStatus = item.getReplyStatus();
        String replyContent = YHStringUtils.value(item.getCommentDesc());
        if (replyStatus == 0) {
            holder.llReplyContent.setVisibility(View.GONE);
            //獲取評論的内容
            holder.tvReply.setText(replyContent);
        } else {
            holder.llReplyContent.setVisibility(View.VISIBLE);

            replyContent = "回复" + YXConfig.SPACE + "@" + YHStringUtils.value(item.getParentNickName()) + YXConfig.SPACE + ":" + replyContent;

            List<TopicModel> topicList = new ArrayList<>();
            //文本内容展示
            List<FriendModel> parentList = new ArrayList<>();//At的用户
            FriendModel parentModel = new FriendModel();
//            parentModel.setUserId(item.getParentUserId());
            parentModel.setUserId(UserManager.getInstance().getUserId());
            parentModel.setNickName(item.getParentNickName());
            parentList.add(parentModel);
            //直接使用RichTextView
            SpanAtUserCallBack spanAtUserCallBack = (view, userModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userModel.getUserId()).navigation();
            };

            RichTextBuilder richTextBuilder = new RichTextBuilder(mContext);
            richTextBuilder.setContent(replyContent)
                    .setListUser(parentList)
                    .setListTopic(topicList)
                    .setTextView(holder.tvReply)
                    //.setVerticalAlignment(CenteredImageSpan.ALIGN_CENTER)
                    .setSpanAtUserCallBack(spanAtUserCallBack)
                    .build();

            String grantName = item.getGrandNickName();
            if (!TextUtils.isEmpty(grantName)) {
                FriendModel grantModel = new FriendModel();
//                grantModel.setUserId(item.getGrandUserId());
                grantModel.setUserId(UserManager.getInstance().getUserId());
                grantModel.setNickName(item.getGrandNickName());

                parentList.add(grantModel);

                //@没有土的木回复 @彭于晏的老婆：看了豆瓣才知道的事
                replyContent = YXConfig.SPACE + "@" + YHStringUtils.value(item.getParentNickName()) + YXConfig.SPACE + "回复" + YXConfig.SPACE + "@" + grantName + YXConfig.SPACE + ":" + YHStringUtils.value(item.getParentCommentDesc());
            }

            RichTextBuilder richTextBuilder1 = new RichTextBuilder(mContext);
            richTextBuilder1.setContent(replyContent)
                    .setListUser(parentList)
                    .setListTopic(topicList)
                    .setTextView(holder.tvOriReply)
                    //.setVerticalAlignment(CenteredImageSpan.ALIGN_CENTER)
                    .setSpanAtUserCallBack(spanAtUserCallBack)
                    .build();
        }

        String createdTime = item.getCreatedTime();// 此處需要獲取評論時間
        holder.tvDate.setText(YXTimeUtils.getFriendlyTimeAtContent(createdTime, true));//将后台返回数据进行加工处理


        int type = item.getType();

        switch (type) {
            case YXConfig.TYPE_BEST:
            case YXConfig.TYPE_MOMENT:
                int contentType = item.getContentType();

                String urlJson = item.getContentUrl();
                String coverImage = null;
                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    FileResult fileResult = fileResults.get(0);
                    coverImage = fileResult.getHttpUrl();

                    ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 0);
                    holder.ivPlay.setVisibility(contentType == 2 ? View.VISIBLE : View.GONE);
                } else {
                    holder.ivPlay.setVisibility(View.GONE);
                    ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 0);
                }
                break;
            default:
                holder.ivPlay.setVisibility(View.GONE);
                ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, item.getCoverUrl(), R.drawable.place_holder, 0);
                break;
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civAvatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvReply)
        RichTextView tvReply;
        @BindView(R2.id.tvOriReply)
        RichTextView tvOriReply;
        @BindView(R2.id.tv_date)
        TextView tvDate;

        @BindView(R2.id.iv_cover)
        ImageView ivCover;
        @BindView(R2.id.iv_play)
        ImageView ivPlay;

        @BindView(R2.id.llReplyContent)
        LinearLayout llReplyContent;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
