package com.yanhua.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;

public class MyAnswerAdapter extends BaseRecyclerAdapter<MyQAModel, MyAnswerAdapter.ViewHolder> {

    public MyAnswerAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_my_answer, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MyQAModel item) {
        ImageLoaderUtil.loadImg(holder.civAvatar, item.getImg());
        holder.tvUserName.setText(item.getAccountName());
        holder.tvQuestionName.setText(item.getParentAskedDesc());
        holder.tvAnswerTitle.setText(item.getAskedDesc());
        holder.tvBarName.setText(item.getBarName());
        holder.tvTime.setText(item.getCreatedTime().substring(0, 16));
        holder.tvWorseNum.setText(String.format("踩%d", item.getWorseCount()));
        holder.tvLoveNum.setText(String.format("赞%d", item.getFabulousCount()));
        int askedAudit = item.getAskedAudit();
        switch (askedAudit) {
            case -1:
                holder.tvStatus.setVisibility(View.VISIBLE);
                holder.tvStatus.setText("已驳回");
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.color_ff4b19));
                break;
            case 0:
                holder.tvStatus.setVisibility(View.VISIBLE);
                holder.tvStatus.setText("待审核");
                holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.color_80868c));
                break;
            case 1:
                holder.tvStatus.setVisibility(View.GONE);
                break;
        }
        holder.llQuestion.setOnClickListener(v -> {
            if (FastClickUtil.isFastClick(500)) {
                BarQAModel qaModel = new BarQAModel();
                qaModel.setParentAskedId(item.getParentAskedId());
                qaModel.setBarId(item.getBarId());
                qaModel.setBarName(item.getBarName());
                qaModel.setBarImg(item.getBarImg());
                qaModel.setBarBrowseCount(item.getBarBrowseCount());
                ARouter.getInstance()
                        .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                        .withSerializable("qModel", qaModel)
                        .navigation();
            }
        });
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvBarName)
        TextView tvBarName;
        @BindView(R2.id.tvQuestionName)
        TextView tvQuestionName;
        @BindView(R2.id.tvAnswerTitle)
        TextView tvAnswerTitle;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvWorseNum)
        TextView tvWorseNum;
        @BindView(R2.id.tvLoveNum)
        TextView tvLoveNum;
        @BindView(R2.id.tvStatus)
        TextView tvStatus;
        @BindView(R2.id.llQuestion)
        LinearLayout llQuestion;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
