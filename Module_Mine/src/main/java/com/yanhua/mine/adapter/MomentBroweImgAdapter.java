package com.yanhua.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;


public class MomentBroweImgAdapter extends BaseRecyclerAdapter<String, MomentBroweImgAdapter.ViewHolder> {

    private Context mContext;
    private int mContentType;
    private int mShowMoreIndex;

    public MomentBroweImgAdapter(Context context, int contentType, int showMoreIndex) {
        super(context);
        mContext = context;
        this.mContentType = contentType;
        mShowMoreIndex = showMoreIndex;
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_moment_browe_img, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        int pos = getPosition(holder);
        String coverImage = item;
//
        ViewGroup.LayoutParams imageLayoutParams = holder.itemContainer.getLayoutParams();

//
        int screenWidth = DisplayUtils.getScreenWidth(mContext);
        int showWidth = (screenWidth - DisplayUtils.dip2px(mContext, 92));//72 + 4 * 5
        int showHeight = showWidth / 5;

        imageLayoutParams.width = showHeight;
        imageLayoutParams.height = showHeight;

        holder.itemContainer.setLayoutParams(imageLayoutParams);

        ImageLoaderUtil.loadImgCenterCrop(mContext, holder.ivCover, coverImage, R.drawable.place_holder, 0);
        holder.ivPlay.setVisibility(mContentType == 2 ? View.VISIBLE : View.GONE);

        if (mShowMoreIndex > 0 && pos == 4) {
            holder.tvIndex.setVisibility(View.VISIBLE);
            holder.tvIndex.setText("+" + mShowMoreIndex);
        } else {
            holder.tvIndex.setVisibility(View.GONE);
        }
    }


    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.ivPlay)
        ImageView ivPlay;
        @BindView(R2.id.tvIndex)
        TextView tvIndex;

        @BindView(R2.id.itemContainer)
        FrameLayout itemContainer;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
