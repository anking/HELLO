package com.yanhua.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;


public class MyCircleJoinAdapter extends BaseRecyclerAdapter<CircleModel, MyCircleJoinAdapter.ViewHolder> {

    public MyCircleJoinAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_circle_join, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText("◎" + YHStringUtils.value(item.getTitle()));
        holder.tvCircleNum.setText("已加入 " + YHStringUtils.quantityFormat(item.getFansCount()) + "人");

        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl());

    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
