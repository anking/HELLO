package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarModel;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import butterknife.BindView;


public class MyFav9barListAdapter extends BaseRecyclerAdapter<BarModel, MyFav9barListAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mListener;

    public MyFav9barListAdapter(Context context, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    public interface OnHandleClickListener {
        void onHandleClick(int type, BarModel item, int pos);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_fab_9bar, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull @NotNull MyFav9barListAdapter.ViewHolder holder, @NonNull @NotNull BarModel item) {
        int position = getPosition(holder);
        holder.tvName.setText(item.getName());
        Glide.with(mContext)
                .asBitmap()
                .load(item.getBarPrimaryImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        holder.ivActivityCover.setSrcBmp(resource);
                    }
                });
        holder.llItem.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onHandleClick(0, item, position);
            }
        });

        // 夜店、清吧、ktv、餐吧
        switch (item.getBarType()) {
            case "夜店":
                holder.ivBarType.setImageResource(R.mipmap.ic_night_club);
                break;
            case "清吧":
                holder.ivBarType.setImageResource(R.mipmap.ic_sober_bar);
                break;
            case "餐吧":
                holder.ivBarType.setImageResource(R.mipmap.ic_dinnng_bar);
                break;
            case "KTV":
                holder.ivBarType.setImageResource(R.mipmap.ic_ktv);
                break;
            case "Live House":
                holder.ivBarType.setImageResource(R.mipmap.ic_live_house);
                break;
        }
        String barTrait = item.getBarTrait();
        String[] barTraitArr = barTrait.split(",");
        holder.tflBarTrait.setAdapter(new TagAdapter<String>(Arrays.asList(barTraitArr)) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_9bar_tag, parent, false);
                //获取标签
                TextView tvTag = view.findViewById(R.id.tvTag);
                tvTag.setText(s);
                return view;
            }
        });
        holder.tflBarTrait.setOnTagClickListener((view, pos, parent) -> {
            return true;
        });
        holder.tvAddress.setText(item.getAddress());
        holder.tvAddressDistance.setText(YHStringUtils.formatDistance(item.getDistance()));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.llItem)
        LinearLayout llItem;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.ivBarType)
        ImageView ivBarType;
        @BindView(R2.id.tflBarTrait)
        TagFlowLayout tflBarTrait;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;
        @BindView(R2.id.tvAddressDistance)
        TextView tvAddressDistance;
        @BindView(R2.id.ivActivityCover)
        AnyShapeImageView ivActivityCover;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
