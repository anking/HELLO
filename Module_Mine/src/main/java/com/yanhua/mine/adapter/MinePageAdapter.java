package com.yanhua.mine.adapter;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.yanhua.common.model.Channel;
import com.yanhua.mine.fragment.MineHighLightFragment;
import com.yanhua.mine.fragment.MineInviteFragment;
import com.yanhua.mine.fragment.MineQAFragmentg;
import com.yanhua.mine.fragment.MyMomentFragment;
import com.yanhua.mine.fragment.MyPublishBreakNewsFragment;
import com.yanhua.mine.fragment.MyPublishStrategyFragment;

import java.util.List;

public class MinePageAdapter extends FragmentStatePagerAdapter {

    private List<Channel> mMenuList;
    private String userId;

    public MinePageAdapter(@NonNull FragmentManager fm, List<Channel> list,String uid) {
        super(fm);
        this.mMenuList = list;
        this.userId = uid;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Channel model = mMenuList.get(position);
        int type = model.getType();
        String idType = model.getId();

        Fragment fragment = null;

        switch (type) {
            case 1:
                fragment = new MyMomentFragment();
                break;
            case 2:
                fragment = new MyPublishBreakNewsFragment();
                break;
            case 3:
                fragment = new MyPublishStrategyFragment();
                break;
            case 4:
                fragment = new MineHighLightFragment();
                break;
            case 5:
                fragment = new MineInviteFragment();
                break;
            case 6:
                fragment = new MineQAFragmentg();
                break;
        }

        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        fragment.setArguments(bundle);
       /* Bundle bundle = new Bundle();
        bundle.putInt("type", type);//此刻首页的类型

        int objType = model.getType();
        String circleId = model.getCircleId();
        String topicId = model.getTipicId();

        bundle.putInt("objType", objType);
        bundle.putString("circleId", circleId);
        bundle.putString("idType", idType);
        bundle.putString("topicId", topicId);

        fragment.setArguments(bundle);*/
        return fragment;
    }

    @Override
    public int getCount() {
        return mMenuList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        Channel model = mMenuList.get(position);
        String reportName = model.getReportName();
        return !TextUtils.isEmpty(reportName) ? reportName : "";
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
    }
}