package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.label.CornerLabelView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


public class MyInviteListAdapter extends BaseRecyclerAdapter<InviteModel, MyInviteListAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mListener;
    private String mType;//"1", "我报名的"|"2", "我发起的"|"3", "我收藏的",

    public MyInviteListAdapter(Context context, String type, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
        mType = type;
    }

    public interface OnHandleClickListener {
        void onHandleClick(int type, InviteModel item, int pos);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_invite, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull @NotNull MyInviteListAdapter.ViewHolder holder, @NonNull @NotNull InviteModel item) {
        int position = getPosition(holder);

        String time = item.getActivityTime();
        holder.tvTime.setText(YHStringUtils.value(time).replace("-","/") + "截止");
        holder.tvName.setText(YHStringUtils.value(item.getTitle()));

        //根据UI拼接字段
        String payWay = item.getPaymentTypeName();
        String people = item.getPartnerTypeName();
        int numLimit = item.getActivityNumber();
        String desc = YHStringUtils.value(payWay) + "｜" + YHStringUtils.value(people) + "｜" + numLimit + "人局";
        holder.tvDesc.setText(desc);

        holder.tvAddress.setText(YHStringUtils.value(item.getDestination()));
        holder.tvJoinedNum.setText(item.getInviteNumber() + "已邀请");//已邀请
        holder.tvJoiningNum.setText(item.getEnrollNumber() + "已报名 >");//已报名

        //发起人信息
        int userGender = item.getUserGender();//邀约用户性别(-1:保密, 1:男, 2:女)
        String userImage = item.getUserImage();//邀约用户头像
        String userName = item.getUserName();//邀约用户名称
        ImageLoaderUtil.loadImgHeadCenterCrop(holder.ivUserHead, userImage, R.drawable.place_holder);
        switch (userGender) {
            case -1:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
            case 1:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_male);
                break;
            case 2:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_female);
                break;
            default:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
        }
        holder.tvUserName.setText(userName);


        Glide.with(mContext)
                .asBitmap()
                .override(DisplayUtils.dip2px(context,100),DisplayUtils.dip2px(context,100)) //图片显示的分辨率 ，像素值 可以转化为DP再设置
                .load(item.getCoverUrl())
                .centerCrop()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        holder.ivActivityCover.setSrcBmp(resource);
                    }
                });
        int status = item.getStatus();
        //2 4 5 状态 App不显示 2换成（ 7 8 ）
        ////审核状态(0:审核中, 1:审核未通过, 2:审核通过, 3:已停约, 4:用户删除, 5:平台删除, 6:已取消,7:进行中,8:已结束)
        switch (status) {
            case 0:
                holder.tvStatus.setText("审核中");
                holder.tvStatus.setTextColor(Color.parseColor("#FE9D1E"));
                break;
            case 1:
                holder.tvStatus.setText("审核未通过");
                holder.tvStatus.setTextColor(Color.parseColor("#FF4B19"));
                break;
            case 3:
                holder.tvStatus.setText("已停约");
                holder.tvStatus.setTextColor(Color.parseColor("#80868C"));
                break;
            case 6:
                holder.tvStatus.setText("已取消");
                holder.tvStatus.setTextColor(Color.parseColor("#80868C"));
                break;
            case 7:
                holder.tvStatus.setText("进行中");
                holder.tvStatus.setTextColor(Color.parseColor("#33C35C"));
                break;
            case 8:
                holder.tvStatus.setText("已结束");
                holder.tvStatus.setTextColor(Color.parseColor("#ffb4b8bc"));
                break;
            case 10:
                holder.tvStatus.setText("编辑中");
                holder.tvStatus.setTextColor(Color.parseColor("#33C35C"));
                break;
            default:
                holder.tvStatus.setText("");
                holder.tvStatus.setTextColor(Color.parseColor("#80868C"));
                break;
        }

        switch (mType) {//"1", "我报名的"|"2", "我发起的"|"3", "我收藏的",
            case "1":
                holder.rlNoMyPublish.setVisibility(View.VISIBLE);
                holder.llMyPublish.setVisibility(View.GONE);
                holder.iconFav.setVisibility(View.GONE);

                //当前用户的邀约状态(0:未邀约, 1:已邀约)
                int inviteState = item.getInviteState();
                holder.clvInvite.setVisibility(inviteState == 1 ? View.VISIBLE : View.GONE);//邀请了就展示这个角标
                break;
            case "2":
                holder.rlNoMyPublish.setVisibility(View.GONE);
                holder.llMyPublish.setVisibility(View.VISIBLE);
                holder.iconFav.setVisibility(View.GONE);
                holder.clvInvite.setVisibility(View.GONE);
                break;
            case "3":
                holder.rlNoMyPublish.setVisibility(View.VISIBLE);
                holder.llMyPublish.setVisibility(View.GONE);
                holder.iconFav.setVisibility(View.VISIBLE);
                holder.clvInvite.setVisibility(View.GONE);
                break;
        }

        holder.llItem.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onHandleClick(0, item, position);
            }
        });
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.rlNoMyPublish)
        RelativeLayout rlNoMyPublish;

        @BindView(R2.id.llMyPublish)
        LinearLayout llMyPublish;
        @BindView(R2.id.iconFav)
        AliIconFontTextView iconFav;
        @BindView(R2.id.clvInvite)
        CornerLabelView clvInvite;

        @BindView(R2.id.llItem)
        LinearLayout llItem;

        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvStatus)
        TextView tvStatus;

        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;

        @BindView(R2.id.tvJoinedNum)
        TextView tvJoinedNum;//已邀请
        @BindView(R2.id.tvJoiningNum)
        TextView tvJoiningNum;//已报名

        @BindView(R2.id.ivActivityCover)
        AnyShapeImageView ivActivityCover;

        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;

        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;

        @BindView(R2.id.tvUserName)
        TextView tvUserName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
