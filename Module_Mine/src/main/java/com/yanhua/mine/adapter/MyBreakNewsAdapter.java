package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyBreakNewsAdapter extends BaseRecyclerAdapter<MyNewsListModel, MyBreakNewsAdapter.ViewHolder> {

    private Context mContext;
    private boolean isMyPublish;
    private OnHandleClickListener mListener;

    public MyBreakNewsAdapter(Context context, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    public MyBreakNewsAdapter(Context context, boolean isMy, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
        isMyPublish = isMy;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_break_news, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MyNewsListModel item) {
        int position = getPosition(holder);

        String coverImage = item.getCoverUrl();

        Glide.with(mContext)
                .asBitmap()
                .override(DisplayUtils.dip2px(context,94),DisplayUtils.dip2px(context,114)) //图片显示的分辨率 ，像素值 可以转化为DP再设置
                .load(coverImage)
                .centerCrop()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        holder.ivCover.setSrcBmp(resource);
                    }
                });

        //城市
        String city = item.getIssueCity();
        if (TextUtils.isEmpty(city)) {
            holder.tvCity.setVisibility(View.GONE);
        } else {
            holder.tvCity.setVisibility(View.VISIBLE);
            holder.tvCity.setText(city);
        }

        String content = item.getContentTitle();
        if (!TextUtils.isEmpty(content)) {
            holder.tvName.setVisibility(View.VISIBLE);
            holder.tvName.setText(content);
        } else {
            holder.tvName.setVisibility(View.GONE);
        }

        //话题
        List<TopicModel> topicList = new ArrayList<>();//存在的话题

        List<TopicModel> topicListItem = item.getTopicList();//话题
        if (null != topicListItem && topicListItem.size() > 0) {
            for (TopicModel topic : topicListItem) {
                if (topic.getDeleted() != 1) {
                    topicList.add(topic);
                }
            }
        }

        if (null != topicList && topicList.size() > 0) {
            holder.tvTopic.setVisibility(View.VISIBLE);
            holder.tvTopic.setText(YHStringUtils.formatTopic(topicList.get(0).getTitle()));
            holder.tvName.setMaxLines(1);
        } else {
            holder.tvName.setMaxLines(2);
            holder.tvTopic.setVisibility(View.GONE);
        }

        String userPhoto = item.getImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }

        String nickName = YHStringUtils.value(item.getNickName());
        holder.tvUserName.setText(nickName);

        String contentCategoryId = item.getContentCategoryId();
        String startTime = item.getActivityStartTime();
        String endTime = item.getActivityEndTime();

        if (!YXConfig.ALL666.equals(contentCategoryId)) {
            //爆料内容
            holder.tvAddress.setVisibility(View.GONE);
            holder.tvTime.setVisibility(View.GONE);
            holder.llBreakNewsContent.setVisibility(View.VISIBLE);

            holder.tvTimeContent.setText(YXTimeUtils.getFriendlyTimeAtContent(item.getPublishTime()));
        } else {
            //活动
            holder.tvAddress.setVisibility(View.VISIBLE);
            holder.tvTime.setVisibility(View.VISIBLE);
            holder.llBreakNewsContent.setVisibility(View.GONE);
            holder.tvAddress.setText(YHStringUtils.value(item.getIssueAddress()));
            holder.tvTime.setText(DiscoTimeUtils.getBreakNewsTopTime(startTime, endTime));
        }

        holder.rlItem.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position));

        int auditStatus = item.getAuditStatus();

        if (isMyPublish) {
            holder.rlAudlitStatus.setVisibility(View.VISIBLE);
            holder.tvAuditStatus.setText(YHStringUtils.value(item.getAuditStatusName()));

            if (auditStatus!=0&&auditStatus!=1) {
                holder.rlAudlitStatus.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.REJECT, item, position));
            }else {
                holder.rlAudlitStatus.setOnClickListener(null);
            }
        } else {
            holder.rlAudlitStatus.setOnClickListener(null);
            holder.rlAudlitStatus.setVisibility(View.GONE);
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.rlItem)
        RelativeLayout rlItem;
        @BindView(R2.id.rlAudlitStatus)
        RelativeLayout rlAudlitStatus;

        @BindView(R2.id.ivCover)
        AnyShapeImageView ivCover;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvTopic)
        TextView tvTopic;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvTimeContent)
        TextView tvTimeContent;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;

        @BindView(R2.id.tvCity)
        TextView tvCity;


        @BindView(R2.id.tvAuditStatus)
        TextView tvAuditStatus;

        @BindView(R2.id.llBreakNewsContent)
        LinearLayout llBreakNewsContent;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
