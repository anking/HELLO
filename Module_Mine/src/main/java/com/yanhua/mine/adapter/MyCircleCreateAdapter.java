package com.yanhua.mine.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.google.android.material.imageview.ShapeableImageView;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.AuditRemarkModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;

public class MyCircleCreateAdapter extends BaseRecyclerAdapter<CircleModel, MyCircleCreateAdapter.ViewHolder> {
    private Context mContext;

    public MyCircleCreateAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_circle_create, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText("◎" + item.getTitle());
        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl());
        holder.tvCircieApplyTime.setText(item.getCreatedTime() + "申请");

        //状态;0待审核 1已通过 2未通过
        int status = item.getStatusCode();
        switch (status) {
            case 0:
                holder.tvCircieDesc.setText("关注 0人");
                holder.llStatusText.setVisibility(View.VISIBLE);
                holder.tvStatusText.setText("审核中");
                holder.tvStatusText.setTextColor(Color.parseColor("#FE9D1E"));
                holder.iconNext.setVisibility(View.GONE);
                break;
            case 1:
                holder.tvCircieDesc.setText("关注 " + item.getFansCount() + "人  动态 " + item.getDynamicCount() + "条");
                holder.llStatusText.setVisibility(View.GONE);
                break;

            case 2:
                holder.tvCircieDesc.setText("关注 0人");
                holder.llStatusText.setVisibility(View.VISIBLE);

                holder.tvStatusText.setText("未通过");
                holder.tvStatusText.setTextColor(Color.parseColor("#FF4B19"));
                holder.iconNext.setVisibility(View.VISIBLE);
                break;
        }

        holder.llStatusText.setOnClickListener(v -> {
            if (status == 2) {
                // 点击效果---放到其他地方处理
                AuditRemarkModel rejectReason = item.getAuditReason();

                String name = rejectReason.getName();
                String remark = rejectReason.getRejectRemark();

                String content = name + (TextUtils.isEmpty(remark) ? "" : ("，" + remark));

                CommonDialog commonDialog = new CommonDialog(mContext, content, "驳回原因", "确定", "取消");
                new XPopup.Builder(mContext).asCustom(commonDialog).show();
                commonDialog.setOnConfirmListener(() -> {
                    commonDialog.dismiss();
                    String inviteUserID = item.getUserId();
                    //先判断是不是自己
                    if (UserManager.getInstance().isMyself(inviteUserID)) {
                        //跳转修改
                        String applyId = item.getId();//申请ID
                        String id = item.getCircleId();
                        if (!TextUtils.isEmpty(id)) {
                            //跳到圈子详情
                            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY).withString("id", id).navigation();
                        } else {
                            if (!TextUtils.isEmpty(applyId)) {
                                //跳到圈子编辑
                                ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY)
                                        .withString("applyId", applyId)
                                        .navigation();
                            }
                        }
                    }
                });

                commonDialog.setOnCancelListener(() -> {
                    commonDialog.dismiss();
                });
            }
        });

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircieApplyTime)
        TextView tvCircieApplyTime;//-申请
        @BindView(R2.id.tvCircieDesc)
        TextView tvCircieDesc;//审核结果：-
        @BindView(R2.id.llStatusText)
        LinearLayout llStatusText;
        @BindView(R2.id.tvStatusText)
        TextView tvStatusText;
        @BindView(R2.id.iconNext)
        AliIconFontTextView iconNext;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
