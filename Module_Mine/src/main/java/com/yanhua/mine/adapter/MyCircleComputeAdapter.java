package com.yanhua.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.MyCircleComputerModel;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import butterknife.BindView;


public class MyCircleComputeAdapter extends BaseRecyclerAdapter<MyCircleComputerModel, MyCircleComputeAdapter.ViewHolder> {

    private Context mContext;

    public MyCircleComputeAdapter(Context context) {
        super(context);

        mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_circle_computer, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MyCircleComputerModel item) {
        int position = getPosition(holder);

        holder.tvTime.setText(YHStringUtils.value(item.getDateType()));
        holder.tvNewFans.setText(YHStringUtils.quantityFormat(item.getAddMemberCount()));
        holder.tvNewDynamic.setText(YHStringUtils.quantityFormat(item.getAddContentCount()));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvNewFans)
        TextView tvNewFans;
        @BindView(R2.id.tvNewDynamic)
        TextView tvNewDynamic;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
