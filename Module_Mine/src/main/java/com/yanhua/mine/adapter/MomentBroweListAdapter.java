package com.yanhua.mine.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class MomentBroweListAdapter extends BaseRecyclerAdapter<MomentListModel, MomentBroweListAdapter.ViewHolder> {

    private final int SHOW_MAX_NUM = 5;
    private Context mContext;
    private OnHandleClickListener mListener;

    public MomentBroweListAdapter(Context context, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_browe_moment, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull MomentListModel item) {
        int position = getPosition(holder);

        //基础信息
        String nickName = YHStringUtils.pickName(item.getNickName(), item.getFriendRemark());
        holder.tvUserName.setText(nickName);

        String userPhoto = item.getUserPhoto();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }


        holder.llItem.setOnClickListener(v -> mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position));

        //获取图片1 视频2 文章3
        int contentType = item.getContentType();
        String content = item.getContent();
        String title = item.getContentTitle();

        String urlJson = item.getContentUrl();

        if (!TextUtils.isEmpty(urlJson)) {
            Type listType = new TypeToken<ArrayList<FileResult>>() {
            }.getType();
            ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

            List<String> imageUrl = new ArrayList<>();
            if (contentType == 2) {
                imageUrl.add(fileResults.get(0).getHttpUrl());
            } else {
                for (FileResult fileResult : fileResults) {
                    imageUrl.add(fileResult.getHttpUrl());
                }
            }
            if (!imageUrl.isEmpty()) {
                holder.rvImgContainer.setVisibility(View.VISIBLE);
                showGridImage(holder, imageUrl, contentType);
            } else {
                holder.rvImgContainer.setVisibility(View.GONE);
            }
        } else {
            holder.rvImgContainer.setVisibility(View.GONE);
        }

        //文本内容展示
        content = YHStringUtils.pickLastFirst(content, title);
        content = YHStringUtils.getHtmlContent(content);

        if (!TextUtils.isEmpty(content)) {
            content = YHStringUtils.firstAtAddSpace(content);

            holder.richTextView.setText(content);

            holder.richTextView.setVisibility(View.VISIBLE);
        } else {
            holder.richTextView.setVisibility(View.GONE);
        }
    }

    private void showGridImage(ViewHolder holder, List<String> imgUrls, int contentType) {
        int imgSize = imgUrls.size();
        int leftShowCount = imgSize - SHOW_MAX_NUM;

        if (imgSize > SHOW_MAX_NUM) {
            imgUrls = imgUrls.subList(0, SHOW_MAX_NUM);
        }

        LinearLayoutManager managerContent = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        MomentBroweImgAdapter mAdapter = new MomentBroweImgAdapter(mContext, contentType, leftShowCount);
        holder.rvImgContainer.setLayoutManager(managerContent);
        holder.rvImgContainer.setAdapter(mAdapter);

        mAdapter.setItems(imgUrls);

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.richText)
        TextView richTextView;

        @BindView(R2.id.rvImgContainer)
        RecyclerView rvImgContainer;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;

        @BindView(R2.id.llItem)
        RelativeLayout llItem;


        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
