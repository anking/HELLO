package com.yanhua.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.model.MyCircleComputerModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.util.List;

import butterknife.BindView;


public class MyCircleManageAdapter extends BaseRecyclerAdapter<CircleManageModel, MyCircleManageAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mlListener;

    public MyCircleManageAdapter(Context context, OnHandleClickListener listener) {
        super(context);

        mContext = context;
        mlListener = listener;
    }


    public interface OnHandleClickListener {
        public static int SHOW_MORE = 1;

        void onHandleClick(int type, CircleManageModel item, int pos);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_my_circle_manage, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CircleManageModel item) {
        int position = getPosition(holder);

        holder.tvCircieName.setText("◎" + YHStringUtils.value(item.getTitle()));
        holder.tvCircleNum.setText("关注 " + item.getFansCount() + "人  动态 " + item.getDynamicCount() + "条");

        ImageLoaderUtil.loadImgCenterCrop(holder.rIVCircleCover, item.getCoverUrl());


        List<MyCircleComputerModel> datas = item.getCircleStatVOList();

        if (null != datas && datas.size() > 0) {
            holder.rvContent.setVisibility(View.VISIBLE);

            LinearLayoutManager manager = new LinearLayoutManager(mContext);

            MyCircleComputeAdapter adapter = new MyCircleComputeAdapter(mContext);
            holder.rvContent.setLayoutManager(manager);
            holder.rvContent.setAdapter(adapter);

            //干掉 取出上拉下拉阴影
            holder.rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
            RecycleViewUtils.clearRecycleAnimation(holder.rvContent);
            adapter.setItems(datas);
        } else {
            holder.rvContent.setVisibility(View.GONE);
        }
        boolean showMore = item.isShowMore();
        if (!showMore) {
            holder.llData.setVisibility(View.GONE);
            holder.rlCompute.setSelected(false);
            holder.tvArrowAction.setText("\ue750");
        } else {
            holder.llData.setVisibility(View.VISIBLE);
            holder.rlCompute.setSelected(true);
            holder.tvArrowAction.setText("\ue739");
        }

        holder.rlCompute.setOnClickListener(v -> mlListener.onHandleClick(OnHandleClickListener.SHOW_MORE, item, position));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;
        @BindView(R2.id.rv_content)
        DataObserverRecyclerView rvContent;
        @BindView(R2.id.llData)
        LinearLayout llData;

        @BindView(R2.id.rlCompute)
        RelativeLayout rlCompute;
        @BindView(R2.id.tvArrowAction)
        AliIconFontTextView tvArrowAction;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
