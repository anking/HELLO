package com.yanhua.mine.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.core.util.CalendarUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.mine.R;

import java.util.Date;

/**
 * 选择约伴类型吧
 *
 * @author Administrator
 */
public class ProfileBottomPop extends BaseMvpBottomPopupView {

    private Context mContext;
    AliIconFontTextView tvCancel;

    private UserInfo userInfo;

    TextView tvEditSignature;
    TextView tvPersonSign;
    LinearLayout llAddHobby;
    TextView tvEditHobby;
    TagFlowLayout tflHobby;
    LinearLayout llAddMakeFriend;
    TextView tvEditMakeFriend;
    LinearLayout llMakeFriends;
    TextView tvMakeFriends1;
    TextView tvMakeFriends2;
    TextView tvMakeFriends3;
    TextView tvJoinDays;
    LinearLayout llAddBaseInfo;
    TextView tvEditBaseInfo;
    TextView tvProfession;
    TextView tvConstellation;
    TextView tvAge;
    TextView tvAddress;
    TextView tvEmotion;
    TextView tvWeight;
    TextView tvHeight;
    LinearLayout llBaseInfo;


    @Override
    protected void creatPresent() {
    }

    public ProfileBottomPop(@NonNull Context context, UserInfo uinfo) {
        super(context);
        mContext = context;

        userInfo = uinfo;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_popup_profile;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();
    }

    private void initView() {
        tvCancel = findViewById(R.id.iv_left);

        tvEditSignature = findViewById(R.id.tvEditSignature);
        tvPersonSign = findViewById(R.id.tvPersonSign);

        llAddHobby = findViewById(R.id.llAddHobby);
        tvEditHobby = findViewById(R.id.tvEditHobby);
        tflHobby = findViewById(R.id.tflHobby);
        llAddMakeFriend = findViewById(R.id.llAddMakeFriend);
        tvEditMakeFriend = findViewById(R.id.tvEditMakeFriend);
        llMakeFriends = findViewById(R.id.llMakeFriends);
        tvMakeFriends1 = findViewById(R.id.tvMakeFriends1);
        tvMakeFriends2 = findViewById(R.id.tvMakeFriends2);
        tvMakeFriends3 = findViewById(R.id.tvMakeFriends3);
        tvJoinDays = findViewById(R.id.tvJoinDays);
        llAddBaseInfo = findViewById(R.id.llAddBaseInfo);
        tvEditBaseInfo = findViewById(R.id.tvEditBaseInfo);
        tvProfession = findViewById(R.id.tvProfession);
        tvConstellation = findViewById(R.id.tvConstellation);
        tvAge = findViewById(R.id.tvAge);
        tvAddress = findViewById(R.id.tvAddress);
        tvEmotion = findViewById(R.id.tvEmotion);
        tvWeight = findViewById(R.id.tvWeight);
        tvHeight = findViewById(R.id.tvHeight);
        llBaseInfo = findViewById(R.id.llBaseInfo);

        tvCancel.setOnClickListener(v -> dismiss());

        initUI(userInfo);
    }


    private void initUI(UserInfo userInfo) {
        //是否可以看见私聊按钮---状态为0可显示
        int hideSetStatus = userInfo.getHideSetStatus();


        if (!TextUtils.isEmpty(userInfo.getPersonalSignature())) {
            tvPersonSign.setText(userInfo.getPersonalSignature());
        }
        if (userInfo.getUserHobbyTagVOList() != null && !userInfo.getUserHobbyTagVOList().isEmpty()) {
            llAddHobby.setVisibility(View.GONE);
            tvEditHobby.setVisibility(View.VISIBLE);
            tflHobby.setVisibility(View.VISIBLE);
            tflHobby.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserHobbyTagVOList()) {
                @Override
                public View getView(FlowLayout parent, int position, PersonTagModel model) {
                    //加载tag布局
                    TextView tvTag = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_tag_sign, parent, false);
                    tvTag.setText(model.getTagName());
                    return tvTag;
                }
            });
        } else {
            llAddHobby.setVisibility(View.VISIBLE);
            tvEditHobby.setVisibility(View.GONE);
            tflHobby.setVisibility(View.GONE);
        }

        String makeFriendType = userInfo.getMakeFriendType();
        if (TextUtils.isEmpty(makeFriendType)) {
            llAddMakeFriend.setVisibility(View.VISIBLE);
            tvEditMakeFriend.setVisibility(View.GONE);
            llMakeFriends.setVisibility(View.GONE);
        } else {
            llAddMakeFriend.setVisibility(View.GONE);
            tvEditMakeFriend.setVisibility(View.VISIBLE);
            llMakeFriends.setVisibility(View.VISIBLE);
            String[] arr = makeFriendType.split(",");
            if (arr.length == 3) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.VISIBLE);
                tvMakeFriends3.setVisibility(View.VISIBLE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
                tvMakeFriends2.setText(arr[1].split("`~`")[1]);
                tvMakeFriends3.setText(arr[2].split("`~`")[1]);
            } else if (arr.length == 2) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.VISIBLE);
                tvMakeFriends3.setVisibility(View.GONE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
                tvMakeFriends2.setText(arr[1].split("`~`")[1]);
            } else if (arr.length == 1) {
                tvMakeFriends1.setVisibility(View.VISIBLE);
                tvMakeFriends2.setVisibility(View.GONE);
                tvMakeFriends3.setVisibility(View.GONE);
                tvMakeFriends1.setText(arr[0].split("`~`")[1]);
            }
        }
        tvJoinDays.setText(String.format("加入音圈%d天", userInfo.getJoinDays()));

        if (!TextUtils.isEmpty(userInfo.getHometown()) || !TextUtils.isEmpty(userInfo.getAddress()) || userInfo.getHeight() != 0 || userInfo.getWeight() != 0 ||
                !TextUtils.isEmpty(userInfo.getBirthday()) || !TextUtils.isEmpty(userInfo.getEmotionTypeId()) || !TextUtils.isEmpty(userInfo.getOccupationTypeId())) {
            llAddBaseInfo.setVisibility(View.GONE);
            tvEditBaseInfo.setVisibility(View.VISIBLE);
            llBaseInfo.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(userInfo.getOccupationTypeName())) {
                tvProfession.setText(userInfo.getOccupationTypeName());
            } else {
                tvProfession.setText("-");
            }

            if (!TextUtils.isEmpty(userInfo.getAddress())) {
                tvAddress.setText(userInfo.getAddress().replace(",", ""));
            } else {
                tvAddress.setText("-");
            }

            if (!TextUtils.isEmpty(userInfo.getEmotionTypeName())) {
                tvEmotion.setText(userInfo.getEmotionTypeName());
            } else {
                tvEmotion.setText("-");
            }

            if (userInfo.getHeight() != 0) {
                tvHeight.setText(String.format("%dcm", userInfo.getHeight()));
            }

            if (userInfo.getWeight() != 0) {
                tvWeight.setText(String.format("%dkg", userInfo.getWeight()));
            }

            String birthday = userInfo.getBirthday();
            if (userInfo.getConstellationState() == 1) {
                if (!TextUtils.isEmpty(birthday)) {
                    tvAge.setVisibility(View.VISIBLE);
                    tvConstellation.setVisibility(View.VISIBLE);
                    tvConstellation.setText(CalendarUtils.getConstellation(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd")));
                    tvAge.setText(String.format("%d岁", CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"), new Date())));
                } else {
                    tvAge.setVisibility(View.GONE);
                    tvConstellation.setVisibility(View.GONE);
                }
            } else {
                if (!TextUtils.isEmpty(birthday)) {
                    tvAge.setVisibility(View.VISIBLE);
                    tvAge.setText(String.format("%d岁", CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"), new Date())));
                } else {
                    tvAge.setVisibility(View.GONE);
                }
                tvConstellation.setText("保密");
            }
        } else {
            llAddBaseInfo.setVisibility(View.VISIBLE);
            tvEditBaseInfo.setVisibility(View.GONE);
            llBaseInfo.setVisibility(View.GONE);
        }

        tvEditBaseInfo.setVisibility(View.GONE);
        tvEditMakeFriend.setVisibility(View.GONE);
        tvEditSignature.setVisibility(View.GONE);
        tvEditHobby.setVisibility(View.GONE);
    }

    @Override
    protected int getMaxHeight() {
        return DisplayUtils.getScreenHeight(mContext) * 3 / 4;
    }
}
