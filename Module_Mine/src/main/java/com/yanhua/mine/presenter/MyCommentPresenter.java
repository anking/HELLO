package com.yanhua.mine.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MineHttpMethod;
import com.yanhua.common.model.MineCommentModel;
import com.yanhua.mine.presenter.contract.MyCommentContract;

import java.util.HashMap;

public class MyCommentPresenter extends MvpPresenter<MyCommentContract.IView> implements MyCommentContract.Presenter {

    public void commentListQuery(HashMap params) {

        addSubscribe(MineHttpMethod.getInstance().commentListQuery(new SimpleSubscriber<HttpResult<ListResult<MineCommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MineCommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleMineCommentList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }
}
