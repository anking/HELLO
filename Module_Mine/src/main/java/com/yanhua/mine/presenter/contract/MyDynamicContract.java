package com.yanhua.mine.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.MomentListModel;

public interface MyDynamicContract {
    interface IView extends BaseView {
        default void handleUserLikeList(ListResult<MomentListModel> listResult) {

        }

        default void handleMyMomentList(ListResult<MomentListModel> listResult) {

        }

        default void updateCollectContentSuccess() {
        }

        default void updateStarContentSuccess() {
        }

        default void updateFollowUserSuccess() {
        }

        default void handleDeleteContentSuccess() {
        }

        default void handledeleteLikeContent(int pos){}
    }

    interface Presenter extends BasePresenter<IView> {

        void updateCollectContent(String id, int type);

        void updateStarContent(String id, int type);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void getMyMomentList(String title, String uid, int current, int size);
    }
}
