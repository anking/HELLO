package com.yanhua.mine.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.MineHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.mine.presenter.contract.MyDynamicContract;

import java.util.HashMap;

public class MyDynamicPresenter extends MvpPresenter<MyDynamicContract.IView> implements MyDynamicContract.Presenter {

    public void deleteContent(String id) {
        addSubscribe(MomentHttpMethod.getInstance().deleteContent(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }


    @Override
    public void getMyMomentList(String title, String uid, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getMomentUserList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMyMomentList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, uid, current, size));
    }

    public void getMyFavMomentList(String title, String uid, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getMyFavMomentList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMyMomentList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, uid, current, size));
    }

    public void getUserLikeList(int current, int size) {
        HashMap<String,Object> params = new HashMap<>();
        params.put("current",current);
        params.put("size",size);

        addSubscribe(MineHttpMethod.getInstance().getUserLikeList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleUserLikeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));

//        addSubscribe(MomentHttpMethod.getInstance().getUserLikeList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
//            @Override
//            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
//                super.onNext(result);
//                if (result != null) {
//                    if (result.getCode().equals(HttpCode.SUCCESS)) {
//                        ListResult<MomentListModel> listResult = result.getData();
//                        baseView.handleUserLikeList(listResult);
//                    } else {
//                        baseView.handleErrorMessage(result.getMesg());
//                    }
//                }
//            }
//        },"", UserManager.getInstance().getUserId(), current, size));
    }

    public void deleteLikeContent(String id, int pos) {
        addSubscribe(CommonHttpMethod.getInstance().deleteLikeNullContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handledeleteLikeContent(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }
}
