package com.yanhua.mine.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.MyNewsListModel;

public interface MyBrowseContract {
    interface IView extends BaseView {
        default void handInviteList(ListResult<InviteModel> listResult) {
        }

        default void handleNews2MeList(ListResult<MyNewsListModel> data) {
        }

        default void handleDiscoCollectList(ListResult<MomentListModel> data) {
        }

        default void handleMomentBroweList(ListResult<MomentListModel> data) {
        }

        default void updateCollectContentSuccess() {
        }

        default void updateStarContentSuccess() {
        }

        default void handleCollectBarListSuccess(ListResult<BarModel> data) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
    }
}
