package com.yanhua.mine.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.HashMap;

public class MyBrowsePresenter extends MvpPresenter<MyBrowseContract.IView> implements MyBrowseContract.Presenter {

    public void getMyCollectBarList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getMyCollectBarList(new SimpleSubscriber<HttpResult<ListResult<BarModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BarModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleCollectBarListSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    public void meetPartnerHistoryList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerHistoryList(new SimpleSubscriber<HttpResult<ListResult<InviteModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<InviteModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }


    //爆料或攻略流量历史
    public void newsViewHistoryList(HashMap<String, Object> params) {

        addSubscribe(HomeHttpMethod.getInstance().newsViewHistoryList(new SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MyNewsListModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    //我的---爆料或者攻略
                    baseView.handleNews2MeList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }


    //我的动态爆料或攻略
    public void newsDynamicList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().newsDynamicList(new SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MyNewsListModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    //我的---爆料或者攻略
                    baseView.handleNews2MeList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    //我的收藏爆料或攻略
    public void newsCollectList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().newsCollectList(new SimpleSubscriber<HttpResult<ListResult<MyNewsListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MyNewsListModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    //我的---爆料或者攻略
                    baseView.handleNews2MeList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void getDiscoCollectList(int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getDiscoCollectList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleDiscoCollectList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, current, size));
    }

    public void getMomentBroweList(int current, int size, String keyword) {
        addSubscribe(MomentHttpMethod.getInstance().getMomentBroweList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleMomentBroweList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, current, size, keyword));
    }

    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }
}
