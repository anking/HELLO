package com.yanhua.mine.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.model.InviteModel;
import com.yanhua.mine.presenter.contract.MyInviteContract;

import java.util.HashMap;

public class MyInvitePresenter extends MvpPresenter<MyInviteContract.IView> implements MyInviteContract.Presenter {

    @Override
    public void meetPartnerMyInvitePage(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerMyInvitePage(new SimpleSubscriber<HttpResult<ListResult<InviteModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<InviteModel> data = (ListResult<InviteModel>) httpResult.getData();
                    baseView.handMyInviteList(data);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

}
