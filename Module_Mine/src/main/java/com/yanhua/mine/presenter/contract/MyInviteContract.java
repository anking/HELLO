package com.yanhua.mine.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.InviteModel;

import java.util.HashMap;

public interface MyInviteContract {
    interface IView extends BaseView {
        void handMyInviteList(ListResult<InviteModel> listResult);
    }

    interface Presenter extends BasePresenter<IView> {
        void meetPartnerMyInvitePage(HashMap<String, Object> params);
    }
}
