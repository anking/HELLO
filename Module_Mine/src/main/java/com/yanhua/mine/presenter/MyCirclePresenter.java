package com.yanhua.mine.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.mine.presenter.contract.MyCircleContract;

public class MyCirclePresenter extends MvpPresenter<MyCircleContract.IView> implements MyCircleContract.Presenter {

    public void getCircleApplyList(int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleApplyList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size));
    }
    public void getCircleJoinedList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleJoinedList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleModel> listResult = result.getData();
                        baseView.handleCircleList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }
    public void getCircleManageList(String title, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getCircleManageList(new SimpleSubscriber<HttpResult<ListResult<CircleManageModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleManageModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CircleManageModel> listResult = result.getData();
                        baseView.handleCircleManageList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, title, current, size));
    }
}
