package com.yanhua.mine.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.model.CircleModel;

public interface MyCircleContract {
    interface IView extends BaseView {
        default void handleCircleList(ListResult<CircleModel> listResult) {
        }


        default void handleCircleManageList(ListResult<CircleManageModel> listResult) {
        }

    }

    interface Presenter extends BasePresenter<IView> {
    }
}
