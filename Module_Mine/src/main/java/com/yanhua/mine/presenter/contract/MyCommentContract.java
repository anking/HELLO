package com.yanhua.mine.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.MineCommentModel;

public interface MyCommentContract {
    interface IView extends BaseView {
        void handleMineCommentList(ListResult<MineCommentModel> listResult);
    }

    interface Presenter extends BasePresenter<IView> {
    }
}
