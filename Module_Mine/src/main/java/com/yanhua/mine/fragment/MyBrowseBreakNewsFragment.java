package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.NormalDecoration;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyBreakNewsAdapter;
import com.yanhua.mine.presenter.MyBrowsePresenter;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


/**
 * 我的浏览-爆料
 */
public class MyBrowseBreakNewsFragment extends BaseMvpFragment<MyBrowsePresenter> implements MyBrowseContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int total;

    private MyBreakNewsAdapter mAdapter;
    private List<MyNewsListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyBrowsePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        refreshLayout.autoRefresh();
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            HashMap<String, Object> params = new HashMap<>();
            params.put("current", current);
            params.put("size", size);
            params.put("type", YXConfig.TYPE_BREAK_NEWS);
            params.put("userId", UserManager.getInstance().getUserId());

            basePresenter.newsViewHistoryList(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });


        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyBreakNewsAdapter(mContext, (OnHandleClickListener<MyNewsListModel>) (type, item, pos) -> {
            String id = item.getId();
            switch (type) {
                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情
                    PageJumpUtil.jumpNewsDetailPage(id, YXConfig.TYPE_BREAK_NEWS);
                    break;
                default:
                    break;
            }
        });
        rvContent.setLayoutManager(manager);

        final NormalDecoration decoration = new NormalDecoration() {
            @Override
            public String getHeaderName(int pos) {
                return mListData.get(pos).getBrowseDay();
            }
        };

        rvContent.addItemDecoration(decoration);
        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handleNews2MeList(ListResult<MyNewsListModel> listResult) {

        List<MyNewsListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            if (list != null && !list.isEmpty()) {
                for (MyNewsListModel itemModel : list) {
                    String date = itemModel.getViewTime();
                    if (!TextUtils.isEmpty(date)) {
                        int subMonth = Integer.parseInt(date.substring(5, 7));//2022-04-18T03:07:03.105Z
                        int subDay = Integer.parseInt(date.substring(8, 10));//2022-04-18T03:07:03.105Z
                        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                        String showDayStr = String.format("%d月%d日", subMonth, subDay);
                        if (subDay == day) {
                            showDayStr = "今日";
                        }
                        itemModel.setBrowseDay(showDayStr);
                    } else {
                        itemModel.setBrowseDay("今日");//
                    }
                }
                mListData.addAll(list);
            }

            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
