package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyAnswerAdapter;
import com.yanhua.mine.adapter.MyQuestionAdapter;
import com.yanhua.mine.presenter.BarPresenter;
import com.yanhua.mine.presenter.contract.BarContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class MineQAFragmentg extends BaseMvpFragment<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_qa)
    DataObserverRecyclerView rvQA;
    @BindView(R2.id.ctlQA)
    CommonTabLayout ctlQA;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyView;

    private int total;
    private List<MyQAModel> qaList;
    private MyQuestionAdapter questionAdapter;
    private MyAnswerAdapter answerAdapter;
    private String userId;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_qa;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }

        qaList = new ArrayList<>();
        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("提问", 0, 0));
        tabEntityList.add(new TabEntity("回答", 0, 0));
        ctlQA.setTabData(tabEntityList);
        ctlQA.setCurrentTab(0);
        ctlQA.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    rvQA.setAdapter(questionAdapter);
                } else if (position == 1) {
                    rvQA.setAdapter(answerAdapter);
                }
                current = 1;
                getDataList();
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getDataList();
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (qaList.size() < total) {
                current++;
                getDataList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        rvQA.setLayoutManager(new LinearLayoutManager(mContext));
        questionAdapter = new MyQuestionAdapter(mContext);
        answerAdapter = new MyAnswerAdapter(mContext);
        rvQA.setAdapter(questionAdapter);
        questionAdapter.setOnItemClickListener((itemView, pos) -> {
            if (FastClickUtil.isFastClick(500)) {
                // 跳转到提问详情页面
                MyQAModel myQAModel = qaList.get(pos);
                ARouter.getInstance()
                        .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                        .withString("askId", myQAModel.getAskedId())
                        .navigation();
            }
        });
        answerAdapter.setOnItemClickListener((itemView, pos) -> {
            if (FastClickUtil.isFastClick(500)) {
                // 跳转到回答详情页面
                MyQAModel myQAModel = qaList.get(pos);
                ARouter.getInstance()
                        .build(ARouterPath.BAR_ANSWER_DETAIL_ACTIVITY)
                        .withString("askId", myQAModel.getAskedId())
                        .navigation();
            }
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
//        refreshLayout.autoRefresh(100);
        getDataList();
    }

    private void getDataList() {
        if (!UserManager.getInstance().isLogin()) {
            return;
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("current", current);
        params.put("size", size);
        params.put("replyStatus", ctlQA.getCurrentTab());
        basePresenter.getMyBarQAList(ctlQA.getCurrentTab(), params);
    }

    @Override
    public void handleMyQAListSuccess(int type, ListResult<MyQAModel> list) {
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            qaList.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        List<MyQAModel> records = list.getRecords();
        total = list.getTotal();
        if (records != null && !records.isEmpty()) {
            qaList.addAll(records);
        }
        if (type == 0) {
            if (qaList.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setErrorType(EmptyLayout.NODATA);
                emptyView.setErrorMessage("暂无提问");
                rvQA.setEmptyView(emptyView);
            } else {
                emptyView.setVisibility(View.GONE);
                questionAdapter.setItems(qaList);
            }
        } else if (type == 1) {
            if (qaList.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setErrorType(EmptyLayout.NODATA);
                emptyView.setErrorMessage("暂无回答");
                rvQA.setEmptyView(emptyView);
            } else {
                emptyView.setVisibility(View.GONE);
                answerAdapter.setItems(qaList);
            }
        }
    }

}
