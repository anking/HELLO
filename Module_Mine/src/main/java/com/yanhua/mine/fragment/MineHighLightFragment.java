package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.utils.UserManager;
import com.yanhua.mine.R;

public class MineHighLightFragment extends BaseMvpFragment {


    private String userId;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_highlight;
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }

        if (!UserManager.getInstance().isLogin()) {
            return;
        }
    }
}
