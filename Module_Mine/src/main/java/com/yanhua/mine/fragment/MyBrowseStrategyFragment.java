package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyPublishStrategyAdapter;
import com.yanhua.mine.presenter.MyBrowsePresenter;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的历史-攻略
 */
public class MyBrowseStrategyFragment extends BaseMvpFragment<MyBrowsePresenter> implements MyBrowseContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int total;

    private MyPublishStrategyAdapter mAdapter;
    private List<MyNewsListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_my_strategy;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyBrowsePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);


        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            HashMap<String, Object> params = new HashMap<>();
            params.put("current", current);
            params.put("size", size);
            params.put("type", YXConfig.TYPE_STRATEGY);
            params.put("userId", UserManager.getInstance().getUserId());

            basePresenter.newsViewHistoryList(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.setLayoutManager(managerContent);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new MyPublishStrategyAdapter(mContext, (OnHandleClickListener<MyNewsListModel>) (type, item, pos) -> {

            String id = item.getId();
            switch (type) {

                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情
                    PageJumpUtil.jumpNewsDetailPage(id, YXConfig.TYPE_STRATEGY);
                    break;
                default:
                    break;
            }
        });
        rvContent.setLayoutManager(managerContent);

        rvContent.setAdapter(mAdapter);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Override
    public void handleNews2MeList(ListResult<MyNewsListModel> listResult) {

        List<MyNewsListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

}
