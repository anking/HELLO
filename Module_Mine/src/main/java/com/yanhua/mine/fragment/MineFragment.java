package com.yanhua.mine.fragment;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.flyco.tablayout.SlidingTabLayout;
import com.github.dfqin.grantor.PermissionListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.Channel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.GlideRoundTransform;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.CalendarUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.core.widget.expandabletext.ExpandableTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MinePageAdapter;
import com.yanhua.mine.view.ProfileBottomPop;
import com.yanhua.rong.msgprovider.UserPageMessage;
import com.yanhua.user.presenter.PersonalPagePresenter;
import com.yanhua.user.presenter.contract.PersonalPageContract;
import com.yanhua.user.widget.SetRemarkPopup;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.listener.OnPageChangeListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imkit.utils.RouteUtils;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.TextMessage;

public class MineFragment extends BaseMvpFragment<PersonalPagePresenter> implements PersonalPageContract.IView {

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.ivUserTopHead)
    CircleImageView ivUserTopHead;
    @BindView(R2.id.tvUserTopName)
    TextView tvUserTopName;
    @BindView(R2.id.iconSet)
    AliIconFontTextView iconSet;

    @BindView(R2.id.ivUserHead)
    CircleImageView ivUserHead;

    @BindView(R2.id.tvUserName)
    TextView tvUserName;

    @BindView(R2.id.iv_left)
    AliIconFontTextView iconLeft;

    @BindView(R2.id.viewTopDivide)
    View viewTopDivide;


    @BindView(R2.id.bannerPhotoWall)
    Banner bannerPhotoWall;
    @BindView(R2.id.tvIndex)
    TextView tvIndex;
    @BindView(R2.id.tflTag)
    TagFlowLayout tflTag;

    @BindView(R2.id.llVerifybyName)
    LinearLayout llVerifybyName;
    @BindView(R2.id.tvVerifyByName)
    TextView tvVerifyByName;
    @BindView(R2.id.tvVerfiyArrow)
    AliIconFontTextView tvVerfiyArrow;

    @BindView(R2.id.tvIdentityId)
    TextView tvIdentityId;

    @BindView(R2.id.tvEditName)
    AliIconFontTextView tvEditName;
    @BindView(R2.id.tvEditInfo)
    TextView tvEditInfo;
    @BindView(R2.id.tvAgeAddress)
    TextView tvAgeAddress;

    @BindView(R2.id.fl_backgroud)
    FrameLayout flBackgroud;
    @BindView(R2.id.rlNoPhoto)
    RelativeLayout rlNoPhoto;
    @BindView(R2.id.rlPhoto)
    RelativeLayout rlPhoto;
    @BindView(R2.id.ivInfoSex)
    ImageView ivInfoSex;

    @BindView(R2.id.stabLayout)
    LinearLayout stabLayout;

    @BindView(R2.id.llFans)
    LinearLayout llFans;
    @BindView(R2.id.llFollows)
    LinearLayout llFollows;
    @BindView(R2.id.llLooks)
    LinearLayout llLooks;

    @BindView(R2.id.tvFansNum)
    OswaldTextView tvFansNum;
    @BindView(R2.id.tvFollowNum)
    OswaldTextView tvFollowNum;
    @BindView(R2.id.tvLooksNum)
    OswaldTextView tvLooksNum;
    @BindView(R2.id.tvIntroduce)
    ExpandableTextView tvIntroduce;
    @BindView(R2.id.tvPersonTag)
    TextView tvPersonTag;
    @BindView(R2.id.rlPersonTag)
    RelativeLayout rlPersonTag;

    @BindView(R2.id.tvMakeFriendType)
    TextView tvMakeFriendType;

    @BindView(R2.id.llShareApp)
    LinearLayout llShareApp;

    @BindView(R2.id.llUserInfo)
    LinearLayout llUserInfo;
    @BindView(R2.id.rlContent)
    RelativeLayout rlContent;

    @BindView(R2.id.llMyMenu)
    LinearLayout llMyMenu;
    @BindView(R2.id.llAppMenu)
    LinearLayout llAppMenu;
    @BindView(R2.id.iconPullDU)
    AliIconFontTextView iconPullDU;

    @BindView(R2.id.ll_login)
    LinearLayout llLogin;
    @BindView(R2.id.sltab)
    SlidingTabLayout sltab;
    @BindView(R2.id.view_pager)
    ViewPager viewPager;

    @BindView(R2.id.ll_bottom)
    LinearLayout ll_bottom;
    @BindView(R2.id.btChat)
    LinearLayout btChat;

    @BindView(R2.id.btTopFollow)
    StatusView btTopFollow;
    @BindView(R2.id.btFollow)
    LinearLayout btFollow;
    @BindView(R2.id.iconFollow)
    AliIconFontTextView iconFollow;
    @BindView(R2.id.tvBottomFollow)
    TextView tvBottomFollow;

    private List<Channel> mChannelList;
    private MinePageAdapter mAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new PersonalPagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        int width = DisplayUtils.getScreenWidth(mContext);
        ViewGroup.LayoutParams imageLayoutParams = flBackgroud.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = width;
        flBackgroud.setLayoutParams(imageLayoutParams);

        setTopView();

        EventBus.getDefault().register(this);
    }

    private void initTabView() {
 /*       if (!UserManager.getInstance().isLogin()) {
            stabLayout.setVisibility(View.GONE);

            if (mChannelList != null && null != mAdapter) {
                mChannelList.clear();

                mAdapter = new MinePageAdapter(getChildFragmentManager(), mChannelList, userId);
                viewPager.setAdapter(mAdapter);
                sltab.setViewPager(viewPager);
                sltab.setCurrentTab(0);
            }

            return;
        }

        stabLayout.setVisibility(View.VISIBLE);*/


        mChannelList = new ArrayList<>();//(1-此刻,2-爆料攻略,3-集锦,4-约伴,5-问答)

        Channel momentMC = new Channel("MOMENT", "此刻", "此刻", 1);
        Channel breaknewsMC = new Channel("BREAKNEWS", "爆料", "爆料", 2);
        Channel strategygenewsMC = new Channel("STRATEGYNEWS", "攻略", "攻略", 3);
        Channel highlightMC = new Channel("HIGHLIGHT", "集锦", "集锦", 4);
        Channel inviteMC = new Channel("INVITE", "约伴", "约伴", 5);
        Channel qaMC = new Channel("QA", "问答", "问答", 6);

        mChannelList.add(momentMC);
        mChannelList.add(breaknewsMC);
        mChannelList.add(strategygenewsMC);
        mChannelList.add(highlightMC);
        mChannelList.add(inviteMC);
        mChannelList.add(qaMC);

        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new MinePageAdapter(getChildFragmentManager(), mChannelList, userId);
            viewPager.setAdapter(mAdapter);
            sltab.setViewPager(viewPager);
            sltab.setCurrentTab(0);
        }
    }

    String userIDTemp;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        boolean isLogin = UserManager.getInstance().isLogin();

        imgPath = new ArrayList<>();

        if (null != bundle) {
            userIDTemp = bundle.getString("userId");
        }
        if (TextUtils.isEmpty(userIDTemp)) {
            userId = mMMKV.getUserId();
        } else {
            userId = userIDTemp;
        }

        if (!TextUtils.isEmpty(userId)) {
            basePresenter.getPersonalInfo(userId);
        }
        if (isLogin) {
            basePresenter.getStsToken();
        }

        setUserInfo();
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;

            float rangle = scroll2top / scrollRangle;
            float alpha = 1 - rangle;
            if (alpha >= 0 && alpha < 0.5) {
                int a = (int) (alpha * 2 * 255);
                if (a > 255) {
                    a = 255;
                }
                tvUserTopName.setAlpha(alpha);
                ivUserTopHead.setAlpha(alpha);
                toolbar.setBackgroundColor(Color.argb(a, 255, 255, 255));
            } else {
                tvUserTopName.setAlpha(1.f);
                ivUserTopHead.setAlpha(1.f);
                toolbar.setBackgroundResource(R.color.white);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    private void chat2ThisPerson() {
        if (userId.equals(mMMKV.getUserId())) {
            ToastUtils.showShort("不能给自己发私信");
            return;
        }

        RouteUtils.routeToConversationActivity(mContext, Conversation.ConversationType.PRIVATE, userId, null);
    }

    private boolean isShowAppMenu;
    private String userId;


    private static final int CHOOSE_PHOTO = 0x001;

    private void chooseImage() {
        PictureSelector.create(mActivity)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(8)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(1, 1)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(false)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(false)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(new OnResultCallbackListener<LocalMedia>() {
                    @Override
                    public void onResult(List<LocalMedia> result) {
                        if (!result.isEmpty()) {
                            LocalMedia item = result.get(0);
                            uploadFile(item.getRealPath());
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    private void checkStoragePermission() {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImage();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }


    @OnClick({R2.id.iconSet, R2.id.rlNoPhoto, R2.id.ivUserTopHead, R2.id.ll_login, R2.id.ivUserHead, R2.id.tvUserName, R2.id.tvEditInfo,
            R2.id.llFans, R2.id.llFollows, R2.id.llMineLike, R2.id.llMineShare, R2.id.llMineClient, R2.id.iv_left,
            R2.id.llMineCertify, R2.id.iconPullDU, R2.id.btFollow, R2.id.btTopFollow, R2.id.btChat, R2.id.tvEditName, R2.id.llShareApp,
            R2.id.llMineComment, R2.id.llMineFav, R2.id.llMineCircle, R2.id.llMineHistory, R2.id.llMineAdvice, R2.id.llMineQuestion})
    public void onViewClick(View v) {
        int id = v.getId();

        if (id == R.id.iv_left) {
            getActivity().onBackPressed();
        } else if (id == R.id.rlNoPhoto) {
            checkStoragePermission();
        } else if (id == R.id.iconSet) {
            if (null == userInfo) {
                ARouter.getInstance()
                        .build(ARouterPath.SET_ACTIVITY)
                        .navigation();
                return;
            }

            if (UserManager.getInstance().isMyself(userInfo.getId())) {
                ARouter.getInstance()
                        .build(ARouterPath.SET_ACTIVITY)
                        .navigation();
            } else {
                showShareBoadr();
            }
        } else if (id == R.id.btChat) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                chat2ThisPerson();
            });
        } else if (id == R.id.btTopFollow || id == R.id.btFollow) {
            PageJumpUtil.firstIsLoginThenJump(() -> {

                if (userInfo.getRelationState() != 2 && userInfo.getRelationState() != 4) {
                    // 关注
                    basePresenter.followUser(userId);
                } else {
                    // 取关
                    basePresenter.cancelFollowUser(userId);
                }
            });


        } else if (id == R.id.iconPullDU) {
            isShowAppMenu = !isShowAppMenu;
            iconPullDU.setText(isShowAppMenu ? "\ue739" : "\ue750");

            if (isShowAppMenu) {
                llAppMenu.setVisibility(View.VISIBLE);
            } else {
                llAppMenu.setVisibility(View.GONE);
            }
        } else if (id == R.id.ll_login) {//是统一跳到登录还是各个击破
            PageJumpUtil.firstIsLoginThenJump(() -> {
            });
        } else if (id == R.id.llMineCertify) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                int realNameValid = UserManager.getInstance().getUserInfo().getRealNameValid();
                //实名认证
                ARouter.getInstance().build(realNameValid != 1 ? ARouterPath.CERTIFY_CENTER_ACTIVITY : ARouterPath.CERTIFY_RESULT_ACTIVITY)
                        .withInt("result", realNameValid)
                        .navigation();
            });
        } else if (id == R.id.ivUserTopHead || id == R.id.ivUserHead || id == R.id.tvUserName) {
//            PageJumpUtil.firstIsLoginThenJump(() -> {
//                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
//                        .withString("userId", UserManager.getInstance().getUserId()).withBoolean("bindStatus", false).navigation();
//            });
        } else if (id == R.id.llFans) {
            if (UserManager.getInstance().isMyself(userInfo.getId())) {

                PageJumpUtil.firstIsLoginThenJump(() -> {
                    ARouter.getInstance().build(ARouterPath.RELATION_USER_LIST_ACTIVITY).withString("userId", UserManager.getInstance().getUserId())
                            .withInt("type", 1)
                            .navigation();
                });
            }
        } else if (id == R.id.llFollows) {
            if (UserManager.getInstance().isMyself(userInfo.getId())) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    ARouter.getInstance().build(ARouterPath.RELATION_USER_LIST_ACTIVITY).withString("userId", UserManager.getInstance().getUserId())
                            .withInt("type", 0)
                            .navigation();
                });
            }
        } else if (id == R.id.llMineLike) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_LIKE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineComment) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MINE_COMMENT_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineFav) {//我的收藏
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_FAV_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineCircle) {//我的圈子
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_CIRCLE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineHistory) {//浏览历史
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_BROWSE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineShare) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.SHARE_FRIENDS_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineClient) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.CONTACT_CS_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineAdvice) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.FEEDBACK_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineQuestion) {
            ARouter.getInstance().build(ARouterPath.COMMON_PROBLEM_ACTIVITY)
                    .navigation();
        } else if (id == R.id.llShareApp) {
            //.isLightStatusBar(true)不会改变状态栏的颜色
            if (null == userInfo) return;
            showShareBoadr();
        } else if (id == R.id.tvEditInfo) {
            if (UserManager.getInstance().isMyself(userInfo.getId())) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    ARouter.getInstance().build(ARouterPath.PERSONAL_INFO_ACTIVITY)
                            .navigation();
                });
            } else {
                //展示更多Dialog
                showProfileBottomPop();
            }
        } else if (id == R.id.tvEditName) {
            SetRemarkPopup setRemarkPopup = new SetRemarkPopup(mContext);
            setRemarkPopup.setOnSetRemarkListener(remark -> basePresenter.setFriendRemark(userId, remark));
            new XPopup.Builder(mContext).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(setRemarkPopup).show();
        }
    }

    private void showProfileBottomPop() {
        ProfileBottomPop popup = new ProfileBottomPop(mContext, userInfo);
        new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
    }

    private void showShareBoadr() {
        //他人主页
        String nickName = userInfo.getNickName();
        String headPic = userInfo.getImg();

        MoreActionBoard.sharePersonPageDetail(mActivity, userId, nickName, headPic, isBlock, (position, tp) -> {
            switch (position) {
                case ShareBoardPopup.BOARD_SET_REPORT:
                    PageJumpUtil.firstIsLoginThenJump(() -> toReport());
                    break;

                case ShareBoardPopup.BOARD_SHARE_APP:
                    //.isLightStatusBar(true)不会改变状态栏的颜色
                    if (null == userInfo) return;
                    SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, userInfo, YXConfig.TYPE_USER_PAGE, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                    new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                            .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                            .autoFocusEditText(false)
                            .moveUpToKeyboard(false).asCustom(sharePopup).show();
                    break;
                case ShareBoardPopup.BOARD_SET_BLACK:
                    if (isBlock) {
                        RongIMClient.getInstance().removeFromBlacklist(userId, new RongIMClient.OperationCallback() {
                            @Override
                            public void onSuccess() {
                                isBlock = false;

                                basePresenter.addOrCancelBlack(userId);
                            }

                            @Override
                            public void onError(RongIMClient.ErrorCode errorCode) {

                            }
                        });
                    } else {
                        CommonDialog commonDialog = new CommonDialog(mContext, "拉黑后，对方将无法搜索到你，也不能再给你发私信", "", "确定", "取消",
                                mContext.getResources().getColor(R.color.color_F42603),
                                mContext.getResources().getColor(R.color.color_EEEEEE));
                        new XPopup.Builder(mContext).asCustom(commonDialog).show();
                        commonDialog.setOnConfirmListener(() -> {
                            commonDialog.dismiss();

                            RongIMClient.getInstance().addToBlacklist(userId, new RongIMClient.OperationCallback() {
                                @Override
                                public void onSuccess() {
                                    isBlock = true;

                                    basePresenter.addOrCancelBlack(userId);
                                }

                                @Override
                                public void onError(RongIMClient.ErrorCode errorCode) {

                                }
                            });
                        });

                        commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
                    }
                    break;
            }
        });
    }

    /**
     * 举报用户主页
     */
    private void toReport() {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", userId)
                    .withInt("reportType", YXConfig.reportType.person)
                    .navigation();
        });
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
    }

    private boolean loginGetUserInfo;//登录状态重新进来
    private UserInfo userInfo;

    @Override
    public void onResume() {
        super.onResume();

//        setUserInfo();
        if (TextUtils.isEmpty(userIDTemp)) {
            userId = mMMKV.getUserId();
        } else {
            userId = userIDTemp;
        }
    }

    private void scrollToTop() {
        CoordinatorLayout.Behavior behavior =
                ((CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams()).getBehavior();
        if (behavior instanceof AppBarLayout.Behavior) {
            AppBarLayout.Behavior appBarLayoutBehavior = (AppBarLayout.Behavior) behavior;
            int height = mAppBarLayout.getHeight();
            appBarLayoutBehavior.setTopAndBottomOffset(height);
            mAppBarLayout.requestLayout();
        }

        tvUserTopName.setAlpha(0.f);
        ivUserTopHead.setAlpha(0.f);
        toolbar.setBackgroundResource(R.color.transparent);
    }

    private void setUserInfo() {
        initTabView();

        if (!UserManager.getInstance().isLogin() && null == userInfo) {
            llLogin.setVisibility(View.VISIBLE);
            rlNoPhoto.setVisibility(View.GONE);
            rlPhoto.setVisibility(View.GONE);

            iconLeft.setVisibility(View.GONE);
            viewTopDivide.setVisibility(View.VISIBLE);

            ll_bottom.setVisibility(View.GONE);

            btTopFollow.setVisibility(View.GONE);
            btFollow.setVisibility(View.GONE);

            llUserInfo.setVisibility(View.GONE);

            tflTag.setVisibility(View.GONE);

            tvFansNum.setText("-");
            tvFollowNum.setText("-");
            tvLooksNum.setText("-");
            tvIntroduce.setContent("-");

            tvUserTopName.setText("");
            tvUserName.setText("");

            llMyMenu.setVisibility(View.VISIBLE);
            llAppMenu.setVisibility(View.VISIBLE);
            iconPullDU.setVisibility(View.INVISIBLE);

            ivUserHead.setImageResource(R.drawable.place_holder);
            ivUserTopHead.setImageResource(R.drawable.place_holder);

            scrollToTop();
        } else {
            llUserInfo.setVisibility(View.VISIBLE);

            llLogin.setVisibility(View.GONE);
            tflTag.setVisibility(View.VISIBLE);

            if (null != userInfo) {
                if (!UserManager.getInstance().isMyself(userInfo.getId())) {
                    if (TextUtils.isEmpty(userInfo.getImagesUrl())) {
                        rlNoPhoto.setVisibility(View.GONE);
                        rlPhoto.setVisibility(View.GONE);
                    } else {
                        rlNoPhoto.setVisibility(View.GONE);
                        rlPhoto.setVisibility(View.VISIBLE);
                    }

                    iconLeft.setVisibility(View.VISIBLE);
                    iconSet.setText("\ue718");
                    iconSet.setTextSize(Dimension.SP, 5);

                    viewTopDivide.setVisibility(View.GONE);
                    ll_bottom.setVisibility(View.VISIBLE);

                    btTopFollow.setVisibility(View.VISIBLE);
                    btFollow.setVisibility(View.VISIBLE);
                    //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
                    int relationState = userInfo.getRelationState();
                    boolean isFollow = DiscoValueFormat.isFollow(relationState);
                    btTopFollow.setStatus(isFollow);

                    int hideSetStatus = userInfo.getHideSetStatus();

                    if (isFollow) {//粉丝
                        tvEditName.setVisibility(View.VISIBLE);
                        btChat.setVisibility(View.VISIBLE);
                        tvBottomFollow.setText("已关注");
                        iconFollow.setVisibility(View.GONE);
                    } else {
                        tvEditName.setVisibility(View.GONE);
                        if (hideSetStatus == 1) {////隐私设置-消息联系设置0(显示)，1（不显示）")
                            btChat.setVisibility(View.INVISIBLE);
                        } else if (hideSetStatus == 0) {
                            btChat.setVisibility(View.VISIBLE);
                        }
                        tvBottomFollow.setText("关注");
                        iconFollow.setVisibility(View.VISIBLE);
                    }


                    tvEditInfo.setText("展示更多>>");

                    tvAgeAddress.setVisibility(View.VISIBLE);

                    llShareApp.setVisibility(View.GONE);

                    String ageAddress = "";
                    if (!TextUtils.isEmpty(userInfo.getAddress())) {
                        ageAddress = userInfo.getAddress().replace(",", "") + " | ";
                    }

                    String birthday = userInfo.getBirthday();
                    if (!TextUtils.isEmpty(birthday)) {
                        ageAddress = ageAddress + CalendarUtils.calYearDiff(CalendarUtils.parseDateStr(birthday, "yyyy-MM-dd"), new Date());
                    }
                    tvAgeAddress.setText(ageAddress);

                    llMyMenu.setVisibility(View.GONE);
                    llAppMenu.setVisibility(View.GONE);
                    iconPullDU.setVisibility(View.GONE);
                } else {
                    if (TextUtils.isEmpty(userInfo.getImagesUrl())) {
                        rlNoPhoto.setVisibility(View.VISIBLE);
                        rlPhoto.setVisibility(View.GONE);
                    } else {
                        rlNoPhoto.setVisibility(View.GONE);
                        rlPhoto.setVisibility(View.VISIBLE);
                    }

                    iconLeft.setVisibility(View.GONE);
                    iconSet.setText("\ue74d");
                    iconSet.setTextSize(Dimension.SP, 24);

                    btTopFollow.setVisibility(View.GONE);

                    viewTopDivide.setVisibility(View.VISIBLE);

                    ll_bottom.setVisibility(View.GONE);

                    tvEditName.setVisibility(View.GONE);
                    tvEditInfo.setText("编辑资料");
                    tvAgeAddress.setVisibility(View.GONE);

                    llShareApp.setVisibility(View.VISIBLE);

                    llMyMenu.setVisibility(View.VISIBLE);
                    if (isShowAppMenu) {
                        llAppMenu.setVisibility(View.VISIBLE);
                    } else {
                        llAppMenu.setVisibility(View.GONE);
                    }
                    iconPullDU.setVisibility(View.VISIBLE);
                }

                if (userInfo.getGender() == -1) {
                    ivInfoSex.setVisibility(View.GONE);
                } else {
                    ivInfoSex.setVisibility(View.VISIBLE);
                    ivInfoSex.setImageResource(userInfo.getGender() == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
                }

                if (userInfo.getUserAloneTagVOList() != null && !userInfo.getUserAloneTagVOList().isEmpty()) {
                    tflTag.setAdapter(new TagAdapter<PersonTagModel>(userInfo.getUserAloneTagVOList()) {
                        @Override
                        public View getView(FlowLayout parent, int position, PersonTagModel model) {
                            //加载tag布局
                            View view = LayoutInflater.from(mContext).inflate(com.yanhua.user.R.layout.item_tfl_tag, parent, false);
                            //获取标签
                            TextView tvTag = view.findViewById(com.yanhua.user.R.id.tvTag);
                            tvTag.setText(model.getTagName());
                            return view;
                        }
                    });
                    tflTag.setOnTagClickListener((view, position, parent) -> {
                        ToastUtils.showShort(userInfo.getUserAloneTagVOList().get(position).getTagName());
                        return true;
                    });
                }

                tvIdentityId.setText(String.format("ID: %s", userInfo.getIdentityId()));

                if (!loginGetUserInfo) {
                    EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGINED_GET_USERINFO, userInfo.getId()));
                    loginGetUserInfo = true;
                }

                String nickName = userInfo.getNickName();
                tvUserTopName.setText(nickName);
                tvUserName.setText(nickName);

                ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userInfo.getImg(), R.drawable.place_holder);
                ImageLoaderUtil.loadImgCenterCrop(ivUserTopHead, userInfo.getImg(), R.drawable.place_holder);


                int realNameValid = userInfo.getRealNameValid();
                if (realNameValid == 1) {
                    // 区分自己和其他用户
                    llVerifybyName.setSelected(true);
                    if (UserManager.getInstance().isMyself(userInfo.getId())) {
                        tvVerifyByName.setText("已实名");
                        tvVerifyByName.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                        tvVerfiyArrow.setVisibility(View.VISIBLE);
                        tvVerfiyArrow.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                        llVerifybyName.setOnClickListener(v -> ARouter.getInstance()
                                .build(ARouterPath.CERTIFY_RESULT_ACTIVITY)
                                .withInt("result", realNameValid)
                                .navigation());
                    } else {
                        tvVerifyByName.setText("已实名");
                        tvVerifyByName.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                        tvVerfiyArrow.setVisibility(View.GONE);
                    }
                } else {
                    llVerifybyName.setSelected(false);
                    if (UserManager.getInstance().isMyself(userInfo.getId())) {
                        tvVerifyByName.setText("实名认证");
                        tvVerifyByName.setTextColor(ContextCompat.getColor(mContext, R.color.subWord));
                        tvVerfiyArrow.setVisibility(View.VISIBLE);
                        tvVerfiyArrow.setTextColor(ContextCompat.getColor(mContext, R.color.theme));
                        llVerifybyName.setOnClickListener(v -> ARouter.getInstance()
                                .build(ARouterPath.CERTIFY_CENTER_ACTIVITY)
                                .withInt("result", realNameValid)
                                .navigation());
                    } else {
                        tvVerifyByName.setText("未实名");
                        tvVerifyByName.setTextColor(ContextCompat.getColor(mContext, R.color.subWord));
                        tvVerfiyArrow.setVisibility(View.GONE);
                    }
                }


                tvFollowNum.setText(YHStringUtils.quantityEnFormat(userInfo.getLikes()));
                tvFansNum.setText(YHStringUtils.quantityEnFormat(userInfo.getFans()));
                tvLooksNum.setText("-");

                tvIntroduce.setContent(userInfo.getPersonalSignature());

                //标签：
                List<PersonTagModel> userTagVOList = userInfo.getUserTagVOList();// 后台给用户打的用户标签
                if (null != userTagVOList && userTagVOList.size() > 0) {
                    rlPersonTag.setVisibility(View.VISIBLE);

                    List<String> personTagList = new ArrayList<>();
                    for (PersonTagModel personTag : userTagVOList) {
                        personTagList.add(personTag.getName());
                    }

                    String userTagVOListContent = StringUtils.listToString(personTagList, ",");
                    tvPersonTag.setText(userTagVOListContent);
                } else {
                    rlPersonTag.setVisibility(View.GONE);
                }

                //交友诉求
                String makeFriendType = userInfo.getMakeFriendType();
                if (TextUtils.isEmpty(makeFriendType)) {
                    tvMakeFriendType.setVisibility(View.GONE);
                } else {
                    tvMakeFriendType.setVisibility(View.VISIBLE);

                    String[] arr = makeFriendType.split(",");

                    List<String> friendTypeList = new ArrayList<>();
                    for (String item : arr) {
                        friendTypeList.add(item.split("`~`")[1]);
                    }

                    String makeFriendTypeContent = StringUtils.listToString(friendTypeList, ",");

                    tvMakeFriendType.setText("交友诉求:" + makeFriendTypeContent);
                }
            }
        }
    }


    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        UserPageMessage messageContent = new UserPageMessage();

        messageContent.setUserId(userInfo.getId());
        messageContent.setHeadImg(userInfo.getImg());
        messageContent.setNickName(userInfo.getNickName());
        messageContent.setPostNum(userInfo.getDynamicCount());
        messageContent.setFansNum(userInfo.getFans());

        if (!TextUtils.isEmpty(userInfo.getImagesUrl())) {
            //TODO NEXT 用户个人主页的视频缩略图或第一张图片
            String imagesUrl = userInfo.getImagesUrl();
            String[] arr = imagesUrl.split(",");
            List<String> imgs = Arrays.asList(arr);
            if (null != imgs && imgs.size() > 0) {
                messageContent.setBgImg(imgs.get(0));
            }
        }

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;
            flBackgroud.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType type = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    io.rong.imlib.model.Message message = io.rong.imlib.model.Message.obtain(targetId,
                            Conversation.ConversationType.PRIVATE,
                            messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, "[个人主页]");
                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        io.rong.imlib.model.Message textMsg = io.rong.imlib.model.Message.obtain(targetId, type, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }


    @Override
    public void handleUserDetail(UserInfo uif) {
        UserManager.getInstance().setUserInfo(uif, false);

        userInfo = uif;

        if (null != userInfo) {
            setUserInfo();

            if (!TextUtils.isEmpty(userInfo.getImagesUrl())) {
                String imagesUrl = userInfo.getImagesUrl();
                String[] arr = imagesUrl.split(",");
                List<String> imgs = Arrays.asList(arr);
                tvIndex.setVisibility(imgs.size() > 1 ? View.VISIBLE : View.GONE);
                tvIndex.setText("1/" + imgs.size());
                bannerPhotoWall.setAdapter(new BannerImageAdapter<String>(imgs) {
                    @Override
                    public void onBindView(BannerImageHolder holder, String data, int position, int size) {
                        GlideRoundTransform glideRoundTransform = new GlideRoundTransform(mContext, 25, GlideRoundTransform.CornerType.BOTTOM);
                        Glide.with(mContext)
                                .load(data)
                                .thumbnail(Glide.with(mContext).load(data + YXConfig.IMAGE_RESIZE))
                                .apply(new RequestOptions()
                                        .fitCenter()
                                        .transform(glideRoundTransform)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .placeholder(com.yanhua.user.R.mipmap.bg_bluer)
                                .into(holder.imageView);
                        holder.imageView.setOnClickListener(view -> {
                            //当你点击图片的时候执行以下代码：
                            // 多图片场景（你有多张图片需要浏览）
                            List<Object> urlList = new ArrayList<>();
                            for (String url : imgs) {
                                urlList.add(url);
                            }
                            //srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
                            new XPopup.Builder(mContext).asImageViewer(holder.imageView, position, urlList, (popupView, position1) -> {
                                // 作用是当Pager切换了图片，需要更新源View
//                                popupView.updateSrcView((ImageView) bannerGoods.getChildAt(position));
                            }, new SmartGlideImageLoader()).show();
                        });
                    }
                });
                bannerPhotoWall.addOnPageChangeListener(new OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        tvIndex.setText((position + 1) + "/" + imgs.size());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case ConstanceEvent.REFRESH_USER_INFO:
                boolean isLogin = UserManager.getInstance().isLogin();
                if (isLogin) {
                    basePresenter.getPersonalInfo(UserManager.getInstance().getUserId());//此处的uid必须为当前的登录用户
                }
                break;
            case CommonConstant.LOGIN_OUT:
                userInfo = null;

                setUserInfo();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    private boolean isBlock;
    private List<String> imgPath;
    private StsTokenModel stsTokenModel;

    private static final int UPLOAD_FILE_FAIL = 1000;
    private static final int UPLOAD_FILE_SUCCESS = 1001;

    private MyHandler mHandler = new MyHandler();

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case UPLOAD_FILE_SUCCESS:
                    hideLoading();
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("imagesUrl", YHStringUtils.join(imgPath.toArray(new String[0]), ","));
                    basePresenter.updateUserDetail(UserManager.getInstance().getUserId(), params);
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("图片上传失败");
                    break;
            }
        }
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;
        String json = GsonUtils.toJson(model);
        UserManager.getInstance().setStsToken(json);
    }

    private void uploadFile(String path) {
        String fileName = new File(path).getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);
        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, path, new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                imgPath.add(httpUrl);
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_SUCCESS;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    @Override
    public void handleSetFriendRemarkSuccess() {
        ToastUtils.showShort("设置成功");
    }


    @Override
    public void updateFollowUserSuccess() {
        basePresenter.getPersonalInfo(userId);
    }

    @Override
    public void updateUserDetailSuccess() {
        basePresenter.getPersonalInfo(userId);
    }

}
