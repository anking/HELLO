package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.StaggeredItemDecoration;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.DiscoBestAdapter;
import com.yanhua.mine.presenter.MyBrowsePresenter;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


/**
 * 我的收藏-集锦
 */
public class MyFavBestFragment extends BaseMvpFragment<MyBrowsePresenter> implements MyBrowseContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int total;

    private DiscoBestAdapter mAdapter;
    private List<MomentListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyBrowsePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getDiscoCollectList(current, size);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        rvContent.setPadding(DisplayUtils.dip2px(mContext, 8), 0, DisplayUtils.dip2px(mContext, 8), 0);//公用布局通过代码设置padding
        rvContent.addItemDecoration(new StaggeredItemDecoration(mContext, 4, 8, 4, 0));
        mAdapter = new DiscoBestAdapter(mContext, (OnHandleClickListener<MomentListModel>) (type, item, pos) -> {
            String id = item.getId();
            switch (type) {
                case OnHandleClickListener.TO_DETAIL:
                    ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                            .withSerializable("contentId", id)
                            .withInt("discoBest", 1)//电音集锦
                            .withString("categoryId", "")
                            .withString("city", "")
                            .withInt("module", 0)
                            .navigation();
                    break;
            }
        });
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);


        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void handleDiscoCollectList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
