package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyCircleCreateAdapter;
import com.yanhua.mine.presenter.MyCirclePresenter;
import com.yanhua.mine.presenter.contract.MyCircleContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


/**
 * 我的圈子-创建
 */
public class MyCircleCreateFragment extends BaseMvpFragment<MyCirclePresenter> implements MyCircleContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private MyCircleCreateAdapter mAdapter;
    private String userId;
    private List<CircleModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyCirclePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        userId = UserManager.getInstance().getUserId();

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getCircleApplyList(current, size);
        }
    }

    @Override
    public void handleCircleList(ListResult<CircleModel> listResult) {
        List<CircleModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        mAdapter = new MyCircleCreateAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(mAdapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            CircleModel circleModel = mAdapter.getmDataList().get(pos);

            //状态;0待审核 1已通过 2未通过
            int status = circleModel.getStatusCode();
            if (status != 2) {
                String applyId = circleModel.getId();//申请ID

                String id = circleModel.getCircleId();
                if (!TextUtils.isEmpty(id)) {
                    //跳到圈子详情
                    ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY).withString("id", id).navigation();
                } else {
                    if (!TextUtils.isEmpty(applyId)) {
                        //跳到圈子编辑
                        ARouter.getInstance().build(ARouterPath.CIRCLE_APPLY_ACTIVITY)
                                .withString("applyId", applyId)
                                .navigation();
                    }
                }
            }
        });

        refreshLayout.autoRefresh(500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
