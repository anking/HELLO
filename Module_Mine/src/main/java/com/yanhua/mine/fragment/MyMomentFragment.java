package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.DynamicMomentListAdapter;
import com.yanhua.mine.presenter.MyDynamicPresenter;
import com.yanhua.mine.presenter.contract.MyDynamicContract;
import com.yanhua.rong.msgprovider.MomentContentMessage;
import com.yanhua.rong.msgprovider.MomentVideoMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 我的此刻列表数据
 */
public class MyMomentFragment extends BaseMvpFragment<MyDynamicPresenter> implements MyDynamicContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private String userId;
    private String title;
    private DynamicMomentListAdapter mAdapter;
    private List<MomentListModel> mListData;


    @Override
    public int bindLayout() {
        return R.layout.fragment_dynamic;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyDynamicPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }

        initRecyclerView();

        EventBus.getDefault().register(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

//        refreshLayout.autoRefresh();

        getListData(1);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            if (!UserManager.getInstance().isLogin()) {
                return;
            }

            current = page;

            basePresenter.getMyMomentList(title, userId, current, size);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                current = 1;
                getListData(current);
            }
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);

        mAdapter = new DynamicMomentListAdapter(mContext, (OnHandleClickListener<MomentListModel>) (type, item, pos) -> {
            currentPosition = pos;

            String id = item.getId();
            int contentType = item.getContentType();
            int attrType = 1;//根据tab或者这一项的属性类型归属进行赋值 ,attrType
            switch (type) {
                case OnHandleClickListener.REJECT://驳回 显示驳回原因，（点击后跳转到编辑页面）
                    String rejectReason = item.getAuditRemark();

                    AlertPopup alertPopup = new AlertPopup(mContext, "驳回原因", rejectReason, "确定", true);
                    new XPopup.Builder(mContext).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        alertPopup.dismiss();
                        //TODO 要不要修改，有时间吗
                    });

                    break;
                case OnHandleClickListener.MORE_ACTION://更多操作
                    int status = item.getAuditStatus();
                    if (UserManager.getInstance().isMyself(userId)) {
                        //1是审核通过，0是未审核，-1是审核不通过
                        if (status == 0) {//"未审核"
                        } else if (status == 1) {
                        } else if (status == -1) {//"已驳回"
                        }

                        //展示更多
                        MoreActionBoard.shareMyDynamic(mActivity, item, (position, type1) -> {
                            switch (position) {
                                case ShareBoardPopup.BOARD_SET_PRIVATE:
                                    item.setPrivacyType(type1);

                                    mAdapter.notifyItemChanged(currentPosition);
                                    break;

                                case ShareBoardPopup.BOARD_SET_DELETE:
                                    basePresenter.deleteContent(id);
                                    break;

                                case ShareBoardPopup.BOARD_SHARE_APP:
                                    //.isLightStatusBar(true)不会改变状态栏的颜色
                                    if (null == item) return;
                                    SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, item, YXConfig.TYPE_MOMENT,
                                            (selecteListData, msg) -> {
                                                int contentTypeTT = item.getContentType();
                                                if (contentTypeTT == 2) {
                                                    shareAppMsgVideo(selecteListData, msg, item);
                                                } else {
                                                    shareAppMsgContent(selecteListData, msg, item);
                                                }
                                            });
                                    new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                            .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                            .autoFocusEditText(false)
                                            .moveUpToKeyboard(false).asCustom(sharePopup).show();
                                    break;
                            }
                        });
                    } else {

                    }
                    break;


                case OnHandleClickListener.COLLECT://收藏
                    basePresenter.updateCollectContent(id, attrType);
                    break;

                case OnHandleClickListener.COMMENT://评论
                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情
                    if (contentType == 2) {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentModel", item)
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withString("fromClassName", className)
                                .withInt("module", 0)
                                .navigation();
                    } else {
                        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                .withString("id", id)
                                .withString("fromClassName", className)
                                .navigation();
                    }

                    break;

                case OnHandleClickListener.THUMB_UP://点赞
                    basePresenter.updateStarContent(id, attrType);
                    break;
            }
        }, UserManager.getInstance().isMyself(userId));
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }


    private void shareAppMsgContent(List<ShareObjectModel> listData, String msg, MomentListModel mModel) {
        //TODO NEXT 1、跳转动态详情（话题可直接进入话题详情）先不做处理
        //TODO NEXT 2、发送者和接受者背景的显示圆角（头像处为直角）
        MomentContentMessage messageContent = new MomentContentMessage();
        messageContent.setId(mModel.getId());

        List<String> imgs = new ArrayList<>();

        int contentType = mModel.getContentType();

        String content = "";
        if (contentType == 3) {
            content = YHStringUtils.pickLastFirst(mModel.getIntroduce(), mModel.getContent());
            content = YHStringUtils.getHtmlContent(content);
        } else {
            content = mModel.getContent();

            String urlJson = mModel.getContentUrl();
            if (!TextUtils.isEmpty(urlJson)) {
                Type listType = new TypeToken<ArrayList<FileResult>>() {
                }.getType();
                ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                for (FileResult fileResult : fileResults) {
                    imgs.add(fileResult.getHttpUrl());
                }
            }
        }

        messageContent.setImgs(imgs);
        messageContent.setContent(content);
        messageContent.setAuthorHeadImg(mModel.getUserPhoto());
        messageContent.setAuthorNickname(mModel.getNickName());
        messageContent.setType(YXConfig.TYPE_MOMENT);


        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;
            rvContent.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);

                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType type = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, type, messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(YXConfig.TYPE_MOMENT));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, type, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }


    private void shareAppMsgVideo(List<ShareObjectModel> listData, String msg, MomentListModel contentModel) {
        MomentVideoMessage messageContent = new MomentVideoMessage();
        messageContent.setId(contentModel.getId());

        //封面
        String urlJson = contentModel.getContentUrl();
        String coverImage = null;
        if (!TextUtils.isEmpty(urlJson)) {
            Type listType = new TypeToken<ArrayList<FileResult>>() {
            }.getType();
            ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

            FileResult fileResult = fileResults.get(0);
            coverImage = fileResult.getHttpUrl();

            messageContent.setHeight(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());
            messageContent.setWidth(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());

            messageContent.setCoverUrl(coverImage);
        }

        String content = YHStringUtils.pickLastFirst(contentModel.getIntroduce(), contentModel.getContent());
        content = YHStringUtils.getHtmlContent(content);//去掉html标签

        if (!TextUtils.isEmpty(content)) {
            messageContent.setContent(content);
        } else {
            messageContent.setContent(null);
        }

        messageContent.setAuthorHeadImg(contentModel.getUserPhoto());
        messageContent.setAuthorNickname(contentModel.getNickName());

        int type = YXConfig.TYPE_MOMENT;
        messageContent.setType(type);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            rvContent.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);
                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(type));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }


    @Override
    public void handleDeleteContentSuccess() {
        mAdapter.notifyItemRemoved(currentPosition);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.ACTION_COLLECT:
                MomentListModel momentCollect = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentCollect.getId().equals(c.getId())) {
                        c.setCollect(momentCollect.isCollect());
                        c.setCollectCount(momentCollect.getCollectCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_LOVE:
                MomentListModel momentLove = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentLove.getId().equals(c.getId())) {
                        c.setFabulous(momentLove.isFabulous());
                        c.setFabulousCount(momentLove.getFabulousCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_DELETE_CONTENT:
                String valueType = event.getValue();
                if (className.equals(valueType)) {
                    mAdapter.notifyItemRemoved(currentPosition);
                }
                break;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void handleMyMomentList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateCollectContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }


}
