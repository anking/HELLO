package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyFavMomentAdapter;
import com.yanhua.mine.presenter.MyDynamicPresenter;
import com.yanhua.mine.presenter.contract.MyDynamicContract;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的收藏-此刻
 */
public class MyFavMomentFragment extends BaseMvpFragment<MyDynamicPresenter> implements MyDynamicContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private String userId;

    private MyFavMomentAdapter mAdapter;
    private List<MomentListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyDynamicPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        userId = UserManager.getInstance().getUserId();

        initRecyclerView();

        EventBus.getDefault().register(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        refreshLayout.autoRefresh();
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getMyFavMomentList("", userId, current, size);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });


        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyFavMomentAdapter(mContext, (OnHandleClickListener<MomentListModel>) (type, item, pos) -> {
            currentPosition = pos;

            String id = item.getId();
            int contentType = item.getContentType();
            int attrType = 1;//根据tab或者这一项的属性类型归属进行赋值 ,attrType
            switch (type) {
                case OnHandleClickListener.MORE_ACTION://更多操作
                    int status = item.getAuditStatus();
                    if (UserManager.getInstance().isMyself(userId)) {
                        //1是审核通过，0是未审核，-1是审核不通过

                        if (status == 0) {//"未审核"
                        } else if (status == 1) {
                        } else if (status == -1) {//"已驳回"
                        }

                        //展示更多
                        MoreActionBoard.shareMyDynamic(mActivity, item, (position, type1) -> {

                        });
                    } else {

                    }
                    break;

                case OnHandleClickListener.COLLECT://收藏
                    basePresenter.updateCollectContent(id, attrType);
                    break;

                case OnHandleClickListener.COMMENT://评论
                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情
                    if (contentType == 2) {
                        ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                .withSerializable("contentModel", item)
                                .withString("categoryId", "")
                                .withString("city", "")
                                .withInt("module", 0)
                                .navigation();
                    } else {
                        ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                .withString("id", id)
                                .navigation();
                    }

                    break;
                case OnHandleClickListener.THUMB_UP://点赞
                    basePresenter.updateStarContent(id, attrType);
                    break;
            }
        });
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {

            case CommonConstant.ACTION_COLLECT:
                MomentListModel momentCollect = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentCollect.getId().equals(c.getId())) {
                        c.setCollect(momentCollect.isCollect());
                        c.setCollectCount(momentCollect.getCollectCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_LOVE:
                MomentListModel momentLove = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentLove.getId().equals(c.getId())) {
                        c.setFabulous(momentLove.isFabulous());
                        c.setFabulousCount(momentLove.getFabulousCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void handleMyMomentList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateCollectContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
