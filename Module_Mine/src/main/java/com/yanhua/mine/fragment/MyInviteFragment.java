package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyInviteListAdapter;
import com.yanhua.mine.presenter.MyInvitePresenter;
import com.yanhua.mine.presenter.contract.MyInviteContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的此刻列表数据
 */
public class MyInviteFragment extends BaseMvpFragment<MyInvitePresenter> implements MyInviteContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;
    private String type;//"1", "我报名的"|"2", "我发起的"|"3", "我收藏的",
    private MyInviteListAdapter mAdapter;
    private List<InviteModel> mListData;
    private String userId;

    @Override
    public int bindLayout() {
        return R.layout.fragment_my_invite;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyInvitePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getString("id");

            userId = bundle.getString("userId");
        }

        initRecyclerView();

    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();
//
        getListData(1);
//        refreshLayout.autoRefresh(100);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            if (!UserManager.getInstance().isLogin()) {
                return;
            }

            current = page;

            HashMap params = new HashMap();
            params.put("current", current);
            params.put("size", size);
            params.put("type", type);//查询类型(1:我报名的, 2:我发起的, 3:我关注的)
            params.put("userId", "userId");
            basePresenter.meetPartnerMyInvitePage(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                getListData(1);
            }
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyInviteListAdapter(mContext, type, (type, item, pos) -> {
            currentPosition = pos;

            int status = item.getStatus();
            //2 4 5 状态 App不显示 2换成（ 7 8 ）
            ////审核状态(0:审核中, 1:审核未通过, 2:审核通过, 3:已停约, 4:用户删除, 5:平台删除, 6:已取消,7:进行中,8:已结束 9:警告 10:编辑中)
            if (status != 1) {
                if (status == 0 || status == 7 || status == 6 || status == 8 || status == 10) {
                    String contentID = item.getId();
                    String publishId = item.getUserId();

                    //已取消非本人的不给看详情
                    if (!UserManager.getInstance().isMyself(publishId) && status == 6) {
                        ToastUtils.showShort("发起者已取消邀约");
                        return;
                    }

                    ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                            .withString("id", contentID)
                            .navigation();
                }
            } else {
                String rejectReason = item.getRejectReason();

                CommonDialog commonDialog = new CommonDialog(mContext, rejectReason, "确定", "取消");
                new XPopup.Builder(mContext).asCustom(commonDialog).show();
                commonDialog.setOnConfirmListener(() -> {
                    commonDialog.dismiss();
                    String inviteUserID = item.getUserId();
                    //先判断是不是自己
                    if (UserManager.getInstance().isMyself(inviteUserID)) {
                        //跳转修改
                        String contentID = item.getId();
                        ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                                .withString("id", contentID)
                                .navigation();
                    }
                });

                commonDialog.setOnCancelListener(() -> {
                    commonDialog.dismiss();
                });
                commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
            }
        });

        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handMyInviteList(ListResult<InviteModel> listResult) {
        if (null == listResult) {
            return;
        }
        List<InviteModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();

            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();

        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
