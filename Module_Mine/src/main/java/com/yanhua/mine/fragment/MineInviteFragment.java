package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.Channel;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MineInviteFragment extends BaseMvpFragment {

    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;

    private String userId;
    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_invite;
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }
        initCategoryView();
    }

    private void initCategoryView() {
        mChannelList = new ArrayList<>();

        //String id, String remarks, String reportName, int sort
        Channel myJoinList = new Channel("1", "我报名的", "我报名的", 1);
        Channel myPublish = new Channel("2", "我发起的", "我发起的", 2);
        Channel myFollow = new Channel("3", "我关注的", "我关注的", 3);

        mChannelList.add(myJoinList);
        mChannelList.add(myPublish);
        mChannelList.add(myFollow);

        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getChildFragmentManager());
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {
        public ChannelPageAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Channel model = mChannelList.get(position);

            Fragment fragment = new MyInviteFragment();

            Bundle bundle = new Bundle();

            //String id, String remarks, String reportName, int sort
            String id = model.getId();
            String reportName = model.getReportName();

            bundle.putString("userId", userId);
            bundle.putString("id", id);
            bundle.putString("reportName", reportName);

            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mChannelList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            Channel model = mChannelList.get(position);
            String reportName = model.getReportName();
            return !TextUtils.isEmpty(reportName) ? reportName : "";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }

}
