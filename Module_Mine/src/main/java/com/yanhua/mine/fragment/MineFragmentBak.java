package com.yanhua.mine.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.config.ConstanceEvent;
import com.yanhua.common.model.PersonTagModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.user.presenter.LoginPresenter;
import com.yanhua.user.presenter.contract.LoginContract;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MineFragmentBak extends BaseMvpFragment<LoginPresenter> implements LoginContract.IView {

    @BindView(R2.id.toolBar)
    NestedScrollView toolBar;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.ivUserTopHead)
    CircleImageView ivUserTopHead;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;
    @BindView(R2.id.iconSet)
    AliIconFontTextView iconSet;

    @BindView(R2.id.ll_login)
    LinearLayout llLogin;

    @BindView(R2.id.ll_logined)
    LinearLayout llLogined;

    @BindView(R2.id.ivUserHead)
    CircleImageView ivUserHead;

    @BindView(R2.id.tvUserName)
    TextView tvUserName;
    @BindView(R2.id.tvCertifyNum)
    TextView tvCertifyNum;

    @BindView(R2.id.llFans)
    LinearLayout llFans;
    @BindView(R2.id.llFollows)
    LinearLayout llFollows;
    @BindView(R2.id.tvInviteNum)
    TextView tvInviteNum;

    @BindView(R2.id.tvFansNum)
    OswaldTextView tvFansNum;
    @BindView(R2.id.tvFollowNum)
    OswaldTextView tvFollowNum;

    @BindView(R2.id.tvVerify)
    TextView tvVerify;
    @BindView(R2.id.rlPersonalSignature)
    RelativeLayout rlPersonalSignature;

    @Override
    protected void creatPresent() {
        basePresenter = new LoginPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_bak;
    }


    @Override
    public void onResume() {
        super.onResume();
        boolean isLogin = UserManager.getInstance().isLogin();

        llLogin.setVisibility(isLogin ? View.GONE : View.VISIBLE);
        llLogined.setVisibility(isLogin ? View.VISIBLE : View.GONE);

//
//        UserInfo userInfo = UserManager.getInstance().getUserInfo();
//
//        if (null != userInfo) {
//            ImageLoaderUtil.loadImg(ivUserHead, userInfo.getImg(), R.drawable.place_holder);
//            ImageLoaderUtil.loadImg(ivUserTopHead, userInfo.getImg(), R.drawable.place_holder);
//        }
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTopView();
        //----------

        EventBus.getDefault().register(this);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        boolean isLogin = UserManager.getInstance().isLogin();
        if (isLogin) {
            basePresenter.getUserDetail(mMMKV.getUserId());
        }
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;

            float rangle = scroll2top / scrollRangle;

            float alpha = 1 - rangle;
            if (alpha > 0 && alpha < 0.5) {
                toolbar.setAlpha(1 - alpha);
                toolbar.setBackgroundResource(R.color.transparent);

                iconSet.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconSet.setAlpha(1.f);
                tvTitle.setAlpha(alpha);
                ivUserTopHead.setAlpha(alpha);
                ivUserTopHead.setVisibility(View.GONE);
            } else if (alpha < 1 && alpha >= 0.5) {
                toolbar.setAlpha(1 - rangle * 2);
                toolbar.setBackgroundResource(R.color.white);

                iconSet.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconSet.setAlpha(alpha);
                tvTitle.setAlpha(alpha);
                ivUserTopHead.setAlpha(alpha);
                ivUserTopHead.setVisibility(View.VISIBLE);
            } else if (alpha == 1) {//避免滑动过快还显示半透明状态
                mAppBarLayout.postDelayed(() -> {
                    toolbar.setAlpha(1 - rangle);
                    toolbar.setBackgroundResource(R.color.white);

                    iconSet.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    iconSet.setAlpha(alpha);
                    tvTitle.setAlpha(alpha);
                    ivUserTopHead.setAlpha(alpha);
                    ivUserTopHead.setVisibility(View.VISIBLE);
                }, 100);
            } else if (alpha == 0) {//避免滑动过快还显示半透明状态
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.transparent);

                iconSet.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconSet.setAlpha(1.f);
                tvTitle.setAlpha(0.f);
                ivUserTopHead.setAlpha(0.f);
                ivUserTopHead.setVisibility(View.GONE);
            }
        });
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }


    @OnClick({R2.id.iconSet, R2.id.ivUserTopHead, R2.id.llCertify, R2.id.ll_login, R2.id.ivUserHead, R2.id.ll_page, R2.id.tvUserName,
            R2.id.llFans, R2.id.llFollows, R2.id.llMineDynamic, R2.id.llMineLike, R2.id.rlMyInvite, R2.id.llMineShare, R2.id.llMineClient,
            R2.id.llMineComment, R2.id.llMineFav, R2.id.llMineCircle, R2.id.llMineHistory, R2.id.llMineAdvice, R2.id.llMineQuestion})
    public void onViewClick(View v) {
        int id = v.getId();
        if (id == R.id.iconSet) {
            ARouter.getInstance()
                    .build(ARouterPath.SET_ACTIVITY)
                    .navigation();
        } else if (id == R.id.llCertify) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                int realNameValid = UserManager.getInstance().getUserInfo().getRealNameValid();
                //实名认证
                ARouter.getInstance().build(realNameValid != 1 ? ARouterPath.CERTIFY_CENTER_ACTIVITY : ARouterPath.CERTIFY_RESULT_ACTIVITY)
                        .withInt("result", realNameValid)
                        .navigation();
            });
        } else if (id == R.id.ivUserTopHead || id == R.id.ivUserHead || id == R.id.ll_page || id == R.id.tvUserName) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", UserManager.getInstance().getUserId()).withBoolean("bindStatus", false).navigation();
            });
        } else if (id == R.id.ll_login) {//是统一跳到登录还是各个击破
            PageJumpUtil.firstIsLoginThenJump(() -> {
            });
        } else if (id == R.id.llFans) {//
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.RELATION_USER_LIST_ACTIVITY).withString("userId", UserManager.getInstance().getUserId())
                        .withInt("type", 1)
                        .navigation();
            });
        } else if (id == R.id.llFollows) {//
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.RELATION_USER_LIST_ACTIVITY).withString("userId", UserManager.getInstance().getUserId())
                        .withInt("type", 0)
                        .navigation();
            });
        } else if (id == R.id.llMineDynamic) {//
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.MY_DYNAMIC_LIST_ACTIVITY)
                        .withString("userId", UserManager.getInstance().getUserId())
                        .navigation();
            });
        } else if (id == R.id.llMineLike) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_LIKE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.rlMyInvite) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MINE_INVITE_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineComment) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MINE_COMMENT_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineFav) {//我的收藏
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_FAV_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineCircle) {//我的圈子
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_CIRCLE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineHistory) {//浏览历史
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MY_BROWSE_LIST_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineShare) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.SHARE_FRIENDS_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineClient) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.CONTACT_CS_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineAdvice) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.FEEDBACK_ACTIVITY)
                        .navigation();
            });
        } else if (id == R.id.llMineQuestion) {
            ARouter.getInstance().build(ARouterPath.COMMON_PROBLEM_ACTIVITY)
                    .navigation();
        }
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
    }

    private boolean loginGetUserInfo;//登录状态重新进来

    @Override
    public void handleUserDetail(UserInfo userInfo) {
        UserManager.getInstance().setUserInfo(userInfo, false);
        if (null != userInfo) {
            if (!loginGetUserInfo) {
                EventBus.getDefault().post(new MessageEvent(CommonConstant.LOGINED_GET_USERINFO, userInfo.getId()));

                loginGetUserInfo = true;
            }

            String nickName = userInfo.getNickName();
            tvUserName.setText(nickName);
            ImageLoaderUtil.loadImgCenterCrop(ivUserHead, userInfo.getImg(), R.drawable.place_holder);
            ImageLoaderUtil.loadImgCenterCrop(ivUserTopHead, userInfo.getImg(), R.drawable.place_holder);
            if (userInfo.getRealNameValid() == 1) {
                int num = 1;
                tvCertifyNum.setText("(已认证" + num + "条)");
            }

            int num = userInfo.getMeetPartnerNum();//meetPartnerNum
            tvInviteNum.setText("目前有" + num + "个约伴正在进行中");

            tvFollowNum.setText(YHStringUtils.quantityEnFormat(userInfo.getLikes()));
            tvFansNum.setText(YHStringUtils.quantityEnFormat(userInfo.getFans()));
            if (userInfo.getUserTagVOList() != null && !userInfo.getUserTagVOList().isEmpty()) {
                List<PersonTagModel> userTagVOList = userInfo.getUserTagVOList();
                List<String> strs = new ArrayList<>();
                for (PersonTagModel p : userTagVOList) {
                    strs.add(p.getName());
                }
                tvVerify.setText(YHStringUtils.join(strs.toArray(new String[0]), "，"));
                rlPersonalSignature.setVisibility(View.VISIBLE);
            } else {
                rlPersonalSignature.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case ConstanceEvent.REFRESH_USER_INFO:
                boolean isLogin = UserManager.getInstance().isLogin();
                if (isLogin) {
                    basePresenter.getUserDetail(mMMKV.getUserId());
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
