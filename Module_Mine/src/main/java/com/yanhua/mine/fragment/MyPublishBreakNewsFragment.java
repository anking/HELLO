package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MyNewsListModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyBreakNewsAdapter;
import com.yanhua.mine.presenter.MyBrowsePresenter;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的此刻列表数据
 */
public class MyPublishBreakNewsFragment extends BaseMvpFragment<MyBrowsePresenter> implements MyBrowseContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private String userId;
    private String title;
    private MyBreakNewsAdapter mAdapter;
    private List<MyNewsListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyBrowsePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }

        initRecyclerView();

    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            if (!UserManager.getInstance().isLogin()) {
                return;
            }

            current = page;

            HashMap<String, Object> params = new HashMap<>();
            params.put("current", current);
            params.put("size", size);
            params.put("userId", userId);
            params.put("type", YXConfig.TYPE_BREAK_NEWS);
            if (!TextUtils.isEmpty(title)) {
                params.put("keyword", title);
            }
            basePresenter.newsDynamicList(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current=1;
            getListData(current);
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyBreakNewsAdapter(mContext, UserManager.getInstance().isMyself(userId), (OnHandleClickListener<MyNewsListModel>) (type, item, pos) -> {
            currentPosition = pos;

            String id = item.getId();
            int attrType = 1;//根据tab或者这一项的属性类型归属进行赋值 ,attrType
            switch (type) {
                case OnHandleClickListener.REJECT://驳回 显示驳回原因，（点击后跳转到编辑页面）
                    String rejectReason = item.getAuditRemark();

                    AlertPopup alertPopup = new AlertPopup(mContext, "驳回原因", rejectReason, "确定", true);
                    new XPopup.Builder(mContext).asCustom(alertPopup).show();
                    alertPopup.setOnConfirmListener(() -> {
                        alertPopup.dismiss();
                        //TODO 要不要修改，有时间吗
                    });
                    break;

                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情
                    PageJumpUtil.jumpNewsDetailPage(id, YXConfig.TYPE_BREAK_NEWS);
                    break;
                default:
                    break;
            }
        });
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handleNews2MeList(ListResult<MyNewsListModel> listResult) {

        List<MyNewsListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
