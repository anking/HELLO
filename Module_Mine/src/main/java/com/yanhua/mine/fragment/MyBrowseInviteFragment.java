package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.adapter.InviteListAdapter;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.NormalDecoration;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.presenter.MyBrowsePresenter;
import com.yanhua.mine.presenter.contract.MyBrowseContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的历史-约伴
 */
public class MyBrowseInviteFragment extends BaseMvpFragment<MyBrowsePresenter> implements MyBrowseContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int total;


    private InviteListAdapter mAdapter;
    private List<InviteModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyBrowsePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);


        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        refreshLayout.autoRefresh();
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            HashMap<String, Object> params = new HashMap<>();
            params.put("current", current);
            params.put("size", size);

            basePresenter.meetPartnerHistoryList(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });


        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        LinearLayoutManager manager = new LinearLayoutManager(mContext);

        mAdapter = new InviteListAdapter(mContext, (type, item, pos) -> {

            String contentID = item.getId();
            ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                    .withString("id", contentID)
                    .navigation();
        });

        final NormalDecoration decoration = new NormalDecoration() {
            @Override
            public String getHeaderName(int pos) {
                return mListData.get(pos).getBrowseDay();
            }
        };

        rvContent.addItemDecoration(decoration);

        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handInviteList(ListResult<InviteModel> listResult) {
        if (null == listResult) {
            return;
        }

        List<InviteModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {

            if (list != null && !list.isEmpty()) {
                for (InviteModel itemModel : list) {
                    String date = itemModel.getBrowseTime();
                    if (!TextUtils.isEmpty(date)) {
                        int subMonth = Integer.parseInt(date.substring(5, 7));//2022-04-18T03:07:03.105Z
                        int subDay = Integer.parseInt(date.substring(8, 10));//2022-04-18T03:07:03.105Z
                        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                        String showDayStr = String.format("%d月%d日", subMonth, subDay);
                        if (subDay == day) {
                            showDayStr = "今日";
                        }
                        itemModel.setBrowseDay(showDayStr);
                    } else {
                        itemModel.setBrowseDay("今日");//
                    }
                }
                mListData.addAll(list);
            }

            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

}
