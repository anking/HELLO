package com.yanhua.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.CircleManageModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyCircleManageAdapter;
import com.yanhua.mine.presenter.MyCirclePresenter;
import com.yanhua.mine.presenter.contract.MyCircleContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 我的圈子-管理
 */
public class MyCircleManageFragment extends BaseMvpFragment<MyCirclePresenter> implements MyCircleContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private MyCircleManageAdapter mAdapter;
    private List<CircleManageModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_mine_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new MyCirclePresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getCircleManageList("", current, size);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {

        mListData = new ArrayList<>();

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                refreshLayout.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyCircleManageAdapter(mContext, (type, item, pos) -> {
            if (type == MyCircleManageAdapter.OnHandleClickListener.SHOW_MORE) {
                boolean showMore = item.isShowMore();
                item.setShowMore(!showMore);

                mAdapter.notifyItemChanged(pos);
            }
        });
        rvContent.setLayoutManager(manager);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleManageModel> list = mAdapter.getmDataList();
            CircleManageModel item = list.get(pos);

            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation();
        });


        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);

        refreshLayout.autoRefresh(500);
    }

    @Override
    public void handleCircleManageList(ListResult<CircleManageModel> listResult) {
        List<CircleManageModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);
    }
}
