package com.yanhua.mine.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.Channel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.fragment.MyInviteFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.MINE_INVITE_ACTIVITY)
public class MyInviteActivity extends BaseMvpActivity {
    @BindView(R2.id.btnRight)
    TextView btnRight;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("我的邀约");
        btnRight.setVisibility(View.VISIBLE);

        initCategoryView();
    }

    private void initCategoryView() {
        mChannelList = new ArrayList<>();

        //String id, String remarks, String reportName, int sort
        Channel myJoinList = new Channel("1", "我报名的", "我报名的", 1);
        Channel myPublish = new Channel("2", "我发起的", "我发起的", 2);
        Channel myfav = new Channel("3", "我收藏的", "我收藏的", 3);

        mChannelList.add(myJoinList);
        mChannelList.add(myPublish);
        mChannelList.add(myfav);

        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager());
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    @Override
    protected void creatPresent() {

    }

    @OnClick({R2.id.btnRight})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.btnRight) {
            //发起邀约

            PageJumpUtil.firstIsLoginThenJump(() -> {
                //发布邀约
                PageJumpUtil.firstRealNameValidThenJump(() -> PageJumpUtil.pageToPublishInvite(null,false));
            });
        }
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_my_invite;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {
        public ChannelPageAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Channel model = mChannelList.get(position);

            Fragment fragment = new MyInviteFragment();

            Bundle bundle = new Bundle();

            //String id, String remarks, String reportName, int sort
            String id = model.getId();
            String reportName = model.getReportName();

            bundle.putString("id", id);
            bundle.putString("reportName", reportName);

            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mChannelList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            Channel model = mChannelList.get(position);
            String reportName = model.getReportName();
            return !TextUtils.isEmpty(reportName) ? reportName : "";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }
}
