package com.yanhua.mine.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.ScrollViewPager;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.fragment.MyFav9BarFragment;
import com.yanhua.mine.fragment.MyFavBestFragment;
import com.yanhua.mine.fragment.MyFavBreakNewsFragment;
import com.yanhua.mine.fragment.MyFavMomentFragment;
import com.yanhua.mine.fragment.MyFavStrategyFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的动态
 */
@Route(path = ARouterPath.MY_FAV_LIST_ACTIVITY)
public class MyFavListActivity extends BaseMvpActivity {

    @BindView(R2.id.viewPager)
    ScrollViewPager viewPager;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;

    //-----------------------------------搜素------------------------------------------------------
    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;
    @BindView(R2.id.tv_search_cancle)
    TextView tvSearchCancle;

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.titleContain)
    RelativeLayout rlTitle;

    @OnClick(R2.id.tv_search_cancle)
    public void hideSearch() {
        rlTitle.setVisibility(View.VISIBLE);
        rlSearch.setVisibility(View.GONE);
        etSearch.setText("");

        viewPager.setScroll(true);

        getListData();
    }

    private void getListData() {
        int pos = sltabChannel.getCurrentTab();
        Fragment fragment = mFragments.get(pos);

        if (fragment instanceof MyFavMomentFragment) {
            ((MyFavMomentFragment) fragment).getListData(1);
        } else if (fragment instanceof MyFavBreakNewsFragment) {
            ((MyFavBreakNewsFragment) fragment).getListData(1);
        } else if (fragment instanceof MyFavStrategyFragment) {
            ((MyFavStrategyFragment) fragment).getListData(1);
        } else if (fragment instanceof MyFavBestFragment) {
            ((MyFavBestFragment) fragment).getListData(1);
        } else if (fragment instanceof MyFav9BarFragment) {
            ((MyFav9BarFragment) fragment).getListData(1);
        }
    }

    @OnClick(R2.id.tvSearch)
    public void showSearch() {
        rlSearch.setVisibility(View.VISIBLE);
        rlTitle.setVisibility(View.GONE);

        viewPager.setScroll(false);
    }

    private void setInputListener() {
        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                getListData();
                return true;
            }
            return false;
        });
    }
    //-----------------------------------搜素------------------------------------------------------

    @Override
    protected void creatPresent() {
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_mine_fragment_container;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("我的收藏");

        mChannelList = new ArrayList<>();

        mChannelList.add("此刻");
        mChannelList.add("爆料");
        mChannelList.add("攻略");
        mChannelList.add("集锦");
        mChannelList.add("酒吧");

        mFragments = new ArrayList<>();
        Fragment momentFragment = new MyFavMomentFragment();
        mFragments.add(momentFragment);

        Fragment breakNewsFragment = new MyFavBreakNewsFragment();
        mFragments.add(breakNewsFragment);

        Fragment strategyFragment = new MyFavStrategyFragment();
        mFragments.add(strategyFragment);

        Fragment bestFragment = new MyFavBestFragment();
        mFragments.add(bestFragment);

        Fragment barFragment = new MyFav9BarFragment();
        mFragments.add(barFragment);

        initCategoryView();
        setInputListener();

        viewPager.setScroll(true);
    }

    private ArrayList<Fragment> mFragments;
    private List<String> mChannelList;
    private ChannelPageAdapter mAdapter;

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager(), mChannelList);
            viewPager.setAdapter(mAdapter);
            sltabChannel.setViewPager(viewPager);
            sltabChannel.setCurrentTab(0);

            viewPager.setOffscreenPageLimit(5);
        }
    }

    public class ChannelPageAdapter extends FragmentStatePagerAdapter {
        private List<String> mMenuList;

        public ChannelPageAdapter(@NonNull FragmentManager fm, List<String> list) {
            super(fm);
            this.mMenuList = list;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mMenuList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            String menuName = mMenuList.get(position);
            return !TextUtils.isEmpty(menuName) ? menuName : "";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }
}
