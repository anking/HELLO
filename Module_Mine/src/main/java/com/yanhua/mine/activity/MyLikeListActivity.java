package com.yanhua.mine.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.view.NormalDecoration;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.mine.R;
import com.yanhua.mine.R2;
import com.yanhua.mine.adapter.MyLikeListAdapter;
import com.yanhua.mine.presenter.MyDynamicPresenter;
import com.yanhua.mine.presenter.contract.MyDynamicContract;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 我的点赞
 */
@Route(path = ARouterPath.MY_LIKE_LIST_ACTIVITY)
public class MyLikeListActivity extends BaseMvpActivity<MyDynamicPresenter> implements MyDynamicContract.IView {

    //-----------------------------------搜素------------------------------------------------------
    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.titleContain)
    RelativeLayout rlTitle;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int currentPosition;
    private int total;

    private String userId;
    private MyLikeListAdapter mAdapter;
    private List<MomentListModel> mListData;

    private String keyWord;

    @OnClick(R2.id.tv_search_cancle)
    public void hideSearch() {
        rlTitle.setVisibility(View.VISIBLE);
        rlSearch.setVisibility(View.GONE);
        etSearch.setText("");

        getListData(1);
    }

    private void getListData(int page) {
        current = page;
        keyWord = etSearch.getText().toString().trim();

        userId = UserManager.getInstance().getUserId();
        basePresenter.getUserLikeList(current, size);
    }

    @OnClick(R2.id.tvSearch)
    public void showSearch() {
        rlSearch.setVisibility(View.VISIBLE);
        rlTitle.setVisibility(View.GONE);
    }

    private void setInputListener() {
        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                getListData(1);
                return true;
            }
            return false;
        });
    }
    //-----------------------------------搜素------------------------------------------------------


    @Override
    protected void creatPresent() {
        basePresenter = new MyDynamicPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_my_like_list;
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("我的赞");
        //列表数据
        mListData = new ArrayList<>();

        setInputListener();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        initRecyclerView();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));
        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new MyLikeListAdapter(mContext, true, (OnHandleClickListener<MomentListModel>) (clickType, item, pos) -> {
            currentPosition = pos;

            String id = item.getId();
            int contentType = item.getContentType();

            int type = item.getType();//根据tab或者这一项的属性类型归属进行赋值 ,type

            switch (clickType) {
                case OnHandleClickListener.FOLLOW://关注
                    //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
                    //    private Integer followStatus;
                    int followStatus = item.getFollowStatus();
                    boolean isFollow = DiscoValueFormat.isFollow(followStatus);
                    String userId = item.getUserId();

                    if (isFollow) {
                        AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                        new XPopup.Builder(mContext).asCustom(alertPopup).show();
                        alertPopup.setOnConfirmListener(() -> {
                            alertPopup.dismiss();
                            basePresenter.cancelFollowUser(userId);
                        });
                        alertPopup.setOnCancelListener(alertPopup::dismiss);
                    } else {
                        basePresenter.followUser(userId);
                    }
                    break;

                case OnHandleClickListener.COLLECT://收藏
                    basePresenter.updateCollectContent(id, type);
                    break;
                case OnHandleClickListener.DELETE://收藏
                    CommonDialog commonDialog = new CommonDialog(this, "是否删除这条内容？", "确定", "取消");
                    new XPopup.Builder(this).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();

                        basePresenter.deleteLikeContent(id, currentPosition);
                    });
                    commonDialog.setOnCancelListener(() -> commonDialog.dismiss());

                    break;
                case OnHandleClickListener.COMMENT://评论
                case OnHandleClickListener.TO_DETAIL://详情
                    //跳转到详情

                    switch (type) {
                        case YXConfig.TYPE_MOMENT:
                            if (contentType == 2) {
                                ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                        .withSerializable("contentModel", null)
                                        .withSerializable("contentId", item.getId())
                                        .withString("categoryId", "")
                                        .withString("city", "")
                                        .withInt("module", 0)
                                        .navigation();
                            } else {
                                ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                        .withString("id", id)
                                        .navigation();
                            }
                            break;

                        case YXConfig.TYPE_INVITE:
                            ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                                    .withString("id", id).navigation();
                            break;
                        case YXConfig.TYPE_BREAK_NEWS:
                        case YXConfig.TYPE_STRATEGY:
                            PageJumpUtil.jumpNewsDetailPage(id, item.getType());
                            break;
                        case YXConfig.TYPE_BEST:
                            ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                    .withSerializable("contentId", id)
                                    .withInt("discoBest", 1)//电音集锦
                                    .withString("categoryId", "")
                                    .withString("city", "")
                                    .withInt("module", 0)
                                    .navigation();
                            break;
                        case YXConfig.TYPE_9BAR:
                            //去酒吧
                            ARouter.getInstance()
                                    .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                                    .withString("barId", id)
                                    .navigation();
                            break;
                        default:
                            break;
                    }
                    break;
                case OnHandleClickListener.THUMB_UP://点赞
                    basePresenter.updateStarContent(id, type);
                    break;
            }
        });
        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);

        final NormalDecoration decoration = new NormalDecoration() {
            @Override
            public String getHeaderName(int pos) {
                return mListData.get(pos).getMonth();
            }
        };
        rvContent.addItemDecoration(decoration);

        refreshLayout.autoRefresh(500);
    }

    @Override
    public void handledeleteLikeContent(int pos) {
        if (mAdapter != null) {
            mAdapter.notifyItemRemoved(pos);
            ToastUtils.showShort("评论删除成功");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getEventName()) {
            case CommonConstant.ACTION_FOLLOW:
                String userIdFollow = event.getValue();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (!TextUtils.isEmpty(userIdFollow) && userIdFollow.equals(c.getUserId())) {
                        c.setFollowStatus(DiscoValueFormat.followAction(c.getFollowStatus()));
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_UNFOLLOW:
                String userIdUnFollow = event.getValue();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (!TextUtils.isEmpty(userIdUnFollow) && userIdUnFollow.equals(c.getUserId())) {
                        c.setFollowStatus(DiscoValueFormat.unFollowAction(c.getFollowStatus()));
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_COLLECT:
                MomentListModel momentCollect = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentCollect.getId().equals(c.getId())) {
                        c.setCollect(momentCollect.isCollect());
                        c.setCollectCount(momentCollect.getCollectCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
            case CommonConstant.ACTION_LOVE:
                MomentListModel momentLove = (MomentListModel) event.getSerializable();
                for (MomentListModel c : mAdapter.getmDataList()) {
                    if (momentLove.getId().equals(c.getId())) {
                        c.setFabulous(momentLove.isFabulous());
                        c.setFabulousCount(momentLove.getFabulousCount());
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void handleUserLikeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            for (MomentListModel itemModel : list) {
                String date = itemModel.getFabulousTime();

                if (!TextUtils.isEmpty(date)) {
                    int subMonth = Integer.parseInt(date.substring(5, 7));
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    String showMonthStr = String.format("%d月", subMonth);
                    if (subMonth == month) {
                        showMonthStr = "本月";
                    }
                    itemModel.setMonth(showMonthStr);
                }
            }
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowUserSuccess() {
        //此处需要更新列表中该人的关注状态
        MomentListModel model = mListData.get(currentPosition);

        //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
        //    private Integer followStatus;
        int followStatus = model.getFollowStatus();
        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
        int changeResult = isFollow ? (followStatus - 1) : (followStatus + 1);

        for (MomentListModel moment : mListData) {
            if (moment.getUserId().equals(model.getUserId())) {
                moment.setFollowStatus(changeResult);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateCollectContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
