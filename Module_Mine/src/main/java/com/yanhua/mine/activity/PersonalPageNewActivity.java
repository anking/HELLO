package com.yanhua.mine.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.mine.fragment.MineFragment;
import com.yanhua.user.R;
import com.yanhua.user.presenter.PersonalPagePresenter;
import com.yanhua.user.presenter.contract.PersonalPageContract;

/**
 * 社交个人主页
 *
 * @author Administrator
 */
@Route(path = ARouterPath.PERSONAL_PAGE_ACTIVITY)
public class PersonalPageNewActivity extends BaseMvpActivity<PersonalPagePresenter> implements PersonalPageContract.IView {

    @Autowired
    String userId;


    protected void creatPresent() {
        basePresenter = new PersonalPagePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_personal_page_new;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);


        MineFragment fragment = new MineFragment();

        Bundle data = new Bundle();
        data.putString("userId", userId);
        fragment.setArguments(data);

        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.flContainer, fragment);
        transaction.commit();
    }

    @Override
    public void handleDeleteContentSuccess() {
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
    }
}
