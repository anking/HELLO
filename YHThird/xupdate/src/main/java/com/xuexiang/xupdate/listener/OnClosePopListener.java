package com.xuexiang.xupdate.listener;

public interface OnClosePopListener {
    /**
     * 关闭更新弹框监听
     */
    void onClose();
}
