/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.bigkoo.pickerview;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.bigkoo.pickerview";
  public static final String BUILD_TYPE = "debug";
}
