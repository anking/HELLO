/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.shuyu.textutillib;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.shuyu.textutillib";
  public static final String BUILD_TYPE = "debug";
}
