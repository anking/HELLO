package com.shuyu.textutillib;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static int start;

    public static void main(String[] args) {
//        System.out.println(findStrCount("#122#222#4455####", "#", 0));
        findStrCount("#122#222#4455####", "#", 0);

        matchAtUser("举报图@烟锁池塘柳\b@烟锁池塘柳\b@烟锁池塘柳\b@烟锁池塘柳");
    }

    public static int findStrCount(String oriStr, String findStr, int count) {
        if (oriStr.contains(findStr)) {
            count++;

            int beginIndex = oriStr.indexOf(findStr) + findStr.length();
            start = start + beginIndex;

            System.out.println("#122#222#4455####" + start);

            String subText = oriStr.substring(0, beginIndex);

            String topic = subText.replaceAll("#", "");

            System.out.println(topic);

            oriStr = oriStr.substring(beginIndex);

            count = findStrCount(oriStr, findStr, count);
        }

        return count;
    }

    private static void matchAtUser(String oriStr) {
        oriStr = oriStr.replaceAll("\b"," ");
        oriStr = oriStr +" ";

        String regex = "@(.*?)\\s+";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(oriStr);

        while (m.find()) {
            System.out.println(m.group());
            String userText = m.group();//已经包含了：@+userName+空格 @YYY
            int beginIndex = oriStr.indexOf(userText);

            oriStr = oriStr.substring(beginIndex + userText.length());

            matchAtUser( oriStr);
            break;
        }
    }
}
