package com.shuyu.textutillib.model;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Objects;

/**
 * 用户model
 * Created by shuyu on 2016/11/10.
 */

public class FriendModel implements Comparable<FriendModel>, Serializable {

    private String nickName;    // 昵称
    private String friendRemark;    // 昵称
    private String realName;    // 真实姓名
    private String userPhoto;   // 用户头像
    private String namePinYin = "";   // pinyin
    private String firstLetter; // 拼音的首字母
    private int followStatus;   // 关联关系
    private String mobile; // 手机号
    private String gender; //
    private String address; //
    private String img; //
    private String userId;//
    private String id;

    public String getId() {
        String idTemp = TextUtils.isEmpty(this.id) ? this.userId : this.id;
        return idTemp;
    }

    public void setId(String id) {
        this.id = id;
        this.userId = id;
    }

    public FriendModel() {

    }

    public FriendModel(String nickName, String user_id) {
        this.nickName = nickName;
        this.userId = user_id;
        this.id = user_id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    public String getUserId() {
        String userIdTemp = TextUtils.isEmpty(this.userId) ? this.id : this.userId;
        return userIdTemp;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        this.id = userId;
    }

    @Override
    public String toString() {
        return this.nickName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickName, userId);
    }


    public String getFriendRemark() {
        return friendRemark;
    }

    public void setFriendRemark(String friendRemark) {
        this.friendRemark = friendRemark;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getNamePinYin() {
        return namePinYin;
    }

    public void setNamePinYin(String namePinYin) {
        this.namePinYin = namePinYin;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected;//用户选择

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            FriendModel friendInfo = (FriendModel) o;
            return (getUserId() != null && getUserId().equals(friendInfo.getUserId()));
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@NonNull FriendModel friend) {
        /*if (this.getNamePinYin().equals("@") || friend.getNamePinYin().equals("#")) {
            return -1;
        } else if (this.getNamePinYin().equals("#") || friend.getNamePinYin().equals("@")) {
            return 1;
        } else {
            return this.getNamePinYin().compareTo(friend.getNamePinYin());
        }*/
        if (firstLetter.equals("#") && !friend.firstLetter.equals("#")) {
            return 1;
        } else if (!firstLetter.equals("#") && friend.firstLetter.equals("#")) {
            return -1;
        } else {
            return firstLetter.compareTo(friend.firstLetter);
        }
    }
}
