package com.shuyu.textutillib.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * 话题model
 * Created by guoshuyu on 2017/8/16.
 */

public class TopicModel implements Serializable {
    /**
     * 话题名字内部不能有#和空格
     */
    /*{
        "contentId": "string",
            "createdTime": "2022-03-18T02:11:13.968Z",
            "dynamicCount": 0,
            "id": "string",
            "status": 0,
            "title": "string",
            "titleUrl": "string",
            "topicCode": "string",
            "topicDesc": "string",
            "viewCount": 0
    }*/
    private String id;
    private String contentId;
    private String title;
    private String topicCode;
    private String topicDesc;
    private String titleUrl;
    private int status;
    private int dynamicCount;
    private int viewCount;
    private String createdTime;


    private int deleted;// "逻辑删除;0正常,1删除"
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopicCode() {
        return topicCode;
    }

    public void setTopicCode(String topicCode) {
        this.topicCode = topicCode;
    }

    public String getTopicDesc() {
        return topicDesc;
    }

    public void setTopicDesc(String topicDesc) {
        this.topicDesc = topicDesc;
    }

    public String getTitleUrl() {
        return titleUrl;
    }

    public void setTitleUrl(String titleUrl) {
        this.titleUrl = titleUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDynamicCount() {
        return dynamicCount;
    }

    public void setDynamicCount(int dynamicCount) {
        this.dynamicCount = dynamicCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public TopicModel() {

    }

    public TopicModel(String topicName, String topicId) {
        this.title = topicName;
        this.id = topicId;
    }

    @Override
    public String toString() {
        return this.title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicModel that = (TopicModel) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }
}
