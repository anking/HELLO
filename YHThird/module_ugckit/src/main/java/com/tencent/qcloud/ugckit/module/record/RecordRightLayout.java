package com.tencent.qcloud.ugckit.module.record;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.tencent.qcloud.ugckit.R;
import com.tencent.qcloud.ugckit.module.record.interfaces.IRecordRightLayout;
import com.tencent.qcloud.ugckit.utils.UIAttributeUtil;
import com.tencent.ugc.TXRecordCommon;
import com.tencent.ugc.TXUGCRecord;

public class RecordRightLayout extends RelativeLayout implements IRecordRightLayout,
        View.OnClickListener, AspectView.OnAspectListener {
    private static final String TAG = "RecordRightLayout";

    private Activity mActivity;
    private AspectView mAspectView;        // 屏比，目前有三种（1:1；3:4；9:16）
    private ImageView mImageBeauty;       // 美颜
    private TextView mTextBeauty;
    private RelativeLayout mLayoutBeauty;
    private ImageView mImageSoundEffect;  // 音效
    private TextView mTextSoundEffect;
    private ImageView mImageSoundEffectMask;
    private RelativeLayout mLayoutSoundEffect;

    private OnItemClickListener mOnItemClickListener;

    private RelativeLayout layoutSwitchBf;
    private ImageView mImageCameraSwitch;         // 切换摄像头
    private ImageView mImageTorch;                // 闪光灯
    private RelativeLayout mSwitchLight;
    private RelativeLayout mSpeed;

    private int mTorchOnImage;
    private int mTorchOffImage;
    private int mTorchDisableImage;
    private boolean mFrontCameraFlag = false;                //是否前置摄像头UI判断
    private boolean mIsTorchOpenFlag;                       // 是否打开闪光灯UI判断

    public RecordRightLayout(Context context) {
        super(context);
        initViews();
    }

    public RecordRightLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RecordRightLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        mActivity = (Activity) getContext();
        inflate(mActivity, R.layout.ugckit_record_right_layout, this);

        mAspectView = (AspectView) findViewById(R.id.aspect_view);
        mAspectView.setOnAspectListener(this);

        mLayoutBeauty = (RelativeLayout) findViewById(R.id.layout_beauty);
        mImageBeauty = (ImageView) findViewById(R.id.iv_beauty);
        mTextBeauty = (TextView) findViewById(R.id.tv_beauty);
        mImageBeauty.setOnClickListener(this);

        mLayoutSoundEffect = (RelativeLayout) findViewById(R.id.layout_sound_effect);
        mImageSoundEffect = (ImageView) findViewById(R.id.iv_sound_effect);
        mTextSoundEffect = (TextView) findViewById(R.id.tv_sound_effect);
        mImageSoundEffect.setOnClickListener(this);
        mImageSoundEffectMask = (ImageView) findViewById(R.id.iv_sound_effect_mask);


        mImageTorch = (ImageView) findViewById(R.id.iv_switch_light);
        mImageTorch.setOnClickListener(this);

        mSwitchLight = findViewById(R.id.layout_switch_light);

        mSpeed = findViewById(R.id.layout_speed);

        layoutSwitchBf = findViewById(R.id.layout_switch_bf);

        mImageCameraSwitch = (ImageView) findViewById(R.id.iv_switch_bf);
        mImageCameraSwitch.setOnClickListener(this);

        mTorchDisableImage = UIAttributeUtil.getResResources(mActivity, R.attr.recordTorchDisableIcon, R.drawable.ugckit_torch_disable);
        mTorchOffImage = UIAttributeUtil.getResResources(mActivity, R.attr.recordTorchOffIcon, R.drawable.ugckit_selector_torch_close);
        mTorchOnImage = UIAttributeUtil.getResResources(mActivity, R.attr.recordTorchOnIcon, R.drawable.ugckit_selector_torch_open);

        if (mFrontCameraFlag) {
            mSwitchLight.setVisibility(View.GONE);
            mImageTorch.setImageResource(mTorchDisableImage);
        } else {
            mSwitchLight.setVisibility(View.VISIBLE);
            mImageTorch.setImageResource(mTorchOffImage);
        }

    }

    @Override
    public void onClick(@NonNull View view) {
        int id = view.getId();
        if (id == R.id.iv_beauty) {
            mOnItemClickListener.onShowBeautyPanel();
        } else if (id == R.id.iv_sound_effect) {
            mOnItemClickListener.onShowSoundEffectPanel();
        } else if (id == R.id.iv_switch_light) {
            toggleTorch();
        } else if (id == R.id.iv_switch_bf) {
            switchCamera();
        }
    }

    /**
     * 切换了一种屏比
     *
     * @param currentAspect 当前屏比
     */
    @Override
    public void onAspectSelect(int currentAspect) {
        mOnItemClickListener.onAspectSelect(currentAspect);
    }

    /**
     * 设置"音乐"按钮是否可用
     *
     * @param enable {@code true} 可点击<br>
     *               {@code false} 不可点击
     */
    @Override
    public void setMusicIconEnable(boolean enable) {
    }

    /**
     * 设置"屏比"按钮是否可用
     *
     * @param enable {@code true} 可点击<br>
     *               {@code false} 不可点击
     */
    @Override
    public void setAspectIconEnable(boolean enable) {
        mAspectView.hideAspectSelectAnim();
        if (enable) {
//            mAspectView.disableMask();
        } else {
//            mAspectView.enableMask();
        }
        mAspectView.setEnabled(enable);
    }

    /**
     * 设置"音效"按钮是否可用
     *
     * @param enable {@code true} 清除背景音后，音效Icon变为可点击<br>
     *               {@code false} 录制添加BGM后是录制不了人声的，而音效是针对人声有效的，此时开启音效遮罩层，音效Icon变为不可用
     */
    @Override
    public void setSoundEffectIconEnable(boolean enable) {
        if (enable) {
            mImageSoundEffectMask.setVisibility(View.INVISIBLE);
        } else {
            mImageSoundEffectMask.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public void setOnSpeedClickListener(OnClickListener listener) {
        mSpeed.setOnClickListener(listener);
    }

    public void disableRecordMusic() {
    }

    public void disableRecordSoundEffect() {
        mLayoutSoundEffect.setVisibility(View.GONE);
    }

    public void disableAspect() {
        mAspectView.setVisibility(View.GONE);
    }

    public void disableBeauty() {
        mLayoutBeauty.setVisibility(View.GONE);
    }

    @Override
    public void setAspectTextSize(int size) {
        mAspectView.setTextSize(size);
    }

    @Override
    public void setAspectTextColor(int color) {
        mAspectView.setTextColor(color);
    }

    @Override
    public void setAspectIconList(int[] residList) {
        mAspectView.setIconList(residList);
    }

    @Override
    public void setBeautyIconResource(int resid) {
        mImageBeauty.setImageResource(resid);
    }

    @Override
    public void setBeautyTextSize(int size) {
        mTextBeauty.setTextSize(size);
    }

    @Override
    public void setBeautyTextColor(int color) {
        mTextBeauty.setTextColor(getResources().getColor(color));
    }

    @Override
    public void setSoundEffectIconResource(int resid) {
        mImageSoundEffect.setImageResource(resid);
    }

    @Override
    public void setSoundEffectTextSize(int size) {
        mTextSoundEffect.setTextSize(size);
    }

    @Override
    public void setSoundEffectTextColor(int color) {
        mTextSoundEffect.setTextColor(getResources().getColor(color));
    }

    public void setAspect(int aspectRatio) {
        mAspectView.setAspect(aspectRatio);
    }


    public void startRecord() {
//        mImageCameraSwitch.setVisibility(View.GONE);
//        mSwitchLight.setVisibility(View.INVISIBLE);
        layoutSwitchBf.setVisibility(View.VISIBLE);

        mSpeed.setVisibility(View.GONE);
        mAspectView.setVisibility(View.GONE);
        mLayoutBeauty.setVisibility(View.GONE);
        mLayoutSoundEffect.setVisibility(View.GONE);
    }

    public void pauseRecord() {
        layoutSwitchBf.setVisibility(View.VISIBLE);
//        mImageCameraSwitch.setVisibility(View.VISIBLE);
//        layoutSwitchBf.setVisibility(VISIBLE);
//        mSwitchLight.setVisibility(View.VISIBLE);
    }


    /**
     * 切换前后摄像头
     */
    private void switchCamera() {
        mFrontCameraFlag = !mFrontCameraFlag;
        mIsTorchOpenFlag = false;
        if (mFrontCameraFlag) {
            mSwitchLight.setVisibility(View.GONE);
            mImageTorch.setImageResource(mTorchDisableImage);
        } else {
            mSwitchLight.setVisibility(View.VISIBLE);
            mImageTorch.setImageResource(mTorchOffImage);
        }

        Animation rotateAnimation = AnimationUtils.loadAnimation(mActivity, R.anim.rotate_anim);
        LinearInterpolator lin = new LinearInterpolator();
        rotateAnimation.setInterpolator(lin);
        rotateAnimation.setDuration(300);
        mImageCameraSwitch.startAnimation(rotateAnimation);

        TXUGCRecord record = VideoRecordSDK.getInstance().getRecorder();
        if (record != null) {
            record.switchCamera(mFrontCameraFlag);
        }
    }


    public void setCamera(){
        TXUGCRecord record = VideoRecordSDK.getInstance().getRecorder();
        if (record != null) {
            record.switchCamera(mFrontCameraFlag);
        }
    }


    /**
     * 切换闪光灯开/关
     */
    private void toggleTorch() {
        mIsTorchOpenFlag = !mIsTorchOpenFlag;
        if (mIsTorchOpenFlag) {
            mImageTorch.setImageResource(mTorchOnImage);

            TXUGCRecord record = VideoRecordSDK.getInstance().getRecorder();
            if (record != null) {
                record.toggleTorch(true);
            }
        } else {
            mImageTorch.setImageResource(mTorchOffImage);
            TXUGCRecord record = VideoRecordSDK.getInstance().getRecorder();
            if (record != null) {
                record.toggleTorch(false);
            }
        }
    }

    /**
     * 设置闪光灯的状态为关闭
     */
    public void closeTorch() {
        if (mIsTorchOpenFlag) {
            mIsTorchOpenFlag = false;
            if (mFrontCameraFlag) {
                mSwitchLight.setVisibility(View.GONE);
                mImageTorch.setImageResource(mTorchDisableImage);
            } else {
                mImageTorch.setImageResource(mTorchOffImage);
                mSwitchLight.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     *
     */
    public void swithToVideoRecord() {
        mLayoutBeauty.setVisibility(View.VISIBLE);
        mLayoutSoundEffect.setVisibility(View.VISIBLE);
        mSpeed.setVisibility(View.VISIBLE);
    }

    public void swithToTakePhoto() {
        mLayoutBeauty.setVisibility(View.VISIBLE);
        mLayoutSoundEffect.setVisibility(View.GONE);
        mSpeed.setVisibility(View.GONE);
    }

    public void swithAspect(boolean show) {
        mAspectView.setVisibility(show ? View.VISIBLE : View.GONE);

        mAspectView.selectAnotherAspect(show ? TXRecordCommon.VIDEO_ASPECT_RATIO_3_4 : TXRecordCommon.VIDEO_ASPECT_RATIO_9_16);
    }
}
