package com.tencent.qcloud.ugckit.module.effect.location;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.tencent.qcloud.ugckit.module.effect.bubble.TCBubbleInfo;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 加载气泡字幕的类
 */
public class TCLocationBubbleManager implements AMapLocationListener, PoiSearch.OnPoiSearchListener {
//
//    private static final String ROOT_DIR = "bubble";
//
//    private static TCLocationBubbleManager sInstance;
//    private final Context mContext;
//    private List<TCBubbleInfo> locations;
//
//    //声明mlocationClient对象
//    public AMapLocationClient mlocationClient;
//    //声明mLocationOption对象
//    public AMapLocationClientOption mLocationOption = null;
//
//    public static TCLocationBubbleManager getInstance(@NonNull Context context) {
//        if (sInstance == null)
//            sInstance = new TCLocationBubbleManager(context);
//        return sInstance;
//    }
//
//    private TCLocationBubbleManager(@NonNull Context context) {
//        mContext = context.getApplicationContext();
//
//        locations = new ArrayList<>();
//        mlocationClient = new AMapLocationClient(mContext);
//        //初始化定位参数
//        mLocationOption = new AMapLocationClientOption();
//        //设置定位监听
//        mlocationClient.setLocationListener(this);
//        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
//        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//        //设置定位间隔,单位毫秒,默认为2000ms
//        mLocationOption.setInterval(2000);
//        //设置定位参数
//        mlocationClient.setLocationOption(mLocationOption);
//        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
//        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
//        // 在定位结束后，在合适的生命周期调用onDestroy()方法
//        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//        //启动定位
//        mlocationClient.startLocation();
//    }
//
//    @Nullable
//    public List<TCBubbleInfo> loadBubbles() {
//        return locations;
//    }
////
////
////    @Nullable
////    public Bitmap getBitmapFromAssets(@Nullable String path) {
////        if (path == null) return null;
////        try {
////            return getBitmap(path);
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////        return null;
////    }
////
//    private String getConfigByPath(String path) throws IOException {
//        BufferedInputStream stream = getInputStreamFromAsset(path);
//        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
//        String line = null;
//        StringBuilder sb = new StringBuilder();
//        while ((line = reader.readLine()) != null) {
//            sb.append(line);
//        }
//        stream.close();
//        reader.close();
//        return sb.toString();
//    }
//
////
////    private Bitmap getBitmap(String path) throws IOException {
////        return BitmapFactory.decodeStream(getInputStreamFromAsset(path));
////    }
////
//    private BufferedInputStream getInputStreamFromAsset(String path) throws IOException {
//        return new BufferedInputStream(mContext.getAssets().open(path));
//    }
//
//    @Override
//    public void onLocationChanged(AMapLocation amapLocation) {
//        if (amapLocation != null) {
//            if (amapLocation.getErrorCode() == 0) {
//                //定位成功回调信息，设置相关消息
//                amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
//                double latitude = amapLocation.getLatitude();//获取纬度
//                double longitude = amapLocation.getLongitude();//获取经度
//                amapLocation.getAccuracy();//获取精度信息
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Date date = new Date(amapLocation.getTime());
//                df.format(date);//定位时间
//                PoiSearch.Query query = new PoiSearch.Query("", "", "");
//                query.setPageSize(30);
//                PoiSearch search = new PoiSearch(mContext, query);
//                search.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude, longitude), 10000));
//                search.setOnPoiSearchListener(this);
//                search.searchPOIAsyn();
//            } else {
//                mlocationClient.stopLocation();
//                if (amapLocation.getErrorCode() == 12) {
//                    // 缺少定位权限
//                } else if (amapLocation.getErrorCode() == 6) {
//                    // 定位失败
//                }
//            }
//        }
//    }
//
//    @Override
//    public void onPoiSearched(PoiResult poiResult, int pos) {
//        ArrayList<PoiItem> pois = poiResult.getPois();
//        if (pois != null && pois.size() > 0) {
//            mlocationClient.stopLocation();
//            try {
//                // 获取气泡字幕总配置文件
//                String rootJson = getConfigByPath(ROOT_DIR + File.separator + "bubbleLocationList.json");
//                JSONObject rootJSONObj = new JSONObject(rootJson);
//                JSONArray rootJSONArr = rootJSONObj.getJSONArray("bubbleList");
//                List<String> folderNameList = new ArrayList<>();
//                for (int i = 0; i < rootJSONArr.length(); i++) {
//                    String folderName = rootJSONArr.getJSONObject(i).getString("name");
//                    folderNameList.add(folderName);
//                }
//
//                // 遍历获取各个气泡字幕的参数
//                locations.clear();
//
//                for (PoiItem poi : pois) {
//                    String folderPath = ROOT_DIR + File.separator + "bubble";
//                    String configPath = folderPath + File.separator + "config.json";
//                    String iconPath = folderPath + File.separator + "icon.png";
//                    String bubblePath = folderPath + File.separator + "bubble.png";
//
//                    String jsonContent = getConfigByPath(configPath);
//                    JSONObject jsonObj = new JSONObject(jsonContent);
//                    int width = jsonObj.getInt("width");
//                    int height = jsonObj.getInt("height");
//                    int top = jsonObj.getInt("textTop");
//                    int left = jsonObj.getInt("textLeft");
//                    int right = jsonObj.getInt("textRight");
//                    int bottom = jsonObj.getInt("textBottom");
//                    int textSize = jsonObj.getInt("textSize");
//
//                    TCBubbleInfo info = new TCBubbleInfo();
//                    info.setWidth(width);
//                    info.setHeight(height);
//                    info.setDefaultSize(textSize);
//                    //归一化坐标
//                    info.setRect(top * 1.0f / height, left * 1.0f / width, right * 1.0f / width, bottom * 1.0f / height);
//                    info.setBubblePath(bubblePath);
//                    info.setIconPath(iconPath);
//                    info.setTitle(poi.getTitle());
//
//                    locations.add(info);
//
//
//                }
//            } catch (@NonNull JSONException | IOException e) {
//                e.printStackTrace();
//            }
//
//        }
//    }
//
//    @Override
//    public void onPoiItemSearched(PoiItem poiItem, int i) {
//
//    }
//}

    private static TCLocationBubbleManager sInstance;
    private final Context mContext;
    //声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;
    private List<TCBubbleInfo> locations;

    public static TCLocationBubbleManager getInstance(@NonNull Context context) {
        if (sInstance == null)
            sInstance = new TCLocationBubbleManager(context);
        return sInstance;
    }

    private TCLocationBubbleManager(@NonNull Context context) {
        mContext = context.getApplicationContext();
        locations = new ArrayList<>();
        try {
            mlocationClient = new AMapLocationClient(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置定位监听
        mlocationClient.setLocationListener(this);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(2000);
        //设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用onDestroy()方法
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
        //启动定位
        mlocationClient.startLocation();
    }

    @Nullable
    public List<TCBubbleInfo> loadLocations() {
        return locations;
    }

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
                double latitude = amapLocation.getLatitude();//获取纬度
                double longitude = amapLocation.getLongitude();//获取经度
                amapLocation.getAccuracy();//获取精度信息
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date(amapLocation.getTime());
                df.format(date);//定位时间
                PoiSearch.Query query = new PoiSearch.Query("", "", "");
                query.setPageSize(30);
                PoiSearch search = null;
                try {
                    search = new PoiSearch(mContext, query);
                } catch (AMapException e) {
                    e.printStackTrace();
                }
                search.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude, longitude), 10000));
                search.setOnPoiSearchListener(this);
                search.searchPOIAsyn();
            } else {
                mlocationClient.stopLocation();
                if (amapLocation.getErrorCode() == 12) {
                    // 缺少定位权限
                } else if (amapLocation.getErrorCode() == 6) {
                    // 定位失败
                }
            }
        }
    }

    @Override
    public void onPoiSearched(PoiResult poiResult, int i) {
        ArrayList<PoiItem> pois = poiResult.getPois();
        if (pois != null && pois.size() > 0) {
            mlocationClient.stopLocation();
            for (PoiItem poi : pois) {
                TCBubbleInfo info = new TCBubbleInfo();
                info.setTitle(poi.getTitle());
                LatLonPoint point = poi.getLatLonPoint();
                info.setLatitude(point.getLatitude());
                info.setLongitude(point.getLongitude());
                locations.add(info);
            }
        }
    }

    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }

    private BufferedInputStream getInputStreamFromAsset(String path) throws IOException {
        return new BufferedInputStream(mContext.getAssets().open(path));
    }
    private Bitmap getBitmap(String path) throws IOException {
        return BitmapFactory.decodeStream(getInputStreamFromAsset(path));
    }
    @Nullable
    public Bitmap getBitmapFromAssets(@Nullable String path) {
        if (path == null) return null;
        try {
            return getBitmap(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
