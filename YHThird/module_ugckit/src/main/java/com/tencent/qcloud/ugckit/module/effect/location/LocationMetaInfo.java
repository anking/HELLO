package com.tencent.qcloud.ugckit.module.effect.location;

/**
 * 保存 从{@link LocationPannel} 之后的设定的 定位index、以及字体颜色的数据结构
 */
public class LocationMetaInfo {

    private int locationPos;
    private int textColor;
    private LocationInfo locationInfo;


    public int getLocationPos() {
        return locationPos;
    }

    public void setLocationPos(int locationPos) {
        this.locationPos = locationPos;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

}
