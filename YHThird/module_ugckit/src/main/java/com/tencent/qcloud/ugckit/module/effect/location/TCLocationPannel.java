package com.tencent.qcloud.ugckit.module.effect.location;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tencent.qcloud.ugckit.R;
import com.tencent.qcloud.ugckit.component.circlebmp.TCCircleView;
import com.tencent.qcloud.ugckit.component.seekbar.TCColorView;
import com.tencent.qcloud.ugckit.module.effect.bubble.IBubbleSubtitlePannel;
import com.tencent.qcloud.ugckit.module.effect.bubble.LocationBubbleAdapter;
import com.tencent.qcloud.ugckit.module.effect.bubble.TCBubbleInfo;
import com.tencent.qcloud.ugckit.module.effect.bubble.TCSubtitleInfo;
import com.yanhua.core.util.DisplayUtils;

import java.util.List;

public class TCLocationPannel extends FrameLayout implements IBubbleSubtitlePannel, LocationBubbleAdapter.OnItemClickListener, View.OnClickListener, TCColorView.OnSelectColorListener {

    private View mContentView;
    private RecyclerView mRecycleLocations;
    private LocationBubbleAdapter mLocationAdapter;
    private List<TCBubbleInfo> locations;
    private ImageView mImageClose;
    private TextView mTextBubbleStyle;   //气泡样式
    private TextView mTextColor;         //文字颜色
    private TCColorView mColorView;
    private TCCircleView mCvColor;
    private LinearLayout mLlColor;

    private Context mContext;

    @Nullable
    private TCSubtitleInfo mLocationMetaInfo;
    private OnBubbleSubtitleCallback mCallback;

    public TCLocationPannel(@NonNull Context context) {
        super(context);
        init(context);
    }

    public TCLocationPannel(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TCLocationPannel(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        mContext = context;

        mLocationMetaInfo = new TCSubtitleInfo();

        mContentView = LayoutInflater.from(getContext()).inflate(R.layout.ugckit_layout_location_window, this, true);
        initViews(mContentView);
    }

    private void initViews(@NonNull View contentView) {
        mImageClose = contentView.findViewById(R.id.iv_close);
        mImageClose.setOnClickListener(this);
        mRecycleLocations = contentView.findViewById(R.id.bubble_rv_style);
        mTextBubbleStyle = contentView.findViewById(R.id.bubble_iv_bubble);
        mTextBubbleStyle.setOnClickListener(this);
        mTextColor = contentView.findViewById(R.id.bubble_iv_color);
        mTextColor.setOnClickListener(this);
        mLlColor = contentView.findViewById(R.id.bubble_ll_color);
        mCvColor = contentView.findViewById(R.id.bubble_cv_color);
        mColorView = contentView.findViewById(R.id.bubble_color_view);
        mColorView.setOnSelectColorListener(this);
        mTextBubbleStyle.setSelected(true);
        mRecycleLocations.setVisibility(View.VISIBLE);
    }

    private void enterAnimator() {
        ObjectAnimator translationY = ObjectAnimator.ofFloat(mContentView, "translationY", mContentView.getHeight(), 0);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(400);
        set.play(translationY);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                TCLocationPannel.this.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        set.start();
    }

    private void resetInfo() {
        mLocationMetaInfo.setBubblePos(0);

        int height = 0, width = 0;
        if (height <= 0) {
            height = DisplayUtils.dip2px(mContext, 40);
        }

        if (width <= 10) {
            width = DisplayUtils.getScreenWidth(mContext) - 200;
        }

        // 创建一个默认的
        TCBubbleInfo info = new TCBubbleInfo();
        // 创建一个默认的
        info.setTitle("东圃商业大厦");
        info.setHeight(height);
        info.setWidth(width);
        info.setDefaultSize(40);
        info.setBubblePath(null);
        info.setIconPath(null);
        info.setRect(0, 0, 0, 0);
        mLocationMetaInfo.setBubbleInfo(info);
    }

    @Override
    public void loadAllBubble(List<TCBubbleInfo> list) {
        locations = list;
        mRecycleLocations.setVisibility(View.VISIBLE);
        mLocationAdapter = new LocationBubbleAdapter(list);
        mLocationAdapter.setOnItemClickListener(this);
        LinearLayoutManager manager = new LinearLayoutManager(mContentView.getContext());
        mRecycleLocations.setLayoutManager(manager);
        mRecycleLocations.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        mRecycleLocations.setAdapter(mLocationAdapter);
    }

    @Override
    public void show(@Nullable TCSubtitleInfo info) {
        if (info == null) {
            resetInfo();
        } else {
            mLocationMetaInfo = info;
        }
        mContentView.post(new Runnable() {
            @Override
            public void run() {
                enterAnimator();
            }
        });
    }

    @Override
    public void dismiss() {
        exitAnimator();
    }

    @Override
    public void setOnBubbleSubtitleCallback(OnBubbleSubtitleCallback callback) {
        mCallback = callback;
    }

    private void exitAnimator() {
        ObjectAnimator translationY = ObjectAnimator.ofFloat(mContentView, "translationY", 0,
                mContentView.getHeight());
        AnimatorSet set = new AnimatorSet();
        set.setDuration(200);
        set.play(translationY);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                TCLocationPannel.super.setVisibility(GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        set.start();
    }

    @Override
    public void onItemClick(View view, int position) {
        mLocationMetaInfo.setBubblePos(position);
        mLocationMetaInfo.setBubbleInfo(locations.get(position));
        callback();
    }

    @Override
    public void onClick(@NonNull View v) {
        int i = v.getId();
        if (i == R.id.bubble_iv_bubble) {
            mLlColor.setVisibility(View.GONE);
            mTextColor.setSelected(false);
            mTextBubbleStyle.setSelected(true);
            mRecycleLocations.setVisibility(View.VISIBLE);
        } else if (i == R.id.bubble_iv_color) {
            mLlColor.setVisibility(View.VISIBLE);
            mTextColor.setSelected(true);
            mTextBubbleStyle.setSelected(false);
            mRecycleLocations.setVisibility(View.GONE);
        } else if (i == R.id.iv_close) {
            dismiss();
        }
    }

    @Override
    public void onFinishColor(@ColorInt int color) {
        mLocationMetaInfo.setTextColor(color);
        callback();
    }

    @Override
    public void onProgressColor(@ColorInt int color) {
        mCvColor.setColor(color);
    }

    private void callback() {
        if (mCallback != null) {
            TCSubtitleInfo info = new TCSubtitleInfo();
            info.setBubblePos(mLocationMetaInfo.getBubblePos());
            info.setBubbleInfo(mLocationMetaInfo.getBubbleInfo());
            info.setTextColor(mLocationMetaInfo.getTextColor());
            mCallback.onBubbleSubtitleCallback(info);
        }
    }

    public void setTabTextColor(int selectedColor, int normalColor) {
        int[] colors = new int[]{selectedColor, normalColor};
        int[][] states = new int[2][];
        states[0] = new int[]{android.R.attr.state_selected, android.R.attr.state_enabled};
        states[1] = new int[]{android.R.attr.state_enabled};
        ColorStateList colorList = new ColorStateList(states, colors);

        mTextBubbleStyle.setTextColor(colorList);
        mTextColor.setTextColor(colorList);
    }

    public void setTabTextSize(int size) {
        mTextBubbleStyle.setTextSize(size);
        mTextColor.setTextSize(size);
    }

    public void setCancelIconResource(int resid) {
        mImageClose.setImageResource(resid);
    }

}