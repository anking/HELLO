package com.tencent.qcloud.ugckit.module.effect.location;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tencent.qcloud.ugckit.R;

import java.lang.ref.WeakReference;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> implements View.OnClickListener {

    private List<LocationInfo> locations;
    private WeakReference<RecyclerView> mRecyclerView;

    public LocationAdapter(List<LocationInfo> locations) {
        this.locations = locations;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mRecyclerView == null)
            mRecyclerView = new WeakReference<>((RecyclerView) parent);
        return new LocationViewHolder(View.inflate(parent.getContext(), R.layout.ugckit_item_location_text, null));
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.itemView.setOnClickListener(this);
        holder.tvLocation.setText(this.locations.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return this.locations.size();
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            RecyclerView recyclerView = mRecyclerView.get();
            if (recyclerView != null) {
                int position = recyclerView.getChildAdapterPosition(v);
                mListener.onItemClick(v, position);
            }
        }
    }

    public static class LocationViewHolder extends RecyclerView.ViewHolder {
        TextView tvLocation;

        public LocationViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLocation = itemView.findViewById(R.id.tv_location);
        }
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

}
