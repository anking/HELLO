

package com.tencent.qcloud.ugckit.module.record;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.tencent.qcloud.ugckit.R;
import com.tencent.ugc.TXUGCPartsManager;
import com.yanhua.core.view.CommonDialog;

import java.util.Locale;

import static com.tencent.qcloud.ugckit.module.record.RecordModeView.RECORD_MODE_CLICK;

public class RecordBottomLayout extends RelativeLayout implements View.OnClickListener {
    private Activity mActivity;
    private TextView mTextProgressTime;

    private RecordSpeedLayout mRecordSpeedLayout;         // 速度面板
    private RecordButton mButtonRecord;              // 录制按钮
    private View mRecordModeDot;
    private LinearLayout mLLDeleteLastPart;  // 删除上一段
    private LinearLayout mLLRecordOk;  // 录制成功

    private View view_height;

    private boolean isSelectDeleteLastPartFlag;             // 是否点击一次过"删除最有一段分段视频"按钮

    private OnDeleteLastPartListener mOnDeleteLastPartListener;

    public RecordBottomLayout(Context context) {
        super(context);
        initViews();
    }

    public RecordBottomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RecordBottomLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        mActivity = (Activity) getContext();
        inflate(mActivity, R.layout.ugckit_record_bottom_layout, this);

        mTextProgressTime = (TextView) findViewById(R.id.record_progress_time);
        mTextProgressTime.setText(0.0 + getResources().getString(R.string.ugckit_unit_second));
        mTextProgressTime.setVisibility(View.INVISIBLE);

        mLLDeleteLastPart = findViewById(R.id.ll_delete_last_part);
        mLLDeleteLastPart.setOnClickListener(this);


        mLLRecordOk = findViewById(R.id.ll_record_ok);

        mButtonRecord = (RecordButton) findViewById(R.id.record_button);
        mRecordModeDot = findViewById(R.id.record_mode_dot);

        mButtonRecord.setCurrentRecordMode(RECORD_MODE_CLICK);

        view_height = findViewById(R.id.view_height);

        mRecordSpeedLayout = (RecordSpeedLayout) findViewById(R.id.record_speed_layout);
        mRecordSpeedLayout.setOnRecordSpeedListener(new RecordSpeedLayout.OnRecordSpeedListener() {
            @Override
            public void onSpeedSelect(int speed) {
                UGCKitRecordConfig.getInstance().mRecordSpeed = speed;
                VideoRecordSDK.getInstance().setRecordSpeed(speed);
            }
        });
    }

    public void setOnRecordOkClickListener(OnClickListener listener) {
        mLLRecordOk.setOnClickListener(listener);
    }

    public void setRecordOkVisible(boolean enable) {
        mLLRecordOk.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onClick(@NonNull View view) {
        int id = view.getId();
        if (id == R.id.ll_delete_last_part) {
            deleteLastPart();
        }
    }

    /**
     * 删除上一段
     */
    private void deleteLastPart() {
        int size = VideoRecordSDK.getInstance().getPartManager().getPartsPathList().size();
        if (size == 0) {
            // 没有任何分段视频，返回
            return;
        }

        //加个判断是需要点击两次的，先选中，此处有Dialog，无须点击两次
        //点击的时候就选中，然后再次弹出Dialog，确认是否删除
        mRecordOnProgressListener.selectLast();
        // 删除最后一段视频，更新进度条颜色
//            mRecordProgressView.deleteLast();
        final CommonDialog commonDialog = new CommonDialog(mActivity, "删除上段视频？", "", "删除", "取消");
        new XPopup.Builder(mActivity).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(new CommonDialog.OnButtonClickListener() {
            @Override
            public void onClick() {
                commonDialog.dismiss();

                mRecordOnProgressListener.deleteLast();
                VideoRecordSDK.getInstance().deleteLastPart();

                long duration = VideoRecordSDK.getInstance().getPartManager().getDuration();
                float timeSecond = duration / 1000;
                if (timeSecond > UGCKitRecordConfig.getInstance().mMaxDuration / 1000) {
                    timeSecond = UGCKitRecordConfig.getInstance().mMaxDuration / 1000;
                }
                mTextProgressTime.setText(String.format(Locale.CHINA, "%.1f", timeSecond) + getResources().getString(R.string.ugckit_unit_second));

                mOnDeleteLastPartListener.onUpdateTitle(timeSecond >= UGCKitRecordConfig.getInstance().mMinDuration / 1000);

                // 删除分段后再次判断size
                int size = VideoRecordSDK.getInstance().getPartManager().getPartsPathList().size();
                if (size == 0) { // 重新开始录
                    mOnDeleteLastPartListener.onReRecord();

                    mLLDeleteLastPart.setVisibility(INVISIBLE);
                }
            }
        });
        commonDialog.setOnCancelListener(new CommonDialog.OnButtonClickListener() {
            @Override
            public void onClick() {
                commonDialog.dismiss();
            }
        });
    }

    public void setOnRecordButtonListener(RecordButton.OnRecordButtonListener listener) {
        mButtonRecord.setOnRecordButtonListener(listener);
    }

    public void setOnDeleteLastPartListener(OnDeleteLastPartListener lister) {
        mOnDeleteLastPartListener = lister;
    }

    public void disableRecordSpeed() {
        mRecordSpeedLayout.setVisibility(View.GONE);
    }

    public void disableTakePhoto() {
        showRecordMode();
    }

    public void disableLongPressRecord() {
        showRecordMode();
    }

    //如果禁用拍照和长按拍摄，则仅剩下单击拍摄
    private void showRecordMode() {
        mRecordModeDot.setVisibility(View.GONE);
    }

    public interface OnDeleteLastPartListener {
        void onUpdateTitle(boolean enable);

        void onReRecord();
    }

    public void startRecord() {
        mLLDeleteLastPart.setVisibility(View.INVISIBLE);
//        mRecordSpeedLayout.setVisibility(View.INVISIBLE);
        mTextProgressTime.setVisibility(View.VISIBLE);
    }

    public void pauseRecord() {
        mTextProgressTime.setVisibility(View.INVISIBLE);

        int recordMode = mButtonRecord.getmRecordMode();

        if (recordMode == RecordModeView.RECORD_MODE_CLICK) {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    TXUGCPartsManager manager = VideoRecordSDK.getInstance().getPartManager();

                    if (null != manager) {
                        int size = manager.getDuration();
                        if (size == 0) {
                            mLLDeleteLastPart.setVisibility(INVISIBLE);
                        } else {
                            mLLDeleteLastPart.setVisibility(VISIBLE);
                        }
                    } else {
                        mLLDeleteLastPart.setVisibility(VISIBLE);
                    }
                }
            }, 100);
        } else {
            mLLDeleteLastPart.setVisibility(View.INVISIBLE);
        }
    }

    public void setVisibleRecordSpeed() {
        boolean enable = mRecordSpeedLayout.getVisibility() == View.VISIBLE ? false : true;

        mRecordSpeedLayout.setVisibility(enable ? View.VISIBLE : View.GONE);
    }

    /**
     * 更新录制进度Progress
     *
     * @param milliSecond
     */
    public void updateProgress(long milliSecond) {
//        mRecordProgressView.setProgress((int) milliSecond);
        mRecordOnProgressListener.setProgress((int) milliSecond);
        float second = milliSecond / 1000f;
        if (second > UGCKitRecordConfig.getInstance().mMaxDuration / 1000) {
            second = UGCKitRecordConfig.getInstance().mMaxDuration / 1000;
        }
        mTextProgressTime.setText(String.format(Locale.CHINA, "%.1f", second) + getResources().getString(R.string.ugckit_unit_second));
    }

    public RecordButton getRecordButton() {
        return mButtonRecord;
    }

    public View getViewBottomHeight() {
        return view_height;
    }

    public RecordSpeedLayout getRecordSpeedLayout() {
        return mRecordSpeedLayout;
    }

    /**
     *
     */
    public void swithToVideoRecord() {
//        mRecordSpeedLayout.setVisibility(VISIBLE);
//        mLLDeleteLastPart.setVisibility(VISIBLE);
    }

    public void swithToTakePhoto() {
        mRecordSpeedLayout.setVisibility(GONE);
        mLLDeleteLastPart.setVisibility(INVISIBLE);
        mLLRecordOk.setVisibility(INVISIBLE);
    }

    private RecordOnProgressListener mRecordOnProgressListener;

    public void setRecordOnProgressListener(RecordOnProgressListener listener) {
        mRecordOnProgressListener = listener;
    }

    public interface RecordOnProgressListener {
        void setProgress(int milliSecond);

        void selectLast();

        void deleteLast();
    }
}
