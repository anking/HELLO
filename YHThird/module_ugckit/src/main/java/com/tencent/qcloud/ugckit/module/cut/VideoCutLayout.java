package com.tencent.qcloud.ugckit.module.cut;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.blankj.utilcode.util.StringUtils;
import com.tencent.qcloud.ugckit.R;
import com.tencent.qcloud.ugckit.basic.UgckitConfig;
import com.tencent.qcloud.ugckit.component.slider.VideoCutView;
import com.tencent.qcloud.ugckit.module.PlayerManagerKit;
import com.tencent.qcloud.ugckit.module.effect.VideoEditerSDK;
import com.tencent.qcloud.ugckit.module.effect.utils.Edit;
import com.tencent.ugc.TXVideoEditConstants;

public class VideoCutLayout extends RelativeLayout implements IVideoCutLayout, View.OnClickListener, Edit.OnCutChangeListener {
    private static final String TAG = "VideoCutLayout";

    private FragmentActivity mActivity;
    private ImageView mImageRotate;
    private TextView mTextDuration, mChooseLeft;
    private VideoCutView mVideoCutView;

    private int mRotation;

    private OnRotateVideoListener mOnRotateVideoListener;

    public VideoCutLayout(Context context) {
        super(context);
        initViews();
    }

    public VideoCutLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public VideoCutLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        mActivity = (FragmentActivity) getContext();
        inflate(mActivity, R.layout.ugckit_video_cut_kit, this);

        mTextDuration = (TextView) findViewById(R.id.tv_choose_duration);
        mChooseLeft = (TextView) findViewById(R.id.tv_choose_left);

        mImageRotate = (ImageView) findViewById(R.id.iv_rotate);
        mVideoCutView = (VideoCutView) findViewById(R.id.video_edit_view);

        mImageRotate.setOnClickListener(this);
        mVideoCutView.setCutChangeListener(this);
    }

    @Override
    public void onClick(@NonNull View view) {
        if (view.getId() == R.id.iv_rotate) {
            // 当旋转角度大于等于270度的时候，下一次就是0度；
            mRotation = mRotation < 270 ? mRotation + 90 : 0;

            if (mOnRotateVideoListener != null) {
                mOnRotateVideoListener.onRotate(mRotation);
            }
        }
    }

    @Override
    public void onCutClick() {

    }

    @Override
    public void onCutChangeKeyDown() {
        PlayerManagerKit.getInstance().stopPlay();
    }

    @Override
    public void onCutChangeKeyUp(long startTime, long endTime, int type) {
        long durationS = (endTime - startTime) / 1000;

        if (durationS > UgckitConfig.VIDEO_CUT_MAX_TIME) {
            mChooseLeft.setVisibility(VISIBLE);

            long min = durationS / 60 - UgckitConfig.VIDEO_CUT_MAX_TIME / 60;
            long sencond = durationS % 60;

            String leftContent = "结尾将裁" + StringUtils.firstZero(String.valueOf(min)) + ":" + StringUtils.firstZero(String.valueOf(sencond));
            mChooseLeft.setText(leftContent);
        } else {
            mChooseLeft.setVisibility(GONE);
        }

        PlayerManagerKit.getInstance().startPlay();

        VideoEditerSDK.getInstance().setCutterStartTime(startTime, endTime);
    }

    @Override
    public void setVideoInfo(@NonNull TXVideoEditConstants.TXVideoInfo videoInfo) {
        mRotation = 0;

        int durationS = (int) (videoInfo.duration / 1000);
        int thumbCount = durationS / 3;

        if (durationS > UgckitConfig.VIDEO_CUT_MAX_TIME) {
            mChooseLeft.setVisibility(VISIBLE);

            long min = durationS / 60 - UgckitConfig.VIDEO_CUT_MAX_TIME / 60;
            long sencond = durationS % 60;

            String leftContent = "结尾将裁" + StringUtils.firstZero(String.valueOf(min)) + ":" + StringUtils.firstZero(String.valueOf(sencond));
            VideoEditerSDK.getInstance().setCutterStartTime(0, UgckitConfig.VIDEO_CUT_MAX_TIME * 1000);//设置最大时长3分钟
            mChooseLeft.setText(leftContent);
        } else {
            mChooseLeft.setVisibility(GONE);

            VideoEditerSDK.getInstance().setCutterStartTime(0, durationS * 1000);

//            if (durationS > 60) {
//                int min = durationS / 60;
//                int sencond = durationS % 60;
//                mTextDuration.setText(getResources().getString(R.string.ugckit_video_cutter_activity_load_video_success_already_picked) + min + "分钟" + sencond + "秒");
//            } else {
//                mTextDuration.setText(getResources().getString(R.string.ugckit_video_cutter_activity_load_video_success_already_picked) + durationS + "秒");
//            }
        }


        mVideoCutView.setMediaFileInfo(videoInfo);
        mVideoCutView.setCount(thumbCount);
    }

    @Override
    public void addThumbnail(int index, Bitmap bitmap) {
        mVideoCutView.addBitmap(index, bitmap);
    }

    public void clearThumbnail() {
        mVideoCutView.clearAllBitmap();
    }

    public VideoCutView getVideoCutView() {
        return mVideoCutView;
    }

    @Override
    public void setOnRotateVideoListener(OnRotateVideoListener listener) {
        mOnRotateVideoListener = listener;
    }

}
