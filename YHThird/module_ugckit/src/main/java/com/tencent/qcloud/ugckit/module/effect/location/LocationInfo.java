package com.tencent.qcloud.ugckit.module.effect.location;

import java.io.Serializable;

public class LocationInfo implements Serializable {

    private double latitude;
    private double longitude;
    private String title;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
