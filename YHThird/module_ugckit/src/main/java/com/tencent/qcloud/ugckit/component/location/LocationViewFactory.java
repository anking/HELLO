package com.tencent.qcloud.ugckit.component.location;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.tencent.qcloud.ugckit.R;


/**
 * 创建 OperationView的工厂
 */
public class LocationViewFactory {

    @NonNull
    public static LocationView newOperationView(Context context) {
        return (LocationView) View.inflate(context, R.layout.ugckit_layout_default_location_view, null);
    }
}
