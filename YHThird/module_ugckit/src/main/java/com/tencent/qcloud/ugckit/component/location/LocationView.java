package com.tencent.qcloud.ugckit.component.location;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;
import com.tencent.qcloud.ugckit.component.floatlayer.FloatLayerView;

public class LocationView extends FloatLayerView {
    private static final String TAG = "BubbleView";

    private long mStartTime;
    private long mEndTime;

    @Nullable
    private LocationViewParams mLocationViewParams;

    public LocationView(Context context) {
        super(context);
    }

    public LocationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setLocationParams(@Nullable LocationViewParams params) {
        mLocationViewParams = params;
        if (params == null) {
            return;
        }
        if (params.text == null) {
            params.text = "";
            Log.w(TAG, "setBubbleParams: bubble text is null");
        }
        LocationViewHelper helper = new LocationViewHelper();
        helper.setLocationTextParams(params);
        Bitmap bitmap = helper.createBubbleTextBitmap();
        setImageBitamp(bitmap);
        mLocationViewParams.bubbleBitmap = null;
        invalidate();
    }

    @Nullable
    public LocationViewParams getLocationParams() {
        return mLocationViewParams;
    }

    public void setStartToEndTime(long startTime, long endTime) {
        mStartTime = startTime;
        mEndTime = endTime;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public long getEndTime() {
        return mEndTime;
    }


}