package com.tencent.qcloud.ugckit.module.effect.location;

import java.util.List;

/**
 * 定位面板
 */
public interface ILocationPannel {

    /**
     * 加载所有的定位
     */
    void loadAllLocation(List<LocationInfo> list);

    /**
     * 显示定位面板
     *
     * @param info 编辑已存在的定位，如果不存在传入null
     */
    void show(LocationMetaInfo info);

    /**
     * 隐藏定位面板
     */
    void dismiss();

    /**
     * 设置添加定位回调接口
     *
     * @param callback
     */
    void setOnLocationCallback(OnLocationCallback callback);

    interface OnLocationCallback {
        /**
         * 添加一个气泡字幕
         *
         * @param info
         */
        void onLocationCallback(LocationMetaInfo info);
    }

}
