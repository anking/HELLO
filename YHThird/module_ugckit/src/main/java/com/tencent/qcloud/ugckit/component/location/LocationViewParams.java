package com.tencent.qcloud.ugckit.component.location;

import android.graphics.Bitmap;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tencent.qcloud.ugckit.module.effect.location.LocationInfo;
import com.tencent.qcloud.ugckit.module.effect.location.LocationMetaInfo;

/**
 * 用于初始化气泡字幕控件{@link LocationView} 的参数配置
 */
public class LocationViewParams {

    @Nullable
    public Bitmap         bubbleBitmap;
    public LocationMetaInfo wordParamsInfo;
    public String         text;
    @NonNull
    public static LocationViewParams createDefaultParams(String text) {
        LocationViewParams params = new LocationViewParams();
        params.bubbleBitmap = null;
        params.text = text;

        LocationMetaInfo info = new LocationMetaInfo();
        info.setTextColor(Color.WHITE);

        // 初始化为无字幕的 配置信息
        // 创建一个默认的
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setTitle("");
        locationInfo.setLatitude(0);
        locationInfo.setLongitude(0);
        info.setLocationInfo(locationInfo);

        params.wordParamsInfo = info;
        return params;
    }
}
