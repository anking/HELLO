package com.yanhua.rong;

import io.rong.imlib.model.Conversation;

public class RongIMUtils {
    public static Conversation.ConversationType getConversationType(String name) {
        Conversation.ConversationType[] conversationTypeEnums = Conversation.ConversationType.values();
        for (Conversation.ConversationType conversationTypeEnum : conversationTypeEnums) {
            if (conversationTypeEnum.getName().equals(name)) {
                return conversationTypeEnum;
            }
        }
        return null;
    }

}
