/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.rong.imkit;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "io.rong.imkit";
  public static final String BUILD_TYPE = "debug";
  // Field from default config.
  public static final String IMSDK_VER = "5.1.8.3";
}
