/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.rong.recognizer;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "io.rong.recognizer";
  public static final String BUILD_TYPE = "debug";
}
