package com.chinalwb.are.styles;

import android.graphics.Color;
import android.view.View;

public class ARE_Helper {

    /**
     * Updates the check status.
     *
     * @param areStyle
     * @param checked
     */
    public static void updateCheckStatus(IARE_Style areStyle, boolean checked) {
        areStyle.setChecked(checked);
        View imageView = areStyle.getImageView();
//        int color = checked ? Constants.CHECKED_COLOR : Constants.UNCHECKED_COLOR;
        String color = checked ? "#FFEEEEEE" : "#00000000";
        imageView.setBackgroundColor(Color.parseColor(color));
    }
}
