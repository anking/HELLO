package com.luck.picture.lib.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;
import androidx.viewpager.widget.ViewPager;

import com.luck.picture.lib.listener.OnDragChangeListener;
import com.luck.picture.lib.tools.ScreenUtils;

/**
 * @author：luck
 * @date：2016-12-31 22:12
 * @describe：PreviewViewPager
 */

public class PreviewViewPager extends ViewPager {
    private MyViewPageHelper helper;
    private OnDragChangeListener dragChangeListener;
    public ViewPager viewPager;

    private int HideTopThreshold = 40;
    private int maxOffset;
    public boolean isReleasing = false;

    private ViewDragHelper dragHelper;
    private Context mContext;


    public PreviewViewPager(Context context) {
        super(context);

        init(context);
    }

    public PreviewViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        helper = new MyViewPageHelper(this);
//        init(context);
    }

    private void init(Context context) {
        mContext = context;

        HideTopThreshold = ScreenUtils.dip2px(mContext, HideTopThreshold);

        viewPager = this;
        dragHelper = ViewDragHelper.create(this, cb);

        setBackgroundColor(Color.TRANSPARENT);
    }


    @Override
    public void setCurrentItem(int item) {
        setCurrentItem(item, true);
    }

    @Override
    public void setCurrentItem(int item, boolean smoothScroll) {
        MScroller scroller = helper.getScroller();
        if (Math.abs(getCurrentItem() - item) > 1) {
            scroller.setNoDuration(true);
            super.setCurrentItem(item, smoothScroll);
            scroller.setNoDuration(false);
        } else {
            scroller.setNoDuration(false);
            super.setCurrentItem(item, smoothScroll);
        }
    }

//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        maxOffset = getHeight() / 3;
//    }

//
//    @Override
//    public void computeScroll() {
//        super.computeScroll();
//        if (dragHelper.continueSettling(false)) {
//            ViewCompat.postInvalidateOnAnimation(viewPager);
//        }
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        if (ev.getPointerCount() > 1) return false;
//        dragHelper.processTouchEvent(ev);
//        try {
//            return super.onTouchEvent(ev);
//        } catch (IllegalArgumentException ex) {
//            ex.printStackTrace();
//        }
//        return false;
//    }
//
//    //
//    boolean isVertical = false;
//    private float touchX, touchY,touchOldX, touchOldY,
//            touchNewX ,touchNewY;;
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (ev.getPointerCount() > 1) return super.dispatchTouchEvent(ev);
//        try {
//            switch (ev.getAction()) {
//                case MotionEvent.ACTION_DOWN:
//                    touchX = ev.getX();
//                    touchY = ev.getY();
//
//                    touchNewX = ev.getX();
//                    touchNewY = ev.getY();
//                    break;
//                case MotionEvent.ACTION_MOVE:
//                    float dx = ev.getX() - touchX;
//                    float dy = ev.getY() - touchY;
//                    isVertical = (Math.abs(dy) > Math.abs(dx));
//                    touchX = ev.getX();
//                    touchY = ev.getY();
//                    touchOldX = ev.getX();
//                    touchOldY = ev.getY();
//                    break;
//                case MotionEvent.ACTION_UP:
//                case MotionEvent.ACTION_CANCEL:
//                    float upDx = touchOldX - touchNewX;
//                    float upDy = touchOldY - touchNewY;
//                    isVertical = (Math.abs(upDy) > Math.abs(upDx));
//                    break;
//            }
//        } catch (Exception e) {
//        }
//        return super.dispatchTouchEvent(ev);
//    }
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        if (isVertical) {
//            boolean result = dragHelper.shouldInterceptTouchEvent(ev);
//            if (ev.getPointerCount() > 1 && ev.getAction() == MotionEvent.ACTION_MOVE) return false;
//            if (isVertical) return true;
//            return result && isVertical;
//        } else {
//            try {
//                return super.onInterceptTouchEvent(ev);
//            } catch (IllegalArgumentException ex) {
//                ex.printStackTrace();
//            }
//            return false;
//        }
//    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException ignored) {
        }
        return false;
    }

    public void setOnDragChangeListener(OnDragChangeListener listener) {
        this.dragChangeListener = listener;
    }


    ViewDragHelper.Callback cb = new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(@NonNull View view, int i) {
            return !isReleasing;
        }

        @Override
        public int getViewVerticalDragRange(@NonNull View child) {
            return 1;
        }

        @Override
        public int clampViewPositionVertical(@NonNull View child, int top, int dy) {
            int t = viewPager.getTop() + dy / 2;
            if (t >= 0) {
                return Math.min(t, maxOffset);
            } else {
                return -Math.min(-t, maxOffset);
            }
        }

        @Override
        public void onViewPositionChanged(@NonNull View changedView, int left, int top, int dx, int dy) {
            super.onViewPositionChanged(changedView, left, top, dx, dy);
            if (changedView != viewPager) {
                viewPager.offsetTopAndBottom(dy);
            }
            float fraction = Math.abs(top) * 1f / maxOffset;
            float pageScale = 1 - fraction * .2f;
            viewPager.setScaleX(pageScale);
            viewPager.setScaleY(pageScale);
            changedView.setScaleX(pageScale);
            changedView.setScaleY(pageScale);
            if (dragChangeListener != null) {
                dragChangeListener.onDragChange(dy, pageScale, fraction);
            }

        }

        @Override
        public void onViewReleased(@NonNull View releasedChild, float xvel, float yvel) {
            super.onViewReleased(releasedChild, xvel, yvel);
            if (Math.abs(releasedChild.getTop()) > HideTopThreshold) {
                if (dragChangeListener != null) dragChangeListener.onRelease();
            } else {
                dragHelper.smoothSlideViewTo(viewPager, 0, 0);
                dragHelper.smoothSlideViewTo(releasedChild, 0, 0);
                ViewCompat.postInvalidateOnAnimation(viewPager);
            }
        }
    };

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        isReleasing = false;
    }
}
