package com.luck.picture.lib.listener;

public interface OnDragChangeListener {
    void onRelease();
    void onDragChange(int dy, float scale, float fraction);
}
