package com.yanhua.home.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.imageview.ShapeableImageView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.util.SmartGlideImageLoader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.adapter.ContentCommentAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CommentInputTemp;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.InviteConfigModel;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.model.InviteMemberNum;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.MapLocationDetailModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.CommentInputPopup;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.expandabletext.ExpandableTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.InviteMemberPhotoAdapter;
import com.yanhua.home.presenter.InviteDetailPresenter;
import com.yanhua.home.presenter.contract.InviteDetailContract;
import com.yanhua.rong.msgprovider.InviteMessage;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 约伴详情页
 *
 * @author Administrator
 */
@Route(path = ARouterPath.INVITE_DETAIL_ACTIVITY)
public class InviteDetailActivity extends BaseMvpActivity<InviteDetailPresenter> implements InviteDetailContract.IView {

    @BindView(R2.id.tb_base)
    LinearLayout tbBase;
    @BindView(R2.id.rv_comment)
    RecyclerView rvComment;
    @BindView(R2.id.iconFav)
    AliIconFontTextView iconFav;

    @BindView(R2.id.llTop)
    LinearLayout llTop;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.ll_nodata)
    LinearLayout llNodata;

    @BindView(R2.id.tv_total_comment)//留言 0
    TextView tvTotalComment;

    @BindView(R2.id.tv_content)
    ExpandableTextView tvContent;

    @BindView(R2.id.ll_content)
    LinearLayout llContent;
    //关注
    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.fl_backgroud)
    FrameLayout flBackgroud;

    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.tv_name)
    TextView tvTitle;

    @BindView(R2.id.tvPeopleNum)
    TextView tvPeopleNum;//"👉8人局（5/8）/ 地主买单 / 男"
    @BindView(R2.id.tvTime)//2021年08月28日 21:00点开始
    TextView tvTime;
    @BindView(R2.id.tvName)
    TextView tvName;
    @BindView(R2.id.rivHeadPic)
    ShapeableImageView rivHeadPic;
    @BindView(R2.id.ivInfoSex)
    ImageView ivInfoSex;

    @BindView(R2.id.tvInviteType)
    TextView tvInviteType;
    @BindView(R2.id.tvInviteTopName)
    TextView tvInviteTopName;
    @BindView(R2.id.timeTick)
    TextView timeTick;

    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;

    @BindView(R2.id.bannerPhotoWall)
    Banner bannerPhotoWall;

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    //底部操作按钮
    @BindView(R2.id.tvJoin)
    TextView tvJoin;//人员已满显示为灰色

    @BindView(R2.id.llJoined)
    LinearLayout llJoined;

    @BindView(R2.id.btnCancelJoin)
    RelativeLayout btnCancelJoin;

    @BindView(R2.id.btnChat)
    LinearLayout btnChat;

    @BindView(R2.id.tvEditInfo)
    TextView tvEditInfo;

    //位置处
    @BindView(R2.id.rivInvitePic)
    ShapeableImageView rivInvitePic;
    @BindView(R2.id.tvInviteName)
    TextView tvInviteName;
    @BindView(R2.id.tvInviteAddress)
    TextView tvInviteAddress;

    //成员-------------
    @BindView(R2.id.llInviteMember)
    LinearLayout llInviteMember;
    @BindView(R2.id.llMore)
    LinearLayout llMore;

    @BindView(R2.id.tvLeftJoinNum)
    TextView tvLeftJoinNum;
    @BindView(R2.id.tvDealToJoiningNum)
    TextView tvDealToJoiningNum;//200成员等待加入
    @BindView(R2.id.llLookMore)
    LinearLayout llLookMore;
    @BindView(R2.id.rvContent)
    DataObserverRecyclerView rvContent;

    @BindView(R2.id.llTopTip)
    LinearLayout llTopTip;
    @BindView(R2.id.tvInviteTip)
    TextView tvInviteTip;
    @BindView(R2.id.bottom)
    RelativeLayout bottom;

    @BindView(R2.id.civ_user)
    CircleImageView civHeadPic;

    private boolean isSelf;

    @Autowired
    String id;
    @Autowired
    boolean showTip;

    private final static int CHILD_SIZE = 1;
    private List<CommentModel> mList = new ArrayList<>();

    //组装后的评论列表
    private List<CommentModel> mCommentList = new ArrayList<>();

    private int current = 1;
    private ContentCommentAdapter mAdapter;
    private int currentPosition;

    private InviteModel mModel;

    private boolean isReply = false;

    private int index = 0;
    private Timer timer;
    private boolean isFirstLoad;//控制第一次加载和下拉的控制

    private InviteMemberPhotoAdapter adapter;

    @Override
    protected void creatPresent() {
        basePresenter = new InviteDetailPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_detail;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        basePresenter.meetPartnerInviteDetail(id);
        basePresenter.getCommentList(id, current, 1);
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        btnChat.setVisibility(View.GONE);
        llContent.setVisibility(View.GONE);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContentCommentAdapter(mContext, "");

        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mList.size() == 0 || mList.size() % 10 != 0) {
                refreshLayout.finishLoadMore(1000, true, true);
                return;
            }
            current = current + 1;
            refreshLayout.finishLoadMore(1000);
            basePresenter.getCommentList(id, current, 1);
        });

        rvComment.setLayoutManager(manager);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvComment.setRecycledViewPool(pool);

        //关闭recyclerview动画
        rvComment.getItemAnimator().setAddDuration(0);
        rvComment.getItemAnimator().setChangeDuration(0);
        rvComment.getItemAnimator().setMoveDuration(0);
        rvComment.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvComment.getItemAnimator()).setSupportsChangeAnimations(false);

        rvComment.setAdapter(mAdapter);

        rvComment.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        mAdapter.setOnADClickListener(clickItem -> {
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);
            PageJumpUtil.adJumpContent(clickItem, (Activity) mContext, -1);
        });

        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            if (cellType == 0) {
                String uId = model.getUserId();
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", uId)
                        .navigation();
            } else if (cellType == 1) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    String commentId = model.getId();
//                    basePresenter.updateStarComment(commentId, YXConfig.TYPE_INVITE);
                    basePresenter.updateStarComment(commentId);
                });

            } else if (cellType == 2) {
                int groupPosition = model.getGroupPosition();
                String parentCommentId = mList.get(groupPosition).getId();
                List<CommentModel> childCommentList = mList.get(groupPosition).getChildrens();
                int size = childCommentList.size() + 5;
                basePresenter.getChildCommentList(parentCommentId, 1, size);
            }
        });

        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            boolean myself = UserManager.getInstance().isMyself(model.getUserId());

            boolean isMyContent = UserManager.getInstance().isMyself(mModel.getUserId());
            CommentBoardPopup popup = new CommentBoardPopup(mActivity, isMyContent, true, true, !myself, myself);
            popup.setOnActionItemListener(actionType -> {
                switch (actionType) {
                    case CommentBoardPopup.ACTION_TOP:
                        toTop(pos);
                        break;
                    case CommentBoardPopup.ACTION_REPLY:
                        //回复
                        replyComment(model);
                        break;
                    case CommentBoardPopup.ACTION_COPY:
                        String content = model.getCommentDesc();

                        YHStringUtils.copyContent(mActivity, content);
                        break;
                    case CommentBoardPopup.ACTION_REPORT:
                        toReport(pos);
                        break;
                    case CommentBoardPopup.ACTION_DELETE:
                        toDelete(pos);
                        break;
                }
            });

            new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
        });

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            //回复
            replyComment(model);
        });

        setTopView();
    }

    float alpha;

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        int width = DisplayUtils.getScreenWidth(mContext);
        ViewGroup.LayoutParams imageLayoutParams = bannerPhotoWall.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = width;
        bannerPhotoWall.setLayoutParams(imageLayoutParams);

        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;


            boolean collectState = mModel.getCollectState() == 1;
            iconFav.setText(collectState ? "\ue760" : "\ue742");
            alpha = 1 - rangle;
            if (alpha > 0 && alpha < 0.3) {
                toolbar.setAlpha(1 - alpha);
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_black);
                iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.white));

                iconFav.setBackgroundResource(R.drawable.shape_half_r_black);

                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvEditInfo.setBackgroundResource(R.drawable.shape_half_r_black);
                tvTitle.setVisibility(View.GONE);
            } else if (alpha < 1 && alpha >= 0.3) {
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.white);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);

                iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.mainWord));

                iconFav.setBackgroundResource(R.drawable.shape_half_r_tran);

                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.theme));
                tvEditInfo.setBackgroundResource(R.drawable.shape_bg_binded);
                tvTitle.setVisibility(View.VISIBLE);

            } else if (alpha == 1) {//避免滑动过快还显示半透明状态
                mAppBarLayout.postDelayed(() -> {
                    toolbar.setAlpha(1 - rangle);
                    toolbar.setBackgroundResource(R.color.white);

                    tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                    iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);

                    iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.mainWord));
                    iconFav.setSelected(collectState);
                    iconFav.setBackgroundResource(R.drawable.shape_half_r_tran);

                    tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.theme));
                    tvEditInfo.setBackgroundResource(R.drawable.shape_bg_binded);
                    tvTitle.setVisibility(View.VISIBLE);
                }, 100);
            } else if (alpha == 0) {//避免滑动过快还显示半透明状态
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconFav.setBackgroundResource(R.drawable.shape_half_r_black);
                iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.white));

                tvEditInfo.setBackgroundResource(R.drawable.shape_half_r_black);
                tvEditInfo.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvTitle.setVisibility(View.GONE);
            }
        });
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }

        adapter = new InviteMemberPhotoAdapter(mContext);
        LinearLayoutManager mManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvContent.setLayoutManager(mManager);
        rvContent.setAdapter(adapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果

        adapter.setOnItemClickListener((itemView, pos) -> {
            //
            jump2InviteMember();
        });
    }

    private void jump2InviteMember() {
        if (isSelf) {
            ARouter.getInstance().build(ARouterPath.INVITE_MEMBER_LIST_ACTIVITY)
                    .withString("inviteId", id)
                    .navigation();
        } else {
            if (showOpenSetEnroll == 0) {
                return;
            }
            ARouter.getInstance().build(ARouterPath.INVITE_ENROLL_LIST_ACTIVITY)
                    .withString("inviteId", id)
                    .withInt("showOpenSetEnroll", showOpenSetEnroll)
                    .navigation();
        }
    }


    /**
     * 置顶评论
     *
     * @param pos
     */
    private void toTop(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();

        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.dealCommentItem2Top(contentId, YXConfig.TYPE_INVITE, pos);
        });
    }

    @Override
    public void handleDealItemTopSuccess(int pos) {
        if (mAdapter != null) {
            List list = mAdapter.getmDataList();
            Collections.swap(list, pos, 0);
            mAdapter.notifyItemMoved(pos, 0);
        }
    }

    /**
     * 删除评论
     *
     * @param pos
     */
    private void toDelete(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();

        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!UserManager.getInstance().isMyself(currentUserId)) {
                ToastUtils.showShort("只能删除自己的评论");
                return;
            }

            basePresenter.deleteCommentContent(contentId, pos);
        });
    }

    @Override
    public void handleDeleteCommentSuccess(int pos) {
        if (mAdapter != null) {
            mAdapter.notifyItemRemoved(pos);
            ToastUtils.showShort("评论删除成功");

            int replyCount = mModel.getMessageNum() - 1;
            mModel.setMessageNum(replyCount);
            tvTotalComment.setText("留言 " + replyCount);

            showNoDataView(replyCount);
        }

    }

    /**
     * 回复动作
     *
     * @param model
     */
    private void replyComment(CommentModel model) {
        String commentId = model.getId();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        int childType = model.getChildType();
        showInputPopup(commentId, nickName, childType);
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        InviteConfigModel inviteConfig = DiscoCacheUtils.getInstance().getInviteConfig();
        if (showTip && null != inviteConfig && !TextUtils.isEmpty(inviteConfig.getMeetPartnerTip()) && inviteConfig.getMeetPartnerOpenTip() == 1) {
            llTopTip.setVisibility(View.VISIBLE);
            tvInviteTip.setText(inviteConfig.getMeetPartnerTip());
        } else {
            llTopTip.setVisibility(View.GONE);
        }

        CommentInputTemp.setNick("");
        CommentInputTemp.setContent("");
        // 评论列表
        isFirstLoad = true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        switch (event.getEventName()) {
            case CommonConstant.REFRESH_CONTENT_COMMENTS_COUNT:
                current = 1;
                basePresenter.getCommentList(id, current, 1);
                break;
        }
    }

    @OnClick({R2.id.iconMore,
            R2.id.iconFav,
            R2.id.btnCancelJoin,
            R2.id.llInviteAddress,
            R2.id.llLookMore,
            R2.id.iconDelete,
            R2.id.tvJoin,
            R2.id.tvLeaveMsg,
            R2.id.tvCommentSomething,
            R2.id.tvEditInfo,
            R2.id.tv_name})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.iconMore) {
            //展示更多
            MoreActionBoard.shareInviteDetail(mActivity, mModel, (position, tp) -> {
                switch (position) {
                    case ShareBoardPopup.BOARD_SET_REPORT:
                        toReport(-1);
                        break;
                    case ShareBoardPopup.BOARD_SET_DELETE:
                        deleteContent();
                        break;
                    case ShareBoardPopup.BOARD_STOP_INVITE:
                        stopInvite();

                    case ShareBoardPopup.BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == mModel) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, mModel, YXConfig.TYPE_INVITE, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                }
            });
        } else if (view.getId() == R.id.iconFav) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toCollect();
            });
        } else if (view.getId() == R.id.tvEditInfo) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                int status = mModel.getStatus();
                if (status == 7) {//进行中
                    CommonDialog commonDialog = new CommonDialog(this, "进入编辑后，此条内容将不展示在约伴信息流，其他用户暂不可报名，" +
                            "且编辑后的内容需二次审核，请确认是否进入编辑？", "确认", "取消");
                    new XPopup.Builder(this).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();

                        jump2Modify();
                    });
                    commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
                } else {
                    jump2Modify();
                }
            });
        } else if (view.getId() == R.id.tv_name) {
            String userId = mModel.getUserId();
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", userId)
                    .navigation();
        } else if (view.getId() == R.id.llLookMore) {
            if (leftNum + waitInviteNum > 0) {
                jump2InviteMember();
            }
        } else if (view.getId() == R.id.tvJoin) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                HashMap<String, Object> params = new HashMap<>();
                params.put("inviteId", id);
                params.put("userId", UserManager.getInstance().getUserId());
                basePresenter.meetPartnerInviteSignIn(params);
            });
        } else if (view.getId() == R.id.iconDelete) {
            llTopTip.setVisibility(View.GONE);
            EventBus.getDefault().post(new MessageEvent(CommonConstant.ACTION_HIDE_INVITE_TOP_TIP));
        } else if (view.getId() == R.id.btnCancelJoin) {
            //
            ARouter.getInstance().build(ARouterPath.INVITE_CANCEL_ACTIVITY)
                    .withString("inviteId", id)
                    .withInt("inviteUserType", 1)
                    .withString("inviteUserId", mModel.getEnrollId())
                    .navigation();
        } else if (view.getId() == R.id.llInviteAddress) {
            checkLoacationStatus();
        } else if (view.getId() == R.id.tvLeaveMsg || view.getId() == R.id.tvCommentSomething) {
            showInputPopup("", "", 0);
        }
    }

    private void jump2Modify() {
        //    约伴-邀约信息表单
        InviteAddForm inviteAddForm = new InviteAddForm();
        int activityNumber = mModel.getActivityNumber();//
        inviteAddForm.setActivityNumber(activityNumber);

        String activityTime = mModel.getActivityTime();
        inviteAddForm.setActivityTime(activityTime);
        String city = mModel.getCity();
        inviteAddForm.setCity(city);
        String destination = mModel.getDestination();
        inviteAddForm.setDestination(destination);
        String destinationLatitude = mModel.getDestinationLatitude();
        inviteAddForm.setDestinationLatitude(destinationLatitude);
        String destinationLongitude = mModel.getDestinationLongitude();
        inviteAddForm.setDestinationLongitude(destinationLongitude);
        String id = mModel.getId();
        inviteAddForm.setId(id);
        String coverUrl = mModel.getCoverUrl();
        inviteAddForm.setCoverUrl(coverUrl);
        ArrayList<String> imagesUrl = mModel.getImagesUrl();
        inviteAddForm.setImagesUrl(imagesUrl);
        String introduction = mModel.getIntroduction();
        inviteAddForm.setIntroduction(introduction);
        String meetPartnerTypeId = mModel.getMeetPartnerTypeId();
        inviteAddForm.setMeetPartnerTypeId(meetPartnerTypeId);
        String partnerTypeName = mModel.getPartnerTypeName();
        inviteAddForm.setPartnerTypeName(partnerTypeName);
        String paymentTypeName = mModel.getPaymentTypeName();
        inviteAddForm.setPaymentTypeName(paymentTypeName);
        String partnerTypeId = mModel.getPartnerTypeId();
        inviteAddForm.setPartnerTypeId(partnerTypeId);
        String paymentTypeId = mModel.getPaymentTypeId();
        inviteAddForm.setPaymentTypeId(paymentTypeId);
        String meetPartnerTypeTemplateId = mModel.getMeetPartnerTypeTemplateId();
        inviteAddForm.setMeetPartnerTypeTemplateId(meetPartnerTypeTemplateId);
        String title = mModel.getTitle();
        inviteAddForm.setTitle(title);
        String userId = mModel.getUserId();
        inviteAddForm.setUserId(userId);
        inviteAddForm.setMeetPartnerTypeName(mModel.getMeetPartnerTypeName());


        int status = mModel.getStatus();
        boolean editable = false;
        if (status == 7) {
            int enrollNumber = mModel.getEnrollNumber();
            editable = enrollNumber == 0;
        }
        //发布邀约
        boolean finalEditable = editable;
        PageJumpUtil.firstRealNameValidThenJump(() -> PageJumpUtil.pageToPublishInvite(inviteAddForm, finalEditable));
    }

    private void deleteContent() {
        basePresenter.meetPartnerInviteDelete(mModel.getId());
    }

    @Override
    public void handleDeleteContentSuccess() {
        onBackPressed();//删除自己的内容，返回上一级页面
    }

    @Override
    public void handInviteSignResult(boolean signin) {
        if (true) {
            ToastUtils.showShort("报名成功");
        }
        basePresenter.meetPartnerInviteDetail(id);
    }

    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        MapLocationDetailModel locationDetailModel = new MapLocationDetailModel();

        locationDetailModel.setAddress(mModel.getAddress());
        locationDetailModel.setName(mModel.getDestination());
        locationDetailModel.setLatitude(mModel.getDestinationLatitude());
        locationDetailModel.setLongitude(mModel.getDestinationLongitude());

        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {//开了定位服务
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // 没有权限
            } else {
                // 有权限了
            }
            ARouter.getInstance()
                    .build(ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
                    .withSerializable("addressDetail", locationDetailModel)
//                    .withString("distanceKM", distanceKM)
                    .navigation();
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });

            commonDialog.setOnCancelListener(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
                        .withSerializable("addressDetail", locationDetailModel)
//                        .withString("distanceKM", distanceKM)
                        .navigation();
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }

    /**
     * 收藏或者取消收藏
     */
    private void toCollect() {
        basePresenter.updateCollectContent(id, YXConfig.TYPE_INVITE);
    }


    private void stopInvite() {
        ARouter.getInstance().build(ARouterPath.INVITE_CANCEL_ACTIVITY)
                .withString("inviteId", id)
                .withInt("inviteUserType", 3)//取消邀约（发起邀约这取消这个活动）
                .navigation();
//        basePresenter.meetPartnerStopReason();
    }

    /**
     * 举报内容或者评论
     *
     * @param pos
     */
    private void toReport(int pos) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            int reportType = 0;
            String contentId = "";
            String currentUserId = null;
            if (pos >= 0) {
                reportType = YXConfig.reportType.comment;
                CommentModel model = mCommentList.get(pos);
                contentId = model.getId();
                currentUserId = model.getUserId();
            } else {
                reportType = YXConfig.reportType.invite;
                contentId = mModel.getId();
                currentUserId = mModel.getUserId();
            }

            if (TextUtils.isEmpty(currentUserId) || currentUserId.equals(UserManager.getInstance().getUserId())) {
                ToastUtils.showShort(reportType == 1 ? "不能举报自己的内容" : "不能举报自己的评论");
                return;
            }

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", contentId)
                    .withInt("reportType", reportType)
                    .navigation();

        });
    }


    private void setFavIconColor(boolean collectState) {
        iconFav.setText(collectState ? "\ue760" : "\ue742");

        if (alpha >= 0 && alpha < 0.3) {
            iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.white));
        } else if (alpha <= 1 && alpha >= 0.3) {
            iconFav.setTextColor(mActivity.getResources().getColor(collectState ? R.color.subFFDB25 : R.color.mainWord));
        }
    }

    @Override
    public void handleInviteDetail(InviteModel data) {
        mModel = data;
        if (null != mModel) {
            mAdapter.setPublishUserId(mModel.getUserId());

            llContent.setVisibility(View.VISIBLE);
            //判断发布者是不是当前登录的用户
            isSelf = UserManager.getInstance().isMyself(mModel.getUserId());

//            tvEditInfo.setVisibility(isSelf ? View.VISIBLE : View.GONE);
            iconFav.setVisibility(!isSelf ? View.VISIBLE : View.GONE);
            llInviteMember.setVisibility(isSelf ? View.VISIBLE : View.GONE);

            //是否收藏
            boolean collectState = mModel.getCollectState() == 1;
            setFavIconColor(collectState);

            //邀约信息
            tvName.setText(mModel.getUserName());
            ImageLoaderUtil.loadImgCenterCrop(rivHeadPic, mModel.getUserImage());
            ImageLoaderUtil.loadImgCenterCrop(civHeadPic, mModel.getUserImage());

            tvTitle.setText(mModel.getTitle());
            tvContent.setContent(YHStringUtils.value(mModel.getIntroduction()));

            String time = mModel.getActivityTime();//"activityTime": "2022-04-17 10:33:00",2022-04-17 10:33:00
            tvTime.setText(YXTimeUtils.inviteTime(time) + "开始");

            String inviteType = mModel.getMeetPartnerTypeName();
            if (TextUtils.isEmpty(inviteType)) {
                tvInviteType.setVisibility(View.GONE);
            } else {
                tvInviteType.setVisibility(View.VISIBLE);
                tvInviteType.setText(inviteType);
            }
            tvInviteTopName.setText(mModel.getTitle());

            //约伴时间-固定时间＞14天：显示年月日-时分；
            //24小时＜约伴时间-固定时间＜14天，显示“距离还有x天x小时开始”
            //1小时＜约伴时间-固定时间＜24小时，显示“距离还有x小时xx分开始”
            //0＜约伴时间-固定时间＜1小时，显示“距离还有xx分开始”
//            .setText(mModel.getTitle());
            int status = mModel.getStatus();
            String lastWord = "";
            //2 4 5 状态 App不显示 2换成（ 7 8 ）
            ////审核状态(0:审核中, 1:审核未通过, 2:审核通过, 3:已停约, 4:用户删除, 5:平台删除, 6:已取消,7:进行中,8:已结束)
            switch (status) {
                case 0:
                    lastWord = "(审核中)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    break;
                case 1:
                    lastWord = "(审核未通过)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    break;
                case 3:
                    lastWord = "(已停约)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    tvEditInfo.setVisibility(View.GONE);
                    break;
                case 6:
                    lastWord = "(已取消)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    tvEditInfo.setVisibility(View.GONE);

                    break;
                case 7:
                    lastWord = "(进行中)";
                    tvJoin.setEnabled(true);
                    btnCancelJoin.setEnabled(true);

                    int enrollNumber = mModel.getEnrollNumber();

                    //如果还没有人进行报名
                    tvEditInfo.setVisibility(enrollNumber > 0 ? View.GONE : (isSelf ? View.VISIBLE : View.GONE));

                    break;
                case 8:
                    lastWord = "(已结束)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    tvEditInfo.setVisibility(View.GONE);

                    break;

                case 10:
                    lastWord = "(编辑中)";
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    tvEditInfo.setVisibility(isSelf ? View.VISIBLE : View.GONE);
                    break;
                default:
                    tvJoin.setEnabled(false);
                    btnCancelJoin.setEnabled(false);
                    lastWord = "";
                    tvEditInfo.setVisibility(View.GONE);

                    break;
            }

            timeTick.setText(YXTimeUtils.inviteDateDiffNow(time) + lastWord);

            //根据UI拼接字段
            String payWay = mModel.getPaymentTypeName();
            String people = mModel.getPartnerTypeName();
            int numLimit = mModel.getActivityNumber();
            int inviteNum = mModel.getInviteNumber();
            String desc = "\uD83D\uDC49" + numLimit + "人局（" + inviteNum + "/" + numLimit + "）/ " + YHStringUtils.value(payWay) + " / " + YHStringUtils.value(people);
            tvPeopleNum.setText(desc);

            //发起人信息
            int userGender = mModel.getUserGender();//邀约用户性别(-1:保密, 1:男, 2:女)
            switch (userGender) {
                case -1:
                    ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                    break;
                case 1:
                    ivInfoSex.setImageResource(R.mipmap.ic_male);
                    break;
                case 2:
                    ivInfoSex.setImageResource(R.mipmap.ic_female);
                    break;
                default:
                    ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                    break;
            }

            int replyCount = mModel.getMessageNum();
            mModel.setMessageNum(replyCount);
            tvTotalComment.setText("留言 " + replyCount);

            //位置信息
            String coverUrl = mModel.getCoverUrl();
            ImageLoaderUtil.loadImgCenterCrop(rivInvitePic, coverUrl);
//            tvInviteName.setText(mModel.getTitle());
            tvInviteName.setText(mModel.getAddress());
            tvInviteAddress.setText(mModel.getDestination());

            //底部操作栏
            //EnrollState	//当前用户的(0:未报名, 1:已报名)
            int enrollState = mModel.getEnrollState();
            //成员处信息展示
            if (isSelf) {
                //查询已加入和未加入的人数
                basePresenter.meetPartnerInviteNum(id);

                HashMap<String, Object> params = new HashMap<>();
                params.put("current", 1);
                params.put("size", 8);
                params.put("type", 2);//(1:待加入, 2:已加入)
                params.put("userGender", 0);//邀约用户性别(0:男女不限, 1:男, 2:女)
                params.put("inviteId", id);
                basePresenter.meetPartnerInviteMembers(params);

                bottom.setVisibility(View.GONE);
            } else {
                tvJoin.setVisibility(enrollState == 0 ? View.VISIBLE : View.GONE);
                llJoined.setVisibility(enrollState == 1 ? View.VISIBLE : View.GONE);
            }

            List<String> list = mModel.getImagesUrl();
            bannerPhotoWall.setAdapter(new BannerImageAdapter<String>(list) {
                @Override
                public void onBindView(BannerImageHolder holder, String url, int position, int size) {
                    ImageLoaderUtil.loadImg(holder.imageView, url);

                    holder.imageView.setOnClickListener(view -> {
                        //当你点击图片的时候执行以下代码：
                        // 多图片场景（你有多张图片需要浏览）
                        List<Object> urlList = new ArrayList<>();
                        for (String itemUrl : list) {
                            urlList.add(itemUrl);
                        }
                        //srcView参数表示你点击的那个ImageView，动画从它开始，结束时回到它的位置。
                        new XPopup.Builder(mContext).asImageViewer(holder.imageView, position, urlList, (popupView, position1) -> {
                            // 作用是当Pager切换了图片，需要更新源View
//                            popupView.updateSrcView((ImageView) bannerPhotoWall.getChildAt(position));
                        }, new SmartGlideImageLoader()).show();
                    });
                }
            }).addBannerLifecycleObserver(this)
                    .setLoopTime(3000)
                    .setIndicator(new CircleIndicator(mContext))
                    .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.gray2))
                    .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.white))
                    .setIndicatorWidth(DisplayUtils.dip2px(mContext, 5), DisplayUtils.dip2px(mContext, 10))
                    .setIndicatorGravity(IndicatorConfig.Direction.RIGHT)
                    .setIndicatorSpace(DisplayUtils.dip2px(mContext, 6))
                    .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, DisplayUtils.dip2px(mContext, 12), DisplayUtils.dip2px(mContext, 88)));

            //新增配置
            int showOpenSet = mModel.getShowOpenSet();//0：关闭，1：开启
            showOpenSetEnroll = mModel.getShowOpenSetEnroll();//未报名  0:关闭，1：金头像 2：头衔昵称 3：个人主页

            if (isSelf) {
                llInviteMember.setVisibility(View.VISIBLE);
            } else {
                llInviteMember.setVisibility(showOpenSet == 0 ? View.GONE : View.VISIBLE);
                llMore.setVisibility(showOpenSetEnroll == 0 ? View.GONE : View.VISIBLE);
            }
        }
    }

    private int showOpenSetEnroll;

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        InviteMessage messageContent = new InviteMessage();
        messageContent.setId(mModel.getId());
//
        String coverUrl = mModel.getCoverUrl();//封面
        int coverType = FileUtils.isVideo(coverUrl) ? 2 : 1;// 封面类型 1：图片 2：视频

        messageContent.setCoverUrl(coverUrl);
        messageContent.setCoverType(coverType);
       /* "city": "广州市",
                "destination": "东圃商业大厦",
                "destinationLongitude": "113.404314",
                "destinationLatitude": "23.120821",
                "address": "中山大道282号",*/
        messageContent.setTitle(mModel.getTitle());
        messageContent.setAddress(mModel.getDestination());

        messageContent.setAuthorHeadImg(mModel.getUserImage());
        messageContent.setAuthorNickname(mModel.getUserName());
        messageContent.setType(YXConfig.TYPE_INVITE);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            tvTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);

                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(YXConfig.TYPE_INVITE));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }


    @Override
    public void handInviteMemberList(ListResult<InviteMemberModel> data) {
        List<InviteMemberModel> list = data.getRecords();
        adapter.setItems(list);
    }

    private int leftNum, waitInviteNum;

    @Override
    public void handInviteMemberNum(InviteMemberNum data) {
        //成员处信息展示
        leftNum = mModel.getActivityNumber() - data.getInviteNumber();
        tvLeftJoinNum.setText("｜(剩下" + leftNum + "人)");
        waitInviteNum = data.getWaitInviteNumber();
        tvDealToJoiningNum.setText(waitInviteNum + "成员等待加入");

        if (data.getInviteNumber() > 0) {
            rvContent.setVisibility(View.VISIBLE);
        } else {
            rvContent.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleErrorMsg(String msg) {
        ToastUtils.showShort(msg);
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int totalSum) {
        if (current == 1) {
            mList.clear();
        }
        if (list != null && !list.isEmpty()) {
            mList.addAll(list);
        }

        //不取列表中的
        showNoDataView(totalSum);

        mCommentList = toBuildCommentList();

        mAdapter.setItems(mCommentList);
        if (null != mCommentList && mCommentList.size() > 0 && current == 1 && !isFirstLoad) {
            rvComment.postDelayed(() -> {
                rvComment.scrollToPosition(0);//第一个
            }, 200);
        }

        if (isFirstLoad) {
            //增加浏览量
            basePresenter.addViewCount(YXConfig.TYPE_INVITE, id);
        }

        isFirstLoad = false;
    }

    private void showNoDataView(int sum) {
        if (sum > 0) {
            llNodata.setVisibility(View.GONE);
            rvComment.setVisibility(View.VISIBLE);
        } else {
            llNodata.setVisibility(View.VISIBLE);
            rvComment.setVisibility(View.GONE);
        }
    }

    int total;

    @Override
    public void handleChildCommentList(ListResult<CommentModel> listResult) {
        if (listResult != null) {
            total = listResult.getTotal();
            CommentModel model = mCommentList.get(currentPosition);
            int groupPosition = model.getGroupPosition();
            mList.get(groupPosition).setChildCount(total);
            List<CommentModel> list = listResult.getRecords();
            if (list != null && !list.isEmpty()) {
                mList.get(groupPosition).setChildrens(list);
            }
            mCommentList = toBuildCommentList();
            mAdapter.setItems(mCommentList);
        }
    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");
            if (!isReply) {
                current = 1;
                basePresenter.getCommentList(id, current, 1);
            } else {
                CommentModel preModel = mCommentList.get(currentPosition);
                int groupPosition = preModel.getGroupPosition();
                int type = preModel.getType();
                if (type == 0) {
                    model.setType(1);
                } else {
                    preModel.setType(1);
                    model.setType(type);

                    //设置回复
                    model.setReplyStatus(1);
                    String preNick = YHStringUtils.pickName(preModel.getNickName(), preModel.getFriendRemark());
                    model.setReplyName(preNick);
                }
                model.setGroupPosition(groupPosition);

                UserInfo userInfo = UserManager.getInstance().getUserInfo();

                String nickName = userInfo.getNickName();
                String userPhoto = userInfo.getImg();
                String userId = userInfo.getId();
                if (TextUtils.isEmpty(model.getUserPhoto())) {
                    model.setUserPhoto(userPhoto);
                }
                if (TextUtils.isEmpty(model.getNickName())) {
                    model.setNickName(nickName);
                }
                if (TextUtils.isEmpty(model.getUserId())) {
                    model.setUserId(userId);
                }

                mCommentList.add(currentPosition + 1, model);
                mAdapter.setItems(mCommentList);

                int replyCount = mModel.getMessageNum() + 1;
                mModel.setMessageNum(replyCount);
                tvTotalComment.setText("留言 " + replyCount);

                showNoDataView(replyCount);
            }
        }
    }

    @Override
    public void updateCollectContentSuccess() {
        int collectState = mModel.getCollectState();

        if (collectState == 1) {
            collectState = 0;
        } else {
            collectState = 1;
        }

        mModel.setCollectState(collectState);
        boolean select = mModel.getCollectState() == 1;
        setFavIconColor(select);
    }

    @Override
    public void updateStarCommentSuccess() {
        CommentModel model = mCommentList.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setIsFabulous(!model.isIsFabulous());

        model.setFabulousCount(model.isIsFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition);
    }

    private void showInputPopup(String commentId, String nickName, int childType) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!TextUtils.isEmpty(commentId)) {
                isReply = true;
            } else {
                isReply = false;
            }
            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);
            popup.setOnSendCommentListener((comment, ct) -> {//发送评论
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(id);
                commentForm.setParentCommentId(commentId);

                if (ct != 0) {
                    commentForm.setReplyType(2);
                } else {
                    commentForm.setReplyType(1);
                }

                commentForm.setUserId(UserManager.getInstance().getUserId());
                basePresenter.publishComment(commentForm, YXConfig.TYPE_INVITE);
            }, childType);
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    private List<CommentModel> toBuildCommentList() {
        List<CommentModel> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            CommentModel model = mList.get(i);

            int resultType = model.getResultType();
            if (resultType == 1) {
                //这是父评论
                model.setType(0);
                model.setGroupPosition(i);
                model.setParentChildCount(0);

                list.add(model);
                List<CommentModel> childrenList = model.getChildrens();
                int childCount = model.getChildCount();
                int count = 0;

                int childSize = childrenList.size();
                for (int j = 0; j < childSize; j++) {
                    count = count + 1;
                    CommentModel childrenComment = childrenList.get(j);
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());

                    if (j == 0) {
                        if (childSize == 1) {
                            childrenComment.setChildType(3);
                        } else {
                            childrenComment.setChildType(1);
                        }
                    } else if (j == childSize - 1) {
                        childrenComment.setChildType(2);
                    } else {
                        childrenComment.setChildType(0);
                    }

                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }
            } else if (resultType == 2) {//广告
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                //点击非输入框区域，清空输入框焦点
                hideSoftKeyboard();
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v instanceof EditText) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
