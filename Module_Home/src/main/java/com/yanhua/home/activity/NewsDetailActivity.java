package com.yanhua.home.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chinalwb.are.render.AreTextView;
import com.google.android.exoplayer2.SeekParameters;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack;
import com.shuyu.gsyvideoplayer.listener.GSYVideoProgressListener;
import com.shuyu.gsyvideoplayer.listener.LockClickListener;
import com.shuyu.gsyvideoplayer.utils.Debuger;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.adapter.ContentCommentAdapter;
import com.yanhua.common.adapter.KeyAdapter;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.MapLocationDetailModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.model.TagFlowModel;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.CommentInputPopup;
import com.yanhua.common.widget.CommentPopup;
import com.yanhua.common.widget.LandLayoutVideo;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;
import com.yanhua.rong.msgprovider.NewsMessage;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;
import tv.danmaku.ijk.media.exo2.Exo2PlayerManager;

/**
 * 爆料、活动、攻略 视频下一个
 *
 * @author Administrator
 */
@Route(path = ARouterPath.NEWS_DETAIL_ACTIVITY)
public class NewsDetailActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView {
    @BindView(R2.id.ll_content)
    LinearLayout llContent;

    @Autowired
    String contentId;
    @Autowired
    int type;

    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.llTitleContainer)
    LinearLayout llTitleContainer;

    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;
    //关注
    @BindView(R2.id.btFollow)
    StatusView btFollow;

    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;


    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;

    @BindView(R2.id.civ_avatar)
    CircleImageView civAvatar;
    @BindView(R2.id.tv_name)
    TextView tvName;

    @BindView(R2.id.rv_comment)
    RecyclerView rvComment;
    @BindView(R2.id.tv_comment_second)
    TextView tvCommentSecond;
    @BindView(R2.id.iv_collect)
    AliIconFontTextView ivCollect;
    @BindView(R2.id.tv_collect)
    TextView tvCollect;
    @BindView(R2.id.ll_collect)
    LinearLayout llCollect;
    @BindView(R2.id.iv_comment)
    AliIconFontTextView ivComment;
    @BindView(R2.id.tv_comment)
    TextView tvComment;
    @BindView(R2.id.ll_comment)
    LinearLayout llComment;
    @BindView(R2.id.iv_love)
    AliIconFontTextView ivLove;
    @BindView(R2.id.tv_love)
    TextView tvLove;
    @BindView(R2.id.ll_love)
    LinearLayout llLove;

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.ll_nodata)
    LinearLayout llNodata;

    @BindView(R2.id.tv_total_comment)
    TextView tvTotalComment;


    @BindView(R2.id.upload)
    FrameLayout uploadContainer;//根据封面比例  351：210 5：3
    @BindView(R2.id.ivUpload)
    ImageView ivUpload;//根据封面比例  351：210 5：3
    @BindView(R2.id.videoPlay)
    LandLayoutVideo detailPlayer;//

    @BindView(R2.id.tvContentTitle)
    TextView tvContentTitle;//

    @BindView(R2.id.areTextView)
    AreTextView areTextView;//

    @BindView(R2.id.llStrategy)
    LinearLayout llStrategy;
    @BindView(R2.id.tvTypeName)
    TextView tvTypeName;
    @BindView(R2.id.rvKeys)
    RecyclerView rvKeys;

    @BindView(R2.id.tvTimeDesc)
    TextView tvTimeDesc;

    @BindView(R2.id.rlAddress)
    RelativeLayout rlAddress;
    @BindView(R2.id.tvActAddress)
    TextView tvActAddress;
    @BindView(R2.id.tvActTime)
    TextView tvActTime;//3.15 17:30至4.15 12:00

    @BindView(R2.id.civUser)
    CircleImageView civUser;
    @BindView(R2.id.tvUserName)
    TextView tvUserName;
    @BindView(R2.id.tvUserSign)
    TextView tvUserSign;
    @BindView(R2.id.btnFollow)
    StatusView btnFollow;


    private List<String> listKeyDatas;
    KeyAdapter keyAdapter;

    @BindView(R2.id.tvIntrduce)
    TextView tvIntrduce;

    @BindView(R2.id.tflMomentItem)
    TagFlowLayout tflMomentItem;

    private int current = 1;
    private ContentCommentAdapter mAdapter;
    private int currentPosition;

    private NewsDetailModel mModel;

    private boolean isReply = false;

    private int uploadContainerHeight;

    private List<CommentModel> mList = new ArrayList<>();
    //组装后的评论列表
    private List<CommentModel> mCommentList = new ArrayList<>();

    private boolean isPlay;
    private boolean isPause;
    private OrientationUtils orientationUtils;

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        llContent.setVisibility(View.GONE);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new ContentCommentAdapter(mContext, "");

        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

//        refreshLayout.setOnRefreshListener(rl -> {
//            refreshLayout.finishRefresh(1000);
//        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mList.size() == 0 || mList.size() % 10 != 0) {
                refreshLayout.finishLoadMore(1000, true, true);
                return;
            }
            current = current + 1;
            refreshLayout.finishLoadMore(1000);
            basePresenter.getCommentList(contentId, current, 1);
        });

        rvComment.setLayoutManager(manager);

        RecyclerView.RecycledViewPool pool = new RecyclerView.RecycledViewPool();
        pool.setMaxRecycledViews(0, 10);
        rvComment.setRecycledViewPool(pool);

        //关闭recyclerview动画
        rvComment.getItemAnimator().setAddDuration(0);
        rvComment.getItemAnimator().setChangeDuration(0);
        rvComment.getItemAnimator().setMoveDuration(0);
        rvComment.getItemAnimator().setRemoveDuration(0);
        ((SimpleItemAnimator) rvComment.getItemAnimator()).setSupportsChangeAnimations(false);

        rvComment.setAdapter(mAdapter);

        rvComment.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果
        RecycleViewUtils.clearRecycleAnimation(rvComment);


        mAdapter.setOnADClickListener(clickItem -> {
            HashMap<String, Object> params = new HashMap<>();
            params.put("userId", UserManager.getInstance().getUserId());
            params.put("id", clickItem.getId());

            basePresenter.addMarketingClick(params);
            PageJumpUtil.adJumpContent(clickItem, (Activity) mContext, -1);
        });
        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            if (cellType == 0) {
                String uId = model.getUserId();
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", uId)
                        .navigation();
            } else if (cellType == 1) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    String commentId = model.getId();
//                    basePresenter.updateStarComment(commentId, type);
                    basePresenter.updateStarComment(commentId);
                });
            } else if (cellType == 2) {
                int groupPosition = model.getGroupPosition();
                String parentCommentId = mList.get(groupPosition).getId();
                List<CommentModel> childCommentList = mList.get(groupPosition).getChildrens();
                int size = childCommentList.size() + 5;
                basePresenter.getChildCommentList(parentCommentId, 1, size);
            }
        });

        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            boolean myself = UserManager.getInstance().isMyself(model.getUserId());

            boolean isMyContent = UserManager.getInstance().isMyself(mModel.getUserId());
            CommentBoardPopup popup = new CommentBoardPopup(mActivity, isMyContent, true, true, !myself, myself);
            popup.setOnActionItemListener(actionType -> {
                switch (actionType) {
                    case CommentBoardPopup.ACTION_TOP:
                        toTop(pos);
                        break;
                    case CommentBoardPopup.ACTION_REPLY:
                        //回复
                        replyComment(model);
                        break;
                    case CommentBoardPopup.ACTION_COPY:
                        String content = model.getCommentDesc();

                        YHStringUtils.copyContent(mActivity, content);
                        break;
                    case CommentBoardPopup.ACTION_REPORT:
                        toReport(pos);
                        break;
                    case CommentBoardPopup.ACTION_DELETE:
                        toDelete(pos);
                        break;
                }

            });

            new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
        });

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            currentPosition = pos;
            CommentModel model = mCommentList.get(pos);
            //回复
            replyComment(model);
        });

        setTopView();
    }


    private void setVideoPlayer(String title, String videoUrl, Bitmap res) {
        resolveNormalVideoUI();

        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, detailPlayer);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);

        //增加封面
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(res);

        Map<String, String> header = new HashMap<>();
        header.put("allowCrossProtocolRedirects", "true");
        header.put("User-Agent", "YH");
        GSYVideoOptionBuilder gsyVideoOption = new GSYVideoOptionBuilder();
        gsyVideoOption
                .setThumbImageView(imageView)
                .setIsTouchWiget(true)
                .setRotateViewAuto(false)
                //仅仅横屏旋转，不变直
                //.setOnlyRotateLand(true)
                .setRotateWithSystem(true)
                .setLockLand(true)
                .setAutoFullWithSize(true)
                .setShowFullAnimation(false)
                .setNeedLockFull(true)
                .setUrl(videoUrl)
                .setMapHeadData(header)
                .setCacheWithPlay(false)
                .setVideoTitle(title)
                .setVideoAllCallBack(new GSYSampleCallBack() {
                    @Override
                    public void onPrepared(String url, Object... objects) {
                        Debuger.printfError("***** onPrepared **** " + objects[0]);
                        Debuger.printfError("***** onPrepared **** " + objects[1]);
                        super.onPrepared(url, objects);
                        //开始播放了才能旋转和全屏
                        orientationUtils.setEnable(detailPlayer.isRotateWithSystem());
                        isPlay = true;


                        //设置 seek 的临近帧。
                        if (detailPlayer.getGSYVideoManager().getPlayer() instanceof Exo2PlayerManager) {
                            ((Exo2PlayerManager) detailPlayer.getGSYVideoManager().getPlayer()).setSeekParameter(SeekParameters.NEXT_SYNC);
                            Debuger.printfError("***** setSeekParameter **** ");
                        }
                    }

                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        Debuger.printfError("***** onEnterFullscreen **** " + objects[0]);//title
                        Debuger.printfError("***** onEnterFullscreen **** " + objects[1]);//当前全屏player
                    }

                    @Override
                    public void onAutoComplete(String url, Object... objects) {
                        super.onAutoComplete(url, objects);
                    }

                    @Override
                    public void onClickStartError(String url, Object... objects) {
                        super.onClickStartError(url, objects);
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        Debuger.printfError("***** onQuitFullscreen **** " + objects[0]);//title
                        Debuger.printfError("***** onQuitFullscreen **** " + objects[1]);//当前非全屏player
                        if (orientationUtils != null) {
                            orientationUtils.backToProtVideo();
                        }
                    }
                })
                .setLockClickListener(new LockClickListener() {
                    @Override
                    public void onClick(View view, boolean lock) {
                        if (orientationUtils != null) {
                            //配合下方的onConfigurationChanged
                            orientationUtils.setEnable(!lock);
                        }
                    }
                })
                .setGSYVideoProgressListener(new GSYVideoProgressListener() {
                    @Override
                    public void onProgress(int progress, int secProgress, int currentPosition, int duration) {
                        Debuger.printfLog(" progress " + progress + " secProgress " + secProgress + " currentPosition " + currentPosition + " duration " + duration);
                    }
                })
                .build(detailPlayer);

        detailPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //直接横屏
                orientationUtils.resolveByClick();

                //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
                detailPlayer.startWindowFullscreen(NewsDetailActivity.this, true, true);
            }
        });

    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        int width = DisplayUtils.getScreenWidth(mContext);
        int height = width;

        ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = height;

        uploadContainerHeight = height;
        uploadContainer.setLayoutParams(imageLayoutParams);

        int paddingDistance = UltimateBarX.getStatusBarHeight() + DisplayUtils.dip2px(mContext, 50);

        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scroll2top = uploadContainerHeight + verticalOffset;

            if (scroll2top > paddingDistance) {
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_black);

                rlTitle.setVisibility(View.GONE);
                btFollow.setVisibility(View.GONE);
            } else {
                toolbar.setBackgroundResource(R.color.white);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);

                rlTitle.setVisibility(View.VISIBLE);

                int userType = mModel.getUserType();//用户类型 1 app用户 2 后端用户
                btFollow.setVisibility(userType == 1 ? View.VISIBLE : View.GONE);
            }
        });

        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }

    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);

        current = 1;
        basePresenter.getNewsDetail(contentId);
        basePresenter.getCommentList(contentId, current, 1);

    }

    private void setContentDetail() {
        String pathUrl = mModel.getCoverUrl();

        String title = YHStringUtils.value(mModel.getContentTitle());

        tvContentTitle.setText(title);

        String content = YHStringUtils.value(mModel.getContent());
        areTextView.fromHtml(YHStringUtils.value(content));

        boolean isStrategy = type == YXConfig.TYPE_STRATEGY;
        if (isStrategy) {
            llStrategy.setVisibility(View.VISIBLE);

            String typeName = mModel.getReportName();
            tvTypeName.setText(typeName);

            String keys = mModel.getKeyword();
            if (!TextUtils.isEmpty(keys)) {
                listKeyDatas = new ArrayList<>();

                LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
                keyAdapter = new KeyAdapter(mContext, null);
                rvKeys.setLayoutManager(manager);
                rvKeys.setAdapter(keyAdapter);

                String[] keyArr = keys.split(",");

                for (String key : keyArr) {
                    listKeyDatas.add(key);
                }
                keyAdapter.setItems(listKeyDatas);
            }
        } else {
            llStrategy.setVisibility(View.INVISIBLE);
            String categyTypeId = mModel.getContentCategoryId();
            if (YXConfig.ALL666.equals(categyTypeId)) {
                rlAddress.setVisibility(View.VISIBLE);

                String startTime = mModel.getActivityStartTime();
                String endTime = mModel.getActivityEndTime();
                String actAddress = mModel.getIssueAddress();

                tvActTime.setText(DiscoTimeUtils.getBreakNewsActTime(startTime, endTime));////3.15 17:30至4.15 12:00
                tvActAddress.setText(actAddress);
            }
        }

        String publishTime = mModel.getPublishTime();
        int viewCount = mModel.getPageViewCount();
        //"2021-09-24 15:20\u3000\u3000阅读 46万+"
        String timeDesc = YXTimeUtils.getFriendlyTimeAtContent(publishTime) + "\u3000\u3000阅读 " + YHStringUtils.quantityFormat(viewCount);
        tvTimeDesc.setText(timeDesc);

        String introduce = mModel.getIntroduction();
        if (TextUtils.isEmpty(introduce)) {
            tvIntrduce.setVisibility(View.GONE);
        } else {
            tvIntrduce.setVisibility(View.VISIBLE);
            tvIntrduce.setText("导语：" + introduce);
        }
        showAddressAndCircle();

        String headPicUrl = mModel.getImg();
        //展示发布人信息
        ImageLoaderUtil.loadImgCenterCrop(civUser, headPicUrl, R.drawable.place_holder);
        ImageLoaderUtil.loadImgCenterCrop(civAvatar, headPicUrl, R.drawable.place_holder);

        String publishName = mModel.getNickName();
        tvUserName.setText(publishName);
        tvName.setText(publishName);

        String sign = mModel.getPersonalSignature();
        if (TextUtils.isEmpty(sign)) {
            tvUserSign.setVisibility(View.GONE);
        } else {
            tvUserSign.setVisibility(View.VISIBLE);

            tvUserSign.setText(sign);
        }

        setTitleBack(pathUrl, title);

        showFollow();

        int fabulousCount = mModel.getFabulousCount();
        int collectCount = mModel.getCollectCount();
        int replyCount = mModel.getCommentCount();

        tvCollect.setText(collectCount > 0 ? YHStringUtils.quantityFormat(collectCount) : "收藏");
        tvLove.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");

        tvTotalComment.setText("评论 " + replyCount);
        tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

        showNoDataView(replyCount);

        ivCollect.setSelected(mModel.isCollect());
        ivLove.setSelected(mModel.isFabulous());

        ivCollect.setText(mModel.isCollect() ? "\ue760" : "\ue742");
        ivLove.setText(mModel.isFabulous() ? "\ue772" : "\ue71b");
    }


    /**
     * 展示地址和圈子
     */
    private void showAddressAndCircle() {
        String issueCity = mModel.getIssueCity();
        String issueAddress = mModel.getIssueAddress();
        String issueLatitude = mModel.getIssueLatitude();
        String issueLongitude = mModel.getIssueLongitude();

        List<TagFlowModel> tagList = new ArrayList<>();

        if (TextUtils.isEmpty(issueLatitude) && TextUtils.isEmpty(issueLongitude)) {
        } else {
            String address = "";
            if (!TextUtils.isEmpty(issueCity)) {
                address = TextUtils.isEmpty(issueAddress) ? issueCity : issueCity + "." + issueAddress;
            } else if (!TextUtils.isEmpty(issueAddress)) {
                address = issueAddress;
            }

            if (!TextUtils.isEmpty(address)) {
                TagFlowModel addressTag = new TagFlowModel(TagFlowModel.ADDRESS, "0", address, R.mipmap.ic_address);
                addressTag.setIssueLatitude(issueLatitude);
                addressTag.setIssueLongitude(issueLongitude);

                tagList.add(addressTag);
            }
        }

        //获取话题
//        List<TopicModel> topicList = mModel.getTopicList();
//        if (null != topicList && topicList.size() > 0) {
//            for (TopicModel item : topicList) {
//                TagFlowModel tag = new TagFlowModel(TagFlowModel.CIRCLE, item.getId(), item.getTitle(), R.mipmap.ic_topic);
//                tagList.add(tag);
//            }
//        }

        if (tagList.size() > 0) {
            tflMomentItem.setVisibility(View.VISIBLE);
            //控制显示5个
            tflMomentItem.setAdapter(new TagAdapter<TagFlowModel>(tagList) {
                @Override
                public View getView(FlowLayout parent, int position, TagFlowModel tagItem) {
                    //加载tag布局
                    View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow, parent, false);
                    //获取标签
                    TextView tvTagName = view.findViewById(R.id.tvTagName);
                    ImageView ivTagIcon = view.findViewById(R.id.ivTagIcon);

                    ivTagIcon.setImageResource(tagItem.getDrawable());
                    tvTagName.setText(tagItem.getName());
                    return view;
                }
            });
            tflMomentItem.setOnTagClickListener((view, pos, parent) -> {
                TagFlowModel tagFlowModel = tagList.get(pos);
                if (tagFlowModel.getType() == TagFlowModel.ADDRESS) {

                } else {
                    ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                            .withString("id", tagFlowModel.getId()).navigation();
                }

                return true;
            });
        } else {
            tflMomentItem.setVisibility(View.GONE);
        }

    }


    @OnClick({R2.id.iconMore, R2.id.tv_comment_second, R2.id.rlAddress,
            R2.id.ll_collect, R2.id.ll_love, R2.id.civ_avatar, R2.id.rlTitle,
            R2.id.tv_name, R2.id.btFollow, R2.id.btnFollow, R2.id.ll_comment})
    public void onViewClicked(View view) {
        if (mModel == null) {
            return;
        }
        hideSoftKeyboard();
        if (view.getId() == R.id.iconMore) {
            //展示更多
            MoreActionBoard.shareMyDynamicNews(mActivity, mModel, type, (position, tp) -> {
                switch (position) {
                    case ShareBoardPopup.BOARD_SET_REPORT:
                        toReport(-1);
                        break;
                    case ShareBoardPopup.BOARD_SET_DELETE:
                        deleteContent();
                        break;

                    case ShareBoardPopup.BOARD_SHARE_APP:
                        //.isLightStatusBar(true)不会改变状态栏的颜色
                        if (null == mModel) return;
                        SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, mModel, type, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                        new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                .autoFocusEditText(false)
                                .moveUpToKeyboard(false).asCustom(sharePopup).show();
                        break;
                }
            });
        } else if (view.getId() == R.id.rlAddress) {
            checkLoacationStatus();
        } else if (view.getId() == R.id.tv_comment_second) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                ////-----评论
                showInputPopup("", "", 0);
            });
        } else if (view.getId() == R.id.ll_comment) {
            //保持跟视频评论点击一致
            String contentId = mModel.getId();
            String userId = mModel.getUserId();
            toComment(contentId, userId);
        } else if (view.getId() == R.id.ll_collect) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toCollect();
            });
        } else if (view.getId() == R.id.ll_love) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                // 点赞前先查一下privacyType，再判断能否点赞
                toStar();
            });
        } else if (view.getId() == R.id.btFollow || view.getId() == R.id.btnFollow) {

            PageJumpUtil.firstIsLoginThenJump(() -> {
                toFollow();
            });
        } else if (view.getId() == R.id.civ_avatar || view.getId() == R.id.tv_name) {
            String userId = mModel.getUserId();
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", userId)
                    .navigation();
        } else if (view.getId() == R.id.rlTitle) {
            String userId = mModel.getUserId();
            ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", userId)
                    .navigation();
        }
    }

    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        if (null == mModel) {
            return;
        }

        MapLocationDetailModel locationDetailModel = new MapLocationDetailModel();
//
        locationDetailModel.setAddress(mModel.getIssueAddress());
//        locationDetailModel.setName(mModel.getContentTitle());
        locationDetailModel.setLatitude(mModel.getIssueLatitude());
        locationDetailModel.setLongitude(mModel.getIssueLongitude());

        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {//开了定位服务
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // 没有权限
            } else {
                // 有权限了
            }
            ARouter.getInstance()
                    .build(ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
                    .withSerializable("addressDetail", locationDetailModel)
//                    .withString("distanceKM", distanceKM)
                    .navigation();
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });

            commonDialog.setOnCancelListener(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
                        .withSerializable("addressDetail", locationDetailModel)
//                        .withString("distanceKM", distanceKM)
                        .navigation();
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }

    private int SHOW_IMAGE = 1;

    private void setTitleBack(String path, String title) {
        if (TextUtils.isEmpty(path)) {
            return;
        }

        boolean isVideo = FileUtils.isVideo(path);
        int type = isVideo ? 0 : 1;

        if (type == SHOW_IMAGE) {
            Glide.with(mContext)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picHeight = resource.getHeight();
                            int picWidth = resource.getWidth();

                            float rate = DisplayUtils.getImageRate(picWidth, picHeight);//4:3 1:1 3:4

                            int width = DisplayUtils.getScreenWidth(mContext);
                            int height = (int) (width / rate);

                            ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
                            imageLayoutParams.width = width;
                            imageLayoutParams.height = height;
                            uploadContainerHeight = height;

                            uploadContainer.setLayoutParams(imageLayoutParams);

                            ivUpload.setImageBitmap(resource);

                            llTitleContainer.setBackgroundResource(R.drawable.bg_publish);
                        }
                    });
            detailPlayer.setVisibility(View.INVISIBLE);
        } else {
            RequestOptions options = new RequestOptions();

            options.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .frame(1000000)
                    .centerCrop();

            Glide.with(mContext)
                    .setDefaultRequestOptions(options)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picHeight = resource.getHeight();
                            int picWidth = resource.getWidth();

                            float rate = DisplayUtils.getImageRate(picWidth, picHeight);//4:3 1:1 3:4

                            int width = DisplayUtils.getScreenWidth(mContext);
                            int height = (int) (width / rate);


                            //视频的高度计算方式，视频比例为1:1
                            ViewGroup.LayoutParams videoLayoutParams = detailPlayer.getLayoutParams();
                            videoLayoutParams.width = width;
                            videoLayoutParams.height = width;
                            detailPlayer.setLayoutParams(videoLayoutParams);

                            //總高度為視頻高度+標題容器高度
                            uploadContainerHeight = videoLayoutParams.height + llTitleContainer.getHeight();
                            ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
                            imageLayoutParams.width = width;
                            imageLayoutParams.height = uploadContainerHeight;
                            uploadContainer.setLayoutParams(imageLayoutParams);
                            ivUpload.setImageBitmap(resource);

                            //設置視頻
                            setVideoPlayer(title, path, resource);

//                            llTitleContainer.setBackgroundResource(R.drawable.bg_publish_video);
                            llTitleContainer.setBackgroundResource(R.drawable.bg_black);
                        }
                    });
            detailPlayer.setVisibility(View.VISIBLE);
        }
    }

    private void showInputPopup(String commentId, String nickName, int childType) {

        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!TextUtils.isEmpty(commentId)) {
                isReply = true;
            } else {
                isReply = false;
            }
            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);

            popup.setOnSendCommentListener((comment, ct) -> {//发送评论
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(contentId);
                commentForm.setParentCommentId(commentId);
                commentForm.setUserId(UserManager.getInstance().getUserId());

                if (ct != 0) {
                    commentForm.setReplyType(2);
                } else {
                    commentForm.setReplyType(1);
                }

                commentForm.setReplyType(1);

                basePresenter.publishComment(commentForm, type);
            }, childType);
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    /**
     * 显示评论弹窗
     *
     * @param contentId
     * @param userId
     */
    private void toComment(String contentId, String userId) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            CommentPopup popup = new CommentPopup(mActivity, contentId, userId, type, mModel.getCommentCount());
            new XPopup.Builder(mContext).moveUpToKeyboard(false).enableDrag(false).atView(llComment).asCustom(popup).show();
        });
    }

    /**
     * 关注状态
     */
    private void showFollow() {
        String pUserId = mModel.getUserId();

        if (UserManager.getInstance().isMyself(pUserId)) {
            btFollow.setVisibility(View.GONE);
            btnFollow.setVisibility(View.GONE);
        } else {
            int userType = mModel.getUserType();//用户类型 1 app用户 2 后端用户

            btFollow.setVisibility(userType == 1 ? View.VISIBLE : View.GONE);
            btnFollow.setVisibility(userType == 1 ? View.VISIBLE : View.GONE);

            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
            int followStatus = mModel.getFollowStatus();

            boolean isFollow = DiscoValueFormat.isFollow(followStatus);
            btFollow.setStatus(isFollow);
            btnFollow.setStatus(isFollow);
        }
    }

    private void toFollow() {
        int followStatus = mModel.getFollowStatus();
        boolean isFollow = DiscoValueFormat.isFollow(followStatus);

        String userId = mModel.getUserId();
        String uId = UserManager.getInstance().getUserId();
        if (uId.equals(userId)) {
            ToastUtils.showShort("不能关注自己");
            return;
        }
        if (isFollow) {
            AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
            new XPopup.Builder(mContext).asCustom(alertPopup).show();
            alertPopup.setOnConfirmListener(() -> {
                alertPopup.dismiss();
                basePresenter.cancelFollowUser(userId);
            });
            alertPopup.setOnCancelListener(alertPopup::dismiss);
        } else {
            basePresenter.followUser(userId);
        }
    }

    /**
     * 收藏或者取消收藏
     */
    private void toCollect() {
        basePresenter.updateCollectContent(contentId, type);
    }

    /**
     * 点赞或者取消点赞
     */
    private void toStar() {
        basePresenter.updateStarContent(contentId, type);
    }

    /**
     * 举报内容或者评论
     *
     * @param pos -1 内容
     */
    private void toReport(int pos) {

        PageJumpUtil.firstIsLoginThenJump(() -> {
            int reportType = 0;
            String contentId = "";
            String currentUserId = null;

            //此处包含爆料和攻略+评论
            if (pos >= 0) {//评论
                reportType = YXConfig.reportType.comment;
                CommentModel model = mCommentList.get(pos);
                contentId = model.getId();
                currentUserId = model.getUserId();
            } else {
                if (type == YXConfig.TYPE_BREAK_NEWS) {
                    reportType = YXConfig.reportType.breakNews;
                } else if (type == YXConfig.TYPE_STRATEGY) {
                    reportType = YXConfig.reportType.strategy;
                }
                contentId = mModel.getId();
                currentUserId = mModel.getUserId();
            }

            if (TextUtils.isEmpty(currentUserId) || currentUserId.equals(UserManager.getInstance().getUserId())) {
                ToastUtils.showShort(reportType == 1 ? "不能举报自己的内容" : "不能举报自己的评论");
                return;
            }

            ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                    .withString("businessID", contentId)
                    .withInt("reportType", reportType)
                    .navigation();
        });
    }

    private void deleteContent() {
        basePresenter.deleteContent(mModel.getId(), type);
    }

    private void showNoDataView(int sum) {
        if (sum > 0) {
            llNodata.setVisibility(View.GONE);
            rvComment.setVisibility(View.VISIBLE);
        } else {
            llNodata.setVisibility(View.VISIBLE);
            rvComment.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleDeleteContentSuccess() {
        onBackPressed();//删除自己的内容，返回上一级页面
    }

    /**
     * 置顶评论
     *
     * @param pos
     */
    private void toTop(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();


        PageJumpUtil.firstIsLoginThenJump(() -> {
            basePresenter.dealCommentItem2Top(contentId, type, pos);
        });
    }

    @Override
    public void handleDealItemTopSuccess(int pos) {
        if (mAdapter != null) {
            List list = mAdapter.getmDataList();
            Collections.swap(list, pos, 0);
            mAdapter.notifyItemMoved(pos, 0);
        }
    }

    /**
     * 删除评论
     *
     * @param pos
     */
    private void toDelete(int pos) {
        CommentModel model = mCommentList.get(pos);
        String currentUserId = model.getUserId();
        String contentId = model.getId();


        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!UserManager.getInstance().isMyself(currentUserId)) {
                ToastUtils.showShort("只能删除自己的评论");
                return;
            }

            basePresenter.deleteCommentContent(contentId, pos);
        });
    }

    @Override
    public void handleDeleteCommentSuccess(int pos) {
        if (mAdapter != null) {
            mAdapter.notifyItemRemoved(pos);
            ToastUtils.showShort("评论删除成功");

            int replyCount = mModel.getCommentCount() - 1;
            mModel.setCommentCount(replyCount);

            tvTotalComment.setText("评论 " + replyCount);
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");
        }
    }

    /**
     * 回复动作
     *
     * @param model
     */
    private void replyComment(CommentModel model) {
        String commentId = model.getId();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        int childType = model.getChildType();
        showInputPopup(commentId, nickName, childType);
    }

    private List<CommentModel> toBuildCommentList() {
        List<CommentModel> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            CommentModel model = mList.get(i);

            int resultType = model.getResultType();
            if (resultType == 1) {
                //这是父评论
                model.setType(0);
                model.setGroupPosition(i);
                model.setParentChildCount(0);

                list.add(model);
                List<CommentModel> childrenList = model.getChildrens();
                int childCount = model.getChildCount();
                int count = 0;

                int childSize = childrenList.size();
                for (int j = 0; j < childSize; j++) {
                    count = count + 1;
                    CommentModel childrenComment = childrenList.get(j);
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());

                    if (j == 0) {
                        if (childSize == 1) {
                            childrenComment.setChildType(3);
                        } else {
                            childrenComment.setChildType(1);
                        }
                    } else if (j == childSize - 1) {
                        childrenComment.setChildType(2);
                    } else {
                        childrenComment.setChildType(0);
                    }

                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }
            } else if (resultType == 2) {//广告
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public void updateStarCommentSuccess() {
        CommentModel model = mCommentList.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setIsFabulous(!model.isIsFabulous());

        model.setFabulousCount(model.isIsFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int totalSum) {

        if (current == 1) {
            mList.clear();
        }
        if (list != null && !list.isEmpty()) {
            mList.addAll(list);
        }
//评论数不要取评论列表中的总数
//        tvTotalComment.setText("评论(" + totalSum + ")");
//        tvComment.setText(totalSum > 0 ? YHStringUtils.quantityFormat(totalSum) : "评论");
        showNoDataView(totalSum);

        mCommentList = toBuildCommentList();

        mAdapter.setItems(mCommentList);

//        if (null != mCommentList && mCommentList.size() > 0 && current == 1 && !isFirstLoad) {
//            rvComment.postDelayed(() -> {
//                int totalNumTop = tvTotalComment.getBottom();//获取评论数的顶部//+ ScreenUtil.dip2px(mContext, 56)
//
//                totalNumTop = totalNumTop + rvComment.getChildAt(0).getBottom();
//
//                rvComment.scrollToPosition(0);//第一个
//            }, 200);
//        }
    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");

            int replyCount = mModel.getCommentCount() + 1;
            mModel.setCommentCount(replyCount);

            tvTotalComment.setText("评论(" + replyCount + ")");
            tvComment.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

            showNoDataView(replyCount);

            if (!isReply) {
                current = 1;
                basePresenter.getCommentList(contentId, current, 1);
            } else {
                CommentModel preModel = mCommentList.get(currentPosition);
                int groupPosition = preModel.getGroupPosition();
                int type = preModel.getType();
                if (type == 0) {
                    model.setType(1);
                } else {
                    preModel.setType(1);
                    model.setType(type);

                    //设置回复
                    model.setReplyStatus(1);
                    String preNick = YHStringUtils.pickName(preModel.getNickName(), preModel.getFriendRemark());
                    model.setReplyName(preNick);
                }
                model.setGroupPosition(groupPosition);

                UserInfo userInfo = UserManager.getInstance().getUserInfo();

                String nickName = userInfo.getNickName();
                String userPhoto = userInfo.getImg();
                String userId = userInfo.getId();
                if (TextUtils.isEmpty(model.getUserPhoto())) {
                    model.setUserPhoto(userPhoto);
                }
                if (TextUtils.isEmpty(model.getNickName())) {
                    model.setNickName(nickName);
                }
                if (TextUtils.isEmpty(model.getUserId())) {
                    model.setUserId(userId);
                }

                mCommentList.add(currentPosition + 1, model);
                mAdapter.setItems(mCommentList);
            }
        }
    }


    @Override
    public void handleActivityListData(ListResult<ActivityModel> data) {
        showFollow();
    }

    @Override
    public void updateFollowUserSuccess() {
        int followStatus = mModel.getFollowStatus();
        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
        if (isFollow) {
            mModel.setFollowStatus(DiscoValueFormat.unFollowAction(followStatus));
        } else {
            mModel.setFollowStatus(DiscoValueFormat.followAction(followStatus));
        }

        showFollow();
    }


    @Override
    public void updateStarContentSuccess() {
        int fabulousCount = mModel.getFabulousCount();
        boolean isFabulous = mModel.isFabulous();

        fabulousCount = !isFabulous ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0);

        mModel.setFabulousCount(fabulousCount);
        mModel.setFabulous(!isFabulous);

        ivLove.setSelected(!isFabulous);

        ivLove.setText(!isFabulous ? "\ue772" : "\ue71b");

        tvLove.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");
    }

    @Override
    public void handleNewsDetailContent(NewsDetailModel data) {
        if (null != data) {
            llContent.setVisibility(View.VISIBLE);
            mModel = data;

            //增加阅读量
            basePresenter.addViewCount(type, contentId);


            mAdapter.setPublishUserId(mModel.getUserId());

            setContentDetail();
        }
    }


    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        NewsMessage messageContent = new NewsMessage();
        messageContent.setId(mModel.getId());
        messageContent.setCoverUrl(mModel.getCoverUrl());//这个到时跟后台确认，目前无论是视频还是图片都用这个
        messageContent.setTitle(mModel.getContentTitle());

        messageContent.setAuthorHeadImg(mModel.getImg());
        messageContent.setAuthorNickname(mModel.getNickName());
        messageContent.setType(type);

        String content = "";
        String introduce = mModel.getIntroduction();
        if (TextUtils.isEmpty(introduce)) {
            content = mModel.getContent();
            content = YHStringUtils.getHtmlContent(content);
        } else {
            content = introduce;
        }

        messageContent.setContent(content);

        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            llContent.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);

                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(type));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }

    @Override
    public void updateCollectContentSuccess() {
        boolean collectState = mModel.isCollect();

        mModel.setCollect(!collectState);
        ivCollect.setSelected(!collectState);

        ivCollect.setText(!collectState ? "\ue760" : "\ue742");

        int collectCount = mModel.getCollectCount();
        collectCount = mModel.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0);
        mModel.setCollectCount(collectCount);

        tvCollect.setText(collectCount > 0 ? YHStringUtils.quantityFormat(collectCount) : "收藏");
    }

    @Override
    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }

        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        getCurPlay().onVideoPause();
        super.onPause();
        isPause = true;
    }

    @Override
    protected void onResume() {
        getCurPlay().onVideoResume(false);
        super.onResume();
        isPause = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isPlay) {
            getCurPlay().release();
        }
        //GSYPreViewManager.instance().releaseMediaPlayer();
        if (orientationUtils != null)
            orientationUtils.releaseListener();
    }

    /**
     * orientationUtils 和  detailPlayer.onConfigurationChanged 方法是用于触发屏幕旋转的
     */
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if (isPlay && !isPause) {
            detailPlayer.onConfigurationChanged(this, newConfig, orientationUtils, true, true);
        }
    }


    private void resolveNormalVideoUI() {
        //增加title
        detailPlayer.getTitleTextView().setVisibility(View.GONE);
        detailPlayer.getBackButton().setVisibility(View.GONE);
    }

    private GSYVideoPlayer getCurPlay() {
        if (detailPlayer.getFullWindowPlayer() != null) {
            return detailPlayer.getFullWindowPlayer();
        }
        return detailPlayer;
    }
}
