package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.view.RecyclerTabLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.LooperPageAdapter;
import com.yanhua.home.adapter.TopLineAdapter;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 活动日历以及历史活动日历
 */
@Route(path = ARouterPath.ACT_CALENDAR_ACTIVITY)
public class ActCalendarActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView, ViewPager.OnPageChangeListener {
    @Autowired
    boolean showHistory;

    @BindView(R2.id.llToHistory)
    LinearLayout llToHistory;
    @BindView(R2.id.rcTabLayout)
    RecyclerTabLayout rcTabLayout;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;
    @BindView(R2.id.leftView)
    View leftView;

    private LooperPageAdapter mAdapter;
    private List<SelectConditionModel> mChannelList;
    private TopLineAdapter topLineAdapter;

    @Override
    public int bindLayout() {
        return R.layout.activity_act_calendar;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        llToHistory.setVisibility(showHistory ? View.GONE : View.VISIBLE);

        mChannelList = new ArrayList<>();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //获取城市
        basePresenter.recentActiveCityList(YXConfig.ALL666, YXConfig.TYPE_BREAK_NEWS,showHistory ? 0 : 1);
    }

    /**
     * 根据城市分类创建
     */
    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new LooperPageAdapter(getSupportFragmentManager(), mChannelList, showHistory);
//            rcTabLayout
            leftView.setVisibility(mChannelList.size() > YXConfig.ACT_TOP_MIN_NUM ? View.GONE : View.VISIBLE);

            vpChannel.setAdapter(mAdapter);
            vpChannel.addOnPageChangeListener(this);
            vpChannel.setCurrentItem(mAdapter.getCenterPosition(0,0,mChannelList.size()));

//            rcTabLayout.setUpWithViewPager(vpChannel);

            topLineAdapter = new TopLineAdapter(vpChannel);
            rcTabLayout.setUpWithAdapter(topLineAdapter);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        //got to center
        boolean nearLeftEdge = (position <= mChannelList.size());
        boolean nearRightEdge = (position >= mAdapter.getCount() - mChannelList.size());
        if (nearLeftEdge || nearRightEdge) {
            vpChannel.setCurrentItem(mAdapter.getCenterPosition(0,position,mChannelList.size()), false);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @OnClick({R2.id.llToHistory})
    public void onClickView(View view) {
        int id = view.getId();

        if (id == R.id.llToHistory) {
            ARouter.getInstance().build(ARouterPath.ACT_CALENDAR_ACTIVITY)
                    .withBoolean("showHistory", true)
                    .navigation();
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handleCityList(List<String> cityList) {
        if (null != cityList) {
            mChannelList.clear();

            SelectConditionModel suggest = new SelectConditionModel();
            suggest.setFieldName("城市");
            suggest.setFieldValue("全部");
            suggest.setValue(0);
            suggest.setSelected(true);
            mChannelList.add(suggest);


            for (int i = 0; i < cityList.size(); i++) {
                String cityName = cityList.get(i);

                if (!TextUtils.isEmpty(cityName)) {
                    SelectConditionModel city = new SelectConditionModel();
                    city.setFieldName("城市");
                    city.setFieldValue(cityName);
                    city.setValue(i + 1);
                    mChannelList.add(city);
                }
            }

            //滚动条
            initCategoryView();
        }
    }
}
