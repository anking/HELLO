package com.yanhua.home.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.common.model.BarAnswerModel;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BarQuestionDeatilAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 问答详情页面
 */
@Route(path = ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
public class BarQuestionDetailActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.tv_name)
    TextView tvName;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_answer)
    RecyclerView rvAnswer;
    @BindView(R2.id.ivBarImg)
    ImageView ivBarImg;
    @BindView(R2.id.tvBarName)
    TextView tvBarName;
    @BindView(R2.id.tvBrowseNum)
    TextView tvBrowseNum;
    @BindView(R2.id.tvQuestionName)
    TextView tvQuestionName;
    @BindView(R2.id.civ_avatar)
    CircleImageView civAvatar;
    @BindView(R2.id.tvUserName)
    TextView tvUserName;
    @BindView(R2.id.tvTime)
    TextView tvTime;
    @BindView(R2.id.tvFollow)
    TextView tvFollow;
    @BindView(R2.id.tvCommentNum)
    TextView tvCommentNum;
    @BindView(R2.id.llCommentNum)
    LinearLayout llCommentNum;

    @Autowired
    String askId;//qModel == null ? myQAModel.getAskedId() : qModel.getParentAskedId()

    private float alpha;
    private BarQuestionDeatilAdapter mAdapter;
    private BarQAModel barQAModel;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_question_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);
        rvAnswer.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BarQuestionDeatilAdapter(this);
        rvAnswer.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            ListResult<BarAnswerModel> appBarAnswerVo = barQAModel.getAppBarAnswerVo();
            List<BarAnswerModel> records = appBarAnswerVo.getRecords();
            ARouter.getInstance()
                    .build(ARouterPath.BAR_ANSWER_DETAIL_ACTIVITY)
                    .withString("askId", records.get(pos).getAskedId())
                    .navigation();
        });
        setTopView();
    }

    private void setTopView() {
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;
            alpha = 1 - rangle;
            if (alpha == 1) {//避免滑动过快还显示半透明状态
                mAppBarLayout.postDelayed(() -> {
                    tvName.setText(barQAModel.getBarName());
                }, 100);
            } else if (alpha == 0) {//避免滑动过快还显示半透明状态
                tvName.setText("问答详情");
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        getBarQDetail();
    }

    private void getBarQDetail() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("askId", askId);
        params.put("current", current);
        params.put("size", size);
        basePresenter.getBarQADetail(params);
    }

    @OnClick({R2.id.rlBarInfo, R2.id.llAnswer, R2.id.iconMore, R2.id.tvFollow, R2.id.civ_avatar, R2.id.tvUserName})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.rlBarInfo) {
            ARouter.getInstance()
                    .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                    .withString("barId", barQAModel.getBarId())
                    .navigation();
        } else if (id == R.id.llAnswer) {
            BarModel barModel = new BarModel();
            barModel.setBarId(barQAModel.getBarId());
            barModel.setName(barQAModel.getBarName());
            barModel.setBarPrimaryImg(barQAModel.getBarImg());
            barModel.setBrowseNum(barQAModel.getBarBrowseCount());
            ARouter.getInstance()
                    .build(ARouterPath.BAR_POST_QA_ACTIVITY)
                    .withSerializable("barModel", barModel)
                    .withSerializable("qModel", barQAModel)
                    .withInt("type", 2)
                    .navigation();
        } else if (id == R.id.iconMore) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                boolean myself = UserManager.getInstance().isMyself(barQAModel.getParentUserId());
                CommentBoardPopup popup = new CommentBoardPopup(mActivity, false, false, false, !myself, myself);
                popup.setOnActionItemListener(actionType -> {
                    switch (actionType) {
                        case CommentBoardPopup.ACTION_REPORT:
                            toReport();
                            break;
                        case CommentBoardPopup.ACTION_DELETE:
                            toDelete();
                            break;
                    }
                });
                new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
            });
        } else if (id == R.id.tvFollow) {
            if (barQAModel.getParentIsFollow() == 1) {
                AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    basePresenter.cancelFollowUser(barQAModel.getParentUserId());
                });
                alertPopup.setOnCancelListener(alertPopup::dismiss);
            } else {
                basePresenter.followUser(barQAModel.getParentUserId());
            }
        } else if (id == R.id.civ_avatar || id == R.id.tvUserName) {
            if (barQAModel.getParentIsShow() != 1) {
                // 跳转到提问者的个人主页
                ARouter.getInstance()
                        .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", barQAModel.getParentUserId())
                        .navigation();
            }
        }
    }

    /**
     * 举报酒吧问答详情
     */
    private void toReport() {
        ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                .withString("businessID", barQAModel.getParentId())
                .withInt("reportType", YXConfig.reportType.question_answer)
                .navigation();
    }

    /**
     * 删除问答详情
     */
    private void toDelete() {
        AlertPopup alertPopup = new AlertPopup(mContext, "确认删除该问题？");
        new XPopup.Builder(mContext).asCustom(alertPopup).show();
        alertPopup.setOnConfirmListener(() -> {
            alertPopup.dismiss();
            basePresenter.deleteMyQuestion(barQAModel.getParentAskedId());
        });
        alertPopup.setOnCancelListener(alertPopup::dismiss);
    }

    @Override
    public void handleDeleteMyQuestionSuccess() {
        ToastUtils.showShort("删除成功");
        setResult(RESULT_OK);
        onBackPressed();
    }

    @Override
    public void handleBarQADetailSuccess(BarQAModel model) {
        if (model != null) {
            barQAModel = model;
            ImageLoaderUtil.loadImg(ivBarImg, model.getBarImg());
            tvBrowseNum.setText(String.format("%d人浏览", model.getBarBrowseCount()));
            tvName.setText(model.getBarName());
            tvQuestionName.setText(model.getParentAskedDesc());
            tvBarName.setText(model.getBarName());
            if (model.getParentIsShow() == 1) {
                // 匿名
                ImageLoaderUtil.loadImg(civAvatar, R.drawable.place_holder);
                tvUserName.setText("匿名用户");
                tvFollow.setVisibility(View.GONE);
            } else {
                if (UserManager.getInstance().isMyself(model.getParentUserId())) {
                    tvFollow.setVisibility(View.GONE);
                } else {
                    tvFollow.setVisibility(View.VISIBLE);
                    tvFollow.setText(model.getParentIsFollow() == 1 ? "已关注" : "关注");
                }

                ImageLoaderUtil.loadImg(civAvatar, model.getParentImg());
                tvUserName.setText(model.getParentAccountName());
            }


            tvTime.setText(String.format("提问于 %s", YXTimeUtils.getFriendlyTimeAtContent(model.getCreatedTime(), true)));
            ListResult<BarAnswerModel> appBarAnswerVo = model.getAppBarAnswerVo();
            if (appBarAnswerVo != null) {
                List<BarAnswerModel> records = appBarAnswerVo.getRecords();
                if (records.size() > 0) {
                    llCommentNum.setVisibility(View.VISIBLE);
                    tvCommentNum.setText(String.format("共%d条回答", appBarAnswerVo.getTotal()));
                } else {
                    llCommentNum.setVisibility(View.GONE);
                    rvAnswer.setVisibility(View.GONE);
                }
                mAdapter.setItems(records);
            } else {
                llCommentNum.setVisibility(View.GONE);
                rvAnswer.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void updateFollowUserSuccess(boolean follow) {
        if (follow) {
            // 关注成功
            ToastUtils.showShort("关注成功");
            barQAModel.setParentIsFollow(1);
            tvFollow.setText("已关注");
        } else {
            // 取关成功
            ToastUtils.showShort("取消关注成功");
            barQAModel.setParentIsFollow(0);
            tvFollow.setText("关注");
        }
    }
}

