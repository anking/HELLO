package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.KeyboardUtils;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.BarConfigModel;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.widget.AutoMarqueeTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BarQuestionListAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 问题列表
 */
@Route(path = ARouterPath.BAR_QUESTION_LIST_ACTIVITY)
public class BarQuestionListActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.tv_right)
    TextView tvRight;
    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.rlTip)
    RelativeLayout rlTip;
    @BindView(R2.id.tvTipContent)
    AutoMarqueeTextView tvTipContent;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_question)
    DataObserverRecyclerView rvQuestion;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    @Autowired
    BarModel barModel;

    private BarQuestionListAdapter mAdapter;
    private List<BarQAModel> qaList;
    private int total;
    private String searchVague;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_question_list;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText("我的\n问答");
        tvHeader.setText("大家问");
        qaList = new ArrayList<>();
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            searchVague = null;
            getDataList();
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (qaList.size() < total) {
                current++;
                getDataList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        rvQuestion.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BarQuestionListAdapter(this);
        rvQuestion.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            BarQAModel barQAModel = qaList.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                    .withString("askId", barQAModel.getParentAskedId())
                    .navigation();
        });
        mAdapter.setOnJumpPageListener((page, pos) -> {
            BarQAModel barQAModel = qaList.get(pos);
            if (page == 1) {
                ARouter.getInstance()
                        .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                        .withString("askId", barQAModel.getParentAskedId())
                        .navigation();
            } else if (page == 2) {
                ARouter.getInstance()
                        .build(ARouterPath.BAR_POST_QA_ACTIVITY)
                        .withSerializable("barModel", barModel)
                        .withSerializable("qModel", barQAModel)
                        .withInt("type", 2)
                        .navigation();
            }
        });
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //关闭软键盘
                KeyboardUtils.hideSoftInput(mActivity);
                searchVague = etSearch.getText().toString().trim();
                getDataList();
                return true;
            }
            return false;
        });
        etSearch.setOnInputTextChangeListener(content -> {
            if (TextUtils.isEmpty(content)) {
                searchVague = null;
                getDataList();
            }
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        basePresenter.getConfigByType(YXConfig.BASE_CONFIG_BAR);
        refreshLayout.autoRefresh(100);
    }

    private void getDataList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("barId", barModel.getBarId());
        params.put("current", current);
        params.put("size", size);
        if (!TextUtils.isEmpty(searchVague)) {
            params.put("searchVague", searchVague);
        }
        basePresenter.getBarQAList(params);
    }

    @OnClick({R2.id.tv_right, R2.id.iconDelete, R2.id.llAsk})
    public void ckickView(View view) {
        int id = view.getId();
        if (id == R.id.tv_right) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.BAR_MY_QA_ACTIVITY).navigation();
            });
        } else if (id == R.id.iconDelete) {
            rlTip.setVisibility(View.GONE);
        } else if (id == R.id.llAsk) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance()
                        .build(ARouterPath.BAR_POST_QA_ACTIVITY)
                        .withInt("type", 1)
                        .withSerializable("barModel", barModel)
                        .navigation();
            });
        }
    }

    @Override
    public void handleBarConfig() {
        BarConfigModel barConfigModel = DiscoCacheUtils.getInstance().getBarConfigModel();
        if (null != barConfigModel && !TextUtils.isEmpty(barConfigModel.getBarRiskStatementOpenAuditTip())) {
            rlTip.setVisibility(View.VISIBLE);
            tvTipContent.setText(barConfigModel.getBarRiskStatementOpenAuditTip());
        } else {
            rlTip.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleBarQAListSuccess(ListResult<BarQAModel> list) {
        if (current == 1) {
            qaList.clear();
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null) {
            List<BarQAModel> records = list.getRecords();
            total = list.getTotal();
            if (records != null && !records.isEmpty()) {
                qaList.addAll(records);
            }
        }
        mAdapter.setItems(qaList);
    }
}