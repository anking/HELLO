package com.yanhua.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.MyAnswerAdapter;
import com.yanhua.home.adapter.MyQuestionAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

@Route(path = ARouterPath.BAR_MY_QA_ACTIVITY)
public class BarMyQAActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_qa)
    DataObserverRecyclerView rvQA;
    @BindView(R2.id.ctlQA)
    CommonTabLayout ctlQA;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyView;

    private int total;
    private List<MyQAModel> qaList;
    private MyQuestionAdapter questionAdapter;
    private MyAnswerAdapter answerAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_my_qa;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        tvHeader.setText("我的问答");
        qaList = new ArrayList<>();
        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("提问", 0, 0));
        tabEntityList.add(new TabEntity("回答", 0, 0));
        ctlQA.setTabData(tabEntityList);
        ctlQA.setCurrentTab(0);
        ctlQA.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    rvQA.setAdapter(questionAdapter);
                } else if (position == 1) {
                    rvQA.setAdapter(answerAdapter);
                }
                current = 1;
                getDataList();
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getDataList();
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (qaList.size() < total) {
                current++;
                getDataList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        rvQA.setLayoutManager(new LinearLayoutManager(this));
        questionAdapter = new MyQuestionAdapter(this);
        answerAdapter = new MyAnswerAdapter(this);
        rvQA.setAdapter(questionAdapter);
        questionAdapter.setOnItemClickListener((itemView, pos) -> {
            if (FastClickUtil.isFastClick(500)) {
                // 跳转到提问详情页面
                MyQAModel myQAModel = qaList.get(pos);
                ARouter.getInstance()
                        .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                        .withString("askId", myQAModel.getAskedId())
                        .navigation(BarMyQAActivity.this, 1024);
            }
        });
        answerAdapter.setOnItemClickListener((itemView, pos) -> {
            if (FastClickUtil.isFastClick(500)) {
                // 跳转到回答详情页面
                MyQAModel myQAModel = qaList.get(pos);
                ARouter.getInstance()
                        .build(ARouterPath.BAR_ANSWER_DETAIL_ACTIVITY)
                        .withString("askId", myQAModel.getAskedId())
                        .navigation(BarMyQAActivity.this, 1024);
            }
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        refreshLayout.autoRefresh(100);
    }

    private void getDataList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", UserManager.getInstance().getUserId());
        params.put("current", current);
        params.put("size", size);
        params.put("replyStatus", ctlQA.getCurrentTab());
        basePresenter.getMyBarQAList(ctlQA.getCurrentTab(), params);
    }

    @Override
    public void handleMyQAListSuccess(int type, ListResult<MyQAModel> list) {
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            qaList.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        List<MyQAModel> records = list.getRecords();
        total = list.getTotal();
        if (records != null && !records.isEmpty()) {
            qaList.addAll(records);
        }
        if (type == 0) {
            if (qaList.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setErrorType(EmptyLayout.NODATA);
                emptyView.setErrorMessage("暂无提问");
                rvQA.setEmptyView(emptyView);
            } else {
                emptyView.setVisibility(View.GONE);
                questionAdapter.setItems(qaList);
            }
        } else if (type == 1) {
            if (qaList.isEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setErrorType(EmptyLayout.NODATA);
                emptyView.setErrorMessage("暂无回答");
                rvQA.setEmptyView(emptyView);
            } else {
                emptyView.setVisibility(View.GONE);
                answerAdapter.setItems(qaList);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1024 && resultCode == RESULT_OK) {
            refreshLayout.autoRefresh(100);
        }
    }
}