package com.yanhua.home.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.lqr.adapter.LQRAdapterForRecyclerView;
import com.lqr.adapter.LQRViewHolderForRecyclerView;
import com.lqr.recyclerview.LQRRecyclerView;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.InviteDetailPresenter;
import com.yanhua.home.presenter.contract.InviteDetailContract;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

@Route(path = ARouterPath.INVITE_ENROLL_LIST_ACTIVITY)
public class InviteEnrollListActivity extends BaseMvpActivity<InviteDetailPresenter> implements InviteDetailContract.IView {

    @BindView(R2.id.tvJoined)
    TextView tvJoined;
    @BindView(R2.id.rvJoinedMember)
    LQRRecyclerView rvJoinedMember;
    @BindView(R2.id.rvJoiningMember)
    LQRRecyclerView rvJoiningMember;

    @Autowired
    String inviteId;//邀約的id
    @Autowired
    int showOpenSetEnroll;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("参加组局详情");

        getListData(2);
        getListData(1);
    }

    public void getListData(int type) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", 1);
        params.put("size", 10000);
        params.put("type", type);//(1:待加入, 2:已加入)
        params.put("userGender", 0);//邀约用户性别(0:男女不限, 1:男, 2:女)
        params.put("inviteId", inviteId);

        basePresenter.meetPartnerInviteMembers(params, type);
    }

    @Override
    public void handInviteMemberList(ListResult<InviteMemberModel> data, int type) {
        List<InviteMemberModel> list = data.getRecords();

        if (list != null && !list.isEmpty()) {
            if (type == 2) {
                //已邀约
                LQRAdapterForRecyclerView<InviteMemberModel> mAdapter = new LQRAdapterForRecyclerView<InviteMemberModel>(mContext, list, R.layout.item_invite_member_info) {
                    @Override
                    public void convert(LQRViewHolderForRecyclerView helper, InviteMemberModel item, int position) {
                        String nickName = item.getUserName();
                        String userImage = item.getUserImage();
                        int gender = item.getUserGender();

                        ImageView ivHeader = helper.getView(R.id.ivHeader);
                        Glide.with(mContext).load(userImage).centerCrop().into(ivHeader);

                        TextView tvName = helper.getView(R.id.tvName);
                        tvName.setText(nickName);

                        ImageView ivInfoSex = helper.getView(R.id.ivInfoSex);

                        //未报名  0:关闭，1：金头像 2：头衔昵称 3：个人主页
                        switch (showOpenSetEnroll) {
                            case 1:
                                tvName.setVisibility(View.GONE);
                                break;
                            case 2:
                            case 3:
                                tvName.setVisibility(View.VISIBLE);
                                if (gender == -1) {
                                    ivInfoSex.setVisibility(View.GONE);
                                } else {
                                    ivInfoSex.setVisibility(View.VISIBLE);
                                    ivInfoSex.setImageResource(gender == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                };
                mAdapter.setOnItemClickListener((helper, parent, itemView, position) -> {
                    if (showOpenSetEnroll == 3) {
                        ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                                .withString("userId", list.get(position).getUserId())
                                .navigation();
                    }
                });
                rvJoinedMember.setAdapter(mAdapter);
            } else if (type == 1) {
                //未确认
                LQRAdapterForRecyclerView<InviteMemberModel> mAdapter = new LQRAdapterForRecyclerView<InviteMemberModel>(mContext, list, R.layout.item_invite_member_info) {
                    @Override
                    public void convert(LQRViewHolderForRecyclerView helper, InviteMemberModel item, int position) {
                        String nickName = item.getUserName();
                        String userImage = item.getUserImage();
                        int gender = item.getUserGender();

                        ImageView ivHeader = helper.getView(R.id.ivHeader);
                        Glide.with(mContext).load(userImage).centerCrop().into(ivHeader);

                        TextView tvName = helper.getView(R.id.tvName);
                        tvName.setText(nickName);

                        ImageView ivInfoSex = helper.getView(R.id.ivInfoSex);


                        //未报名  0:关闭，1：金头像 2：头衔昵称 3：个人主页
                        switch (showOpenSetEnroll) {
                            case 1:
                                tvName.setVisibility(View.GONE);
                                break;
                            case 2:
                            case 3:
                                tvName.setVisibility(View.VISIBLE);
                                if (gender == -1) {
                                    ivInfoSex.setVisibility(View.GONE);
                                } else {
                                    ivInfoSex.setVisibility(View.VISIBLE);
                                    ivInfoSex.setImageResource(gender == 1 ? R.mipmap.ic_male : R.mipmap.ic_female);
                                }
                                break;

                            default:
                                break;
                        }

                    }
                };
                mAdapter.setOnItemClickListener((helper, parent, itemView, position) -> {
                    if (showOpenSetEnroll == 3) {
                        ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                                .withString("userId", list.get(position).getUserId())
                                .navigation();
                    }
                });
                rvJoiningMember.setAdapter(mAdapter);
            }
        }
    }


    @Override
    protected void creatPresent() {
        basePresenter = new InviteDetailPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_enroll_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

}
