package com.yanhua.home.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.home.R;
import com.yanhua.home.fragment.PublishInviteFirstFragment;
import com.yanhua.home.presenter.InvitePresenter;
import com.yanhua.home.presenter.contract.InviteContract;

import java.util.HashMap;

import butterknife.OnClick;

@Route(path = ARouterPath.PUBLISH_INVITE_ACTIVITY)
public class PublishInviteActivity extends BaseMvpActivity<InvitePresenter> implements InviteContract.IView {

    @Autowired
    InviteAddForm inviteAddForm;//来自修改

    @Autowired
    boolean editable;//进行中 7

    @Override
    protected void creatPresent() {
        basePresenter = new InvitePresenter();
    }

    @OnClick({})
    public void onClickView(View view) {
        int id = view.getId();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_publish;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        PublishInviteFirstFragment fragment = new PublishInviteFirstFragment();

        Bundle data = new Bundle();
        data.putSerializable("addInviteForm", inviteAddForm);
        fragment.setArguments(data);

        FragmentManager manager = this.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container, fragment);
        transaction.commit();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //获取邀约分类列表
        HashMap params = new HashMap();

        params.put("current", 1);
        params.put("size", 500);//不分页
//        params.put("username", "string");
        basePresenter.meetPartnerTypeList(params);

        basePresenter.meetPartnerCreateType();

        if (editable) {
            basePresenter.meetPartnerUpdateStatus(inviteAddForm.getId());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0)//这里是取出我们返回栈存在Fragment的个数
            finish();
        else
            getSupportFragmentManager().popBackStack();
    }
}
