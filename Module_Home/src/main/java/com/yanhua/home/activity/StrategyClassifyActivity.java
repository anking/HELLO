package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.StrategyClassifyAdapter;
import com.yanhua.home.adapter.StrategyGameAdapter;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

@Route(path = ARouterPath.STRATEGY_CLASSIFY_ACTIVITY)
public class StrategyClassifyActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.tvPublish)
    TextView tvPublish;

    @BindView(R2.id.rvContent)
    RecyclerView rvContent;

    @BindView(R2.id.rvClassify)
    RecyclerView rvClassify;

    @Autowired
    int selectPos = 0;
    @Autowired
    ArrayList<NewsTypeModel> channelList;

    private StrategyGameAdapter mAdapter;
    ArrayList<StrategyModel> mListData;
    private int total;

    @Override
    public int bindLayout() {
        return R.layout.activity_strategy_classify;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, /*llEquipment, llMethod, llPlays, llShop,*/ tvPublish);

        refreshLayout.setOnRefreshListener(rl -> {
            current = 1;
            getListData(current);
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        //StrategyHotSortModel---栏目列表
        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new StrategyGameAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            StrategyModel clickItem = mAdapter.getItemObject(pos);
            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_STRATEGY);
        });


        //===分类---
        LinearLayoutManager classifyManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        StrategyClassifyAdapter classifyMainAdapter = new StrategyClassifyAdapter(mContext, selectPos);//selectPos
        rvClassify.setLayoutManager(classifyManager);
        rvClassify.setAdapter(classifyMainAdapter);

        classifyMainAdapter.setItems(channelList);//

        classifyMainAdapter.setOnItemClickListener((itemView, pos) -> {
            if (pos != selectPos) {
                classifyMainAdapter.setSelectPos(pos);
                selectPos = pos;

                //刷新
                current = 1;
                getListData(current);
//
//                refreshLayout.autoRefresh(100);//刷新
            }
        });
    }

    private void getListData(int page) {
        //请求列表接口数据
        String typeId = "";
        NewsTypeModel selectItem = channelList.get(selectPos);
        if (null != selectItem && !TextUtils.isEmpty(selectItem.getId())) {
            typeId = selectItem.getId();
        }

        basePresenter.strategyList(page, size, "", typeId, YXConfig.TYPE_STRATEGY);
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mListData = new ArrayList<>();

        //刷新
//        refreshLayout.autoRefresh(100);
        current = 1;
        getListData(current);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.tvPublish) {
            //发布策略、攻略
            PublishNewsForm publishNewsForm = PublishTemp.getStrategyTemp(mContext);
            ARouter.getInstance().build(ARouterPath.PUBLISH_STRATEGY_ACTIVITY)
                    .withSerializable("publishNewsForm", publishNewsForm).navigation();
        }
    }


    @Override
    public void handleStrategyList(ListResult<StrategyModel> listResult) {
        List<StrategyModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();

            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }


}
