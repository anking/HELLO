package com.yanhua.home.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.view.TimePickerView;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chinalwb.are.AREditText;
import com.chinalwb.are.strategies.ImageStrategy;
import com.chinalwb.are.styles.toolbar.ARE_ToolbarDefault;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentCenter;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentLeft;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_AlignmentRight;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_BackgroundColor;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Bold;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontColor;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_FontSize;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Hr;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Image;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Italic;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Link;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_ListBullet;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Quote;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Strikethrough;
import com.chinalwb.are.styles.toolitems.ARE_ToolItem_Underline;
import com.chinalwb.are.styles.toolitems.IARE_ToolItem;
import com.chinalwb.are.styles.toolitems.styles.ARE_Style_Image;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.lxj.xpopup.XPopup;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.OnDeleteImageListen;
import com.yanhua.common.adapter.SelectKeyAdapter;
import com.yanhua.common.adapter.SelectTagAdapter;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.PublishResultModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoImageStrategy;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.OSSPushListener;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.OssManagerUtil;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.widget.AddKeyWordPopup;
import com.yanhua.common.widget.ChooseImageDialog;
import com.yanhua.common.widget.PickTopicBottomPop;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.PUBLISH_STRATEGY_ACTIVITY)
public class PublishStrategyActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView {
    private static final int SELECT_ADDRESS = 0x006;
    private static final int CHOOSE_CONTENT_IMAGE = 0x001;
    private static final int CHOOSE_CONTENT_VIDEO = 0x002;
    private static final int CHOOSE_COVER_IMAGE = 0x003;
    private static final int CHOOSE_COVER_VIDEO = 0x004;
    private static final int CHOOSE_TYPE_IMAGE = PictureConfig.TYPE_IMAGE;
    private static final int CHOOSE_TYPE_VIDEO = PictureConfig.TYPE_VIDEO;

    @BindView(R2.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R2.id.llTop)
    LinearLayout llTop;

    @BindView(R2.id.areToolbar)
    ARE_ToolbarDefault mToolbar;
    @BindView(R2.id.arEditText)
    AREditText mEditText;
    @BindView(R2.id.etTitle)
    EditText etTitle;
    @BindView(R2.id.etGuideTitle)
    EditText etGuideTitle;
    @BindView(R2.id.upload)
    RelativeLayout uploadContainer;//根据封面比例  351：210 5：3
    @BindView(R2.id.ivUpload)
    ImageView ivUpload;//根据封面比例  351：210 5：3
    @BindView(R2.id.ivVideoPlay)
    ImageView ivVideoPlay;//
    @BindView(R2.id.llAddCover)
    LinearLayout llAddCover;//

    @BindView(R2.id.llBottom)
    LinearLayout llBottom;//

    @BindView(R2.id.llSmallAddCover)
    LinearLayout llSmallAddCover;//添加后才显示
    @BindView(R2.id.viewTitle)
    View viewTitle;//添加后才显示
    @BindView(R2.id.rlTitleContainer)
    RelativeLayout rlTitleContainer;//添加后才显示

    @BindView(R2.id.llTitle)
    LinearLayout llTitle;//添加后才显示背景颜色变透明

    @BindView(R2.id.iconRootTypeAndTime)
    AliIconFontTextView iconRootTypeAndTime;
    @BindView(R2.id.llShowTypeAndTime)
    LinearLayout llShowTypeAndTime;
    @BindView(R2.id.rvType)
    RecyclerView rvType;

    @BindView(R2.id.tvPublishNow)
    TextView tvPublishNow;
    @BindView(R2.id.tvPublishByTime)
    TextView tvPublishByTime;
    @BindView(R2.id.llPublishByTime)
    LinearLayout llPublishByTime;

    @BindView(R2.id.publisTimeType)
    TextView publisTimeType;
    @BindView(R2.id.iconAddressAndKeys)
    AliIconFontTextView iconAddressAndKeys;
    @BindView(R2.id.llAddressAndKeys)
    LinearLayout llAddressAndKeys;

    @BindView(R2.id.rvKeys)
    RecyclerView rvKeys;
    SelectKeyAdapter keyAdapter;

    @BindView(R2.id.llAddAddress)
    LinearLayout llAddAddress;
    @BindView(R2.id.tvAddAddress)
    TextView tvAddAddress;

    @BindView(R2.id.tflHistory)
    TagFlowLayout tflHistory;

    @BindView(R2.id.btnPreview)
    TextView btnPublish;

    private TimePickerView pvTime;

    @Autowired
    PublishNewsForm publishNewsForm;
    private TagAdapter tagAdapterHistory;
    private List<String> listKeyDatas;

    private SelectTagAdapter typeAdapter;//攻略分类

    private void setRVKeys() {
        listKeyDatas = new ArrayList<>();
        listKeyDatas.add("添加");

        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        keyAdapter = new SelectKeyAdapter(mContext, new OnDeleteImageListen() {
            @Override
            public void onDeleteImage(int pos) {
                listKeyDatas.remove(pos);

                keyAdapter.setItems(listKeyDatas);
            }
        });
        rvKeys.setLayoutManager(manager);
        rvKeys.setAdapter(keyAdapter);

        keyAdapter.setItems(listKeyDatas);

        keyAdapter.setOnItemClickListener((itemView, pos) -> {
            if (pos == 0) {
                if (listKeyDatas.size() > 3) {
                    ToastUtils.showShort("最多只能添加三个关键字");
                    return;
                }
                showAddKeyWord();
            }
        });
    }

    private void showAddKeyWord() {
        AddKeyWordPopup popup = new AddKeyWordPopup(mContext);
        popup.setOnSendCommentListener((comment) -> {//发送评论
            listKeyDatas.add(comment);

            keyAdapter.setItems(listKeyDatas);
        });
        new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
    }

    private int mSlectStrategyTypePos;

    private void setRVStrategy() {
        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        typeAdapter = new SelectTagAdapter(mContext);
        rvType.setLayoutManager(manager);
        rvType.setAdapter(typeAdapter);

        typeAdapter.setOnItemClickListener((itemView, pos) -> {

            if (pos != mSlectStrategyTypePos) {
                mSlectStrategyTypePos = pos;
                typeAdapter.setSelect(mSlectStrategyTypePos);
            }
        });
        /**
         *{@link #handleNewsTypeList(List<NewsTypeModel> data)}
         */
        basePresenter.getNewsTypeList(YXConfig.TYPE_STRATEGY, 1);
    }

    @Override
    public void handleNewsTypeList(List<NewsTypeModel> data) {
        if (null != data && data.size() > 0) {
            typeAdapter.setItems(data);
        }
    }

    private void initPublishButton() {
        ivVideoPlay.setVisibility(View.GONE);
        tvPublishNow.setSelected(true);
        tvPublishByTime.setSelected(false);
        llPublishByTime.setVisibility(View.GONE);
    }

    private void initViewStatus() {
        setRVStrategy();
        initPublishButton();
        setRVKeys();
    }

    @OnClick({R2.id.btnPreview, R2.id.llRootTypeAndTime, R2.id.rlPublishByTime, R2.id.llAddCover, R2.id.llSmallAddCover,
            R2.id.llRootAddressAndKeys, R2.id.llAddAddress, R2.id.tvPublishNow, R2.id.tvPublishByTime, R2.id.llTopic
    })
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.btnPreview) {
            //发布----------------
            //分类
            List<NewsTypeModel> typeList = typeAdapter.getmDataList();
            if (null != typeList && typeList.size() > 0) {
                String typeId = typeList.get(mSlectStrategyTypePos).getId();
                String guideText = etGuideTitle.getText().toString().trim();
                String title = etTitle.getText().toString().trim();
                String html = this.mEditText.getHtml();
//

                if (FastClickUtil.isFastClick(1000)) {
                    publishNewsForm.setCoverUrl(uploadUrl);
                    publishNewsForm.setContentCategoryId(typeId);// //所属文章分类主键ID
                    publishNewsForm.setContentTitle(title);// //内容标题
                    publishNewsForm.setContent(html);// //内容
                    publishNewsForm.setIntroduction(guideText);// 导语
                    publishNewsForm.setIssueAddress(mPoiName);
//                publishNewsForm.setIssueAddressDetail(issueAddressDetail);
                    publishNewsForm.setIssueCity(mCityName);
                    publishNewsForm.setIssueLatitude(String.valueOf(mLatitude));
                    publishNewsForm.setIssueLongitude(String.valueOf(mLongitude));

                    //删除第一个
                    publishNewsForm.setKeyword(YHStringUtils.parseListToString(listKeyDatas.subList(1, listKeyDatas.size())));///关键词
                    boolean isNowPublish = tvPublishNow.isSelected();
                    if (isNowPublish) {
                        publishNewsForm.setPrePublishTime(null);//预计发布时间---在选择时间的时候已经赋值了
                    }

                    publishNewsForm.setPublishType(isNowPublish ? 1 : 2);//1 立即发布 2 定时发布
                    publishNewsForm.setType(4);//类型 3:爆料 4:攻略
//                publishNewsForm.setUsername(UserManager.getInstance().getUserInfo().getNickName());//类型 3:爆料 4:攻略

                    if (!TextUtils.isEmpty(videoCoverUrl)) {
                        publishNewsForm.setVedioCoverUrl(videoCoverUrl);
                        publishNewsForm.setVideoTime(videoTime);
                    } else {
                        publishNewsForm.setVedioCoverUrl(null);
                    }
                    basePresenter.publishNewsDetail(publishNewsForm);
                }
            } else {
                ToastUtils.showShort("请选择攻略分类");
            }
        } else if (id == R.id.llTopic) {
            pickTopic(false);
        } else if (id == R.id.llRootTypeAndTime) {
            int visible = llShowTypeAndTime.getVisibility();
            if (visible == View.VISIBLE) {
                llShowTypeAndTime.setVisibility(View.GONE);
                iconRootTypeAndTime.setText("\ue750");
            } else {
                llShowTypeAndTime.setVisibility(View.VISIBLE);
                iconRootTypeAndTime.setText("\ue739");//&#xe739;
            }
        } else if (id == R.id.llAddCover || id == R.id.llSmallAddCover) {
            ChooseImageDialog chooseImageDialog = new ChooseImageDialog(mContext, new ChooseImageDialog.OnButtonClickListener() {
                @Override
                public void onClick(int type) {
                    if (type == CHOOSE_TYPE_IMAGE) {
                        checkStoragePermission(type, CHOOSE_COVER_IMAGE);
                    } else {
                        checkStoragePermission(type, CHOOSE_COVER_VIDEO);
                    }
                }
            });
            new XPopup.Builder(this).asCustom(chooseImageDialog).show();
        } else if (id == R.id.rlPublishByTime) {
            //选择时间
            if (null != pvTime) {
                pvTime.setDate(Calendar.getInstance());
                pvTime.show(); //show timePicker*/
//                pvTime.show(v);//弹出时间选择器，传递参数过去，回调的时候则可以绑定此view
            }
        } else if (id == R.id.llRootAddressAndKeys) {
            int visible = llAddressAndKeys.getVisibility();
            if (visible == View.VISIBLE) {
                llAddressAndKeys.setVisibility(View.GONE);
                iconAddressAndKeys.setText("\ue750");
            } else {
                llAddressAndKeys.setVisibility(View.VISIBLE);
                iconAddressAndKeys.setText("\ue739");//&#xe739;
            }
        } else if (id == R.id.llAddAddress) {
            checkLoacationStatus();
        } else if (id == R.id.tvPublishNow) {
            tvPublishNow.setSelected(true);
            tvPublishByTime.setSelected(false);
            llPublishByTime.setVisibility(View.GONE);

            publishNewsForm.setPrePublishTime("");
        } else if (id == R.id.tvPublishByTime) {
            tvPublishNow.setSelected(false);
            tvPublishByTime.setSelected(true);
            llPublishByTime.setVisibility(View.VISIBLE);
        }
    }


    private List<TopicModel> pickedTopics;

    private void pickTopic(boolean isEnter) {
        PickTopicBottomPop popup = new PickTopicBottomPop(mContext, topic -> {
            if (null != topic) {
                pickedTopics.add(topic);

                tagAdapterHistory.refreshData(pickedTopics);
            }
        });
        new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(false).autoFocusEditText(false).moveUpToKeyboard(false).asCustom(popup).show();
    }

    private void initTimePicker() {//Dialog 模式下，在底部弹出
        pvTime = new TimePickerBuilder(mContext, (date, v) -> {
            Date now = Calendar.getInstance().getTime();
            long minutes = DiscoTimeUtils.getDatePoor(date, now);
            if (minutes <= YXConfig.PUBLISH_MIN_TIME) {
                ToastUtils.showShort("选取时间需要在" + YXConfig.PUBLISH_MIN_TIME + "分钟后");
                return;
            }
            String selectTime = DiscoTimeUtils.getTimeNews(date);
            publishNewsForm.setPrePublishTime(DiscoTimeUtils.getRequestTime(date));//后台所需参数格式
            publisTimeType.setText(selectTime);
        })
                .setTimeSelectChangeListener(date -> {
                })
                .setType(new boolean[]{true, true, true, true, true, false})
                .isDialog(false) //默认设置false ，内部实现将DecorView 作为它的父控件。
                .addOnCancelClickListener(view -> {
                })
                .setItemVisibleCount(7) //若设置偶数，实际值会加1（比如设置6，则最大可见条目为7）
                .setLineSpacingMultiplier(2.0f)
                .setTitleText("发布时间")
                .isAlphaGradient(true)
                .build();
    }


    /**
     * 检测是否打开定位
     */
    private void checkLoacationStatus() {
        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ok) {// 开了定位服务
            ARouter.getInstance().build(ARouterPath.ADDRESS_SEARCH_ACTIVITY).navigation(mActivity, SELECT_ADDRESS);
        } else {
            CommonDialog commonDialog = new CommonDialog(this, "是否开启定位服务？", "开启", "取消");
            new XPopup.Builder(this).asCustom(commonDialog).show();
            commonDialog.setOnConfirmListener(() -> {
                commonDialog.dismiss();
                Intent i = new Intent();
                i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            });
            commonDialog.setOnCancelListener(() -> commonDialog.dismiss());
        }
    }

    ARE_ToolItem_Image image;


    private DiscoImageStrategy imageStrategy = new DiscoImageStrategy();

    @Override
    public int bindLayout() {
        return R.layout.activity_publish_strategy;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

//        //5：3
        int width = DisplayUtils.getScreenWidth(mContext);
        int height = width;


        mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    llBottom.setVisibility(View.VISIBLE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.scrollTo(0, height * 2 / 3);
                        }
                    }, 500);
                } else {
                    llBottom.setVisibility(View.GONE);
                }
            }
        });

        ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = height;
        uploadContainer.setLayoutParams(imageLayoutParams);

        initEditToolbar();

        initViewStatus();

        initTimePicker();

        setTopView();
    }

    private void setTopView() {
        int paddingDistance = UltimateBarX.getStatusBarHeight() + DisplayUtils.dip2px(mContext, 50);
        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int uploadContainerHeight = uploadContainer.getHeight() - paddingDistance;
            if (scrollY < uploadContainerHeight) {
                llTop.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
            } else {
                llTop.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }
        });
    }

    private void initEditToolbar() {
        IARE_ToolItem bold = new ARE_ToolItem_Bold();
        IARE_ToolItem italic = new ARE_ToolItem_Italic();
        IARE_ToolItem underline = new ARE_ToolItem_Underline();
        IARE_ToolItem strikethrough = new ARE_ToolItem_Strikethrough();
        IARE_ToolItem fontSize = new ARE_ToolItem_FontSize();
        IARE_ToolItem fontColor = new ARE_ToolItem_FontColor();
        IARE_ToolItem backgroundColor = new ARE_ToolItem_BackgroundColor();
        IARE_ToolItem quote = new ARE_ToolItem_Quote();
//        IARE_ToolItem listNumber = new ARE_ToolItem_ListNumber();
        IARE_ToolItem listBullet = new ARE_ToolItem_ListBullet();
        IARE_ToolItem hr = new ARE_ToolItem_Hr();
        IARE_ToolItem link = new ARE_ToolItem_Link();
//        IARE_ToolItem subscript = new ARE_ToolItem_Subscript();
//        IARE_ToolItem superscript = new ARE_ToolItem_Superscript();
        IARE_ToolItem left = new ARE_ToolItem_AlignmentLeft();
        IARE_ToolItem center = new ARE_ToolItem_AlignmentCenter();
        IARE_ToolItem right = new ARE_ToolItem_AlignmentRight();
        image = new ARE_ToolItem_Image(() -> {
            //
            checkStoragePermission(CHOOSE_TYPE_IMAGE, CHOOSE_CONTENT_IMAGE);
        });
//        IARE_ToolItem video = new ARE_ToolItem_Video();
//        IARE_ToolItem at = new ARE_ToolItem_At();

        mToolbar.addToolbarItem(image);
        mToolbar.addToolbarItem(bold);
        mToolbar.addToolbarItem(italic);
        mToolbar.addToolbarItem(underline);
        mToolbar.addToolbarItem(strikethrough);
        mToolbar.addToolbarItem(fontSize);
        mToolbar.addToolbarItem(fontColor);
        mToolbar.addToolbarItem(backgroundColor);
        mToolbar.addToolbarItem(quote);
//        mToolbar.addToolbarItem(listNumber);
        mToolbar.addToolbarItem(listBullet);
        mToolbar.addToolbarItem(hr);
        mToolbar.addToolbarItem(link);
//        mToolbar.addToolbarItem(subscript);
//        mToolbar.addToolbarItem(superscript);
        mToolbar.addToolbarItem(left);
        mToolbar.addToolbarItem(center);
        mToolbar.addToolbarItem(right);
//        mToolbar.addToolbarItem(video);
//        mToolbar.addToolbarItem(at);

        mEditText = this.findViewById(R.id.arEditText);
        mEditText.setToolbar(mToolbar);
        mEditText.setImageStrategy(imageStrategy);
    }

    private void checkStoragePermission(int chooseType, int resultBackType) {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                chooseImageOrVideo(chooseType, resultBackType);
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    private void chooseImageOrVideo(int chooseType, int resultBackType) {
        PictureSelector.create(mActivity)
                .openGallery(chooseType)// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(true)// 是否可预览视频
                .withAspectRatio(3, 2)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .videoMaxSecond(YXConfig.VIDEO_SELECT_MAX_SECOND)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(false)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(resultBackType);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        if (null == publishNewsForm) {
            publishNewsForm = new PublishNewsForm();
        }

        basePresenter.getStsToken();

        //显示选择的话题
        pickedTopics = new ArrayList<>();
        tagAdapterHistory = new TagAdapter<TopicModel>(pickedTopics) {
            @Override
            public View getView(FlowLayout parent, int position, TopicModel tagItem) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_tag_flow_topic_publish, parent, false);
                //获取标签
                TextView tvTagName = view.findViewById(R.id.tvTagName);
                tvTagName.setText("#" + tagItem.getTitle() + "#");

                AliIconFontTextView iconDelete = view.findViewById(R.id.iconDelete);
                iconDelete.setOnClickListener(v -> {
                    pickedTopics.remove(position);
                    tflHistory.getAdapter().refreshData(pickedTopics);
                });
                return view;
            }
        };

        //控制显示5个
        tflHistory.setAdapter(tagAdapterHistory);
    }

    private String mCityName;
    private String issueAddressDetail;
    private double mLatitude;
    private double mLongitude;
    private String mPoiName;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mToolbar.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_CONTENT_IMAGE && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();

            String path;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }

            ARE_Style_Image imageStyle = (ARE_Style_Image) image.getStyle();
            Uri uri = Uri.parse(path);
            ImageStrategy imageStrategy = image.getEditText().getImageStrategy();
            if (imageStrategy != null) {
                imageStrategy.uploadAndInsertImage(uri, imageStyle);
                return;
            }
        } else if (requestCode == CHOOSE_COVER_IMAGE && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);
            //裁剪路径
            String cutPath = localMedia.getCutPath();
            String realPath = localMedia.getRealPath();

            String path;
            //如果裁剪路径是空的，那么使用真是路径
            if (TextUtils.isEmpty(cutPath)) {
                path = realPath;
            } else {
                path = cutPath;
            }

            uploadFile(new File(path), CHOOSE_COVER_IMAGE);
        } else if (requestCode == CHOOSE_COVER_VIDEO && resultCode == RESULT_OK) {
            List<LocalMedia> result = PictureSelector.obtainMultipleResult(data);
            LocalMedia localMedia = result.get(0);

            File videoFile = new File(localMedia.getRealPath());
            uploadFile(videoFile, CHOOSE_COVER_VIDEO);

            videoTime = FileUtils.getLocalVideoDuration(localMedia.getPath());
        } else if (requestCode == SELECT_ADDRESS && resultCode == RESULT_OK) {
            if (data != null) {
                PoiItem poiItem = data.getParcelableExtra("poiItem");
                if (poiItem != null) {
                    mPoiName = poiItem.getTitle();
                    issueAddressDetail = poiItem.getSnippet();//详细地址
                    mCityName = poiItem.getCityName();
                    LatLonPoint latLonPoint = poiItem.getLatLonPoint();
                    mLatitude = latLonPoint.getLatitude();
                    mLongitude = latLonPoint.getLongitude();

                    publishNewsForm.setIssueLatitude(String.valueOf(mLatitude));
                    publishNewsForm.setIssueLongitude(String.valueOf(mLongitude));
                    publishNewsForm.setIssueAddress(mPoiName);
                    publishNewsForm.setIssueCity(mCityName);
                }

                tvAddAddress.setSelected(true);
            } else {
                mPoiName = null;
                issueAddressDetail = null;
                mCityName = null;
                mLatitude = 0;
                mLongitude = 0;
            }

            String address = !TextUtils.isEmpty(mPoiName) ? mPoiName : "请选择发布关联的酒吧/位置";
            tvAddAddress.setText(address);
        }
    }

    private void setTitleBack(int type, String path) {
        etTitle.setTextColor(mContext.getResources().getColor(R.color.white));
        etTitle.setHintTextColor(mContext.getResources().getColor(R.color.white));

        if (type == CHOOSE_COVER_IMAGE) {
            Glide.with(mContext)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picHeight = resource.getHeight();
                            int picWidth = resource.getWidth();

                            float rate = DisplayUtils.getImageRate(picWidth, picHeight);//4:3 1:1 3:4

                            int width = DisplayUtils.getScreenWidth(mContext);
                            int height = (int) (width / rate);

                            ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
                            imageLayoutParams.width = width;
                            imageLayoutParams.height = height;
                            uploadContainer.setLayoutParams(imageLayoutParams);

                            ivUpload.setImageBitmap(resource);
                        }
                    });
            ivVideoPlay.setVisibility(View.GONE);
        } else {
            RequestOptions options = new RequestOptions();

            options.skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .frame(1000000)
                    .centerCrop();

            Glide.with(mContext)
                    .setDefaultRequestOptions(options)
                    .asBitmap()
                    .load(path)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            int picHeight = resource.getHeight();
                            int picWidth = resource.getWidth();

                            float rate = DisplayUtils.getImageRate(picWidth, picHeight);//4:3 1:1 3:4

                            int width = DisplayUtils.getScreenWidth(mContext);
                            int height = (int) (width / rate);

                            ViewGroup.LayoutParams imageLayoutParams = uploadContainer.getLayoutParams();
                            imageLayoutParams.width = width;
                            imageLayoutParams.height = height;
                            uploadContainer.setLayoutParams(imageLayoutParams);

                            ivUpload.setImageBitmap(resource);

                            String newPath = System.currentTimeMillis() + ".jpg";
                            new AsyncTask<Void, Integer, String>() {//Params, Progress, Result
                                @Override
                                protected String doInBackground(Void... voids) {
                                    String filePath = FileUtils.saveImageBitmap(newPath, resource, mContext);

                                    return filePath;
                                }

                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                }

                                @Override
                                protected void onPostExecute(String filePath) {
                                    uploadFile(new File(filePath), -1);//视频封面
                                }
                            }.execute();
                        }
                    });

            ivVideoPlay.setVisibility(View.VISIBLE);
        }

        llTitle.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
        llSmallAddCover.setVisibility(View.VISIBLE);
//        rlTitleContainer.setBackgroundResource(R.mipmap.bg_publish_news);
        rlTitleContainer.setBackgroundResource(R.drawable.bg_publish);
        viewTitle.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
        llAddCover.setVisibility(View.GONE);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    private StsTokenModel stsTokenModel;

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    private void uploadFile(File file, int retype) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(new Date()) + System.currentTimeMillis() + fileName.substring(index);

        OssManagerUtil.getInstance().uploadFile(mContext, stsTokenModel, objectKey, file.getPath(), new OSSPushListener() {
            @Override
            public void onProgress(long currentSize, long totalSize) {
                // 这里不用关注进度
            }

            @Override
            public void onSuccess(PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();

                //上传完毕后提交内容
                Message msg = Message.obtain();
                msg.what = retype;
                if (retype == -1) {
                    videoCoverUrl = fileResult.getHttpUrl();

                    msg.obj = videoCoverUrl;
                } else {
                    uploadUrl = fileResult.getHttpUrl();
                    msg.obj = uploadUrl;
                }

                mHandler.sendMessage(msg);
            }

            @Override
            public void onFailure() {
                Message msg = Message.obtain();
                msg.what = UPLOAD_FILE_FAIL;
                mHandler.sendMessage(msg);
            }
        });
    }

    @Override
    public void doBusiness() {
        super.doBusiness();

        setViewData();
        //
        etTitle.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));
        mEditText.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String title = etTitle.getText().toString().trim();
        String html = this.mEditText.getHtml();

        btnPublish.setEnabled(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(html) && !TextUtils.isEmpty(uploadUrl));

        if (null != publishNewsForm) {
            publishNewsForm.setContentTitle(title);
            publishNewsForm.setContent(html);
        }
    }

    private MyHandler mHandler = new MyHandler();
    private String uploadUrl;
    private String videoCoverUrl;
    private int videoTime;

    private final static int UPLOAD_FILE_FAIL = 1000;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case CHOOSE_COVER_IMAGE:
                    videoCoverUrl = null;
                    videoTime = 0;
                    setTitleBack(CHOOSE_COVER_IMAGE, uploadUrl);
                    addListenEdit();
                    break;
                case CHOOSE_COVER_VIDEO:
                    setTitleBack(CHOOSE_COVER_VIDEO, uploadUrl);
                    addListenEdit();
                    break;
                case UPLOAD_FILE_FAIL:
                    ToastUtils.showShort("封面上传失败");
                    break;
            }
        }
    }

    @Override
    public void handPublishNewsResult(PublishResultModel model) {
        if (model != null) {
            int auditStatus = model.getAuditStatus();
            if (auditStatus == 0) {
                ToastUtils.showShort("发布成功，等待管理员审核");
            } else {
                ToastUtils.showShort("发布成功");
            }
        }

        publishNewsForm = null;

        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (null == publishNewsForm || null == publishNewsForm.getContentTitle()) {
            PublishTemp.deleteStrategyTemp();
            super.onBackPressed();
            return;
        }

        CommonDialog commonDialog = new CommonDialog(mContext, "将此次编辑保留草稿？", "保留", "不保留");
        new XPopup.Builder(mContext).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(() -> {
            commonDialog.dismiss();
//            saveToDraft

            publishNewsForm.setIssueLatitude(String.valueOf(mLatitude));
            publishNewsForm.setIssueLongitude(String.valueOf(mLongitude));
            publishNewsForm.setIssueAddress(mPoiName);
            publishNewsForm.setIssueCity(mCityName);

            publishNewsForm.setCoverUrl(uploadUrl);

            publishNewsForm.setContentCategoryId(null);
            publishNewsForm.setActivityEndTime(null);
            publishNewsForm.setActivityStartTime(null);
            publishNewsForm.setKeyword(null);
            publishNewsForm.setPrePublishTime(null);
            publishNewsForm.setVedioCoverUrl(null);

            PublishTemp.saveStrategyTemp(publishNewsForm);

            super.onBackPressed();
        });
        commonDialog.setOnCancelListener(() -> {
            commonDialog.dismiss();

            PublishTemp.deleteStrategyTemp();

            super.onBackPressed();
        });
    }

    private void setViewData() {
        if (null != publishNewsForm) {
            etTitle.setText(YHStringUtils.value(publishNewsForm.getContentTitle()));
            etGuideTitle.setText(YHStringUtils.value(publishNewsForm.getIntroduction()));
            mEditText.fromHtml(YHStringUtils.value(publishNewsForm.getContent()));

            uploadUrl = publishNewsForm.getCoverUrl();
            if (!TextUtils.isEmpty(uploadUrl)) {
                boolean isVideo = FileUtils.isVideo(uploadUrl);

                setTitleBack(isVideo ? CHOOSE_COVER_VIDEO : CHOOSE_COVER_IMAGE, uploadUrl);
            }

            String poiName = publishNewsForm.getIssueAddress();
            String address = !TextUtils.isEmpty(poiName) ? poiName : "请选择发布关联的酒吧/位置";
            tvAddAddress.setText(address);
        }
    }
}
