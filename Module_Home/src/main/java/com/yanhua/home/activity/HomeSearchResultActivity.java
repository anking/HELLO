package com.yanhua.home.activity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.UserModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.StaggeredItemDecoration;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.HomeSearchResultAdapter;
import com.yanhua.home.adapter.UserAdapter;
import com.yanhua.home.presenter.HomePresenter;
import com.yanhua.home.presenter.contract.HomeContract;
import com.yanhua.home.view.HomeTimeFilterPopup;
import com.yanhua.home.view.HomeZHFilterPopup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.HOME_SEARCH_RESULT_ACTIVITY)
public class HomeSearchResultActivity extends BaseMvpActivity<HomePresenter> implements HomeContract.IView {

    @BindView(R2.id.ctl)
    CommonTabLayout ctl;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.llContent)
    LinearLayout llContent;
    @BindView(R2.id.refreshLayout1)
    SmartRefreshLayout refreshLayout1;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rvContent)
    RecyclerView rvContent;
    @BindView(R2.id.rvUser)
    RecyclerView rvUser;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyView;
    @BindView(R2.id.llZHFilter)
    LinearLayout llZHFilter;
    @BindView(R2.id.llTimeFilter)
    LinearLayout llTimeFilter;
    @BindView(R2.id.llFilter)
    LinearLayout llFilter;
    @BindView(R2.id.tvTimeFilter)
    TextView tvTimeFilter;
    @BindView(R2.id.tvZHFilter)
    TextView tvZHFilter;

    @Autowired
    String keyWord;

    private int userTotal;
    private List<UserModel> userList;
    private UserAdapter userAdapter;
    private int currentPos;

    private int contentTotal;
    private List<HomeSearchResultModel> contentList;
    private HomeSearchResultAdapter contentAdapter;

    private int publishTimeType;
    private int synthesizeSortType;

    private int currentPosition;

    @Override
    protected void creatPresent() {
        basePresenter = new HomePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_home_search_result;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        etSearch.setText(keyWord);
        userList = new ArrayList<>();
        contentList = new ArrayList<>();
        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("综合", 0, 0));
        tabEntityList.add(new TabEntity("用户", 0, 0));
        ctl.setTabData(tabEntityList);
        ctl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    llContent.setVisibility(View.VISIBLE);
                    refreshLayout1.setVisibility(View.GONE);
                    refreshLayout.autoRefresh(100);
                } else if (position == 1) {
                    llContent.setVisibility(View.GONE);
                    refreshLayout1.setVisibility(View.VISIBLE);
                    refreshLayout1.autoRefresh(100);
                }
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
        ctl.setCurrentTab(0);
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                keyWord = etSearch.getText().toString().trim();
                int currentTab = ctl.getCurrentTab();
                if (currentTab == 0) {
                    // 综合搜索
                    getContentList();
                } else if (currentTab == 1) {
                    // 搜索用户
                    getUserList();
                }
                return true;
            }
            return false;
        });
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getContentList();
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (contentList.size() < contentTotal) {
                current++;
                getContentList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        refreshLayout1.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getUserList();
        });
        refreshLayout1.setOnLoadMoreListener(refreshLayout -> {
            if (userList.size() < userTotal) {
                current++;
                getUserList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        rvUser.setLayoutManager(new LinearLayoutManager(this));
        userAdapter = new UserAdapter(this);
        rvUser.setAdapter(userAdapter);
        userAdapter.setOnItemCellClickListener((pos, cellType) -> {
            UserModel userModel = userList.get(pos);
            if (cellType == 0) {
                // 跳个人主页
                ARouter.getInstance()
                        .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userModel.getId())
                        .navigation();
            } else if (cellType == 1) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    currentPos = pos;
                    int relationState = userModel.getRelationState();
                    if (relationState == 1 || relationState == 3) {
                        // 关注
                        basePresenter.followUser(userModel.getId());
                    } else if (relationState == 2 || relationState == 4) {
                        // 取关
                        AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                        new XPopup.Builder(mContext).asCustom(alertPopup).show();
                        alertPopup.setOnConfirmListener(() -> {
                            alertPopup.dismiss();
                            basePresenter.cancelFollowUser(userModel.getId());
                        });
                        alertPopup.setOnCancelListener(alertPopup::dismiss);
                    }
                });
            }
        });

        rvContent.setLayoutManager(new LinearLayoutManager(this));
        contentAdapter = new HomeSearchResultAdapter(this);
        rvContent.addItemDecoration(new StaggeredItemDecoration(this, 0, 8, 0, 0));
        rvContent.setAdapter(contentAdapter);
        contentAdapter.setOnHandleClickListener(new HomeSearchResultAdapter.OnHandleClickListener() {
            @Override
            public void onHandleClick(int type, MomentListModel item, int pos) {
                currentPosition = pos;
                String id = item.getId();
                int contentType = item.getContentType();
                switch (type) {
                    case HomeSearchResultAdapter.OnHandleClickListener.FOLLOW://关注
                        PageJumpUtil.firstIsLoginThenJump(() -> {
                            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
                            //    private Integer followStatus;
                            int followStatus = item.getFollowStatus();
                            boolean isFollow = DiscoValueFormat.isFollow(followStatus);
                            String userId = item.getUserId();
                            if (isFollow) {
                                AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                                alertPopup.setOnConfirmListener(() -> {
                                    alertPopup.dismiss();
                                    basePresenter.cancelFollowContentUser(userId);
                                });
                                alertPopup.setOnCancelListener(alertPopup::dismiss);
                            } else {
                                basePresenter.followContentUser(userId);
                            }
                        });
                        break;
                    case HomeSearchResultAdapter.OnHandleClickListener.COLLECT://收藏
                        PageJumpUtil.firstIsLoginThenJump(() -> {
                            basePresenter.updateCollectContent(id, 1);
                        });
                        break;
                    case HomeSearchResultAdapter.OnHandleClickListener.COMMENT://评论
                    case HomeSearchResultAdapter.OnHandleClickListener.TO_DETAIL://详情
                        //跳转到详情
                        if (contentType == 2) {
                            ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                                    .withSerializable("contentModel", item)
                                    .withString("categoryId", "")
                                    .withString("city", "")
                                    .withString("fromClassName", className)
                                    .withInt("module", 0)
                                    .navigation();
                        } else {
                            ARouter.getInstance().build(ARouterPath.CONTENT_DETAIL_ACTIVITY)
                                    .withString("id", id)
                                    .withString("fromClassName", className)
                                    .navigation();
                        }
                        break;
                    case HomeSearchResultAdapter.OnHandleClickListener.THUMB_UP://点赞
                        PageJumpUtil.firstIsLoginThenJump(() -> {
                            basePresenter.updateStarContent(id, 1);
                        });
                        break;
                }
            }

            @Override
            public void onHandleClick(int type, UserModel item, int pos) {
                currentPosition = pos;
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
                    //    private Integer followStatus;
                    int followStatus = item.getRelationState();
                    boolean isFollow = DiscoValueFormat.isFollow(followStatus);
                    String userId = item.getId();
                    if (isFollow) {
                        AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                        new XPopup.Builder(mContext).asCustom(alertPopup).show();
                        alertPopup.setOnConfirmListener(() -> {
                            alertPopup.dismiss();
                            basePresenter.cancelFollowZHUser(userId);
                        });
                        alertPopup.setOnCancelListener(alertPopup::dismiss);
                    } else {
                        basePresenter.followZHUser(userId);
                    }
                });
            }
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        refreshLayout.autoRefresh(100);
    }

    private void getContentList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        params.put("keyword", keyWord);
        params.put("publishTimeType", publishTimeType);
        params.put("synthesizeSortType", synthesizeSortType);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", UserManager.getInstance().getUserId());
        }
        basePresenter.homeSearch(params);
    }

    private void getUserList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        params.put("keyword", keyWord);
        if (UserManager.getInstance().isLogin()) {
            params.put("userId", UserManager.getInstance().getUserId());
        }
        basePresenter.userSearch(params);
    }

    @OnClick({R2.id.tvCancel, R2.id.llZHFilter, R2.id.llTimeFilter})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.tvCancel) {
            hideSoftKeyboard();
            onBackPressed();
        } else if (id == R.id.llZHFilter) {
            HomeZHFilterPopup zhFilterPopup = new HomeZHFilterPopup(this, synthesizeSortType);
            zhFilterPopup.setOnSelectTimeListener(new HomeZHFilterPopup.OnSelectTimeListener() {
                @Override
                public void selectTime(int pos, String time) {
                    tvZHFilter.setText(time);
                    synthesizeSortType = pos;
                    llZHFilter.setSelected(true);
                    current = 1;
                    getContentList();
                }
            });
            new XPopup.Builder(this).atView(llFilter).asCustom(zhFilterPopup).show();
        } else if (id == R.id.llTimeFilter) {
            HomeTimeFilterPopup timeFilterPopup = new HomeTimeFilterPopup(this, publishTimeType);
            timeFilterPopup.setOnSelectTimeListener((pos, time) -> {
                tvTimeFilter.setText(time);
                publishTimeType = pos;
                llTimeFilter.setSelected(true);
                current = 1;
                getContentList();
            });
            new XPopup.Builder(this).atView(llFilter).asCustom(timeFilterPopup).show();
        }
    }

    @Override
    public void handleUserSearchSuccess(ListResult<UserModel> result) {
        if (current == 1) {
            refreshLayout1.finishRefresh();
            userList.clear();
        } else {
            refreshLayout1.finishLoadMore();
        }
        if (result != null) {
            userTotal = result.getTotal();
            List<UserModel> records = result.getRecords();
            if (records != null && !records.isEmpty()) {
                userList.addAll(records);
            }
        }
        if (userList.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
            refreshLayout1.setVisibility(View.GONE);
        } else {
            refreshLayout1.setVisibility(View.VISIBLE);
            userAdapter.setItems(userList);
        }
    }

    @Override
    public void handleHomeSearchSuccess(ListResult<HomeSearchResultModel> result) {
        if (current == 1) {
            refreshLayout.finishRefresh();
            contentList.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (result != null) {
            contentTotal = result.getTotal();
            List<HomeSearchResultModel> records = result.getRecords();
            if (records != null && !records.isEmpty()) {
                contentList.addAll(records);
            }
        }
        if (contentList.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
            llContent.setVisibility(View.GONE);
        } else {
            llContent.setVisibility(View.VISIBLE);
            contentAdapter.setItems(contentList);
        }
    }

    @Override
    public void updateFollowUserSuccess(boolean follow) {
        UserModel userModel = userList.get(currentPos);
        int relationState = userModel.getRelationState();
        if (follow) {
            if (relationState == 1) {
                userModel.setRelationState(2);
            } else if (relationState == 3) {
                userModel.setRelationState(4);
            }
        } else {
            if (relationState == 2) {
                userModel.setRelationState(1);
            } else if (relationState == 4) {
                userModel.setRelationState(3);
            }
        }
        userAdapter.notifyItemChanged(currentPos);
    }

    @Override
    public void updateCollectContentSuccess() {
        HomeSearchResultModel result = contentList.get(currentPosition);
        MomentListModel model = result.getContent();
        int collectCount = model.getCollectCount();
        model.setCollect(!model.isCollect());
        model.setCollectCount(model.isCollect() ? collectCount + 1 : (collectCount > 0 ? collectCount - 1 : 0));
        contentAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowUserSuccess() {
        //此处需要更新列表中该人的关注状态
        HomeSearchResultModel result = contentList.get(currentPosition);
        MomentListModel model = result.getContent();
        //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
        //    private Integer followStatus;
        int followStatus = model.getFollowStatus();

        boolean isFollow = DiscoValueFormat.isFollow(followStatus);
        int changeResult = isFollow ? (followStatus - 1) : (followStatus + 1);

        for (HomeSearchResultModel obj : contentList) {
            if (obj.getType() == 4) {
                MomentListModel content = obj.getContent();
                String userId = content.getUserId();
                if (null != userId && userId.equals(model.getUserId())) {
                    content.setFollowStatus(changeResult);
                }
            }
        }
        contentAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateStarContentSuccess() {
        HomeSearchResultModel result = contentList.get(currentPosition);
        MomentListModel model = result.getContent();
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        contentAdapter.notifyItemChanged(currentPosition);
    }

    @Override
    public void updateFollowZHUserSuccess(boolean follow) {
        HomeSearchResultModel result = contentList.get(currentPosition);
        UserModel userModel = result.getUser();
        int relationState = userModel.getRelationState();
        if (follow) {
            if (relationState == 1) {
                userModel.setRelationState(2);
            } else if (relationState == 3) {
                userModel.setRelationState(4);
            }
        } else {
            if (relationState == 2) {
                userModel.setRelationState(1);
            } else if (relationState == 4) {
                userModel.setRelationState(3);
            }
        }
        contentAdapter.notifyItemChanged(currentPosition);
    }
}