package com.yanhua.home.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.flyco.tablayout.SlidingTabLayout;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.SelectPartShadowPopupView;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BreakNewsAdapter;
import com.yanhua.home.adapter.BreakNewsBannerAdapter;
import com.yanhua.home.adapter.BreakNewsTopAdapter;
import com.yanhua.home.fragment.BreakNewsFragment;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.listener.OnPageChangeListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;

@Route(path = ARouterPath.BREAK_NEWS_ACTIVITY)
public class BreakNewsActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.refreshSearch)
    SmartRefreshLayout refreshSearch;
    @BindView(R2.id.rv_result)
    RecyclerView rvResult;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyView;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.iconShowMore)
    TextView iconShowMore;
    @BindView(R2.id.bottomLine)
    View bottomLine;

    @BindView(R2.id.flImages)
    FrameLayout flImages;
    @BindView(R2.id.tvDay)
    TextView tvDay;
    @BindView(R2.id.tvMonth)
    TextView tvMonth;
    @BindView(R2.id.tvYear)
    TextView tvYear;

    @BindView(R2.id.stabLayout)
    RelativeLayout stabLayout;
    @BindView(R2.id.llMore)
    LinearLayout llMore;
    @BindView(R2.id.bannerTop)
    Banner bannerTop;
    @BindView(R2.id.rvIndex)
    RecyclerView rvIndex;
    @BindView(R2.id.bgBluer)
    ImageView bgBluer;

    @BindView(R2.id.tvAddress)
    TextView tvAddress;
    @BindView(R2.id.clayout)
    CoordinatorLayout clayout;

    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;
    @BindView(R2.id.tv_search_cancle)
    TextView tvSearchCancle;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    private BreakNewsAdapter adapter;
    private List<BreakNewsListModel> mListData;
    private List<SelectConditionModel> mCityList;
    private List<NewsTypeModel> mChannelList;
    private ChannelPageAdapter mAdapter;
    private ArrayList<Fragment> mFragments;
    private SelectConditionModel selectCondition;
    private SelectPartShadowPopupView conditionPopupView;
    private int total;

    @Autowired
    String id;

    @Override
    public int bindLayout() {
        return R.layout.activity_break_news;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, llMore);

        refreshLayout.setOnRefreshListener(rl -> {
            rl.finishRefresh(1000);

            refreshData();
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);

        refreshSearch.setOnRefreshListener(refreshLayout -> {
            current = 1;
            String keyword = etSearch.getText().toString().trim();
            getData(keyword);
        });

        refreshSearch.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                String keyword = etSearch.getText().toString().trim();
                getData(keyword);
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });

        setTopView();

        initBanner();

        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                String keyword = etSearch.getText().toString().trim();
                getData(keyword);
                return true;
            }
            return false;
        });
        mListData = new ArrayList<>();
        rvResult.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BreakNewsAdapter(this);
        rvResult.setAdapter(adapter);
        adapter.setItems(mListData);
        adapter.setOnItemClickListener((itemView, pos) -> {
            BreakNewsListModel clickItem = adapter.getItemObject(pos);
            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_BREAK_NEWS);
        });
    }

    private void getData(String keyword) {
        if (null != basePresenter) {
            current = 1;
            basePresenter.cityContentList(current, size, "", keyword, id, YXConfig.TYPE_BREAK_NEWS);
        }
    }

    /**
     * 刷新数据
     */
    private void refreshData() {
        //需要更新当前tab的数据MomentCHANELFragment
        if (null != mAdapter && null != mChannelList && mChannelList.size() > 0) {
            int curPos = sltabChannel.getCurrentTab();

            Fragment fragment = mFragments.get(curPos);

            if (fragment instanceof BreakNewsFragment) {
                String cityName = "";
                if (null != selectCondition) {
                    cityName = selectCondition.getFieldValue();
                }

                ((BreakNewsFragment) fragment).getListData(1, cityName);
            }
        }
    }


    @OnClick({R2.id.llDateList, R2.id.tvPublish, R2.id.llToInvite, R2.id.ll_to_search, R2.id.tv_search_cancle})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.llDateList) {
            ARouter.getInstance().build(ARouterPath.ACT_CALENDAR_ACTIVITY).navigation();
        } else if (id == R.id.tvPublish) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                //发布爆料
                PublishNewsForm publishNewsForm = PublishTemp.getBreakNewTemp(mContext);

                ARouter.getInstance().build(ARouterPath.PUBLISH_BREAK_NEWS_ACTIVITY)
                        .withSerializable("publishNewsForm", publishNewsForm).navigation();
            });
        } else if (id == R.id.llToInvite) {
            //去约伴
            ARouter.getInstance().build(ARouterPath.INVITE_LIST_ACTIVITY).navigation();
        } else if (id == R.id.ll_to_search) {
            rlSearch.setVisibility(View.VISIBLE);
            etSearch.setFocusable(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.requestFocus();
        } else if (id == R.id.tv_search_cancle) {
            hideSoftKeyboard();
            rlSearch.setVisibility(View.GONE);
            etSearch.setText("");
            refreshSearch.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
        }
    }

    private ArrayList<BreakNewsRecommendModel> topicList;
    private BreakNewsBannerAdapter mTopAdapter;
    private BreakNewsTopAdapter topAdapter;

    /**
     *
     */
    private void initBanner() {
        topicList = new ArrayList<>();
        mTopAdapter = new BreakNewsBannerAdapter(topicList, mContext);

        //展示滚动效果
        bannerTop.setAdapter(mTopAdapter).addBannerLifecycleObserver(this)
                .setLoopTime(3000)
                .setOnBannerListener(new OnBannerListener() {
                    @Override
                    public void OnBannerClick(Object data, int position) {
                        BreakNewsRecommendModel clickItem = (BreakNewsRecommendModel) data;

                        PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_BREAK_NEWS);
                    }
                })
                .isAutoLoop(true);

        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        topAdapter = new BreakNewsTopAdapter(mContext, pos -> {
            bannerTop.setCurrentItem(pos + 1);
        });

        rvIndex.setLayoutManager(manager);
        rvIndex.setAdapter(topAdapter);

        //干掉 取出上拉下拉阴影
        rvIndex.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvIndex);

        bannerTop.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < topicList.size(); i++) {
                    BreakNewsRecommendModel sortModel = topicList.get(i);

                    if (i == position) {
                        sortModel.setSelect(true);
                    } else {
                        sortModel.setSelect(false);
                    }

                    topAdapter.notifyDataSetChanged();
                    rvIndex.scrollToPosition(position);
                }

                blurTopBg(topicList.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void blurTopBg(BreakNewsRecommendModel item) {
        if (null == item) {
            return;
        }

        String cover = "";
        if (!TextUtils.isEmpty(item.getVideoCoverUrl())) {
            cover = item.getVideoCoverUrl();
        } else {
            cover = item.getShowUrl();
        }

        Glide.with(mContext)
                .load(cover)
                .dontAnimate()
                .error(R.drawable.place_holder)                    // 设置高斯模糊
                .transform(new BlurTransformation(10, 20))
                .into(bgBluer);
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);
            if (alp >= 0 && alp < 168) {
                bottomLine.setBackgroundColor(mContext.getResources().getColor(R.color.color_f9faf9));
                stabLayout.setBackgroundColor(mContext.getResources().getColor(R.color.color_f9faf9));
            } else if (alp >= 168 && alp <= 255) {
                bottomLine.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                stabLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        tvDay.setText(String.valueOf(day));
        tvMonth.setText(YHStringUtils.monthShortName(month, true));
        tvYear.setText(String.valueOf(year));

        mCityList = new ArrayList<>();
        mChannelList = new ArrayList<>();

        String city = "";
        String locationCity = DiscoCacheUtils.getInstance().getCurrentCity();
        if (TextUtils.isEmpty(locationCity)) {
            city = YXConfig.city;
        } else {
            city = locationCity;
        }

        //// queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
        //        // type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7:酒吧新闻
        //        public static final String BREAK_NEWS_RECOMMEND = "sysadmin/news/content/recommend";
        basePresenter.getNewsRecommendList(3, 1, 1, 5, city, "");//最新爆料置顶请求位置
//        获取城市
        //app 查询有活动的城市 GET
//        public static final String BREAK_NEWS_ACTIVITY_CITY_LIST = "sysadmin/news/content/recentActiveCityList";
        basePresenter.recentActiveCityList("", YXConfig.TYPE_BREAK_NEWS,1);
        //获取分类 --public static final String BREAK_NEWS_TYPE_LIST = "sysadmin/newsContentCategory/list/{type}";
        basePresenter.getNewsTypeList(YXConfig.TYPE_BREAK_NEWS, 0);
    }

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mFragments = new ArrayList<>();

            for (int i = 0; i < mChannelList.size(); i++) {
                NewsTypeModel model = mChannelList.get(i);

                Fragment fragment = new BreakNewsFragment();
                Bundle bundle = new Bundle();
                String name = model.getReportName();
                String id = model.getId();

                bundle.putString("id", id);
                bundle.putString("name", name);
                fragment.setArguments(bundle);

                mFragments.add(fragment);
            }

            mAdapter = new ChannelPageAdapter(getSupportFragmentManager());
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {

        public ChannelPageAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mChannelList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            NewsTypeModel model = mChannelList.get(position);
            String fieldValue = model.getReportName();
            return fieldValue;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.llMore) {
            showConditionPopup();
        }
    }

    public void showConditionPopup() {
        if (conditionPopupView == null) {
            if (null == selectCondition) {
                //默认取第一个作为默认值
                selectCondition = mCityList.get(0);
                setFilterAddress();
            }

            conditionPopupView = new SelectPartShadowPopupView(this, mCityList, selectCondition, 0, R.drawable.shape_bg_white);
            conditionPopupView.setOnSelectTypeListener((item, pos) -> {
                setConditionText(true, item, pos);
            });
        }

        scrollToTop();

        clayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                new XPopup.Builder(mContext).atView(stabLayout).asCustom(conditionPopupView).show();
                iconShowMore.setText("\ue739");
            }
        }, 200);
    }


    private void scrollToTop() {
        CoordinatorLayout.Behavior behavior =
                ((CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams()).getBehavior();
        if (behavior instanceof AppBarLayout.Behavior) {
            AppBarLayout.Behavior appBarLayoutBehavior = (AppBarLayout.Behavior) behavior;
            int height = mAppBarLayout.getHeight();
            appBarLayoutBehavior.setTopAndBottomOffset(-height + stabLayout.getHeight());
            mAppBarLayout.requestLayout();
        }
    }

    private void setConditionText(boolean select, SelectConditionModel selectObj, int pos) {
        if (null == selectCondition) {
            selectCondition = mCityList.get(0);
        } else {
            selectCondition = selectObj;
        }
        setFilterAddress();

        iconShowMore.setText("\ue750");

        if (select) {
            refreshData();
        }
    }

    @Override
    public void handleCityList(List<String> cityList) {
        if (null != cityList) {
            SelectConditionModel suggest = new SelectConditionModel();
            suggest.setFieldName("城市");
            suggest.setFieldValue("全部");

            mCityList.add(suggest);

            selectCondition = suggest;
            setFilterAddress();

            for (String city : cityList) {
                SelectConditionModel item = new SelectConditionModel();
                item.setFieldName("城市");
                item.setFieldValue(city);

                mCityList.add(item);
            }


        }
    }

    private void setFilterAddress() {
        if (null != selectCondition) {
            tvAddress.setText(selectCondition.getFieldValue());
        }
    }

    @Override
    public void handleNewsTypeList(List<NewsTypeModel> data) {
        if (null != data && data.size() > 0) {
            NewsTypeModel suggest = new NewsTypeModel();
            suggest.setId("");
            suggest.setReportName("综合");

            mChannelList.add(suggest);
            mChannelList.addAll(data);

            initCategoryView();
        }
    }

    //顶部最新爆料数据展示
    @Override
    public void handleRecommendList(List<BreakNewsRecommendModel> data) {
        if (null != data && data.size() > 0) {
            flImages.setVisibility(View.VISIBLE);

            topicList.addAll(data);

            bannerTop.setDatas(topicList);
            topAdapter.setItems(topicList);//
            blurTopBg(data.get(0));
        } else {
            flImages.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleBreakNewList(ListResult<BreakNewsListModel> data) {
        if (data != null) {
            List<BreakNewsListModel> records = data.getRecords();
            if (records != null && !records.isEmpty()) {
                refreshSearch.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.VISIBLE);
            }
            total = data.getTotal();

            if (current == 1) {
                refreshSearch.finishRefresh();
                mListData.clear();
            } else {
                refreshSearch.finishLoadMore();
            }

            if (records != null && !records.isEmpty()) {
                mListData.addAll(records);
            }
            adapter.setItems(mListData);
        }
    }
}
