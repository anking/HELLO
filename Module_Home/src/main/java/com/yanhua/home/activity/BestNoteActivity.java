package com.yanhua.home.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.SelectPartShadowPopupView;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.fragment.HomeSuggestFragment;
import com.yanhua.home.presenter.BestNoteListPresenter;
import com.yanhua.home.presenter.contract.BestNoteListContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

@Route(path = ARouterPath.BEST_NOTE_ACTIVITY)
public class BestNoteActivity extends BaseMvpActivity<BestNoteListPresenter> implements BestNoteListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.toolbar)
    LinearLayout toolbar;
    @BindView(R2.id.rlTitle)
    RelativeLayout rlTitle;
    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;

    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;

    @BindView(R2.id.rlContent)
    RelativeLayout rlContent;

    @BindView(R2.id.tvTitle)
    TextView tvTitle;

    @BindView(R2.id.iconShowMore)
    TextView iconShowMore;

    @BindView(R2.id.stabLayout)
    RelativeLayout stabLayout;

    @BindView(R2.id.llMore)
    LinearLayout llMore;

    private List<SelectConditionModel> mChannelList;
    private ChannelPageAdapter mAdapter;
    private SelectConditionModel selectCondition;
    private SelectPartShadowPopupView conditionPopupView;
    @Autowired
    String id;


    @Override
    public int bindLayout() {
        return R.layout.activity_best_note;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, iconShowMore);

        refreshLayout.setOnRefreshListener(rl -> {
            rl.finishRefresh(1000);
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        setTopView();
    }

    /**
     * 计算顶部Bar的交互
     */
    private void setTopView() {
        iconMore.setVisibility(View.GONE);

        //监听滑动事件
        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            float alpha = (1 - rangle) * 255;
            int alp = (int) (alpha > 255 ? 255 : alpha);

            if (alp >= 0 && alp < 168) {
                rlTitle.setVisibility(View.GONE);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));

                rlContent.setBackgroundResource(R.drawable.bg_best_note_content);

                toolbar.setBackgroundColor(Color.argb(alp, 255, 255, 255));
            } else if (alp >= 168 && alp <= 255) {
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                rlTitle.setVisibility(View.VISIBLE);

                rlContent.setBackgroundResource(R.drawable.shape_bg_white);

                toolbar.setBackgroundColor(Color.argb(255, 255, 255, 255));
            }
//            toolbar.setBackgroundColor(Color.argb(alp, 255, 255, 255));
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        tvTitle.setText("精华日记");

        mChannelList = new ArrayList<>();

        //获取城市
        basePresenter.getBestNoteCityList();
    }

    private void initCategoryView() {
        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager(), mChannelList);
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);

            sltabChannel.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelect(int position) {

                    for (int i = 0; i < mChannelList.size(); i++) {
                        SelectConditionModel item = mChannelList.get(i);

                        if (i == position) {
                            item.setSelected(true);
                            selectCondition = item;
                        } else {
                            item.setSelected(false);
                        }
                    }

                    if (null != conditionPopupView) {
                        conditionPopupView.refreshData(position);
                    }
                }

                @Override
                public void onTabReselect(int position) {

                }
            });
        }
    }


    @Override
    protected void creatPresent() {
        basePresenter = new BestNoteListPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {
        private List<SelectConditionModel> mMenuList;

        public ChannelPageAdapter(@NonNull FragmentManager fm, List<SelectConditionModel> list) {
            super(fm);
            this.mMenuList = list;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            SelectConditionModel model = mMenuList.get(position);

            Fragment fragment = new HomeSuggestFragment();

            Bundle bundle = new Bundle();
            String city = model.getFieldValue();

            bundle.putInt("type", position);
            bundle.putString("city", city);

            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mMenuList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            SelectConditionModel model = mMenuList.get(position);
            String fieldValue = model.getFieldValue();

            String name = fieldValue;

            //除去最后的市
//            if (!TextUtils.isEmpty(fieldValue)) {
//                int indexOf = fieldValue.lastIndexOf("市");
//                if (indexOf > 0) {
//                    name = fieldValue.replace(fieldValue.charAt(fieldValue.length() - 1) + "", "");
//                }
//                return name;
//            } else {
//                name = "";
//            }

            return name;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.iconShowMore) {
            showConditionPopup();
        }
    }

    public void showConditionPopup() {
        if (conditionPopupView == null) {
            if (null == selectCondition) {
                //默认取第一个作为默认值
                selectCondition = mChannelList.get(0);
            }

            conditionPopupView = new SelectPartShadowPopupView(this, mChannelList, selectCondition, 4, R.drawable.shape_bg_white);
            conditionPopupView.setOnSelectTypeListener((item, pos) -> {
                setConditionText(true, item, pos);
            });
        }
        new XPopup.Builder(this).atView(stabLayout).asCustom(conditionPopupView).show();

        iconShowMore.setText("\ue739");
    }

    private void setConditionText(boolean select, SelectConditionModel selectObj, int pos) {
        if (null == selectCondition) {
            selectCondition = mChannelList.get(0);
        } else {
            selectCondition = selectObj;
        }
        iconShowMore.setText("\ue750");

        if (select) {
            sltabChannel.setCurrentTab(pos);
        }
    }

    @Override
    public void handBestNoteCityList(ValueContentModel data) {
        if (null != data) {
            List<SelectConditionModel> cityList = data.getValue_count();
            SelectConditionModel suggest = new SelectConditionModel();
            suggest.setFieldName("城市");
            suggest.setFieldValue("推荐");
            suggest.setValue(0);

            mChannelList.add(suggest);

            if (null != cityList && cityList.size() > 0) {
                for (SelectConditionModel city : cityList) {
                    String cityName = city.getFieldValue();
                    //避免后台返回空城
                    if (!TextUtils.isEmpty(cityName)) {
                        mChannelList.add(city);
                    }
                }
            }

            initCategoryView();
            llMore.setVisibility(mChannelList.size() > YXConfig.SHOW_ME_MIN ? View.VISIBLE : View.GONE);
        } else {
            llMore.setVisibility(View.GONE);
        }
    }
}
