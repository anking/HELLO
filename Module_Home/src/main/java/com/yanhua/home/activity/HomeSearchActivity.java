package com.yanhua.home.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.model.TabEntity;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.HomeSearchHistoryAdapter;
import com.yanhua.home.presenter.HomePresenter;
import com.yanhua.home.presenter.contract.HomeContract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.HOME_SEARCH_ACTIVITY)
public class HomeSearchActivity extends BaseMvpActivity<HomePresenter> implements HomeContract.IView {

    @BindView(R2.id.ctl)
    CommonTabLayout ctl;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_rank)
    RecyclerView rvRank;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.rv_search_records)
    RecyclerView rvSearchRecords;
    @BindView(R2.id.tvAllRecord)
    TextView tvAllRecord;
    @BindView(R2.id.tvCleanAllRecord)
    TextView tvCleanAllRecord;

    private HotTopicListAdapter hotTopicListAdapter;
    private HotCircleListAdapter hotCircleListAdapter;

    private String keyWord;
    private List<String> records;
    private HomeSearchHistoryAdapter historyAdapter;
    private boolean isExpand;// 搜索记录是否展开

    @Override
    protected void creatPresent() {
        basePresenter = new HomePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_home_search;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        ArrayList<CustomTabEntity> tabEntityList = new ArrayList<>();
        tabEntityList.add(new TabEntity("热门话题", 0, 0));
        tabEntityList.add(new TabEntity("热门圈子", 0, 0));
        ctl.setTabData(tabEntityList);
        ctl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    rvRank.setAdapter(hotTopicListAdapter);
                } else if (position == 1) {
                    rvRank.setAdapter(hotCircleListAdapter);
                }
                current = 1;
                getHotData(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });
        ctl.setCurrentTab(0);
        getHotData(0);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getHotData(ctl.getCurrentTab());
        });
        hotTopicListAdapter = new HotTopicListAdapter(this);
        hotCircleListAdapter = new HotCircleListAdapter(this);
        rvRank.setLayoutManager(new LinearLayoutManager(this));
        rvRank.setAdapter(hotTopicListAdapter);
        hotTopicListAdapter.setOnItemClickListener((itemView, pos) -> {
            List<TopicModel> list = hotTopicListAdapter.getmDataList();
            TopicModel item = list.get(pos);
            ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation();
        });
        hotCircleListAdapter.setOnItemClickListener((itemView, pos) -> {
            List<CircleModel> list = hotCircleListAdapter.getmDataList();
            CircleModel item = list.get(pos);
            ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                    .withString("id", item.getId()).navigation();
        });
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                keyWord = etSearch.getText().toString().trim();
                toSearch();
                return true;
            }
            return false;
        });
        records = new ArrayList<>();
        rvSearchRecords.setLayoutManager(new LinearLayoutManager(this));
        historyAdapter = new HomeSearchHistoryAdapter(this);
        rvSearchRecords.setAdapter(historyAdapter);
        historyAdapter.setItems(records);
        historyAdapter.setOnDeleteRecordListener(pos -> {
            // 区分展开和未展开的情况
            List<String> strings = historyAdapter.getmDataList();
            String s = strings.get(pos);
            String homeSearchHistory = DiscoCacheUtils.getInstance().getHomeSearchHistory();
            if (!TextUtils.isEmpty(homeSearchHistory)) {
                records.clear();
                String[] arr = homeSearchHistory.split(",");
                List<String> list = new ArrayList<>(Arrays.asList(arr));
                list.remove(s);
                // 删除后再组成新的记录字符串
                String newHomeSearchHistory = YHStringUtils.join(list.toArray(new String[0]), ",");
                DiscoCacheUtils.getInstance().setHomeSearchHistory(newHomeSearchHistory);
                if (list.size() > 3 && !isExpand) {
                    // 默认只显示3条，展示查看全部记录按钮
                    tvAllRecord.setVisibility(View.VISIBLE);
                    records.addAll(new ArrayList<>(list.subList(0, 3)));
                } else {
                    tvAllRecord.setVisibility(View.GONE);
                    records.addAll(list);
                }
                historyAdapter.notifyDataSetChanged();
            }
        });
        historyAdapter.setOnItemClickListener((itemView, pos) -> {
            List<String> strings = historyAdapter.getmDataList();
            keyWord = strings.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.HOME_SEARCH_RESULT_ACTIVITY)
                    .withString("keyWord", keyWord)
                    .navigation();
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        records.clear();
        String homeSearchHistory = DiscoCacheUtils.getInstance().getHomeSearchHistory();
        if (!TextUtils.isEmpty(homeSearchHistory)) {
            String[] arr = homeSearchHistory.split(",");
            List<String> list = Arrays.asList(arr);
            if (list.size() > 3) {
                // 默认只显示3条，展示查看全部记录按钮
                tvAllRecord.setVisibility(View.VISIBLE);
                tvCleanAllRecord.setVisibility(View.GONE);
                records.addAll(new ArrayList<>(list.subList(0, 3)));
            } else {
                tvAllRecord.setVisibility(View.GONE);
                tvCleanAllRecord.setVisibility(View.GONE);
                records.addAll(list);
            }
            historyAdapter.notifyDataSetChanged();
        }
    }

    private void toSearch() {
        if (TextUtils.isEmpty(keyWord)) {
            ToastUtils.showShort("请输入要搜索的内容");
            return;
        }
        String homeSearchHistory = DiscoCacheUtils.getInstance().getHomeSearchHistory();
        String[] arr = homeSearchHistory.split(",");
        List<String> list = Arrays.asList(arr);
        List<String> copyList = new ArrayList<>(list);
        if (list.contains(keyWord)) {
            // 如果包含，去掉之前的，在最前面加一个新的
            copyList.remove(keyWord);
            copyList.add(0, keyWord);
            homeSearchHistory = YHStringUtils.join(copyList.toArray(new String[0]), ",");
        } else {
            // 如果不包含，加在最前面
            if (arr.length >= 10) {
                homeSearchHistory = homeSearchHistory.substring(0, homeSearchHistory.length() - 1);
                homeSearchHistory = homeSearchHistory.substring(0, homeSearchHistory.lastIndexOf(","));
            }
            homeSearchHistory = keyWord + "," + homeSearchHistory;
        }
        DiscoCacheUtils.getInstance().setHomeSearchHistory(homeSearchHistory);
        ARouter.getInstance()
                .build(ARouterPath.HOME_SEARCH_RESULT_ACTIVITY)
                .withString("keyWord", keyWord)
                .navigation();
    }

    private void getHotData(int pos) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", 1);
        params.put("size", 100);
        if (pos == 0) {
            basePresenter.getHotTopicList(params);
        } else {
            basePresenter.getHotCircleList(params);
        }
    }

    @OnClick({R2.id.tvCancel, R2.id.tvAllRecord, R2.id.tvCleanAllRecord})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.tvCancel) {
            hideSoftKeyboard();
            onBackPressed();
        } else if (id == R.id.tvAllRecord) {
            // 点击查看全部搜索记录后，隐藏查看全部按钮，显示清除全部记录按钮
            isExpand = true;
            records.clear();
            String homeSearchHistory = DiscoCacheUtils.getInstance().getHomeSearchHistory();
            if (!TextUtils.isEmpty(homeSearchHistory)) {
                String[] arr = homeSearchHistory.split(",");
                List<String> list = Arrays.asList(arr);
                records.addAll(list);
                historyAdapter.notifyDataSetChanged();
            }
            tvAllRecord.setVisibility(View.GONE);
            tvCleanAllRecord.setVisibility(View.VISIBLE);
        } else if (id == R.id.tvCleanAllRecord) {
            AlertPopup alertPopup = new AlertPopup(mContext, "是否清空历史记录？");
            new XPopup.Builder(mContext).asCustom(alertPopup).show();
            alertPopup.setOnConfirmListener(() -> {
                alertPopup.dismiss();
                // 清空搜索记录缓存
                DiscoCacheUtils.getInstance().setHomeSearchHistory("");
                records.clear();
                historyAdapter.notifyDataSetChanged();
                tvCleanAllRecord.setVisibility(View.GONE);
            });
            alertPopup.setOnCancelListener(alertPopup::dismiss);
        }
    }

    @Override
    public void handleHotTopicList(ListResult<TopicModel> result) {
        refreshLayout.finishRefresh();
        List<TopicModel> topicList = result.getRecords();
        if (null != topicList && topicList.size() > 0) {
            hotTopicListAdapter.setItems(topicList);
        }
    }

    @Override
    public void handleHotCircleList(ListResult<CircleModel> result) {
        refreshLayout.finishRefresh();
        List<CircleModel> circleList = result.getRecords();
        if (null != circleList && circleList.size() > 0) {
            hotCircleListAdapter.setItems(circleList);
        }
    }

    static class HotTopicListAdapter extends BaseRecyclerAdapter<TopicModel, TopicViewHolder> {

        public HotTopicListAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected TopicViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new TopicViewHolder(inflater.inflate(R.layout.item_hot, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull TopicViewHolder holder, @NonNull TopicModel item) {
            int position = getPosition(holder);

            if (position < 3) {
                holder.viewDot.setVisibility(View.VISIBLE);
                holder.otvOrder.setTextSize(Dimension.SP, 20);
            } else {
                holder.viewDot.setVisibility(View.GONE);
                holder.otvOrder.setTextSize(Dimension.SP, 16);
            }

            holder.tvTag.setText("#" + item.getTitle() + "#");
            holder.otvOrder.setText(String.valueOf(position + 1));
            holder.tvViewCount.setText(YHStringUtils.quantityFormat(item.getDynamicCount()));
        }
    }

    static class TopicViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvTag)
        TextView tvTag;
        @BindView(R2.id.otvOrder)
        OswaldTextView otvOrder;
        @BindView(R2.id.tvViewCount)
        TextView tvViewCount;
        @BindView(R2.id.viewDot)
        View viewDot;

        public TopicViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class HotCircleListAdapter extends BaseRecyclerAdapter<CircleModel, CircleViewHolder> {

        public HotCircleListAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected CircleViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new CircleViewHolder(inflater.inflate(R.layout.item_hot, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull CircleViewHolder holder, @NonNull CircleModel item) {
            int position = getPosition(holder);

            if (position < 3) {
                holder.viewDot.setVisibility(View.VISIBLE);
                holder.otvOrder.setTextSize(Dimension.SP, 20);
            } else {
                holder.viewDot.setVisibility(View.GONE);
                holder.otvOrder.setTextSize(Dimension.SP, 16);
            }

            holder.tvTag.setText("◎" + item.getTitle());
            holder.otvOrder.setText(String.valueOf(position + 1));
            holder.tvViewCount.setText(YHStringUtils.quantityFormat(item.getDynamicCount()));
        }
    }

    static class CircleViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvTag)
        TextView tvTag;
        @BindView(R2.id.otvOrder)
        OswaldTextView otvOrder;
        @BindView(R2.id.tvViewCount)
        TextView tvViewCount;
        @BindView(R2.id.viewDot)
        View viewDot;

        public CircleViewHolder(View itemView) {
            super(itemView);
        }
    }
}