package com.yanhua.home.activity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.PublishTemp;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.view.EmptyLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.StrategyClassifyMainAdapter;
import com.yanhua.home.adapter.StrategyCompassAdapter;
import com.yanhua.home.adapter.StrategyGameAdapter;
import com.yanhua.home.adapter.StrategySuggestAdapter;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.STRATEGY_NEWS_ACTIVITY)
public class StrategyActivity extends BaseMvpActivity<NewsPresenter> implements NewsContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.refreshSearch)
    SmartRefreshLayout refreshSearch;
    @BindView(R2.id.rv_result)
    RecyclerView rvResult;
    @BindView(R2.id.emptyView)
    EmptyLayout emptyView;

    @BindView(R2.id.tvPublish)
    TextView tvPublish;

    @BindView(R2.id.rvCompass)
    RecyclerView rvCompass;
    @BindView(R2.id.rvSuggest)
    RecyclerView rvSuggest;
    @BindView(R2.id.rvContent)
    RecyclerView rvContent;

    @BindView(R2.id.rvClassify)
    RecyclerView rvClassify;
    @BindView(R2.id.llCompass)
    LinearLayout llCompass;
    @BindView(R2.id.rlSuggest)
    RelativeLayout rlSuggest;

    @BindView(R2.id.rl_search)
    LinearLayout rlSearch;
    @BindView(R2.id.tv_search_cancle)
    TextView tvSearchCancle;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    StrategyCompassAdapter hotAdapter;
    StrategySuggestAdapter suggestAdapter;

    private List<StrategyModel> mListData;
    private StrategyGameAdapter mAdapter;
    private StrategyClassifyMainAdapter classifyMainAdapter;//顶部分类

    @Override
    public int bindLayout() {
        return R.layout.activity_strategy_news;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, /*llEquipment, llMethod, llPlays, llShop,*/ tvPublish);

        refreshLayout.setOnRefreshListener(rl -> {
            current = 1;
            getRefreshData();
        });

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                basePresenter.strategyList(current, size, "", "", YXConfig.TYPE_STRATEGY);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        //===指南针---数据初始化---
        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        hotAdapter = new StrategyCompassAdapter(mContext);
        rvCompass.setLayoutManager(manager);
        rvCompass.setAdapter(hotAdapter);
        hotAdapter.setOnItemClickListener((itemView, pos) -> {
            StrategyHotSortModel clickItem = hotAdapter.getItemObject(pos);

            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_STRATEGY);
        });


        //===热门推荐---数据初始化---
        LinearLayoutManager suggestmanager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        suggestAdapter = new StrategySuggestAdapter(mContext);
        rvSuggest.setLayoutManager(suggestmanager);
        rvSuggest.setAdapter(suggestAdapter);

        suggestAdapter.setOnItemClickListener((itemView, pos) -> {
            StrategyHotSortModel clickItem = suggestAdapter.getItemObject(pos);
            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_STRATEGY);
        });

        //StrategyHotSortModel----玩转娱乐
        mListData = new ArrayList<>();

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new StrategyGameAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            StrategyModel clickItem = mAdapter.getItemObject(pos);

            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_STRATEGY);
        });

        //===分类---
        LinearLayoutManager classifyManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        classifyMainAdapter = new StrategyClassifyMainAdapter(mContext);
        rvClassify.setLayoutManager(classifyManager);
        rvClassify.setAdapter(classifyMainAdapter);

        classifyMainAdapter.setOnItemClickListener((itemView, pos) -> ARouter.getInstance()
                .build(ARouterPath.STRATEGY_CLASSIFY_ACTIVITY)
                .withInt("selectPos", pos)
                .withSerializable("channelList", mChannelList)
                .navigation());

        etSearch.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                String keyword = etSearch.getText().toString().trim();
                basePresenter.strategyList(current, size, keyword, "", YXConfig.TYPE_STRATEGY);
                return true;
            }
            return false;
        });
    }

    private void getRefreshData() {
        //指南针攻略 attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
        //type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
        //指南针 7 4
        basePresenter.hotSortList(7, "", YXConfig.TYPE_STRATEGY);
        //推荐 6   4
        basePresenter.hotSortList(6, "", YXConfig.TYPE_STRATEGY);

        //获取玩转娱乐数据
        basePresenter.strategyList(current, size, "", "", YXConfig.TYPE_STRATEGY);
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        mChannelList = new ArrayList<>();
        //分类初始化
        basePresenter.getNewsTypeList(YXConfig.TYPE_STRATEGY, 0);

        refreshLayout.autoRefresh(100);
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.tvPublish) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                //发布策略、攻略
                PublishNewsForm publishNewsForm = PublishTemp.getStrategyTemp(mContext);
                ARouter.getInstance().build(ARouterPath.PUBLISH_STRATEGY_ACTIVITY)
                        .withSerializable("publishNewsForm", publishNewsForm).navigation();
            });
        }
    }

    @OnClick({R2.id.ll_to_search, R2.id.tv_search_cancle})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.ll_to_search) {
            rlSearch.setVisibility(View.VISIBLE);
            etSearch.setFocusable(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.requestFocus();
        } else if (id == R.id.tv_search_cancle) {
            hideSoftKeyboard();
            rlSearch.setVisibility(View.GONE);
            etSearch.setText("");
            refreshSearch.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
        }
    }

    private ArrayList<NewsTypeModel> mChannelList;

    @Override
    public void handleNewsTypeList(List<NewsTypeModel> data) {
        if (null != data && data.size() > 0) {
            rvClassify.setVisibility(View.VISIBLE);

            mChannelList.clear();

            mChannelList.addAll(data);

            classifyMainAdapter.setItems(mChannelList);//
        } else {
            rvClassify.setVisibility(View.GONE);
        }
    }

    private int total;

    @Override
    public void handleStrategyList(ListResult<StrategyModel> listResult) {
        List<StrategyModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();

            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }


    @Override
    public void handleStrategySortList(List<StrategyHotSortModel> data, int attributeType) {
        if (attributeType == 6) {
            //热门推荐
            if (null != data && data.size() > 0) {
                rlSuggest.setVisibility(View.VISIBLE);

                suggestAdapter.setItems(data);//
            } else {
                rlSuggest.setVisibility(View.GONE);
            }
        } else if (attributeType == 7) {
            //指南针攻略
            if (null != data && data.size() > 0) {
                llCompass.setVisibility(View.VISIBLE);
                hotAdapter.setItems(data);//
            } else {
                llCompass.setVisibility(View.GONE);
            }
        }
    }
}
