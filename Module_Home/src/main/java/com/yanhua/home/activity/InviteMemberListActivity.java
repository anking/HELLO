package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.SlidingTabLayout;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.Channel;
import com.yanhua.common.model.ConditionPartModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.ConditionPartShadowPopupView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.fragment.InviteJoinedMemberListFragment;
import com.yanhua.home.fragment.InviteMemberListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.INVITE_MEMBER_LIST_ACTIVITY)
public class InviteMemberListActivity extends BaseMvpActivity {
    @BindView(R2.id.btnRight)
    TextView btnRight;
    @BindView(R2.id.sltab_channel)
    SlidingTabLayout sltabChannel;
    @BindView(R2.id.vp_channel)
    ViewPager vpChannel;

    @BindView(R2.id.tvFilter)
    TextView tvFilter;

    @Autowired
    String inviteId;//邀約的id

    private List<Channel> mChannelList;
    private ChannelPageAdapter mAdapter;
    private ArrayList<Fragment> mFragments;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        setTitle("参加组局详情");

        initCategoryView();
    }

    private void initCategoryView() {
        mChannelList = new ArrayList<>();

        Channel joiningList = new Channel("1", "待加入", "待加入", 1);
        Channel joinedList = new Channel("2", "已加入", "已加入", 2);

        mChannelList.add(joiningList);
        mChannelList.add(joinedList);

        mFragments = new ArrayList<>();

        //

        Fragment joiningFragment = new InviteMemberListFragment();
        Bundle joiningBundle = new Bundle();
        joiningBundle.putString("id", joiningList.getId());
        joiningBundle.putString("reportName", joiningList.getReportName());
        joiningBundle.putString("inviteId", inviteId);
        joiningFragment.setArguments(joiningBundle);
        mFragments.add(joiningFragment);

        //---------------------------------------
        Fragment joinedFragment = new InviteJoinedMemberListFragment();
        Bundle joinedBundle = new Bundle();
        joinedBundle.putString("id", joinedList.getId());
        joiningBundle.putString("reportName", joiningList.getReportName());
        joinedBundle.putString("inviteId", inviteId);
        joinedFragment.setArguments(joinedBundle);
        mFragments.add(joinedFragment);

        if (mChannelList != null && mChannelList.size() > 0) {
            mAdapter = new ChannelPageAdapter(getSupportFragmentManager());
            vpChannel.setAdapter(mAdapter);
            sltabChannel.setViewPager(vpChannel);
            sltabChannel.setCurrentTab(0);
        }
    }

    public void setTabTitle(int pos, String title) {
        sltabChannel.getTitleView(pos).setText(title);
    }

    private ConditionPartShadowPopupView conditionPopupView;

    private ConditionPartModel selectCondition;
    private ArrayList<ConditionPartModel> listDataResponse = new ArrayList<>();

    @OnClick(R2.id.tvFilter)
    public void showConditionPopup() {
        if (conditionPopupView == null) {
            //模拟数据
            //String name, boolean selected, String param
            ConditionPartModel peop = new ConditionPartModel("男女不限", true, 0, "0");
            ConditionPartModel man = new ConditionPartModel("仅限男生", false, 1, "1");
            ConditionPartModel woman = new ConditionPartModel("仅限女生", false, 2, "2");

            listDataResponse.add(peop);
            listDataResponse.add(man);
            listDataResponse.add(woman);

            //默认取第一个作为默认值
            selectCondition = listDataResponse.get(0);

            conditionPopupView = new ConditionPartShadowPopupView(this, listDataResponse, selectCondition);
            conditionPopupView.setOnSelectTypeListener(item -> {
                setConditionText(true, item);
            });
        }
        new XPopup.Builder(this).atView(tvFilter).asCustom(conditionPopupView).show();
    }

    private void setConditionText(boolean select, ConditionPartModel selectObj) {
        if (null == selectCondition) {
            selectCondition = listDataResponse.get(0);
        } else {
            selectCondition = selectObj;
        }
        tvFilter.setText(selectCondition.getName());

        if (select) {
            current = 1;

            int pos = sltabChannel.getCurrentTab();
            getListData(pos);
        }
    }

    public void getListData(int pos) {
        Fragment fragment = mFragments.get(pos);

        int userGender = 0;//0:男女不限, 1:男, 2:女
        if (null != selectCondition) {
            userGender = selectCondition.getChangeType();
        }

        if (fragment instanceof InviteMemberListFragment) {
            ((InviteMemberListFragment) fragment).getListData(1, userGender);
        } else if (fragment instanceof InviteJoinedMemberListFragment) {
            ((InviteJoinedMemberListFragment) fragment).getListData(1, userGender);
        }
    }

    @Override
    protected void creatPresent() {

    }

    @OnClick({R2.id.btnRight})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.btnRight) {
            //发起邀约
        }
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_member_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
    }

    class ChannelPageAdapter extends FragmentStatePagerAdapter {

        public ChannelPageAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mChannelList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            Channel model = mChannelList.get(position);
            String reportName = model.getReportName();
            return !TextUtils.isEmpty(reportName) ? reportName : "";
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
        }
    }
}
