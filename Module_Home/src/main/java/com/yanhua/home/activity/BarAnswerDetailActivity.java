package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.CommentBoardPopup;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.CommentInputPopup;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.widget.AlertPopup;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BarQACommentAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 回答详情页面
 */
@Route(path = ARouterPath.BAR_ANSWER_DETAIL_ACTIVITY)
public class BarAnswerDetailActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.rlTopBar)
    RelativeLayout rlTopBar;
    @BindView(R2.id.tvTitle)
    TextView tvTitle;
    @BindView(R2.id.tv_left)
    TextView tvLeft;
    @BindView(R2.id.iconMore)
    TextView iconMore;
    @BindView(R2.id.tvWorseNum)
    TextView tvWorseNum;
    @BindView(R2.id.tvLoveNum)
    TextView tvLoveNum;
    @BindView(R2.id.iv_worse)
    AliIconFontTextView ivWorse;
    @BindView(R2.id.iv_love)
    AliIconFontTextView ivLove;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_comment)
    RecyclerView rvComment;

    private TextView tvQuestionName;
    private TextView tvAnswer;
    private CircleImageView civAvatar;
    private TextView tvUserName;
    private TextView tvTime;
    private TextView tvFollow;
    private LinearLayout llNum;
    private TextView tvCommentNum;

    @Autowired
    String askId;

    private BarQACommentAdapter mAdapter;
    private MyQAModel myQAModel;
    private int currentPosition;
    private boolean isReply;// 是否是回复某条评论
    private List<CommentModel> mList = new ArrayList<>();
    // 组装后的评论列表
    private List<CommentModel> mCommentList = new ArrayList<>();
    private int total;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_answer_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        rlTopBar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        tvTitle.setText("回答详情");
        tvLeft.setTextColor(ContextCompat.getColor(this, R.color.mainWord));
        iconMore.setTextColor(ContextCompat.getColor(this, R.color.mainWord));

        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            basePresenter.getBarAnswerDetail(askId);
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mList.size() < total) {
                current++;
                basePresenter.getCommentList(myQAModel.getId(), current, 2);
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });

        rvComment.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BarQACommentAdapter(this);
        mAdapter.setOnItemLongClickListener((itemView, pos) -> {
            if (pos == 0) return;
            currentPosition = pos - 1;
            CommentModel commentModel = mCommentList.get(currentPosition);
            PageJumpUtil.firstIsLoginThenJump(() -> {
                boolean myself = UserManager.getInstance().isMyself(commentModel.getUserId());
                CommentBoardPopup popup = new CommentBoardPopup(mActivity, false, false, false, !myself, myself);
                popup.setOnActionItemListener(actionType -> {
                    switch (actionType) {
                        case CommentBoardPopup.ACTION_REPORT:
                            reportComment(commentModel.getId());
                            break;
                        case CommentBoardPopup.ACTION_DELETE:
                            deleteComment(commentModel.getId(), currentPosition);
                            break;
                    }
                });
                new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
            });
        });
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            if (pos == 0) return;
            currentPosition = pos - 1;
            CommentModel model = mCommentList.get(currentPosition);
            //回复
            replyComment(model);
        });
        mAdapter.setOnItemCellClickListener((pos, cellType) -> {
            if (pos == 0) return;
            currentPosition = pos - 1;
            CommentModel commentModel = mCommentList.get(currentPosition);
            switch (cellType) {
                case 0:
                    ARouter.getInstance()
                            .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", commentModel.getUserId())
                            .navigation();
                    break;
                case 1:
                    PageJumpUtil.firstIsLoginThenJump(() -> basePresenter.doLoveComment(commentModel.getId()));
                    break;
                case 2:
                    PageJumpUtil.firstIsLoginThenJump(() -> basePresenter.doWorseComment(commentModel.getId()));
                    break;
                case 3:
                    int groupPosition = commentModel.getGroupPosition();
                    String parentCommentId = mList.get(groupPosition).getId();
                    List<CommentModel> childCommentList = mList.get(groupPosition).getChildrens();
                    int size = childCommentList.size() + 5;
                    basePresenter.getChildCommentList(parentCommentId, 1, size);
                    break;
                case 4:
                    PageJumpUtil.firstIsLoginThenJump(() -> {
                        boolean myself = UserManager.getInstance().isMyself(commentModel.getUserId());
                        CommentBoardPopup popup = new CommentBoardPopup(mActivity, false, false, false, !myself, myself);
                        popup.setOnActionItemListener(actionType -> {
                            switch (actionType) {
                                case CommentBoardPopup.ACTION_REPORT:
                                    reportComment(commentModel.getId());
                                    break;
                                case CommentBoardPopup.ACTION_DELETE:
                                    deleteComment(commentModel.getId(), currentPosition);
                                    break;
                            }
                        });
                        new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
                    });
                    break;
            }
        });
        rvComment.setAdapter(mAdapter);

        View header = LayoutInflater.from(this).inflate(R.layout.layout_header_bar_answer_detail, null);
        header.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tvQuestionName = header.findViewById(R.id.tvQuestionName);
        tvAnswer = header.findViewById(R.id.tvAnswer);
        civAvatar = header.findViewById(R.id.civ_avatar);
        tvUserName = header.findViewById(R.id.tvUserName);
        tvTime = header.findViewById(R.id.tvTime);
        llNum = header.findViewById(R.id.llNum);
        tvCommentNum = header.findViewById(R.id.tvCommentNum);
        tvFollow = header.findViewById(R.id.tvFollow);
        tvFollow.setOnClickListener(v -> {
            if (myQAModel == null) return;
            if (myQAModel.getIsFollow() == 1) {
                AlertPopup alertPopup = new AlertPopup(mContext, "确认不再关注？");
                new XPopup.Builder(mContext).asCustom(alertPopup).show();
                alertPopup.setOnConfirmListener(() -> {
                    alertPopup.dismiss();
                    basePresenter.cancelFollowUser(myQAModel.getUserId());
                });
                alertPopup.setOnCancelListener(alertPopup::dismiss);
            } else {
                // 关注用户
                basePresenter.followUser(myQAModel.getUserId());
            }
        });
        mAdapter.addHeaderView(header);
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        refreshLayout.autoRefresh(100);
    }

    @OnClick({R2.id.iconMore, R2.id.ll_worse, R2.id.ll_love, R2.id.llToComment})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.iconMore) {
            if (myQAModel == null) return;
            PageJumpUtil.firstIsLoginThenJump(() -> {
                boolean myself = UserManager.getInstance().isMyself(myQAModel.getUserId());
                CommentBoardPopup popup = new CommentBoardPopup(mActivity, false, false, false, !myself, myself);
                popup.setOnActionItemListener(actionType -> {
                    switch (actionType) {
                        case CommentBoardPopup.ACTION_REPORT:
                            toReport();
                            break;
                        case CommentBoardPopup.ACTION_DELETE:
                            toDelete();
                            break;
                    }
                });
                new XPopup.Builder(mActivity).autoFocusEditText(false).asCustom(popup).show();
            });
        } else if (id == R.id.ll_worse) {
            if (myQAModel == null) return;
            basePresenter.doWorseAnswer(myQAModel.getId());
        } else if (id == R.id.ll_love) {
            if (myQAModel == null) return;
            basePresenter.doLoveAnswer(myQAModel.getId());
        } else if (id == R.id.llToComment) {
            if (myQAModel == null) return;
            showInputPopup("", "",0);
        }
    }

    /**
     * 弹出评论框
     *
     * @param commentId
     * @param nickName
     */
    private void showInputPopup(String commentId, String nickName,int childType) {
        PageJumpUtil.firstIsLoginThenJump(() -> {
            if (!TextUtils.isEmpty(commentId)) {
                isReply = true;
            } else {
                isReply = false;
            }
            CommentInputPopup popup = new CommentInputPopup(mContext, nickName);
            popup.setOnSendCommentListener((comment,ct) -> {//发送评论
                PublishCommentForm commentForm = new PublishCommentForm();
                commentForm.setCommentDesc(comment);
                commentForm.setContentId(myQAModel.getId());
                commentForm.setParentCommentId(commentId);

                if (ct != 0) {
                    commentForm.setReplyType(2);
                } else {
                    commentForm.setReplyType(1);
                }
                commentForm.setUserId(UserManager.getInstance().getUserId());
                basePresenter.publishComment(commentForm, YXConfig.TYPE_9BAR);
            },childType);
            new XPopup.Builder(mContext).hasShadowBg(true).autoOpenSoftInput(true).autoFocusEditText(true).asCustom(popup).show();
        });
    }

    /**
     * 回复动作
     *
     * @param model
     */
    private void replyComment(CommentModel model) {
        String commentId = model.getId();
        String nickName = YHStringUtils.pickName(model.getNickName(), model.getFriendRemark());

        int childType = model.getChildType();
        showInputPopup(commentId, nickName,childType);
    }

    /**
     * 举报酒吧问答详情
     */
    private void toReport() {
        ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                .withString("businessID", myQAModel.getId())
                .withInt("reportType", YXConfig.reportType.question_answer)
                .navigation();
    }

    private void reportComment(String id) {
        ARouter.getInstance().build(ARouterPath.REPORT_ACTIVITY)
                .withString("businessID", id)
                .withInt("reportType", YXConfig.reportType.comment)
                .navigation();
    }

    private void deleteComment(String id, int pos) {
        AlertPopup alertPopup = new AlertPopup(mContext, "确认删除该评论？");
        new XPopup.Builder(mContext).asCustom(alertPopup).show();
        alertPopup.setOnConfirmListener(() -> {
            alertPopup.dismiss();
            basePresenter.deleteMyComment(id, pos);
        });
        alertPopup.setOnCancelListener(alertPopup::dismiss);
    }

    @Override
    public void handleDeleteMyCommentSuccess(int pos) {
        mCommentList.remove(pos);
        mAdapter.notifyItemRemoved(pos + 1);
        mAdapter.notifyItemRangeChanged(pos + 1, mCommentList.size() - pos);
        ToastUtils.showShort("评论删除成功");
        if (mCommentList.size() == 0) {
            llNum.setVisibility(View.GONE);
        } else {
            llNum.setVisibility(View.VISIBLE);
            tvCommentNum.setText(String.format("评论(%s)", YHStringUtils.quantityFormat(mCommentList.size())));
        }
    }

    /**
     * 删除问答详情
     */
    private void toDelete() {
        AlertPopup alertPopup = new AlertPopup(mContext, "确认删除该回答？");
        new XPopup.Builder(mContext).asCustom(alertPopup).show();
        alertPopup.setOnConfirmListener(() -> {
            alertPopup.dismiss();
            basePresenter.deleteMyAnswer(myQAModel.getAskedId());
        });
        alertPopup.setOnCancelListener(alertPopup::dismiss);
    }

    @Override
    public void handleDeleteMyAnswerSuccess() {
        ToastUtils.showShort("删除成功");
        setResult(RESULT_OK);
        onBackPressed();
    }

    @Override
    public void handleWorseSuccess(String msg) {
        ToastUtils.showShort(msg);
        if (myQAModel.getWorseStatus() == 0) {
            myQAModel.setWorseStatus(1);
            myQAModel.setWorseCount(myQAModel.getWorseCount() + 1);
        } else {
            myQAModel.setWorseStatus(0);
            myQAModel.setWorseCount(myQAModel.getWorseCount() - 1);
        }
        tvWorseNum.setText(myQAModel.getWorseCount() > 0 ? String.valueOf(myQAModel.getWorseCount()) : "踩");
        tvWorseNum.setTextColor(myQAModel.getWorseStatus() == 1 ? ContextCompat.getColor(this, R.color.theme) : ContextCompat.getColor(this, R.color.mainWord));
        ivWorse.setSelected(myQAModel.getWorseStatus() == 1);
        ivWorse.setText(myQAModel.getWorseStatus() == 1 ? "\ue779" : "\ue77a");
    }

    @Override
    public void handleLoveSuccess(String msg) {
        ToastUtils.showShort(msg);
        if (myQAModel.getFabulousStatus() == 0) {
            myQAModel.setFabulousStatus(1);
            myQAModel.setFabulousCount(myQAModel.getFabulousCount() + 1);
        } else {
            myQAModel.setFabulousStatus(0);
            myQAModel.setFabulousCount(myQAModel.getFabulousCount() - 1);
        }
        tvLoveNum.setText(myQAModel.getFabulousCount() > 0 ? String.valueOf(myQAModel.getFabulousCount()) : "赞");
        tvLoveNum.setTextColor(ContextCompat.getColor(this, R.color.mainWord));

//        tvLoveNum.setTextColor(myQAModel.getFabulousStatus() == 1 ? ContextCompat.getColor(this, R.color.theme) : ContextCompat.getColor(this, R.color.mainWord));
        ivLove.setSelected(myQAModel.getFabulousStatus() == 1);
        ivLove.setText(myQAModel.getFabulousStatus() == 1 ? "\ue772" : "\ue71b");
    }

    @Override
    public void handleBarAnswerDetailSuccess(MyQAModel model) {
        if (model != null) {
            myQAModel = model;
            basePresenter.getCommentList(model.getId(), current, 2);
            tvQuestionName.setText(model.getParentAskedDesc());
            tvAnswer.setText(model.getAskedDesc());
            if (model.getIsShow() == 1) {
                // 匿名
                ImageLoaderUtil.loadImg(civAvatar, R.drawable.rc_default_portrait);
                tvUserName.setText("匿名用户");
                tvFollow.setVisibility(View.GONE);
            } else {
                // 非匿名
                ImageLoaderUtil.loadImg(civAvatar, model.getImg());
                tvUserName.setText(model.getAccountName());
                tvFollow.setVisibility(View.VISIBLE);
                tvFollow.setText(model.getIsFollow() == 1 ? "已关注" : "关注");
            }
            if (model.getUserId().equals(UserManager.getInstance().getUserId())) {
                // 如果回答者是自己，不显示关注按钮
                tvFollow.setVisibility(View.GONE);
            }
            tvTime.setText(String.format("回答于 %s", YXTimeUtils.getFriendlyTimeAtContent(model.getCreatedTime(), true)));
            tvWorseNum.setText(model.getWorseCount() > 0 ? String.valueOf(model.getWorseCount()) : "踩");
            tvLoveNum.setText(model.getFabulousCount() > 0 ? String.valueOf(model.getFabulousCount()) : "赞");

            tvWorseNum.setTextColor(ContextCompat.getColor(this, R.color.mainWord));
//            tvWorseNum.setTextColor(model.getWorseStatus() == 1 ? ContextCompat.getColor(this, R.color.theme) : ContextCompat.getColor(this, R.color.mainWord));
//            tvLoveNum.setTextColor(model.getFabulousStatus() == 1 ? ContextCompat.getColor(this, R.color.theme) : ContextCompat.getColor(this, R.color.mainWord));
            tvLoveNum.setTextColor(ContextCompat.getColor(this, R.color.mainWord));

            ivLove.setSelected(model.getFabulousStatus() == 1);
            ivWorse.setSelected(model.getWorseStatus() == 1);
            ivWorse.setText(model.getWorseStatus() == 1 ? "\ue779" : "\ue77a");
            ivLove.setText(model.getFabulousStatus() == 1 ? "\ue772" : "\ue71b");
        }
    }

    @Override
    public void handlePublishCommentSuccess(CommentModel model) {
        if (model.getCommentAudit() == 0) {
            ToastUtils.showShort("提交成功，等待管理员审核");
        } else if (model.getCommentAudit() == 1) {
            ToastUtils.showShort("评论成功");
            if (!isReply) {
                current = 1;
                basePresenter.getCommentList(myQAModel.getId(), current, 2);
            } else {
                CommentModel preModel = mCommentList.get(currentPosition);
                int groupPosition = preModel.getGroupPosition();
                int type = preModel.getType();
                if (type == 0) {
                    model.setType(1);
                } else {
                    preModel.setType(1);
                    model.setType(type);
                    //设置回复
                    model.setReplyStatus(1);
                    String preNick = YHStringUtils.pickName(preModel.getNickName(), preModel.getFriendRemark());
                    model.setReplyName(preNick);
                }
                model.setGroupPosition(groupPosition);

                UserInfo userInfo = UserManager.getInstance().getUserInfo();

                String nickName = userInfo.getNickName();
                String userPhoto = userInfo.getImg();
                String userId = userInfo.getId();
                if (TextUtils.isEmpty(model.getUserPhoto())) {
                    model.setUserPhoto(userPhoto);
                }
                if (TextUtils.isEmpty(model.getNickName())) {
                    model.setNickName(nickName);
                }
                if (TextUtils.isEmpty(model.getUserId())) {
                    model.setUserId(userId);
                }

                mCommentList.add(currentPosition + 1, model);
                mAdapter.setItems(mCommentList);
            }
        }
    }

    @Override
    public void handleCommentList(List<CommentModel> list, int totalSum) {
        total = totalSum;
        if (current == 1) {
            mList.clear();
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            mList.addAll(list);
        }

        if (totalSum == 0) {
            llNum.setVisibility(View.GONE);
        } else {
            llNum.setVisibility(View.VISIBLE);
            tvCommentNum.setText(String.format("评论(%s)", YHStringUtils.quantityFormat(totalSum)));
        }

        mCommentList = toBuildCommentList();
        mAdapter.setItems(mCommentList);
    }

    @Override
    public void handleChildCommentList(ListResult<CommentModel> listResult) {
        if (listResult != null) {
            int total = listResult.getTotal();
            CommentModel model = mCommentList.get(currentPosition);
            int groupPosition = model.getGroupPosition();
            mList.get(groupPosition).setChildCount(total);
            List<CommentModel> list = listResult.getRecords();
            if (list != null && !list.isEmpty()) {
                mList.get(groupPosition).setChildrens(list);
            }
            mCommentList = toBuildCommentList();
            mAdapter.setItems(mCommentList);
        }
    }

    private List<CommentModel> toBuildCommentList() {
        List<CommentModel> list = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            CommentModel model = mList.get(i);

            int resultType = model.getResultType();
            if (resultType == 1) {

                //这是父评论
                model.setType(0);
                model.setGroupPosition(i);
                model.setParentChildCount(0);

                list.add(model);
                List<CommentModel> childrenList = model.getChildrens();
                int childCount = model.getChildCount();
                int count = 0;

                int childSize = childrenList.size();
                for (int j = 0; j < childSize; j++) {
                    count = count + 1;
                    CommentModel childrenComment = childrenList.get(j);
                    childrenComment.setGroupPosition(i);
                    childrenComment.setParentChildCount(model.getChildCount());

                    if (j == 0) {
                        if (childSize == 1) {
                            childrenComment.setChildType(3);
                        } else {
                            childrenComment.setChildType(1);
                        }
                    } else if (j == childSize - 1) {
                        childrenComment.setChildType(2);
                    } else {
                        childrenComment.setChildType(0);
                    }

                    //最后一条子评论并且总的评论的数量大于当前子评论的数量
                    if (count == childrenList.size() && count < childCount) {
                        childrenComment.setType(2);
                    } else {
                        childrenComment.setType(1);
                    }
                    list.add(childrenComment);
                }
            } else if (resultType == 2) {//广告
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public void updateFollowUserSuccess(boolean follow) {
        if (follow) {
            // 关注成功
            ToastUtils.showShort("关注成功");
            myQAModel.setIsFollow(1);
            tvFollow.setText("已关注");
        } else {
            // 取关成功
            ToastUtils.showShort("取消关注成功");
            myQAModel.setIsFollow(0);
            tvFollow.setText("关注");
        }
    }

    @Override
    public void handleLoveCommentSuccess(String msg) {
        CommentModel model = mCommentList.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setIsFabulous(!model.isIsFabulous());
        model.setFabulousCount(model.isIsFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition + 1);
    }

    @Override
    public void handleWorseCommentSuccess(String msg) {
        CommentModel model = mCommentList.get(currentPosition);
        int worseCount = model.getWorseCount();
        model.setWorse(!model.isWorse());
        model.setWorseCount(model.isWorse() ? worseCount + 1 : (worseCount > 0 ? worseCount - 1 : 0));
        mAdapter.notifyItemChanged(currentPosition + 1);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                //点击非输入框区域，清空输入框焦点
                hideSoftKeyboard();
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v instanceof EditText) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}