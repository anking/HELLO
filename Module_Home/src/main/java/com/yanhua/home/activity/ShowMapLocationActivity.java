package com.yanhua.home.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.animation.AlphaAnimation;
import com.amap.api.maps.model.animation.AnimationSet;
import com.amap.api.maps.model.animation.ScaleAnimation;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.MapLocationDetailModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.widget.PickMapPopup;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

@Route(path = ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
public class ShowMapLocationActivity extends BaseMvpActivity {

    @BindView(R2.id.tv_name)
    TextView tvName;
    @BindView(R2.id.tv_shop_address)
    TextView tvShopAddress;
    @BindView(R2.id.map_view)
    MapView mapView;

    @BindView(R2.id.btn_select_map_navigation)
    AliIconFontTextView btnNavigation;

    @Autowired
    MapLocationDetailModel addressDetail;
    @Autowired
    String distanceKM;

    //初始化地图控制器对象
    private AMap aMap;
    private PickMapPopup popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mvLocation.onResume ()，重新绘制加载地图
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mvLocation.onPause ()，暂停地图的绘制
        mapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mvLocation.onSaveInstanceState (outState)，保存地图当前的状态
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mvLocation.onDestroy()，销毁地图
        mapView.onDestroy();
    }

    @Override
    protected void creatPresent() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_show_map_location;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        if (null == addressDetail) {
            addressDetail = new MapLocationDetailModel();

            Intent intent = getIntent();
            Bundle data = intent.getExtras();
            if (null != data) {
                String address = data.getString("address");
                String longitude = data.getString("longitude");
                String latitude = data.getString("latitude");

                addressDetail.setAddress(address);
                addressDetail.setLatitude(latitude);
                addressDetail.setLongitude(longitude);
            }

        }


        applyDebouncingClickListener(true, btnNavigation);

        if (TextUtils.isEmpty(addressDetail.getName())) {
            tvName.setVisibility(View.INVISIBLE);
        } else {
            tvName.setText(addressDetail.getName());
        }

        String address = addressDetail.getAddress();

        if (!TextUtils.isEmpty(distanceKM) && !TextUtils.isEmpty(address)) {
            tvShopAddress.setText(distanceKM + "|" + address);
        } else {
            if (!TextUtils.isEmpty(distanceKM)) {
                tvShopAddress.setText(distanceKM);
            }
            if (!TextUtils.isEmpty(address)) {
                tvShopAddress.setText(address);
            }
        }

        if (aMap == null) {
            aMap = mapView.getMap();
        }

        LatLng latLng = new LatLng(Double.parseDouble(addressDetail.getLatitude()), Double.parseDouble(addressDetail.getLongitude()));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location));
        markerOptions.position(latLng);
        Marker marker = aMap.addMarker(markerOptions);

        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.8f, 1f);
        alphaAnimation.setDuration(2000);
        //设置不断重复
        alphaAnimation.setRepeatCount(3);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0.5f, 1f, 0.5f);
        scaleAnimation.setDuration(2000);
        //设置不断重复
        scaleAnimation.setRepeatCount(3);

        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);

        animationSet.setInterpolator(new LinearInterpolator());

        marker.setAnimation(animationSet);
        marker.startAnimation();

        //改变地图的中心点
        aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));

        marker.showInfoWindow();
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.btn_select_map_navigation) {
            if (popup == null) {
                popup = new PickMapPopup(this);
                popup.setOnPickListener(pos -> {
                    if (pos == 1) {
                        // 跳转高德地图
                        openGaoDeMap();
                    } else if (pos == 2) {
                        // 跳转百度地图
                        openBaiduMap();
                    }
                });
            }
            new XPopup.Builder(mContext).asCustom(popup).show();
        }
    }

    /**
     * 打开高德地图（公交出行，起点位置使用地图当前位置）
     * <p>
     * t = 0（驾车）= 1（公交）= 2（步行）= 3（骑行）= 4（火车）= 5（长途客车）
     */
    private void openGaoDeMap() {
        String address = YHStringUtils.pickLastFirst(addressDetail.getAddress(), addressDetail.getName());

        if (checkMapAppsIsExist(this, "com.autonavi.minimap")) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.autonavi.minimap");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("androidamap://route?sourceApplication=" + R.string.app_name
                    + "&sname=我的位置&dlat=" + addressDetail.getLatitude()
                    + "&dlon=" + addressDetail.getLongitude()
                    + "&dname=" + address
                    + "&dev=0&m=0&t=0"));
            startActivity(intent);
        } else {
            ToastUtils.showShort("高德地图未安装");
        }
    }

    /**
     * 打开百度地图
     *
     * @param
     * @param
     * @param
     */
    private void openBaiduMap() {
        String address = YHStringUtils.pickLastFirst(addressDetail.getAddress(), addressDetail.getName());

        if (checkMapAppsIsExist(this, "com.baidu.BaiduMap")) {
            double[] bds = gcj02_To_Bd09(Double.parseDouble(addressDetail.getLatitude()), Double.parseDouble(addressDetail.getLongitude()));
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("baidumap://map/direction?origin=我的位置&destination=name:"
                    + address
                    + "|latlng:" + bds[0] + "," + bds[1]
                    + "&mode=driving&sy=0&index=0&target=1"));
            startActivity(intent);
        } else {
            ToastUtils.showShort("百度地图未安装");
        }
    }

    /**
     * 检测地图应用是否安装
     *
     * @param context
     * @param packagename
     * @return
     */
    private boolean checkMapAppsIsExist(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (Exception e) {
            packageInfo = null;
            e.printStackTrace();
        }
        return packageInfo != null;
    }

    /**
     * 高德坐标系转百度坐标系
     *
     * @param lat
     * @param lon
     * @return
     */
    public double[] gcj02_To_Bd09(double lat, double lon) {
        double x_pi = 3.14159265358979324 * 3000.0 / 180.0;
        double x = lon, y = lat;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
        double tempLon = z * Math.cos(theta) + 0.0065;
        double tempLat = z * Math.sin(theta) + 0.006;
        double[] gps = {tempLat, tempLon};
        return gps;
    }
}