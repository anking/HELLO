package com.yanhua.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 个人数量
 *
 * @author Administrator
 */
@Route(path = ARouterPath.PUBLISH_INVITE_NUM_ACTIVITY)
public class InvitePeopleNumActivity extends BaseMvpActivity {

    @BindView(R2.id.tvChoose)
    TextView tvChoose;
    @BindView(R2.id.tv_right)
    TextView tvRight;
    @BindView(R2.id.etNum)
    EditText etNum;

    @Override
    protected void creatPresent() {
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_people_num;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        tvRight.setText("确定");
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        etNum.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> addListenEdit()));

        addListenEdit();
    }

    /**
     * 监听手机号和密码是否输入完毕
     */
    private void addListenEdit() {
        String num = etNum.getText().toString().trim();

        tvRight.setEnabled(!TextUtils.isEmpty(num));
    }

    @Override
    public void handleErrorMessage(String errorMsg) {
        ToastUtils.showShort(errorMsg, Toast.LENGTH_LONG);
    }


    @OnClick({R2.id.tv_right})
    public void onClickView(View view) {//点击保存
        int id = view.getId();
        if (id == R.id.tv_right) {
            hideSoftKeyboard();
            String num = etNum.getText().toString().trim();

            int peopleNum = Integer.valueOf(num);
            if (peopleNum >= 2) {
                Intent intent = new Intent();
                intent.putExtra("PEOPLE_NUM", peopleNum);
                setResult(100, intent);
                finish();
            } else {
                ToastUtils.showShort("活动人数不能低于2人哦");
            }
        }
    }
}
