package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.KeyboardUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.adapter.InviteListAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.InviteConfigModel;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.InviteFilterDrawerPopup;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.core.widget.AutoMarqueeTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.InvitePresenter;
import com.yanhua.home.presenter.contract.InviteContract;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.INVITE_LIST_ACTIVITY)
public class InviteListActivity extends BaseMvpActivity<InvitePresenter> implements InviteContract.IView {
    @BindView(R2.id.tv_rightIcon)
    TextView btnRight;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    @BindView(R2.id.ll_to_publish)
    LinearLayout ll_to_publish;

    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;

    @BindView(R2.id.llCity)
    LinearLayout llCity;
    @BindView(R2.id.tvCity)
    TextView tvCity;
    @BindView(R2.id.llType)
    LinearLayout llType;
    @BindView(R2.id.tvType)
    TextView tvType;
    @BindView(R2.id.llTime)
    LinearLayout llTime;
    @BindView(R2.id.tvTime)
    TextView tvTime;

    @BindView(R2.id.rl_tip)
    RelativeLayout rl_tip;
    @BindView(R2.id.tvTipContent)
    AutoMarqueeTextView tvTipContent;

    @BindView(R2.id.iconFilter)
    AliIconFontTextView iconFilter;

    private InviteListAdapter mAdapter;
    private List<InviteModel> mListData;
    private int total;
    private int currentPosition;
    private String keyWord;

    @Override
    protected void creatPresent() {
        basePresenter = new InvitePresenter();
    }

    @OnClick({R2.id.tv_rightIcon, R2.id.iconDelete})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.tv_rightIcon) {
            PageJumpUtil.firstIsLoginThenJump(() -> {
                ARouter.getInstance().build(ARouterPath.MINE_INVITE_ACTIVITY).navigation();
            });
        } else if (id == R.id.iconDelete) {
            rl_tip.setVisibility(View.GONE);
        }
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_list;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, ll_to_publish);

        initRecyclerView();

//        搜索栏
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String keyWord = editable.toString().trim();
                if (keyWord.length() <= 0) {
                    searchkeyWord(null);//此处只是为了清除输入框中内容，搜索时候不搜关键字
                }
            }
        });

        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                //关闭软键盘
                KeyboardUtils.hideSoftInput(mActivity);

                String keyWord = etSearch.getText().toString().trim();
                searchkeyWord(keyWord);

                return true;
            }
            return false;
        });
    }

    /**
     * 搜索
     *
     * @param word
     */
    private void searchkeyWord(String word) {
        keyWord = word;

        getListData(1);
    }

    @Override
    public void onDebouncingClick(@NonNull @NotNull View view) {
        super.onDebouncingClick(view);
        int id = view.getId();
        if (id == R.id.ll_to_publish) {
            PageJumpUtil.firstRealNameValidThenJump(() -> PageJumpUtil.pageToPublishInvite(null, false));
        }
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        String locationCity = DiscoCacheUtils.getInstance().getCurrentCity();
        if (TextUtils.isEmpty(locationCity)) {
            llCity.setSelected(true);
            tvCity.setText(YXConfig.city);
            mSelectCity = YXConfig.city;
        } else {
            llCity.setSelected(true);
            tvCity.setText(locationCity);
            mSelectCity = locationCity;
        }


        //列表数据
        mListData = new ArrayList<>();

        //获取配置信息
        basePresenter.getConfigByType(YXConfig.TYPE_INVITE);

        //获取邀约分类列表
        HashMap params = new HashMap();

        params.put("current", 1);
        params.put("size", 500);//不分页
//        params.put("username", "string");
        basePresenter.meetPartnerTypeList(params);

        refreshLayout.autoRefresh(100);
    }

    ArrayList<InviteTypeModel> mList = new ArrayList<>();
    ArrayList<InviteTypeModel> mFilterList = new ArrayList<>();
    private String mStartTime, mEndTime, mSelectCity;
    ArrayList<String> ids = new ArrayList<>();

    @Override
    public void handInviteTypeList(List<InviteTypeModel> result) {
        List<InviteTypeModel> inviteTypeList = DiscoCacheUtils.getInstance().getInviteTypeList();
        if (null != inviteTypeList && inviteTypeList.size() > 0) {
            mList.clear();
            mList.addAll(inviteTypeList);
        }
    }

    @OnClick({R2.id.llCity, R2.id.llType, R2.id.llTime, R2.id.llFilter})
    public void openFilterMenu(View view) {
        InviteFilterDrawerPopup popup = new InviteFilterDrawerPopup(mContext, mSelectCity, mList, mFilterList, mStartTime, mEndTime);
        popup.setOnButtonConfirmClickListener((selectCity, reset, filterList, startTime, endTime) -> {
            if (reset) {
                mFilterList.clear();
                mStartTime = null;
                mEndTime = null;
                mSelectCity = null;
                ids.clear();
                String locationCity = DiscoCacheUtils.getInstance().getCurrentCity();
                if (TextUtils.isEmpty(locationCity)) {
                    llCity.setSelected(true);
                    tvCity.setText(YXConfig.city);
                    mSelectCity = YXConfig.city;
                } else {
                    llCity.setSelected(true);
                    tvCity.setText(locationCity);
                    mSelectCity = locationCity;
                }

                iconFilter.setSelected(false);
            } else {
                mFilterList = filterList;
                mStartTime = startTime;
                mEndTime = endTime;
                mSelectCity = selectCity;
                tvCity.setText(mSelectCity);

                iconFilter.setSelected(true);
            }

            if (null != mFilterList && mFilterList.size() > 0) {
                llType.setSelected(true);

                ids.clear();

                StringBuffer names = new StringBuffer();
                for (InviteTypeModel type : mFilterList) {
                    names.append(type.getName() + " ");
                    ids.add(type.getId());
                }
                tvType.setText(names);
            } else {
                llType.setSelected(false);
                tvType.setText("全部分类");
            }

            if (!TextUtils.isEmpty(mStartTime) && !TextUtils.isEmpty(mEndTime)) {
                llTime.setSelected(true);
                tvTime.setText(mStartTime + "-" + mEndTime);
            } else if (!TextUtils.isEmpty(mStartTime) && TextUtils.isEmpty(mEndTime)) {
                llTime.setSelected(true);
                tvTime.setText(mStartTime + "-不限制");
            } else if (TextUtils.isEmpty(mStartTime) && !TextUtils.isEmpty(mEndTime)) {
                llTime.setSelected(true);
                tvTime.setText("不限制-" + mEndTime);
            } else {
                llTime.setSelected(false);
                tvTime.setText("全部时间");
            }

            getListData(1);
        });
        new XPopup.Builder(mContext).popupPosition(PopupPosition.Right).hasStatusBarShadow(true).hasStatusBar(true).isLightStatusBar(false).asCustom(popup).show();
    }

    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            HashMap params = new HashMap();
            params.put("current", current);
            params.put("size", size);
            if (!TextUtils.isEmpty(mSelectCity)) {
                String city = mSelectCity.equals("全国") ? "" : mSelectCity;

                params.put("city", city);
            }
            if (!TextUtils.isEmpty(mStartTime)) {
                params.put("startTime", YHStringUtils.fitTimeZero(mStartTime));
            }

            if (!TextUtils.isEmpty(mEndTime)) {
                params.put("endTime", YHStringUtils.fitTimeZero(mEndTime));
            }

            if (ids.size() > 0) {
                params.put("meetPartnerTypeIds", ids);
            }
            params.put("keyword", keyWord);
            basePresenter.meetPartnerInvitePage(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(refreshLayout -> getListData(1));

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new InviteListAdapter(mContext, (type, item, pos) -> {
            currentPosition = pos;

            String contentID = item.getId();
            ARouter.getInstance().build(ARouterPath.INVITE_DETAIL_ACTIVITY)
                    .withBoolean("showTip", rl_tip.getVisibility() == View.VISIBLE)
                    .withString("id", contentID)
                    .navigation();
        });

        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        super.onMessageEvent(event);
        switch (event.getEventName()) {
            case CommonConstant.ACTION_HIDE_INVITE_TOP_TIP:
                rl_tip.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void handInviteList(ListResult<InviteModel> listResult) {
        if (null == listResult) {
            return;
        }

        List<InviteModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void handInviteConfig() {//顶部条
        InviteConfigModel inviteConfig = DiscoCacheUtils.getInstance().getInviteConfig();
        if (null != inviteConfig && !TextUtils.isEmpty(inviteConfig.getMeetPartnerTip()) && inviteConfig.getMeetPartnerOpenTip() == 1) {
            rl_tip.setVisibility(View.VISIBLE);
            tvTipContent.setText(inviteConfig.getMeetPartnerTip());
        } else {
            rl_tip.setVisibility(View.GONE);
        }
    }
}
