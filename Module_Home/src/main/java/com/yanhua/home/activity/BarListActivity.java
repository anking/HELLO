package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.AppBaseConfig;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.core.view.AutoClearEditText;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BarListAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;
import com.yanhua.home.view.BarAreaPopup;
import com.yanhua.home.view.BarSortPopup;
import com.yanhua.home.view.BarTypePopup;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = ARouterPath.BAR_LIST_ACTIVITY)
public class BarListActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_bar)
    RecyclerView rvBar;
    @BindView(R2.id.llCondition)
    LinearLayout llCondition;
    @BindView(R2.id.llType)
    LinearLayout llType;
    @BindView(R2.id.tvType)
    TextView tvType;
    @BindView(R2.id.llFilter)
    LinearLayout llFilter;
    @BindView(R2.id.tvFilter)
    TextView tvFilter;
    @BindView(R2.id.llCity)
    LinearLayout llCity;
    @BindView(R2.id.tvCity)
    TextView tvCity;
    @BindView(R2.id.et_search)
    AutoClearEditText etSearch;
    @BindView(R2.id.tvLocation)
    TextView tvLocation;

    private List<BarModel> bars;
    private BarListAdapter mAdapter;
    private int total;
    private BarTypePopup barTypePopup;
    private String selectedType;
    private BarSortPopup barSortPopup;
    private int selectedSort = -1;
    private BarAreaPopup barAreaPopup;
    private String keywordLike;
    private String barTypeId;
    private List<CommonReasonModel> barTypeList;
    private List<CityModel> cityAreaList;
    private String sortingRules;
    private String city;
    private String latitude;
    private String longitude;
    private String cityId;
    private String areaId;
    private String area;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_list;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        bars = new ArrayList<>();
        city = DiscoCacheUtils.getInstance().getCurrentBarCity();
        if (TextUtils.isEmpty(city)) {
            city = YXConfig.city;
            latitude = YXConfig.latitude;
            longitude = YXConfig.longitude;
        } else {
            latitude = DiscoCacheUtils.getInstance().getBarLat();
            longitude = DiscoCacheUtils.getInstance().getBarLng();
        }
        tvLocation.setText(city);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getBarList();
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (bars.size() < total) {
                current++;
                getBarList();
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        rvBar.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BarListAdapter(this);
        rvBar.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            BarModel barModel = bars.get(pos);
            ARouter.getInstance().build(ARouterPath.BAR_DETAIL_ACTIVITY)
                    .withSerializable("barId", barModel.getBarId())
                    .navigation();
        });
        mAdapter.setOnLoveBarListener(pos -> {
            BarModel barModel = bars.get(pos);
            PageJumpUtil.firstIsLoginThenJump(() -> {
                basePresenter.doLoveOrNotBar(barModel.getBarId(), pos);
            });
        });
        etSearch.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard();
                current = 1;
                keywordLike = etSearch.getText().toString().trim();
                getBarList();
                return true;
            }
            return false;
        });
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        // 获取区级数据
        basePresenter.getCityOrArea();
        // 获取酒吧类型
        HashMap<String, Object> params = new HashMap<>();
        params.put("type", AppBaseConfig.TYPE_BAR_TYPE.getValue());
        basePresenter.getCommonReasonList(params);
    }

    private void getBarList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("current", current);
        params.put("size", size);
        if (!TextUtils.isEmpty(keywordLike)) {
            params.put("keywordLike", keywordLike);
        }
        if (!TextUtils.isEmpty(barTypeId)) {
            params.put("barTypeId", barTypeId);
        }
        if (!TextUtils.isEmpty(sortingRules)) {
            params.put("sortingRules", sortingRules);
        }
        if (!TextUtils.isEmpty(latitude)) {
            params.put("latitude", Double.parseDouble(latitude));
        }
        if (!TextUtils.isEmpty(longitude)) {
            params.put("longitude", Double.parseDouble(longitude));
        }
        if (!TextUtils.isEmpty(cityId)) {
            params.put("cityId", cityId);
        }
        if (!TextUtils.isEmpty(areaId)) {
            params.put("areaId", areaId);
        }
        basePresenter.getBarList(params);
    }

    @OnClick({R2.id.llCity, R2.id.llType, R2.id.llFilter, R2.id.llLocation})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.llCity) {
            if (cityAreaList == null) return;
            barAreaPopup = new BarAreaPopup(this, city, area, cityAreaList);
            barAreaPopup.setOnSelectAreaListener(cityModel -> {
                llCity.setSelected(true);
                if (cityModel == null) {
                    // 全部地区
                    area = null;
                    tvCity.setText("全部地区");
                    for (CityModel c : cityAreaList) {
                        if (c.getAreaName().equals(city)) {
                            areaId = null;
                            cityId = c.getId();
                            latitude = c.getLat();
                            longitude = c.getLng();
                        }
                    }
                } else {
                    area = cityModel.getAreaName();
                    tvCity.setText(area);
                    areaId = cityModel.getId();
                    latitude = cityModel.getLat();
                    longitude = cityModel.getLng();
                }
                getBarList();
            });
            new XPopup.Builder(this).atView(llCondition).asCustom(barAreaPopup).show();
        } else if (id == R.id.llType) {
            barTypePopup = new BarTypePopup(this, selectedType, barTypeList);
            barTypePopup.setOnSelectTypeListener(model -> {
                current = 1;
                llType.setSelected(true);
                if (model == null) {
                    barTypeId = null;
                    selectedType = null;
                    tvType.setText("全部分类");
                } else {
                    selectedType = model.getContent();
                    barTypeId = model.getId();
                    tvType.setText(selectedType);
                }
                getBarList();
            });
            new XPopup.Builder(this).atView(llCondition).asCustom(barTypePopup).show();
        } else if (id == R.id.llFilter) {
            barSortPopup = new BarSortPopup(this, selectedSort);
            barSortPopup.setOnSelectTypeListener(pos -> {
                current = 1;
                selectedSort = pos;
                llFilter.setSelected(true);
                switch (pos) {
                    case 0:
                        tvFilter.setText("距离优先");
                        sortingRules = "distanceAsc";
                        break;
                    case 1:
                        tvFilter.setText("点赞优先");
                        sortingRules = "popularityDesc";
                        break;
                }
                getBarList();
            });
            new XPopup.Builder(this).atView(llCondition).asCustom(barSortPopup).show();
        } else if (id == R.id.llLocation) {
            ARouter.getInstance().build(ARouterPath.BAR_CITY_ACTIVITY)
                    .withString("city", tvLocation.getText().toString())
                    .withString("location", DiscoCacheUtils.getInstance().getCurrentBarCity())
                    .withString("plongitude", DiscoCacheUtils.getInstance().getBarLng())
                    .withString("platitude", DiscoCacheUtils.getInstance().getBarLat())
                    .navigation();
        }
    }

    @Override
    public void handleCityOrAreaSuccess(List<CityModel> list) {
        this.cityAreaList = list;
        for (CityModel c : cityAreaList) {
            if (c.getAreaName().equals(city)) {
                cityId = c.getId();
            }
        }
        // 获取酒吧列表
        refreshLayout.autoRefresh(100);
    }

    @Override
    public void handleBarTypeListSuccess(List<CommonReasonModel> list) {
        barTypeList = list;
    }

    @Override
    public void handleBarListSuccess(ListResult<BarModel> list) {
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            bars.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null) {
            List<BarModel> records = list.getRecords();
            total = list.getTotal();
            bars.addAll(records);
        }
        mAdapter.setItems(bars);
    }

    @Override
    public void handleLoveOrNotBarSuccess(String msg, int pos) {
        ToastUtils.showShort(msg);
        BarModel barModel = bars.get(pos);
        int popularityStatus = barModel.getPopularityStatus();
        int popularityNum = barModel.getPopularityNum();
        barModel.setPopularityStatus(popularityStatus == 1 ? 0 : 1);
        barModel.setPopularityNum(popularityStatus == 1 ? popularityNum - 1 : popularityNum + 1);
        mAdapter.notifyItemChanged(pos);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventName().equals(EventName.SELECT_BAR_CITY)) {
            CityModel cityModel = (CityModel) event.getSerializable();
            cityId = cityModel.getId();
            city = cityModel.getAreaName();
            latitude = cityModel.getLat();
            longitude = cityModel.getLng();
            tvLocation.setText(city);
            getBarList();
            DiscoCacheUtils.getInstance().setCurrentBarCity(city);
            DiscoCacheUtils.getInstance().setBarLng(longitude);
            DiscoCacheUtils.getInstance().setBarLat(latitude);
        }
    }

}