package com.yanhua.home.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.blankj.utilcode.util.ToastUtils;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.OnValueChangeTextWatcher;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.QuitInviteReasonAdapter;
import com.yanhua.home.presenter.InviteDetailPresenter;
import com.yanhua.home.presenter.contract.InviteDetailContract;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 个人数量
 *
 * @author Administrator
 */
@Route(path = ARouterPath.INVITE_CANCEL_ACTIVITY)
public class InviteCancelJoinActivity extends BaseMvpActivity<InviteDetailPresenter> implements InviteDetailContract.IView {
    @Autowired
    String inviteUserId;//用户报名id

    @Autowired
    String inviteId;//邀約的id
    @Autowired
    int inviteUserType;//  * 约伴取消原因(1:报名者, 2:邀约者)

    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;
    @BindView(R2.id.etOtherReason)
    EditText etOtherReason;
    @BindView(R2.id.tvOtherReasonNum)
    TextView tvOtherReasonNum;
    @BindView(R2.id.llOther)
    LinearLayout llOther;

    @BindView(R2.id.tvOK)
    TextView tvOK;


    QuitInviteReasonAdapter mAdapter;

    private CommonReasonModel mSelectItem;

    @Override
    protected void creatPresent() {
        basePresenter = new InviteDetailPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_invite_cancel_join;
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        setTitle(inviteUserType == 1 ? "取消报名" : "取消邀约");

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new QuitInviteReasonAdapter(mContext);
        rvContent.setLayoutManager(manager);
        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            mSelectItem = mAdapter.getmDataList().get(pos);
            mAdapter.setSelectId(mSelectItem.getId());

            if (mSelectItem.getContent().equals("其他")) {
                llOther.setVisibility(View.VISIBLE);
            } else {
                llOther.setVisibility(View.GONE);
            }
            tvOK.setEnabled(true);
        });

        etOtherReason.addTextChangedListener(new OnValueChangeTextWatcher().setOnValueChangeListener(s -> {
            int left = 300 - s.length();
            tvOtherReasonNum.setText(String.valueOf(left));
        }));
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        tvOK.setEnabled(false);


        if (inviteUserType != 3) {//  * 约伴取消原因(1:报名者, 2:邀约者)
            HashMap<String, Object> params = new HashMap<>();
            params.put("current", 1);
            params.put("size", 100);
            params.put("type", 6);//5:约伴停约原因, 6:约伴取消原因,
            params.put("extendId", inviteUserType);//     * 约伴取消原因(1:报名者, 2:邀约者)
            basePresenter.getCommonReasonList(params);///sysadmin/app/baseConfig/list
        } else {
            HashMap<String, Object> params = new HashMap<>();
            params.put("current", 1);
            params.put("size", 100);
            params.put("type", 6);//5:约伴停约原因, 6:约伴取消原因,
            params.put("extendId", 2);//     * 约伴取消原因(1:报名者, 2:邀约者)
            basePresenter.getCommonReasonList(params);///sysadmin/app/baseConfig/list
        }
    }

    @OnClick(R2.id.tvOK)
    public void onClickView() {//点击保存
        HashMap<String, Object> params = new HashMap<>();
        if (null != mSelectItem) {
            params.put("cancelReasonId", mSelectItem.getId());

            if (mSelectItem.getContent().equals("其他")) {
                String cancelReason = etOtherReason.getText().toString().trim();

                if (TextUtils.isEmpty(cancelReason)) {
                    ToastUtils.showShort("请先输入其他原因");
                    return;
                }

                params.put("cancelReason", cancelReason);
            } else {
                params.put("cancelReason", mSelectItem.getContent());
            }

            if (!TextUtils.isEmpty(inviteUserId)) {
                //移除用户报名id
                params.put("id", inviteUserId);//取消原因id
            }
        }

        params.put("inviteId", inviteId);
        params.put("userId", UserManager.getInstance().getUserId());

        if (inviteUserType == 1) {////  * 约伴取消原因(1:报名者, 2:邀约者)
            basePresenter.meetPartnerInviteSignOut(params);
        } else if (inviteUserType == 2) {
            basePresenter.meetPartnerInviteRemoveSigned(params);
        } else if (inviteUserType == 3) {
            basePresenter.meetPartnerStopReason(params);
        }
    }

    @Override
    public void handInviteSignResult(boolean signin) {
        if (!signin) {
            ToastUtils.showShort(inviteUserType == 1 ? "取消报名成功" : "取消邀约成功");

            if (inviteUserType != 1) {
                EventBus.getDefault().post(CommonConstant.ACTION_REFRESH_INVITE_JOINDE);
            }

            onBackPressed();
        }
    }

    @Override
    public void handCommonReason(List<CommonReasonModel> data) {
        if (null != data) {
            rvContent.setVisibility(View.VISIBLE);

            mAdapter.setItems(data);
        } else {
            rvContent.setVisibility(View.GONE);
        }
    }
}
