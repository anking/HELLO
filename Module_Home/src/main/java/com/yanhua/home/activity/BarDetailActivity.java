package com.yanhua.home.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.base.view.ShareBoardPopup;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.MapLocationDetailModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.ShareObjectModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.RongIMAppMsg;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.common.widget.MoreActionBoard;
import com.yanhua.common.widget.PhonePickerPopup;
import com.yanhua.common.widget.SelectShareListBottomPop;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BarDetailQuestionAdapter;
import com.yanhua.home.adapter.HomeSuggestAdapter;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;
import com.yanhua.rong.msgprovider.NBarMessage;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

@Route(path = ARouterPath.BAR_DETAIL_ACTIVITY)
public class BarDetailActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.cltoolbar)
    CollapsingToolbarLayout cltoolbar;
    @BindView(R2.id.tv_name)
    TextView tvTitle;
    @BindView(R2.id.bannerPhotoWall)
    Banner bannerPhotoWall;
    @BindView(R2.id.iconMore)
    AliIconFontTextView iconMore;
    @BindView(R2.id.iconFav)
    AliIconFontTextView iconFav;
    @BindView(R2.id.tv_left)
    AliIconFontTextView tvLeft;
    @BindView(R2.id.tvBarName)
    TextView tvBarName;
    @BindView(R2.id.ivBarType)
    ImageView ivBarType;
    @BindView(R2.id.tflBarTrait)
    TagFlowLayout tflBarTrait;
    @BindView(R2.id.tvBusinessTime)
    TextView tvBusinessTime;
    @BindView(R2.id.tvBarAddress)
    TextView tvBarAddress;
    @BindView(R2.id.llNoQA)
    LinearLayout llNoQA;
    @BindView(R2.id.llHasQA)
    LinearLayout llHasQA;
    @BindView(R2.id.rv_qa)
    RecyclerView rvQA;
    @BindView(R2.id.tvQuestionNum)
    TextView tvQuestionNum;
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rvContent)
    RecyclerView rvContent;
    @BindView(R2.id.tvRecommandTitle)
    TextView tvRecommandTitle;
    @BindView(R2.id.tvDistance)
    TextView tvDistance;

    @Autowired
    String barId;

    private BarModel barModel;
    private float alpha;
    private PhonePickerPopup phonePickerPopup;
    private BarDetailQuestionAdapter mAdapter;
    private HomeSuggestAdapter suggestAdapter;
    private List<MomentListModel> mListData;
    private int total;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_detail;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        rvQA.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new BarDetailQuestionAdapter(this);
        rvQA.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((itemView, pos) -> {
            List<BarQAModel> barQAList = mAdapter.getmDataList();
            BarQAModel qaModel = barQAList.get(pos);
            ARouter.getInstance()
                    .build(ARouterPath.BAR_QUESTION_DETAIL_ACTIVITY)
                    .withString("askId", qaModel.getParentAskedId())
                    .navigation();
        });

        mListData = new ArrayList<>();
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);
        refreshLayout.setOnLoadMoreListener(refreshLayout -> {
            if (mListData.size() < total) {
                current++;
                getRecommandData(current);
            } else {
                refreshLayout.finishLoadMoreWithNoMoreData();
            }
        });
        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        suggestAdapter = new HomeSuggestAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(suggestAdapter);
    }

    private void initBarView() {
        String rotationImgs = barModel.getRotationImgs();
        String[] imgs = rotationImgs.split(",");
        List<String> list = Arrays.asList(imgs);
        bannerPhotoWall.setAdapter(new BannerImageAdapter<String>(list) {
            @Override
            public void onBindView(BannerImageHolder holder, String url, int position, int size) {
                ImageLoaderUtil.loadImg(holder.imageView, url);
            }
        }).addBannerLifecycleObserver(this)
                .setLoopTime(3000)
                .setIndicator(new CircleIndicator(mContext))
                .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.gray2))
                .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.white))
                .setIndicatorWidth(DisplayUtils.dip2px(mContext, 5), DisplayUtils.dip2px(mContext, 10))
                .setIndicatorGravity(IndicatorConfig.Direction.RIGHT)
                .setIndicatorSpace(DisplayUtils.dip2px(mContext, 6))
                .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, DisplayUtils.dip2px(mContext, 12), DisplayUtils.dip2px(mContext, 12)));
        tvBarName.setText(barModel.getName());
        tvTitle.setText(barModel.getName());
        // 夜店、清吧、ktv、餐吧
        switch (barModel.getBarType()) {
            case "夜店":
                ivBarType.setImageResource(R.mipmap.ic_night_club);
                break;
            case "清吧":
                ivBarType.setImageResource(R.mipmap.ic_sober_bar);
                break;
            case "餐吧":
                ivBarType.setImageResource(R.mipmap.ic_dinnng_bar);
                break;
            case "KTV":
                ivBarType.setImageResource(R.mipmap.ic_ktv);
                break;
            case "Live House":
                ivBarType.setImageResource(R.mipmap.ic_live_house);
                break;
        }

        String barTrait = barModel.getBarTrait();
        String[] barTraitArr = barTrait.split(",");
        tflBarTrait.setAdapter(new TagAdapter<String>(Arrays.asList(barTraitArr)) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_bar_detail_trait_tag, parent, false);
                //获取标签
                TextView tvTag = view.findViewById(R.id.tvTag);
                tvTag.setText(s);
                return view;
            }
        });
        tvBusinessTime.setText(barModel.getBusinessTime());
        tvBarAddress.setText(barModel.getAddress());
        tvDistance.setText(String.format("距你%s", YHStringUtils.formatDistance(barModel.getDistance())));
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        if (!TextUtils.isEmpty(barId)) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("latitude", YXConfig.latitude);
            params.put("longitude", YXConfig.longitude);
            basePresenter.getBarDetail(barId, params);
        }
        // 查询关于酒吧的问题列表，3条
        getBarQAList();
        // 获取推荐数据
        getRecommandData(1);
    }

    private void getBarQAList() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("barId", barModel == null ? barId : barModel.getBarId());
        params.put("current", 1);
        params.put("size", 3);
        basePresenter.getBarQAList(params);
    }

    private void getRecommandData(int page) {
        current = page;
        basePresenter.getBestNoteList("", current, size);
    }

    private void setTopView() {
        int width = DisplayUtils.getScreenWidth(mContext);
        ViewGroup.LayoutParams imageLayoutParams = bannerPhotoWall.getLayoutParams();
        imageLayoutParams.width = width;
        imageLayoutParams.height = (int) (width * 0.75);
        bannerPhotoWall.setLayoutParams(imageLayoutParams);

        mAppBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float scrollRangle = appBarLayout.getTotalScrollRange();
            float scroll2top = scrollRangle + verticalOffset;
            float rangle = scroll2top / scrollRangle;

            boolean collectStatus = barModel.getCollectStatus() == 1;
            alpha = 1 - rangle;
            if (alpha > 0 && alpha < 0.3) {
                toolbar.setAlpha(1 - alpha);
                toolbar.setBackgroundResource(R.color.transparent);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_black);
                iconFav.setTextColor(mActivity.getResources().getColor(collectStatus ? R.color.theme : R.color.white));
                iconFav.setBackgroundResource(R.drawable.shape_half_r_black);
                tvTitle.setVisibility(View.GONE);
            } else if (alpha < 1 && alpha >= 0.3) {
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.white);
                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);
                iconFav.setTextColor(mActivity.getResources().getColor(collectStatus ? R.color.theme : R.color.mainWord));
                iconFav.setBackgroundResource(R.drawable.shape_half_r_tran);
                tvTitle.setVisibility(View.VISIBLE);
            } else if (alpha == 1) {//避免滑动过快还显示半透明状态
                mAppBarLayout.postDelayed(() -> {
                    toolbar.setAlpha(1 - rangle);
                    toolbar.setBackgroundResource(R.color.white);

                    tvLeft.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    tvLeft.setBackgroundResource(R.drawable.shape_half_r_tran);
                    iconMore.setTextColor(mActivity.getResources().getColor(R.color.mainWord));
                    iconMore.setBackgroundResource(R.drawable.shape_half_r_tran);

                    iconFav.setTextColor(mActivity.getResources().getColor(collectStatus ? R.color.theme : R.color.mainWord));
                    iconFav.setSelected(collectStatus);
                    iconFav.setBackgroundResource(R.drawable.shape_half_r_tran);

                    tvTitle.setVisibility(View.VISIBLE);
                }, 100);
            } else if (alpha == 0) {//避免滑动过快还显示半透明状态
                toolbar.setAlpha(1);
                toolbar.setBackgroundResource(R.color.transparent);

                tvLeft.setTextColor(mActivity.getResources().getColor(R.color.white));
                tvLeft.setBackgroundResource(R.drawable.shape_half_r_black);
                iconMore.setTextColor(mActivity.getResources().getColor(R.color.white));
                iconFav.setBackgroundResource(R.drawable.shape_half_r_black);
                iconFav.setTextColor(mActivity.getResources().getColor(collectStatus ? R.color.theme : R.color.white));

                tvTitle.setVisibility(View.GONE);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setOutlineProvider(null);
            cltoolbar.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
    }

    @OnClick({R2.id.iconPhone, R2.id.iconMap, R2.id.tvGoAsk, R2.id.llCheckAllQ,
            R2.id.iconMore, R2.id.iconFav})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.iconPhone) {
            String contact = barModel.getContact();
            String[] split = contact.split(",");
            phonePickerPopup = new PhonePickerPopup(this, Arrays.asList(split));
            new XPopup.Builder(this).enableDrag(false).asCustom(phonePickerPopup).show();
        } else if (id == R.id.iconMap) {
            MapLocationDetailModel locationDetailModel = new MapLocationDetailModel();
            locationDetailModel.setAddress(barModel.getAddress());
            locationDetailModel.setName(barModel.getName());
            locationDetailModel.setLatitude(barModel.getLatitude());
            locationDetailModel.setLongitude(barModel.getLongitude());
            ARouter.getInstance()
                    .build(ARouterPath.SHOW_MAP_LOCATION_ACTIVITY)
                    .withSerializable("addressDetail", locationDetailModel)
                    .navigation();
        } else if (id == R.id.tvGoAsk) {
            if (FastClickUtil.isFastClick(500)) {
                ARouter.getInstance()
                        .build(ARouterPath.BAR_POST_QA_ACTIVITY)
                        .withSerializable("barModel", barModel)
                        .withInt("type", 1)
                        .navigation();
            }
        } else if (id == R.id.llCheckAllQ) {
            ARouter.getInstance()
                    .build(ARouterPath.BAR_QUESTION_LIST_ACTIVITY)
                    .withSerializable("barModel", barModel)
                    .navigation();
        } else if (id == R.id.iconFav) {
            basePresenter.updateCollectContent(barModel.getBarId(), YXConfig.TYPE_9BAR);
        } else if (id == R.id.iconMore) {
            if (null != barModel) {
                String barId = barModel.getBarId();//酒吧id
                String barTitle = barModel.getName();//酒吧名
                String barAddress = barModel.getAddress(); //酒吧地址
                String barPicUrl = barModel.getBarPrimaryImg();//酒吧主图
                MoreActionBoard.share9BarDetail(mActivity, barId, barTitle, barAddress, barPicUrl, (position, tp) -> {

                    switch (position) {

                        case ShareBoardPopup.BOARD_SHARE_APP:
                            //.isLightStatusBar(true)不会改变状态栏的颜色
                            if (null == barModel) return;
                            SelectShareListBottomPop sharePopup = new SelectShareListBottomPop(mContext, barModel, YXConfig.TYPE_9BAR, (selecteListData, msg) -> shareAppMsg(selecteListData, msg));
                            new XPopup.Builder(mContext).enableDrag(false).isLightStatusBar(true).hasShadowBg(false)
                                    .statusBarBgColor(mContext.getResources().getColor(R.color.white))
                                    .autoFocusEditText(false)
                                    .moveUpToKeyboard(false).asCustom(sharePopup).show();
                            break;
                    }
                });
            }
        }
    }

    @Override
    public void handleBarDetailSuccess(BarModel model) {
        if (model != null) {
            barModel = model;
            setTopView();
            initBarView();

        }
    }

    private void shareAppMsg(List<ShareObjectModel> listData, String msg) {
        NBarMessage messageContent = new NBarMessage();

        messageContent.setId(barModel.getBarId());
        messageContent.setCoverUrl(barModel.getBarPrimaryImg());
        messageContent.setTitle(barModel.getName());
        messageContent.setAddress(barModel.getAddress());
        messageContent.setBarType(barModel.getBarType());
        messageContent.setType(YXConfig.TYPE_9BAR);


        for (int i = 0; i < listData.size(); i++) {
            int finalI = i;

            tvTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ShareObjectModel shareObject = listData.get(finalI);
                    String targetId = shareObject.getUserId();
                    boolean isGroup = shareObject.isGroup();

                    Conversation.ConversationType ctype = isGroup ? Conversation.ConversationType.GROUP : Conversation.ConversationType.PRIVATE;

                    Message message = Message.obtain(targetId, ctype, messageContent);

                    RongIMAppMsg.sendCustomMessage(mContext, message, finalI == 0, YXConfig.getTypeName(YXConfig.TYPE_9BAR));

                    if (!TextUtils.isEmpty(msg)) {
                        TextMessage textMessage = TextMessage.obtain(msg);
                        Message textMsg = Message.obtain(targetId, ctype, textMessage);
                        RongIMAppMsg.sendCustomMessage(mContext, textMsg, false, msg);
                    }
                }
            }, 500 * i);
        }
    }


    @Override
    public void handleBarQAListSuccess(ListResult<BarQAModel> list) {
        if (list != null) {
            List<BarQAModel> records = list.getRecords();
            int total = list.getTotal();
            if (total == 0 || records == null || records.isEmpty()) {
                llNoQA.setVisibility(View.VISIBLE);
                llHasQA.setVisibility(View.GONE);
            } else {
                llNoQA.setVisibility(View.GONE);
                llHasQA.setVisibility(View.VISIBLE);
                mAdapter.setItems(records);
                tvQuestionNum.setText(String.format("大家问（%d）", total));
            }
        } else {
            llNoQA.setVisibility(View.VISIBLE);
            llHasQA.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();
        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            tvRecommandTitle.setVisibility(View.VISIBLE);
            mListData.addAll(list);
            suggestAdapter.setItems(mListData);
        } else {
            tvRecommandTitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateCollectContentSuccess(String msg) {
        ToastUtils.showShort(msg);
        barModel.setCollectStatus(barModel.getCollectStatus() == 1 ? 0 : 1);
        if (alpha > 0 && alpha < 0.3) {
            iconFav.setTextColor(mActivity.getResources().getColor(barModel.getCollectStatus() == 1 ? R.color.theme : R.color.white));
        } else if (alpha < 1 && alpha >= 0.3) {
            iconFav.setTextColor(mActivity.getResources().getColor(barModel.getCollectStatus() == 1 ? R.color.theme : R.color.mainWord));
        } else if (alpha == 1) {
            iconFav.setTextColor(mActivity.getResources().getColor(barModel.getCollectStatus() == 1 ? R.color.theme : R.color.mainWord));
        } else if (alpha == 0) {
            iconFav.setTextColor(mActivity.getResources().getColor(barModel.getCollectStatus() == 1 ? R.color.theme : R.color.white));
        }
    }

}