package com.yanhua.home.activity;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.mvp.BaseMvpActivity;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.BarPresenter;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 提问/回答页面
 */
@Route(path = ARouterPath.BAR_POST_QA_ACTIVITY)
public class BarPostQAActivity extends BaseMvpActivity<BarPresenter> implements BarContract.IView {

    @BindView(R2.id.tv_header)
    TextView tvHeader;
    @BindView(R2.id.tv_right)
    TextView tvRight;
    @BindView(R2.id.llQuestion)
    LinearLayout llQuestion;
    @BindView(R2.id.ivBarImg)
    ShapeableImageView ivBarImg;
    @BindView(R2.id.tvBarName)
    TextView tvBarName;
    @BindView(R2.id.tvBrowseNum)
    TextView tvBrowseNum;
    @BindView(R2.id.editText)
    EditText editText;
    @BindView(R2.id.tvTextLimit)
    TextView tvTextLimit;
    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;
    @BindView(R2.id.tvQuestionName)
    TextView tvQuestionName;

    @Autowired
    BarModel barModel;
    @Autowired
    BarQAModel qModel;
    @Autowired(name = "type", desc = "1提问 2回答")
    int type;

    @Override
    protected void creatPresent() {
        basePresenter = new BarPresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_bar_post_qa;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);
        tvRight.setText("发布");
        tvRight.setEnabled(false);
        tvHeader.setText(type == 1 ? "提问" : "回答");
        if (type == 1) {
            llQuestion.setVisibility(View.GONE);
            editText.setHint("写下您的提问，体验充足的人会为你解答哦～");
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
            tvTextLimit.setText("50");
        } else {
            llQuestion.setVisibility(View.VISIBLE);
            editText.setHint("我知道，我来回答～");
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(200)});
            tvTextLimit.setText("200");
            if (qModel != null) {
                tvQuestionName.setText(qModel.getParentAskedDesc());
            }
        }
        ImageLoaderUtil.loadImgCenterCrop(ivBarImg, barModel.getBarPrimaryImg());
        tvBarName.setText(barModel.getName());
        tvBrowseNum.setText(String.format("%d人浏览", barModel.getBrowseNum()));
    }

    @Override
    public void initData(@Nullable Bundle bundle) {
        super.initData(bundle);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                tvRight.setEnabled(!TextUtils.isEmpty(text) && text.length() >= 3);
                if (type == 1) {
                    tvTextLimit.setText(TextUtils.isEmpty(text) ? "50" : String.valueOf(50 - s.length()));
                } else if (type == 2) {
                    tvTextLimit.setText(TextUtils.isEmpty(text) ? "200" : String.valueOf(200 - s.length()));
                }
            }
        });
    }

    @OnClick({R2.id.rlBarInfo, R2.id.llHideName, R2.id.tv_right})
    public void clickView(View view) {
        int id = view.getId();
        if (id == R.id.rlBarInfo) {
            ARouter.getInstance()
                    .build(ARouterPath.BAR_DETAIL_ACTIVITY)
                    .withString("barId", barModel.getBarId())
                    .navigation();
        } else if (id == R.id.llHideName) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        } else if (id == R.id.tv_right) {
            if (type == 1) {
                toPublishAsk();
            } else if (type == 2) {
                toPublishAnswer();
            }
        }
    }

    private void toPublishAsk() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("barInfoId", barModel.getBarId());
        params.put("isShow", checkboxPro.isChecked() ? 1 : 0);
        params.put("remark", editText.getText().toString());
        params.put("replyStatus", 0);
        basePresenter.postMyQA(1, params);
    }

    private void toPublishAnswer() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("barInfoId", barModel.getBarId());
        params.put("parentAskedId", qModel.getParentAskedId());
        params.put("isShow", checkboxPro.isChecked() ? 1 : 0);
        params.put("remark", editText.getText().toString());
        params.put("replyStatus", 1);
        basePresenter.postMyQA(2, params);
    }

    @Override
    public void handlePostQASuccess(int type, String data) {
        if (type == 1) {
            // 提问成功
            ToastUtils.showLong(data);
            new Handler().postDelayed(() -> finish(), 1000);
        } else if (type == 2) {
            // 回答成功
            ToastUtils.showLong(data);
            new Handler().postDelayed(() -> finish(), 1000);
        }
    }
}