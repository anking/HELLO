package com.yanhua.home.view;

import android.content.Context;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.lxj.xpopup.core.CenterPopupView;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.home.R;
import com.yanhua.common.adapter.AdvertiseBannerAdapter;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.RectangleIndicator;

import java.util.List;

public class HomePagePopAd extends CenterPopupView {

    private Context mContext;
    private List<AdvertiseModel> advertiseList;

    FrameLayout flPopAd;
    Banner bannerPopAd;
    AliIconFontTextView iconClose;

    int mWidth;
    int mHeight;

    public HomePagePopAd(@NonNull Context context, List<AdvertiseModel> adList, int width, int height, OnClickSelectItem onClickSelectItem) {
        super(context);
        mContext = context;
        advertiseList = adList;
        mWidth = width;
        mHeight = height;
        mListener = onClickSelectItem;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.center_popup_home_ad;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();
    }

    private void initView() {
        flPopAd = findViewById(R.id.flPopAd);
        bannerPopAd = findViewById(R.id.bannerPopAd);
        iconClose = findViewById(R.id.iconClose);

        iconClose.setOnClickListener(v -> dismiss());

        AdvertiseBannerAdapter adapter = new AdvertiseBannerAdapter(advertiseList, mContext);

        //展示滚动效果
        bannerPopAd.setAdapter(adapter);

        if (null != advertiseList && advertiseList.size() > 1) {
            bannerPopAd.addBannerLifecycleObserver(this)
                    .setLoopTime(3000)
                    .isAutoLoop(false)
                    .setIndicator(new RectangleIndicator(mContext))
                    .setIndicatorRadius(DisplayUtils.dip2px(mContext, 6))
                    .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                    .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.sub_first))
                    .setIndicatorHeight(DisplayUtils.dip2px(mContext, 6))
                    .setIndicatorWidth(DisplayUtils.dip2px(mContext, 6), DisplayUtils.dip2px(mContext, 12))
                    .setIndicatorGravity(IndicatorConfig.Direction.CENTER)
                    .setIndicatorSpace(DisplayUtils.dip2px(mContext, 3))
                    .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 9)));
        }

        bannerPopAd.setOnBannerListener((data, position) -> {
            AdvertiseModel clickItem = (AdvertiseModel) data;
            mListener.onSelectItem(clickItem);
        });
    }

    private OnClickSelectItem mListener;

    public interface OnClickSelectItem {
        void onSelectItem(AdvertiseModel item);
    }

    @Override
    protected int getPopupHeight() {
        return mHeight;
    }

    @Override
    protected int getPopupWidth() {
        return mWidth;
    }

    @Override
    protected int getMaxHeight() {
        return mHeight;
    }

    @Override
    protected int getMaxWidth() {
        return mWidth;
    }
}
