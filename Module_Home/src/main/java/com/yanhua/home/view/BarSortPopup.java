package com.yanhua.home.view;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.home.R;

public class BarSortPopup extends PartShadowPopupView {

    private int selectedPos = -1;
    private TextView tvDistanceFirst, tvLoveFirst;

    public BarSortPopup(@NonNull Context context, int selectedPos) {
        super(context);
        this.selectedPos = selectedPos;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_bar_sort;
    }

    private OnSelectTypeListener listener;

    public interface OnSelectTypeListener {
        void selectType(int pos);
    }

    public void setOnSelectTypeListener(OnSelectTypeListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        tvDistanceFirst = findViewById(R.id.tvDistanceFirst);
        tvLoveFirst = findViewById(R.id.tvLoveFirst);
        clearSelected();
        switch (selectedPos) {
            case 0:
                tvDistanceFirst.setSelected(true);
                break;
            case 1:
                tvLoveFirst.setSelected(true);
                break;
        }
        tvDistanceFirst.setOnClickListener(v -> {
            clearSelected();
            tvDistanceFirst.setSelected(true);
            dismiss();
            if (listener != null) {
                listener.selectType(0);
            }
        });
        tvLoveFirst.setOnClickListener(v -> {
            clearSelected();
            tvLoveFirst.setSelected(true);
            dismiss();
            if (listener != null) {
                listener.selectType(1);
            }
        });
    }

    private void clearSelected() {
        tvDistanceFirst.setSelected(false);
        tvLoveFirst.setSelected(false);
    }

}
