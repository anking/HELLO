package com.yanhua.home.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ScreenUtils;
import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CityModel;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.List;

import butterknife.BindView;

public class BarAreaPopup extends PartShadowPopupView {

    private Context mContext;
    private RecyclerView rvArea;
    private String city, area;
    private List<CityModel> cityAreaList;
    private List<CityModel> subCity;
    private TextView tvAllArea;

    public BarAreaPopup(@NonNull Context context, String city, String area, List<CityModel> cityAreaList) {
        super(context);
        this.mContext = context;
        this.city = city;
        this.area = area;
        this.cityAreaList = cityAreaList;
    }

    private OnSelectAreaListener listener;

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_bar_area;
    }

    public interface OnSelectAreaListener {
        void selectArea(CityModel cityModel);
    }

    public void setOnSelectAreaListener(OnSelectAreaListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        rvArea = findViewById(R.id.rv_area);
        rvArea.setLayoutManager(new LinearLayoutManager(mContext));
        AreaAdapter areaAdapter = new AreaAdapter(mContext);
        View header = LayoutInflater.from(mContext).inflate(R.layout.layout_header_bar_filter_area, null);
        header.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tvAllArea = header.findViewById(R.id.tvAllArea);
        if (TextUtils.isEmpty(area)) {
            tvAllArea.setSelected(true);
        }
        tvAllArea.setOnClickListener(v -> {
            if (listener != null) {
                dismiss();
                listener.selectArea(null);
            }
        });
        areaAdapter.addHeaderView(header);
        rvArea.setAdapter(areaAdapter);
        for (CityModel c : cityAreaList) {
            if (c.getAreaName().equals(city)) {
                subCity = c.getSubCity();
                areaAdapter.setItems(subCity);
            }
        }
        areaAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null && pos > 0) {
                area = subCity.get(pos - 1).getAreaName();
                areaAdapter.notifyDataSetChanged();
                dismiss();
                listener.selectArea(subCity.get(pos - 1));
            }
        });
    }

    @Override
    protected int getMaxHeight() {
        return (int) (ScreenUtils.getScreenHeight() * 0.7);
    }

    class AreaAdapter extends BaseRecyclerAdapter<CityModel, ViewHolder> {

        public AreaAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new ViewHolder(inflater.inflate(R.layout.item_bar_filter_area, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CityModel item) {
            holder.tvArea.setText(item.getAreaName());
            if (!TextUtils.isEmpty(area) && item.getAreaName().equals(area)) {
                holder.tvArea.setSelected(true);
            } else {
                holder.tvArea.setSelected(false);
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvArea)
        TextView tvArea;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
