package com.yanhua.home.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class InviteStaggeredItemDecoration extends RecyclerView.ItemDecoration {

    private  int left;
    private  int bottom;


    public InviteStaggeredItemDecoration(Context context, int l,int b) {
        left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, l, context.getResources().getDisplayMetrics());
        bottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, b, context.getResources().getDisplayMetrics());

    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
    }


    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
    }


    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {

        outRect.set(left, 0, 0, bottom);
    }
}


