package com.yanhua.home.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HomeZHFilterPopup extends PartShadowPopupView {

    private Context context;
    private int synthesizeSortType;

    public HomeZHFilterPopup(@NonNull Context context, int synthesizeSortType) {
        super(context);
        this.context = context;
        this.synthesizeSortType = synthesizeSortType;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_home_zh_filter;
    }

    private OnSelectTimeListener listener;

    public interface OnSelectTimeListener {
        void selectTime(int pos, String time);
    }

    public void setOnSelectTimeListener(OnSelectTimeListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        List<String> times = new ArrayList<>();
        times.add("综合排序");
        times.add("最新发布");
        times.add("点赞最多");
        RecyclerView rvTime = findViewById(R.id.rvTime);
        rvTime.setLayoutManager(new LinearLayoutManager(context));
        TimeAdapter timeAdapter = new TimeAdapter(context);
        rvTime.setAdapter(timeAdapter);
        timeAdapter.setItems(times);
        timeAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                dismiss();
                listener.selectTime(pos, times.get(pos));
            }
        });
    }

    class TimeAdapter extends BaseRecyclerAdapter<String, HomeTimeFilterPopup.ViewHolder> {

        public TimeAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected HomeTimeFilterPopup.ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new HomeTimeFilterPopup.ViewHolder(inflater.inflate(R.layout.item_bar_filter_area, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull HomeTimeFilterPopup.ViewHolder holder, @NonNull String item) {
            int position = getPosition(holder);
            holder.tvTime.setText(item);
            if (position == synthesizeSortType) {
                holder.tvTime.setSelected(true);
            } else {
                holder.tvTime.setSelected(false);
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvArea)
        TextView tvTime;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
