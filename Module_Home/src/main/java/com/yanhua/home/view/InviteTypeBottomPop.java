package com.yanhua.home.view;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.yanhua.base.mvp.BaseMvpBottomPopupView;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.home.R;
import com.yanhua.home.adapter.InviteTypeAdapter;

import java.util.List;

/**
 * 选择约伴类型吧
 *
 * @author Administrator
 */
public class InviteTypeBottomPop extends BaseMvpBottomPopupView {

    private Context mContext;
    TextView tvCancel;
    RecyclerView rvContent;

    private InviteTypeAdapter mAdapter;

    @Override
    protected void creatPresent() {
    }

    public InviteTypeBottomPop(@NonNull Context context,OnClickSelectItem listener) {
        super(context);
        mContext = context;

        mListener = listener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.bottom_popup_invite_type;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

        initView();

    }

    private void initView() {
        rvContent = findViewById(R.id.rv_content);
        tvCancel = findViewById(R.id.tvCancel);

        tvCancel.setOnClickListener(v -> dismiss());


        List<InviteTypeModel> mList = DiscoCacheUtils.getInstance().getInviteTypeList();
//        InviteTypeModel type1 = new InviteTypeModel("蹦迪");
//        InviteTypeModel type2 = new InviteTypeModel("品酒");
//        InviteTypeModel type3 = new InviteTypeModel("KTV");
//        InviteTypeModel type4 = new InviteTypeModel("酒吧");
//        InviteTypeModel type5 = new InviteTypeModel("夜店");
//        InviteTypeModel type6 = new InviteTypeModel("演唱会蹦迪");
//        InviteTypeModel type7 = new InviteTypeModel("赛博朋克");
//        InviteTypeModel type8 = new InviteTypeModel("迪斯科");
//        InviteTypeModel type9 = new InviteTypeModel("娱乐活动");
//        mList.add(type1);
//        mList.add(type2);
//        mList.add(type3);
//        mList.add(type4);
//        mList.add(type5);
//        mList.add(type6);
//        mList.add(type7);
//        mList.add(type8);
//        mList.add(type9);

        GridLayoutManager mSubContentManager = new GridLayoutManager(mContext, 3, GridLayoutManager.VERTICAL, false);
        rvContent.setLayoutManager(mSubContentManager);

        mAdapter = new InviteTypeAdapter(mContext);

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
//        managerContent.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvContent.addItemDecoration(new InviteStaggeredItemDecoration(mContext, 12, 32));
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);//去掉上拉下拉的阴影效果
        mAdapter.setItems(mList);

        mAdapter.setOnItemClickListener((itemView, pos) -> {

            InviteTypeModel inviteTypeModel = mList.get(pos);

            if (null!=mListener){
                mListener.onSelectItem(inviteTypeModel);
            }

            InviteTypeBottomPop.this.doAfterDismiss();
        });
    }

    private OnClickSelectItem mListener;

    public void setOnClickSelectItem(OnClickSelectItem listener) {
        mListener = listener;
    }

    public interface OnClickSelectItem {
        void onSelectItem(InviteTypeModel item);
    }


    @Override
    protected int getMaxHeight() {
        return DisplayUtils.getScreenHeight(mContext) * 9 / 10;
    }
}
