package com.yanhua.home.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.impl.PartShadowPopupView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.List;

import butterknife.BindView;

public class BarTypePopup extends PartShadowPopupView {

    private Context context;
    private String barType;
    private List<CommonReasonModel> barTypeList;
    private TextView tvAllType;
    private RecyclerView rvBarType;

    public BarTypePopup(@NonNull Context context, String barType, List<CommonReasonModel> barTypeList) {
        super(context);
        this.context = context;
        this.barType = barType;
        this.barTypeList = barTypeList;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_bar_type;
    }

    private OnSelectTypeListener listener;

    public interface OnSelectTypeListener {
        void selectType(CommonReasonModel model);
    }

    public void setOnSelectTypeListener(OnSelectTypeListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        tvAllType = findViewById(R.id.tvAllType);
        rvBarType = findViewById(R.id.rv_bar_type);
        rvBarType.setLayoutManager(new LinearLayoutManager(context));
        BarTypeAdapter barTypeAdapter = new BarTypeAdapter(context);
        rvBarType.setAdapter(barTypeAdapter);
        barTypeAdapter.setItems(barTypeList);
        if (TextUtils.isEmpty(barType)) {
            tvAllType.setSelected(true);
        }
        tvAllType.setOnClickListener(v -> {
            tvAllType.setSelected(true);
            dismiss();
            if (listener != null) {
                listener.selectType(null);
            }
        });
        barTypeAdapter.setOnItemClickListener((itemView, pos) -> {
            if (listener != null) {
                barType = barTypeList.get(pos).getContent();
                barTypeAdapter.notifyDataSetChanged();
                dismiss();
                listener.selectType(barTypeList.get(pos));
            }
        });
    }

    class BarTypeAdapter extends BaseRecyclerAdapter<CommonReasonModel, ViewHolder> {

        public BarTypeAdapter(Context context) {
            super(context);
        }

        @NonNull
        @Override
        protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
            return new ViewHolder(inflater.inflate(R.layout.item_bar_filter_area, parent, false));
        }

        @Override
        protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommonReasonModel item) {
            holder.tvType.setText(item.getContent());
            if (!TextUtils.isEmpty(barType) && item.getContent().equals(barType)) {
                holder.tvType.setSelected(true);
            } else {
                holder.tvType.setSelected(false);
            }
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvArea)
        TextView tvType;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
