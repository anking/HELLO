package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

//主页分类显示
public class StrategyClassifyMainAdapter extends BaseRecyclerAdapter<NewsTypeModel, StrategyClassifyMainAdapter.ViewHolder> {
    public StrategyClassifyMainAdapter(Context context) {
        super(context);
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_strategy_main_classify, parent, false));

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull NewsTypeModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImgCenterCrop(holder.ivIcon, item.getLogo(), R.drawable.place_holder);
        holder.tvName.setText(item.getReportName());

        holder.llEquipment.getLayoutParams();

        ViewGroup.LayoutParams layoutParams = holder.llEquipment.getLayoutParams();

        int sw = DisplayUtils.getScreenWidth(context);
        layoutParams.width = sw / 4;
        layoutParams.height = WRAP_CONTENT;
        holder.llEquipment.setLayoutParams(layoutParams);
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.llEquipment)
        LinearLayout llEquipment;
        @BindView(R2.id.ivIcon)
        ImageView ivIcon;
        @BindView(R2.id.tvName)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
