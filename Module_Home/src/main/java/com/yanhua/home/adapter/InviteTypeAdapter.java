package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ClickUtils;
import com.google.android.material.imageview.ShapeableImageView;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

public class InviteTypeAdapter extends BaseRecyclerAdapter<InviteTypeModel, InviteTypeAdapter.ViewHolder> {
    private Context mContext;

    public InviteTypeAdapter(Context context) {
        super(context);
        this.mContext = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_select_invite_type, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InviteTypeModel item) {
        int pos = getPosition(holder);

        if (pos == 0 || pos == 2) {
            holder.viewPlacaHolder.setVisibility(View.VISIBLE);
        } else {
            holder.viewPlacaHolder.setVisibility(View.GONE);
        }

        ImageLoaderUtil.loadImg(holder.sivImg, item.getImageUrl(), R.mipmap.ic_menu_invite);
        String name = item.getName();

        holder.tvTitle.setText(name);
        holder.flItem.setBackgroundResource(bgRes[pos % 7]);

        ClickUtils.applyPressedViewScale(holder.llItem);
    }

    //七彩背景
    private int bgRes[] = {R.drawable.shape_gradient_r100_hot, R.drawable.shape_gradient_r100_sub, R.drawable.shape_gradient_r100_theme,
            R.drawable.shape_gradient_r100_publish, R.drawable.shape_gradient_r100_location, R.drawable.shape_gradient_r100_pic, R.drawable.shape_gradient_r100_at};

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tv_title)
        TextView tvTitle;

        @BindView(R2.id.ll_item)
        LinearLayout llItem;

        @BindView(R2.id.viewPlacaHolder)
        View viewPlacaHolder;

        @BindView(R2.id.flItem)
        FrameLayout flItem;

        @BindView(R2.id.sivImg)
        ShapeableImageView sivImg;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
