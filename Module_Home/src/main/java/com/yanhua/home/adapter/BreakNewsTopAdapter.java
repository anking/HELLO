package com.yanhua.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;


public class BreakNewsTopAdapter extends BaseRecyclerAdapter<BreakNewsRecommendModel, BreakNewsTopAdapter.ViewHolder> {

    private OnSelectListener onSelectListener;

    public BreakNewsTopAdapter(Context context, OnSelectListener listener) {
        super(context);

        onSelectListener = listener;
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_break_news_top, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BreakNewsRecommendModel item) {
        int position = getPosition(holder);


        String advertisingImgUrl = "";
        if (!TextUtils.isEmpty(item.getVideoCoverUrl())){
            advertisingImgUrl = item.getVideoCoverUrl();
        }else {
            advertisingImgUrl = item.getShowUrl();
        }

        ImageLoaderUtil.loadImgCenterCrop(context, holder.ivCover, advertisingImgUrl, R.drawable.place_holder, 4);

        holder.flItem.setOnClickListener(view -> {
            onSelectListener.onSelect(position);
        });

        boolean isSelect = item.isSelect();
        if (isSelect) {
            holder.flItem.setPadding(4, 4, 4, 4);
            holder.flItem.setBackgroundResource(R.drawable.shape_bg_r4_white);
        } else {
            holder.flItem.setPadding(0, 0, 0, 0);
            holder.flItem.setBackgroundResource(R.drawable.shape_half_r_tran);
        }
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.flItem)
        FrameLayout flItem;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnSelectListener {
        void onSelect(int pos);
    }
}
