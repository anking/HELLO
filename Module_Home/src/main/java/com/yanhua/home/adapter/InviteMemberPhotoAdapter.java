package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

public class InviteMemberPhotoAdapter extends BaseRecyclerAdapter<InviteMemberModel, InviteMemberPhotoAdapter.ViewHolder> {
    public InviteMemberPhotoAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite_member_photo, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InviteMemberModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImg(holder.iVCircle, item.getUserImage());
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.iVCircle)
        CircleImageView iVCircle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
