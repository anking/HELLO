package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;


public class InviteMemberListAdapter extends BaseRecyclerAdapter<InviteMemberModel, InviteMemberListAdapter.ViewHolder> {

    private Context mContext;
    private OnHandleClickListener mListener;
    private String mType;//"1", "待加入"|"2", "已加入"

    public InviteMemberListAdapter(Context context, String type, OnHandleClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
        mType = type;
    }

    public interface OnHandleClickListener {
        int DETAIL = 1;//个人主页
        int CHAT = 2;//聊一聊
        int ADD_MEMBER = 3;//同意加入
        int REMOVE_MEMBER = 4;//移除成员

        void onHandleClick(int type, InviteMemberModel item, int pos);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite_member, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull @NotNull ViewHolder holder, @NonNull @NotNull InviteMemberModel item) {
        int position = getPosition(holder);

        switch (mType) {//"1", "待加入"|"2", "已加入"
            case "1":
                String enrollTime = YXTimeUtils.getFriendlyTimeAtContent(item.getEnrollTime());
                holder.tvApplyTime.setText(enrollTime);
                holder.tvRemoveMember.setVisibility(View.GONE);
                holder.tvAddMember.setVisibility(View.VISIBLE);
                break;
            case "2":
                String inviteTime = YXTimeUtils.getFriendlyTimeAtContent(item.getInviteTime());
                holder.tvApplyTime.setText(inviteTime);
                holder.tvRemoveMember.setVisibility(View.VISIBLE);
                holder.tvAddMember.setVisibility(View.GONE);
                break;
        }
//        holder.tvChat.setVisibility(View.GONE);
        holder.llItem.setOnClickListener(v -> {
            mListener.onHandleClick(OnHandleClickListener.DETAIL, item, position);
        });

        holder.tvChat.setOnClickListener(v -> {
            mListener.onHandleClick(OnHandleClickListener.CHAT, item, position);
        });
        holder.tvAddMember.setOnClickListener(v -> {
            mListener.onHandleClick(OnHandleClickListener.ADD_MEMBER, item, position);
        });
        holder.tvRemoveMember.setOnClickListener(v -> {
            mListener.onHandleClick(OnHandleClickListener.REMOVE_MEMBER, item, position);
        });

        //发起人信息
        int userGender = item.getUserGender();//邀约用户性别(-1:保密, 1:男, 2:女)
        String userImage = item.getUserImage();//邀约用户头像
        String userName = item.getUserName();//邀约用户名称
        ImageLoaderUtil.loadImgHeadCenterCrop(holder.ivUserHead, userImage, R.drawable.place_holder);
        switch (userGender) {
            case -1:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
            case 1:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_male);
                break;
            case 2:
                holder.ivInfoSex.setImageResource(R.mipmap.ic_female);
                break;
            default:
                holder.ivInfoSex.setImageResource(R.drawable.bg_tran_circle_no_stroke);
                break;
        }
        holder.tvUserName.setText(userName);
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.tvRemoveMember)
        TextView tvRemoveMember;

        @BindView(R2.id.tvChat)
        TextView tvChat;

        @BindView(R2.id.tvAddMember)
        TextView tvAddMember;

        @BindView(R2.id.tvApplyTime)
        TextView tvApplyTime;

        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.llItem)
        LinearLayout llItem;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
