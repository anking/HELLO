package com.yanhua.home.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import butterknife.BindView;

public class BarQACommentAdapter extends BaseRecyclerAdapter<CommentModel, BarQACommentAdapter.ViewHolder> {

    private OnItemCellClickListener listener;

    public interface OnItemCellClickListener {
        /**
         * @param pos
         * @param cellType 0为点击用户相关 1为点赞相关 2踩赞 3展示更多子评论 4举报或删除
         */
        void onItemCellClick(int pos, int cellType);
    }

    public void setOnItemCellClickListener(OnItemCellClickListener listener) {
        this.listener = listener;
    }

    public BarQACommentAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_qa_comment, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommentModel item) {
        int position = getPosition(holder);
        ImageLoaderUtil.loadImg(holder.civAvatar, item.getUserPhoto());
        holder.tvName.setText(item.getNickName());
        holder.tvReplyTime.setText(YXTimeUtils.getFriendlyTimeAtContent(item.getCreatedTime(), true));
        String comment = item.getCommentDesc();
        if (!TextUtils.isEmpty(comment)) {
            try {
                comment = URLDecoder.decode(comment.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        String replyName = item.getReplyName();
        String commentUserId = item.getCommentUserId();
        int replyStatus = item.getReplyStatus();
        if (replyStatus == 1 && !TextUtils.isEmpty(replyName) && !TextUtils.isEmpty(commentUserId)) {
            SpannableString applyComment = makeReplyCommentSpan(replyName, commentUserId, comment, position);
            holder.tvComment.setText(applyComment);
        } else {
            holder.tvComment.setText(comment);
        }
        int type = item.getType();
        if (type == 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(context, 32), DisplayUtils.dip2px(context, 32));
            holder.civAvatar.setLayoutParams(params);
            holder.llComment.setPadding(0, 0, 0, 0);
            holder.tvExpand.setVisibility(View.GONE);
        } else if (type == 1) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(context, 20), DisplayUtils.dip2px(context, 20));
            holder.civAvatar.setLayoutParams(params);
            holder.llComment.setPadding(DisplayUtils.dip2px(context, 40), 0, 0, 0);
            holder.tvExpand.setVisibility(View.GONE);
        } else if (type == 2) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(context, 20), DisplayUtils.dip2px(context, 20));
            holder.civAvatar.setLayoutParams(params);
            holder.llComment.setPadding(DisplayUtils.dip2px(context, 40), 0, 0, 0);
            holder.tvExpand.setVisibility(View.VISIBLE);
            int childCount = item.getParentChildCount();
            holder.tvExpand.setText("展开" + (childCount - 2) + "条评论");
            holder.tvExpand.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemCellClick(position, 3);
                }
            });
        }
        if (item.getFabulousCount() == 0) {
            holder.tvLoveNum.setText("赞");
        } else {
            holder.tvLoveNum.setText(String.format("赞(%d)", item.getFabulousCount()));
        }
        if (item.getWorseCount() == 0) {
            holder.tvWorseNum.setText("踩");
        } else {
            holder.tvWorseNum.setText(String.format("踩(%d)", item.getWorseCount()));
        }
        if (item.isWorse()) {
            holder.ivWorse.setText("\ue779");
            holder.ivWorse.setTextColor(ContextCompat.getColor(context, R.color.theme));
            holder.tvWorseNum.setTextColor(ContextCompat.getColor(context, R.color.theme));
        } else {
            holder.ivWorse.setText("\ue77a");
            holder.ivWorse.setTextColor(ContextCompat.getColor(context, R.color.subWord));
            holder.tvWorseNum.setTextColor(ContextCompat.getColor(context, R.color.assistWord));
        }


        if (item.isFabulous()) {
            holder.ivLove.setText("\ue772");
            holder.ivLove.setTextColor(ContextCompat.getColor(context, R.color.theme));
            holder.tvLoveNum.setTextColor(ContextCompat.getColor(context, R.color.theme));
        } else {
            holder.ivLove.setText("\ue71b");
            holder.ivLove.setTextColor(ContextCompat.getColor(context, R.color.subWord));
            holder.tvLoveNum.setTextColor(ContextCompat.getColor(context, R.color.assistWord));
        }
        holder.civAvatar.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 0);
            }
        });
        holder.tvName.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemCellClick(position, 0);
            }
        });
        holder.llLove.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemCellClick(position, 1);
            }
        });
        holder.llWorse.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemCellClick(position, 2);
            }
        });
        holder.ivMore.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemCellClick(position, 4);
            }
        });
    }

    public SpannableString makeReplyCommentSpan(final String atSomeone, final String id, String commentContent, int position) {
        String richText = String.format("回复 %s : %s", atSomeone, commentContent);

        SpannableString builder = new SpannableString(richText);

        if (!TextUtils.isEmpty(atSomeone)) {
            builder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(@NonNull View view) {
                    if (listener != null) {
                        listener.onItemCellClick(position, 0);
                    }
                }

                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(ContextCompat.getColor(context, com.yanhua.common.R.color.color_5B83E1));
                    ds.setTextSize(DisplayUtils.dip2px(context, 13));
                    ds.setFlags(Paint.FAKE_BOLD_TEXT_FLAG);
                }
            }, 2, atSomeone.length() + 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);//设置回复的人的名字
        }
        return builder;
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tv_name)
        TextView tvName;
        @BindView(R2.id.tv_comment)
        TextView tvComment;
        @BindView(R2.id.tvReplyTime)
        TextView tvReplyTime;
        @BindView(R2.id.tvWorseNum)
        TextView tvWorseNum;
        @BindView(R2.id.tvLoveNum)
        TextView tvLoveNum;
        @BindView(R2.id.iv_worse)
        AliIconFontTextView ivWorse;
        @BindView(R2.id.iv_love)
        AliIconFontTextView ivLove;
        @BindView(R2.id.llLove)
        LinearLayout llLove;
        @BindView(R2.id.iv_more)
        AliIconFontTextView ivMore;
        @BindView(R2.id.llWorse)
        LinearLayout llWorse;
        @BindView(R2.id.tv_expand)
        TextView tvExpand;
        @BindView(R2.id.ll_comment)
        LinearLayout llComment;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
