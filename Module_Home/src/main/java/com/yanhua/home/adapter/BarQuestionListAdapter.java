package com.yanhua.home.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

public class BarQuestionListAdapter extends BaseRecyclerAdapter<BarQAModel, BarQuestionListAdapter.ViewHolder> {

    public BarQuestionListAdapter(Context context) {
        super(context);
    }

    private OnJumpPageListener listener;

    public interface OnJumpPageListener {
        void jumpPage(int page, int pos);
    }

    public void setOnJumpPageListener(OnJumpPageListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_question, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BarQAModel item) {
        int position = getPosition(holder);
        holder.tvQuestionName.setText(item.getParentAskedDesc());
        holder.tvBrowseNum.setText(String.format("%d浏览", item.getBrowseNum()));
        if (item.getAskedNum() > 0) {
            holder.tvAnswerNum.setText(String.format("全部%d个回答", item.getAskedNum()));
            holder.llAnswer.setVisibility(View.VISIBLE);
            String content = "<font color=\"#B4B8BC\" >" + (item.getIsShow() == 1 ? "匿名用户" : item.getUserName()) + "：</font>"
                    + "<font color=\"#36404A\" >" + item.getAskedDesc() + "</font>";
            holder.tvAnswerTitle.setText(Html.fromHtml(content));
            holder.llAnswerNum.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(500) && listener != null) {
                    listener.jumpPage(1, position);
                }
            });
        } else {
            holder.tvAnswerNum.setText("去回答");
            holder.llAnswer.setVisibility(View.GONE);
            holder.llAnswerNum.setOnClickListener(v -> {
                if (FastClickUtil.isFastClick(500) && listener != null) {
                    listener.jumpPage(2, position);
                }
            });
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvQuestionName)
        TextView tvQuestionName;
        @BindView(R2.id.tvBrowseNum)
        TextView tvBrowseNum;
        @BindView(R2.id.tvAnswerNum)
        TextView tvAnswerNum;
        @BindView(R2.id.llAnswer)
        LinearLayout llAnswer;
        @BindView(R2.id.llAnswerNum)
        LinearLayout llAnswerNum;
        @BindView(R2.id.tvAnswerTitle)
        TextView tvAnswerTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
