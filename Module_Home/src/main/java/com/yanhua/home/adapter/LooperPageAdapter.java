package com.yanhua.home.adapter;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.home.fragment.ActCalendarFragment;

import java.util.List;

public class LooperPageAdapter extends FragmentStatePagerAdapter {

    private boolean showHistory;
    private int NUMBER_OF_LOOPS = 100;
    private List<SelectConditionModel> mMenuList;

    public SelectConditionModel getValueAt(int position) {
        if (mMenuList.size() == 0) {
            return null;
        }
        return mMenuList.get(position % mMenuList.size());
    }


    public int getCenterPosition(int position, int pos, int listSize) {
        if (listSize > YXConfig.ACT_TOP_MIN_NUM) {
            return mMenuList.size() * NUMBER_OF_LOOPS / 2 + position;
        } else {
            return pos;
        }
    }

    public LooperPageAdapter(@NonNull FragmentManager fm, List<SelectConditionModel> list, boolean showHistory) {
        super(fm);
        this.mMenuList = list;
        this.showHistory = showHistory;

        if (this.mMenuList.size() > YXConfig.ACT_TOP_MIN_NUM) {
            NUMBER_OF_LOOPS = 100;
        } else {
            NUMBER_OF_LOOPS = 1;
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        SelectConditionModel model = getValueAt(position);

        Fragment fragment = new ActCalendarFragment();
        Bundle bundle = new Bundle();
        String city = model.getFieldValue();
        bundle.putInt("type", position);
        bundle.putString("city", city);
        bundle.putBoolean("showHistory", showHistory);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mMenuList.size() * NUMBER_OF_LOOPS;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        SelectConditionModel model = getValueAt(position);
        String fieldValue = model.getFieldValue();

        String name = fieldValue;

        return name;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {//避免销毁后有重新，导致闪频效果
    }
}