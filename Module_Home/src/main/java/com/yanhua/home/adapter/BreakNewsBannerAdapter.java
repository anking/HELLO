package com.yanhua.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.home.R;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;

/**
 * @author Administrator
 */
public class BreakNewsBannerAdapter extends BannerAdapter<BreakNewsRecommendModel, BreakNewsBannerAdapter.BannerHolder> {

    private List<BreakNewsRecommendModel> dataList;
    private Context mContext;

    public BreakNewsBannerAdapter(List<BreakNewsRecommendModel> datas, Context context) {
        super(datas);
        dataList = datas;
        mContext = context;
    }


    @Override
    public BannerHolder onCreateHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner_break_news, parent, false);
        return new BannerHolder(itemView);
    }

    @Override
    public void onBindView(BannerHolder holder, BreakNewsRecommendModel item, int position, int size) {
        holder.iv_banner.setVisibility(View.VISIBLE);

        String advertisingImgUrl = "";

        holder.tvName.setText(YHStringUtils.value(item.getContentTitle()));
        if (YXConfig.ALL666.equals(item.getContentCategoryId())) {
            String startTime = item.getActivityStartTime();
            String endTime = item.getActivityEndTime();

            holder.tvTime.setText(DiscoTimeUtils.getBreakNewsTopTime(startTime, endTime));
            holder.tvTime.setVisibility(View.VISIBLE);
        } else {
            String publishTime = item.getPublishTime();
            if (TextUtils.isEmpty(publishTime)) {
                holder.tvTime.setVisibility(View.GONE);
            } else {
                holder.tvTime.setVisibility(View.VISIBLE);
                holder.tvTime.setText("发布于" + YXTimeUtils.getFriendlyTimeAtContent(publishTime));
            }
        }

        if (!TextUtils.isEmpty(item.getVideoCoverUrl())) {
            advertisingImgUrl = item.getVideoCoverUrl();
        } else {
            advertisingImgUrl = item.getShowUrl();
        }

        if (!TextUtils.isEmpty(advertisingImgUrl)) {
            ImageLoaderUtil.loadImgCenterCrop(mContext, holder.iv_banner, advertisingImgUrl, R.drawable.place_holder, 0);
        } else {
            holder.iv_banner.setImageResource(R.drawable.place_holder);
        }

        holder.fl_item.setOnClickListener(view -> {

        });
    }

    static class BannerHolder extends RecyclerView.ViewHolder {
        private ImageView iv_banner;
        private LinearLayout fl_item;
        private TextView tvName;
        private TextView tvTime;


        public BannerHolder(@NonNull View itemView) {
            super(itemView);
            iv_banner = itemView.findViewById(R.id.iv_banner);
            fl_item = itemView.findViewById(R.id.fl_item);
            tvName = itemView.findViewById(R.id.tvName);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }
}
