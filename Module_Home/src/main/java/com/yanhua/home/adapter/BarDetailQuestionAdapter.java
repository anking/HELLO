package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

/**
 * 酒吧详情界面的问题列表适配器
 */
public class BarDetailQuestionAdapter extends BaseRecyclerAdapter<BarQAModel, BarDetailQuestionAdapter.ViewHolder> {

    public BarDetailQuestionAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_detail_question, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BarQAModel item) {
        holder.tvQuestionName.setText(item.getParentAskedDesc());
        holder.tvAnswerNum.setText(String.format("%d个回答", item.getAskedNum()));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvQuestionName)
        TextView tvQuestionName;
        @BindView(R2.id.tvAnswerNum)
        TextView tvAnswerNum;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
