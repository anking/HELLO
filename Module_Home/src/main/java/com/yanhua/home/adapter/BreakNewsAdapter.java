package com.yanhua.home.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BreakNewsAdapter extends BaseRecyclerAdapter<BreakNewsListModel, BreakNewsAdapter.ViewHolder> {

    private Context mContext;

    public BreakNewsAdapter(Context context) {
        super(context);
        mContext = context;
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_break_news, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BreakNewsListModel item) {
        int position = getPosition(holder);
        String coverImage = item.getCoverUrl();

        if (TextUtils.isEmpty(coverImage)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.place_holder, options);

            holder.ivCover.setSrcBmp(bitmap);
        } else {
            Glide.with(mContext)
                    .asBitmap()
                    .load(coverImage + "?x-oss-process=image/resize,m_fixed,h_600,w_450")
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            holder.ivCover.setSrcBmp(resource);
                        }
                    });
        }
        //城市
        String city = item.getIssueCity();
        if (TextUtils.isEmpty(city)) {
            holder.tvCity.setVisibility(View.GONE);
        } else {
            holder.tvCity.setVisibility(View.VISIBLE);
            int indexOf = city.lastIndexOf("市");
            if (indexOf > 0) {
                city = city.replace(city.charAt(city.length() - 1) + "", "");
            }

            holder.tvCity.setText(city);
        }

        String content = item.getContentTitle();
        if (!TextUtils.isEmpty(content)) {
            holder.tvName.setVisibility(View.VISIBLE);
            holder.tvName.setText(content);
        } else {
            holder.tvName.setVisibility(View.GONE);
        }

        //话题
        List<TopicModel> topicList = new ArrayList<>();//存在的话题

        List<TopicModel> topicListItem = item.getTopicList();//话题
        if (null != topicListItem && topicListItem.size() > 0) {
            for (TopicModel topic : topicListItem) {
                if (topic.getDeleted() != 1) {
                    topicList.add(topic);
                }
            }
        }

        if (null != topicList && topicList.size() > 0) {
            holder.tvTopic.setVisibility(View.VISIBLE);
            holder.tvTopic.setText(YHStringUtils.formatTopic(topicList.get(0).getTitle()));
            holder.tvName.setMaxLines(1);
        } else {
            holder.tvName.setMaxLines(2);
            holder.tvTopic.setVisibility(View.GONE);
        }

        String userPhoto = item.getImg();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }

        String nickName = YHStringUtils.value(item.getNickName());
        holder.tvUserName.setText(nickName);

        String contentCategoryId = item.getContentCategoryId();
        String startTime = item.getActivityStartTime();
        String endTime = item.getActivityEndTime();

        if (!YXConfig.ALL666.equals(contentCategoryId)) {
            //爆料内容
            holder.tvAddress.setVisibility(View.GONE);
            holder.tvTime.setVisibility(View.GONE);
            holder.llBreakNewsContent.setVisibility(View.VISIBLE);

            holder.tvTimeContent.setText(YXTimeUtils.getFriendlyTimeAtContent(item.getPublishTime()));
        } else {
            //活动
            holder.tvAddress.setVisibility(View.VISIBLE);
            holder.tvTime.setVisibility(View.VISIBLE);
            holder.llBreakNewsContent.setVisibility(View.GONE);
            holder.tvAddress.setText(YHStringUtils.value(item.getIssueAddress()));
            holder.tvTime.setText(DiscoTimeUtils.getBreakNewsTopTime(startTime, endTime));
        }

        //为什么还要判断时间？？
        if ((!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) || "6".equals(item.getAttributeType())) {
            holder.tvHot.setVisibility(View.VISIBLE);
        } else {
            holder.tvHot.setVisibility(View.GONE);
        }

    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivCover)
        AnyShapeImageView ivCover;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvTopic)
        TextView tvTopic;
        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvTimeContent)
        TextView tvTimeContent;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;

        @BindView(R2.id.tvHot)
        TextView tvHot;

        @BindView(R2.id.tvCity)
        TextView tvCity;


        @BindView(R2.id.llBreakNewsContent)
        LinearLayout llBreakNewsContent;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
