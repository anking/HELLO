package com.yanhua.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class ActNewsAdapter extends BaseRecyclerAdapter<ActivityModel, ActNewsAdapter.ViewHolder> {

    private Context mContext;

    public ActNewsAdapter(Context context) {
        super(context);
        mContext = context;
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_act_news, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull ActivityModel item) {
        int position = getPosition(holder);

        String advertisingImgUrl = "";
        if (!TextUtils.isEmpty(item.getVedioCoverUrl())) {
            advertisingImgUrl = item.getVedioCoverUrl();
        } else {
            advertisingImgUrl = item.getCoverUrl();
        }
        ImageLoaderUtil.loadImgCenterCrop(holder.ivCover, advertisingImgUrl, R.drawable.place_holder);

        String content = item.getContentTitle();
        if (!TextUtils.isEmpty(content)) {
            holder.tvName.setVisibility(View.VISIBLE);
            holder.tvName.setText(content);
        } else {
            holder.tvName.setVisibility(View.GONE);
        }

        //话题
        List<TopicModel> topicList = new ArrayList<>();//存在的话题

        List<TopicModel> topicListItem = item.getTopicList();//话题
        if (null != topicListItem && topicListItem.size() > 0) {
            for (TopicModel topic : topicListItem) {
                if (topic.getDeleted() != 1) {
                    topicList.add(topic);
                }
            }
        }

        if (null != topicList && topicList.size() > 0) {
            holder.tvTopic.setVisibility(View.VISIBLE);
            holder.tvTopic.setText(YHStringUtils.formatTopic(topicList.get(0).getTitle()));
            holder.tvName.setMaxLines(1);
        } else {
            holder.tvName.setMaxLines(2);
            holder.tvTopic.setVisibility(View.GONE);
        }

        if ("6".equals(item.getAttributeType())) {
            holder.tvHotTag.setVisibility(View.VISIBLE);
        } else {
            holder.tvHotTag.setVisibility(View.GONE);
        }

        String startTime = item.getActivityStartTime();
        String endTime = item.getActivityEndTime();

        //活动
        holder.tvAddress.setText(YHStringUtils.value(item.getIssueAddress()));
        holder.tvTime.setText(DiscoTimeUtils.getBreakNewsTopTime(startTime, endTime));
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.tvTopic)
        TextView tvTopic;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvAddress)
        TextView tvAddress;

        @BindView(R2.id.tvHotTag)
        TextView tvHotTag;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
