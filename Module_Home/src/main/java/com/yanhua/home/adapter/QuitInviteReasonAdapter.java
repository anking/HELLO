package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;


public class QuitInviteReasonAdapter extends BaseRecyclerAdapter<CommonReasonModel, QuitInviteReasonAdapter.ViewHolder> {
    public QuitInviteReasonAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite_quit_reason, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull CommonReasonModel item) {
        int position = getPosition(holder);

        if (null != mSelectId && mSelectId.equals(item.getId())) {
            holder.ivSelect.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelect.setVisibility(View.GONE);
        }

        holder.tvName.setText(item.getContent());
    }

    private String mSelectId;

    public void setSelectId(String id) {
        mSelectId = id;

        notifyDataSetChanged();
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.iv_select)
        AliIconFontTextView ivSelect;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
