package com.yanhua.home.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.launcher.ARouter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AnyShapeImageView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.Arrays;

import butterknife.BindView;

public class BarListAdapter extends BaseRecyclerAdapter<BarModel, BarListAdapter.ViewHolder> {

    private Context mContext;

    public BarListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    private OnLoveBarListener listener;

    public interface OnLoveBarListener {
        void doLove(int pos);
    }

    public void setOnLoveBarListener(OnLoveBarListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BarModel item) {
        int pos = getPosition(holder);
        holder.tvBarName.setText(item.getName());
        Glide.with(mContext)
                .asBitmap()
                .load(item.getBarPrimaryImg())
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        holder.ivBarImg.setSrcBmp(resource);
                    }
                });
        // 夜店、清吧、ktv、餐吧
        switch (item.getBarType()) {
            case "夜店":
                holder.ivBarType.setImageResource(R.mipmap.ic_night_club);
                break;
            case "清吧":
                holder.ivBarType.setImageResource(R.mipmap.ic_sober_bar);
                break;
            case "餐吧":
                holder.ivBarType.setImageResource(R.mipmap.ic_dinnng_bar);
                break;
            case "KTV":
                holder.ivBarType.setImageResource(R.mipmap.ic_ktv);
                break;
            case "Live House":
                holder.ivBarType.setImageResource(R.mipmap.ic_live_house);
                break;
        }
        String barTrait = item.getBarTrait();
        String[] barTraitArr = barTrait.split(",");
        holder.tflBarTrait.setAdapter(new TagAdapter<String>(Arrays.asList(barTraitArr)) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                //加载tag布局
                View view = LayoutInflater.from(mContext).inflate(R.layout.item_bar_trait_tag, parent, false);
                //获取标签
                TextView tvTag = view.findViewById(R.id.tvTag);
                tvTag.setText(s);
                return view;
            }
        });
        holder.tflBarTrait.setOnTagClickListener((view, position, parent) -> {
            ARouter.getInstance().build(ARouterPath.BAR_DETAIL_ACTIVITY)
                    .withString("barId", item.getBarId())
                    .navigation();
            return true;
        });
        holder.tvBarAddress.setText(item.getAddress());
        holder.tvBarDistance.setText(YHStringUtils.formatDistance(item.getDistance()));
        if (item.getPopularityNum() > 0) {
            holder.tvLoveNum.setText(YHStringUtils.quantityFormat(item.getPopularityNum()));
        } else {
            holder.tvLoveNum.setText("喜欢");
        }
        holder.tvLoveNum.setTextColor(ContextCompat.getColor(mContext, R.color.subWord));
        holder.ivBarLove.setSelected(item.getPopularityStatus() == 1);
        holder.llLove.setOnClickListener(v -> {
            if (FastClickUtil.isFastClick(500) && listener != null) {
                listener.doLove(pos);
            }
        });
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivBarImg)
        AnyShapeImageView ivBarImg;
        @BindView(R2.id.tvBarName)
        TextView tvBarName;
        @BindView(R2.id.ivBarType)
        ImageView ivBarType;
        @BindView(R2.id.tflBarTrait)
        TagFlowLayout tflBarTrait;
        @BindView(R2.id.tvBarAddress)
        TextView tvBarAddress;
        @BindView(R2.id.tvBarDistance)
        TextView tvBarDistance;
        @BindView(R2.id.tvLoveNum)
        TextView tvLoveNum;
        @BindView(R2.id.ivBarLove)
        ImageView ivBarLove;
        @BindView(R2.id.llLove)
        LinearLayout llLove;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
