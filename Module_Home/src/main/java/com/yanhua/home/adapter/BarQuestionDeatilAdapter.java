package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BarAnswerModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

public class BarQuestionDeatilAdapter extends BaseRecyclerAdapter<BarAnswerModel, BarQuestionDeatilAdapter.ViewHolder> {

    private Context context;

    public BarQuestionDeatilAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_bar_question_answer, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BarAnswerModel item) {
        if (item.getIsShow() == 1) {
            ImageLoaderUtil.loadImg(holder.civAvatar, R.drawable.rc_default_portrait);
            holder.tvUserName.setText("匿名用户");
        } else {
            ImageLoaderUtil.loadImg(holder.civAvatar, item.getImg());
            holder.tvUserName.setText(item.getAccountName());
            holder.civAvatar.setOnClickListener(v -> ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", item.getUserId())
                    .navigation());
        }
        holder.tvAnswer.setText(item.getAskedDesc());
        holder.tvTime.setText(YXTimeUtils.getFriendlyTimeAtContent(item.getCreatedTime()));
        holder.tvCommentNum.setText(item.getCommentCount() == 0 ? "评论" : String.format("评论 %d", item.getCommentCount()));
        if (item.getAskedFabulousCount() == 0) {
            holder.tvLoveNum.setText("赞");
        } else {
            holder.tvLoveNum.setText(String.format("赞 (%d)", item.getAskedFabulousCount()));
        }
        if (item.getAskedWorseCount() == 0) {
            holder.tvWorseNum.setText("踩");
        } else {
            holder.tvWorseNum.setText(String.format("踩 (%d)", item.getAskedWorseCount()));
        }
        if (item.getWorseStatus() == 1) {
            holder.ivWorse.setText("\ue779");
            holder.ivWorse.setTextColor(ContextCompat.getColor(context, R.color.theme));
            holder.tvWorseNum.setTextColor(ContextCompat.getColor(context, R.color.theme));
        } else {
            holder.ivWorse.setText("\ue77a");
            holder.ivWorse.setTextColor(ContextCompat.getColor(context, R.color.subWord));
            holder.tvWorseNum.setTextColor(ContextCompat.getColor(context, R.color.subWord));
        }
        if (item.getFabulousStatus() == 1) {
            holder.ivLove.setText("\ue772");
            holder.ivLove.setTextColor(ContextCompat.getColor(context, R.color.theme));
            holder.tvLoveNum.setTextColor(ContextCompat.getColor(context, R.color.theme));
        } else {
            holder.ivLove.setText("\ue71b");
            holder.ivLove.setTextColor(ContextCompat.getColor(context, R.color.subWord));
            holder.tvLoveNum.setTextColor(ContextCompat.getColor(context, R.color.subWord));
        }
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.civ_avatar)
        CircleImageView civAvatar;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvAnswer)
        TextView tvAnswer;
        @BindView(R2.id.tvTime)
        TextView tvTime;
        @BindView(R2.id.tvCommentNum)
        TextView tvCommentNum;
        @BindView(R2.id.tvWorseNum)
        TextView tvWorseNum;
        @BindView(R2.id.tvLoveNum)
        TextView tvLoveNum;
        @BindView(R2.id.iv_worse)
        AliIconFontTextView ivWorse;
        @BindView(R2.id.iv_love)
        AliIconFontTextView ivLove;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
