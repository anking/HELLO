package com.yanhua.home.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.gridlayout.widget.GridLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.GsonUtils;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.gson.reflect.TypeToken;
import com.shuyu.textutillib.RichTextView;
import com.shuyu.textutillib.listener.CustomTextViewTouchListener;
import com.shuyu.textutillib.listener.SpanAtUserCallBack;
import com.shuyu.textutillib.listener.SpanTopicCallBack;
import com.shuyu.textutillib.model.FriendModel;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.base.config.YXConfig;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.TagFlowModel;
import com.yanhua.common.model.UserModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoValueFormat;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.ShowDiscoListImage;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.SampleCoverVideo;
import com.yanhua.common.widget.StatusView;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.util.YXTimeUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CircleImageView;
import com.yanhua.core.view.OswaldTextView;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HomeSearchResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE_USER = 1;
    private static final int ITEM_VIEW_TYPE_CIRCLE = 2;
    private static final int ITEM_VIEW_TYPE_TOPIC = 3;
    private static final int ITEM_VIEW_TYPE_CONTENT = 4;

    private Context context;
    private LayoutInflater mInflater;
    private List<HomeSearchResultModel> resultList;

    public HomeSearchResultAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public interface OnHandleClickListener {
        int FOLLOW = 1;//关注
        int COLLECT = 2;//收藏
        int COMMENT = 3;//评论
        int THUMB_UP = 4;//点赞
        int TO_DETAIL = 0;//详情

        void onHandleClick(int type, MomentListModel item, int pos);

        void onHandleClick(int type, UserModel item, int pos);
    }

    private OnHandleClickListener mListener;

    public void setOnHandleClickListener(OnHandleClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        HomeSearchResultModel homeSearchResultModel = resultList.get(position);
        //类型(1:用户模块,2:圈子模块,3:话题模块,4:内容模块)
        int type = homeSearchResultModel.getType();
        return type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM_VIEW_TYPE_USER:
                viewHolder = new UserViewHolder(mInflater.inflate(R.layout.item_user, parent, false));
                break;
            case ITEM_VIEW_TYPE_CIRCLE:
                viewHolder = new CircleViewHolder(mInflater.inflate(R.layout.item_circle, parent, false));
                break;
            case ITEM_VIEW_TYPE_TOPIC:
                viewHolder = new TopicViewHolder(mInflater.inflate(R.layout.item_topic, parent, false));
                break;
            case ITEM_VIEW_TYPE_CONTENT:
                viewHolder = new ContentViewHolder(mInflater.inflate(R.layout.item_content, parent, false));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HomeSearchResultModel homeSearchResultModel = resultList.get(position);
        int type = homeSearchResultModel.getType();
        switch (type) {
            case ITEM_VIEW_TYPE_USER:
                UserViewHolder userVH = (UserViewHolder) holder;
                setUser(userVH, homeSearchResultModel, position);
                break;
            case ITEM_VIEW_TYPE_CIRCLE:
                CircleViewHolder circleVH = (CircleViewHolder) holder;
                setCircle(circleVH, homeSearchResultModel);
                break;
            case ITEM_VIEW_TYPE_TOPIC:
                TopicViewHolder topicVH = (TopicViewHolder) holder;
                setTopic(topicVH, homeSearchResultModel);
                break;
            case ITEM_VIEW_TYPE_CONTENT:
                ContentViewHolder contentVH = (ContentViewHolder) holder;
                setContent(contentVH, homeSearchResultModel, position);
                break;
        }
    }

    private void setUser(UserViewHolder holder, HomeSearchResultModel model, int position) {
        UserModel item = model.getUser();
        String userPhoto = item.getImg();
        String nickName = item.getNickName();
        String personalSignature = item.getPersonalSignature();

        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.iv_header, userPhoto, R.drawable.place_holder);
        } else {
            holder.iv_header.setImageResource(R.drawable.place_holder);
        }

        if (TextUtils.isEmpty(personalSignature)) {
            holder.tvSign.setVisibility(View.GONE);
        } else {
            holder.tvSign.setVisibility(View.VISIBLE);
            holder.tvSign.setText(String.format("签名:%s", personalSignature));
        }

        holder.tv_name.setText(!TextUtils.isEmpty(nickName) ? nickName : "");

        holder.iv_header.setOnClickListener(view -> {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", item.getId())
                    .navigation();
        });
        holder.ll_item.setOnClickListener(view -> {
            ARouter.getInstance()
                    .build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                    .withString("userId", item.getId())
                    .navigation();
        });

        if (item.getGender() == 1) {
            holder.ivInfoSex.setVisibility(View.VISIBLE);
            holder.ivInfoSex.setImageResource(R.mipmap.ic_male);
        } else if (item.getGender() == 2) {
            holder.ivInfoSex.setVisibility(View.VISIBLE);
            holder.ivInfoSex.setImageResource(R.mipmap.ic_female);
        } else {
            holder.ivInfoSex.setVisibility(View.GONE);
        }

//       关注状态 1 未关注 2 已关注 3 被关注 4互相关注
        int followStatus = item.getRelationState();

        holder.btn_follow.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.FOLLOW, item, position);
            }
        });

        String userID = UserManager.getInstance().getUserId();
        if (!TextUtils.isEmpty(userID) && userID.equals(item.getId())) {
            holder.btn_follow.setVisibility(View.GONE);
        } else {
            holder.btn_follow.setVisibility(View.VISIBLE);
        }
        switch (followStatus) {
            case 1:
                holder.btn_follow.setStatus(false, "关注");
                break;
            case 2:
                holder.btn_follow.setStatus(true, "已关注");
                break;
            case 3:
                holder.btn_follow.setStatus(false, "回粉");
                break;
            case 4:
                holder.btn_follow.setStatus(true, "互相关注");
                break;
        }
    }

    private void setCircle(CircleViewHolder holder, HomeSearchResultModel model) {
        CircleModel item = model.getCircle();
        holder.tvCircieName.setText(YHStringUtils.value(item.getTitle()));
        holder.tvCircleNum.setText("帖子：" + YHStringUtils.quantityFormat(item.getDynamicCount()));
        holder.tvDesc.setText(YHStringUtils.value(item.getCircleDesc()));
        ImageLoaderUtil.loadImg(holder.rIVCircleCover, item.getCoverUrl());
        holder.ll2Detail.setVisibility(View.VISIBLE);
        holder.ll2Detail.setOnClickListener(v -> ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                .withString("id", item.getId()).navigation());
    }

    private void setTopic(TopicViewHolder holder, HomeSearchResultModel model) {
        TopicModel item = model.getTopic();
        holder.tvCircieName.setText(YHStringUtils.value(item.getTitle()));
        holder.tvCircleNum.setText("帖子：" + YHStringUtils.quantityFormat(item.getDynamicCount()));
        holder.tvDesc.setText(YHStringUtils.value(item.getTopicDesc()));
        ImageLoaderUtil.loadImg(holder.rIVCircleCover, item.getTitleUrl());
        holder.root.setOnClickListener(v -> ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                .withString("id", item.getId()).navigation());
    }

    private void setContent(ContentViewHolder holder, HomeSearchResultModel model, int position) {
        MomentListModel item = model.getContent();
        //基础信息
        String nickName = YHStringUtils.pickName(item.getNickName(), item.getFriendRemark());
        holder.tvUserName.setText(nickName);

        String publishTime = item.getCreatedTime();
        holder.tvPublishTime.setText(YXTimeUtils.getFriendlyTimeAtContent(publishTime, true));//将后台返回数据进行加工处理

        //操作按钮
        int fabulousCount = item.getFabulousCount();
        boolean isFabulous = item.isFabulous();
        holder.tvLikeNum.setText(fabulousCount > 0 ? YHStringUtils.quantityFormat(fabulousCount) : "点赞");

        holder.iconLike.setText(isFabulous?"\ue772":"\ue71b");
        holder.iconLike.setSelected(isFabulous);

        int favCount = item.getCollectCount();
        boolean isFav = item.isCollect();
        holder.tvFavNum.setText(favCount > 0 ? YHStringUtils.quantityFormat(favCount) : "收藏");

        holder.iconFav.setText(isFav?"\ue760":"\ue742");
        holder.iconFav.setSelected(isFav);

        int replyCount = item.getReplyCount();
        holder.tvCommentNum.setText(replyCount > 0 ? YHStringUtils.quantityFormat(replyCount) : "评论");

        String userPhoto = item.getUserPhoto();
        if (!TextUtils.isEmpty(userPhoto)) {
            ImageLoaderUtil.loadImg(holder.ivUserHead, userPhoto);
        } else {
            holder.ivUserHead.setImageResource(R.drawable.place_holder);
        }

        String pUserId = item.getUserId();
        if (UserManager.getInstance().isMyself(pUserId)) {
            holder.llFollow.setVisibility(View.GONE);
        } else {
            holder.llFollow.setVisibility(View.VISIBLE);

            //(value = "关注状态 1 未关注 2 已关注 3 被关注 4互相关注")
            //    private Integer followStatus;
            int followStatus = item.getFollowStatus();

            boolean isFollow = DiscoValueFormat.isFollow(followStatus);
            holder.iconFollowStatus.setVisibility(isFollow ? View.GONE : View.VISIBLE);


            holder.tvFollow.setTextColor(context.getResources().getColor(isFollow ? R.color.assistEACBFF : R.color.theme));
            holder.tvFollow.setText(isFollow ? "已关注" : "关注");

            holder.tvFollow.setTextSize(Dimension.SP, (isFollow ? 12 : 16));
            holder.llFollow.setBackgroundResource(isFollow ? R.drawable.shape_half_r_eacbff : R.drawable.shape_half_r_tran);
        }

        holder.llFollow.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.FOLLOW, item, position);
            }
        });
        holder.llFav.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.COLLECT, item, position);
            }
        });
        holder.llComment.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.COMMENT, item, position);
            }
        });
        holder.llLike.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.THUMB_UP, item, position);
            }
        });
        holder.llItem.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position);
            }
        });

        List<TagFlowModel> tagList = new ArrayList<>();
        String issueCity = item.getIssueCity();
        String issueAddress = item.getIssueAddress();
        String issueAddressDetail = item.getIssueAddressDetail();
        String issueLatitude = item.getIssueLatitude();
        String issueLongitude = item.getIssueLongitude();

        if (TextUtils.isEmpty(issueLatitude) && TextUtils.isEmpty(issueLongitude)) {
        } else {
            String address = "";
            if (!TextUtils.isEmpty(issueCity)) {
                address = TextUtils.isEmpty(issueAddress) ? issueCity : issueCity + "." + issueAddress;
            } else if (!TextUtils.isEmpty(issueAddress)) {
                address = issueAddress;
            }

            if (!TextUtils.isEmpty(address)) {
                TagFlowModel addressTag = new TagFlowModel(TagFlowModel.ADDRESS, "0", address, R.mipmap.ic_address);
                addressTag.setIssueLatitude(issueLatitude);
                addressTag.setIssueLongitude(issueLongitude);

                tagList.add(addressTag);
            }
        }

        //获取圈子
        List<CircleModel> circleList = item.getCircleList();
        if (null != circleList && circleList.size() > 0) {
            for (CircleModel circleItem : circleList) {
                TagFlowModel tag = new TagFlowModel(TagFlowModel.CIRCLE, circleItem.getId(), circleItem.getTitle(), R.mipmap.ic_circle);

                if (circleItem.getDeleted() != 1) {
                    tagList.add(tag);
                }
            }
        }

        if (tagList.size() > 0) {
            holder.tflMomentItem.setVisibility(View.VISIBLE);
            //控制显示5个
            holder.tflMomentItem.setAdapter(new TagAdapter<TagFlowModel>(tagList) {
                @Override
                public View getView(FlowLayout parent, int position, TagFlowModel tagItem) {
                    //加载tag布局
                    View view = LayoutInflater.from(context).inflate(R.layout.item_tag_flow, parent, false);
                    //获取标签
                    TextView tvTagName = view.findViewById(R.id.tvTagName);
                    ImageView ivTagIcon = view.findViewById(R.id.ivTagIcon);

                    ivTagIcon.setImageResource(tagItem.getDrawable());
                    tvTagName.setText(tagItem.getName());
                    return view;
                }
            });
            holder.tflMomentItem.setOnTagClickListener((view, pos, parent) -> {
                TagFlowModel tagFlowModel = tagList.get(pos);
                if (tagFlowModel.getType() == TagFlowModel.ADDRESS) {

                } else {
                    ARouter.getInstance().build(ARouterPath.CIRCLE_DETAIL_ACTIVITY)
                            .withString("id", tagFlowModel.getId()).navigation();
                }

                return true;
            });
        } else {
            holder.tflMomentItem.setVisibility(View.GONE);
        }

        //获取图片1 视频2 文章3
        int contentType = item.getContentType();

        String urlJson = item.getContentUrl();
        String content = "";
        switch (contentType) {
            case 1:
            case 4:
                holder.gsyVideoPlayer.setVisibility(View.GONE);
                holder.rlVideoItemPlayer.setVisibility(View.GONE);
                holder.flTopTitle.setVisibility(View.GONE);

                content = item.getContent();

                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    List<String> imageUrl = new ArrayList<>();
                    for (FileResult fileResult : fileResults) {
                        imageUrl.add(fileResult.getHttpUrl());
                    }

                    if (!imageUrl.isEmpty()) {
                        holder.flImgContainer.setVisibility(View.VISIBLE);
                        ShowDiscoListImage.showGridImageNew(context,holder.gridImage,holder.tvIndex, imageUrl);
                    } else {
                        holder.flImgContainer.setVisibility(View.GONE);
                    }
                }

                break;
            case 2:
                holder.flImgContainer.setVisibility(View.GONE);

                holder.gsyVideoPlayer.setVisibility(View.VISIBLE);
                holder.rlVideoItemPlayer.setVisibility(View.VISIBLE);
                holder.flTopTitle.setVisibility(View.GONE);

                content = item.getContent();
                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    if (!fileResults.isEmpty()) {
                        holder.rlVideoItemPlayer.setVisibility(View.VISIBLE);

                        holder.gsyVideoPlayer.setOnVideoListener(new SampleCoverVideo.OnVideoListener() {
                            @Override
                            public void setOnMute(boolean isMute) {
//                    HomeFollowAdapter.this.notifyDataSetChanged();
                            }
                        });

                        holder.gsyVideoPlayer.setOnDataNetPlayListener(new SampleCoverVideo.OnDataNetPlayListener() {
                            @Override
                            public void play() {
                                YXConfig.needShowNoWifi = false;
                                String currentTime = String.valueOf(System.currentTimeMillis());
                                DiscoCacheUtils.getInstance().setFirstOpenTime(currentTime);
                            }

                            @Override
                            public void stop() {
                            }

                            @Override
                            public void detail() {
                                if (mListener != null) {
                                    mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position);
                                }
                            }
                        });

                        holder.gsyVideoPlayer.setVideoTime(item.getVideoTime());

                        FileResult fileResult = fileResults.get(0);

                        String coverImage = fileResult.getHttpUrl();
                        String url = fileResults.get(1).getHttpUrl();//

                        if (!url.equals(holder.gsyVideoPlayer.getTag())) {
                            int width = Integer.parseInt(TextUtils.isEmpty(fileResult.getWidth()) ? "0" : fileResult.getWidth());
                            int height = Integer.parseInt(TextUtils.isEmpty(fileResult.getHeight()) ? "0" : fileResult.getHeight());

                            holder.gsyVideoPlayer.loadCoverImage(coverImage, R.drawable.bg_tran_circle_no_stroke, width, height);

                            holder.gsyVideoPlayer.setUpLazy(url, true, null, null, "");
                            //增加title
                            holder.gsyVideoPlayer.getTitleTextView().setVisibility(View.GONE);

                            //设置返回键
                            holder.gsyVideoPlayer.getBackButton().setVisibility(View.GONE);

                            holder.gsyVideoPlayer.setRotateViewAuto(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setLockLand(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setPlayTag("Disco");
                            holder.gsyVideoPlayer.setReleaseWhenLossAudio(false);
                            holder.gsyVideoPlayer.setAutoFullWithSize(true);
                            holder.gsyVideoPlayer.setShowFullAnimation(!getListNeedAutoLand());
                            holder.gsyVideoPlayer.setIsTouchWiget(false);
                            holder.gsyVideoPlayer.setNeedShowWifiTip(false);
                            //循环
                            holder.gsyVideoPlayer.setNeedLockFull(true);
                            holder.gsyVideoPlayer.setPlayPosition(position);

                            ViewGroup.LayoutParams imageLayoutParams = holder.gsyVideoPlayer.getLayoutParams();
                            int sw = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 24);//设置图片的宽


                            if (height > 0 && width > 0) {
                                int showWidth;
                                int showHeight;
                                if (sw > width) {
                                    showWidth = width;
                                    showHeight = height;
                                } else {
                                    showWidth = sw;
                                    showHeight = sw * height / width;
                                }

                                imageLayoutParams.width = showWidth;
                                imageLayoutParams.height = showHeight;
                            } else {
                                imageLayoutParams.width = sw;
                                imageLayoutParams.height = imageLayoutParams.width * 3 / 4;
                            }

                            holder.gsyVideoPlayer.setLayoutParams(imageLayoutParams);
                            holder.gsyVideoPlayer.setTag(url);
                        }

                    } else {
                        holder.rlVideoItemPlayer.setVisibility(View.GONE);
                    }
                } else {
                    holder.rlVideoItemPlayer.setVisibility(View.GONE);
                }
                break;
            case 3:
                holder.flImgContainer.setVisibility(View.GONE);
                holder.gsyVideoPlayer.setVisibility(View.GONE);
                holder.rlVideoItemPlayer.setVisibility(View.GONE);
                holder.flTopTitle.setVisibility(View.VISIBLE);

                //先获取导语，没有则从正文里头取
                content = YHStringUtils.pickLastFirst(item.getIntroduce(), item.getContent());
                content = YHStringUtils.getHtmlContent(content);
                contentType = 3;

                holder.tvTopTitle.setText(item.getContentTitle());

                ViewGroup.LayoutParams imageLayoutParams = holder.flTopTitle.getLayoutParams();
                int sw = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 24);//设置图片的宽

                imageLayoutParams.width = sw;
                imageLayoutParams.height = imageLayoutParams.width * 3 / 5;
                holder.flTopTitle.setLayoutParams(imageLayoutParams);

                if (!TextUtils.isEmpty(urlJson)) {
                    Type listType = new TypeToken<ArrayList<FileResult>>() {
                    }.getType();
                    ArrayList<FileResult> fileResults = GsonUtils.fromJson(urlJson, listType);

                    if (null != fileResults && fileResults.size() > 0) {
                        FileResult fileResult = fileResults.get(0);

                        ImageLoaderUtil.loadImgCenterCrop(holder.ivTopCover, fileResult.getHttpUrl());
                    }
                }
                break;
        }

        //文本内容展示
        if (!TextUtils.isEmpty(content)) {
            List<FriendModel> nameList = item.getUserList();//At的用户

            List<TopicModel> topicList = new ArrayList<>();//存在的话题

            List<TopicModel> topicListItem = item.getTopicList();//话题
            if (null != topicListItem && topicListItem.size() > 0) {
                for (TopicModel topic : topicListItem) {
                    if (topic.getDeleted() != 1) {
                        topicList.add(topic);
                    }
                }
            }

            if (contentType == 3) {
                if (null != topicList && topicList.size() > 0) {
                    String topicContent = "";

                    for (int i = 0; i < topicList.size(); i++) {
                        topicContent = topicContent + "#" + topicList.get(i).getTitle() + "# ";
                    }
                    content = topicContent + content;
                }
            }

            //直接使用RichTextView
            SpanAtUserCallBack spanAtUserCallBack = (view, userModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                        .withString("userId", userModel.getUserId()).navigation();
            };

            SpanTopicCallBack spanTopicCallBack = (view, topicModel) -> {
                if (view instanceof TextView) {
                    ((TextView) view).setHighlightColor(Color.TRANSPARENT);
                }
                ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                        .withString("id", topicModel.getId()).navigation();
            };

            holder.richTextView.setOnTouchListener(new CustomTextViewTouchListener(
                    v -> {
                        if (mListener != null) {
                            mListener.onHandleClick(OnHandleClickListener.TO_DETAIL, item, position);
                        }
                    }
            ));
            holder.richTextView.setSpanAtUserCallBackListener(spanAtUserCallBack);
            holder.richTextView.setSpanTopicCallBackListener(spanTopicCallBack);
            //所有配置完成后才设置text
            content = YHStringUtils.firstAtAddSpace(content);
            holder.richTextView.setRichText(content, null != nameList ? nameList : new ArrayList<>(), topicList);

            holder.richTextView.setVisibility(View.VISIBLE);
        } else {
            holder.richTextView.setVisibility(View.GONE);
        }
    }

    private void showGridImage(ContentViewHolder holder, List<String> imgUrls) {
        ViewGroup.LayoutParams imageLayoutParams = holder.gridImage.getLayoutParams();
        holder.gridImage.removeAllViews();

        int imgSize = imgUrls.size();

        int screenWidth = DisplayUtils.getScreenWidth(context);
        //默认初始值--
        //展示1:1，大致为宽的一半。计算方式为screenWidth-左右边距24
        int showWidth = (screenWidth - DisplayUtils.dip2px(context, 14));
        int showHeight = showWidth / 2;

        if (imgSize >= 3) {
            int leftShowCount = imgSize - 3;
            if (leftShowCount > 0) {
                holder.tvIndex.setVisibility(View.VISIBLE);
                holder.tvIndex.setText(leftShowCount + "+");
            } else {
                holder.tvIndex.setVisibility(View.GONE);
                holder.tvIndex.setText("0");
            }
            ////展示2:1，大致为宽的一半。计算方式为screenWidth-左右边距24,第一张图比较大
            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showWidth / 3 * 2;
        } else {
            holder.tvIndex.setVisibility(View.GONE);
            holder.tvIndex.setText("0");

            imageLayoutParams.width = showWidth;
            imageLayoutParams.height = showHeight;
        }

        holder.gridImage.setLayoutParams(imageLayoutParams);

        //根据UI的逻辑，一张和两张展示都只有一行
        //1--（0,0），（0,1）
        //2--（0,0），（0,1）
        //3--（0,0），（0,1），（1,1）

        //使用Spec定义子控件的位置和比重 设置行列下标， 所占行列  ，比重
        // 设置行列下标， 所占行列  ，比重
        // 对应： layout_row  , layout_rowSpan , layout_rowWeight
        //GridLayout.spec(0, 2, 2f)
        if (imgSize >= 3) {
            for (int i = 0; i < imgUrls.size(); i++) {
                ImageView imageView = new ImageView(context);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                if (i == 0) {
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 2, 2f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 2f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是
                    layoutParams.rightMargin = DisplayUtils.dip2px(context, 1);

                    holder.gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 1) {//(0,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(context, 1);
                    layoutParams.bottomMargin = DisplayUtils.dip2px(context, 1);

                    holder.gridImage.addView(imageView, layoutParams);
                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
                if (i == 2) {////(1,1)
                    GridLayout.Spec rowSpec = GridLayout.spec(1, 1, 1f);//设置行列下标， 所占行列  ，比重
                    GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);

                    //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                    layoutParams.height = 0;
                    layoutParams.width = 0;
                    //还可以根据位置动态定义子控件直接的边距，下面的意思是

                    layoutParams.leftMargin = DisplayUtils.dip2px(context, 1);
                    layoutParams.topMargin = DisplayUtils.dip2px(context, 1);

                    holder.gridImage.addView(imageView, layoutParams);

                    ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
                }
            }
        } else if (imgSize == 2) {
            for (int i = 0; i < imgUrls.size(); i++) {

                ImageView imageView = new ImageView(context);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
                GridLayout.Spec columnSpec = GridLayout.spec(i, 1, 1f);

                //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
                GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
                layoutParams.height = 0;
                layoutParams.width = 0;
                //还可以根据位置动态定义子控件直接的边距，下面的意思是

                if (i == 0)
                    layoutParams.rightMargin = DisplayUtils.dip2px(context, 1);
                if (i == 1) {
                    layoutParams.leftMargin = DisplayUtils.dip2px(context, 1);
                }

                holder.gridImage.addView(imageView, layoutParams);
                ImageLoaderUtil.loadImg(imageView, imgUrls.get(i));
            }
        } else {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            GridLayout.Spec imgRowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec imgColumnSpec = GridLayout.spec(0, 1, 1f);

            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams imglayoutParams = new GridLayout.LayoutParams(imgRowSpec, imgColumnSpec);
            imglayoutParams.height = 0;
            imglayoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是
            imglayoutParams.rightMargin = DisplayUtils.dip2px(context, 1);
            holder.gridImage.addView(imageView, imglayoutParams);
            ImageLoaderUtil.loadImg(imageView, imgUrls.get(0));

            //占位的
            View functionView = new View(context);
            GridLayout.Spec rowSpec = GridLayout.spec(0, 1, 1f);//设置行列下标， 所占行列  ，比重
            GridLayout.Spec columnSpec = GridLayout.spec(1, 1, 1f);
            //将Spec传入GridLayout.LayoutParams并设置宽高为0，必须设置宽高，否则视图异常
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(rowSpec, columnSpec);
            layoutParams.height = 0;
            layoutParams.width = 0;
            //还可以根据位置动态定义子控件直接的边距，下面的意思是

            layoutParams.leftMargin = DisplayUtils.dip2px(context, 1);
            holder.gridImage.addView(functionView, layoutParams);
        }
    }

    /**
     * 列表时是否需要支持重力旋转
     *
     * @return 返回true为支持列表重力全屏
     */
    public boolean getListNeedAutoLand() {
        return true;
    }

    @Override
    public int getItemCount() {
        return resultList == null ? 0 : resultList.size();
    }

    public void setItems(List<HomeSearchResultModel> data) {
        this.resultList = data;
        notifyDataSetChanged();
    }

    public void setMoreData(List<HomeSearchResultModel> list) {
        int start = resultList.size();
        if (list != null && list.size() > 0) {
            resultList.addAll(list);
            int end = resultList.size();
            notifyItemRangeInserted(start, end);
        }
    }

    static class UserViewHolder extends BaseViewHolder {
        @BindView(R2.id.iv_header)
        CircleImageView iv_header;
        @BindView(R2.id.tv_name)
        TextView tv_name;
        @BindView(R2.id.tvSign)
        TextView tvSign;
        @BindView(R2.id.ll_item)
        RelativeLayout ll_item;
        @BindView(R2.id.btn_follow)
        StatusView btn_follow;
        @BindView(R2.id.ivInfoSex)
        ImageView ivInfoSex;

        public UserViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class CircleViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;
        @BindView(R2.id.ll2Detail)
        LinearLayout ll2Detail;

        public CircleViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class TopicViewHolder extends BaseViewHolder {
        @BindView(R2.id.root)
        RelativeLayout root;
        @BindView(R2.id.tvCircieName)
        TextView tvCircieName;
        @BindView(R2.id.tvDesc)
        TextView tvDesc;
        @BindView(R2.id.tvCircleNum)
        TextView tvCircleNum;
        @BindView(R2.id.rIVCircleCover)
        ShapeableImageView rIVCircleCover;

        public TopicViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class ContentViewHolder extends BaseViewHolder {
        @BindView(R2.id.tflMomentItem)
        TagFlowLayout tflMomentItem;
        @BindView(R2.id.gridImage)
        GridLayout gridImage;
        @BindView(R2.id.tvIndex)
        OswaldTextView tvIndex;
        @BindView(R2.id.richText)
        RichTextView richTextView;
        @BindView(R2.id.flImgContainer)
        FrameLayout flImgContainer;
        @BindView(R2.id.tvUserName)
        TextView tvUserName;
        @BindView(R2.id.tvPublishTime)
        TextView tvPublishTime;
        //关注
        @BindView(R2.id.llFollow)
        LinearLayout llFollow;
        @BindView(R2.id.iconFollowStatus)
        AliIconFontTextView iconFollowStatus;
        @BindView(R2.id.tvFollow)
        TextView tvFollow;
        //收藏
        @BindView(R2.id.llFav)
        LinearLayout llFav;
        @BindView(R2.id.iconFav)
        AliIconFontTextView iconFav;
        @BindView(R2.id.tvFavNum)
        TextView tvFavNum;
        //评论
        @BindView(R2.id.llComment)
        LinearLayout llComment;

        @BindView(R2.id.llItem)
        LinearLayout llItem;

        @BindView(R2.id.iconComment)
        AliIconFontTextView iconComment;
        @BindView(R2.id.tvCommentNum)
        TextView tvCommentNum;
        //点赞
        @BindView(R2.id.llLike)
        LinearLayout llLike;
        @BindView(R2.id.iconLike)
        AliIconFontTextView iconLike;
        @BindView(R2.id.tvLikeNum)
        TextView tvLikeNum;

        @BindView(R2.id.ivUserHead)
        CircleImageView ivUserHead;
        @BindView(R2.id.video_item_player)
        SampleCoverVideo gsyVideoPlayer;
        @BindView(R2.id.rlvideo_item_player)
        RelativeLayout rlVideoItemPlayer;
        @BindView(R2.id.container)
        FrameLayout container;
        @BindView(R2.id.flTopTitle)
        FrameLayout flTopTitle;
        @BindView(R2.id.ivTopCover)
        ImageView ivTopCover;
        @BindView(R2.id.tvTopTitle)
        TextView tvTopTitle;

        public ContentViewHolder(View itemView) {
            super(itemView);
        }
    }

}
