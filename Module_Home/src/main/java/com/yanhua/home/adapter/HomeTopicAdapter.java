package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.HomeTopicPageModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.core.widget.tagflow.FlowLayout;
import com.yanhua.core.widget.tagflow.TagAdapter;
import com.yanhua.core.widget.tagflow.TagFlowLayout;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import java.util.List;

import butterknife.BindView;


public class HomeTopicAdapter extends BaseRecyclerAdapter<HomeTopicPageModel, HomeTopicAdapter.ViewHolder> {
    public HomeTopicAdapter(Context context) {
        super(context);
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_home_topic, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull HomeTopicPageModel item) {
        List<TopicModel> topicList = item.getTopicList();

        //控制显示5个
        holder.tflTopic.setAdapter(new TagAdapter<TopicModel>(topicList) {
            @Override
            public View getView(FlowLayout parent, int position, TopicModel s) {
                //加载tag布局
                View view = LayoutInflater.from(context).inflate(R.layout.item_topic_tag, parent, false);
                //获取标签
                TextView tvTag = view.findViewById(R.id.tvTag);
                tvTag.setText(s.getTitle());
                return view;
            }
        });

        holder.tflTopic.setOnTagClickListener((view, position, parent) -> {
            TopicModel topic = topicList.get(position);
            ARouter.getInstance().build(ARouterPath.TOPIC_DETAIL_ACTIVITY)
                    .withString("id", topic.getId()).navigation(context);

            return true;
        });
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tflTopic)
        TagFlowLayout tflTopic;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
