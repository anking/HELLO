package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

//主页分类显示
public class StrategyClassifyAdapter extends BaseRecyclerAdapter<NewsTypeModel, StrategyClassifyAdapter.ViewHolder> {

    private int selectPos;

    public StrategyClassifyAdapter(Context context, int pos) {
        super(context);
        selectPos = pos;
    }

    public void setSelectPos(int pos) {
        selectPos = pos;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_strategy_classify, parent, false));

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull NewsTypeModel item) {
        int position = getPosition(holder);

        ImageLoaderUtil.loadImgCenterCrop(holder.ivIcon, item.getLogo(), R.drawable.place_holder);
        holder.tvName.setText(item.getReportName());

        if (position == selectPos) {
            holder.itemContainer.setSelected(true);
        } else {
            holder.itemContainer.setSelected(false);
        }
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivIcon)
        ImageView ivIcon;
        @BindView(R2.id.tvName)
        TextView tvName;
        @BindView(R2.id.itemContainer)
        LinearLayout itemContainer;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
