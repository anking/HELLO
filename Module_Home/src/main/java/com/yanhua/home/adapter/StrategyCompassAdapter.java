package com.yanhua.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.TitleBoldTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;


public class StrategyCompassAdapter extends BaseRecyclerAdapter<StrategyHotSortModel, StrategyCompassAdapter.ViewHolder> {
    public StrategyCompassAdapter(Context context) {
        super(context);
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_strategy_compass, parent, false));

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull StrategyHotSortModel item) {
        int position = getPosition(holder);
        String coverUrl = "";
        if (!TextUtils.isEmpty(item.getVedioCoverUrl())) {
            coverUrl = item.getVedioCoverUrl();
            holder.ivVideoPlay.setVisibility(View.VISIBLE);
        } else {
            coverUrl = item.getCoverUrl();
            holder.ivVideoPlay.setVisibility(View.GONE);
        }

        ImageLoaderUtil.loadImgCenterCrop(context, holder.ivCover, coverUrl, R.drawable.place_holder, 12);

        holder.tvHotValue.setText(item.getWeightValue() + "热度");
        holder.tvTopIndex.setText("TOP." + (position + 1));

        holder.tvTitle.setText(YHStringUtils.value(item.getContentTitle()));
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.ivVideoPlay)
        ImageView ivVideoPlay;
        @BindView(R2.id.tvHotValue)
        TextView tvHotValue;
        @BindView(R2.id.tvTopIndex)
        TitleBoldTextView tvTopIndex;
        @BindView(R2.id.tvTitle)
        TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
