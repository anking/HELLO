package com.yanhua.home.adapter;

import android.graphics.Color;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Dimension;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.yanhua.common.model.SelectConditionModel;
import com.yanhua.core.view.RecyclerTabLayout;
import com.yanhua.home.R;

/**
 * 自定义
 */
public class TopLineAdapter extends RecyclerTabLayout.Adapter<TopLineAdapter.ViewHolder> {
    private LooperPageAdapter mAdapater;

    public TopLineAdapter(ViewPager viewPager) {
        super(viewPager);
        mAdapater = (LooperPageAdapter) mViewPager.getAdapter();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_top_line, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SelectConditionModel data = mAdapater.getValueAt(position);

        SpannableString name = new SpannableString(data.getFieldValue());
        if (position == getCurrentIndicatorPosition()) {
            holder.tvName.setTextColor(Color.parseColor("#36384A"));
            holder.tvName.setTextSize(Dimension.SP, 34);
        } else {
            holder.tvName.setTextColor(Color.parseColor("#B4B8BC"));
            holder.tvName.setTextSize(Dimension.SP, 26);
        }
        holder.tvName.setText(name);
    }

    @Override
    public int getItemCount() {
        return mAdapater.getCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getViewPager().setCurrentItem(getAdapterPosition());
                }
            });
        }
    }
}
