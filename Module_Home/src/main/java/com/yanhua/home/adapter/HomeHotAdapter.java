package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;


public class HomeHotAdapter extends BaseRecyclerAdapter<BreakNewsRecommendModel, HomeHotAdapter.ViewHolder> {
    public HomeHotAdapter(Context context) {
        super(context);
    }


    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_home_hot, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull BreakNewsRecommendModel item) {
        int position = getPosition(holder);

        String coverUrl = item.getShowUrl();
        int fabulousCount = item.getFabulousCount();
        int commentCount = item.getCommentCount();

        ImageLoaderUtil.loadImgCenterCrop(holder.ivCover, coverUrl, R.drawable.place_holder);

        holder.tvCommentNum.setText(YHStringUtils.quantityFormat(commentCount));
        holder.tvLikeNum.setText(YHStringUtils.quantityFormat(fabulousCount));

        holder.tvHotTitle.setText(YHStringUtils.value(item.getContentTitle()));
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R2.id.ivCover)
        ImageView ivCover;
        @BindView(R2.id.tvCommentNum)
        TextView tvCommentNum;
        @BindView(R2.id.tvLikeNum)
        TextView tvLikeNum;

        @BindView(R2.id.tvHotTitle)
        TextView tvHotTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
