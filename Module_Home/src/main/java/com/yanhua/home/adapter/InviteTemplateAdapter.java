package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.home.R;
import com.yanhua.home.R2;


import butterknife.BindView;


public class InviteTemplateAdapter extends BaseRecyclerAdapter<InviteTemplateModel, InviteTemplateAdapter.ViewHolder> {
    public InviteTemplateAdapter(Context context) {
        super(context);
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_invite_template, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull InviteTemplateModel item) {
        int position = getPosition(holder);

        if (null != mSelectId && mSelectId.equals(item.getMeetPartnerTypeId())) {
            holder.flItem.setSelected(true);
        } else {
            holder.flItem.setSelected(false);
        }

        ImageLoaderUtil.loadImg(holder.ivCover, item.getUrl());
    }

    private String mSelectId;

    public void setSelectId(String id) {
        mSelectId = id;

        notifyDataSetChanged();
    }


    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.flItem)
        FrameLayout flItem;

        @BindView(R2.id.ivCover)
        ImageView ivCover;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
