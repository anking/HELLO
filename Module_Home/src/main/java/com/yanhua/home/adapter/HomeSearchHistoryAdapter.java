package com.yanhua.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.yanhua.base.adapter.BaseRecyclerAdapter;
import com.yanhua.base.adapter.BaseViewHolder;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.home.R;
import com.yanhua.home.R2;

import butterknife.BindView;

public class HomeSearchHistoryAdapter extends BaseRecyclerAdapter<String, HomeSearchHistoryAdapter.ViewHolder> {

    public HomeSearchHistoryAdapter(Context context) {
        super(context);
    }

    private OnDeleteRecordListener listener;

    public interface OnDeleteRecordListener {
        void doDelete(int pos);
    }

    public void setOnDeleteRecordListener(OnDeleteRecordListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    protected ViewHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.item_home_search_history, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, @NonNull String item) {
        int pos = getPosition(holder);
        holder.tvRecordTitle.setText(item);
        holder.tvDelete.setOnClickListener(v -> {
            if (listener != null) {
                listener.doDelete(pos);
            }
        });
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R2.id.tvRecordTitle)
        TextView tvRecordTitle;
        @BindView(R2.id.tvDelete)
        AliIconFontTextView tvDelete;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
