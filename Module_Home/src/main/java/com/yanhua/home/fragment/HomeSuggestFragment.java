package com.yanhua.home.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.HomeSuggestAdapter;
import com.yanhua.home.presenter.BestNoteListPresenter;
import com.yanhua.home.presenter.contract.BestNoteListContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 此刻列表数据
 */
public class HomeSuggestFragment extends BaseMvpFragment<BestNoteListPresenter> implements BestNoteListContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private int type;
    private String city;


    private HomeSuggestAdapter mAdapter;
    private List<MomentListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_suggest_channel;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new BestNoteListPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            city = bundle.getString("city");
            type = bundle.getInt("type");
        }

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    /**
     * 获取列表数据
     * @param page
     */
    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getBestNoteList("推荐".equals(city) ? "" : city, current, size);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });


        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new HomeSuggestAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);
    }


    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        }else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
