package com.yanhua.home.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.Gson;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.event.EventName;
import com.yanhua.base.event.MessageEvent;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.adapter.AdvertiseBannerAdapter;
import com.yanhua.common.config.CommonConstant;
import com.yanhua.common.listener.OnHandleClickListener;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.HomeTopicPageModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.HomeStaggeredItemDecoration;
import com.yanhua.core.util.DisplayUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.core.view.AliIconFontTextView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.HomeHotAdapter;
import com.yanhua.home.adapter.HomeSuggestAdapter;
import com.yanhua.home.adapter.HomeTopicAdapter;
import com.yanhua.home.presenter.HomePresenter;
import com.yanhua.home.presenter.contract.HomeContract;
import com.yanhua.home.view.HomePagePopAd;
import com.youth.banner.Banner;
import com.youth.banner.config.IndicatorConfig;
import com.youth.banner.indicator.RectangleIndicator;
import com.zackratos.ultimatebarx.ultimatebarx.java.UltimateBarX;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeFragment extends BaseMvpFragment<HomePresenter> implements HomeContract.IView, AMapLocationListener {
//    @BindView(R2.id.toolBar)
//    LinearLayout toolBar;

    @BindView(R2.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;

    @BindView(R2.id.rcvHot)
    RecyclerView rcvHot;

    @BindView(R2.id.rvTopic)
    RecyclerView rvTopic;

    @BindView(R2.id.rvContent)
    RecyclerView rvContent;

    @BindView(R2.id.llAddress)
    LinearLayout llAddress;

    @BindView(R2.id.llSearch)
    LinearLayout llSearch;

    @BindView(R2.id.tvPublish)
    AliIconFontTextView tvPublish;

    @BindView(R2.id.tvAddress)
    TextView tvAddress;

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R2.id.ivMenuInvite)
    ImageView ivMenuInvite;
    @BindView(R2.id.ivMenu9Bar)
    ImageView ivMenu9Bar;
    @BindView(R2.id.ivMenuCircle)
    ImageView ivMenuCircle;

    @BindView(R2.id.llNew)
    LinearLayout llNew;
    @BindView(R2.id.llUpper)
    LinearLayout llUpper;
    @BindView(R2.id.llCovid19)
    LinearLayout llCovid19;
    @BindView(R2.id.llDisco)
    LinearLayout llDisco;
    @BindView(R2.id.llNote)
    LinearLayout llNote;
    @BindView(R2.id.banner_home)
    Banner bannerHome;
    @BindView(R2.id.llSwitchOther)
    LinearLayout llSwitchOther;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;
    HomeSuggestAdapter mAdapter;
    private List<MomentListModel> mListData;
    private HomeHotAdapter hotAdapter;
    private HomeTopicAdapter topicAdapter;

    @Override
    protected void creatPresent() {
        basePresenter = new HomePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();
        if (id == R.id.ivMenu9Bar) {
            ARouter.getInstance().build(ARouterPath.BAR_LIST_ACTIVITY).navigation(getContext());
        } else if (id == R.id.llSwitchOther) {
            boolean isCanNext = (homeBreakNewsTotal - homeBreakNewsPage * YXConfig.HOME_BREAK_NEWS_SIZE) >= 3;//超过三就可以请求下一页

            if (!isCanNext) {
                homeBreakNewsPage = 1;
            } else {
                homeBreakNewsPage++;
            }

            basePresenter.homePageBreakNewsList(homeBreakNewsPage, YXConfig.HOME_BREAK_NEWS_SIZE);
        } else if (id == R.id.ivMenuCircle) {
            EventBus.getDefault().post(new MessageEvent(ARouterPath.RANKING_LIST_ACTIVITY));
//            ARouter.getInstance().build(ARouterPath.RANKING_LIST_ACTIVITY).withInt("rankingTopic", MomentHotListModel.CIRCLE).navigation(getContext());
        } else if (id == R.id.ivMenuInvite) {
            ARouter.getInstance().build(ARouterPath.INVITE_LIST_ACTIVITY).navigation(getContext());
        } else if (id == R.id.llNew) {
            ARouter.getInstance().build(ARouterPath.BREAK_NEWS_ACTIVITY)
                    .navigation();
        } else if (id == R.id.llUpper) {
            ARouter.getInstance().build(ARouterPath.STRATEGY_NEWS_ACTIVITY)
                    .navigation();
        } else if (id == R.id.llCovid19) {
            ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                    .withString("title", "防疫政策")
                    .withString("url", "http://www.gov.cn/zhuanti/2021yqfkgdzc/mobile.htm")
                    .navigation();
        } else if (id == R.id.llDisco) {
            ARouter.getInstance().build(ARouterPath.ELECT_SOUND_LIST_ACTIVITY)
                    .navigation();
            /*
            ARouter.getInstance().build(ARouterPath.SHORT_VIDEO_DETAIL_ACTIVITY)
                    .withSerializable("contentModel", null)
                    .withInt("discoBest", 1)//电音集锦
                    .withString("categoryId", "")
                    .withString("city", "")
                    .withInt("module", 0)
                    .navigation();
                    */
        } else if (id == R.id.llNote) {
            ARouter.getInstance().build(ARouterPath.BEST_NOTE_ACTIVITY)
                    .navigation();
        }
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, ivMenu9Bar, ivMenuCircle, ivMenuInvite, llNew, llUpper, llCovid19, llDisco, llNote, llSwitchOther);
//        UltimateBarX.addStatusBarTopPadding(toolBar);

        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(false);
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                refreshLayout.finishRefresh(1000);//模拟
                refreshData();
            }
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        UltimateBarX.statusBarOnly(this)
                .transparent();
        UltimateBarX.addStatusBarTopPadding(toolbar);

        int halfBarHeight = UltimateBarX.getStatusBarHeight() / 2;
        //先设置为透明的
        toolbar.setBackgroundColor(Color.argb(0, 255, 255, 255));
        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int toolbarHeight = toolbar.getHeight();
            int alpha = (scrollY) * 255 / toolbarHeight;

            alpha = alpha > 255 ? 255 : alpha;

            if (scrollY >= 0 && scrollY < halfBarHeight) {
                llAddress.setBackgroundResource(R.drawable.shape_bg_r_white);
                llAddress.getBackground().setAlpha(255 - alpha);
                llSearch.setBackgroundResource(R.drawable.shape_bg_r_white);
                llSearch.getBackground().setAlpha(255 - alpha);

                tvPublish.setBackgroundResource(R.drawable.shape_bg_r_white);
                tvPublish.getBackground().setAlpha(255 - alpha);
            } else if (scrollY >= halfBarHeight && scrollY <= toolbarHeight) {
                llAddress.setBackgroundResource(R.drawable.shape_bg_r_bgui);
                llAddress.getBackground().setAlpha(alpha);
                llSearch.setBackgroundResource(R.drawable.shape_bg_r_bgui);
                llSearch.getBackground().setAlpha(alpha);

                tvPublish.setBackgroundResource(R.drawable.shape_bg_r_bgui);
                tvPublish.getBackground().setAlpha(alpha);
            }

            toolbar.setBackgroundColor(Color.argb(alpha, 255, 255, 255));
//            LogUtils.e("onScrollChange", "alpha=" + alpha + ",toolbarHeight=" + toolbarHeight + ",scrollY=" + scrollY + ",halfBarHeight=" + halfBarHeight);
        });

        StaggeredGridLayoutManager managerContent = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvContent.addItemDecoration(new HomeStaggeredItemDecoration(mContext, 8));
        mAdapter = new HomeSuggestAdapter(mContext);

        mAdapter.setOnHandleClickListener((OnHandleClickListener<MomentListModel>) (type, item, pos) -> {
            currentPosition = pos;

            String id = item.getId();

            if (type == OnHandleClickListener.THUMB_UP) {
                PageJumpUtil.firstIsLoginThenJump(() -> {
                    basePresenter.updateStarContent(id, YXConfig.TYPE_MOMENT);
                });
            }
        });

        rvContent.setLayoutManager(managerContent);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        RecycleViewUtils.clearRecycleAnimation(rvContent);
        rvContent.setAdapter(mAdapter);
    }

    /**
     * 下拉刷新获取数据
     */
    private void refreshData() {
        //列表数据
        if (null == mListData) {
            mListData = new ArrayList<>();
        } else {
            mListData.clear();
        }
        //获取话题
        basePresenter.getRecommendTopicList();

        homeBreakNewsPage = 1;
        basePresenter.homePageBreakNewsList(homeBreakNewsPage, YXConfig.HOME_BREAK_NEWS_SIZE);

        //获取首页轮播图
        basePresenter.queryAdList(YXConfig.AD.main_page);

        getListData(1);
    }

    private int homeBreakNewsPage = 1;
    private int homeBreakNewsTotal;

    @Override
    public void handHomePageBreakNewsList(ListResult<BreakNewsRecommendModel> data) {
        homeBreakNewsTotal = data.getTotal();

        List<BreakNewsRecommendModel> listData = data.getRecords();
        if (null != listData && listData.size() > 0) {
            hotAdapter.setItems(listData);

            if (homeBreakNewsPage != 1) {
                ToastUtils.showShort("已为您换一批");
            }
        }
    }

    /**
     * 获取列表数据
     *
     * @param page
     */
    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getBestNoteList("", current, size);
        }
    }


    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        EventBus.getDefault().register(this);
        //开启定位系统
        try {
            mlocationClient = new AMapLocationClient(getContext());
            if (null != mlocationClient) {
                //初始化定位参数
                mLocationOption = new AMapLocationClientOption();
                //设置定位监听
                mlocationClient.setLocationListener(this);
                //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
                mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
                //设置定位间隔,单位毫秒,默认为2000ms
                mLocationOption.setInterval(2000);
                //设置定位参数
                mlocationClient.setLocationOption(mLocationOption);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //获取数据
        refreshData();

        //获取首页轮播图
        basePresenter.queryAdList(YXConfig.AD.fp_popup_adv);

        LinearLayoutManager topicManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        topicAdapter = new HomeTopicAdapter(mContext);
        rvTopic.setLayoutManager(topicManager);
        rvTopic.setAdapter(topicAdapter);

        //===大家都在看---数据初始化---
        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        hotAdapter = new HomeHotAdapter(mContext);
        rcvHot.setLayoutManager(manager);
        rcvHot.setAdapter(hotAdapter);

        hotAdapter.setOnItemClickListener((itemView, pos) -> {
            BreakNewsRecommendModel breakNews = hotAdapter.getItemObject(pos);

            PageJumpUtil.jumpNewsDetailPage(breakNews.getId(), YXConfig.TYPE_BREAK_NEWS);
        });

        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        tvAddress.setText(TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress);
//        checkLocationPermission();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
    }

    public String plongitude = YXConfig.longitude;
    public String platitude = YXConfig.latitude;
    //声明mlocationClient对象
    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;

    private void checkLocationPermission() {
        String[] permissionsO = new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

        requestCheckPermission(permissionsO, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                mlocationClient.startLocation();
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_location_permission_tip);
            }
        });
    }

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
                double latitude = amapLocation.getLatitude();//获取纬度
                double longitude = amapLocation.getLongitude();//获取经度
                amapLocation.getAccuracy();//获取精度信息
                platitude = String.valueOf(latitude);//获取纬度
                plongitude = String.valueOf(longitude);//获取经度

                DiscoCacheUtils.getInstance().setLng(plongitude);
                DiscoCacheUtils.getInstance().setLat(platitude);

                String city = amapLocation.getCity();
                DiscoCacheUtils.getInstance().setCurrentCity(city);

                tvAddress.setText(city);
                mlocationClient.stopLocation();

                if (UserManager.getInstance().isLogin()) {
                    HashMap<String, Object> params = new HashMap<String, Object>();

                    params.put("address", amapLocation.getAddress());
                    params.put("city", city);
                    params.put("latitude", latitude);
                    params.put("longitude", longitude);
                    params.put("province", amapLocation.getProvince());

                    basePresenter.uploadUserLocation(params);
                }
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                mlocationClient.stopLocation();
                if (amapLocation.getErrorCode() == 12) {
                    // 缺少定位权限
                    ToastUtils.showShort("请开启定位服务");
                    LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
                    boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!ok) {// 开启了定位服务
                        CommonDialog commonDialog = new CommonDialog(getContext(), "是否开启定位服务？", "开启", "取消");
                        new XPopup.Builder(getContext()).asCustom(commonDialog).show();
                        commonDialog.setOnConfirmListener(() -> {
                            commonDialog.dismiss();
                            Intent i = new Intent();
                            i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(i, 1024);
                        });
                        commonDialog.setOnCancelListener(() -> {
                            commonDialog.dismiss();
                        });
                    }
                } else if (amapLocation.getErrorCode() == 6) {
                    // 定位失败
                    ToastUtils.showShort(R.string.fail_to_get_location);
                    platitude = "";//获取纬度
                    plongitude = "";//获取经度
                }
            }
        }
    }

    @OnClick({R2.id.llSearch, R2.id.llAddress, R2.id.llLookMore, R2.id.tvPublish})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.llSearch) {
            ARouter.getInstance().build(ARouterPath.HOME_SEARCH_ACTIVITY).navigation();
        } else if (id == R.id.llAddress) {
            ARouter.getInstance().build(ARouterPath.CITY_ACTIVITY)
                    .withString("city", tvAddress.getText().toString())
                    .withString("location", DiscoCacheUtils.getInstance().getCurrentCity())
                    .withString("plongitude", DiscoCacheUtils.getInstance().getLongitude())
                    .withString("platitude", DiscoCacheUtils.getInstance().getLatitude())
                    .navigation();
        } else if (id == R.id.llLookMore) {
            ARouter.getInstance().build(ARouterPath.BREAK_NEWS_ACTIVITY)
                    .navigation();
        } else if (id == R.id.tvPublish) {
            EventBus.getDefault().post(new MessageEvent(CommonConstant.TO_MOMENT_PUBLISH));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventName().equals(EventName.SELECT_CITY)) {
            CityModel cityModel = (CityModel) event.getSerializable();
            tvAddress.setText(cityModel.getAreaName());
        } else if (event.getEventName().equals(CommonConstant.REQUEST_PERMISSIONS)) {
            //启动定位
            new Handler().postDelayed(() -> checkLocationPermission(), 2000);
        }
    }

    @Override
    public void handleRecommendTopicList(List<TopicModel> listResult) {
        List<TopicModel> topicList = listResult;
        if (null != topicList && topicList.size() > 0) {
            rvTopic.setVisibility(View.VISIBLE);

            List<HomeTopicPageModel> homeTopicPages = new ArrayList<>();
            if (topicList.size() > 5) {
                HomeTopicPageModel topicPage1 = new HomeTopicPageModel();
                topicPage1.setTopicList(topicList.subList(0, 5));
                topicPage1.setPage(1);

                HomeTopicPageModel topicPage2 = new HomeTopicPageModel();
                topicPage2.setTopicList(topicList.subList(5, topicList.size()));
                topicPage2.setPage(2);

                homeTopicPages.add(topicPage1);
                homeTopicPages.add(topicPage2);
            } else {
                HomeTopicPageModel topicPage = new HomeTopicPageModel();
                topicPage.setTopicList(topicList);
                topicPage.setPage(1);

                homeTopicPages.add(topicPage);
            }

            topicAdapter.setItems(homeTopicPages);
        } else {
            rvTopic.setVisibility(View.GONE);
        }
    }


    @Override
    public void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        List<MomentListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }
        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }


    @Override
    public void handleAdvertiseList(List<AdvertiseModel> advertiseList, String code) {
        if (advertiseList != null && !advertiseList.isEmpty()) {
            if (YXConfig.AD.main_page.equals(code)) {
                bannerHome.setVisibility(View.VISIBLE);
                int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 24);
                int height = (int) (width * 0.25); // 4：1
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bannerHome.getLayoutParams();
                layoutParams.width = width;
                layoutParams.height = height;
                bannerHome.setLayoutParams(layoutParams);
                AdvertiseBannerAdapter adapter = new AdvertiseBannerAdapter(advertiseList, mContext);

                //展示滚动效果
                bannerHome.setAdapter(adapter);

                if (null != advertiseList && advertiseList.size() > 1) {
                    bannerHome.addBannerLifecycleObserver(this)
                            .setLoopTime(3000)
                            .isAutoLoop(true)
                            .setIndicator(new RectangleIndicator(mContext))
                            .setIndicatorRadius(DisplayUtils.dip2px(mContext, 6))
                            .setIndicatorNormalColor(ContextCompat.getColor(mContext, R.color.assistWord))
                            .setIndicatorSelectedColor(ContextCompat.getColor(mContext, R.color.sub_first))
                            .setIndicatorHeight(DisplayUtils.dip2px(mContext, 6))
                            .setIndicatorWidth(DisplayUtils.dip2px(mContext, 6), DisplayUtils.dip2px(mContext, 12))
                            .setIndicatorGravity(IndicatorConfig.Direction.CENTER)
                            .setIndicatorSpace(DisplayUtils.dip2px(mContext, 3))
                            .setIndicatorMargins(new IndicatorConfig.Margins(0, 0, 0, DisplayUtils.dip2px(mContext, 9)));

                }

                bannerHome.setOnBannerListener((data, position) -> {
                    AdvertiseModel clickItem = (AdvertiseModel) data;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("userId", UserManager.getInstance().getUserId());
                    params.put("id", clickItem.getId());

                    basePresenter.addMarketingClick(params);

                    PageJumpUtil.adJumpContent(clickItem, mActivity, -1);
                });
            } else if (YXConfig.AD.fp_popup_adv.equals(code)) {
                //根据上次的结果是不是相等，不相等就展示
                String homePopAd = DiscoCacheUtils.getInstance().getHomePopAd();

                String homePopAdNow = new Gson().toJson(advertiseList);
                if (null == homePopAd || (null != homePopAd && !homePopAd.equals(homePopAdNow))) {
                    DiscoCacheUtils.getInstance().setHomePopAd(homePopAdNow);

                    //取出第一个获取长高
                    AdvertiseModel firstItem = advertiseList.get(0);

                    String imgUrl = firstItem.getImageUrl();
                    String videoCoverUrl = firstItem.getVideoCoverUrl();
                    String showUrl = YHStringUtils.pickLastFirst(imgUrl, videoCoverUrl);

                    Glide.with(mContext)
                            .asBitmap()
                            .load(showUrl)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    int picWidth = resource.getWidth();
                                    int picHeight = resource.getHeight();

                                    int width = DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 76);
                                    int height = width * picHeight / picWidth;

                                    HomePagePopAd homePagePopAd = new HomePagePopAd(mContext, advertiseList, width, height, clickItem -> {
                                        HashMap<String, Object> params = new HashMap<>();
                                        params.put("userId", UserManager.getInstance().getUserId());
                                        params.put("id", clickItem.getId());

                                        basePresenter.addMarketingClick(params);

                                        PageJumpUtil.adJumpContent(clickItem, mActivity, -1);
                                    });
                                    new XPopup.Builder(mContext)
                                            .asCustom(homePagePopAd)
                                            .show();
                                }
                            });
                }
            }
        }
    }

    @Override
    public void updateStarContentSuccess() {
        MomentListModel model = mListData.get(currentPosition);
        int fabulousCount = model.getFabulousCount();
        model.setFabulous(!model.isFabulous());
        model.setFabulousCount(model.isFabulous() ? fabulousCount + 1 : (fabulousCount > 0 ? fabulousCount - 1 : 0));

        mAdapter.notifyItemChanged(currentPosition);
    }
}
