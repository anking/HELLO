package com.yanhua.home.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.launcher.ARouter;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.activity.InviteMemberListActivity;
import com.yanhua.home.adapter.InviteMemberListAdapter;
import com.yanhua.home.presenter.InviteDetailPresenter;
import com.yanhua.home.presenter.contract.InviteDetailContract;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 我的此刻列表数据
 */
public class InviteMemberListFragment extends BaseMvpFragment<InviteDetailPresenter> implements InviteDetailContract.IView {
    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;
    private String type;
    private InviteMemberListAdapter mAdapter;
    private List<InviteMemberModel> mListData;

    private String inviteId;
    private int userGender;


    @Override
    public int bindLayout() {
        return R.layout.fragment_invite_member_list;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new InviteDetailPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            type = bundle.getString("id");
            inviteId = bundle.getString("inviteId");
        }

        initRecyclerView();

    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1, userGender);
    }

    public void getListData(int page, int gender) {
        if (null != basePresenter) {
            current = page;

            userGender = gender;

            HashMap<String, Object> params = new HashMap<>();
            params.put("current", current);
            params.put("size", size);
            params.put("type", type);//(1:待加入, 2:已加入)
            params.put("userGender", userGender);//邀约用户性别(0:男女不限, 1:男, 2:女)
            params.put("inviteId", inviteId);

            basePresenter.meetPartnerInviteMembers(params);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(true);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                getListData(1, userGender);
            }
        });

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current, userGender);
            } else {
                rl.finishLoadMore(100, true, true);
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        mAdapter = new InviteMemberListAdapter(mContext, type, (type, item, pos) -> {
            currentPosition = pos;

            switch (type) {
                case InviteMemberListAdapter.OnHandleClickListener.DETAIL:
                    ARouter.getInstance().build(ARouterPath.PERSONAL_PAGE_ACTIVITY)
                            .withString("userId", item.getUserId())
                            .navigation();
                    break;
                case InviteMemberListAdapter.OnHandleClickListener.CHAT:
                    ToastUtils.showShort("聊一聊");

                    break;
                case InviteMemberListAdapter.OnHandleClickListener.REMOVE_MEMBER:
                    ToastUtils.showShort("移除成员");


                    break;
                case InviteMemberListAdapter.OnHandleClickListener.ADD_MEMBER:
//                    ToastUtils.showShort("同意加入");

                    CommonDialog commonDialog = new CommonDialog(mContext, "是否确认 " + item.getUserName() + " 的加入？", "确定", "取消");
                    new XPopup.Builder(mContext).asCustom(commonDialog).show();
                    commonDialog.setOnConfirmListener(() -> {
                        commonDialog.dismiss();

                        basePresenter.meetPartnerInviteAgreeSignIn(item.getId());
                    });

                    commonDialog.setOnCancelListener(() -> {
                        commonDialog.dismiss();
                    });
                    commonDialog.setOnCancelListener(() -> commonDialog.dismiss());

                    break;
            }
        });

        rvContent.setLayoutManager(manager);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        rvContent.setAdapter(mAdapter);
    }

    @Override
    public void handInviteAgreenSuccess() {
        ToastUtils.showShort("同意加入");

        getListData(1, userGender);

        ((InviteMemberListActivity) getActivity()).getListData(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void handInviteMemberList(ListResult<InviteMemberModel> data) {
        List<InviteMemberModel> list = data.getRecords();

        total = data.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }

        mAdapter.setItems(mListData);

        ((InviteMemberListActivity) getActivity()).setTabTitle(0, "待加入(" + mListData.size() + ")");
    }
}
