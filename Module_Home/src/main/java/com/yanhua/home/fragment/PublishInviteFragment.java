package com.yanhua.home.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.github.dfqin.grantor.PermissionListener;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.SdkVersionUtils;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.adapter.ImageAdapter;
import com.yanhua.common.model.FileResult;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.GlideEngine;
import com.yanhua.common.utils.ImageLoaderUtil;
import com.yanhua.common.utils.OssManager;
import com.yanhua.common.utils.PickImageUtil;
import com.yanhua.common.utils.UserManager;
import com.yanhua.common.widget.ITCheckBox;
import com.yanhua.core.util.ColorClickableSpan;
import com.yanhua.core.util.FastClickUtil;
import com.yanhua.core.util.FileUtils;
import com.yanhua.core.util.YHStringUtils;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.InviteTemplateAdapter;
import com.yanhua.home.presenter.InvitePresenter;
import com.yanhua.home.presenter.contract.InviteContract;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import top.zibin.luban.Luban;

public class PublishInviteFragment extends BaseMvpFragment<InvitePresenter> implements InviteContract.IView {

    @BindView(R2.id.tvCommit)
    TextView tvCommit;

    @BindView(R2.id.tv_pro)
    TextView tvPro;
    @BindView(R2.id.checkbox_pro)
    ITCheckBox checkboxPro;

    @BindView(R2.id.rvInvitePics)
    RecyclerView rvInvitePics;

    @BindView(R2.id.tvSuggest)
    TextView tvSuggest;

    @BindView(R2.id.rvModule)
    RecyclerView rvModule;
    @BindView(R2.id.etInviteTitle)
    EditText etInviteTitle;
    @BindView(R2.id.etInviteDesc)
    EditText etInviteDesc;

    @BindView(R2.id.ivUpload)
    ImageView ivUpload;

    @BindView(R2.id.upload)
    FrameLayout upload;

    ArrayList<String> imagesList = new ArrayList<>();
    ArrayList<String> coverList = new ArrayList<>();
    ImageAdapter imageAdapter;

    List<LocalMedia> coversPaths;

    private OssManager ossManager;
    private StsTokenModel stsTokenModel;
    private static final int CHOOSE_IMAGES = 0x001;
    private static final int CHOOSE_TEMPLATE = 0x002;

    private ArrayList<String> imagesUrl;
    private int updateIndex;
    private List<File> fileList;

    @Override
    protected void creatPresent() {
        basePresenter = new InvitePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_publish_invite;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvCommit, checkboxPro, upload);
        setPro();
    }

    private void setPro() {
        String textContent = "我同意遵守《音圈发布规范》";
        SpannableString spannableString = new SpannableString(textContent);

        spannableString.setSpan(new ColorClickableSpan(getResources().getColor(R.color.theme)) {
            @Override
            public void onClick(@NonNull View widget) {
//                avoidHintColor(tvPro);
                if (FastClickUtil.isFastClick(500)) {
                    if (null == publishAgreement) {
                        basePresenter.getAppProtocol(YXConfig.protocol.publishAgreement);
                    } else {
                        toProtocolPage(publishAgreement);
                    }
                }
            }
        }, 5, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new AbsoluteSizeSpan(12, true), 0, textContent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPro.setText(spannableString);
        tvPro.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private ProtocolModel publishAgreement;

    @Override
    public void handleAppProtocol(String code, ProtocolModel data) {
        if (null != data) {
            if (YXConfig.protocol.publishAgreement.equals(code)) {
                publishAgreement = data;
            }

            toProtocolPage(data);
        }
    }

    private void toProtocolPage(ProtocolModel data) {
        ARouter.getInstance().build(ARouterPath.WEB_DETAIL_ACTIVITY)
                .withString("title", data.getTitle())
                .withString("content", data.getContent())
                .navigation();
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tvCommit) {//一键登录
            if (FastClickUtil.isFastClick(1000)) {
                boolean isSelect = checkboxPro.isChecked();
                if (isSelect) {
                    String title = etInviteTitle.getText().toString().trim();//标题
                    String desc = etInviteDesc.getText().toString().trim();//简介

                    if (TextUtils.isEmpty(title)) {
                        ToastUtils.showShort("请输入标题");
                        etInviteTitle.setHint("请输入标题");
                        return;
                    }
                    if (TextUtils.isEmpty(desc)) {
                        ToastUtils.showShort("请输入简介内容");
                        etInviteDesc.setHint("请输入简介内容");
                        return;
                    }

                    if (imagesList.size() == 0 || (imagesList.size() == 1 && imagesList.get(0) == null)) {
                        ToastUtils.showShort("请选取组局图片");
                        return;
                    }

                    if (coverList.size() == 0) {
                        ToastUtils.showShort("请选取组局封面");
                        return;
                    }
                    fileList = new ArrayList<>();
                    imagesUrl = new ArrayList<>();
                    updateIndex = 0;
                    inviteAddForm.setTitle(title);
                    inviteAddForm.setIntroduction(desc);
                    inviteAddForm.setUserId(UserManager.getInstance().getUserId());

                    uploadImage();
                } else {
                    ToastUtils.showShort("请先阅读并同意相关协议");
                }
            }
        } else if (id == R.id.checkbox_pro) {
            boolean checked = checkboxPro.isChecked();
            checkboxPro.setChecked(!checked);
        } else if (id == R.id.upload) {
            checkStoragePermission(2);//封面
        }

    }

    private String getPath() {
        String path = Environment.getExternalStorageDirectory() + "/Luban/image/";
        File file = new File(path);
        file.mkdirs();
        return path;
    }

    ArrayList<String> uploadFileList = null;

    @SuppressLint("AutoDispose")
    private void uploadImage() {
        uploadFileList = new ArrayList<>();
        for (String path : imagesList) {
            if (null == path || path.startsWith("http://") || path.startsWith("https://")) {

                if (null != path) {
                    imagesUrl.add(path);
                }
            } else {
                uploadFileList.add(path);
            }
        }

        if (uploadFileList.size() == 0) {
            inviteAddForm.setImagesUrl(imagesUrl);
            if (null != inviteAddForm.getId()) {
                basePresenter.meetPartnerInviteInfoUpdate(inviteAddForm);
            } else {
                basePresenter.meetPartnerInviteAdd(inviteAddForm);
            }
            return;
        }

        basePresenter.addSubscribe(Flowable.just(uploadFileList)
                        .observeOn(Schedulers.io())
                        .map(list -> {
                            List<File> fileList = new ArrayList<>();
                            for (String filePath : list) {
                                try {
                                    File file = new File(filePath);
                                    fileList.add(file);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            return fileList;
                        })
                        .map(new Function<List<File>, List<File>>() {
                            @Override
                            public List<File> apply(@NonNull List<File> list) throws Exception {
                                return Luban.with(mContext)
                                        .setTargetDir(getPath())
                                        .ignoreBy(500)
                                        .load(list)
                                        .get();
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                            }
                        })
//                    .onErrorResumeNext(Flowable.<List<File>>empty())
                        .subscribe(new Consumer<List<File>>() {
                            @Override
                            public void accept(@NonNull List<File> list) {
                                fileList.clear();
                                fileList.addAll(list);

                                uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                            }
                        })
        );
    }


    private void uploadFile(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }
        // 判断是图片还是视频
        int index = fileName.lastIndexOf(".");
//        String mediaType = fileName.substring(index + 1);
        boolean isVideo = FileUtils.isVideo(fileName);

        String objectKey = "";
        Date date = new Date();
        try {
            if (isVideo) {
                objectKey = "video/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            } else {
                objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
                String httpUrl = fileResult.getHttpUrl();
                imagesUrl.add(httpUrl);
                updateIndex++;

                if (updateIndex < uploadFileList.size()) {
                    uploadFile(fileList.get(updateIndex).getName(), fileList.get(updateIndex).getPath());
                } else {
                    inviteAddForm.setImagesUrl(imagesUrl);
                    String coverPath = coverList.get(0);

                    if (!TextUtils.isEmpty(coverPath)) {
                        String urlPath = coverPath.toLowerCase();
                        if (urlPath.startsWith("http://") || urlPath.startsWith("https://")) {
                            inviteAddForm.setCoverUrl(urlPath);

                            if (null != inviteAddForm.getId()) {
                                basePresenter.meetPartnerInviteInfoUpdate(inviteAddForm);
                            } else {
                                basePresenter.meetPartnerInviteAdd(inviteAddForm);
                            }
                        } else {
                            //需要上传
                            File file = new File(coverPath);
                            uploadCover(file.getName(), file.getPath());
                        }
                    }
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {

            }
        });
    }

    private void uploadCover(String fileName, String filePath) {
        if (stsTokenModel == null) {
            stsTokenModel = GsonUtils.fromJson(DiscoCacheUtils.getInstance().getStsToken(), StsTokenModel.class);
        }

        int index = fileName.lastIndexOf(".");

        String objectKey = "";
        Date date = new Date();
        objectKey = "image/" + new SimpleDateFormat("yyyy/MM/").format(date) + System.currentTimeMillis() + fileName.substring(index);

        OssManager.Builder builder = new OssManager.Builder(mContext);
        builder.accessKeyId(stsTokenModel.getAccessKeyId())
                .accessKeySecret(stsTokenModel.getAccessKeySecret())
                .bucketName(stsTokenModel.getBucket())
                .endPoint(stsTokenModel.getEndPoint())
                .callbackUrl(YXConfig.getBaseUrl() + "/opc/oss/policy/callback")
                .objectKey(objectKey)
                .localFilePath(filePath);
        ossManager = builder.build();
        ossManager.push(stsTokenModel.getAccessKeyId(), stsTokenModel.getAccessKeySecret(), stsTokenModel.getStsToken());

        ossManager.setPushProgressListener((request, currentSize, totalSize) -> {

        });
        ossManager.setPushStateListener(new OssManager.OnPushStateListener() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                String resultJson = result.getServerCallbackReturnBody();
                Type type = new TypeToken<HttpResult<FileResult>>() {
                }.getType();
                HttpResult<FileResult> httpResult = GsonUtils.fromJson(resultJson, type);
                FileResult fileResult = httpResult.getData();
//                String resultId = fileResult.getId();
                String httpUrl = fileResult.getHttpUrl();
//                String width = fileResult.getWidth();
//                String height = fileResult.getHeight();
                coverList.clear();
//                coverList.add(httpUrl);

                inviteAddForm.setCoverUrl(httpUrl);
                inviteAddForm.setMeetPartnerTypeTemplateId(null);

                if (null != inviteAddForm.getId()) {
                    basePresenter.meetPartnerInviteInfoUpdate(inviteAddForm);
                } else {
                    basePresenter.meetPartnerInviteAdd(inviteAddForm);
                }
            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientException, ServiceException serviceException) {

            }
        });
    }

    private InviteAddForm inviteAddForm;
    private List<InviteTemplateModel> inviteTempList;

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //取出对象
        inviteAddForm = (InviteAddForm) bundle.getSerializable("addInviteForm");

        etInviteTitle.setText(YHStringUtils.value(inviteAddForm.getTitle()));
        etInviteDesc.setText(YHStringUtils.value(inviteAddForm.getIntroduction()));

        String coverUrl = inviteAddForm.getCoverUrl();

        if (!TextUtils.isEmpty(coverUrl)) {
            ImageLoaderUtil.loadImgCenterCrop(ivUpload, coverUrl);

            coverList.add(coverUrl);
        }

        ArrayList<String> imagesUrlTemp = inviteAddForm.getImagesUrl();

        //封面--先null
        inviteAddForm.setImagesUrl(imagesUrlTemp);
        inviteAddForm.setCoverUrl(coverUrl);
        inviteAddForm.setMeetPartnerTypeTemplateId(null);

        //---------------------------------------------------------------------
        if (null != imagesUrlTemp && imagesUrlTemp.size() > 0) {
            imagesList.addAll(imagesUrlTemp);
        }
        if (imagesList.size() < 8) {
            imagesList.add(null);
        }

        imageAdapter = new ImageAdapter(mContext, 4, 56, pos -> {
            imagesList.remove(pos);
            imageAdapter.setItems(imagesList);
        });
        imageAdapter.setOnItemClickListener((itemView, index) -> {
            checkStoragePermission(1);
        });
        imageAdapter.setItems(imagesList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 4);
        rvInvitePics.setLayoutManager(gridLayoutManager);
        rvInvitePics.setAdapter(imageAdapter);
        rvInvitePics.setOverScrollMode(View.OVER_SCROLL_NEVER);

        basePresenter.getStsToken();

        //模板数据
        inviteTempList = new ArrayList<>();
        HashMap tempParams = new HashMap();
        tempParams.put("current", 1);
        tempParams.put("size", 500);//不分页
        tempParams.put("meetPartnerTypeId", inviteAddForm.getMeetPartnerTypeId());
//        tempParams.put("username", "string");
        basePresenter.meetPartnerTemplateList(tempParams);
    }

    @Override
    public void handInviteTemplateList(List<InviteTemplateModel> data) {
        if (null == data || data.size() == 0) {
            inviteTempList = new ArrayList<>();

            tvSuggest.setVisibility(View.INVISIBLE);
        } else {
            tvSuggest.setVisibility(View.VISIBLE);
            inviteTempList.clear();
            inviteTempList.addAll(data);
        }

        LinearLayoutManager manager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        InviteTemplateAdapter mAdapter = new InviteTemplateAdapter(mContext);
        rvModule.setLayoutManager(manager);
        rvModule.setAdapter(mAdapter);
        mAdapter.setItems(inviteTempList);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            InviteTemplateModel temp = inviteTempList.get(pos);

            ImageLoaderUtil.loadImgCenterCrop(ivUpload, temp.getUrl());
            mAdapter.setSelectId(temp.getMeetPartnerTypeId());

            inviteAddForm.setMeetPartnerTypeTemplateId(temp.getMeetPartnerTypeId());

            coverList.clear();
            coverList.add(temp.getUrl());
        });
    }

    @Override
    public void handleStsToken(StsTokenModel model) {
        stsTokenModel = model;

        String json = GsonUtils.toJson(model);
        DiscoCacheUtils.getInstance().setStsToken(json);
    }

    private void removeLastNull(ArrayList<String> pathList) {
        if (null != pathList && pathList.size() > 0) {
            //删除最后一个为null的，避免出现多个加号
            int lastIndex = pathList.size() - 1;
            String last = pathList.get(lastIndex);

            if (TextUtils.isEmpty(last)) {
                pathList.remove(lastIndex);
            }
        }
    }


    private void chooseImage() {
        removeLastNull(imagesList);
        int leftSize = 8 - imagesList.size();

        PictureSelector.create(PublishInviteFragment.this)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(true)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(leftSize)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
//                .filterMinFileSize(200)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.MULTIPLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(1, 1)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
//                .selectionData(imagesPaths)// 是否传入已选图片
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(true)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .isGif(false)// 是否显示gif图片
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(CHOOSE_IMAGES);
    }

    private void chooseImageTemplate() {
        PictureSelector.create(PublishInviteFragment.this)
                .openGallery(PictureMimeType.ofAll())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWeChatStyle(false)
                .theme(R.style.picture_white_style)
                .setPictureStyle(PickImageUtil.getWhiteStyle(mActivity))
//                .setPictureUIStyle(PictureSelectorUIStyle.ofSelectTotalStyle()).isUseCustomCamera(false)// 是否使用自定义相机
                .isPageStrategy(true)// 是否开启分页策略 & 每页多少条；默认开启
                .isWithVideoImage(false)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)// 选择数到了最大阀值列表是否启用蒙层效果
                .setCaptureLoadingColor(ContextCompat.getColor(mContext, R.color.blue0))
                .maxSelectNum(1)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .maxVideoSelectNum(1) // 视频最大选择数量
                .imageSpanCount(4)// 每行显示个数
                .filterMinFileSize(200)
                .isGif(false)// 是否显示gif图片
                .isBmp(false)
                .isWebp(false)
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .closeAndroidQChangeWH(true)//如果图片有旋转角度则对换宽高,默认为true
                .closeAndroidQChangeVideoWH(!SdkVersionUtils.checkedAndroid_Q())// 如果视频有旋转角度则对换宽高,默认为false
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && .isEnableCrop(false);有效,默认处理
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果设置为true则用户可以自由选择是否使用原图，压缩、裁剪功能将会失效
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选
                .isSingleDirectReturn(true)
                .isPreviewImage(true)// 是否可预览图片
                .isPreviewVideo(false)// 是否可预览视频
                .withAspectRatio(1, 1)
                .isEnablePreviewAudio(false) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .showCropGrid(false)
                .rotateEnabled(false)
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .setCameraImageFormat(PictureMimeType.JPEG) // 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .isEnableCrop(true)// 是否裁剪
                .setCropDimmedColor(R.color.picture_crop_frame)
                .isCompress(true)// 是否压缩
                .synOrAsy(false)//同步true或异步false 压缩 默认同步
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .forResult(CHOOSE_TEMPLATE);
    }

    private void checkStoragePermission(int type) {
        requestCheckPermission(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, new PermissionListener() {
            @Override
            public void permissionGranted(@NonNull String[] permission) {
                if (type == 1) {
                    chooseImage();
                } else {
                    chooseImageTemplate();
                }
            }

            @Override
            public void permissionDenied(@NonNull String[] permission) {
                ToastUtils.showShort(R.string.denied_storage_permission);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_IMAGES && resultCode == Activity.RESULT_OK) {
            removeLastNull(imagesList);

            List<LocalMedia> imagesPaths = PictureSelector.obtainMultipleResult(data);

            for (LocalMedia localMedia : imagesPaths) {
                //裁剪路径
                String cutPath = localMedia.getCutPath();
                String realPath = localMedia.getRealPath();

                String path;
                //如果裁剪路径是空的，那么使用真是路径
                if (TextUtils.isEmpty(cutPath)) {
                    path = realPath;
                } else {
                    path = cutPath;
                }

                imagesList.add(path);
            }

            imageAdapter.setItems(imagesList);
        } else if (requestCode == CHOOSE_TEMPLATE && resultCode == Activity.RESULT_OK) {
            coverList.clear();
            coversPaths = PictureSelector.obtainMultipleResult(data);

            for (LocalMedia localMedia : coversPaths) {
                //裁剪路径
                String cutPath = localMedia.getCutPath();
                String realPath = localMedia.getRealPath();

                String path;
                //如果裁剪路径是空的，那么使用真是路径
                if (TextUtils.isEmpty(cutPath)) {
                    path = realPath;
                } else {
                    path = cutPath;
                }

                coverList.add(path);
            }

            ImageLoaderUtil.loadImgFillCenter(mContext, ivUpload, coverList.get(0), 0);
        }
    }

    @Override
    public void handAddInviteSuccess() {
        ToastUtils.showShort("提交成功");
        getActivity().finish();
    }
}
