package com.yanhua.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.launcher.ARouter;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectChangeListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.TypeWayModel;
import com.yanhua.common.router.ARouterPath;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.common.utils.DiscoPageUtils;
import com.yanhua.common.utils.DiscoTimeUtils;
import com.yanhua.common.widget.TypeWayBottomPopup;
import com.yanhua.core.util.DateUtils;
import com.yanhua.core.util.SystemUtil;
import com.yanhua.core.view.CommonDialog;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.presenter.InvitePresenter;
import com.yanhua.home.presenter.contract.InviteContract;
import com.yanhua.home.view.InviteTypeBottomPop;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PublishInviteFirstFragment extends BaseMvpFragment<InvitePresenter> implements InviteContract.IView {

    @BindView(R2.id.tvNextStep)
    TextView tvNextStep;

    @BindView(R2.id.rlAddress)
    RelativeLayout rlAddress;
    @BindView(R2.id.tvAddress)
    TextView tvAddress;

    @BindView(R2.id.rlSelectInviteType)
    RelativeLayout rlSelectInviteType;
    @BindView(R2.id.tvSelectInviteType)
    TextView tvSelectInviteType;

    @BindView(R2.id.rlSelectInviteTime)
    RelativeLayout rlSelectInviteTime;
    @BindView(R2.id.tvSelectInviteTime)
    TextView tvSelectInviteTime;

    @BindView(R2.id.rlSelectInviteNum)
    RelativeLayout rlSelectInviteNum;
    @BindView(R2.id.tvSelectInviteNum)
    TextView tvSelectInviteNum;

    @BindView(R2.id.rlSelectInvitePeople)
    RelativeLayout rlSelectInvitePeople;
    @BindView(R2.id.tvSelectInvitePeople)
    TextView tvSelectInvitePeople;

    @BindView(R2.id.rlSelectInvitePayWay)
    RelativeLayout rlSelectInvitePayWay;
    @BindView(R2.id.tvSelectInvitePayWay)
    TextView tvSelectInvitePayWay;
    private TimePickerView pvTime;

    private InviteAddForm inviteAddForm;

    @Override
    protected void creatPresent() {
        basePresenter = new InvitePresenter();
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_publish_invite_first;
    }

    @Override
    public void initView(@Nullable Bundle savedInstanceState, @Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        applyDebouncingClickListener(true, tvNextStep);

        initTimePicker();

    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);
        //取出对象
        inviteAddForm = (InviteAddForm) bundle.getSerializable("addInviteForm");
        if (null == inviteAddForm) {
            inviteAddForm = new InviteAddForm();
        } else {
            tvAddress.setText(inviteAddForm.getDestination());
            int peopleNum = inviteAddForm.getActivityNumber();
            tvSelectInviteNum.setText(peopleNum + "人局");
            tvSelectInviteType.setText(inviteAddForm.getMeetPartnerTypeName());
            String selectTime = null;
            try {
                selectTime = DiscoTimeUtils.getTime(DateUtils.stringToDate(inviteAddForm.getActivityTime(), DateUtils.yyyyMMddHHmmss));
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvSelectInviteTime.setText(selectTime);
            tvSelectInvitePayWay.setText(inviteAddForm.getPaymentTypeName());
            tvSelectInvitePeople.setText(inviteAddForm.getPartnerTypeName());
        }
    }

    @Override
    public void onDebouncingClick(View v) {
        int id = v.getId();

        if (id == R.id.tvNextStep) {//一键登录
            String address = tvAddress.getText().toString().trim();
            String num = tvSelectInviteNum.getText().toString().trim();

            if (TextUtils.isEmpty(address)) {
                ToastUtils.showShort("您还未选择目的地");
                tvAddress.setHint("您还未选择目的地");
                return;
            }

            if (TextUtils.isEmpty(inviteAddForm.getMeetPartnerTypeId())) {
                ToastUtils.showShort("您还未选择约伴类型");
                tvSelectInviteType.setHint("您还未选择约伴类型");
                return;
            }

            if (TextUtils.isEmpty(inviteAddForm.getActivityTime())) {
                ToastUtils.showShort("您还未选择活动时间");
                tvSelectInviteTime.setHint("您还未选择活动时间");
                return;
            }

            if (TextUtils.isEmpty(num)) {
                ToastUtils.showShort("您还未选择活动人数");
                tvSelectInviteNum.setHint("您还未选择活动人数");
                return;
            }

            if (TextUtils.isEmpty(inviteAddForm.getPartnerTypeId())) {
                ToastUtils.showShort("您还未选择邀约的伙伴类别");
                tvSelectInvitePeople.setHint("您还未选择邀约的伙伴类别");
                return;
            }
            if (TextUtils.isEmpty(inviteAddForm.getPaymentTypeId())) {
                ToastUtils.showShort("您还未选择买单方式");
                tvSelectInvitePayWay.setHint("您还未选择买单方式");
                return;
            }

            PublishInviteFragment fragment = new PublishInviteFragment();

            Bundle data = new Bundle();
            data.putSerializable("addInviteForm", inviteAddForm);
            fragment.setArguments(data);

            FragmentManager manager = this.getActivity().getSupportFragmentManager();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /**
     * 打开GPS对话框
     */
    private void showLoacationDialog() {
        CommonDialog commonDialog = new CommonDialog(mContext, "为了提高定位的准确度，更好的为您服务，请打开定位服务", "请打开定位服务", "设置", "取消");
        new XPopup.Builder(mContext).asCustom(commonDialog).show();
        commonDialog.setOnConfirmListener(() -> {
            commonDialog.dismiss();
            //跳转到手机打开GPS页面
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            //设置完成后返回原来的界面
            startActivityForResult(intent, 1001);
        });
        commonDialog.setOnCancelListener(() -> {
            commonDialog.dismiss();
        });
    }
    
    @OnClick({R2.id.rlAddress, R2.id.rlSelectInviteType, R2.id.rlSelectInviteTime, R2.id.rlSelectInviteNum, R2.id.rlSelectInvitePeople, R2.id.rlSelectInvitePayWay})
    public void onClickView(View view) {
        int id = view.getId();
        if (id == R.id.rlAddress) {
            if (SystemUtil.isLocationEnabled(mContext)) {
                Postcard postcard = ARouter.getInstance().build(ARouterPath.AMAP_LOCATION_SELECT_ACTIVITY);
                DiscoPageUtils.fragmentNavigation(this, postcard, 1002);
            } else {
                showLoacationDialog();
            }

        } else if (id == R.id.rlSelectInviteType) {
            selectInviteType();
        } else if (id == R.id.rlSelectInviteTime) {
            if (null != pvTime) {
                pvTime.setDate(Calendar.getInstance());
                pvTime.show(); //show timePicker*/
//                pvTime.show(v);//弹出时间选择器，传递参数过去，回调的时候则可以绑定此view
            }
        } else if (id == R.id.rlSelectInviteNum) {
            Postcard postcard = ARouter.getInstance().build(ARouterPath.PUBLISH_INVITE_NUM_ACTIVITY);
            DiscoPageUtils.fragmentNavigation(this, postcard, 1001);
        } else if (id == R.id.rlSelectInvitePeople) {
            List<TypeWayModel> list = DiscoCacheUtils.getInstance().getInviteCreateType().getPartnerType();
            if (null == list || list.size() == 0) {
                list = new ArrayList<>();
                TypeWayModel peop = new TypeWayModel("1", "男女不限");
                TypeWayModel man = new TypeWayModel("2", "仅限男生");
                TypeWayModel woman = new TypeWayModel("3", "仅限女生");
                list.add(peop);
                list.add(man);
                list.add(woman);
            }
            TypeWayBottomPopup popup = new TypeWayBottomPopup(mContext, "伙伴类型", list);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();
                String itemName = bean.getName();
                String itemId = bean.getId();

                inviteAddForm.setPartnerTypeName(itemName);
                inviteAddForm.setPartnerTypeId(itemId);
                tvSelectInvitePeople.setText(itemName);
            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        } else if (id == R.id.rlSelectInvitePayWay) {
            List<TypeWayModel> list = DiscoCacheUtils.getInstance().getInviteCreateType().getPaymentType();
            if (null == list || list.size() == 0) {
                list = new ArrayList<>();
                TypeWayModel aa = new TypeWayModel("1", "AA支付");
                TypeWayModel tuhao = new TypeWayModel("2", "地主买单");
                TypeWayModel meinv = new TypeWayModel("3", "美女免单");
                TypeWayModel shuaige = new TypeWayModel("4", "帅哥免单");
                list.add(aa);
                list.add(tuhao);
                list.add(meinv);
                list.add(shuaige);
            }

            TypeWayBottomPopup popup = new TypeWayBottomPopup(mContext, "买单方式", list);
            popup.setOnItemClickListener(bean -> {
                popup.dismiss();
                String itemName = bean.getName();
                String itemId = bean.getId();
//
                inviteAddForm.setPaymentTypeId(itemId);
                inviteAddForm.setPaymentTypeName(itemName);
                tvSelectInvitePayWay.setText(itemName);
            });
            new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
        }
    }

    private void initTimePicker() {//Dialog 模式下，在底部弹出
        pvTime = new TimePickerBuilder(mContext, (date, v) -> {
            Date now = Calendar.getInstance().getTime();
            long minutes = DiscoTimeUtils.getDatePoor(date, now);
            if (minutes <= YXConfig.PUBLISH_MIN_TIME) {
                ToastUtils.showShort("选取时间需要在" + YXConfig.PUBLISH_MIN_TIME + "分钟后");
                return;
            }

            String selectTime = DiscoTimeUtils.getTime(date);
            inviteAddForm.setActivityTime(DiscoTimeUtils.getRequestTime(date));//后台所需参数格式
            tvSelectInviteTime.setText(selectTime);
        })
                .setTimeSelectChangeListener(new OnTimeSelectChangeListener() {
                    @Override
                    public void onTimeSelectChanged(Date date) {
                    }
                })
                .setType(new boolean[]{true, true, true, true, true, false})
                .isDialog(false) //默认设置false ，内部实现将DecorView 作为它的父控件。
                .addOnCancelClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setItemVisibleCount(7) //若设置偶数，实际值会加1（比如设置6，则最大可见条目为7）
                .setLineSpacingMultiplier(2.0f)
                .setTitleText("活动时间")
                .isAlphaGradient(true)
                .build();
    }

    //选择约伴类型
    private void selectInviteType() {
        InviteTypeBottomPop popup = new InviteTypeBottomPop(mContext, item -> {
            tvSelectInviteType.setText(item.getName());

            inviteAddForm.setMeetPartnerTypeId(item.getId());
        });
        new XPopup.Builder(mContext).enableDrag(false).asCustom(popup).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        //人数
        if (requestCode == 1001 && resultCode == 100) {
            int peopleNum = data.getIntExtra("PEOPLE_NUM", 0);
            tvSelectInviteNum.setText(peopleNum + "人局");
            inviteAddForm.setActivityNumber(peopleNum);

        }

        //活动地点
        if (requestCode == 1002) {
            String mLatResult = data.getStringExtra("lat");
            String mLngResult = data.getStringExtra("lng");
            String mPoiResult = data.getStringExtra("poi");
            String city = data.getStringExtra("city");
            String address = data.getStringExtra("address");

            //經緯度
            inviteAddForm.setDestinationLatitude(mLatResult);
            inviteAddForm.setDestinationLongitude(mLngResult);
            inviteAddForm.setDestination(mPoiResult);
            inviteAddForm.setCity(city);
            inviteAddForm.setAddress(address);

            tvAddress.setText(mPoiResult);
        }
    }
}
