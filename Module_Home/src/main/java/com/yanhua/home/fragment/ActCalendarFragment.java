package com.yanhua.home.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.core.view.NormalDecoration;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.ActNewsAdapter;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;

/**
 * 活动列表数据
 */
public class ActCalendarFragment extends BaseMvpFragment<NewsPresenter> implements NewsContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private int type;
    private String city;
    private boolean showHistory;

    private ActNewsAdapter mAdapter;
    private List<ActivityModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_act_calendar;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            city = bundle.getString("city");
            type = bundle.getInt("type");
            showHistory = bundle.getBoolean("showHistory");
        }

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    /**
     * 获取列表数据
     *
     * @param page
     */
    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.getRecentActiveList(YXConfig.TYPE_BREAK_NEWS, showHistory ? 0 : 1, current, size, city.equals("全部") ? "" : city, YXConfig.ALL666);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        LinearLayoutManager managerContent = new LinearLayoutManager(mContext);

        mAdapter = new ActNewsAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);

//        final ActCalendarDecoration decoration = new ActCalendarDecoration() {
//            @Override
//            public String getHeaderName(int pos) {
//                return mListData.get(pos).getMonth();
//            }
//        };
        final NormalDecoration decoration = new NormalDecoration() {
            @Override
            public String getHeaderName(int pos) {
                return mListData.get(pos).getMonth();
            }
        };

        rvContent.addItemDecoration(decoration);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            ActivityModel clickItem = mAdapter.getItemObject(pos);
            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_BREAK_NEWS);
        });
    }


    @Override
    public void handleActivityListData(ListResult<ActivityModel> listResult) {
        List<ActivityModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            for (ActivityModel itemModel : list) {
                String date = showHistory ? itemModel.getActivityEndTime() : itemModel.getActivityStartTime();

                if (!TextUtils.isEmpty(date)) {
                    int subMonth = Integer.parseInt(date.substring(5, 7));
                    int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
                    String showMonthStr = String.format("%d月", subMonth);
                    if (subMonth == month) {
                        showMonthStr = "本月";
                    }
                    itemModel.setMonth(showMonthStr);
                }
            }

            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
