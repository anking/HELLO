package com.yanhua.home.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BaseMvpFragment;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.utils.PageJumpUtil;
import com.yanhua.common.utils.RecycleViewUtils;
import com.yanhua.common.widget.DataObserverRecyclerView;
import com.yanhua.home.R;
import com.yanhua.home.R2;
import com.yanhua.home.adapter.BreakNewsAdapter;
import com.yanhua.home.presenter.NewsPresenter;
import com.yanhua.home.presenter.contract.NewsContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 爆料列表数据
 */
public class BreakNewsFragment extends BaseMvpFragment<NewsPresenter> implements NewsContract.IView {

    @BindView(R2.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R2.id.rv_content)
    DataObserverRecyclerView rvContent;

    private int current = 1;
    private int size = 20;
    private int currentPosition;
    private int total;

    private String id;
    private String name;


    private BreakNewsAdapter mAdapter;
    private List<BreakNewsListModel> mListData;

    @Override
    public int bindLayout() {
        return R.layout.fragment_break_news;
    }

    @Override
    protected void creatPresent() {
        basePresenter = new NewsPresenter();
    }

    @Override
    public void initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState, @Nullable @org.jetbrains.annotations.Nullable View contentView) {
        super.initView(savedInstanceState, contentView);

        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getString("id");
            name = bundle.getString("name");
        }

        initRecyclerView();
    }

    @Override
    public void initData(@Nullable @org.jetbrains.annotations.Nullable Bundle bundle) {
        super.initData(bundle);

        //列表数据
        mListData = new ArrayList<>();

        getListData(1);
    }

    private String reqCityName = "";

    /**
     * 获取列表数据
     *
     * @param page
     */
    public void getListData(int page) {
        if (null != basePresenter) {
            current = page;

            basePresenter.cityContentList(current, size, reqCityName, "", id, YXConfig.TYPE_BREAK_NEWS);
        }
    }

    /**
     * 获取列表数据
     *
     * @param page
     */
    public void getListData(int page, String cityName) {
        if (null != basePresenter) {
            current = page;

            if ("全部".equals(cityName)) {
                reqCityName = "";
            } else {
                reqCityName = cityName;
            }
            //
            basePresenter.cityContentList(current, size, reqCityName, "", id, YXConfig.TYPE_BREAK_NEWS);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initRecyclerView() {
        refreshLayout.setEnableRefresh(false);
        refreshLayout.setOnRefreshListener(refreshLayout -> {
            current = 1;
            getListData(current);
        });
        refreshLayout.setEnableLoadMore(true);

        refreshLayout.setOnLoadMoreListener(rl -> {
            if (mListData.size() < total) {
                current++;
                getListData(current);
            } else {
                rl.finishLoadMore(1000, true, true);
            }
        });

        LinearLayoutManager managerContent = new LinearLayoutManager(mContext);

        mAdapter = new BreakNewsAdapter(mContext);
        rvContent.setLayoutManager(managerContent);
        rvContent.setAdapter(mAdapter);

        //干掉 取出上拉下拉阴影
        rvContent.setOverScrollMode(View.OVER_SCROLL_NEVER);

        RecycleViewUtils.clearRecycleAnimation(rvContent);

        mAdapter.setOnItemClickListener((itemView, pos) -> {
            BreakNewsListModel clickItem = mAdapter.getItemObject(pos);

            PageJumpUtil.jumpNewsDetailPage(clickItem.getId(), YXConfig.TYPE_BREAK_NEWS);
        });
    }

    @Override
    public void handleBreakNewList(ListResult<BreakNewsListModel> listResult) {
        List<BreakNewsListModel> list = listResult.getRecords();
        total = listResult.getTotal();

        if (current == 1) {
            refreshLayout.finishRefresh();
            refreshLayout.resetNoMoreData();
            mListData.clear();
        } else {
            refreshLayout.finishLoadMore();
        }

        if (list != null && !list.isEmpty()) {
            mListData.addAll(list);
        }
        mAdapter.setItems(mListData);
    }
}
