package com.yanhua.home.presenter;

import android.text.TextUtils;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.config.YXConfig;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.UserModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.home.presenter.contract.HomeContract;

import java.util.HashMap;
import java.util.List;

public class HomePresenter extends MvpPresenter<HomeContract.IView> implements HomeContract.Presenter {

    @Override
    public void homeSearch(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().homeSearch(new SimpleSubscriber<HttpResult<ListResult<HomeSearchResultModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<HomeSearchResultModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleHomeSearchSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void userSearch(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().userSearch(new SimpleSubscriber<HttpResult<ListResult<UserModel>>>(baseView, true) {
            @Override
            public void onNext(HttpResult<ListResult<UserModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleUserSearchSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getHotTopicList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getHotTopicList(new SimpleSubscriber<HttpResult<ListResult<TopicModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<TopicModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<TopicModel> listResult = result.getData();
                    baseView.handleHotTopicList(listResult);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getHotCircleList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getHotCircleList(new SimpleSubscriber<HttpResult<ListResult<CircleModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CircleModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<CircleModel> listResult = result.getData();
                    baseView.handleHotCircleList(listResult);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    public void homePageBreakNewsList(int current, int size) {
        addSubscribe(HomeHttpMethod.getInstance().homePageBreakNewsList(new SimpleSubscriber<HttpResult<ListResult<BreakNewsRecommendModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BreakNewsRecommendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handHomePageBreakNewsList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size));
    }

    public void getRecommendTopicList() {
        addSubscribe(MomentHttpMethod.getInstance().getRecommendTopicList(new SimpleSubscriber<HttpResult<List<TopicModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<TopicModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        List<TopicModel> listResult = result.getData();
                        baseView.handleRecommendTopicList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }));
    }


    public void getBestNoteList(String city, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getBestNoteList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, city, current, size));
    }

    public void uploadUserLocation(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().uploadUserLocation(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void queryAdList(String code) {
//        areaName	广告投放区域名称
//        code	广告位置编码
//        subModule	适用模块, 此刻位置使用(1:关注,2:推荐,3:广场,4:同城)
//        systemType	广告投放系统类型(1:Android,2:iOS)
        HashMap<String, Object> params = new HashMap<>();

//        "subModule":0,
        String locationAddress = DiscoCacheUtils.getInstance().getCurrentCity();
        String cityName = TextUtils.isEmpty(locationAddress) ? YXConfig.city : locationAddress;

        params.put("areaName", cityName);
        params.put("code", code);
        params.put("systemType", 1);

        addSubscribe(CommonHttpMethod.getInstance().queryAdList(new SimpleSubscriber<HttpResult<List<AdvertiseModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<AdvertiseModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleAdvertiseList(result.getData(), code);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess(false);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess(true);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void cancelFollowContentUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followContentUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void cancelFollowZHUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowZHUserSuccess(false);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followZHUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowZHUserSuccess(true);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, userId));
    }
}
