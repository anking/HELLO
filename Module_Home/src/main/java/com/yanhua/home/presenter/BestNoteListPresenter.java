package com.yanhua.home.presenter;

import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.ValueContentModel;
import com.yanhua.home.presenter.contract.BestNoteListContract;

public class BestNoteListPresenter extends MvpPresenter<BestNoteListContract.IView> implements BestNoteListContract.Presenter {

    public void getBestNoteList(String city, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getBestNoteList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<MomentListModel> listResult = result.getData();
                        baseView.handleMomentHomeList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, city,  current, size));
    }


    public void getBestNoteCityList() {
        addSubscribe(MomentHttpMethod.getInstance().getBestNoteCityList(new SimpleSubscriber<HttpResult<ValueContentModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ValueContentModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handBestNoteCityList(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }


    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }
}
