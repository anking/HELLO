package com.yanhua.home.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;

import java.util.HashMap;
import java.util.List;

public interface InviteContract {
    interface IView extends BaseView {
        default void handInviteConfig() {
        }

        default void handInviteList(ListResult<InviteModel> listResult) {
        }

        default void handleStsToken(StsTokenModel model) {
        }

        default void handInviteTypeList(List<InviteTypeModel> result) {
        }

        default void handInviteTemplateList(List<InviteTemplateModel> data) {
        }

        default void handAddInviteSuccess() {
        }

        default void handleAppProtocol(String code, ProtocolModel data) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        void meetPartnerTypeList(HashMap<String, Object> params);

        void meetPartnerTemplateList(HashMap<String, Object> params);

        void meetPartnerInviteAdd(InviteAddForm form);

        void meetPartnerInvitePage(HashMap<String, Object> params);

        void getConfigByType(int type);

        void meetPartnerCreateType();

        void getStsToken();
    }
}
