package com.yanhua.home.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonConfigModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.home.presenter.contract.BarContract;

import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BarPresenter extends MvpPresenter<BarContract.IView> implements BarContract.Presenter {

    @Override
    public void getCityOrArea() {
        addSubscribe(CommonHttpMethod.getInstance().getCityOrArea(new SimpleSubscriber<HttpResult<List<CityModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CityModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleCityOrAreaSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void getConfigByType(int type) {
        addSubscribe(CommonHttpMethod.getInstance().getConfigByType(new SimpleSubscriber<HttpResult<CommonConfigModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<CommonConfigModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    DiscoCacheUtils.getInstance().setCommonConfigType(type, result.getData().getContent());
                    baseView.handleBarConfig();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, type));
    }

    @Override
    public void getCommonReasonList(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarTypeListSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getBarList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getBarList(new SimpleSubscriber<HttpResult<ListResult<BarModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BarModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarListSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getBarDetail(String barId, HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getBarDetail(new SimpleSubscriber<HttpResult<BarModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<BarModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarDetailSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, barId, params));
    }

    @Override
    public void doLoveOrNotBar(String barId, int pos) {
        addSubscribe(HomeHttpMethod.getInstance().doLoveOrNotBar(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleLoveOrNotBarSuccess(result.getMesg(), pos);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, barId));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateCollectContentSuccess(result.getMesg());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, type, id));
    }

    @Override
    public void getBarQAList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getBarQAList(new SimpleSubscriber<HttpResult<ListResult<BarQAModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BarQAModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarQAListSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getBarQADetail(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getBarQADetail(new SimpleSubscriber<HttpResult<BarQAModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<BarQAModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarQADetailSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void getBarAnswerDetail(String askId) {
        addSubscribe(HomeHttpMethod.getInstance().getBarAnswerDetail(new SimpleSubscriber<HttpResult<MyQAModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<MyQAModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleBarAnswerDetailSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, askId));
    }

    @Override
    public void postMyQA(int type, HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().postMyQA(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    String data = (String) result.getData();
                    baseView.handlePostQASuccess(type, data);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void deleteMyQuestion(String askedId) {
        addSubscribe(HomeHttpMethod.getInstance().deleteMyQuestion(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleDeleteMyQuestionSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, askedId));
    }

    @Override
    public void deleteMyAnswer(String askedId) {
        addSubscribe(HomeHttpMethod.getInstance().deleteMyAnswer(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleDeleteMyAnswerSuccess();
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, askedId));
    }

    @Override
    public void deleteMyComment(String id, int pos) {
        addSubscribe(CommonHttpMethod.getInstance().deleteCommentItem(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleDeleteMyCommentSuccess(pos);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void getMyBarQAList(int type, HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().getMyBarQAList(new SimpleSubscriber<HttpResult<ListResult<MyQAModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MyQAModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleMyQAListSuccess(type, result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, params));
    }

    public void getBestNoteList(String city, int current, int size) {
        addSubscribe(MomentHttpMethod.getInstance().getBestNoteList(new SimpleSubscriber<HttpResult<ListResult<MomentListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<MomentListModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<MomentListModel> listResult = result.getData();
                    baseView.handleMomentHomeList(listResult);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, city, current, size));
    }

    @Override
    public void getCommentList(String contentId, int current, int childSize) {
        addSubscribe(CommonHttpMethod.getInstance().getCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<CommentModel> listResult = result.getData();
                    int totalSum = result.getData().getTotal();
                    baseView.handleCommentList(listResult.getRecords(), totalSum);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, contentId, current, childSize));
    }

    @Override
    public void getChildCommentList(String commentId, int current, int size) {
        addSubscribe(CommonHttpMethod.getInstance().getChildCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<CommentModel> listResult = result.getData();
                    baseView.handleChildCommentList(listResult);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, commentId, current, size));
    }

    @Override
    public void publishComment(PublishCommentForm form, int type) {
        if (null != form) {
            form.setType(type);
        }
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(CommonHttpMethod.getInstance().publishComment(new SimpleSubscriber<HttpResult<CommentModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<CommentModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handlePublishCommentSuccess(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, body));
    }

    @Override
    public void cancelFollowUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess(false);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    @Override
    public void followUser(String userId) {
        addSubscribe(CommonHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.updateFollowUserSuccess(true);
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, userId));
    }

    @Override
    public void doWorseAnswer(String id) {
        addSubscribe(CommonHttpMethod.getInstance().worseComment(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleWorseSuccess(result.getMesg());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void doLoveAnswer(String id) {
        addSubscribe(CommonHttpMethod.getInstance().starComment(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleLoveSuccess(result.getMesg());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void doLoveComment(String commentId) {
        addSubscribe(CommonHttpMethod.getInstance().starComment(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleLoveCommentSuccess(result.getMesg());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, commentId));
    }

    @Override
    public void doWorseComment(String commentId) {
        addSubscribe(CommonHttpMethod.getInstance().worseComment(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleWorseCommentSuccess(result.getMesg());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }, commentId));
    }
}
