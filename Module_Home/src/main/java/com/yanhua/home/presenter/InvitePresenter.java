package com.yanhua.home.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.CommonConfigModel;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.InviteCreateTypeModel;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteTemplateModel;
import com.yanhua.common.model.InviteTypeModel;
import com.yanhua.common.model.ProtocolModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.utils.DiscoCacheUtils;
import com.yanhua.home.presenter.contract.InviteContract;

import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class InvitePresenter extends MvpPresenter<InviteContract.IView> implements InviteContract.Presenter {

    public void getAppProtocol(String code) {
        addSubscribe(CommonHttpMethod.getInstance().getAppProtocol(new SimpleSubscriber<HttpResult<ProtocolModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ProtocolModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleAppProtocol(code,httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, code));
    }

    @Override
    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    @Override
    public void meetPartnerTypeList(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerTypeList(new SimpleSubscriber<HttpResult<List<InviteTypeModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<InviteTypeModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    List<InviteTypeModel> result = httpResult.getData();

                    DiscoCacheUtils.getInstance().setInviteTypeList(result);
                    baseView.handInviteTypeList(result);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    @Override
    public void meetPartnerTemplateList(HashMap<String, Object> params) {

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerTemplateList(new SimpleSubscriber<HttpResult<List<InviteTemplateModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<InviteTemplateModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteTemplateList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void meetPartnerInviteInfoUpdate(InviteAddForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteInfoUpdate(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handAddInviteSuccess();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, body));
    }

    @Override
    public void meetPartnerInviteAdd(InviteAddForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteAdd(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handAddInviteSuccess();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, body));
    }

    @Override
    public void getConfigByType(int type) {
        addSubscribe(CommonHttpMethod.getInstance().getConfigByType(new SimpleSubscriber<HttpResult<CommonConfigModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<CommonConfigModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    DiscoCacheUtils.getInstance().setCommonConfigType(type, httpResult.getData().getContent());
                    baseView.handInviteConfig();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, type));
    }

    /**
     * 新建约伴配置信息（伙伴类型、买单方式）
     */
    @Override
    public void meetPartnerCreateType() {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerCreateType(new SimpleSubscriber<HttpResult<InviteCreateTypeModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<InviteCreateTypeModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    DiscoCacheUtils.getInstance().setInviteCreateType(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }));
    }
    @Override
    public void meetPartnerInvitePage(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInvitePage(new SimpleSubscriber<HttpResult<ListResult<InviteModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<InviteModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    ListResult<InviteModel> listResult = httpResult.getData();
                    baseView.handInviteList(listResult);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void meetPartnerUpdateStatus(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerUpdateStatus(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }
}
