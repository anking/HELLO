package com.yanhua.home.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.InviteAddForm;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.model.InviteMemberNum;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteSetInfoModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.home.presenter.contract.InviteDetailContract;

import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class InviteDetailPresenter extends MvpPresenter<InviteDetailContract.IView> implements InviteDetailContract.Presenter {

    public void deleteCommentContent(String id, int pos) {
        addSubscribe(CommonHttpMethod.getInstance().deleteCommentItem(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteCommentSuccess(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void dealCommentItem2Top(String commentId, int type, int pos) {
        addSubscribe(CommonHttpMethod.getInstance().dealCommentItem2Top(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDealItemTopSuccess(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, commentId));
    }

    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    public void meetPartnerInviteDelete(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteDelete(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleDeleteContentSuccess();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }

    public void meetPartnerInviteSetInfo(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteSetInfo(new SimpleSubscriber<HttpResult<InviteSetInfoModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<InviteSetInfoModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleInviteSetInfo(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }

    public void meetPartnerInviteDetail(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteDetail(new SimpleSubscriber<HttpResult<InviteModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<InviteModel> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleInviteDetail(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }

    public void meetPartnerInviteInfoUpdate(InviteAddForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteInfoUpdate(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, body));
    }

    //-----------------------------------------------
    //同意申请加入
    public void meetPartnerInviteAgreeSignIn(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteAgreeSignIn(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteAgreenSuccess();
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }

    @Override
    public void publishComment(PublishCommentForm form, int type) {
        if (null != form) {
            form.setType(type);
        }

        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(CommonHttpMethod.getInstance().publishComment(new SimpleSubscriber<HttpResult<CommentModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<CommentModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handlePublishCommentSuccess(result.getData());
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, body));
    }

    @Override
    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    @Override
    public void updateStarComment(String id) {
        addSubscribe(CommonHttpMethod.getInstance().starComment(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarCommentSuccess();
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, id));
    }

    @Override
    public void getChildCommentList(String commentId, int current, int size) {
        addSubscribe(CommonHttpMethod.getInstance().getChildCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        baseView.handleChildCommentList(listResult);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, commentId, current, size));
    }

    @Override
    public void addViewCount(int type, String id) {
        addSubscribe(CommonHttpMethod.getInstance().addViewCount(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, type, id));
    }

    @Override
    public void getCommentList(String contentId, int current, int childSize) {
        addSubscribe(CommonHttpMethod.getInstance().getCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        int totalSum = result.getData().getTotal();
                        baseView.handleCommentList(listResult.getRecords(), totalSum);
                    } else {
                        baseView.handleErrorMsg(result.getMesg());
                    }
                }
            }
        }, contentId, current, childSize));
    }

    public void getCommonReasonList(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().getCommonReasonList(new SimpleSubscriber<HttpResult<List<CommonReasonModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<CommonReasonModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handCommonReason(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void meetPartnerInviteSignIn(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteSignIn(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteSignResult(true);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    /**
     * 取消报名
     */
    public void meetPartnerInviteSignOut(HashMap<String, Object> params) {

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteSignOut(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteSignResult(false);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    /**
     * 移除已通过的报名
     */
    public void meetPartnerInviteRemoveSigned(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteRemoveSigned(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteSignResult(false);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    public void meetPartnerStopReason(HashMap<String, Object> params) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerStopReason(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteSignResult(false);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    /**
     * 获取已加入和未加入的人数
     */
    public void meetPartnerInviteNum(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteNum(new SimpleSubscriber<HttpResult<InviteMemberNum>>(baseView, false) {
            @Override
            public void onNext(HttpResult<InviteMemberNum> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteMemberNum(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, id));
    }

    /**
     * 获取已加入和未加入的人
     */
    public void meetPartnerInviteMembers(HashMap<String, Object> params) {

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteMembers(new SimpleSubscriber<HttpResult<ListResult<InviteMemberModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<InviteMemberModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteMemberList(httpResult.getData());
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }


    public void meetPartnerInviteMembers(HashMap<String, Object> params,int type) {

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteMembers(new SimpleSubscriber<HttpResult<ListResult<InviteMemberModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<InviteMemberModel>> httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handInviteMemberList(httpResult.getData(),type);
                } else {
                    baseView.handleErrorMessage(httpResult.getMesg());
                }
            }
        }, params));
    }

    //---------------------------------------
/*
    public void meetPartnerInviteCollect(HashMap<String, Object> params) {
        params.put("inviteId", "0");//点赞的留言或回复所属的邀约id
        params.put("userId", "string");

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteCollect(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, params));
    }

    public void meetPartnerInviteUnCollect(String id) {
        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteUnCollect(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, id));
    }

    //---------------------------------------
    public void meetPartnerInviteMessage(HashMap<String, Object> params) {
        params.put("inviteId", "0");//邀约id
        params.put("messageContent", "string");//留言或回复内容
        params.put("messageType", "0");//(1:留言, 2:回复)
        params.put("parentMessageId", "string");//回复类型消息所属的留言id, 消息类型为'回复'时，需传入
        params.put("replyType", "0");//回复类型(0:回复留言, 1:回复回复)
        params.put("replyUserId", "string");//被回复的用户id
        params.put("userId", "string");//留言或回复用户ID

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteMessage(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, params));
    }

    public void meetPartnerInviteMessageList(HashMap<String, Object> params) {
        params.put("current", "0");
        params.put("size", "string");
        params.put("type", "0");//(1:留言, 2:回复)
        params.put("username", "string");//被回复的用户id
        params.put("messageId", "string");//留言id, 查询类型为2时, 需要传入该参数

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerInviteMessageList(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, params));
    }

    //-----------------------------------------

    public void meetPartnerAddFabulous(HashMap<String, Object> params) {
        params.put("fabulousType", 0);//点赞类型(1:留言, 2:回复)
        params.put("inviteId", "0");//点赞的留言或回复所属的邀约id
        params.put("messageId", "string");//点赞的留言或回复id
        params.put("userId", "string");

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerAddFabulous(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, params));
    }

    public void meetPartnerDeleteFabulous(String id) {

        addSubscribe(HomeHttpMethod.getInstance().meetPartnerDeleteFabulous(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult httpResult) {
                super.onNext(httpResult);
                if (httpResult.getCode().equals(HttpCode.SUCCESS)) {

                }
            }
        }, id));
    }
    //---------------------------------------------------*/
}
