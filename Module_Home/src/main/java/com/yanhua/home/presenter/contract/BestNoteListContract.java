package com.yanhua.home.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.common.model.ValueContentModel;

public interface BestNoteListContract {
    interface IView extends BaseView {
        default void handleStsToken(StsTokenModel model) {
        }

        default void handleMomentHomeList(ListResult<MomentListModel> listResult) {

        }

        default void updateStarContentSuccess() {
        }


        default  void handBestNoteCityList(ValueContentModel data){}
    }

    interface Presenter extends BasePresenter<IView> {
        void getStsToken();
    }
}
