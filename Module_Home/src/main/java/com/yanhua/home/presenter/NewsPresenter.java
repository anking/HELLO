package com.yanhua.home.presenter;

import com.google.gson.Gson;
import com.yanhua.base.model.HttpResult;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.MvpPresenter;
import com.yanhua.base.net.HttpCode;
import com.yanhua.base.net.SimpleSubscriber;
import com.yanhua.common.api.CommonHttpMethod;
import com.yanhua.common.api.HomeHttpMethod;
import com.yanhua.common.api.MomentHttpMethod;
import com.yanhua.common.api.UploadFileHttpMethod;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.PublishNewsForm;
import com.yanhua.common.model.PublishResultModel;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.model.StsTokenModel;
import com.yanhua.home.presenter.contract.NewsContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class NewsPresenter extends MvpPresenter<NewsContract.IView> implements NewsContract.Presenter {


    public void deleteCommentContent(String id,int pos) {
        addSubscribe(CommonHttpMethod.getInstance().deleteCommentItem(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteCommentSuccess(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void dealCommentItem2Top(String commentId,int type,int pos) {
        addSubscribe(CommonHttpMethod.getInstance().dealCommentItem2Top(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDealItemTopSuccess(pos);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        },type, commentId));
    }


    public void addMarketingClick(HashMap<String, Object> params) {
        addSubscribe(CommonHttpMethod.getInstance().addMarketingClick(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, params));
    }

    public void deleteContent(String id, int type) {
        HashMap<String, Object> params = new HashMap<>();
//        params.put("auditRemark","");//驳回原因
        ArrayList<String> contentIds = new ArrayList<>();
        contentIds.add(id);
        params.put("contentIds", contentIds);//驳回原因
        params.put("status", 5);//内容审核状态; -1驳回 0待审核 1正常 2隐藏 3平台删除 4沉帖 5 用户删除
        params.put("type", type);// 3 爆料 4 攻略

        addSubscribe(HomeHttpMethod.getInstance().deleteNewsContent(new SimpleSubscriber<HttpResult>(baseView, true) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);

                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleDeleteContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, params));
    }

    public void updateStarContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().starContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }


    public void updateCollectContent(String id, int type) {
        addSubscribe(CommonHttpMethod.getInstance().collectContent(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateCollectContentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type, id));
    }

    public void updateStarComment(String id) {
        addSubscribe(CommonHttpMethod.getInstance().starComment(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateStarCommentSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    public void cancelFollowUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().cancelFollowUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    public void followUser(String userId) {
        addSubscribe(MomentHttpMethod.getInstance().followUser(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.updateFollowUserSuccess();
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, userId));
    }

    public void getChildCommentList(String commentId, int current, int size) {
        addSubscribe(CommonHttpMethod.getInstance().getChildCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        baseView.handleChildCommentList(listResult);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, commentId, current, size));
    }

    public void publishComment(PublishCommentForm form, int type) {
        if (null != form) {
            form.setType(type);
        }

        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(CommonHttpMethod.getInstance().publishComment(new SimpleSubscriber<HttpResult<CommentModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<CommentModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handlePublishCommentSuccess(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, body));
    }

    public void getCommentList(String contentId, int current, int childSize) {
        addSubscribe(CommonHttpMethod.getInstance().getCommentList(new SimpleSubscriber<HttpResult<ListResult<CommentModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<CommentModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        ListResult<CommentModel> listResult = result.getData();
                        int totalSum = result.getData().getTotal();
                        baseView.handleCommentList(listResult.getRecords(), totalSum);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, contentId, current, childSize));
    }


    public void getStsToken() {
        addSubscribe(UploadFileHttpMethod.getInstance().getStsToken(new SimpleSubscriber<HttpResult<StsTokenModel>>(baseView, false) {
            @Override
            public void onNext(HttpResult<StsTokenModel> result) {
                super.onNext(result);
                if (result.getCode().equals(HttpCode.SUCCESS)) {
                    baseView.handleStsToken(result.getData());
                } else {
                    baseView.handleErrorMessage(result.getMesg());
                }
            }
        }));
    }

    public void addViewCount(int type, String id) {
        addSubscribe(CommonHttpMethod.getInstance().addViewCount(new SimpleSubscriber<HttpResult>(baseView, false) {
            @Override
            public void onNext(HttpResult result) {
                super.onNext(result);
            }
        }, type, id));
    }

    //-------start------爆料相关-------------------------------
    public void getNewsDetail(String id) {
        addSubscribe(HomeHttpMethod.getInstance().getNewsDetail(new SimpleSubscriber<HttpResult<NewsDetailModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<NewsDetailModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleNewsDetailContent(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, id));
    }

    /**
     * 首页看一看
     *
     * @param current
     * @param size
     */
    public void homePageBreakNewsList(int current, int size) {
        addSubscribe(HomeHttpMethod.getInstance().homePageBreakNewsList(new SimpleSubscriber<HttpResult<ListResult<BreakNewsRecommendModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BreakNewsRecommendModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size));
    }

    /**
     * 最新爆料滚动活动条栏---以及历史
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void getRecentActiveList(int type, int queryType,
                                    int current, int size,
                                    String city, String contentCategoryId) {

        addSubscribe(HomeHttpMethod.getInstance()
                .getRecentActiveList(new SimpleSubscriber<HttpResult<ListResult<ActivityModel>>>(baseView, false) {
                                         @Override
                                         public void onNext(HttpResult<ListResult<ActivityModel>> result) {
                                             super.onNext(result);
                                             if (result != null) {
                                                 if (result.getCode().equals(HttpCode.SUCCESS)) {
                                                     baseView.handleActivityListData(result.getData());
                                                 } else {
                                                     baseView.handleErrorMessage(result.getMesg());
                                                 }
                                             }
                                         }
                                     }, type, queryType,
                        current, size,
                        city, contentCategoryId));
    }

    /**
     * 最新爆料 推荐置顶
     * current size city contentCategoryId
     * //获取爆料分类和攻略分类
     *
     * @param queryType 活动查询类型 1正在进行中的 0 历史活动 正在进行的是按照结束时间正序 查询历史 是按照结束时间倒序
     * @param type      类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void getNewsRecommendList(int type, int queryType,
                                     int current, int size,
                                     String city, String contentCategoryId) {
        addSubscribe(HomeHttpMethod.getInstance()
                .getNewsRecommendList(new SimpleSubscriber<HttpResult<List<BreakNewsRecommendModel>>>(baseView, false) {
                                          @Override
                                          public void onNext(HttpResult<List<BreakNewsRecommendModel>> result) {
                                              super.onNext(result);
                                              if (result != null) {
                                                  if (result.getCode().equals(HttpCode.SUCCESS)) {
                                                      baseView.handleRecommendList(result.getData());
                                                  } else {
                                                      baseView.handleErrorMessage(result.getMesg());
                                                  }
                                              }
                                          }
                                      }, type, queryType,
                        current, size,
                        city, contentCategoryId));
    }

    /**
     * //获取爆料分类和攻略分类
     *
     * @param type 类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void getNewsTypeList(int type,int publish) {
        addSubscribe(HomeHttpMethod.getInstance().getNewsTypeList(new SimpleSubscriber<HttpResult<List<NewsTypeModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<NewsTypeModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleNewsTypeList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, type,publish));
    }

    /**
     * 这个获取城市不需要分页
     *
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void recentActiveCityList(String contentCategoryId, int type,int queryType) {
        addSubscribe(HomeHttpMethod.getInstance().recentActiveCityList(new SimpleSubscriber<HttpResult<List<String>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<String>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleCityList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, contentCategoryId, type,queryType));
    }

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param city              城市
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void cityContentList(int current, int size, String city, String keyword, String contentCategoryId, int type) {
        addSubscribe(HomeHttpMethod.getInstance().cityContentList(new SimpleSubscriber<HttpResult<ListResult<BreakNewsListModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<BreakNewsListModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleBreakNewList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size, city, keyword, contentCategoryId, type));
    }

    /**
     * @param current
     * @param size
     * @param contentCategoryId
     * @param type              类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void strategyList(int current, int size, String keyword, String contentCategoryId, int type) {
        addSubscribe(HomeHttpMethod.getInstance().strategyList(new SimpleSubscriber<HttpResult<ListResult<StrategyModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<ListResult<StrategyModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleStrategyList(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, current, size, keyword, contentCategoryId, type));
    }

    /**
     * @param attributeType     attributeType 属性类型;1:精华 2:热门 3:推荐 4:日记,5置顶,6:热,7:排行
     * @param contentCategoryId 这个不用管
     * @param type              type   类型 1:此刻 2:约伴 3:爆料 4:攻略 5:集锦 6:新闻 7酒吧新闻
     * @return
     */
    public void hotSortList(int attributeType, String contentCategoryId, int type) {
        addSubscribe(HomeHttpMethod.getInstance().hotSortList(new SimpleSubscriber<HttpResult<List<StrategyHotSortModel>>>(baseView, false) {
            @Override
            public void onNext(HttpResult<List<StrategyHotSortModel>> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handleStrategySortList(result.getData(), attributeType);
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, attributeType, contentCategoryId, type));
    }

    public void publishNewsDetail(PublishNewsForm form) {
        String json = new Gson().toJson(form);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);
        addSubscribe(HomeHttpMethod.getInstance().publishNewsDetail(new SimpleSubscriber<HttpResult<PublishResultModel>>(baseView, true) {
            @Override
            public void onNext(HttpResult<PublishResultModel> result) {
                super.onNext(result);
                if (result != null) {
                    if (result.getCode().equals(HttpCode.SUCCESS)) {
                        baseView.handPublishNewsResult(result.getData());
                    } else {
                        baseView.handleErrorMessage(result.getMesg());
                    }
                }
            }
        }, body));
    }

    //--------end-----爆料相关-------------------------------
}
