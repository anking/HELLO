package com.yanhua.home.presenter.contract;


import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.ContentUserInfoModel;
import com.yanhua.common.model.InviteMemberModel;
import com.yanhua.common.model.InviteMemberNum;
import com.yanhua.common.model.InviteModel;
import com.yanhua.common.model.InviteSetInfoModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.PublishCommentForm;
import com.yanhua.common.model.UserInfo;

import java.util.List;

public interface InviteDetailContract {
    interface IView extends BaseView {
        default void handleDeleteContentSuccess() {
        }

        default void handleContentUserDetailSuccess(ContentUserInfoModel model) {
        }

        default void handleUserDetail(UserInfo userInfo) {
        }

        default void handleErrorMsg(String msg) {
        }

        default void handleCommentList(List<CommentModel> list, int totalSum) {
        }

        default void handleChildCommentList(ListResult<CommentModel> listResult) {
        }

        default void handlePublishCommentSuccess(CommentModel model) {
        }

        default void updateCollectContentSuccess() {
        }

        default void updateStarCommentSuccess() {
        }

        default void handleContentListSuccess(List<MomentListModel> list) {
        }

        default void handleInviteDetail(InviteModel data) {
        }

        default void handInviteMemberNum(InviteMemberNum data) {
        }

        default void handInviteMemberList(ListResult<InviteMemberModel> data) {
        }
        default void handInviteMemberList(ListResult<InviteMemberModel> data,int type) {
        }

        default void handInviteSignResult(boolean signin) {
        }

        default void handCommonReason(List<CommonReasonModel> data) {
        }

        default void handInviteAgreenSuccess() {
        }

        default void handleDeleteCommentSuccess(int pos){}

        default void handleDealItemTopSuccess(int pos){}

        default void handleInviteSetInfo(InviteSetInfoModel data){}
    }

    interface Presenter extends BasePresenter<IView> {
        void getCommentList(String contentId, int current, int childSize);

        void publishComment(PublishCommentForm form, int type);

        void updateCollectContent(String id, int type);

        void updateStarComment(String id);

        void getChildCommentList(String commentId, int current, int size);

        void addViewCount(int type, String id);
    }
}
