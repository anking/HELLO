package com.yanhua.home.presenter.contract;

import com.shuyu.textutillib.model.TopicModel;
import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.AdvertiseModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CircleModel;
import com.yanhua.common.model.HomeSearchResultModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.UserModel;

import java.util.HashMap;
import java.util.List;

public interface HomeContract {

    interface IView extends BaseView {

        default void handleRecommendTopicList(List<TopicModel> listResult) {
        }

        default void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        }

        default void handHomePageBreakNewsList(ListResult<BreakNewsRecommendModel> data) {
        }

        default void handleAdvertiseList(List<AdvertiseModel> data, String code) {
        }

        default void updateStarContentSuccess() {
        }

        default void handleHomeSearchSuccess(ListResult<HomeSearchResultModel> result) {
        }

        default void handleUserSearchSuccess(ListResult<UserModel> result) {
        }

        default void handleHotTopicList(ListResult<TopicModel> result) {
        }

        default void handleHotCircleList(ListResult<CircleModel> result) {
        }

        default void updateFollowUserSuccess(boolean follow) {
        }

        default void updateCollectContentSuccess() {
        }

        default void updateFollowUserSuccess() {
        }

        default void updateFollowZHUserSuccess(boolean follow) {
        }
    }

    interface Presenter extends BasePresenter<IView> {

        void homeSearch(HashMap<String, Object> params);

        void userSearch(HashMap<String, Object> params);

        void getHotTopicList(HashMap<String, Object> params);

        void getHotCircleList(HashMap<String, Object> params);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void updateCollectContent(String id, int type);

        // 关注/取关内容里面的用户，区别于单纯的用户关注
        void cancelFollowContentUser(String userId);

        void followContentUser(String userId);

        // 关注/取关综合模块里面的用户item，区别于用户tab栏的关注操作
        void cancelFollowZHUser(String userId);

        void followZHUser(String userId);
    }
}
