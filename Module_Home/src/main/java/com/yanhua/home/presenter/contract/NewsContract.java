package com.yanhua.home.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.ActivityModel;
import com.yanhua.common.model.BreakNewsListModel;
import com.yanhua.common.model.BreakNewsRecommendModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.NewsDetailModel;
import com.yanhua.common.model.NewsTypeModel;
import com.yanhua.common.model.PublishResultModel;
import com.yanhua.common.model.StrategyHotSortModel;
import com.yanhua.common.model.StrategyModel;
import com.yanhua.common.model.StsTokenModel;

import java.util.List;

public interface NewsContract {
    interface IView extends BaseView {
        default void handleStsToken(StsTokenModel model) {
        }

        default void handleCommentList(List<CommentModel> list, int totalSum) {
        }

        default void handPublishNewsResult(PublishResultModel data) {
        }

        default void handleNewsTypeList(List<NewsTypeModel> data) {
        }

        default  void handleRecommendList(List<BreakNewsRecommendModel> data){}

        default void handleCityList(List<String> data){}

        default void handleBreakNewList(ListResult<BreakNewsListModel> data){}

        default void handleActivityListData(ListResult<ActivityModel> data){}

        default void handleDeleteContentSuccess(){}

        default void updateStarContentSuccess(){}


        default void handleChildCommentList(ListResult<CommentModel> listResult){}

        default  void handlePublishCommentSuccess(CommentModel model){}

        default void updateCollectContentSuccess(){}


        default  void updateStarCommentSuccess(){}

        default  void updateFollowUserSuccess(){}


        default void handleStrategySortList(List<StrategyHotSortModel> data, int attributeType){}

        default void handleStrategyList(ListResult<StrategyModel> data){}

        default void handleNewsDetailContent(NewsDetailModel data){}

        default void handleDeleteCommentSuccess(int pos){}

        default void handleDealItemTopSuccess(int pos){}
    }

    interface Presenter extends BasePresenter<IView> {


    }
}
