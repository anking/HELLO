package com.yanhua.home.presenter.contract;

import com.yanhua.base.model.ListResult;
import com.yanhua.base.mvp.BasePresenter;
import com.yanhua.base.mvp.BaseView;
import com.yanhua.common.model.BarModel;
import com.yanhua.common.model.BarQAModel;
import com.yanhua.common.model.CityModel;
import com.yanhua.common.model.CommentModel;
import com.yanhua.common.model.CommonReasonModel;
import com.yanhua.common.model.MomentListModel;
import com.yanhua.common.model.MyQAModel;
import com.yanhua.common.model.PublishCommentForm;

import java.util.HashMap;
import java.util.List;

public interface BarContract {

    interface IView extends BaseView {
        default void handleBarListSuccess(ListResult<BarModel> list) {
        }

        default void handleBarDetailSuccess(BarModel model) {
        }

        default void handleBarQAListSuccess(ListResult<BarQAModel> list) {
        }

        default void handleBarQADetailSuccess(BarQAModel model) {
        }

        default void handlePostQASuccess(int type, String data) {
        }

        default void handleDeleteMyQuestionSuccess() {
        }

        default void handleDeleteMyAnswerSuccess() {
        }

        default void handleDeleteMyCommentSuccess(int pos) {
        }

        default void handleMyQAListSuccess(int type, ListResult<MyQAModel> list) {
        }

        default void handlePostCommentSuccess() {
        }

        default void handleBarConfig() {
        }

        default void handleMomentHomeList(ListResult<MomentListModel> listResult) {
        }

        default void handleLoveOrNotBarSuccess(String msg, int pos) {
        }

        default void updateCollectContentSuccess(String msg) {
        }

        default void handleBarAnswerDetailSuccess(MyQAModel model) {
        }

        default void handleBarTypeListSuccess(List<CommonReasonModel> list) {
        }

        default void handleCityOrAreaSuccess(List<CityModel> list) {
        }

        default void handleCommentList(List<CommentModel> list, int totalSum) {
        }

        default void handleChildCommentList(ListResult<CommentModel> listResult) {
        }

        default void updateFollowUserSuccess(boolean follow) {
        }

        default void handlePublishCommentSuccess(CommentModel model) {
        }

        default void handleWorseSuccess(String msg) {
        }

        default void handleLoveSuccess(String msg) {
        }

        default void handleLoveCommentSuccess(String msg) {
        }

        default void handleWorseCommentSuccess(String msg) {
        }
    }

    interface Presenter extends BasePresenter<IView> {
        void getCityOrArea();

        void getConfigByType(int type);

        void getCommonReasonList(HashMap<String, Object> params);

        void getBarList(HashMap<String, Object> params);

        void getBarDetail(String barId, HashMap<String, Object> params);

        void doLoveOrNotBar(String barId, int pos);

        void updateCollectContent(String id, int type);

        void getBarQAList(HashMap<String, Object> params);

        void getBarQADetail(HashMap<String, Object> params);

        void getBarAnswerDetail(String askId);

        void postMyQA(int type, HashMap<String, Object> params);

        void deleteMyQuestion(String askedId);

        void deleteMyAnswer(String askedId);

        void deleteMyComment(String id, int pos);

        void getMyBarQAList(int type, HashMap<String, Object> params);

        void getCommentList(String contentId, int current, int childSize);

        void getChildCommentList(String commentId, int current, int size);

        void publishComment(PublishCommentForm form, int type);

        void cancelFollowUser(String userId);

        void followUser(String userId);

        void doWorseAnswer(String id);

        void doLoveAnswer(String id);

        void doLoveComment(String commentId);

        void doWorseComment(String commentId);
    }
}
