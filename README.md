## 工程尽量最低适配到SDK >=16\19(21)---其他的一些三方到用到的时候加上（支付、地图、图片选择等等）

## 项目工程模块说明
* include ':YHCommon' 公司Lib构建（YX_XYZ）【网络、图片、工具之类的】(后续可以复用--特殊页面请写入特定模块内定义)基层
* --------------------------------------------------------------------------------
* include ':YHBase' 定义一些基础类，根据不同业务再以App级别定义App基类（Net）
* include ':YHUser' 见一
* include ':YHThird' 其他三方的库（融云、图片、升级等）(后续可以服用)---thirdSdk
* //include ':YHNet' 通讯中间层（根据公司定义的协议进行封装）=----已经合并到Base中
* include ':YHMedia' 视频、图片合成编辑（腾讯云短视频SDK）
* --------------------------------------上方的作为公用的，可复用的，后续整理作为依赖库导入，避免没必要的改动-------------------------------------------------------------------
* include ':appCommon' App级别一些继承下来的类或者一些通用的资源
* include ':app' 入口
* include ':module_XXX' 模块moduleXXX
* include ':Module_Home' 首页
* include ':Module_Moment' 此刻---（展示UI布局、核心在YHMedia）
* include ':Module_Mine' 我的 ---（展示UI布局、核心在YHUser）

##渠道
* 在gradle中增加assembleEnvProduct

## 大致模块说明
### 一、App通用模块（User）
        * 1、引导页
		* 2、启动页
		* 3、广告
		* 4、登录注册
		* 5、关于我们
		* 6、修改密码
		* 7、常见问题
		* 8、联系客户
		* 9、认证中心
		* 10、意见反馈
		* 认证中心
		* 认证中心——视频认证
		* 认证中心——实名认证
		* 认证结果
		* 联系客服
		* 意见反馈
		* 常见问题
		* 分享好友
		* 设置
		* 设置-账号安全
		* 设置-账号安全-修改密码
		* 设置-关于我们
		* 设置-隐私权限设置
		* 个人主页
		* 个人主页-个人资料
### 二、Main---业务性比较复杂--看写（约伴、新闻、圈子等、酒吧/KTV、此刻）
### 三、Message---业务性比较复杂(后续可以服用)消息（）
### 四、其他三方的库（融云、图片、升级等）(后续可以服用)---thirdSdk
### 五、公司Lib构建（YX_XYZ）【网络、图片、工具之类的】(后续可以服用)---Common
### //六、通讯中间层
### 七、视频、图片合成编辑（腾讯云短视频SDK）
### 八、Base---定义一些基础类


### appCommon
 api 包含所有模块的以 模块名___API   模块名_XXX_Model

Common
Base ThirdSDK
Media Message User 



## 注意：
*1).功能模块和类型模块均可以划分，如果没有需要的话，模块的划分都可以省略。
*2).activity和service这类组件划分，如果没有需要的话，组件的划分都可以省略。
*3).所有的划分，如果没有需要的话，所有的划分都可以省略。

## 规范问题：
    *为了避免合作开发写的代码风格迥异。或做出了多套开发模式。这样利于生命周期管理。也可以方便的全局修改。

### 所有Activity继承BaseActivity
### 所有Fragment继承BaseFragment
### 所有Presenter继承BasePresenter

### 命名(模块Xxx功能Yyy)，eg：
* XxxYyyFragment
* XxxYyyActivity

### layout命名(模块Xxx功能Yyy)，eg：
* activityXxxyyy
* fragmentXxxyyy
* itemXxxyyy
* include_toolbar
* mergeXxxyyy
* viewXxxyyy

### id命名（控件缩写+(模块Xxx功能Yyy)），例
* btnXxxyyy
* tvXxxyyy
* listXxxyyy
* etXxxyyy

### 变量命名:以m开头。


### 方法命名:与其写好名字不如写好注释。

### 定好网络请求
### 文件存储方式与位置
### 类库框架用法
### 三方SDK
* 高德
* QQ
* 微信
* 融云
* 极光
* 几大厂商通道（目前针对华为、小米、OPPO、Vivo、魅族）
* 授权登录（QQ、微信）+分享

## 关于
不再区分（开发、测试、预生产）直接通过设置处进行修改--也就是这三公用一个packageName包名
# to be continue


